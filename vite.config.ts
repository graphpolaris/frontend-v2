import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';
import path from 'path';
// import sassDts from "vite-plugin-sass-dts";
import ImportMetaEnvPlugin from '@import-meta-env/unplugin';

// https://vite.dev/config/
export default defineConfig({
  plugins: [
    react(),
    // basicSsl(),
    // dts({
    //   insertTypesEntry: true,
    // }),
    // sassDts(),
    ImportMetaEnvPlugin.vite({
      example: '.env.example',
    }),
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
  optimizeDeps: {},
});
