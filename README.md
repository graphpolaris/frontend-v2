# GraphPolaris Frontend

## Preparing to Run Locally

Be sure that you have node.js (v18 or up) and bun installed.
Please use bun for building and running the scripts of package.json.

## Running Locally

### Install

Before anything, make sure to have bun.sh installed.

First run `bun i` on the root of the workspace to install all dependencies.

If you need to install a new dependency or upgrade an existing one, do so in the respective package.json and rerun `bun i` in the root of the workspace. If you find any issues, feel free to delete the node_modules folder from the workspace root and from the app/lib which you are changing the dependency and rerun the `bun i` command from the root of the workspace. Most issues are solved by then.

Secondly, make sure you create a local `.env` file. For local development, you can simply copy the contents of the `.env.development` template.

### Running Storybook.js

To run the dev storybook (implementing visualizations) simply run `bun sb` and once it is running, ctrl-click the link what appears in the terminal. The url should be [http://localhost:6006].

### Dev server

The dev server (as of now) expects the backend to be also running in your machine. Therefore, for it to be useful you will need first to have the Backend
running. After that you also need to be sure tp update your hosts file. Finally, to run the application simply run `bun dev` from the workspace root.

#### Hosts file

To configure the hosts file, you need to first find it in your machine.

- Windows: c:\Windows\System32\Drivers\etc\hosts
- Mac/Linux: /etc/hosts

Open the file with administrative privileges (e.g. sudo nano, sudo vim, or use vscode and when you try to save it will prompt you to save as an administrator) and add these two lines at the end:

```
127.0.0.1 local.graphpolaris.com
127.0.0.1 api.graphpolaris.com
```

This will make the dev server to try to connect to the backend running in your own machine. If you want to make the dev server to call the production (cloud-based) backend of graph polaris, only add the first line of the two like so:

```
127.0.0.1 local.graphpolaris.com
```

### Other Commands

You can run bun commands (see available ones in packages.json) from the root of the workspace, which will trigger turborepo to run it in all libraries and apps of the workspace. You can run `bun test` or `bun lint` this way to test or lint the entire workspace.

You can also go into a specific lib/app and run bun commands from there to scope the task to only that part of the workspace.
