import type { Preview } from '@storybook/react';
import '../src/main.css';

const preview: Preview = {
  decorators: [
    (Story, context) => {
      if (document?.body?.className !== undefined) {
        document.body.className += ' light-mode';
      }
      return <Story />;
    },
  ],
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
    actions: {},
    docs: {},
  },
};

export default preview;
