import Graph from 'graphology';
import { Attributes } from 'graphology-types';
import { Layout } from './layout';
import { ILayoutFactory } from './layoutCreatorUsecase';

export type DagreProvider = 'Dagre';
export type DagreLayoutAlgorithms = 'Dagre_Dagre';

export enum DagreLayouts {
  DAGRE = 'Dagre_Dagre',
}

export class DagreFactory implements ILayoutFactory<DagreLayoutAlgorithms> {
  public createLayout() {
    return new DagreLayout();
  }
}

export class DagreLayout extends Layout<DagreProvider> {
  constructor() {
    super('Dagre', 'Dagre_Dagre');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
    direction: string = 'TB',
  ): Promise<void> {
    const dagre = await import('@dagrejs/dagre');

    const dagreGraph = new dagre.graphlib.Graph().setDefaultEdgeLabel(() => ({})).setGraph({ rankdir: direction });

    const gg = graph.export();
    gg.nodes.forEach(node => {
      dagreGraph.setNode(node.key, { label: node.key, width: 150, height: 50 });
    });
    gg.edges.forEach(edge => {
      dagreGraph.setEdge(edge.source, edge.target);
    });

    dagre.layout(dagreGraph, {});

    graph.forEachNode(node => {
      const nodeWithPosition = dagreGraph.node(node);
      const x = nodeWithPosition.x + (boundingBox ? boundingBox.x1 : 0);
      const y = nodeWithPosition.y + (boundingBox ? boundingBox.y1 : 0);
      graph.setNodeAttribute(node, 'x', x);
      graph.setNodeAttribute(node, 'y', y);
    });
  }

  /**
   * Retrieves the position of a node in the graph layout.
   * @param nodeId - The ID of the node.
   * @returns The position of the node as an object with `x` and `y` coordinates.
   * @throws Error if the node is not found in the current graph.
   */
  public getNodePosition(nodeId: string) {
    if (this.graph === null) {
      throw new Error('The graph is not set.');
    }

    return this.graph.getNodeAttributes(nodeId);
  }
}
