import cytoscape from 'cytoscape';
import Graph from 'graphology';
import { Attributes } from 'graphology-types';
import { Layout } from './layout';
import { ILayoutFactory } from './layoutCreatorUsecase';
import { LayoutAlgorithm } from './types';

export type CytoscapeProvider = 'Cytoscape';

export type CytoscapeLayoutAlgorithms =
  | 'Cytoscape_klay'
  | 'Cytoscape_dagre'
  | 'Cytoscape_elk'
  | 'Cytoscape_fcose'
  | 'Cytoscape_cose-bilkent'
  | 'Cytoscape_cise'
  | 'Cytoscape_cose'
  | 'Cytoscape_grid'
  | 'Cytoscape_circle'
  | 'Cytoscape_concentric'
  | 'Cytoscape_breadthfirst';

export enum CytoscapeLayouts {
  KLAY = 'Cytoscape_klay',
  DAGRE = 'Cytoscape_dagre',
  ELK = 'Cytoscape_elk',
  FCOSE = 'Cytoscape_fcose',
  COSE_BILKENT = 'Cytoscape_cose-bilkent',
  CISE = 'Cytoscape_cise',
  GRID = 'Cytoscape_grid',
  COSE = 'Cytoscape_cose',
  CIRCLE = 'Cytoscape_circle',
  CONCENTRIC = 'Cytoscape_concentric',
  BREATHFIRST = 'Cytoscape_breadthfirst',
}

type CytoNode = {
  data: {
    id: string;
    type?: string;
    source?: string;
    target?: string;
    position?: {
      x: number;
      y: number;
    };
    label?: string;
    count?: number;
    color?: string;

    sbgnbbox?: {
      x: number;
      y: number;
      w: number;
      h: number;
    };
  };
};

const DEFAULTWIDTH = 120;
const DEFAULTHEIGHT = 60;

/**
 * This is the Cytoscape Factory
 */
export class CytoscapeFactory implements ILayoutFactory<CytoscapeLayoutAlgorithms> {
  createLayout(LayoutAlgorithm: CytoscapeLayoutAlgorithms): CytoscapeLayout | null {
    switch (LayoutAlgorithm) {
      case 'Cytoscape_klay':
        //https://github.com/cytoscape/cytoscape.js-klay
        return new CytoscapeKlay();
      case 'Cytoscape_dagre':
        //https://github.com/cytoscape/cytoscape.js-dagre
        return new CytoscapeDagre();
      case 'Cytoscape_elk':
        //https://github.com/cytoscape/cytoscape.js-elk
        return new CytoscapeElk();

      case 'Cytoscape_fcose':
        //https://github.com/iVis-at-Bilkent/cytoscape.js-fcose
        return new CytoscapeFCose();

      case 'Cytoscape_cose-bilkent':
        //https://github.com/cytoscape/cytoscape.js-cose-bilkent
        return new CytoscapeCoseBilkent();

      case 'Cytoscape_cise':
        //https://github.com/iVis-at-Bilkent/cytoscape.js-cise
        return new CytoscapeCise();

      case 'Cytoscape_grid':
        //https://js.cytoscape.org/#layouts
        return new CytoscapeGrid();

      case 'Cytoscape_circle':
        //https://js.cytoscape.org/#layouts
        return new CytoscapeCircle();
      case 'Cytoscape_concentric':
        //https://js.cytoscape.org/#layouts
        return new CytoscapeConcentric();

      case 'Cytoscape_breadthfirst':
        //https://js.cytoscape.org/#layouts
        return new CytoscapeBreathFirst();

      case 'Cytoscape_cose':
        //https://js.cytoscape.org/#layouts
        return new CytoscapeCose();
      default:
        return null;
    }
  }
}

export abstract class CytoscapeLayout extends Layout<CytoscapeProvider> {
  protected cytoscapeInstance: cytoscape.Core | undefined = undefined;

  protected defaultLayoutSettings = {
    animate: false,
    animationDuration: 5000,
    ready: function () {}, // on layoutready
    stop: () => {
      this.updateNodePositions();
    }, // on layoutstop
  };

  constructor(public override algorithm: LayoutAlgorithm<CytoscapeProvider>) {
    super('Cytoscape', algorithm);
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    const cytonodes: CytoNode[] = this.convertToCytoscapeModel(graph);
    this.createCytoscapeInstance(cytonodes);
  }

  /**
   * Converts a graph to a Cytoscape model.
   * @param graph The graph to be converted.
   * @returns An array of CytoNode objects representing the graph in Cytoscape format.
   */
  public convertToCytoscapeModel(graph: Graph): CytoNode[] {
    const cytonodes: CytoNode[] = [];

    graph.forEachNode(node => {
      cytonodes.push({
        data: {
          id: node,
          type: 'node',
          label: 'start',
          count: 50,
          color: 'green',

          sbgnbbox: {
            x: 0,
            y: 0,
            w: DEFAULTWIDTH,
            h: DEFAULTHEIGHT,
          },
        },
      });
    });

    graph.forEachEdge((edge, _attributes, source, target) => {
      cytonodes.push({
        data: {
          id: edge,
          type: 'edge',
          source: source,
          target: target,
        },
      });
    });

    return cytonodes;
  }

  /**
   * Retrieves the position of a node in the graph layout.
   * @param nodeId - The ID of the node.
   * @returns The position of the node as an object with `x` and `y` coordinates.
   * @throws Error if the node is not found in the current graph.
   */
  public getNodePosition(nodeId: string) {
    if (!this.cytoscapeInstance) {
      return { x: 0, y: 0 };
    }
    const node = this.cytoscapeInstance.getElementById(nodeId);
    if (!node) {
      throw Error('Node not found in current graph, Cannot retrieve layout position.');
    }
    return node.position();
  }

  /**
   * Retrieves the full layout update.
   * @returns The updated graph layout or null if the cytoscape instance or graph is not available.
   */
  public getFullLayoutUpdate() {
    if (!this.cytoscapeInstance || !this.graph) {
      return null;
    }
    this.updateNodePositions();
    return this.graph;
  }

  createCytoscapeInstance(cytonodes: CytoNode[]) {
    this.cytoscapeInstance = cytoscape({
      elements: cytonodes,
      headless: true,
      styleEnabled: true,
    });

    return this.cytoscapeInstance;
  }

  protected updateNodePositions() {
    if (!this.graph || !this.cytoscapeInstance) {
      return;
    }
    this.cytoscapeInstance.nodes().forEach((node: cytoscape.NodeSingular) => {
      if (this.verbose) {
        console.log(node.id(), node.position());
      }

      // graph.setNodeAttribute(node.id(), 'x', node.position().x);
      // graph.setNodeAttribute(node.id(), 'y', node.position().y);

      if (!this.graph) return;

      if (!this.graph.hasNode(node.id())) {
        throw Error('Node not found in graph; Cannot update position.');
      }

      this.graph.updateNode(node.id(), attr => {
        return {
          ...attr,
          x: node.position().x,
          y: node.position().y,
        };
      });
    });
  }
}

function getWidth(node: any) {
  /**
  Calculate the width of a node given its text label `node.data('lbl')`
  */

  // Create element with attributes needed to calculate text size
  const ctx = document.createElement('canvas').getContext('2d');
  const fStyle = node.pstyle('font-style').strValue;
  const size = node.pstyle('font-size').pfValue + 'px';
  const family = node.pstyle('font-family').strValue;
  const weight = node.pstyle('font-weight').strValue;
  ctx!.font = fStyle + ' ' + weight + ' ' + size + ' ' + family;

  // For multiple lines, evaluate the width of the largest line
  const lines = node.data('lbl').split('\n');
  const lengths = lines.map((a: any) => a.length);
  const max_line = lengths.indexOf(Math.max(...lengths));

  // User-defined padding
  const padding = 30;

  return ctx!.measureText(lines[max_line]).width + padding;
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeKlay extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_klay');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    await import('cytoscape-klay').then(m => {
      cytoscape.use(m.default);
    });
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: boundingBox,
      name: 'klay',
      padding: 5,
      klay: {
        borderSpacing: 40,
        aspectRatio: 0.5,
        compactComponents: true,
      },
    } as any);
    layout.run();

    // all options here: https://github.com/cytoscape/cytoscape.js-klay/blob/master/README.md
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeElk extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_elk');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    //@ts-expect-error - cytoscape-elk does not have types
    await import('cytoscape-elk').then(m => {
      cytoscape.use(m.default);
    });
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    cy.style().fromJson([
      {
        selector: 'node',
        style: {
          shape: 'rectangle',
          width: function (n: any) {
            return n['_private'].data.sbgnbbox?.w || DEFAULTWIDTH;
          },
          height: function (n: any) {
            return n['_private'].data.sbgnbbox?.h || DEFAULTHEIGHT;
          },
        },
      },
      {
        selector: 'edge',
        style: {
          opacity: 0.5,
        },
      },
    ]);

    // options here https://github.com/cytoscape/cytoscape.js-elk
    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      // boundingBox: boundingBox,
      name: 'elk',
      // nodeDimensionsIncludeLabels: true,
      // fit: true,
      // ranker: 'longest-path',
      // animate: false,
      // padding: 30,
      elk: {
        // zoomToFit: true,
        algorithm: 'box',
        // separateConnectedComponents: false,
        // direction: 'DOWN',
      },
    } as any);
    const layouts = layout.run();

    console.log('layouts', layouts);

    this.updateNodePositions();
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeDagre extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_dagre');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    await import('cytoscape-dagre').then(m => {
      cytoscape.use(m.default);
    });
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: boundingBox,
      name: 'dagre',
      // acyclicer: 'greedy',
      ranker: 'longest-path',
      spacingFactor: 0.95,
    } as any);
    layout.run();

    this.updateNodePositions();
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeFCose extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_fcose');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    await import('cytoscape-fcose').then(m => cytoscape.use(m.default));
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: boundingBox,
      // boundingBox: { x1: 500, x2: 2000, y1: 1000, y2: 2000 },
      name: 'fcose',
    } as any);
    layout.run();

    this.updateNodePositions();
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeCoseBilkent extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_cose-bilkent');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    //@ts-expect-error - cytoscape-cose-bilkent does not have types
    await import('cytoscape-cose-bilkent').then(m => cytoscape.use(m.default));
    super.layout(graph, boundingBox);

    // options here: https://github.com/cytoscape/cytoscape.js-cose-bilkent

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      animate: false,
      animationDuration: 10000,
      boundingBox: boundingBox,
      name: 'cose-bilkent',
      randomize: true,
      idealEdgeLength: 30,
    } as any);

    layout.run();

    this.updateNodePositions();

    // cy.on('position', 'node', (event) => {
    //   const node = event.target;
    //   // The node's position has changed
    //   // You can access the new position with node.position()
    //   this.updateNodePositions();
    // });
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeCise extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_cise');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    //@ts-expect-error - cytoscape-cise does not have types
    await import('cytoscape-cise').then(m => cytoscape.use(m.default));
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: boundingBox,
      name: 'cise',
    } as any);
    layout?.run();

    this.updateNodePositions();
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeCose extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_cose');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: boundingBox,
      name: 'cose',
    } as any);
    layout?.run();

    this.updateNodePositions();
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeGrid extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_grid');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: boundingBox,
      name: 'grid',
    } as any);
    layout?.run();

    this.updateNodePositions();
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeCircle extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_circle');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: boundingBox,

      name: 'circle',
    } as any);
    layout?.run();

    this.updateNodePositions();
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeConcentric extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_concentric');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: boundingBox,
      name: 'concentric',
    } as any);

    layout?.run();

    this.updateNodePositions();
  }
}

/**
 * This is a ConcreteProduct
 */
class CytoscapeBreathFirst extends CytoscapeLayout {
  constructor() {
    super('Cytoscape_breadthfirst');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    const cy = this.cytoscapeInstance;
    if (!cy) return;

    const layout = cy.layout({
      ...this.defaultLayoutSettings,
      boundingBox: this.boundingBox,

      name: 'breadthfirst',
    } as any);

    layout.run();

    this.updateNodePositions();
  }
}
