import { CytoscapeLayout, CytoscapeLayoutAlgorithms, CytoscapeLayouts, CytoscapeProvider } from './cytoscapeLayouts';
import { DagreLayout, DagreLayoutAlgorithms, DagreLayouts, DagreProvider } from './dagreLayout';
import { GraphologyLayout, GraphologyLayoutAlgorithms, GraphologyLayouts, GraphologyProvider } from './graphologyLayouts';
import { ListLayout, ListLayoutAlgorithms, ListLayoutProvider, ListLayouts } from './listLayouts';

export type AllLayoutAlgorithms = GraphologyLayoutAlgorithms | CytoscapeLayoutAlgorithms | ListLayoutAlgorithms | DagreLayoutAlgorithms;

export type Providers = GraphologyProvider | CytoscapeProvider | ListLayoutProvider | DagreProvider;
export type LayoutAlgorithm<Provider extends Providers> = `${Provider}_${string}`;

export type AlgorithmToLayoutProvider<Algorithm extends AllLayoutAlgorithms = AllLayoutAlgorithms> =
  Algorithm extends GraphologyLayoutAlgorithms
    ? GraphologyLayout
    : Algorithm extends CytoscapeLayoutAlgorithms
      ? CytoscapeLayout
      : Algorithm extends ListLayoutAlgorithms
        ? ListLayout
        : DagreLayout;

export const Layouts = {
  ...GraphologyLayouts,
  ...CytoscapeLayouts,
  ...ListLayouts,
  ...DagreLayouts,
} as const;

export type LayoutTypes = (typeof Layouts)[keyof typeof Layouts];
