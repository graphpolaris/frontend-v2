import Graph from 'graphology';
import { circular, random } from 'graphology-layout';
import forceAtlas2, { ForceAtlas2Settings } from 'graphology-layout-forceatlas2';
import FA2Layout from 'graphology-layout-forceatlas2/worker';
import noverlap from 'graphology-layout-noverlap';
import { Attributes } from 'graphology-types';
import { Layout } from './layout';
import { ILayoutFactory } from './layoutCreatorUsecase';
import { LayoutAlgorithm } from './types';

export type GraphologyProvider = 'Graphology';

export type GraphologyLayoutAlgorithms =
  | `Graphology_circular`
  | `Graphology_random`
  | `Graphology_noverlap`
  | `Graphology_forceAtlas2`
  | `Graphology_forceAtlas2_webworker`;

export enum GraphologyLayouts {
  RANDOM = 'Graphology_random',
  CIRCULAR = 'Graphology_circular',
  NOVERLAP = 'Graphology_noverlap',
  FORCEATLAS2 = 'Graphology_forceAtlas2',
  FORCEATLAS2WEBWORKER = 'Graphology_forceAtlas2_webworker',
}

/**
 * This is the Graphology Constructor for the main layouts available at
 * https://graphology.github.io/
 */
export class GraphologyFactory implements ILayoutFactory<GraphologyLayoutAlgorithms> {
  createLayout(layoutAlgorithm: GraphologyLayoutAlgorithms): GraphologyLayout | null {
    switch (layoutAlgorithm) {
      case 'Graphology_random':
        return new GraphologyRandom();
      case 'Graphology_circular':
        return new GraphologyCircular();
      case 'Graphology_noverlap':
        return new GraphologyNoverlap();
      case 'Graphology_forceAtlas2':
        return new GraphologyForceAtlas2();
      case 'Graphology_forceAtlas2_webworker':
        return new GraphologyForceAtlas2Webworker();
      default:
        return null;
    }
  }
}

export abstract class GraphologyLayout extends Layout<GraphologyProvider> {
  protected defaultLayoutSettings = {
    dimensions: ['x', 'y'],
    center: 0.5,
  };

  constructor(public override algorithm: LayoutAlgorithm<GraphologyProvider>) {
    super('Graphology', algorithm);
  }

  /**
   * Retrieves the position of a node in the graph layout.
   * @param nodeId - The ID of the node.
   * @returns The position of the node as an object with `x` and `y` coordinates.
   * @throws Error if the node is not found in the current graph.
   */
  public getNodePosition(nodeId: string) {
    if (this.graph === null) {
      throw new Error('The graph is not set.');
    }

    return this.graph.getNodeAttributes(nodeId);
  }
}

/**
 * This is a ConcreteProduct
 */
export class GraphologyCircular extends GraphologyLayout {
  constructor() {
    super('Graphology_circular');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);
    // To directly assign the positions to the nodes:
    circular.assign(graph, {
      scale: (graph.order * graph.order) / 10,
      ...this.defaultLayoutSettings,
    });
  }
}

/**
 * This is a ConcreteProduct
 */
export class GraphologyRandom extends GraphologyLayout {
  constructor() {
    super('Graphology_random');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);
    // const positions = random(graph);

    // To directly assign the positions to the nodes:
    random.assign(graph, {
      scale: (graph.order * graph.order) / 10,
      ...this.defaultLayoutSettings,
      center: 0,
    });
  }
}

const DEFAULT_NOVERLAP_SETTINGS = {
  margin: 40,
  ratio: 40,
  gridSize: 50,

  // gridSize ?number 20: number of grid cells horizontally and vertically subdivising the graph’s space. This is used as an optimization scheme. Set it to 1 and you will have O(n²) time complexity, which can sometimes perform better with very few nodes.
  // margin ?number 5: margin to keep between nodes.
  // expansion ?number 1.1: percentage of current space that nodes could attempt to move outside of.
  // ratio ?number 1.0: ratio scaling node sizes.
  // speed ?number 3: dampening factor that will slow down node movements to ease the overall process.
};

/**
 * This is a ConcreteProduct
 */
export class GraphologyNoverlap extends GraphologyLayout {
  constructor() {
    super('Graphology_noverlap');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);
    // To directly assign the positions to the nodes:
    noverlap.assign(graph, {
      maxIterations: 10000,
      settings: {
        ...this.defaultLayoutSettings,
        ...DEFAULT_NOVERLAP_SETTINGS,
      },
    });
  }
}

const DEFAULT_FORCEATLAS2_SETTINGS: ForceAtlas2Settings = {
  gravity: 1,
  adjustSizes: true,
  linLogMode: true,
  strongGravityMode: true,

  // adjustSizes ?boolean false: should the node’s sizes be taken into account?
  // barnesHutOptimize ?boolean false: whether to use the Barnes-Hut approximation to compute repulsion in O(n*log(n)) rather than default O(n^2), n being the number of nodes.
  // barnesHutTheta ?number 0.5: Barnes-Hut approximation theta parameter.
  // edgeWeightInfluence ?number 1: influence of the edge’s weights on the layout. To consider edge weight, don’t forget to pass weighted as true when applying the synchronous layout or when instantiating the worker.
  // gravity ?number 1: strength of the layout’s gravity.
  // linLogMode ?boolean false: whether to use Noack’s LinLog model.
  // outboundAttractionDistribution ?boolean false
  // scalingRatio ?number 1
  // slowDown ?number 1
  // strongGravityMode ?boolean false
};

/**
 * This is a ConcreteProduct
 */
export class GraphologyForceAtlas2 extends GraphologyLayout {
  constructor() {
    super('Graphology_forceAtlas2');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    const sensibleSettings = forceAtlas2.inferSettings(graph);
    forceAtlas2.assign(graph, {
      ...this.defaultLayoutSettings,
      iterations: 500,
      settings: sensibleSettings,
    });
  }
}

/**
 * This is a ConcreteProduct
 */
export class GraphologyForceAtlas2Webworker extends GraphologyLayout {
  public _layout: FA2Layout | null = null;

  constructor() {
    super('Graphology_forceAtlas2_webworker');
  }

  public cleanup() {
    this._layout?.kill();
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    const sensibleSettings = forceAtlas2.inferSettings(graph);

    let settings = {
      ...this.defaultLayoutSettings,
      ...sensibleSettings,
      adjustSizes: graph.order < 300 ? true : false,
    };

    if (graph.order > 2500) {
      settings = {
        ...settings,
        barnesHutOptimize: true,
        barnesHutTheta: 0.75,
        slowDown: 0.75,
      };
    }

    this._layout = new FA2Layout(graph, { settings });
    this._layout.start();

    // stop the layout after 60 seconds
    setTimeout(() => {
      console.log('Stopping layout after set threshold');
      this._layout?.stop();
    }, 60000);
  }
}
