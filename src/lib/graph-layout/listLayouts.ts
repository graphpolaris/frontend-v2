import Graph from 'graphology';
import { Attributes } from 'graphology-types';
import { Layout } from './layout';
import { ILayoutFactory } from './layoutCreatorUsecase';
import { LayoutAlgorithm } from './types';

export type ListLayoutProvider = 'ListLayout';

export type ListLayoutAlgorithms = 'ListLayout_intersected' | 'ListLayout_nodesfirst' | 'ListLayout_edgesfirst';

export enum ListLayouts {
  LISTINTERSECTED = 'ListLayout_intersected',
  LISTNODEFIRST = 'ListLayout_nodesfirst',
  LISTEDGEFIRST = 'ListLayout_edgesfirst',
}

export class ListLayoutFactory implements ILayoutFactory<ListLayoutAlgorithms> {
  createLayout(layoutAlgorithm: ListLayoutAlgorithms): ListLayout | null {
    switch (layoutAlgorithm) {
      case 'ListLayout_intersected':
        return new ListIntersectedLayout();
      case 'ListLayout_nodesfirst':
        return new ListNodesFirstLayout();
      case 'ListLayout_edgesfirst':
        return new ListEdgesFirstLayout();
      default:
        return null;
    }
  }
}

const Y_OFFSET = 50;
const X_RELATION_OFFSET = 50;
const X_RELATION_OVERLAP_OFFSET = X_RELATION_OFFSET * 0.05;

const RELATION_IDENTIFIER = 'relation';

export abstract class ListLayout extends Layout<ListLayoutProvider> {
  protected defaultLayoutSettings = {
    dimensions: ['x', 'y'],
    center: 0.5,
  };

  constructor(public override algorithm: LayoutAlgorithm<ListLayoutProvider>) {
    super('ListLayout', algorithm);
  }

  /**
   * Retrieves the position of a node in the graph layout.
   * @param nodeId - The ID of the node.
   * @returns The position of the node as an object with `x` and `y` coordinates.
   * @throws Error if the node is not found in the current graph.
   */
  public getNodePosition(nodeId: string) {
    if (this.graph === null) {
      throw new Error('The graph is not set.');
    }

    // console.log('Getting position for node:', nodeId, this.graph.getNodeAttributes(nodeId));
    return this.graph.getNodeAttributes(nodeId);
  }
}

/**
 * This is a ConcreteProduct
 */
export class ListNodesFirstLayout extends ListLayout {
  constructor() {
    super('ListLayout_nodesfirst');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    if (this.verbose) {
      console.log('ListLayout_nodesfirst layouting now', boundingBox);
    }

    if (boundingBox === undefined) {
      boundingBox = { x1: 0, x2: 1000, y1: 0, y2: 1000 };
    }

    const relationNodes = graph.nodes().filter(node => node.toLowerCase().startsWith(RELATION_IDENTIFIER));
    const entityNodes = graph.nodes().filter(node => !relationNodes.includes(node));

    let y = 0;
    entityNodes.map((node, index) => {
      y = index * Y_OFFSET;
      graph.updateNodeAttribute(node, 'x', () => boundingBox.x1);
      graph.updateNodeAttribute(node, 'y', () => y);
    });

    let nodeXIncrement = 0;
    relationNodes.map((node, index) => {
      const relationsY = y + Y_OFFSET + index * Y_OFFSET;
      graph.updateNodeAttribute(node, 'x', () => boundingBox.x1 + nodeXIncrement);
      graph.updateNodeAttribute(node, 'y', () => relationsY);
      nodeXIncrement += X_RELATION_OVERLAP_OFFSET;
    });

    if (this.verbose) {
      console.log(`ListLayout_nodesfirst layouting finished`);
    }
  }
}

/**
 * This is a ConcreteProduct
 */
export class ListEdgesFirstLayout extends ListLayout {
  constructor() {
    super('ListLayout_edgesfirst');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    if (this.verbose) {
      console.log('ListLayout_edgesfirst layouting now', boundingBox);
    }

    if (boundingBox === undefined) {
      boundingBox = { x1: 0, x2: 1000, y1: 0, y2: 1000 };
    }

    const relationNodes = graph.nodes().filter(node => node.toLowerCase().startsWith(RELATION_IDENTIFIER));
    const entityNodes = graph.nodes().filter(node => !relationNodes.includes(node));

    let y = 0;
    let nodeXIncrement = 0;
    relationNodes.map((node, index) => {
      y = index * Y_OFFSET + nodeXIncrement;
      graph.updateNodeAttribute(node, 'x', () => +X_RELATION_OFFSET + nodeXIncrement);
      graph.updateNodeAttribute(node, 'y', () => y);
      nodeXIncrement += X_RELATION_OVERLAP_OFFSET;
    });

    entityNodes.map((node, index) => {
      const relationsY = y + Y_OFFSET + index * Y_OFFSET;
      graph.updateNodeAttribute(node, 'x', () => boundingBox.x1);
      graph.updateNodeAttribute(node, 'y', () => relationsY);
    });

    if (this.verbose) {
      console.log(`ListLayout_edgesfirst layouting finished`);
    }
  }
}

/**
 * This is a ConcreteProduct
 */
export class ListIntersectedLayout extends ListLayout {
  constructor() {
    super('ListLayout_intersected');
  }

  public override async layout(
    graph: Graph<Attributes, Attributes, Attributes>,
    boundingBox?: { x1: number; x2: number; y1: number; y2: number },
  ): Promise<void> {
    super.layout(graph, boundingBox);

    if (this.verbose) {
      console.log('ListLayout_intersected layouting now', boundingBox);
    }

    if (boundingBox === undefined) {
      boundingBox = { x1: 0, x2: 1000, y1: 0, y2: 1000 };
    }

    const relationNodes = graph.nodes().filter(node => node.toLowerCase().startsWith(RELATION_IDENTIFIER));
    const entityNodes = graph.nodes().filter(node => !relationNodes.includes(node));

    const graphAllNodes = graph.nodes();

    const intersectedList: string[] = [];

    entityNodes.forEach(node => {
      intersectedList.push(node);

      graph.forEachNeighbor(node, neighbor => {
        if (graphAllNodes.includes(neighbor)) {
          graphAllNodes.splice(graphAllNodes.indexOf(neighbor), 1);
          intersectedList.push(neighbor);
        }
      });
    });

    let y = 0;
    let nodeXIncrement = 0;
    intersectedList.map((node, index) => {
      y = index * Y_OFFSET;

      graph.updateNodeAttribute(node, 'x', () => {
        nodeXIncrement += X_RELATION_OVERLAP_OFFSET;
        if (node.toLowerCase().startsWith(RELATION_IDENTIFIER)) {
          return boundingBox.x1 + X_RELATION_OFFSET + nodeXIncrement;
        } else {
          return boundingBox.x1 + nodeXIncrement;
        }
      });
      graph.updateNodeAttribute(node, 'y', () => y);
    });

    if (this.verbose) {
      console.log(`ListLayout_intersected layouting finished`);
    }
  }
}
