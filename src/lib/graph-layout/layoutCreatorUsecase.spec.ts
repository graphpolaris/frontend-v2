import { assert, describe, expect, it, test } from 'vitest';
// import 'vitest-canvas-mock';

describe('LayoutFactory Graphology Libraries', () => {
  /**
   * @jest-environment jsdom
   */
  it('should ignore ', () => {
    return true;
  });
});

// // import {
// //   movieSchemaRaw,
// //   northwindSchemaRaw,
// //   simpleSchemaRaw,
// //   twitterSchemaRaw,
// // } from '@/lib/mock-data';
// import Graph, { MultiGraph } from 'graphology';

// import connectedCaveman from 'graphology-generators/community/connected-caveman';
// import ladder from 'graphology-generators/classic/ladder';
// import { LayoutFactory } from './index';

// // import 'vitest-canvas-mock';

// const TIMEOUT = 10;

// /**
//  * @jest-environment jsdom
//  */
// describe('LayoutFactory Graphology Libries', () => {
//   /**
//    * @jest-environment jsdom
//    */
//   it(
//     'should work with noverlap from graphology ',
//     () => {
//       const graph = new MultiGraph();

//       // Adding some nodes
//       // graph.addNode('John', { x: 0, y: 0, width: 200, height: 200 });
//       // graph.addNode('Martha', { x: 0, y: 0 });
//       graph.addNode('John');
//       graph.addNode('Martha');

//       // Adding an edge
//       graph.addEdge('John', 'Martha');

//       const layoutFactory = new LayoutFactory();
//       const layoutAlgorithm = layoutFactory.createLayout('Graphology_noverlap');
//       layoutAlgorithm?.layout(graph);

//       checkNodeAttributesDefined(graph);
//     },
//     TIMEOUT
//   );

//   test(
//     'should work with noverlap from graphology on generated graph',
//     () => {
//       // Creating a ladder graph
//       const graph = ladder(Graph, 10);

//       graph.forEachNode((node, attr) => {
//         graph.setNodeAttribute(node, 'x', 0);
//         graph.setNodeAttribute(node, 'y', 0);
//       });

//       const layoutFactory = new LayoutFactory();

//       const layout = layoutFactory.createLayout('Graphology_noverlap');
//       layout?.layout(graph);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);

//       // const positionMap = new Set<string>();
//       // graph.forEachNode((node, attr) => {
//       //   const pos = '' + attr['x'] + '' + attr['y'];

//       //   expect(positionMap.has(pos)).toBeFalsy();
//       //   positionMap.add(pos);
//       // });
//     },
//     TIMEOUT
//   );

//   test(
//     'should work with random from graphology on generated graph',
//     () => {
//       // Creating a ladder graph
//       const graph = ladder(Graph, 10);

//       graph.forEachNode((node, attr) => {
//         graph.setNodeAttribute(node, 'x', 0);
//         graph.setNodeAttribute(node, 'y', 0);
//       });

//       const layoutFactory = new LayoutFactory();

//       const layout = layoutFactory.createLayout('Graphology_random');
//       layout?.setDimensions(100, 100);
//       layout?.layout(graph);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);

//       // const positionMap = new Set<string>();
//       // graph.forEachNode((node, attr) => {
//       //   const pos = '' + attr['x'] + '' + attr['y'];

//       //   expect(positionMap.has(pos)).toBeFalsy();
//       //   positionMap.add(pos);
//       // });
//     },
//     TIMEOUT
//   );

//   test(
//     'should work with circular from graphology on generated graph',
//     () => {
//       // Creating a ladder graph
//       const graph = ladder(Graph, 100);

//       graph.forEachNode((node) => {
//         graph.setNodeAttribute(node, 'x', 0);
//         graph.setNodeAttribute(node, 'y', 0);
//       });

//       const layoutFactory = new LayoutFactory();

//       const layout = layoutFactory.createLayout('Graphology_circular');
//       layout?.setDimensions(100, 100);
//       layout?.layout(graph);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);

//       // const positionMap = new Set<string>();
//       // graph.forEachNode((node, attr) => {
//       //   const pos = '' + attr['x'] + '' + attr['y'];

//       //   expect(positionMap.has(pos)).toBeFalsy();
//       //   positionMap.add(pos);
//       // });
//     },
//     TIMEOUT
//   );

//   test(
//     'should work with Graphology_forceAtlas2 from graphology on generated graph',
//     () => {
//       // console.log(Object.keys(AllLayoutAlgorithms))

//       const graph = connectedCaveman(Graph, 6, 8);

//       graph.forEachNode((node, attr) => {
//         expect(graph.getNodeAttribute(node, 'x')).toBeUndefined();
//         expect(graph.getNodeAttribute(node, 'y')).toBeUndefined();
//       });

//       // console.log('before');
//       const layoutFactory = new LayoutFactory();
//       const layout = layoutFactory.createLayout('Graphology_forceAtlas2');
//       layout?.setDimensions(100, 100);
//       layout?.layout(graph);

//       // console.log('after');

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);

//       // const positionMap = new Set<string>();
//       // graph.forEachNode((node, attr) => {
//       //   const pos = '' + attr['x'] + '' + attr['y'];
//       //   // console.log(pos);

//       //   expect(positionMap.has(pos)).toBeFalsy();
//       //   positionMap.add(pos);
//       // });
//     },
//     TIMEOUT
//   );
// });

// describe('LayoutFactory Cytoscape Libraries', () => {
//   it('should convert between models Graphology <-> Cytoscape ', () => {
//     const graph = connectedCaveman(Graph, 10, 10);

//     const layoutFactory = new LayoutFactory();
//     const layoutAlgorithm = layoutFactory.createLayout('Cytoscape_klay');

//     const cytonodes = layoutAlgorithm?.convertToCytoscapeModel(graph);

//     let nodes = 0;
//     let edges = 0;
//     cytonodes?.forEach((element) => {
//       if (element.data.type == 'node') {
//         nodes++;
//       } else if (element.data.type == 'edge') {
//         edges++;
//       }
//     });

//     // console.log('Number of nodes', graph.order);
//     // console.log('Number of edges', graph.size);
//     try {
//       expect(nodes).toBe(graph.order);
//     } catch (error) {
//       console.error(
//         'expect(nodes).toBe(graph.order) but was',
//         nodes,
//         graph.order
//       );
//     }
//     expect(edges).toBe(graph.size);
//   });

//   it(
//     'should execute Cytoscape_klay from Cytoscape ',
//     () => {
//       /**
//        * l number: number of components in the graph.
//        * k number: number of nodes of the components.
//        */
//       const graph = connectedCaveman(Graph, 5, 5);
//       graph.addNode('Martha', { occurrences: 1 });

//       // graph.forEachNode(node => {
//       //   console.log(node);
//       // });

//       const layoutFactory = new LayoutFactory();
//       const layoutAlgorithm = layoutFactory.createLayout('Cytoscape_klay');
//       layoutAlgorithm?.layout(graph);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);

//       // const positionMap = new Set<string>();
//       // graph.forEachNode((node, attr) => {
//       //   const pos = '' + attr['x'] + '' + attr['y'];
//       //   expect(positionMap.has(pos)).toBeFalsy();
//       //   positionMap.add(pos);
//       // });
//     },
//     TIMEOUT
//   );

//   it(
//     'should execute Cytoscape_elk from Cytoscape ',
//     () => {
//       /**
//        * l number: number of components in the graph.
//        * k number: number of nodes of the components.
//        */
//       const graph = connectedCaveman(Graph, 5, 5);
//       graph.addNode('Martha', { occurrences: 1 });

//       // graph.forEachNode(node => {
//       //   console.log(node);
//       // });

//       const layoutFactory = new LayoutFactory();
//       const layoutAlgorithm = layoutFactory.createLayout('Cytoscape_elk');
//       layoutAlgorithm?.layout(graph, false);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);
//       // const positionMap = new Set<string>();
//       // graph.forEachNode((node, attr) => {
//       //   const pos = '' + attr['x'] + '' + attr['y'];
//       //   expect(positionMap.has(pos)).toBeFalsy();
//       //   positionMap.add(pos);
//       // });
//     },
//     TIMEOUT
//   );

//   it(
//     'should execute Cytoscape_dagre from Cytoscape ',
//     () => {
//       /**
//        * l number: number of components in the graph.
//        * k number: number of nodes of the components.
//        */
//       const graph = connectedCaveman(Graph, 5, 5);
//       graph.addNode('Martha', { occurrences: 1 });

//       // graph.forEachNode(node => {
//       //   console.log(node);
//       // });

//       const layoutFactory = new LayoutFactory();
//       const layoutAlgorithm = layoutFactory.createLayout('Cytoscape_dagre');
//       layoutAlgorithm?.layout(graph);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);
//     },
//     TIMEOUT
//   );

//   it(
//     'should execute Cytoscape_fcose from Cytoscape ',
//     () => {
//       /**
//        * l number: number of components in the graph.
//        * k number: number of nodes of the components.
//        */
//       const graph = connectedCaveman(Graph, 5, 5);
//       graph.addNode('Martha', { occurrences: 1 });

//       // graph.forEachNode(node => {
//       //   console.log(node);
//       // });

//       const layoutFactory = new LayoutFactory();
//       const layoutAlgorithm = layoutFactory.createLayout('Cytoscape_fcose');
//       layoutAlgorithm?.layout(graph, false);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);
//     },
//     TIMEOUT
//   );

//   it(
//     'should execute Cytoscape_cose-bilkent from Cytoscape ',
//     () => {
//       /**
//        * l number: number of components in the graph.
//        * k number: number of nodes of the components.
//        */
//       const graph = connectedCaveman(Graph, 5, 5);
//       graph.addNode('Martha', { occurrences: 1 });

//       // graph.forEachNode(node => {
//       //   console.log(node);
//       // });

//       const layoutFactory = new LayoutFactory();
//       const layoutAlgorithm = layoutFactory.createLayout(
//         'Cytoscape_cose-bilkent'
//       );
//       layoutAlgorithm?.layout(graph);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);
//     },
//     TIMEOUT
//   );

//   it(
//     'should execute Cytoscape_cise from Cytoscape ',
//     () => {
//       /**
//        * l number: number of components in the graph.
//        * k number: number of nodes of the components.
//        */
//       const graph = connectedCaveman(Graph, 5, 5);
//       graph.addNode('Martha', { occurrences: 1 });

//       // graph.forEachNode(node => {
//       //   console.log(node);
//       // });

//       const layoutFactory = new LayoutFactory();
//       const layoutAlgorithm = layoutFactory.createLayout('Cytoscape_cise');
//       layoutAlgorithm?.layout(graph);

//       checkNodeAttributesDefined(graph);
//       checkPositionOverlap(graph);
//     },
//     TIMEOUT
//   );
// });

// function checkNodeAttributesDefined(graph: Graph) {
//   try {
//     graph.forEachNode((node, attr) => {
//       expect(graph.getNodeAttribute(node, 'x')).toBeDefined();
//     });
//   } catch (error) {
//     console.error('Node attribute x is not defined');
//   }

//   try {
//     graph.forEachNode((node, attr) => {
//       expect(graph.getNodeAttribute(node, 'y')).toBeDefined();
//     });
//   } catch (error) {
//     console.error('Node attribute y is not defined');
//   }
// }

// function checkPositionOverlap(graph: Graph) {
//   // try {
//   //   const positionMap = new Set<string>();
//   //   graph.forEachNode((node, attr) => {
//   //     const pos = '' + attr['x'] + '' + attr['y'];
//   //     expect(positionMap.has(pos)).toBeFalsy();
//   //     positionMap.add(pos);
//   //   });
//   // } catch (error) {
//   //   console.error('A node position overlap was found');
//   // }

//   try {
//     const positionMap = new Set<string>();
//     graph.forEachNode((node, attr) => {
//       expect(isNaN(graph.getNodeAttribute(node, 'x'))).toBeFalsy();
//       expect(isNaN(graph.getNodeAttribute(node, 'y'))).toBeFalsy();
//     });
//   } catch (error) {
//     console.error('A node position may not be NaN');
//   }
// }
