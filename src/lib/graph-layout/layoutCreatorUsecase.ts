import { CytoscapeFactory, CytoscapeLayoutAlgorithms } from './cytoscapeLayouts';
import { DagreFactory } from './dagreLayout';
import { GraphologyFactory, GraphologyLayoutAlgorithms } from './graphologyLayouts';
import { ListLayoutAlgorithms, ListLayoutFactory } from './listLayouts';
import { AlgorithmToLayoutProvider, AllLayoutAlgorithms } from './types';

export interface ILayoutFactory<Algorithm extends AllLayoutAlgorithms> {
  createLayout: (Algorithm: Algorithm) => AlgorithmToLayoutProvider<Algorithm> | null;
}

/**
 * This is our Creator
 */
export class LayoutFactory implements ILayoutFactory<AllLayoutAlgorithms> {
  private graphologyFactory = new GraphologyFactory();
  private cytoscapeFactory = new CytoscapeFactory();
  private listlayoutFactory = new ListLayoutFactory();
  private dagreFactory = new DagreFactory();

  private isSpecificAlgorithm<Algorithm extends AllLayoutAlgorithms>(
    LayoutAlgorithm: AllLayoutAlgorithms,
    startsWith: string,
  ): LayoutAlgorithm is Algorithm {
    return LayoutAlgorithm.startsWith(startsWith);
  }

  // todo make this static
  createLayout<Algorithm extends AllLayoutAlgorithms = AllLayoutAlgorithms>(
    layoutAlgorithm: Algorithm,
  ): AlgorithmToLayoutProvider<Algorithm> {
    if (this.isSpecificAlgorithm<GraphologyLayoutAlgorithms>(layoutAlgorithm, 'Graphology')) {
      return this.graphologyFactory.createLayout(layoutAlgorithm) as AlgorithmToLayoutProvider<Algorithm>;
    }

    if (this.isSpecificAlgorithm<CytoscapeLayoutAlgorithms>(layoutAlgorithm, 'Cytoscape')) {
      return this.cytoscapeFactory.createLayout(layoutAlgorithm) as AlgorithmToLayoutProvider<Algorithm>;
    }

    if (this.isSpecificAlgorithm<CytoscapeLayoutAlgorithms>(layoutAlgorithm, 'Dagre')) {
      return this.dagreFactory.createLayout() as unknown as AlgorithmToLayoutProvider<Algorithm>;
    }

    if (this.isSpecificAlgorithm<ListLayoutAlgorithms>(layoutAlgorithm, 'List')) {
      return this.listlayoutFactory.createLayout(layoutAlgorithm) as AlgorithmToLayoutProvider<Algorithm>;
    }

    throw Error('Invalid layout algorithm ' + layoutAlgorithm);
  }
}
