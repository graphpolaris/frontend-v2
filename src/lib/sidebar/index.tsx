import React from 'react';
import { Button, Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from '../components';
import ColorMode from '../components/color-mode';
import { FeatureEnabled } from '@/lib/components/featureFlags';

export type SideNavTab = 'Schema' | 'Search' | undefined;

const tabs: Array<{
  name: SideNavTab;
  icon: string | undefined;
  className?: string;
}> = [
  {
    name: 'Search',
    icon: 'icon-[ic--outline-search]',
    className: 'searchbar',
  },
  {
    name: 'Schema',
    icon: 'icon-[ic--baseline-schema]',
  },
];

export function Sidebar({
  onTab,
  tab,
  openMonitoringDialog,
}: {
  onTab: (tab: SideNavTab) => void;
  tab: SideNavTab;
  openMonitoringDialog: () => void;
}) {
  return (
    <div className="side-bar w-fit h-full flex shrink">
      <TooltipProvider>
        <div className="w-11 flex flex-col items-center">
          {tabs.map(t => (
            <Tooltip key={t.name} placement={'right'}>
              <TooltipTrigger asChild>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="lg"
                  iconComponent={t.icon}
                  onClick={() => {
                    if (tab === t.name) {
                      onTab(undefined);
                    } else {
                      onTab(t.name);
                    }
                  }}
                  className={`${t.className || ''} ${tab === t.name ? 'bg-secondary-100' : ''}`}
                />
              </TooltipTrigger>
              <TooltipContent>{t.name}</TooltipContent>
            </Tooltip>
          ))}

          <FeatureEnabled featureFlag="INSIGHT_SHARING">
            <Tooltip placement={'right'}>
              <TooltipTrigger asChild>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="lg"
                  iconComponent="icon-[ic--outline-analytics]"
                  onClick={openMonitoringDialog}
                />
              </TooltipTrigger>
              <TooltipContent>Insight Sharing</TooltipContent>
            </Tooltip>
          </FeatureEnabled>

          <div className="mt-auto mb-2">
            <ColorMode />
          </div>
        </div>
      </TooltipProvider>
    </div>
  );
}
