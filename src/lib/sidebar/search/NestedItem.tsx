import React from 'react';

type NestedItemProps = {
  keyName: string;
  value: Record<string, any>;
};

export const NestedItem = ({ keyName, value }: NestedItemProps) => {
  const numberOfMaxElementsShow = 9;

  return (
    <div>
      <span className="font-bold">{keyName}:</span>
      {Object.keys(value).map((keyInside, indexInside) => (
        <div key={`tooltipItem_${keyName}_${keyInside}`} className="mx-2">
          <span className="font-bold">{keyInside}:</span>
          {typeof value[keyInside] === 'object' ? (
            <div>
              {Object.keys(value[keyInside])
                .slice(0, numberOfMaxElementsShow)
                .map(keyInside2 => (
                  <div key={`tooltipItem_${keyName}_${keyInside}_${keyInside2}`} className="mx-2">
                    <span className="font-bold">{keyInside2}:</span>
                    <span className="truncate"> {JSON.stringify(value[keyInside][keyInside2])}</span>
                  </div>
                ))}
            </div>
          ) : (
            <span> {value[keyInside]}</span>
          )}
        </div>
      ))}
    </div>
  );
};
