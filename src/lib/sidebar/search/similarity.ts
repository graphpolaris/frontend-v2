export const filterData = (query: string, data: Record<string, any>[], threshold: number): Record<string, any>[] => {
  return data
    .map(object => {
      const similarity = matches(query, object);
      return { ...object, similarity };
    })
    .filter(object => object.similarity >= threshold);
};

const matches = (query: string, object: Record<string, any>): number => {
  let highestScore = 0;

  for (const key in object) {
    if (Object.prototype.hasOwnProperty.call(object, key)) {
      const attributeValue = object[key];
      if (typeof attributeValue === 'object') {
        const subScore = matches(query, attributeValue);
        if (subScore > highestScore) {
          highestScore = subScore;
        }
      } else if (typeof attributeValue === 'string') {
        const value = attributeValue.toString().toLowerCase();
        const similarity = jaroSimilarity(query, value);
        if (similarity > highestScore) {
          highestScore = similarity;
        }
      }
    }
  }
  return highestScore;
};

export const jaroSimilarity = (s1: string, s2: string) => {
  if (s1 == s2) return 1.0;

  const len1 = s1.length;
  const len2 = s2.length;
  const max_dist = Math.floor(Math.max(len1, len2) / 2) - 1;

  let match = 0;

  const hash_s1 = Array(s1.length).fill(0);
  const hash_s2 = Array(s1.length).fill(0);

  for (let i = 0; i < len1; i++) {
    for (let j = Math.max(0, i - max_dist); j < Math.min(len2, i + max_dist + 1); j++)
      if (s1[i] == s2[j] && hash_s2[j] == 0) {
        hash_s1[i] = 1;
        hash_s2[j] = 1;
        match++;
        break;
      }
  }

  if (match == 0) return 0.0;

  let t = 0;
  let point = 0;

  for (let i = 0; i < len1; i++)
    if (hash_s1[i]) {
      while (hash_s2[point] == 0) point++;

      if (s1[i] != s2[point++]) t++;
    }

  t /= 2;

  return (match / len1 + match / len2 + (match - t) / match) / 3.0;
};
