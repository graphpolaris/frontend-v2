import React, { useState, useRef, useEffect, useMemo } from 'react';
import { Handle, Position, NodeProps, useViewport } from 'reactflow';
import { SchemaReactflowRelationWithFunctions } from '../../../model/reactflow';
import { SchemaEdge, QueryElementTypes } from 'ts-common';
import { RelationPill } from '@/lib/components';
import { Popover, PopoverContent, PopoverTrigger } from '@/lib/components/popover';
import { NodeDetails } from '@/lib/components/nodeDetails';
import { SchemaPopUp } from '../SchemaPopUp/SchemaPopUp';
import { useSchemaStats } from '@/lib/data-access';

export const SchemaRelationPill = React.memo(({ id, selected, data, ...props }: NodeProps<SchemaReactflowRelationWithFunctions>) => {
  const [openPopupLocation, setOpenPopupLocation] = useState<{ x: number; y: number } | null>(null);

  const viewport = useViewport();
  const schemaStats = useSchemaStats();

  const ref = useRef<HTMLDivElement>(null);
  /**
   * Adds drag functionality in order to be able to drag the relationNode to the schema.
   * @param event React Mouse drag event.
   */
  const onDragStart = (event: React.DragEvent<HTMLDivElement>) => {
    const eventData: SchemaEdge = {
      type: QueryElementTypes.Relation,
      name: id, //TODO id?
      from: data.from,
      to: data.to,
      collection: data.collection,
      label: data.label,
      attributes: Array.from(data.attributes)
        .map(attribute => attribute.attributes)
        .flat(),
    };
    event.dataTransfer.setData('application/reactflow', JSON.stringify(eventData));
    event.dataTransfer.effectAllowed = 'move';
  };

  useEffect(() => {
    if (data.popoverClose === true) {
      setOpenPopupLocation(null);
    }
  }, [data.popoverClose]);

  const popoverX = useMemo(() => {
    if (ref.current == null || openPopupLocation == null) return -1;
    const rect = ref.current.getBoundingClientRect();
    return rect.x - openPopupLocation.x + rect.width / 2;
  }, [viewport.x, openPopupLocation]);

  const popoverY = useMemo(() => {
    if (ref.current == null || openPopupLocation == null) return -1;
    const rect = ref.current.getBoundingClientRect();
    return rect.y - openPopupLocation.y + rect.height / 2;
  }, [viewport.y, openPopupLocation]);

  return (
    <>
      <div
        className="w-fit h-fit"
        onDragStart={event => onDragStart(event)}
        onDragStartCapture={event => onDragStart(event)}
        onMouseDownCapture={event => {
          if (!event.shiftKey) event.stopPropagation();
        }}
        onClickCapture={event => {
          if (openPopupLocation != null || ref.current == null) {
            return;
          }

          setOpenPopupLocation(ref.current.getBoundingClientRect());
        }}
        draggable
        ref={ref}
      >
        {openPopupLocation !== null && (
          <Popover key={data.name} open={true} boundaryElement={data.reactFlowRef} showArrow={true}>
            <PopoverTrigger x={popoverX} y={popoverY} />
            <PopoverContent>
              <NodeDetails name={data.collection} colorHeader={'hsl(var(--clr-relation))'}>
                <SchemaPopUp
                  data={
                    data.attributes.length > 0
                      ? data.attributes.reduce(
                          (acc, attr) => {
                            if (attr.name && attr.type) {
                              acc[attr.name] = attr.type;
                            }
                            return acc;
                          },
                          {} as Record<string, string>,
                        )
                      : {}
                  }
                  connections={{ from: data.from, to: data.to }}
                  numberOfElements={schemaStats.edges.stats[data.collection]?.count}
                />
              </NodeDetails>
            </PopoverContent>
          </Popover>
        )}
        <RelationPill
          draggable
          title={data.collection}
          withHandles="vertical"
          handleUp={
            <Handle
              style={{ pointerEvents: 'none' }}
              id="entitySourceLeft"
              position={Position.Top}
              className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !b-0 !top-0 !bottom-0'}
              type="target"
            ></Handle>
          }
          handleDown={
            <Handle
              style={{ pointerEvents: 'none' }}
              id="entitySourceRight"
              position={Position.Bottom}
              className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !top-0 !bottom-0'}
              type="source"
            ></Handle>
          }
        />
      </div>
    </>
  );
});

SchemaRelationPill.displayName = 'RelationNode';
