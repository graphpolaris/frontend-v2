/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import { FormBody, FormCard, FormControl, FormHBar, FormTitle } from '@/lib/components/forms';
import { SchemaReactflowRelation } from '@/lib/schema/model';
import { FormEvent } from 'react';

export type SchemaRelationshipPopupProps = {
  data: SchemaReactflowRelation;
  onClose: () => void;
};

/**
 * NodeQualityEntityPopupNode is the node that represents the popup that shows the node quality for an entity
 * @param data Input data of type NodeQualityDataForEntities, which is for the popup.
 */
export const SchemaRelationshipPopup = (props: SchemaRelationshipPopupProps) => {
  function submit() {
    // dispatch(setSchemaSettings(state));
  }

  return (
    <>
      <FormCard>
        <FormBody
          onSubmit={(e: FormEvent<HTMLFormElement>) => {
            e.preventDefault();
            submit();
          }}
        >
          <FormTitle title="Edge Statistics" onClose={props.onClose} />
          <FormHBar />

          <span className="px-5">
            <span>Name</span>
            <span className="float-right break-all text-wrap text-pretty font-light font-data">{props.data.collection}</span>
          </span>
          <span className="px-5">
            <span>From</span>
            <span className="float-right break-all text-wrap text-pretty font-light font-data">{props.data.from}</span>
          </span>
          <span className="px-5">
            <span>To</span>
            <span className="float-right break-all text-wrap text-pretty font-light font-data">{props.data.to}</span>
          </span>

          <FormHBar />

          <span className="px-5 pt-2">
            <span>Attributes</span>
            <span className="float-right font-light font-data">{Object.keys(props.data.attributes).length}</span>
          </span>

          {Object.values(props.data.attributes).map((attribute: any) => {
            return (
              <div key={attribute.name} className="px-5">
                <span>{attribute.name}</span>
                <span className="float-right font-light font-data">{attribute.type}</span>
              </div>
            );
          })}
          <FormHBar />

          <FormControl>
            <button
              className="btn btn-outline btn-accent border-0 btn-sm p-0 m-0 text-[0.8rem] mb-2 mx-2.5 min-h-0 h-5"
              onClick={() => {
                submit();
              }}
            >
              Close
            </button>
          </FormControl>
        </FormBody>
      </FormCard>
    </>
  );
};
