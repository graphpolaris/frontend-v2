import { Meta, StoryObj } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';

import { ReactFlowProvider } from 'reactflow';
import { AttributeAnalyticsPopupMenu } from './attribute-analytics-popup-menu';
import { AttributeCategory, NodeType } from '../../../model/reactflow';

const Component: Meta<typeof AttributeAnalyticsPopupMenu> = {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: 'Schema/Pills/Popups/AttributeAnalyticsPopupMenu',
  component: AttributeAnalyticsPopupMenu,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <ReactFlowProvider>{story()}</ReactFlowProvider>
      </Provider>
    ),
  ],
};

export default Component;

// A super-simple mock of a redux store
const Mockstore = configureStore({
  reducer: {
    // schema: schemaSlice.reducer,
  },
});

type Story = StoryObj<typeof AttributeAnalyticsPopupMenu>;
export const Default: Story = {
  args: {
    data: {
      nodeID: '1',
      nodeType: NodeType.entity,
      attributes: [
        {
          category: AttributeCategory.categorical,
          nullAmount: 0,
          attribute: {
            name: 'Foo',
            type: 'string',
            attributes: [],
          },
        },
      ],
      isAttributeDataIn: false,
      onClickCloseButton: () => {},
      onClickPlaceInQueryBuilderButton: (name: string, type) => {},
      searchForAttributes: (id: string, searchbarValue: string) => {},
      resetAttributeFilters: (id: string) => {},
      applyAttributeFilters: (id: string, category: AttributeCategory, predicate: string, percentage: number) => {},
    },
  },
};
