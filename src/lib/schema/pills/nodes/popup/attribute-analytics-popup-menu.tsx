/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React from 'react';
import { AttributeAnalyticsData } from '../../../model/reactflow';
import { NodeProps } from 'reactflow';
import './attribute-analytics-popup-menu.module.scss';

/**
 * AttributeAnalyticsPopupMenuNode is the node that represents the popup menu that shows the attribute analytics for an entity or relation
 * @param data Input data of type AttributeAnalyticsData, which is for the popup menu.
 */
export const AttributeAnalyticsPopupMenu = ({ data }: NodeProps<AttributeAnalyticsData>) => {
  return <div></div>;
  // const theme = useTheme();
  // if (data == undefined) throw new Error('No Attribute Analytics data is available for the node.');
  // let entityOrRelationBase: [string, string, string];
  // if (data.nodeType == NodeType.entity) entityOrRelationBase = theme.palette.custom.elements.entityBase;
  // else entityOrRelationBase = theme.palette.custom.elements.relationBase;
  // let attributesDivs: any[] = [];
  // if (data.isAttributeDataIn) {
  //   data.attributes.forEach((attributeItem: any) => {
  //     attributesDivs.push(
  //       <Accordion className="attributesAccordion">
  //         <AccordionSummary className="attribute" expandIcon={<ExpandMore className="expandIcon" />}>
  //           {attributeItem.attribute.name}
  //         </AccordionSummary>
  //         <AccordionDetails className="accordionDetails">{attributeItem.category}</AccordionDetails>
  //         <AccordionDetails className="accordionDetails">
  //           <span>Null values:</span>
  //           <span
  //             className="nullAmountValue"
  //             style={{
  //               backgroundColor: entityOrRelationBase[0],
  //             }}
  //           >
  //             {attributeItem.nullAmount}%
  //           </span>
  //         </AccordionDetails>
  //         <AccordionDetails className="accordionDetails">
  //           <div className="attributeButtons">
  //             <span>See visualization</span>
  //             <span className="rightSideValue">
  //               <Visibility className="visualisationEye" />
  //             </span>
  //           </div>
  //         </AccordionDetails>
  //         <AccordionDetails className="accordionDetails">
  //           <div
  //             className="attributeButtons"
  //             onClick={() => data.onClickPlaceInQueryBuilderButton(attributeItem.attribute.name, attributeItem.attribute.type)}
  //           >
  //             <span>Place in query builder</span>
  //           </div>
  //         </AccordionDetails>
  //       </Accordion>
  //     );
  //   });
  // } else {
  //   data.attributes.forEach((attributeItem: AttributeWithData) => {
  //     attributesDivs.push(
  //       <Accordion className="attributesAccordion">
  //         <AccordionSummary className="attribute" expandIcon={<ExpandMore className="expandIcon" />}>
  //           {attributeItem.attribute.name}
  //         </AccordionSummary>
  //         <AccordionDetails className="accordionDetails">
  //           <div
  //             className="attributeButtons"
  //             onClick={() => data.onClickPlaceInQueryBuilderButton(attributeItem.attribute.name, attributeItem.attribute.type)}
  //           >
  //             <span>Place in query builder</span>
  //           </div>
  //         </AccordionDetails>
  //       </Accordion>
  //     );
  //   });
  // }
  // console.log(data.attributes);
  // return (
  //   <div>
  //     <div className="title">
  //       <span id="name">Attributes</span>
  //       <span className="rightSideValue">{data.attributes.length}</span>
  //     </div>
  //     <div className="bar">search bar</div>
  //     <div className="bar">filter bar</div>
  //     <div className="attributesWrapper">{attributesDivs}</div>
  //     <div className="closeButtonWrapper">
  //       <ButtonBase
  //         onClick={() => data.onClickCloseButton()}
  //         id="closeButton"
  //         style={{
  //           backgroundColor: entityOrRelationBase[0],
  //         }}
  //       >
  //         Close
  //       </ButtonBase>
  //     </div>
  //   </div>
  // );
};
