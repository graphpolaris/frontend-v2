/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import React, { useState } from 'react';

/** The typing for the props of the searchbar. */
type SearchbarProps = {
  searchForAttributes(value: string): void;
};

/** The variables in the state of the searchbar. */
type SearchbarState = {
  searchbarValue: string;
};

/** React component that renders a searchbar with input fields for choosing and changing the filters. */
export const Search = (props: SearchbarProps) => {
  const [state, setState] = useState<SearchbarState>({ searchbarValue: '' });

  function searchbarValueChanged(event: React.ChangeEvent<HTMLInputElement>) {
    const newValue = event.target.value;

    setState({ searchbarValue: newValue });

    props.searchForAttributes(newValue);
  }

  return (
    <input
      className="bar"
      type="text"
      name="searchValue"
      placeholder="Searchbar"
      onChange={searchbarValueChanged}
      value={state.searchbarValue}
    />
  );
};
