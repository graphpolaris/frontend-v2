/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/

import React, { useState } from 'react';
import { AttributeAnalyticsData, AttributeCategory } from '../../../../model/reactflow';

/** The typing for the props of the filter bar. */
type FilterbarProps = {
  data: AttributeAnalyticsData;
  resetAttributeFilters(): void;
  applyAttributeFilters(dataType: AttributeCategory, predicate: string, percentage: number): void;
};

/** The variables in the state of the filter bar. */
type FilterbarState = {
  dataTypeValue: string;
  nullValuesPredicate: string;
  nullValuesValue: string;
  onHoverApplyButton: boolean;
  onHoverResetButton: boolean;
};

/** React component that renders a filter bar with input fields for choosing and changing the filters. */
export const Filter = (props: FilterbarProps) => {
  const [state, setState] = useState({
    dataTypeValue: '',
    nullValuesPredicate: '',
    nullValuesValue: '',
    onHoverApplyButton: false,
    onHoverResetButton: false,
  });

  /**
   * Processes the chosen data-type in the new state.
   * @param event The event that contains the value of the chosen data-type.
   */
  const dataTypeChanged = (event: any) => {
    const newValue = event.target.value;
    setState({ ...state, dataTypeValue: newValue });
  };

  /**
   * Processes the chosen predicate in the new state.
   * @param event The event that contains the value of the chosen predicate.
   */
  const nullValuesPredicateChanged = (event: any) => {
    const newValue = event.target.value;

    setState({ ...state, nullValuesPredicate: newValue });
  };

  /**
   * Processes the chosen null-values-amount in the new state.
   * @param event The event that contains the value of the chosen null-values-amount.
   */
  const nullValuesValueChanged = (event: any) => {
    const value = event.target.validity.valid ? event.target.value : state.nullValuesValue;
    let newValue = value;
    if (Number(newValue) < 0) newValue = '0';
    if (Number(newValue) > 100) newValue = '100';

    setState({ ...state, nullValuesValue: newValue });
  };
  /**
   * This resets the current filters and let the popup-menu show the original list of attributes again.
   */
  const resetFilters = () => {
    setState({
      ...state,
      dataTypeValue: '',
      nullValuesPredicate: '',
      nullValuesValue: '',
    });
    props.resetAttributeFilters();
  };

  /**
   * This applies the chosen filters and let the popup-menu show only the attributes that satifsy the chosen filters.
   */
  const applyFilters = () => {
    // Check if the chosen null-amount-value is valid.
    if (!(state.nullValuesValue == '') && Number.isNaN(Number(state.nullValuesValue))) {
      setState({
        ...state,
      });
      return;
    }

    // Sets the data-type.
    let dataType;
    if (state.dataTypeValue == 'Categorical') dataType = AttributeCategory.categorical;
    else if (state.dataTypeValue == 'Numerical') dataType = AttributeCategory.numerical;
    else if (state.dataTypeValue == 'Other') dataType = AttributeCategory.other;
    else dataType = AttributeCategory.undefined;

    // Sets the percentage of the null-amount-values.
    let percentage;
    if (state.nullValuesValue == '') percentage = -1;
    else percentage = Number(state.nullValuesValue);

    props.applyAttributeFilters(dataType, state.nullValuesPredicate, percentage);
  };

  // Makes sure that the hovered button get the right hover-state.
  const toggleHoverApply = () => {
    const currentHover = state.onHoverApplyButton;
    setState({
      ...state,
      onHoverApplyButton: !currentHover,
      onHoverResetButton: false,
    });
  };

  // Resets all buttons to the original hover-state.
  const toggleHoverReset = () => {
    const currentHover = state.onHoverResetButton;
    setState({
      ...state,
      onHoverApplyButton: false,
      onHoverResetButton: !currentHover,
    });
  };

  let entityOrRelationBase: [string, string, string];
  // if (props.data.nodeType == NodeType.entity) entityOrRelationBase = theme.palette.custom.elements.entityBase;
  // else entityOrRelationBase = theme.palette.custom.elements.relationBase;

  // let applyButtonColor;
  // if (state.onHoverApplyButton) applyButtonColor = '#' + entityOrRelationBase[0];
  // else applyButtonColor = 'inherit';

  // let resetButtonColor;
  // if (state.onHoverResetButton) resetButtonColor = '#' + entityOrRelationBase[0];
  // else resetButtonColor = 'inherit';

  return (
    <div />
    // <div className="bar">
    //   <Accordion className={['attributesAccordion', 'filterbarAccordion'].join(' ')}>
    //     <AccordionSummary className="filterbarSummary" expandIcon={<ExpandMore className="expandIcon" />}>
    //       Filter bar
    //     </AccordionSummary>
    //     <AccordionDetails className={['accordionDetails', 'accordionDetailsFilterbar'].join(' ')}>
    //       <span>Datatype:</span>
    //       <span>
    //         <select
    //           className={['nullAmountValue', 'dataTypeSelect'].join(' ')}
    //           style={{
    //             backgroundColor: entityOrRelationBase[0],
    //           }}
    //           value={state.dataTypeValue}
    //           onChange={dataTypeChanged}
    //         >
    //           <option value=""></option>
    //           <option value="Categorical">Categorical</option>
    //           <option value="Numerical">Numerical</option>
    //           <option value="Other">Other</option>
    //         </select>
    //       </span>
    //     </AccordionDetails>
    //     <AccordionDetails className={['accordionDetails', 'accordionDetailsFilterbar'].join(' ')}>
    //       <span>Null-values:</span>
    //       <span
    //         className={['nullAmountValue', 'nullValuesBox'].join(' ')}
    //         style={{
    //           backgroundColor: entityOrRelationBase[0],
    //         }}
    //       >
    //         <input
    //           className="nullValuesSelect"
    //           style={{
    //             backgroundColor: entityOrRelationBase[0],
    //           }}
    //           value={state.nullValuesValue}
    //           onChange={nullValuesValueChanged}
    //           type="text"
    //           pattern="[0-9]*"
    //         ></input>
    //         %
    //       </span>
    //       <span>
    //         <select
    //           className={['nullAmountValue', 'dataTypeSelect', 'predicateSelect'].join(' ')}
    //           style={{
    //             backgroundColor: entityOrRelationBase[0],
    //           }}
    //           value={state.nullValuesPredicate}
    //           onChange={nullValuesPredicateChanged}
    //         >
    //           <option value=""></option>
    //           <option value="Equal">==</option>
    //           <option value="NotEqual">!=</option>
    //           <option value="Smaller">&#60;</option>
    //           <option value="SmallerOrEqual">&#8804;</option>
    //           <option value="Bigger">&#62;</option>
    //           <option value="BiggerOrEqual">&#8805;</option>
    //         </select>
    //       </span>

    //       <ButtonBase
    //         className="attributeButtons"
    //         onClick={() => resetFilters()}
    //         onMouseEnter={toggleHoverReset}
    //         onMouseLeave={toggleHoverReset}
    //         id="resetFiltersButton"
    //         style={{
    //           backgroundColor: resetButtonColor,
    //         }}
    //       >
    //         Reset
    //       </ButtonBase>
    //       <ButtonBase
    //         className="attributeButtons"
    //         onClick={() => applyFilters()}
    //         onMouseEnter={toggleHoverApply}
    //         onMouseLeave={toggleHoverApply}
    //         id="applyFiltersButton"
    //         style={{
    //           backgroundColor: applyButtonColor,
    //         }}
    //       >
    //         Apply
    //       </ButtonBase>
    //     </AccordionDetails>
    //   </Accordion>
    // </div>
  );
};
