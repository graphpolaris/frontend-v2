import React from 'react';
import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { Search } from './searchbar';

const Component: Meta<typeof Search> = {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: 'Schema/Pills/Popups/Menus/Search',
  component: Search,
  decorators: [story => <Provider store={Mockstore}>{story()}</Provider>],
};

export default Component;

// A super-simple mock of a redux store
const Mockstore = configureStore({
  reducer: {
    // schema: schemaSlice.reducer,
  },
});

export const Default = {
  args: {},
};
