import React from 'react';
import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';

import { ReactFlowProvider } from 'reactflow';
import { NodeQualityRelationPopupNode } from './node-quality-relation-popup';

const Component: Meta<typeof NodeQualityRelationPopupNode> = {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: 'Schema/Pills/Popups/NodeQualityRelationPopupNode',
  component: NodeQualityRelationPopupNode,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <ReactFlowProvider>{story()}</ReactFlowProvider>
      </Provider>
    ),
  ],
};

export default Component;

// A super-simple mock of a redux store
const Mockstore = configureStore({
  reducer: {
    // schema: schemaSlice.reducer,
  },
});

export const Default = {
  args: {
    data: {
      name: 'TestEntity',
      attributes: [{ id: 'a' }],
      handles: [],
      nodeCount: 10,
      from: 1,
      to: 2,
    },
  },
};
