import { EntityPill } from '@/lib/components';
import { Popover, PopoverContent, PopoverTrigger } from '@/lib/components/popover';
import { NodeDetails } from '@/lib/components/nodeDetails';
import { useSchemaStats } from '@/lib/data-access';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { Handle, NodeProps, Position, useViewport } from 'reactflow';
import { SchemaReactflowNodeWithFunctions } from '../../../model/reactflow';
import { SchemaPopUp } from '../SchemaPopUp/SchemaPopUp';
import { QueryElementTypes, SchemaNode } from 'ts-common';

export const SchemaListEntityPill = React.memo(({ id, selected, data }: NodeProps<SchemaReactflowNodeWithFunctions>) => {
  const [openPopupLocation, setOpenPopupLocation] = useState<{ x: number; y: number } | null>(null);

  const viewport = useViewport();
  const schemaStats = useSchemaStats();

  const ref = useRef<HTMLDivElement>(null);
  /**
   * adds drag functionality in order to be able to drag the entityNode to the schema
   * @param event React Mouse drag event
   */
  const onDragStart = (event: React.DragEvent<HTMLDivElement>) => {
    const eventData: SchemaNode = {
      type: QueryElementTypes.Entity,
      name: id,
      attributes: data.attributes.map(attribute => attribute.attributes).flat(), // TODO: this seems wrong
    };
    event.dataTransfer.setData('application/reactflow', JSON.stringify(eventData));
    event.dataTransfer.effectAllowed = 'move';
  };

  useEffect(() => {
    if (data.popoverClose) {
      setOpenPopupLocation(null);
    }
  }, [data.popoverClose]);

  const popoverX = useMemo(() => {
    if (ref.current == null || openPopupLocation == null) return -1;
    const rect = ref.current.getBoundingClientRect();
    return rect.x - openPopupLocation.x + rect.width / 2;
  }, [viewport.x, openPopupLocation]);

  const popoverY = useMemo(() => {
    if (ref.current == null || openPopupLocation == null) return -1;
    const rect = ref.current.getBoundingClientRect();
    return rect.y - openPopupLocation.y + rect.height / 2;
  }, [viewport.y, openPopupLocation]);

  return (
    <>
      <div
        className="w-fit h-fit"
        onDragStart={event => onDragStart(event)}
        onDragStartCapture={event => onDragStart(event)}
        onMouseDownCapture={event => {
          if (!event.shiftKey) event.stopPropagation();
        }}
        onClickCapture={event => {
          if (openPopupLocation != null || ref.current == null) {
            return;
          }

          setOpenPopupLocation(ref.current.getBoundingClientRect());
        }}
        draggable
        ref={ref}
      >
        {openPopupLocation !== null && (
          <Popover key={data.name} open={true} boundaryElement={data.reactFlowRef} showArrow={true}>
            <PopoverTrigger x={popoverX} y={popoverY} />
            <PopoverContent>
              <NodeDetails name={data.name} colorHeader={'hsl(var(--clr-node))'}>
                <SchemaPopUp
                  data={data.attributes.reduce(
                    (acc, attr) => {
                      if (attr.name && attr.type) {
                        acc[attr.name] = attr.type;
                      }
                      return acc;
                    },
                    {} as Record<string, string>,
                  )}
                  numberOfElements={schemaStats?.nodes?.stats[data.name]?.count}
                />
              </NodeDetails>
            </PopoverContent>
          </Popover>
        )}

        <EntityPill
          draggable
          title={
            <div className="flex flex-row justify-between items-center">
              <span className="line-clamp-1">{id || ''}</span>
            </div>
          }
          withHandles="horizontal"
          handleRight={
            <Handle
              style={{ pointerEvents: 'none' }}
              id="entityTargetLeft"
              position={Position.Left}
              className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !b-0 !top-0 !bottom-0'}
              type="target"
            ></Handle>
          }
          handleLeft={
            <Handle
              style={{ pointerEvents: 'none' }}
              id="entitySourceRight"
              position={Position.Right}
              className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !top-0 !bottom-0'}
              type="source"
            ></Handle>
          }
        />
      </div>
    </>
  );
});

SchemaListEntityPill.displayName = 'EntityNode';
