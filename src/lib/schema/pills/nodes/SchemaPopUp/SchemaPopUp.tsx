import React from 'react';
import { Icon } from '@/lib/components/icon';
import { Tooltip, TooltipTrigger, TooltipContent, TooltipProvider } from '@/lib/components/tooltip';
import { getDataTypeIcon } from '@/lib/components/DataTypeIcon';

const formatNumber = (number: number) => {
  return number.toLocaleString('de-DE');
};

export type SchemaPopUpProps = {
  data: Record<string, any>;
  connections?: { to: string; from: string };
  numberOfElements?: number;
};

export const SchemaPopUp: React.FC<SchemaPopUpProps> = ({ data, numberOfElements, connections }) => {
  return (
    <>
      <div className="divide-y divide-y-secondary-200">
        {numberOfElements != null && numberOfElements != 0 && (
          <div className="flex flex-row gap-1 items-center justify-between px-3 py-1">
            <Icon component="icon-[ic--baseline-numbers]" size={24} />
            <span className="ml-auto text-right">{formatNumber(numberOfElements)}</span>
          </div>
        )}
        {connections && (
          <div className="px-1.5 py-1">
            <div className="flex flex-row gap-1 items-center justify-between">
              <span className="font-medium shrink-0">From</span>
              <span className="ml-auto text-right truncate grow">{connections.from}</span>
            </div>
            <div className="flex flex-row gap-1 items-center justify-between">
              <span className="font-medium shrink-0">To</span>
              <span className="ml-auto text-right truncate grow">{connections.to}</span>
            </div>
          </div>
        )}
        <TooltipProvider delay={300}>
          <div className="px-1.5 py-1">
            {Object.keys(data).length === 0 ? (
              <div className="flex justify-center items-center h-full ">
                <span>No attributes</span>
              </div>
            ) : (
              Object.entries(data).map(([k, v]) => (
                <Tooltip key={k}>
                  <div className="flex flex-row gap-1 items-center min-h-6">
                    <span className="font-medium truncate">{k}</span>
                    <TooltipTrigger asChild>
                      <span className="ml-auto text-right truncate shrink-0 flex items-center text-secondary-400 hover:text-secondary-600">
                        <Icon component={<Icon component={getDataTypeIcon(v)} size={24} />} size={24} />
                      </span>
                    </TooltipTrigger>
                    <TooltipContent side="right">
                      <div className="max-w-[18rem] break-all line-clamp-6">
                        {v !== undefined && (typeof v !== 'object' || Array.isArray(v)) && v != '' ? v : 'noData'}
                      </div>
                    </TooltipContent>
                  </div>
                </Tooltip>
              ))
            )}
          </div>
        </TooltipProvider>
      </div>
    </>
  );
};
