import { SchemaReturnFormat } from 'ts-common';
import { SchemaGraphology, SchemaGraphologyNode } from '../model';

export class SchemaUtils {
  public static schemaBackend2Graphology(schemaFromBackend: SchemaReturnFormat): SchemaGraphology {
    const { nodes, edges } = schemaFromBackend;

    // Instantiate a directed graph that allows self loops and parallel edges
    const schemaGraphology = new SchemaGraphology({ allowSelfLoops: true });

    // The graph schema needs a node for each node AND edge. These need then be connected
    if (!nodes) return schemaGraphology;

    nodes.forEach(node => {
      const attributes: SchemaGraphologyNode = {
        ...node,
        name: node.name,
        nodeCount: 0,
        summedNullAmount: 0,
        connectedRatio: 1,
        attributes: [...node.attributes],
        x: 0,
        y: 0,
      };
      schemaGraphology.addNode(node.name, attributes);
    });

    if (!edges) return schemaGraphology;

    // The name of the edge will be name + from + to, since edge names are not unique
    edges.forEach(edge => {
      const edgeID = [edge.name, '_', edge.from, edge.to].join(''); //ensure that all interpreted as string

      // This node is the actual edge
      schemaGraphology.addDirectedEdgeWithKey(edgeID, edge.from, edge.to, {
        nodeCount: 0,
        summedNullAmount: 0,
        fromRatio: 0,
        toRatio: 0,
        // width: 10,
        // height: 20,
        ...edge,
        attributes: edge.attributes,
      });
    });
    return schemaGraphology;
  }

  // public static addAttributeDimensionsToGraph(graph: SchemaGraph, inference: GraphAttributeDimensions): SchemaGraph {
  //   const { nodes, edges } = inference;

  //   graph.nodes.forEach((node) => {
  //     const dimensions = nodes[node.key];
  //     if (dimensions) {
  //       node.attributes?.attributes.forEach((attribute) => {
  //         const dimension = dimensions[attribute.name] as DimensionType | undefined;
  //         if (dimension) {
  //           attribute.dimension = dimension;
  //         }
  //       });
  //     }
  //   });

  //   graph.edges.forEach((edge) => {
  //     const dimensions = edges[edge.attributes?.name];
  //     if (dimensions) {
  //       edge.attributes?.attributes.forEach((attribute: any) => {
  //         const dimension = dimensions[attribute.name] as DimensionType | undefined;
  //         if (dimension) {
  //           attribute.dimension = dimension;
  //         }
  //       });
  //     }
  //   });

  //   return graph;
  // }

  // public static addAttributeInfoToGraph(graph: SchemaGraph, graphStats: GraphAttributeStats): SchemaGraph {
  //   const { nodeStats, edgeStats } = graphStats;

  //   graph.nodes.forEach((node) => {
  //     const nodeStat = nodeStats.find((stat) => stat.key === node.key);
  //     if (nodeStat && node.attributes?.attributes) {
  //       node.attributes.attributes = node.attributes.attributes.map((attribute) => {
  //         const attrStat = nodeStat.attributeStats.find((stat) => stat.name === attribute.name);
  //         return attrStat ? { ...attribute, ...attrStat } : attribute;
  //       });
  //     }
  //   });

  //   graph.edges.forEach((edge) => {
  //     const edgeStat = edgeStats.find((stat) => stat.type === edge.attributes?.name);
  //     if (edgeStat && edge.attributes?.attributes) {
  //       edge.attributes.attributes = edge.attributes.attributes.map((attribute: any) => {
  //         const attrStat = edgeStat.attributeStats.find((stat) => stat.name === attribute.name);
  //         return attrStat ? { ...attribute, ...attrStat } : attribute;
  //       });
  //     }
  //   });

  //   return graph;
  // }
}
