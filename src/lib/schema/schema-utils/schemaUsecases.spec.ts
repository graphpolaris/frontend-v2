import { describe, expect, it, test } from 'vitest';
import { movieSchemaRaw, northwindSchemaRaw, simpleSchemaRaw, twitterSchemaRaw } from '../../mock-data/schema';
import { SchemaGraphology } from '../model';
import { SchemaUtils } from './schemaUtils';
import { SchemaFromBackend } from 'ts-common';

describe('SchemaUsecases', () => {
  test.each([{ data: simpleSchemaRaw }, { data: movieSchemaRaw }, { data: northwindSchemaRaw }, { data: twitterSchemaRaw }])(
    'parseSchemaFromBackend parsing should work',
    ({ data }: any) => {
      const parsed = SchemaUtils.schemaBackend2Graphology(data as SchemaFromBackend);
      expect(parsed).toBeDefined();
    },
  );

  it('should export and reimport', () => {
    const parsed = SchemaUtils.schemaBackend2Graphology(simpleSchemaRaw as SchemaFromBackend);
    const reload = SchemaGraphology.from(parsed.export());

    expect(parsed).toEqual(reload);
  });

  test.each([{ data: simpleSchemaRaw }, { data: movieSchemaRaw }, { data: northwindSchemaRaw }, { data: twitterSchemaRaw }])(
    'should load my test json $data',
    ({ data }: any) => {
      expect(data).toBeDefined();
      expect(data.nodes).toBeDefined();
    },
  );
});
