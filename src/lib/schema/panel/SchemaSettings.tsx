import { Icon, TooltipProvider, Tooltip, TooltipTrigger, TooltipContent } from '@/lib/components';
import { Popover, PopoverTrigger, PopoverContent } from '@/lib/components/layout/Popover';
import { useState } from 'react';
import { Input } from '../../components/inputs';
import { useAppDispatch, useSchemaSettings } from '../../data-access';
import { setSchemaSettings } from '../../data-access/store/schemaSlice';
import { SchemaLayoutConfig } from './LayoutDescription/SchemaLayoutConfig';

export const SchemaSettings = () => {
  const settings = useSchemaSettings();
  const dispatch = useAppDispatch();

  const [selectedLayout, setSelectedLayout] = useState(
    Object.values(SchemaLayoutConfig).find(layout => layout.id === settings.layoutName) || Object.values(SchemaLayoutConfig)[0],
  );
  return (
    <div className="flex flex-col w-full gap-2 p-2">
      <span className="text-xs font-bold">Schema Settings</span>
      <Input
        size="sm"
        type="boolean"
        value={settings.animatedEdges}
        label="Animated Edges"
        onChange={(value: boolean) => {
          dispatch(setSchemaSettings({ ...settings, animatedEdges: value as any }));
        }}
      />
      <Input
        size="sm"
        type="boolean"
        value={settings.showMinimap}
        label="Show Minimap"
        onChange={(value: boolean) => {
          dispatch(setSchemaSettings({ ...settings, showMinimap: value as any }));
        }}
      />
      <div className="flex flex-row items-center gap-2 justify-between">
        <span className="text-sm">{'Layout Type'}</span>
        <Popover>
          <PopoverTrigger
            onClick={e => {
              e.stopPropagation();
            }}
          >
            <div className="flex flex-row items-center gap-1 border bg-light rounded p-1 ">
              {selectedLayout.icons.sm && <selectedLayout.icons.sm className="h-4 w-4" />}{' '}
              <span className="text-sm">{selectedLayout.displayName}</span>{' '}
              <Icon component="icon-[ic--baseline-arrow-drop-down]" size={16} />
            </div>
          </PopoverTrigger>
          <PopoverContent className="p-2">
            <div>
              <span className="text-secondary-700 m-2">Layout types</span>
            </div>
            <div className="grid grid-cols-2 gap-4 p-2">
              <TooltipProvider delay={0}>
                {Object.values(SchemaLayoutConfig).map(thisSchemaLayoutConfig => (
                  <Tooltip key={thisSchemaLayoutConfig.id}>
                    <TooltipTrigger asChild>
                      <div
                        className="flex flex-col items-center hover:bg-secondary-100 transition duration-200 cursor-pointer p-2 "
                        onClick={async () => {
                          dispatch(setSchemaSettings({ ...settings, layoutName: thisSchemaLayoutConfig.id as any }));
                          setSelectedLayout(thisSchemaLayoutConfig);
                        }}
                      >
                        {thisSchemaLayoutConfig.icons.sm && <thisSchemaLayoutConfig.icons.sm className="h-8 w-8" />}
                        <span className="text-sm text-secondary-700">{thisSchemaLayoutConfig.displayName}</span>
                      </div>
                    </TooltipTrigger>
                    <TooltipContent>
                      <span className="text-xs">{thisSchemaLayoutConfig.description}</span>
                    </TooltipContent>
                  </Tooltip>
                ))}
              </TooltipProvider>
            </div>
          </PopoverContent>
        </Popover>
      </div>
    </div>
  );
};
