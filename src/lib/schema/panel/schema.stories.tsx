import { schemaSlice, setSchema } from '@/lib/data-access/store';
import { movieSchemaRaw, northwindSchemaRaw, twitterSchemaRaw } from '@/lib/mock-data';
import { SchemaUtils } from '@/lib/schema/schema-utils';
import { configureStore } from '@reduxjs/toolkit';
import { Meta } from '@storybook/react';
import { Provider } from 'react-redux';
import { Schema } from './Schema';

const Component: Meta<typeof Schema> = {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: 'Schema/Panel',

  component: Schema,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

// Mock the schema and palette store
const Mockstore = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
  },
});

export const TestSimple = {
  play: async () => {
    const dispatch = Mockstore.dispatch;
    const schema = SchemaUtils.schemaBackend2Graphology({
      nodes: [
        {
          name: 'Thijs',
          attributes: [
            { name: 'city', type: 'string' },
            { name: 'vip', type: 'bool' },
            { name: 'state', type: 'string' },
          ],
        },
        {
          name: 'Airport',
          attributes: [
            { name: 'city', type: 'string' },
            { name: 'vip', type: 'bool' },
            { name: 'state', type: 'string' },
          ],
        },
      ],
      edges: [
        {
          name: 'Thijs:Airport',
          label: 'Thijs:Airport',
          from: 'Thijs',
          to: 'Airport',
          collection: 'flights',
          attributes: [
            { name: 'arrivalTime', type: 'int' },
            { name: 'departureTime', type: 'int' },
          ],
        },
      ],
    });

    dispatch(setSchema(schema.export()));
  },
};

export const TestTooltip = {
  play: async () => {
    const dispatch = Mockstore.dispatch;
    const schema = SchemaUtils.schemaBackend2Graphology({
      nodes: [
        {
          name: 'Thijs',
          attributes: [
            { name: 'city', type: 'string' },
            { name: 'vip', type: 'bool' },
            { name: 'state', type: 'string' },
          ],
        },
      ],
      edges: [],
    });

    dispatch(setSchema(schema.export()));
  },
};

export const TestMovieSchema = {
  play: async () => {
    console.log('TestMovieSchema');
    const dispatch = Mockstore.dispatch;

    const data = await movieSchemaRaw;
    console.log('data', data);
    const schema = SchemaUtils.schemaBackend2Graphology(data);

    dispatch(setSchema(schema.export()));
  },
};

export const TestNorthWindSchema = {
  play: async () => {
    const dispatch = Mockstore.dispatch;
    const schema = await SchemaUtils.schemaBackend2Graphology(northwindSchemaRaw);

    dispatch(setSchema(schema.export()));
  },
};

export const TestTwitterSchema = {
  play: async () => {
    const dispatch = Mockstore.dispatch;
    const schema = await SchemaUtils.schemaBackend2Graphology(twitterSchemaRaw);

    dispatch(setSchema(schema.export()));
  },
};

// export const TestWithSchema = {
//   play: async () => {
//     const dispatch = Mockstore.dispatch;
//     const schema = SchemaUtils.ParseSchemaFromBackend({
//       nodes: [
//         {
//           name: 'Thijs',
//           attributes: [],
//         },
//         {
//           name: 'Airport',
//           attributes: [
//             { name: 'city', type: 'string' },
//             { name: 'vip', type: 'bool' },
//             { name: 'state', type: 'string' },
//           ],
//         },
//         {
//           name: 'Airport2',
//           attributes: [
//             { name: 'city', type: 'string' },
//             { name: 'vip', type: 'bool' },
//             { name: 'state', type: 'string' },
//           ],
//         },
//         {
//           name: 'Plane',
//           attributes: [
//             { name: 'type', type: 'string' },
//             { name: 'maxFuelCapacity', type: 'int' },
//           ],
//         },
//         { name: 'Staff', attributes: [] },
//       ],
//       edges: [
//         {
//           name: 'Airport2:Airport',
//           from: 'Airport2',
//           to: 'Airport',
//           collection: 'flights',
//           attributes: [
//             { name: 'arrivalTime', type: 'int' },
//             { name: 'departureTime', type: 'int' },
//           ],
//         },
//         {
//           name: 'Airport:Staff',
//           from: 'Airport',
//           to: 'Staff',
//           collection: 'flights',
//           attributes: [{ name: 'salary', type: 'int' }],
//         },
//         {
//           name: 'Plane:Airport',
//           from: 'Plane',
//           to: 'Airport',
//           collection: 'flights',
//           attributes: [],
//         },
//         {
//           name: 'Airport:Thijs',
//           from: 'Airport',
//           to: 'Thijs',
//           collection: 'flights',
//           attributes: [{ name: 'hallo', type: 'string' }],
//         },
//         {
//           name: 'Thijs:Airport',
//           from: 'Thijs',
//           to: 'Airport',
//           collection: 'flights',
//           attributes: [{ name: 'hallo', type: 'string' }],
//         },
//         {
//           name: 'Staff:Plane',
//           from: 'Staff',
//           to: 'Plane',
//           collection: 'flights',
//           attributes: [{ name: 'hallo', type: 'string' }],
//         },
//         {
//           name: 'Staff:Airport2',
//           from: 'Staff',
//           to: 'Airport2',
//           collection: 'flights',
//           attributes: [{ name: 'hallo', type: 'string' }],
//         },
//         {
//           name: 'Airport2:Plane',
//           from: 'Airport2',
//           to: 'Plane',
//           collection: 'flights',
//           attributes: [{ name: 'hallo', type: 'string' }],
//         },

//         {
//           name: 'Airport:Airport',
//           from: 'Airport',
//           to: 'Airport',
//           collection: 'flights',
//           attributes: [{ name: 'test', type: 'string' }],
//         },
//       ],
//     });

//     console.info('dispatch dummy schema', schema.order);
//     dispatch(setSchema(schema.export()));
//   },
// };

// export const TestWithMockNorthWindSchema = {
//   play: async () => {
//     const dispatch = Mockstore.dispatch;

//     const schema = northWindSchema;
//     console.info('dispatch northWindSchema schema', schema.order);
//     dispatch(setSchema(schema.export()));
//   },
// };

// export const TestWithMockTwitterSchema = {
//   play: async () => {
//     const dispatch = Mockstore.dispatch;

//     const schema = twitterSchema;
//     console.info('dispatch TestWithMockTwitterSchema schema', schema.order);
//     dispatch(setSchema(schema.export()));
//   },
// };

// export const TestWithMockSimpleSchema = {
//   play: async () => {
//     const dispatch = Mockstore.dispatch;

//     const schema = simpleSchema;
//     console.info('dispatch simpleSchema schema', schema.order);
//     dispatch(setSchema(schema.export()));
//   },
// };

// export const TestWithMockMovieSchema = {
//   play: async () => {
//     const dispatch = Mockstore.dispatch;

//     const schema = movieSchema;
//     console.info('dispatch movieSchema schema', schema.order);
//     dispatch(setSchema(schema.export()));
//   },
// };

export default Component;
