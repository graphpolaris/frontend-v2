import { SmartBezierEdge, SmartStepEdge, SmartStraightEdge } from '@tisoap/react-flow-smart-edge';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import ReactFlow, { Edge, MiniMap, Node, ReactFlowInstance, ReactFlowProvider, useEdgesState, useNodesState } from 'reactflow';
import 'reactflow/dist/style.css';
import { Icon, Panel, TooltipProvider } from '../../components';
import { Button } from '../../components/buttons';
import { Popover, PopoverContent, PopoverTrigger } from '@/lib/components/popover';
import { Tooltip, TooltipContent, TooltipTrigger } from '../../components/tooltip/Tooltip';
import { useActiveSaveState, useSchema, useSchemaSettings, useSearchResultSchema, wsSchemaRequest } from '../../data-access';
import { setSchemaLoading, toSchemaGraphology } from '../../data-access/store/schemaSlice';
import { AlgorithmToLayoutProvider, AllLayoutAlgorithms, LayoutFactory } from '../../graph-layout';
import { ConnectionDragLine, ConnectionLine } from '../../querybuilder';
import { SchemaEntityPill } from '../pills/nodes/entity/SchemaEntityPill';
import { SchemaListEntityPill } from '../pills/nodes/entity/SchemaListEntityPill';
import { SchemaListRelationPill } from '../pills/nodes/relation/SchemaListRelationPill';
import { SchemaRelationPill } from '../pills/nodes/relation/SchemaRelationPill';
import { schemaExpandRelation, schemaGraphology2Reactflow } from '../schema-utils';
import { SchemaSettings } from './SchemaSettings';
import { NodeEdge } from '../pills/edges/NodeEdge';
import { SelfEdge } from '../pills/edges/SelfEdge';
import { SchemaLayoutConfig } from './LayoutDescription/SchemaLayoutConfig';

interface Props {
  content?: string;
  auth?: boolean;
  onRemove?: () => void;
}

const graphEntityPillNodeTypes = {
  entity: SchemaEntityPill,
  relation: SchemaRelationPill,
};
const listEntityPillNodeTypes = {
  entity: SchemaListEntityPill,
  relation: SchemaListRelationPill,
};

const edgeTypes = {
  nodeEdge: NodeEdge,
  selfEdge: SelfEdge,
  bezier: SmartBezierEdge,
  connection: ConnectionLine,
  straight: SmartStraightEdge,
  step: SmartStepEdge,
};

export const Schema = (props: Props) => {
  const settings = useSchemaSettings();
  const activeSaveState = useActiveSaveState();
  const searchResults = useSearchResultSchema();
  const dispatch = useDispatch();
  const [nodes, setNodes, onNodesChange] = useNodesState([] as Node[]);
  const [edges, setEdges, onEdgesChange] = useEdgesState([] as Edge[]);

  const [nodeTypes, setNodeTypes] = useState<{
    entity: React.FC<any>;
    relation: React.FC<any>;
  }>(graphEntityPillNodeTypes);

  const [hasLayoutBeenRun, setHasLayoutBeenRun] = useState(false);

  const reactFlowInstanceRef = useRef<ReactFlowInstance | null>(null);
  const reactFlowRef = useRef<HTMLDivElement>(null);

  // In case the schema is updated
  const schema = useSchema();
  const schemaGraphology = useMemo(() => toSchemaGraphology(schema.graph), [schema.graph]);
  const layout = useRef<AlgorithmToLayoutProvider<AllLayoutAlgorithms>>(null);

  function updateLayout() {
    const layoutFactory = new LayoutFactory();
    layout.current = layoutFactory.createLayout(settings.layoutName);
  }

  const maxZoom = 1.2;
  const fitView = () => {
    if (reactFlowInstanceRef.current) {
      reactFlowInstanceRef.current.fitView({ maxZoom });
    }
  };

  const refreshSchema = (useCached: boolean) => {
    if (!activeSaveState) return;
    dispatch(setSchemaLoading(true));
    wsSchemaRequest({ saveStateID: activeSaveState.id, useCached: useCached }); // No callback, this would override global behavior
  };

  useEffect(() => {
    updateLayout();
    if (sessionStorage.getItem('firstUserConnection') === 'true') {
      sessionStorage.setItem('firstUserConnection', 'false');
    } else {
      sessionStorage.setItem('firstUserConnection', 'true');
    }
  }, []);

  async function layoutGraph() {
    setNodeTypes(graphEntityPillNodeTypes);
    updateLayout();
    const expandedSchema = schemaExpandRelation(schemaGraphology);
    const bounds = reactFlowRef.current?.getBoundingClientRect();
    const boundWidthOverflow = bounds && bounds.width < 600 ? 600 : 0; // for small screens, allow it to overflow
    const xy = bounds ? { x1: 0, x2: boundWidthOverflow, y1: 0, y2: bounds.height - 200 } : { x1: 0, x2: 500, y1: 0, y2: 1000 }; // layout.current?.setVerbose(true);
    await layout.current?.layout(expandedSchema, xy);
    const schemaFlow = schemaGraphology2Reactflow(expandedSchema, settings.connectionType, settings.animatedEdges);

    let nodesWithRef, edgesWithRef;
    if (!hasLayoutBeenRun) {
      nodesWithRef = schemaFlow.nodes.map(node => {
        return {
          ...node,
          data: { ...node.data, reactFlowRef, popoverClose: false },
        };
      });

      edgesWithRef = schemaFlow.edges.map(edge => {
        return {
          ...edge,
          data: { ...edge.data, reactFlowRef, popoverClose: false },
        };
      });

      setHasLayoutBeenRun(true);
    } else {
      nodesWithRef = schemaFlow.nodes.map(node => {
        return {
          ...node,
          data: { ...node.data },
        };
      });

      edgesWithRef = schemaFlow.edges.map(edge => {
        return {
          ...edge,
          data: { ...edge.data },
        };
      });
    }

    setNodes(nodesWithRef);
    setEdges(edgesWithRef);
    setTimeout(() => fitView(), 100);
  }

  async function layoutList() {
    setNodeTypes(listEntityPillNodeTypes);
    updateLayout();
    const expandedSchema = schemaExpandRelation(schemaGraphology);
    const bounds = reactFlowRef.current?.getBoundingClientRect();
    const xy = bounds ? { x1: 50, x2: bounds.width - 50, y1: 50, y2: bounds.height - 200 } : { x1: 0, x2: 500, y1: 0, y2: 1000 };
    await layout.current?.layout(expandedSchema, xy);
    const schemaFlow = schemaGraphology2Reactflow(expandedSchema, settings.connectionType, settings.animatedEdges);

    schemaFlow.nodes = schemaFlow.nodes.filter(node => !node.id.toLowerCase().includes('bloom'));
    schemaFlow.edges = schemaFlow.edges.filter(edge => !edge.id.toLowerCase().includes('bloom'));

    schemaFlow.nodes = schemaFlow.nodes.filter(node => !node.id.toLowerCase().includes('bloom'));
    schemaFlow.edges = schemaFlow.edges.filter(edge => !edge.id.toLowerCase().includes('bloom'));

    let nodesWithRef, edgesWithRef;
    if (!hasLayoutBeenRun) {
      nodesWithRef = schemaFlow.nodes.map(node => {
        return {
          ...node,
          data: { ...node.data, reactFlowRef, popoverClose: false },
        };
      });

      edgesWithRef = schemaFlow.edges.map(edge => {
        return {
          ...edge,
          data: { ...edge.data, reactFlowRef, popoverClose: false },
        };
      });

      setHasLayoutBeenRun(true);
    } else {
      nodesWithRef = schemaFlow.nodes.map(node => {
        return {
          ...node,
          data: { ...node.data },
        };
      });

      edgesWithRef = schemaFlow.edges.map(edge => {
        return {
          ...edge,
          data: { ...edge.data },
        };
      });
    }

    setNodes(nodesWithRef);
    setEdges(edgesWithRef);
    setTimeout(() => fitView(), 100);
  }

  useEffect(() => {
    if (schemaGraphology === undefined || schemaGraphology.order == 0) {
      setNodes([]);
      setEdges([]);
      return;
    }

    const selectedLayout =
      Object.values(SchemaLayoutConfig).find(layout => layout.id === settings.layoutName) || Object.values(SchemaLayoutConfig)[0];
    if (selectedLayout.layout === 'Graph') {
      layoutGraph();
    } else {
      layoutList();
    }
  }, [schema.graph, settings]);

  useEffect(() => {
    setNodes(nds =>
      nds.map(node => ({
        ...node,
        selected: searchResults.includes(node.id) || searchResults.includes(node.data.label),
      })),
    );
  }, [searchResults]);

  const nodeColor = (node: any) => {
    switch (node.type) {
      case 'entity':
        return 'hsl(var(--clr-node))';
      case 'relation':
        return 'hsl(var(--clr-relation))';
      default:
        return '#ff0072';
    }
  };

  const handleOnClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const target = event.target as HTMLElement;
    const clickedOutsideNode = target.classList.contains('react-flow__pane');

    setNodes(nds =>
      nds.map(node => ({
        ...node,
        data: {
          ...node.data,
          popoverClose: clickedOutsideNode,
        },
      })),
    );

    setEdges(edg =>
      edg.map(edge => ({
        ...edge,
        data: {
          ...edge.data,
          popoverClose: clickedOutsideNode,
        },
      })),
    );
  };

  return (
    <Panel
      title="Schema"
      className="schema-panel"
      tooltips={
        <>
          <TooltipProvider>
            <Tooltip>
              <TooltipTrigger>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--baseline-remove]"
                  onClick={() => {
                    if (props.onRemove) props.onRemove();
                  }}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Hide</p>
              </TooltipContent>
            </Tooltip>
            <Tooltip>
              <TooltipTrigger>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--baseline-content-copy]"
                  onClick={() => {
                    // Copy the schema to the clipboard
                    navigator.clipboard.writeText(JSON.stringify(schema.graph, null, 2));
                  }}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Copy Schema to Clipboard</p>
              </TooltipContent>
            </Tooltip>
            <Tooltip>
              <TooltipTrigger>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--baseline-fullscreen]"
                  onClick={() => {
                    fitView();
                  }}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Fit to screen</p>
              </TooltipContent>
            </Tooltip>
            <Tooltip>
              <TooltipTrigger>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--baseline-cached]"
                  onClick={() => {
                    refreshSchema(true);
                  }}
                  onDoubleClick={() => {
                    refreshSchema(false);
                  }}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Refresh Schema</p>
              </TooltipContent>
            </Tooltip>
            <Popover>
              <PopoverTrigger>
                <Tooltip>
                  <TooltipTrigger>
                    <Button
                      variantType="secondary"
                      variant="ghost"
                      size="xs"
                      iconComponent="icon-[ic--baseline-settings]"
                      className="schema-settings"
                    />
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Schema Settings</p>
                  </TooltipContent>
                </Tooltip>
              </PopoverTrigger>
              <PopoverContent>
                <SchemaSettings />
              </PopoverContent>
            </Popover>
          </TooltipProvider>
        </>
      }
    >
      <div className="w-full h-full flex flex-col justify-between" ref={reactFlowRef}>
        {schema.loading ? (
          <div className="h-full flex flex-col items-center justify-center">
            <Icon component="icon-[mingcute--loading-line]" size={56} className="w-15 h-15 animate-spin " />
          </div>
        ) : schema.error ? (
          <div className="m-3 self-center text-center flex h-full flex-col justify-center">
            <p className="text-xl font-bold text-error">Error fetching schema!</p>
            <p className="">Please retry or contact your Database's Administrator</p>
          </div>
        ) : nodes.length === 0 ? (
          <p className="m-3 text-xl font-bold">No Elements found in Schema</p>
        ) : (
          <ReactFlowProvider>
            <ReactFlow
              snapGrid={[10, 10]}
              snapToGrid
              onlyRenderVisibleElements={false}
              nodesDraggable={true}
              nodeTypes={nodeTypes}
              edgeTypes={edgeTypes}
              connectionLineComponent={ConnectionDragLine}
              onNodesChange={onNodesChange}
              onEdgesChange={onEdgesChange}
              nodes={nodes}
              edges={edges}
              onInit={reactFlowInstance => {
                reactFlowInstanceRef.current = reactFlowInstance;
                setTimeout(() => fitView(), 100);
              }}
              onClick={handleOnClick}
              proOptions={{ hideAttribution: true }}
            >
              {settings.showMinimap && <MiniMap nodeColor={nodeColor} />}
            </ReactFlow>
          </ReactFlowProvider>
        )}
      </div>
    </Panel>
  );
};
