import React from 'react';

interface IconProps {
  className?: string;
}

export const DagreIcon: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="16"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <rect fill="hsl(var(--clr-relation))" width="7.3312178" height="2.7723093" x="-7.0483686e-10" y="6.3607378" />
    <rect fill="hsl(var(--clr-node))" width="7.3312178" height="2.7723093" x="-7.0483686e-10" y="12.721476" />
    <rect fill="hsl(var(--clr-node))" width="7.3312178" height="2.7723093" x="4.3343911" y="-1.7570567e-09" />
    <rect fill="hsl(var(--clr-relation))" width="7.3312178" height="2.7723093" x="8.6687822" y="6.3607378" />
    <rect fill="hsl(var(--clr-relation))" width="7.3312178" height="2.7723093" x="8.6687822" y="12.721476" />
  </svg>
);
