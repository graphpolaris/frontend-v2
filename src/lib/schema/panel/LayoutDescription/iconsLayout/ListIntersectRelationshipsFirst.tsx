import React from 'react';

interface IconProps {
  className?: string;
}

export const ListIntersectRelationshipsFirst: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="16"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <rect fill="hsl(var(--clr-relation))" width="7.3312178" height="2.7723093" x="3.5840001" y="0.25310755" />
    <rect fill="hsl(var(--clr-node))" width="7.3312178" height="2.7723093" x="5.0840001" y="12.974584" />
    <rect fill="hsl(var(--clr-relation))" width="7.3312178" height="2.7723093" x="3.5840001" y="4.4935994" />
    <rect fill="hsl(var(--clr-node))" width="7.3312178" height="2.7723093" x="5.0840001" y="8.7340918" />
  </svg>
);
