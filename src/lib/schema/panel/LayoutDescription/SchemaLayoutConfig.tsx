import { GridIcon } from '@/lib/schema/panel/LayoutDescription/iconsLayout/GridIcon';
import { ListIntersectIcon } from '@/lib/schema/panel/LayoutDescription/iconsLayout/ListIntersect';
import { ListIntersectNodesEdgesFirstIcon } from '@/lib/schema/panel/LayoutDescription/iconsLayout/ListIntersectNodesFirst';
import { CircleIcon } from '@/lib/schema/panel/LayoutDescription/iconsLayout/CircleIcon';
import { DagreIcon } from '@/lib/schema/panel/LayoutDescription/iconsLayout/DagreIcon';
import { ListIntersectRelationshipsFirst } from '@/lib/schema/panel/LayoutDescription/iconsLayout/ListIntersectRelationshipsFirst';
import { Layouts } from '@/lib/graph-layout';

export type IconSize = 'sm' | 'md' | 'lg';

export type SchemaLayoutConfigMeta = {
  id: (typeof Layouts)[keyof typeof Layouts];
  displayName: string;
  description: string;
  icons: Record<IconSize, React.FC<React.SVGProps<SVGSVGElement>>>;
  layout: 'Graph' | 'List';
};

export const SchemaLayoutConfig: Record<string, SchemaLayoutConfigMeta> = {
  DAGRE: {
    id: Layouts.DAGRE,
    displayName: 'Dagre',
    description: 'Physycal Force-Directed Layout',
    icons: { sm: DagreIcon, md: DagreIcon, lg: DagreIcon },
    layout: 'Graph',
  },
  CIRCLE: {
    id: Layouts.CIRCLE,
    displayName: 'Circle',
    description: 'Circular Layout',
    icons: { sm: CircleIcon, md: CircleIcon, lg: CircleIcon },
    layout: 'Graph',
  },
  GRID: {
    id: Layouts.GRID,
    displayName: 'Grid',
    description: 'Grid Layout',
    icons: { sm: GridIcon, md: GridIcon, lg: GridIcon },
    layout: 'Graph',
  },
  LIST: {
    id: Layouts.LISTINTERSECTED,
    displayName: 'List',
    description: 'Vertical List Layout',
    icons: { sm: ListIntersectIcon, md: ListIntersectIcon, lg: ListIntersectIcon },
    layout: 'List',
  },
  LIST_NODES_FIRST: {
    id: Layouts.LISTNODEFIRST,
    displayName: 'List Nodes First',
    description: 'Vertical List Layout with Nodes First',
    icons: {
      sm: ListIntersectNodesEdgesFirstIcon,
      md: ListIntersectNodesEdgesFirstIcon,
      lg: ListIntersectNodesEdgesFirstIcon,
    },
    layout: 'List',
  },
  LIST_EDGES_FIRST: {
    id: Layouts.LISTEDGEFIRST,
    displayName: 'List Edges First',
    description: 'Vertical List Layout with Edges First',
    icons: {
      sm: ListIntersectRelationshipsFirst,
      md: ListIntersectRelationshipsFirst,
      lg: ListIntersectRelationshipsFirst,
    },
    layout: 'List',
  },
};
