import React, { useEffect, useMemo, useRef, useState } from 'react';
import { SmartBezierEdge, SmartStepEdge, SmartStraightEdge } from '@tisoap/react-flow-smart-edge';
import ReactFlow, { Edge, Node, ReactFlowInstance, ReactFlowProvider, useEdgesState, useNodesState, MiniMap } from 'reactflow';
import 'reactflow/dist/style.css';
import { Button } from '../../components/buttons';
import { useSchemaGraph, useSchemaSettings, useSearchResultSchema } from '../../data-access';
import { toSchemaGraphology } from '../../data-access/store/schemaSlice';
import { NodeEdge } from '../pills/edges/NodeEdge';
import { SelfEdge } from '../pills/edges/SelfEdge';
import { SchemaEntityPill } from '../pills/nodes/entity/SchemaEntityPill';
import { SchemaRelationPill } from '../pills/nodes/relation/SchemaRelationPill';
import { SchemaSettings } from './SchemaSettings';
import { AlgorithmToLayoutProvider, AllLayoutAlgorithms, LayoutFactory } from '../../graph-layout';
import { ConnectionLine, ConnectionDragLine } from '../../querybuilder';
import { schemaExpandRelation, schemaGraphology2Reactflow } from '../schema-utils';
import { Panel } from '../../components';
import { Tooltip, TooltipContent, TooltipTrigger } from '@/lib/components/tooltip';
import { resultSetFocus } from '../../data-access/store/interactionSlice';
import { useDispatch } from 'react-redux';
import { Popover, PopoverContent, PopoverTrigger } from '@/lib/components/popover';

interface Props {
  content?: string;
  auth?: boolean;
  onRemove?: () => void;
}

const onInit = (reactFlowInstance: ReactFlowInstance) => {
  setTimeout(() => reactFlowInstance.fitView(), 100);
};

const nodeTypes = {
  entity: SchemaEntityPill,
  relation: SchemaRelationPill,
};
const edgeTypes = {
  nodeEdge: NodeEdge,
  selfEdge: SelfEdge,
  bezier: SmartBezierEdge,
  connection: ConnectionLine,
  straight: SmartStraightEdge,
  step: SmartStepEdge,
};

export const Schema = (props: Props) => {
  const settings = useSchemaSettings();
  const searchResults = useSearchResultSchema();
  const dispatch = useDispatch();
  const [nodes, setNodes, onNodesChange] = useNodesState([] as Node[]);
  const [edges, setEdges, onEdgesChange] = useEdgesState([] as Edge[]);

  // viewport
  const initialViewportRef = useRef<{ x: number; y: number; zoom: number } | null>(null);
  const [hasLayoutBeenRun, setHasLayoutBeenRun] = useState(false);

  // Time threshold for distinguishing between a click and a drag
  const isPillClicked = useRef<boolean>(false);

  const reactFlowInstanceRef = useRef<ReactFlowInstance | null>(null);
  const reactFlowRef = useRef<HTMLDivElement>(null);

  // In case the schema is updated
  const schemaGraph = useSchemaGraph();
  const schemaGraphology = useMemo(() => toSchemaGraphology(schemaGraph), [schemaGraph]);
  const layout = useRef<AlgorithmToLayoutProvider<AllLayoutAlgorithms>>(null);

  function updateLayout() {
    const layoutFactory = new LayoutFactory();
    layout.current = layoutFactory.createLayout(settings.layoutName);
  }

  const fitView = () => {
    if (reactFlowInstanceRef.current) {
      reactFlowInstanceRef.current.fitView();
    }
  };

  useEffect(() => {
    updateLayout();
    if (sessionStorage.getItem('firstUserConnection') === 'true') {
      sessionStorage.setItem('firstUserConnection', 'false');
    } else {
      sessionStorage.setItem('firstUserConnection', 'true');
    }
  }, []);

  async function layoutGraph() {
    if (schemaGraphology === undefined || schemaGraphology.order == 0) {
      setNodes([]);
      setEdges([]);
      return;
    }

    updateLayout();
    const expandedSchema = schemaExpandRelation(schemaGraphology);
    const bounds = reactFlowRef.current?.getBoundingClientRect();
    const xy = bounds ? { x1: 50, x2: bounds.width - 50, y1: 50, y2: bounds.height - 200 } : { x1: 0, x2: 500, y1: 0, y2: 1000 };
    await layout.current?.layout(expandedSchema, xy);
    const schemaFlow = schemaGraphology2Reactflow(expandedSchema, settings.connectionType, settings.animatedEdges);

    let nodesWithRef, edgesWithRef;
    if (!hasLayoutBeenRun) {
      nodesWithRef = schemaFlow.nodes.map(node => {
        return {
          ...node,
          data: { ...node.data, reactFlowRef, popoverClose: false },
        };
      });

      edgesWithRef = schemaFlow.edges.map(edge => {
        return {
          ...edge,
          data: { ...edge.data, reactFlowRef, popoverClose: false },
        };
      });

      setHasLayoutBeenRun(true);
    } else {
      nodesWithRef = nodes.map(node => {
        return {
          ...node,
          data: { ...node.data },
        };
      });

      edgesWithRef = edges.map(edge => {
        return {
          ...edge,
          data: { ...edge.data },
        };
      });
    }

    setNodes(nodesWithRef);
    setEdges(edgesWithRef);
    setTimeout(() => fitView(), 100);
  }

  useEffect(() => {
    layoutGraph();
  }, [schemaGraph, settings]);

  useEffect(() => {
    setNodes(nds =>
      nds.map(node => ({
        ...node,
        selected: searchResults.includes(node.id) || searchResults.includes(node.data.label),
      })),
    );
  }, [searchResults]);

  const nodeColor = (node: any) => {
    switch (node.type) {
      case 'entity':
        return 'hsl(var(--clr-node))';
      case 'relation':
        return 'hsl(var(--clr-relation))';
      default:
        return '#ff0072';
    }
  };

  const handleOnClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const target = event.target as HTMLElement;
    const clickedOutsideNode = target.classList.contains('react-flow__pane');

    setNodes(nds =>
      nds.map(node => ({
        ...node,
        data: {
          ...node.data,
          popoverClose: clickedOutsideNode,
        },
      })),
    );

    setEdges(edg =>
      edg.map(edge => ({
        ...edge,
        data: {
          ...edge.data,
          popoverClose: clickedOutsideNode,
        },
      })),
    );
  };

  return (
    <Panel
      title="Schema"
      tooltips={
        <>
          <Tooltip>
            <TooltipTrigger>
              <Button
                variantType="secondary"
                variant="ghost"
                size="xs"
                iconComponent="icon-[ic--baseline-remove]"
                onClick={() => {
                  if (props.onRemove) props.onRemove();
                }}
              />
            </TooltipTrigger>
            <TooltipContent>
              <p>Hide</p>
            </TooltipContent>
          </Tooltip>
          <Tooltip>
            <TooltipTrigger>
              <Button
                variantType="secondary"
                variant="ghost"
                size="xs"
                iconComponent="icon-[ic--baseline-content-copy]"
                onClick={() => {
                  // Copy the schema to the clipboard
                  navigator.clipboard.writeText(JSON.stringify(schemaGraph, null, 2));
                }}
              />
            </TooltipTrigger>
            <TooltipContent>
              <p>Copy Schema to Clipboard</p>
            </TooltipContent>
          </Tooltip>
          <Tooltip>
            <TooltipTrigger>
              <Button
                variantType="secondary"
                variant="ghost"
                size="xs"
                iconComponent="icon-[ic--baseline-fullscreen]"
                onClick={() => {
                  fitView();
                }}
              />
            </TooltipTrigger>
            <TooltipContent>
              <p>Fit to screen</p>
            </TooltipContent>
          </Tooltip>
          <Popover>
            <PopoverTrigger>
              <Tooltip>
                <TooltipTrigger>
                  <Button
                    variantType="secondary"
                    variant="ghost"
                    size="xs"
                    iconComponent="icon-[ic--baseline-settings]"
                    className="schema-settings"
                  />
                </TooltipTrigger>
                <TooltipContent>
                  <p>Schema settings</p>
                </TooltipContent>
              </Tooltip>
            </PopoverTrigger>
            <PopoverContent>
              <SchemaSettings />
            </PopoverContent>
          </Popover>
        </>
      }
    >
      <div className="schema-panel w-full h-full flex flex-col justify-between" ref={reactFlowRef}>
        {nodes.length === 0 ? (
          <p className="m-3 text-xl font-bold">No Elements in List</p>
        ) : (
          <ReactFlowProvider>
            <ReactFlow
              snapGrid={[10, 10]}
              snapToGrid
              onlyRenderVisibleElements={false}
              nodesDraggable={true}
              nodeTypes={nodeTypes}
              edgeTypes={edgeTypes}
              connectionLineComponent={ConnectionDragLine}
              onNodesChange={onNodesChange}
              onEdgesChange={onEdgesChange}
              onMouseDownCapture={() => dispatch(resultSetFocus({ focusType: 'schema' }))}
              nodes={nodes}
              edges={edges}
              onInit={reactFlowInstance => {
                reactFlowInstanceRef.current = reactFlowInstance;
                onInit(reactFlowInstance);
              }}
              onClick={handleOnClick}
              proOptions={{ hideAttribution: true }}
            >
              {settings.showMinimap && <MiniMap nodeColor={nodeColor} />}
            </ReactFlow>
          </ReactFlowProvider>
        )}
      </div>
    </Panel>
  );
};
