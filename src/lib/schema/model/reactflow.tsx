/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Node } from 'reactflow';
import { SchemaGraphologyNode } from './graphology';

/** All possible options of node-types */
export enum NodeType {
  entity = 'entity',
  relation = 'relation',
}

/** All possible options of categories of an attribute */
export enum AttributeCategory {
  categorical = 'Categorical',
  numerical = 'Numerical',
  other = 'Other',
  undefined = 'undefined',
}

export type SchemaReactflowData = {
  name: string;
  attributes: SchemaGraphologyNode[];
  nodeCount: number;
  summedNullAmount: number;
  label: string;
  type: string;
  hovered: boolean;
};

export type SchemaReactflowEntity = SchemaReactflowData & {
  // handles: string[];
  connectedRatio: number;
  name: string;
  x: number;
  y: number;
  reactFlowRef: any;
  popoverClose: boolean;
};

export type SchemaReactflowRelation = SchemaReactflowData & {
  from: string;
  to: string;
  collection: string;
  fromRatio: number;
  toRatio: number;
  x: number;
  y: number;
  reactFlowRef: any;
  popoverClose: boolean;
};

export type SchemaReactflowNodeWithFunctions = SchemaReactflowEntity & {
  toggleNodeQualityPopup: (id: string) => void;
  toggleAttributeAnalyticsPopupMenu: (id: string) => void;
};

export type SchemaReactflowRelationWithFunctions = SchemaReactflowRelation & {
  toggleNodeQualityPopup: (id: string) => void;
  toggleAttributeAnalyticsPopupMenu: (id: string) => void;
};

/**
 * Typing for the Node Quality data of an entity.
 * It is used for the Node quality analytics and will be displayed in the corresponding popup.
 */
export type NodeQualityDataForEntities = {
  nodeCount: number;
  attributeNullCount: number;
  notConnectedNodeCount: number;

  isAttributeDataIn: boolean; // is true when the data to display has arrived

  // for user interactions
  onClickCloseButton: () => void;
};

/**
 * Typing for the Node Quality data of a relation.
 * It is used for the Node quality analytics and will be displayed in the corresponding popup.
 */
export type NodeQualityDataForRelations = {
  nodeCount: number;
  attributeNullCount: number;
  // from-entity node --relation--> to-entity node
  fromRatio: number; // the ratio of from-entity nodes to nodes that have this relation
  toRatio: number; // the ratio of to-entity nodes to nodes that have this relation

  isAttributeDataIn: boolean; // is true when the data to display has arrived

  // for user interactions
  onClickCloseButton: () => void;
};

/**
 * Typing for the Node Quality popup of an entity or relation node.
 */
export type NodeQualityPopupNode = Node & {
  data: NodeQualityDataForEntities | NodeQualityDataForRelations;
  nodeID: string; //ID of the node for which the popup is
};

/**
 * Typing for the attribute analytics popup menu data of an entity or relation.
 */
export type AttributeAnalyticsData = {
  nodeType: NodeType;
  nodeID: string;
  attributes: AttributeWithData[];
  // nullAmount: number;

  isAttributeDataIn: boolean; // is true when the data to display has arrived

  // for user interactions
  onClickCloseButton: () => void;
  onClickPlaceInQueryBuilderButton: (name: string, type: string) => void;
  searchForAttributes: (id: string, searchbarValue: string) => void;
  resetAttributeFilters: (id: string) => void;
  applyAttributeFilters: (id: string, category: AttributeCategory, predicate: string, percentage: number) => void;
};

/**
 * Typing for the attribute analytics popup menu of entity or relation nodes
 */
export type AttributeAnalyticsPopupMenuNode = Node & {
  nodeID: string; //ID of the node for which the popup is
  data: AttributeAnalyticsData;
};

/** Typing of the attributes which are stored in the popup menu's */
export type AttributeWithData = {
  attribute: SchemaGraphologyNode;
  category: AttributeCategory;
  nullAmount: number;
};
