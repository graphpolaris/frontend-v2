import { MultiGraph } from 'graphology';
import { Attributes as GAttributes, NodeEntry, EdgeEntry, SerializedGraph } from 'graphology-types';
import { SchemaNode } from 'ts-common';

/** Attribute type, consist of a name */
export type SchemaGraphologyNode = GAttributes & SchemaNode;
export type SchemaGraphologyEdge = GAttributes;

export type SchemaGraphologyNodeEntry = NodeEntry<SchemaGraphologyNode>;
export type SchemaGraphologyEdgeEntry = EdgeEntry<SchemaGraphologyNode, SchemaGraphologyNode>;

export class SchemaGraphology extends MultiGraph<SchemaGraphologyNode, SchemaGraphologyEdge, GAttributes> {}
export type SchemaGraph = SerializedGraph<SchemaGraphologyNode, SchemaGraphologyEdge, GAttributes>;
