export enum URLParams {
  saveState = 'saveState',
  impersonateID = 'impersonateID',
}

export function getParam(param: URLParams) {
  const query = new URLSearchParams(window.location.search);
  return query.get(param);
}

export function setParam(param: URLParams, value: string | undefined) {
  const query = new URLSearchParams(window.location.search);
  if (!value) {
    deleteParam(param);
  } else {
    query.set(param, value);
    history.pushState(null, '', '?' + query.toString());
  }
}

export function deleteParam(param: URLParams) {
  const query = new URLSearchParams(window.location.search);
  query.delete(param);
  history.pushState(null, '', '?' + query.toString());
}
