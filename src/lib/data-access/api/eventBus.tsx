import {
  useAuthentication,
  useAuthCache,
  useAppDispatch,
  useSessionCache,
  useQuerybuilderHash,
  readInSchemaFromBackend,
  useVisualization,
  wsSchemaRequest,
  wsSchemaSubscription,
  wsSchemaStatsRequest,
  useML,
  useActiveQuery,
  useActiveSaveState,
  resetGraphQueryResults,
} from '@/lib/data-access';
import { Broker, wsQueryErrorSubscription, wsQueryRequest, wsQuerySubscription } from '@/lib/data-access/broker';
import { addInfo } from '@/lib/data-access/store/configSlice';
import { setMLResult } from '@/lib/data-access/store/mlSlice';
import { useEffect } from 'react';
import {
  wsGetState,
  wsGetStates,
  wsUpdateState,
  nilUUID,
  wsGetStatesSubscription,
  wsDeleteStateSubscription,
  wsGetStateSubscription,
  wsTestSaveStateConnectionSubscription,
  wsStateGetPolicy,
  DatabaseStatus,
  wsTestSaveStateConnection,
} from '../broker/wsState';
import {
  addSaveState,
  testedSaveState,
  selectSaveState,
  updateSaveStateList,
  updateSelectedSaveState,
  setFetchingSaveStates,
  setStateAuthorization,
  setActiveQueryID,
  setQueryState,
  setQuerybuilderNodes,
} from '../store/sessionSlice';
import { URLParams, getParam } from './url';
import { VisState, setVisualizationState } from '../store/visualizationSlice';
import { isEqual } from 'lodash-es';
import { clearSchema, setSchemaAttributeInformation, setSchemaLoading } from '../store/schemaSlice';
import { addError } from '@/lib/data-access/store/configSlice';
import { unSelect } from '../store/interactionSlice';
import { wsReconnectSubscription, wsUserGetPolicy } from '../broker/wsUser';
import { authorized, setSessionID } from '../store/authSlice';
import { asyncSetNewGraphQueryResult, queryingBackend } from '../store/graphQueryResultSlice';
import {
  SaveState,
  SchemaGraphStats,
  allMLTypes,
  LinkPredictionInstance,
  GraphQueryTranslationResultMessage,
  wsReturnKey,
} from 'ts-common';

export const EventBus = (props: { onRunQuery: (useCached: boolean) => void; onAuthorized: () => void }) => {
  const { login } = useAuthentication();
  const auth = useAuthCache();
  const dispatch = useAppDispatch();
  const ml = useML();
  const session = useSessionCache();
  const queryHash = useQuerybuilderHash();
  const activeSS = useActiveSaveState();
  const activeQuery = useActiveQuery();
  const visState = useVisualization();

  async function loadSaveState(saveStateID: string | undefined, saveStates: Record<string, SaveState>) {
    if (saveStateID && saveStates && saveStateID in saveStates) {
      console.debug('Setting state fetched from database', saveStateID, saveStates);
      const state = saveStates[saveStateID];
      if (state) {
        if (state.queryStates && state.queryStates.openQueryArray.length > 0) {
          let activeQuery = state.queryStates.openQueryArray.find(q => q.id === state.queryStates.activeQueryId);
          if (!activeQuery) {
            activeQuery = state.queryStates.openQueryArray[0];
            dispatch(setActiveQueryID(activeQuery.id || -1));
          }
          if (activeQuery) {
            const queryBuilderState = {
              ...state.queryStates,
              openQueryArray: state.queryStates.openQueryArray.map(query => ({
                ...query,
                ignoreReactivity: false,
                queryTranslation: {} as GraphQueryTranslationResultMessage,
              })),
            };

            dispatch(setQueryState(queryBuilderState));
            dispatch(setQuerybuilderNodes(activeQuery));
          }
        } else {
          console.error('No queries found in state', state);
        }
        dispatch(
          setVisualizationState(
            Object.keys(state.visualizations).length !== 0 // should only occur in mock data
              ? (state.visualizations as VisState)
              : {
                  activeVisualizationIndex: -1,
                  openVisualizationArray: [],
                },
          ),
        );

        testConnection(state.id);
      }
    }
  }

  function testConnection(saveStateID: string) {
    dispatch(testedSaveState({ saveStateID, status: DatabaseStatus.testing }));
    wsTestSaveStateConnection({ saveStateID });
  }

  useEffect(() => {
    const unsubs: (() => void)[] = [];

    unsubs.push(
      wsSchemaSubscription((data, status) => {
        if (status !== 'success') {
          dispatch(addError('Failed to fetch schema'));
          dispatch(clearSchema({ error: true }));
          return;
        }
        if (!data) {
          dispatch(addError('Failed to fetch schema'));
          return;
        } else {
          dispatch(readInSchemaFromBackend(data));
          dispatch(addInfo('Schema graph updated'));
        }
      }),
    );

    unsubs.push(
      wsQuerySubscription(data => {
        if (!data) {
          dispatch(addError('Failed to fetch graph query result'));
          return;
        }
        asyncSetNewGraphQueryResult(data, dispatch);
        dispatch(addInfo('Query Executed!'));
        dispatch(unSelect());
      }),
    );

    unsubs.push(
      wsQueryErrorSubscription(data => {
        dispatch(addError('Failed to fetch graph query result'));
        console.error('Query Error', data);
        dispatch(resetGraphQueryResults());
      }),
    );

    unsubs.push(
      wsTestSaveStateConnectionSubscription(data => {
        if (data?.status === 'success' && data.saveStateID) {
          dispatch(
            testedSaveState({
              saveStateID: data.saveStateID,
              status: data.status == 'success' ? DatabaseStatus.online : DatabaseStatus.offline,
            }),
          );
        } else {
          dispatch(addError('DB connection failed'));
        }
      }),
    );

    unsubs.push(
      wsReconnectSubscription(data => {
        if (data == null) return;

        if (!Broker.instance().checkSessionID(data.sessionID)) {
          dispatch(addError('DEBUG: session id does not match!!'));
          throw new Error('Session ID does not match!!');
        }

        dispatch(setSessionID(data));

        props.onAuthorized();

        // Process URL Params
        const paramSaveState = getParam(URLParams.saveState);
        if (paramSaveState && paramSaveState !== nilUUID) {
          wsGetState({ saveStateID: paramSaveState });
        }

        dispatch(setFetchingSaveStates(true));

        // check authorizations
        wsUserGetPolicy(null, res => {
          if (res) dispatch(authorized(res));
        });

        wsGetStates(null, data => {
          if (data) {
            dispatch(setFetchingSaveStates(false));
            return true;
          } else {
            dispatch(addError('Failed to fetch states'));
            return false;
          }
        });

        // Broker.instance().sendMessage({ //TODO!!
        //   sessionID: auth?.sessionID || '',
        //   key: 'broadcastState',
        //   body: { type: 'subscribe', status: '', value: {} },
        // });
      }),
    );

    Broker.instance().subscribe((data: SchemaGraphStats) => {
      dispatch(setSchemaAttributeInformation(data));
      dispatch(addInfo('Received attribute information'));
    }, wsReturnKey.schemaStatsResult);

    // Broker.instance().subscribe((data: QueryBuilderState) => dispatch(setQuerybuilderNodes(data)), 'query_builder_state');

    allMLTypes.forEach(mlType => {
      const id = Broker.instance().subscribe((data: LinkPredictionInstance[]) => {
        dispatch(setMLResult({ type: mlType, result: data }));
      }, mlType);
      unsubs.push(() => Broker.instance().unSubscribe(mlType, id));
    });

    unsubs.push(
      wsGetStatesSubscription((data, status) => {
        console.debug('Save States updated', data, status);
        if (status !== 'success' || !data) {
          dispatch(addError('Failed to fetch states'));
          return;
        }

        dispatch(updateSaveStateList(data));
        const d = Object.fromEntries(data.map(x => [x.id, x]));
        loadSaveState(session.currentSaveState, d);
        data.forEach(ss => {
          if (session.saveStatesAuthorization?.[ss.id] === undefined) {
            wsStateGetPolicy({ saveStateID: ss.id }, ret => {
              if (!ret) {
                dispatch(addError('Failed to fetch state authorization'));
                return;
              }
              dispatch(setStateAuthorization({ id: ss.id, authorization: ret }));
            });
          }
        });
      }),
    );

    unsubs.push(
      wsGetStateSubscription((data, status) => {
        if (!['success', 'unchanged'].includes(status) || !data) {
          dispatch(addError('Failed to fetch state'));
          return;
        }
        if (data.id !== nilUUID) {
          if (isEqual(data, session.saveStates[data.id])) {
            console.log('State unchanged, not updating');
            return;
          }
          dispatch(addSaveState(data));
          dispatch(selectSaveState(data.id));
          loadSaveState(data.id, session.saveStates);
          wsStateGetPolicy({ saveStateID: data.id }, ret => {
            if (!ret) {
              dispatch(addError('Failed to fetch state authorization'));
              return;
            }
            dispatch(setStateAuthorization({ id: data.id, authorization: ret }));
          });
        }
      }),
    );

    unsubs.push(wsDeleteStateSubscription(data => {}));

    // Broker.instance().subscribe((response: TestDatabaseConnectionResponse) => {
    //   if (response && response.status === 'success') dispatch(testedSaveState(response.saveStateID));
    // }, 'tested_connection');

    login();

    // Setup cleanup
    return () => {
      // clear callback subscriptions
      unsubs.forEach(unsub => {
        unsub();
      });
      Broker.instance().unSubscribeAll(wsReturnKey.queryStatusTranslationResult);
      Broker.instance().unSubscribeAll(wsReturnKey.queryStatusResult);
      Broker.instance().unSubscribeAll(wsReturnKey.schemaInference);
    };
  }, []);

  useEffect(() => {
    if (activeSS) {
      console.log('Query State Hash Updated', queryHash, activeSS.id);
      wsUpdateState(activeSS, (data, status) => {
        // if (status === 'unchanged') {
        //  TODO: check if this is needed, but for now removed so that the query is always sent, solving issue with changing ss
        //   console.log('State unchanged, not updating');
        //   return;
        // }

        if (data) {
          if (activeQuery?.id) {
            dispatch(queryingBackend());
            wsQueryRequest({ saveStateID: activeSS.id, ml, queryID: activeQuery.id, useCached: true });
          }
        }
      });
      // }
    }
  }, [queryHash]);

  useEffect(() => {
    if (session.currentSaveState) {
      const state = { ...session.saveStates[session.currentSaveState] };
      if (!isEqual(state.visualizations, visState)) {
        console.debug('Updating visState state', visState);
        state.visualizations = { ...visState };
        dispatch(updateSelectedSaveState(state));
        wsUpdateState(state);
      }
    }
  }, [visState]);

  useEffect(() => {
    // New active database
    if (session.currentSaveState && session.currentSaveState !== nilUUID) {
      dispatch(setSchemaLoading(true));
      wsSchemaRequest({ saveStateID: session.currentSaveState, useCached: true });
      wsSchemaStatsRequest({ saveStateID: session.currentSaveState });
      loadSaveState(session.currentSaveState, session.saveStates);
    }
  }, [session.currentSaveState]);

  useEffect(() => {
    // New active database
    if (
      session.currentSaveState &&
      session.testedSaveState[session.currentSaveState] === DatabaseStatus.online &&
      activeQuery?.id != null
    ) {
      dispatch(queryingBackend());
      wsQueryRequest({ saveStateID: session.currentSaveState, ml, queryID: activeQuery.id, useCached: true });
    }
  }, [session.currentSaveState, session.testedSaveState]);

  useEffect(() => {
    // Newly (un)authorized
    if (auth.authentication?.authenticated && auth.authentication?.jwt) {
      console.warn('Authorized', auth.authentication);
      if (Broker.instance().isConnected()) {
        console.log('WS already connected, no need to reconnect');
        props.onAuthorized();
      } else {
        console.log('WS not connected, connecting');
        Broker.instance()
          .useAuth(auth.authentication)
          .connect(() => {
            console.debug('WS connected', session.currentSaveState, window.location.search);
          });
      }
    } else {
      // dispatch(logout());
    }
  }, [auth.authentication]);

  return <div className="hide"></div>;
};
