import { MultiGraph } from 'graphology';
import { assert, describe, expect, it } from 'vitest';

describe('SchemaSlice Tests', () => {
  it('should make a graphology graph', () => {
    const graph = new MultiGraph({ allowSelfLoops: true });
    expect(graph);

    const graph2 = new MultiGraph();
    expect(graph2);

    const exported = graph.export();
    expect(exported);
  });

  it('export and reimport equality check for graphology graph', () => {
    const graph = new MultiGraph({ allowSelfLoops: true });
    expect(graph);
    const exported = graph.export();
    expect(exported);

    const graphReloaded = MultiGraph.from(exported);
    expect(graphReloaded).toStrictEqual(graph);
  });

  // it('get the initial state', () => {
  //   expect(initialState);
  // });

  // it('should return the initial state', () => {
  //   const state = store.getState();

  //   const schema = state.schema;
  //   expect(schema);

  //   const graph = MultiGraph.from(schema.graphologySerialized);

  //   // console.log(graph);
  //   // console.log(initialState);
  //   expect(graph);
  // });

  //   it('should handle a todo being added to an empty list', () => {
  //     let state = store.getState().schema;
  //     let schema = useSchema();

  //     // const unchangedBook = state.bookList.find((book) => book.id === '1');
  //     // expect(unchangedBook?.title).toBe('1984');
  //     // expect(unchangedBook?.author).toBe('George Orwell');

  //     // store.dispatch(updateBook({ id: '1', title: '1985', author: 'George Bush' }));
  //     // state = store.getState().book;
  //     // let changeBook = state.bookList.find((book) => book.id === '1');
  //     // expect(changeBook?.title).toBe('1985');
  //     // expect(changeBook?.author).toBe('George Bush');

  //     // store.dispatch(
  //     //     updateBook({ id: '1', title: '1984', author: 'George Orwell' })
  //     // );
  //     // state = store.getState().book;
  //     // const backToUnchangedBook = state.bookList.find((book) => book.id === '1');

  //     // expect(backToUnchangedBook).toEqual(unchangedBook);
  //     // ]);
  //   });
});

// test('Deletes a book from list with id', () => {
//   let state = store.getState().book;
//   const initialBookCount = state.bookList.length;

//   store.dispatch(deleteBook({ id: '1' }));
//   state = store.getState().book;

//   expect(state.bookList.length).toBeLessThan(initialBookCount); // Checking if new length smaller than inital length, which is 3
// });

// test('Adds a new book', () => {
//   let state = store.getState().book;
//   const initialBookCount = state.bookList.length;

//   store.dispatch(
//     addNewBook({ id: '4', author: 'Tester', title: 'Testers manual' })
//   );
//   state = store.getState().book;
//   const newlyAddedBook = state.bookList.find((book) => book.id === '4');
//   expect(newlyAddedBook?.author).toBe('Tester');
//   expect(newlyAddedBook?.title).toBe('Testers manual');
//   expect(state.bookList.length).toBeGreaterThan(initialBookCount);
// });
