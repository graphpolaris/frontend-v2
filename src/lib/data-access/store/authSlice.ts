import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';
import { cloneDeep } from 'lodash-es';
import { UserAuthorizationHeaders } from 'ts-common';

export const UserAuthorizationHeadersDefaults: UserAuthorizationHeaders = {
  savestate: {
    R: false,
    W: false,
  },
  demoUser: {
    // checks if user can impersonate demoUser to create and share demo save states
    R: false,
    W: false,
  },
};

export type UseIsAuthorizedState = SingleIsAuthorizedState & {
  roomID: string | undefined;
};

export type SingleIsAuthorizedState = {
  authenticated: boolean;
  jwt: string;
  userID: number;
  username: string;
  roomID: string | undefined;
  sessionID: string;
};

export type AuthSliceState = {
  authentication: SingleIsAuthorizedState | undefined;
  authorization: UserAuthorizationHeaders;
};

export const initialState: AuthSliceState = {
  authentication: undefined,
  authorization: cloneDeep(UserAuthorizationHeadersDefaults),
};

export const authSlice = createSlice({
  name: 'auth',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    authenticated(state, action: PayloadAction<SingleIsAuthorizedState>) {
      console.info('%cAuthenticated ', 'background-color: blue', action.payload);
      state.authentication = action.payload;
      state.authorization = cloneDeep(UserAuthorizationHeadersDefaults);
    },
    authorized(state, action: PayloadAction<UserAuthorizationHeaders>) {
      console.info(`%cAuthorized Result: `, 'background-color: green', action.payload);
      state.authorization = action.payload;
    },
    changeRoom(state, action: PayloadAction<string | undefined>) {
      if (state.authentication === undefined) {
        console.warn('Not authenticated');
        return;
      }

      console.info('Changing Room to', action.payload);
      state.authentication.roomID = action.payload;
      const query = new URLSearchParams(window.location.search);
      if (action?.payload) {
        query.set('roomID', action?.payload || 'null');
        history.pushState(null, '', '?' + query.toString());
      } else {
        query.delete('roomID');
        history.pushState(null, '', '?' + query.toString());
      }
    },
    logout(state) {
      console.info('Logging out');
      state = cloneDeep(initialState);
      const query = new URLSearchParams(window.location.search);
      query.delete('roomID');
      history.pushState(null, '', '?' + query.toString());
    },
    setSessionID(state, action: PayloadAction<{ sessionID: string }>) {
      if (!state.authentication) {
        console.warn('Not authenticated');
        return;
      }
      if (state.authentication.sessionID === action.payload.sessionID) {
        console.info('Already connected with', action.payload);
      } else {
        console.info('Reconnecting with', action.payload, 'from', state.authentication.sessionID);
        state.authentication.sessionID = action.payload.sessionID;
      }
    },
    // unauthorized(state) {
    //   console.warn('Unauthorized');
    //   state.authentication.authenticated = false;
    //   const query = new URLSearchParams(window.location.search);
    //   query.delete('roomID');
    //   history.pushState(null, '', '?' + query.toString());
    // },
  },
});

export const { authorized, authenticated, logout, changeRoom, setSessionID } = authSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const authState = (state: RootState) => state.auth;

export default authSlice.reducer;
