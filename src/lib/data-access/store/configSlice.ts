import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';

export enum Theme {
  system = 'system-mode',
  light = 'light-mode',
  dark = 'dark-mode',
}

// Define the initial state using that type
export type ConfigStateI = {
  theme: Theme;
  currentTheme: Theme;
  autoSendQueries: boolean;
  errors: string[];
  warnings: string[];
  infos: string[];
  successes: string[];
};
export const initialState: ConfigStateI = {
  theme: (localStorage.getItem('theme') as Theme) ?? Theme.system,
  currentTheme: resolveTheme((localStorage.getItem('theme') as Theme) ?? Theme.system),
  autoSendQueries: true,
  errors: [],
  warnings: [],
  infos: [],
  successes: [],
};

function resolveTheme(theme: Theme) {
  // needs if check for test
  if (theme == Theme.system && window?.matchMedia) {
    const prefersDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
    return prefersDarkMode ? Theme.dark : Theme.light;
  }
  return theme;
}

export const configSlice = createSlice({
  name: 'config',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    addSuccess: (state, action: PayloadAction<string>) => {
      state.successes.push(action.payload);
    },
    removeLastSuccess: state => {
      state.successes.shift();
    },
    addInfo: (state, action: PayloadAction<string>) => {
      state.infos.push(action.payload);
    },
    removeLastInfo: state => {
      state.infos.shift();
    },
    addError: (state, action: PayloadAction<string>) => {
      console.error('Error Received!', action.payload);
      state.errors.push(action.payload);
    },
    removeLastError: state => {
      state.errors.shift();
    },
    addWarning: (state, action: PayloadAction<string>) => {
      console.warn('Warn Received!', action.payload);
      state.warnings.push(action.payload);
    },
    removeLastWarning: state => {
      state.warnings.shift();
    },
    setTheme: (state, action: PayloadAction<Theme>) => {
      localStorage.setItem('theme', action.payload);
      state.theme = action.payload;
      state.currentTheme = resolveTheme(action.payload);
    },
    setCurrentTheme: (state, action: PayloadAction<boolean>) => {
      if (state.theme === Theme.system) {
        state.currentTheme = action.payload ? Theme.dark : Theme.light;
      }
    },
  },
});

export const {
  addError,
  removeLastError,
  addWarning,
  removeLastWarning,
  addSuccess,
  removeLastSuccess,
  addInfo,
  removeLastInfo,
  setTheme,
  setCurrentTheme,
} = configSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const configState = (state: RootState) => state.config;

export default configSlice.reducer;
