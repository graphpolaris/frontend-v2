import { configureStore } from '@reduxjs/toolkit';
import graphQueryResultSlice from './graphQueryResultSlice';
import schemaSlice from './schemaSlice';
import configSlice from './configSlice';
import sessionSlice from './sessionSlice';
import authSlice from './authSlice';
import mlSlice from './mlSlice';
import searchResultSlice from './searchResultSlice';
import visualizationSlice from './visualizationSlice';
import interactionSlice from './interactionSlice';
import policyUsersSlice from './authorizationUsersSlice';
import policyPermissionSlice from './authorizationResourcesSlice';
import insightSharingSlice from './insightSharingSlice';
import projectSlice from './projectSlice';

export const store = configureStore({
  reducer: {
    graphQueryResult: graphQueryResultSlice,
    schema: schemaSlice,
    sessionCache: sessionSlice,
    auth: authSlice,
    config: configSlice,
    ml: mlSlice,
    searchResults: searchResultSlice,
    interaction: interactionSlice,
    visualize: visualizationSlice,
    policyUsers: policyUsersSlice,
    policyResources: policyPermissionSlice,
    insightSharing: insightSharingSlice,
    project: projectSlice,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredPaths: ['schema.graphologySerialized', 'querybuilder.graphologySerialized'],
      },
    }),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {counter: CounterState}
export type AppDispatch = typeof store.dispatch;
