import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { GraphQueryResult, selectGraphQueryResult, selectGraphQueryResultMetaData } from './graphQueryResultSlice';
import {
  schemaGraph,
  selectSchemaLayout,
  schemaSettingsState,
  SchemaSettings,
  schemaStatsState,
  SchemaSliceI,
  schema,
} from './schemaSlice';
import type { RootState, AppDispatch } from './store';
import { ConfigStateI, configState } from '@/lib/data-access/store/configSlice';

import {
  activeSaveState,
  activeSaveStateAuthorization,
  activeSaveStateQuery,
  selectQuerybuilderHash,
  SessionCacheI,
  sessionCacheState,
} from './sessionSlice';
import { AuthSliceState, authState } from './authSlice';
import { visualizationState, VisState, visualizationActive } from './visualizationSlice';
import { allMLEnabled, selectML } from './mlSlice';
import {
  searchResultState,
  searchResultData,
  searchResultSchema,
  searchResultQB,
  recentSearches,
  SearchCategoryMapI,
  CategoryDataI,
} from './searchResultSlice';
import { AllLayoutAlgorithms } from '../../graph-layout';
import { SchemaGraph } from '../../schema';
import { SelectionStateI, FocusStateI, focusState, selectionState } from './interactionSlice';
import { VisualizationSettingsType } from '../../vis/common';
import { PolicyUsersState, selectPolicyState } from './authorizationUsersSlice';
import { selectResourcesPolicyState } from './authorizationResourcesSlice';
import { selectInsights } from './insightSharingSlice';
import {
  SchemaGraphStats,
  QueryGraphEdgeHandle,
  PolicyResourcesState,
  InsightModel,
  QueryBuilderSettings,
  ML,
  GraphStatistics,
  SaveState,
  SaveStateAuthorizationHeaders,
  Query,
} from 'ts-common';
import { ProjectState, selectProject } from './projectSlice';

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch: () => AppDispatch = useDispatch;
// export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

/** Gives the graphQueryResult from the store */
export const useGraphQueryResult: () => GraphQueryResult = () => useAppSelector(selectGraphQueryResult);
export const useGraphQueryResultMeta: () => GraphStatistics | undefined = () => useAppSelector(selectGraphQueryResultMetaData);

// Gives the schema
export const useSchema: () => SchemaSliceI = () => useAppSelector(schema);
export const useSchemaGraph: () => SchemaGraph = () => useAppSelector(schemaGraph);
export const useSchemaSettings: () => SchemaSettings = () => useAppSelector(schemaSettingsState);
export const useSchemaStats: () => SchemaGraphStats = () => useAppSelector(schemaStatsState);
export const useSchemaLayout: () => AllLayoutAlgorithms = () => useAppSelector(selectSchemaLayout);

// Querybuilder Slices
export const useActiveQuery: () => Query | undefined = () => useAppSelector(activeSaveStateQuery);
export const useQuerybuilderHash: () => string = () => useAppSelector(selectQuerybuilderHash);
// export const useQuerybuilderGraph: () => QueryMultiGraph = () => useAppSelector(selectQuerybuilderGraph);
// export const useQuerybuilderSettings: () => QueryBuilderSettings = () => useAppSelector(queryBuilderSettingsState);
// export const useQuerybuilder: () => QueryBuilderState = () => useAppSelector(queryBuilderState);
// export const useQuerybuilderAttributesShown: () => QueryGraphEdgeHandle[] = () => useAppSelector(queryBuilderAttributesShown);

// Overall Configuration of the app
export const useConfig: () => ConfigStateI = () => useAppSelector(configState);
export const useSessionCache: () => SessionCacheI = () => useAppSelector(sessionCacheState);
export const useActiveSaveState: () => SaveState | undefined = () => useAppSelector(activeSaveState);
export const useActiveSaveStateAuthorization: () => SaveStateAuthorizationHeaders = () => useAppSelector(activeSaveStateAuthorization);
export const useAuthCache: () => AuthSliceState = () => useAppSelector(authState);

// Machine Learning Slices
export const useML: () => ML = () => useAppSelector(selectML);
export const useMLEnabledHash: () => string = () => useAppSelector(allMLEnabled);

// Search Result Slices
export const useSearchResult: () => SearchCategoryMapI = () => useAppSelector(searchResultState);
export const useSearchResultData: () => CategoryDataI = () => useAppSelector(searchResultData);
export const useSearchResultSchema: () => any[] = () => useAppSelector(searchResultSchema);
export const useSearchResultQB: () => CategoryDataI = () => useAppSelector(searchResultQB);
export const useRecentSearches: () => string[] = () => useAppSelector(recentSearches);

// Visualization Slices
export const useVisualization: () => VisState = () => useAppSelector(visualizationState);
export const useActiveVisualization: () => VisualizationSettingsType | undefined = () => useAppSelector(visualizationActive);

// Interaction Slices
export const useSelection: () => SelectionStateI | undefined = () => useAppSelector(selectionState);
export const useFocus: () => FocusStateI | undefined = () => useAppSelector(focusState);

// Authorization Users Slice
export const useUsersPolicy: () => PolicyUsersState = () => useAppSelector(selectPolicyState);

// Authorization Resources Slice
export const useResourcesPolicy: () => PolicyResourcesState = () => useAppSelector(selectResourcesPolicyState);

// Insights - Reports and Alerts
export const useInsights: () => InsightModel[] = () => useAppSelector(selectInsights);

// Project Slices
export const useProjects: () => ProjectState = () => useAppSelector(selectProject);
