import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';
import { ML, MLTypes } from 'ts-common';

/**************************************************************** */

// Define the initial state using that type
export const mlDefaultState: ML = {
  linkPrediction: { enabled: false, result: [] },
  centrality: { enabled: false, result: {} },
  communityDetection: { enabled: false, result: [], jaccard_threshold: 0.5 },
  shortestPath: { enabled: false, result: [] },
};
export const mlSlice = createSlice({
  name: 'ml',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState: mlDefaultState,
  reducers: {
    setLinkPredictionEnabled: (state, action: PayloadAction<boolean>) => {
      state.linkPrediction.enabled = action.payload;
    },
    setCommunityDetectionEnabled: (state, action: PayloadAction<boolean>) => {
      state.communityDetection.enabled = action.payload;
    },
    setCentralityEnabled: (state, action: PayloadAction<boolean>) => {
      state.centrality.enabled = action.payload;
    },
    setShortestPathEnabled: (state, action: PayloadAction<boolean>) => {
      state.shortestPath.enabled = action.payload;
    },
    setShortestPathSource: (state, action: PayloadAction<string | undefined>) => {
      state.shortestPath.srcNode = action.payload;
    },
    setShortestPathTarget: (state, action: PayloadAction<string | undefined>) => {
      state.shortestPath.trtNode = action.payload;
    },
    setMLResult: (state, action: PayloadAction<{ type: MLTypes; result: any[] }>) => {
      state[action.payload.type].result = action.payload.result;
    },
  },
});
export const {
  setMLResult,
  setLinkPredictionEnabled,
  setCommunityDetectionEnabled,
  setCentralityEnabled,
  setShortestPathSource,
  setShortestPathTarget,
  setShortestPathEnabled,
} = mlSlice.actions;

export const allMLEnabled = (state: RootState): string => {
  return JSON.stringify(Object.values(state.ml).map(ml => ml.enabled));
};

export const selectML = (state: RootState) => state.ml;
export default mlSlice.reducer;
