import { AllLayoutAlgorithms, Layouts } from '@/lib/graph-layout';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SchemaUtils } from '../../schema/schema-utils';
import type { RootState } from './store';
import { SchemaGraph, SchemaGraphology } from '@/lib/schema';
import { SchemaGraphStats, SchemaReturnFormat } from 'ts-common';

/**************************************************************** */

export const schemaConnectionTypeArray: Array<SchemaConnectionTypes> = ['connection', 'bezier', 'straight', 'step'];
export type SchemaConnectionTypes = 'connection' | 'bezier' | 'straight' | 'step';
export type SchemaSettings = {
  connectionType: SchemaConnectionTypes;
  layoutName: AllLayoutAlgorithms;
  animatedEdges: boolean;
  showMinimap: boolean;
};

export type SchemaSliceI = {
  graph: SchemaGraph;
  graphStats: SchemaGraphStats;
  loading: boolean;
  settings: SchemaSettings;
  error: boolean;
};

// Define the initial state using that type
export const initialState: SchemaSliceI = {
  graph: new SchemaGraphology().export(),
  graphStats: {
    nodes: {
      count: 0,
      stats: {},
    },
    edges: {
      count: 0,
      stats: {},
    },
  },
  loading: true,
  error: false,
  settings: {
    connectionType: 'connection',
    layoutName: Layouts.DAGRE,
    animatedEdges: false,
    showMinimap: false,
  },
};
export const schemaSlice = createSlice({
  name: 'schema',
  initialState,
  reducers: {
    setSchema: (state, action: PayloadAction<SchemaGraph>) => {
      if (action.payload === undefined) throw new Error('Schema is undefined');
      state.graph = action.payload;
      state.loading = false;
      state.error = false;
    },
    clearSchema: (state, action: PayloadAction<undefined | { error: boolean }>) => {
      state.graph = new SchemaGraphology().export();
      state.loading = false;
      state.error = action.payload?.error ?? false;
    },

    readInSchemaFromBackend: (state, action: PayloadAction<SchemaReturnFormat>) => {
      state.graph = SchemaUtils.schemaBackend2Graphology(action.payload).export();
      state.graphStats = action.payload.graphStats;
      state.error = false;
      state.loading = false;
    },

    setSchemaSettings: (state, action: PayloadAction<SchemaSettings>) => {
      state.settings = action.payload;
    },

    setSchemaAttributeInformation: (state, action: PayloadAction<SchemaGraphStats>) => {
      state.graphStats = action.payload;
    },

    setSchemaLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
      state.error = false;
      if (!action.payload) state.graph = new SchemaGraphology().export();
    },
  },
});
export const { readInSchemaFromBackend, setSchema, setSchemaSettings, clearSchema, setSchemaAttributeInformation, setSchemaLoading } =
  schemaSlice.actions;

export const schema = (state: RootState) => state.schema;
export const schemaSettingsState = (state: RootState) => state.schema.settings;
export const schemaStatsState = (state: RootState) => state.schema.graphStats;

/**
 * Select the schema and convert it to a graphology object
 * */
export const toSchemaGraphology = (graph: SchemaGraph): SchemaGraphology => {
  // This is really weird but for some reason all the attributes appeared as read-only otherwise
  const ret = new SchemaGraphology();
  ret.import(SchemaGraphology.from(graph).export());
  return ret;
};

/**
 * Select the schema
 * */
export const schemaGraph = (state: RootState): SchemaGraph => {
  return state.schema.graph;
};

// /**
//  * selects the SchemaLayout enum
//  * @param {GraphLayout} state
//  * @returns {GraphLayout} enum of type GraphLayout
//  */
export const selectSchemaLayout = (state: RootState) => state.schema.settings.layoutName;

export default schemaSlice.reducer;
