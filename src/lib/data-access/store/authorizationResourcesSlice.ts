import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';
import { PolicyResourcesState } from 'ts-common';

//TODO !FIXME: add middleware to fetch resources from backend
const initialState: PolicyResourcesState = {
  read: ['database', 'query', 'visualization', 'policy'],
  write: ['database', 'query', 'visualization', 'policy'],
};

export const policyResourcesSlice = createSlice({
  name: 'policyResources',
  initialState,
  reducers: {
    getResourcesPolicy: (state, action: PayloadAction<PolicyResourcesState>) => {
      return action.payload;
    },
  },
});

export const { getResourcesPolicy } = policyResourcesSlice.actions;
export default policyResourcesSlice.reducer;
export const selectResourcesPolicyState = (state: RootState) => state.policyResources;
