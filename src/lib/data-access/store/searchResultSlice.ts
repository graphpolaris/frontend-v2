import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';

export type CATEGORY_KEYS = 'data' | 'schema' | 'querybuilder';

export type CategoryDataI = { nodes: Record<string, any>[]; edges: Record<string, any>[] };

export type SearchCategoryMapI = {
  data: CategoryDataI;
  schema: CategoryDataI;
  querybuilder: CategoryDataI;
};

type InitialState = {
  categories: SearchCategoryMapI;
  recentSearches: string[];
};

const initialState: InitialState = {
  categories: {
    data: { nodes: [], edges: [] },
    schema: { nodes: [], edges: [] },
    querybuilder: { nodes: [], edges: [] },
  },
  recentSearches: [],
};

export const searchResultSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    addSearchResultData: (state, action: PayloadAction<{ nodes: Record<string, any>[]; edges: Record<string, any>[] }>) => {
      state.categories.data = action.payload;
    },
    addSearchResultSchema: (state, action: PayloadAction<{ nodes: Record<string, any>[]; edges: Record<string, any>[] }>) => {
      state.categories.schema = action.payload;
    },
    addSearchResultQueryBuilder: (state, action: PayloadAction<{ nodes: Record<string, any>[]; edges: Record<string, any>[] }>) => {
      state.categories.querybuilder = action.payload;
    },
    addSearchResultSelected: (
      state,
      action: PayloadAction<{
        category: CATEGORY_KEYS;
        value: { nodes: Record<string, any>[]; edges: Record<string, any>[] };
      }>,
    ) => {
      state.categories.data = { nodes: [], edges: [] };
      state.categories.schema = { nodes: [], edges: [] };
      state.categories.querybuilder = { nodes: [], edges: [] };

      const { category, value } = action.payload;
      state.categories[category] = value;
    },
    resetSearchResults: state => {
      return {
        ...state,
        data: initialState.categories.data,
        schema: initialState.categories.schema,
        querybuilder: initialState.categories.querybuilder,
      };
    },
    addRecentSearch: (state, action: PayloadAction<string>) => {
      if (!state.recentSearches.includes(action.payload)) {
        state.recentSearches.unshift(action.payload);
        state.recentSearches = state.recentSearches.slice(0, 10);
      }
    },
  },
});

export const {
  addSearchResultData,
  addSearchResultSchema,
  addSearchResultQueryBuilder,
  addSearchResultSelected,
  resetSearchResults,
  addRecentSearch,
} = searchResultSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const recentSearches = (state: RootState) => state.searchResults.recentSearches;
export const searchResultState = (state: RootState) => state.searchResults?.categories;
export const searchResultData = (state: RootState) => state.searchResults?.categories.data;

export const searchResultSchema = createSelector(searchResultState, categories => {
  const nodes = categories?.schema?.nodes?.map(node => node.key) || [];
  const edges = categories?.schema?.edges?.map(edge => edge.key) || [];
  return [...nodes, ...edges];
});
export const searchResultQB = (state: RootState) => state.searchResults.categories.querybuilder;

export default searchResultSlice.reducer;
