import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { InsightModel } from 'ts-common';

type InsightState = {
  insights: InsightModel[];
};

const initialState: InsightState = {
  insights: [],
};

const insightSharingSlice = createSlice({
  name: 'insightSharing',
  initialState,
  reducers: {
    setInsights(state, action: PayloadAction<InsightModel[]>) {
      state.insights = action.payload;
    },
    addInsight: (state, action: PayloadAction<InsightModel>) => {
      if (!action.payload) {
        console.error('Received null payload in addInsight');
        return;
      }

      const type = action.payload.type;
      if (!type) {
        console.error('Insight type is missing in payload', action.payload);
        return;
      }

      state.insights.push(action.payload);
    },
    updateInsight(state, action: PayloadAction<InsightModel>) {
      const index = state.insights.findIndex(i => i.id === action.payload.id);
      if (index !== -1) {
        state.insights[index] = { ...action.payload };
      } else {
        state.insights.push(action.payload);
      }
    },
    deleteInsight(state, action: PayloadAction<{ id: number }>) {
      const index = state.insights.findIndex(i => i.id === action.payload.id);
      if (index !== -1) {
        state.insights.splice(index, 1);
      }
    },
  },
});

export const { setInsights, addInsight, updateInsight, deleteInsight } = insightSharingSlice.actions;

export const selectReports = (state: RootState): InsightModel[] => state.insightSharing.insights?.filter(i => i.type === 'report') ?? [];
export const selectAlerts = (state: RootState): InsightModel[] => state.insightSharing.insights?.filter(i => i.type === 'alert') ?? [];
export const selectInsights = (state: RootState): InsightModel[] => state.insightSharing.insights ?? [];

export default insightSharingSlice.reducer;
