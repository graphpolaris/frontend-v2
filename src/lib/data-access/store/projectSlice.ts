import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './store';
import { ProjectModel } from 'ts-common/src/model/webSocket/project';

export interface ProjectState {
  projects: { [id: number]: ProjectModel };
  currentPath: ProjectModel[];
  currentProject: ProjectModel | null;
  isLoading: boolean;
}

const initialState: ProjectState = {
  projects: {},
  currentPath: [],
  currentProject: null,
  isLoading: false,
};

export const projectSlice = createSlice({
  name: 'project',
  initialState,
  reducers: {
    setProjects: (state, action: PayloadAction<ProjectModel[]>) => {
      state.projects = {};
      action.payload.forEach(project => {
        state.projects[project.id] = project;
      });
    },
    addProject: (state, action: PayloadAction<ProjectModel>) => {
      state.projects[action.payload.id] = action.payload;
    },
    updateProject: (state, action: PayloadAction<ProjectModel>) => {
      state.projects[action.payload.id] = action.payload;
    },
    deleteProject: (state, action: PayloadAction<number>) => {
      delete state.projects[action.payload];
    },
    setCurrentProject: (state, action: PayloadAction<ProjectModel | null>) => {
      state.currentProject = action.payload;
    },
    addToPath: (state, action: PayloadAction<ProjectModel>) => {
      state.currentPath.push(action.payload);
    },
    setPath: (state, action: PayloadAction<ProjectModel[]>) => {
      state.currentPath = action.payload;
    },
    goBackInPath: state => {
      state.currentPath.pop();
    },
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload;
    },
  },
});

export const { setProjects, addProject, updateProject, deleteProject, setCurrentProject, addToPath, setPath, goBackInPath, setLoading } =
  projectSlice.actions;

export const selectProject = (state: RootState) => state.project;

export default projectSlice.reducer;
