import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';
import { DatabaseStatus } from '../broker/wsState';
import { getParam, setParam, URLParams } from '../api/url';
import { cloneDeep, isEqual } from 'lodash-es';
import {
  CountQueryResultFromBackend,
  defaultGraph,
  Query,
  QueryBuilderSettings,
  QueryGraphEdgeHandle,
  QueryMultiGraph,
  QueryState,
  QueryUnionType,
  SaveState,
  SaveStateAuthorizationHeaders,
} from 'ts-common';
import { QueryMultiGraphology } from '@/lib/querybuilder';
import { MultiGraph } from 'graphology';
import objectHash from 'object-hash';

/** Message format of the error message from the backend */
export type ErrorMessage = {
  errorStatus: any;
  queryID: any;
};

/** Cache type */
export type SessionCacheI = {
  currentSaveState?: string; // id of the current save state
  saveStates: Record<string, SaveState>;
  saveStatesAuthorization: Record<string, SaveStateAuthorizationHeaders>;
  fetchingSaveStates: boolean;
  testedSaveState: Record<string, DatabaseStatus>;
};

const newStateAuthorizationHeaders: SaveStateAuthorizationHeaders = {
  query: { W: true, R: true },
  database: { W: true, R: true },
  visualization: { W: true, R: true },
  schema: { W: true, R: true },
  savestate: { W: true, R: true },
};

// Define the initial state using that type
export const initialState: SessionCacheI = {
  currentSaveState: undefined,
  saveStates: {},
  saveStatesAuthorization: {},
  fetchingSaveStates: true, // default to true to prevent flashing of the UI
  testedSaveState: {},
};

export const sessionSlice = createSlice({
  name: 'session',
  initialState: initialState,
  reducers: {
    setFetchingSaveStates: (state: SessionCacheI, action: PayloadAction<boolean>) => {
      state.fetchingSaveStates = action.payload;
    },
    selectSaveState: (state: SessionCacheI, action: PayloadAction<string | undefined>) => {
      if (action.payload && action.payload in state.saveStates) {
        state.currentSaveState = action.payload;
      } else {
        console.error('Save state not found');
        state.currentSaveState = undefined;
      }
      setParam(URLParams.saveState, action.payload);
    },
    updateSelectedSaveState: (state: SessionCacheI, action: PayloadAction<SaveState>) => {
      if (state.currentSaveState === action.payload.id && state.currentSaveState) state.saveStates[state.currentSaveState] = action.payload;
    },
    updateSaveStateList: (state: SessionCacheI, action: PayloadAction<SaveState[]>) => {
      // Does NOT clear the states, just adds in new data
      const newState: Record<string, SaveState> = {};
      if (!action.payload) return;
      action.payload.forEach((ss: SaveState) => {
        newState[ss.id] = ss;
      });
      state.saveStates = { ...state.saveStates, ...newState };
      const paramSaveState = getParam(URLParams.saveState);
      if (!state.currentSaveState) {
        if (paramSaveState && paramSaveState in state.saveStates) {
          state.currentSaveState = paramSaveState;
        } else if (Object.keys(state.saveStates).length > 0) {
          state.currentSaveState = Object.keys(state.saveStates)[0];
        } else state.currentSaveState = undefined;
      }
    },
    setSaveStateList: (state: SessionCacheI, action: PayloadAction<SaveState[]>) => {
      // Clears the states and puts in new data
      const newState: Record<string, SaveState> = {};
      action.payload.forEach(ss => {
        newState[ss.id] = ss;
      });
      state.saveStates = newState;
      const paramSaveState = getParam(URLParams.saveState);
      if (!state.currentSaveState) {
        if (paramSaveState && paramSaveState in state.saveStates) {
          state.currentSaveState = paramSaveState;
        } else if (Object.keys(state.saveStates).length > 0) {
          state.currentSaveState = Object.keys(state.saveStates)[0];
        } else state.currentSaveState = undefined;
      }
    },
    addSaveState: (state: SessionCacheI, action: PayloadAction<SaveState>) => {
      if (state.saveStates === undefined) state.saveStates = {};
      state.saveStates[action.payload.id] = action.payload;
      state.currentSaveState = action.payload.id;
      if (!state.saveStatesAuthorization[action.payload.id]) {
        state.saveStatesAuthorization[action.payload.id] = cloneDeep(newStateAuthorizationHeaders);
      }
    },
    deleteSaveState: (state: SessionCacheI, action: PayloadAction<string>) => {
      delete state.saveStates[action.payload];
      delete state.saveStatesAuthorization[action.payload];
      if (state.currentSaveState === action.payload) {
        if (Object.keys(state.saveStates).length > 0) state.currentSaveState = Object.keys(state.saveStates)[0];
        else state.currentSaveState = undefined;
      }
    },
    testedSaveState: (state: SessionCacheI, action: PayloadAction<{ saveStateID: string; status: DatabaseStatus }>) => {
      state.testedSaveState = { ...state.testedSaveState, [action.payload.saveStateID]: action.payload.status };
    },
    setStateAuthorization: (state: SessionCacheI, action: PayloadAction<{ id: string; authorization: SaveStateAuthorizationHeaders }>) => {
      state.saveStatesAuthorization[action.payload.id] = action.payload.authorization;
    },
    deleteAuthorization: (state: SessionCacheI, action: PayloadAction<string>) => {
      delete state.saveStatesAuthorization[action.payload];
    },
    setQuerybuilderGraph: (state: SessionCacheI, action: PayloadAction<QueryMultiGraph>) => {
      const activeQuery = getActiveQuery(state);
      if (activeQuery) {
        activeQuery.graph = {
          ...action.payload,
          nodeCounts: activeQuery.graph.nodeCounts, // ensure visual query building does not get rid of statistics information from the backend
        };
      }
    },
    setQuerybuilderSettings: (state: SessionCacheI, action: PayloadAction<QueryBuilderSettings>) => {
      const activeQuery = getActiveQuery(state);
      if (activeQuery) {
        activeQuery.settings = action.payload;
      }
    },
    attributeShownToggle: (state: SessionCacheI, action: PayloadAction<QueryGraphEdgeHandle>) => {
      const activeQuery = getActiveQuery(state);
      if (activeQuery) {
        const existing = activeQuery.attributesBeingShown.findIndex(a => isEqual(a, action.payload));
        if (existing === -1) {
          activeQuery.attributesBeingShown.push(action.payload);
        } else {
          activeQuery.attributesBeingShown.splice(existing, 1);
        }
      }
    },
    setQueryUnionType: (state: SessionCacheI, action: PayloadAction<{ nodeId: string; unionType: QueryUnionType }>) => {
      const activeQuery = getActiveQuery(state);
      if (activeQuery) {
        if (activeQuery.settings.unionTypes == null) {
          activeQuery.settings.unionTypes = {};
        }
        activeQuery.settings.unionTypes[action.payload.nodeId] = action.payload.unionType;
      }
    },
    setQueryName: (state: SessionCacheI, action: PayloadAction<string>) => {
      const activeQuery = getActiveQuery(state);
      if (activeQuery) {
        activeQuery.name = action.payload;
      }
    },
    addNewQuery: (state: SessionCacheI, payload: PayloadAction<Query>) => {
      if (!payload.payload.id) {
        console.error('No query id found');
        return;
      }

      const activeSS = getActiveSaveState(state);
      if (!activeSS) {
        console.error('No active save state found');
        return;
      }

      activeSS.queryStates.openQueryArray.push(payload.payload);
      activeSS.queryStates.activeQueryId = payload.payload.id;
    },
    removeQueryByID: (state: SessionCacheI, action: PayloadAction<number>) => {
      const activeSS = getActiveSaveState(state);
      if (!activeSS) {
        console.error('No active save state found');
        return;
      }
      activeSS.queryStates.openQueryArray = activeSS.queryStates.openQueryArray.filter(query => query.id !== action.payload);
      if (activeSS.queryStates.activeQueryId === action.payload) {
        // Set the active query to the first query if the active query was removed
        const availableQuery = activeSS.queryStates.openQueryArray.find(query => query.id !== action.payload);
        activeSS.queryStates.activeQueryId =
          availableQuery && availableQuery.id ? availableQuery.id : activeSS.queryStates.openQueryArray[0]?.id || -1;
        if (activeSS.queryStates.activeQueryId === -1) {
          console.error('No active query found 1');
        }
      }
    },
    setActiveQueryID: (state: SessionCacheI, action: PayloadAction<number>) => {
      const activeSS = getActiveSaveState(state);
      if (!activeSS) {
        console.error('No active save state found');
        return;
      }
      if (activeSS.queryStates.openQueryArray.find(query => query.id === action.payload) === undefined) {
        const availableQuery = activeSS.queryStates.openQueryArray.find(query => query.id !== action.payload);
        activeSS.queryStates.activeQueryId = availableQuery && availableQuery.id ? availableQuery.id : -1;
        console.error('QueryID not found', action.payload, activeSS.queryStates.openQueryArray);
        return;
      }
      activeSS.queryStates.activeQueryId = action.payload;
    },
    setQueryState: (state: SessionCacheI, action: PayloadAction<QueryState>) => {
      const activeSS = getActiveSaveState(state);
      if (!activeSS) {
        console.error('No active save state found');
        return;
      }

      activeSS.queryStates = action.payload;
    },
    setQueryNodeCounts: (state: SessionCacheI, action: PayloadAction<CountQueryResultFromBackend>) => {
      const activeQuery = getActiveQuery(state);
      if (activeQuery) {
        activeQuery.graph.nodeCounts = action.payload;
      }
    },
    /**
     * Sets the querybuilder nodes, settings, and attributes being shown,
     * if the payload contains the required information.
     * @param {QueryBuilderState} action.payload the payload with the new state
     */
    setQuerybuilderNodes: (state: SessionCacheI, action: PayloadAction<Query>) => {
      const activeQuery = getActiveQuery(state);

      if (activeQuery && action.payload.graph?.nodes && action.payload.graph?.edges) {
        activeQuery.graph = action.payload.graph;
        activeQuery.settings = action.payload.settings;
        activeQuery.attributesBeingShown = action.payload.attributesBeingShown || [];
      }
    },
    clearQB: (state: SessionCacheI) => {
      const activeQuery = getActiveQuery(state);
      if (activeQuery) {
        activeQuery.graph = defaultGraph();
        activeQuery.settings = {
          limit: 500,
          depth: { min: 1, max: 1 },
          layout: 'manual',
          autocompleteRelation: true,
          unionTypes: {},
        };
        activeQuery.attributesBeingShown = [];
      } else {
        console.error('No active query found 2');
      }
    },
  },
});

export const {
  selectSaveState,
  deleteSaveState,
  updateSaveStateList,
  setSaveStateList,
  addSaveState,
  testedSaveState,
  setFetchingSaveStates,
  updateSelectedSaveState,
  setStateAuthorization,
  deleteAuthorization,
  setQuerybuilderGraph,
  setQuerybuilderSettings,
  setActiveQueryID,
  addNewQuery,
  setQueryName,
  clearQB,
  removeQueryByID,
  setQueryState,
  setQueryNodeCounts,
  setQuerybuilderNodes,
  attributeShownToggle,
  setQueryUnionType,
} = sessionSlice.actions;

function getActiveSaveState(sessionCache: SessionCacheI): SaveState | undefined {
  return sessionCache.saveStates?.[sessionCache.currentSaveState!];
}
function getActiveQuery(state: SessionCacheI): Query | undefined {
  const ss = getActiveSaveState(state);
  if (!ss || !ss.queryStates || !ss.queryStates.activeQueryId || ss.queryStates.activeQueryId === -1) {
    return; // Return an empty object if no active query exists
  }
  const activeQuery = ss.queryStates.openQueryArray.find(q => q.id === ss.queryStates.activeQueryId);
  if (!activeQuery) {
    console.error('No active query found, returning first query');
    if (ss.queryStates.openQueryArray.length === 0) {
      console.error('No queries found');
      return;
    }
    if (ss.queryStates.openQueryArray[0]?.id == null || ss.queryStates.openQueryArray[0]?.id === -1) {
      console.error('First query has no id');
      return;
    }
    sessionSlice.actions.setActiveQueryID(ss.queryStates.openQueryArray[0].id);
    return ss.queryStates.openQueryArray[0];
  }
  return activeQuery;
}

/** Select the querybuilder nodes in serialized fromat */
export const toQuerybuilderGraphology = (graph: QueryMultiGraph): QueryMultiGraphology => {
  const ret = new QueryMultiGraphology();
  ret.import(MultiGraph.from(graph).export());
  return ret;
};

// Other code such as selectors can use the imported `RootState` type
export const sessionCacheState = (state: RootState) => state.sessionCache;
export const activeSaveState = (state: RootState): SaveState | undefined => getActiveSaveState(state.sessionCache);
export const activeSaveStateAuthorization = (state: RootState): SaveStateAuthorizationHeaders =>
  state.sessionCache.saveStatesAuthorization?.[state.sessionCache.currentSaveState!] || newStateAuthorizationHeaders;

export default sessionSlice.reducer;

export const activeSaveStateQuery = (state: RootState): Query | undefined => getActiveQuery(state.sessionCache);

export const setQuerybuilderGraphology = (payload: QueryMultiGraphology) => {
  return sessionSlice.actions.setQuerybuilderGraph(payload.export());
};

/** Select the querybuilder nodes and convert it to a graphology object */
export const selectQuerybuilderHash = (state: RootState): string => {
  const activeQuery = activeSaveStateQuery(state);

  if (!activeQuery?.graph) {
    return ''; // Return an empty string if no active graph exists
  }

  function isEmptyLogicPill(node: any) {
    if (node == null || node.attributes.inputs == null) return false;
    const input = Object.values(node.attributes.inputs)[0];
    return node.attributes.type == 'logic' && input != null && input == node.attributes.logic.input.default;
  }

  const hashedNodes = activeQuery.graph.nodes
    .filter(n => {
      if (isEmptyLogicPill(n)) return false;
      return true;
    })
    .map(n => {
      const node = { ...n };
      if (n?.attributes) {
        const newAttributes = { ...n?.attributes };
        newAttributes.x = 0;
        newAttributes.y = 0;
        newAttributes.height = 0;
        newAttributes.width = 0;
        node.attributes = newAttributes;
      }
      return node;
    });

  const ret = {
    nodes: hashedNodes,
    edges: activeQuery.graph.edges
      .filter(e => {
        const targetNode = activeQuery.graph.nodes.find(n => n.key == e.target);
        if (isEmptyLogicPill(targetNode)) return false;
        return true;
      })
      .map(n => ({
        key: n.key,
        source: n.source,
        target: n.target,
      })),
    settings: activeQuery.settings,
  };
  return objectHash(ret);
};
