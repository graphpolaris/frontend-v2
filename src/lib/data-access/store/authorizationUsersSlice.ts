import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';
import { UserPolicy } from 'ts-common';

export interface PolicyUsersState {
  users: UserPolicy[];
}

//TODO !FIXME: add middleware to fetch users from backend
const initialState: PolicyUsersState = {
  users: [
    { name: 'Scheper. W', email: 'user1@companyA.com', type: 'creator' },
    { name: 'Smit. S', email: 'user2@companyB.com', type: 'viewer' },
    { name: 'De Jong. B', email: 'user3@companyC.com', type: 'creator' },
  ],
};

export const policyUsersSlice = createSlice({
  name: 'policyUsers',
  initialState,
  reducers: {
    setUsersPolicy: (state, action: PayloadAction<PolicyUsersState>) => {
      return action.payload;
    },
  },
});

export const { setUsersPolicy } = policyUsersSlice.actions;
export default policyUsersSlice.reducer;
export const selectPolicyState = (state: RootState) => state.policyUsers;
