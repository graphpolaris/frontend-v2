import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { AppDispatch, RootState } from './store';
import { GraphQueryResultMetaFromBackend, NodeAttributes, QueryStatusResult } from 'ts-common';
import { setQueryNodeCounts } from './sessionSlice';
export type UniqueEdge = {
  attributes: NodeAttributes;
  source: string;
  target: string;
  count: number;
};

// Define a type for the slice state
export type GraphQueryResult = GraphQueryResultMetaFromBackend & {
  queryingBackend: boolean;
  error: boolean;
};

// Define the initial state using that type
export const initialState: GraphQueryResult = {
  metaData: {
    nodes: { count: 0, labels: [], types: {} },
    edges: { count: 0, labels: [], types: {} },
    topological: { density: 0, self_loops: 0 },
  },
  nodes: [],
  edges: [],
  queryingBackend: false,
  error: false,
  nodeCounts: { updatedAt: 0 },
};

export function parseValue(value: number | string | Array<any> | object) {
  switch (value.constructor) {
    case Number:
      return (value as number).toFixed(2);
    case String:
      return value as string;
    case Array:
      return (value as Array<any>).map(String).join(', ');
    case Object:
      return JSON.stringify(value as object);
  }

  return null;
}

export const graphQueryResultSlice = createSlice({
  name: 'graphQueryResult',
  initialState,
  reducers: {
    setNewGraphQueryResult: (state, action: PayloadAction<QueryStatusResult>) => {
      // Assign new state
      state.metaData = action.payload.result.payload.metaData;
      state.nodes = action.payload.result.payload.nodes;
      state.edges = action.payload.result.payload.edges;
      state.nodeCounts = action.payload.result.payload.nodeCounts;
      state.queryingBackend = false;
      state.error = false;
    },
    resetGraphQueryResults: (state, action: PayloadAction<undefined | { error: boolean }>) => {
      // Assign new state
      state.metaData = {
        nodes: { count: 0, labels: [], types: {} },
        edges: { count: 0, labels: [], types: {} },
        topological: { density: 0, self_loops: 0 },
      };
      state.nodes = [];
      state.edges = [];
      state.nodeCounts = { updatedAt: 0 };
      state.queryingBackend = false;
      state.error = action.payload?.error ?? false;
    },
    queryingBackend: (state, action: PayloadAction<boolean | undefined>) => {
      state.queryingBackend = action.payload ?? true;
      state.error = false;
    },
  },
});

export const { resetGraphQueryResults, queryingBackend } = graphQueryResultSlice.actions;

/**
 * Asynchronously sets a new graph query result.
 * Prefer to use this then the synchronous version since if the result is very large,
 *  the entire website will freeze while graphQueryBackend2graphQuery runs.
 *
 * This function dispatches an action to update the graph query result
 * in the store with the provided payload.
 *
 * @param payload - The result of the graph query to be set.
 * @param dispatcher - The dispatch function to trigger the action.
 */
export const asyncSetNewGraphQueryResult = async (payload: QueryStatusResult, dispatcher: AppDispatch) => {
  dispatcher(setQueryNodeCounts(payload.result.payload.nodeCounts));
  dispatcher(graphQueryResultSlice.actions.setNewGraphQueryResult(payload));
};

// Other code such as selectors can use the imported `RootState` type
export const selectGraphQueryResult = (state: RootState) => state.graphQueryResult;
export const selectGraphQueryResultNodes = (state: RootState) => state.graphQueryResult.nodes;
export const selectGraphQueryResultLinks = (state: RootState) => state.graphQueryResult.edges;
export const selectGraphQueryResultMetaData = (state: RootState) => state.graphQueryResult.metaData;

export default graphQueryResultSlice.reducer;
