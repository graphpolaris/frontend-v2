import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';
import { NodeQueryResult, EdgeQueryResult } from 'ts-common';

export type HoverStateI = { [id: string]: any };

export type SelectionStateI =
  | {
      selectionType: 'node';
      contentType: 'data';
      content: NodeQueryResult[];
    }
  | {
      // TODO: other selection types
      selectionType: 'relation';
      contentType: 'data';
      content: EdgeQueryResult[];
    };

export type FocusStateI = {
  focusType: 'schema' | 'query' | 'visualization';
};

// Define the initial state using that type
export type InteractionsType = {
  hover?: HoverStateI;
  selection?: SelectionStateI;
  focus?: FocusStateI;
};

export const initialState: InteractionsType = {
  hover: undefined,
  selection: undefined,
  focus: { focusType: 'visualization' },
};

export const interactionSlice = createSlice({
  name: 'interaction',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    addHover: (state, action: PayloadAction<HoverStateI | undefined>) => {
      state.hover = action.payload;
    },
    unSelect: state => {
      state.selection = undefined;
    },
    resultSetSelection: (
      state,
      action: PayloadAction<{ selectionType?: 'node' | 'relation'; content: NodeQueryResult[] | EdgeQueryResult[] }>,
    ) => {
      if (!action.payload.selectionType || action.payload.content.length === 0) {
        state.selection = undefined;
      } else if (action.payload.selectionType === 'node') {
        state.selection = {
          selectionType: 'node',
          contentType: 'data',
          content: action.payload.content as NodeQueryResult[],
        };
      } else if (action.payload.selectionType === 'relation') {
        state.selection = {
          selectionType: 'relation',
          contentType: 'data',
          content: action.payload.content as EdgeQueryResult[],
        };
      }
    },

    resultSetFocus: (state, action: PayloadAction<FocusStateI | undefined>) => {
      state.focus = action.payload;
    },
  },
});

export const { addHover, unSelect, resultSetSelection, resultSetFocus } = interactionSlice.actions;

export const interactionState = (state: RootState) => state.interaction;
export const selectionState = (state: RootState) => state.interaction.selection;
export const focusState = (state: RootState) => state.interaction.focus;

export default interactionSlice.reducer;
