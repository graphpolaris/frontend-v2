import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';
import { VisualizationSettingsType } from '../../vis/common';
import { isEqual } from 'lodash-es';
import { v4 as uuidv4 } from 'uuid';

export type VisStateSettings = VisualizationSettingsType[];
export type VisState = {
  id?: number;
  activeVisualizationIndex: number;
  openVisualizationArray: VisStateSettings;
};

export const initialState: VisState = {
  activeVisualizationIndex: -1,
  openVisualizationArray: [],
};

export const visualizationSlice = createSlice({
  name: 'visualization',
  initialState,
  reducers: {
    removeVisualization: (state, action: PayloadAction<number | undefined>) => {
      const index = action.payload ?? state.activeVisualizationIndex;

      if (index >= 0 && index < state.openVisualizationArray.length) {
        state.openVisualizationArray.splice(index, 1);

        if (state.openVisualizationArray.length === 0) {
          state.activeVisualizationIndex = -1;
        } else if (state.activeVisualizationIndex >= state.openVisualizationArray.length) {
          state.activeVisualizationIndex = state.openVisualizationArray.length - 1;
        } else if (state.activeVisualizationIndex === index) {
          state.activeVisualizationIndex = index < state.openVisualizationArray.length ? index : index - 1;
        } else if (state.activeVisualizationIndex > index) {
          state.activeVisualizationIndex--;
        }
      }
    },
    setActiveVisualization: (state, action: PayloadAction<number>) => {
      if (action.payload >= -1 && action.payload < state.openVisualizationArray.length) {
        state.activeVisualizationIndex = action.payload;
      }
    },
    setVisualizationState: (state, action: PayloadAction<VisState>) => {
      if (action.payload.activeVisualizationIndex !== undefined && !isEqual(action.payload, state)) {
        state.openVisualizationArray = (action.payload.openVisualizationArray || []).map(item => ({
          ...item,
          uuid: item.uuid || uuidv4(),
        }));
        state.activeVisualizationIndex = Math.min(action.payload.activeVisualizationIndex, state.openVisualizationArray.length - 1);
      }
    },
    updateVisualization: (state, action: PayloadAction<{ id: number; settings: VisualizationSettingsType }>) => {
      const { id, settings } = action.payload;
      if (id >= 0 && id < state.openVisualizationArray.length) {
        state.openVisualizationArray[id] = settings;
      }
    },
    addVisualization: (state, action: PayloadAction<VisualizationSettingsType>) => {
      const newItem = {
        ...action.payload,
        uuid: action.payload.uuid || uuidv4(),
      };
      state.openVisualizationArray.push(newItem);

      // state.openVisualizationArray.push(action.payload);
      state.activeVisualizationIndex = state.openVisualizationArray.length - 1;
    },
    updateActiveVisualization: (state, action: PayloadAction<VisualizationSettingsType>) => {
      if (state.activeVisualizationIndex >= 0) {
        state.openVisualizationArray[state.activeVisualizationIndex] = action.payload;
      }
    },
    updateActiveVisualizationAttributes: (state, action: PayloadAction<Record<string, any>>) => {
      if (state.activeVisualizationIndex >= 0) {
        state.openVisualizationArray[state.activeVisualizationIndex] = {
          ...state.openVisualizationArray[state.activeVisualizationIndex],
          ...action.payload,
        };
      }
    },
    reorderVisState: (
      state,
      action: PayloadAction<{
        id: number;
        newPosition: number;
      }>,
    ) => {
      const { id, newPosition } = action.payload;
      if (id >= 0 && id < state.openVisualizationArray.length && newPosition >= 0 && newPosition < state.openVisualizationArray.length) {
        const settingsCopy = [...state.openVisualizationArray];
        const [movedItem] = settingsCopy.splice(id, 1);
        settingsCopy.splice(newPosition, 0, movedItem);

        state.openVisualizationArray = settingsCopy;

        if (state.activeVisualizationIndex === id) {
          state.activeVisualizationIndex = newPosition;
        } else if (state.activeVisualizationIndex > id && state.activeVisualizationIndex <= newPosition) {
          state.activeVisualizationIndex--;
        } else if (state.activeVisualizationIndex < id && state.activeVisualizationIndex >= newPosition) {
          state.activeVisualizationIndex++;
        }
      }
    },
  },
});

export const {
  removeVisualization,
  setActiveVisualization,
  updateActiveVisualization,
  setVisualizationState,
  updateVisualization,
  reorderVisState,
  addVisualization,
  updateActiveVisualizationAttributes,
} = visualizationSlice.actions;

export const visualizationState = (state: RootState) => state.visualize;
export const visualizationAllSettings = (state: RootState) => state.visualize.openVisualizationArray;
export const visualizationActive = (state: RootState) =>
  state.visualize.activeVisualizationIndex !== -1
    ? state.visualize.openVisualizationArray?.[state.visualize.activeVisualizationIndex]
    : undefined;
export default visualizationSlice.reducer;
