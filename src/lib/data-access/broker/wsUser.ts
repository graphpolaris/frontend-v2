import { ResponseCallback, StateSetPolicyRequest, WsFrontendCall, wsKeys, wsReturnKey, wsSubKeys } from 'ts-common';
import { Broker } from './broker';

export const wsShareSaveState: WsFrontendCall<StateSetPolicyRequest, wsReturnKey.shareSaveState> = (request, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.user,
      subKey: wsSubKeys.policyCheck,
      body: request,
    },
    callback,
  );
};

export const wsUserGetPolicy: WsFrontendCall<null, wsReturnKey.userPolicy> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.user,
      subKey: wsSubKeys.getPolicy,
      body: params,
    },
    callback,
  );
};

export function wsReconnectSubscription(callback: ResponseCallback<wsReturnKey.reconnect>) {
  const id = Broker.instance().subscribe(callback, wsReturnKey.reconnect);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.reconnect, id);
  };
}
