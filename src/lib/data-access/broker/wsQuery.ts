// All database related API calls

import { Broker } from './broker';
import { ML, Query, ResponseCallback, WsFrontendCall, wsKeys, wsReturnKey, wsSubKeys } from 'ts-common';

export const wsQueryRequest: WsFrontendCall<{ saveStateID: string; ml: ML; queryID: number; useCached: boolean }> = params => {
  const mlEnabled = Object.entries(params.ml)
    .filter(([_, value]) => value.enabled)
    .map(([, value]) => value);
  Broker.instance().sendMessage({
    key: wsKeys.query,
    subKey: wsSubKeys.get,
    body: {
      saveStateID: params.saveStateID,
      queryID: params.queryID,
      ml: mlEnabled,
      useCached: params.useCached,
    },
  });
};
export const wsQueryErrorSubscription = (callback: ResponseCallback<wsReturnKey.queryStatusError>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.queryStatusError);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.queryStatusError, id);
  };
};

export const wsAddQuery: WsFrontendCall<{ saveStateID: string }, wsReturnKey.queryAdd> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.query,
      subKey: wsSubKeys.create,
      body: {
        saveStateID: params.saveStateID,
      },
    },
    callback,
  );
};
export const wsAddQuerySubscription = (callback: ResponseCallback<wsReturnKey.queryAdd>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.queryAdd);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.queryAdd, id);
  };
};

export const wsUpdateQuery: WsFrontendCall<{ saveStateID: string; query: Query }, wsReturnKey.queryUpdate> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.query,
      subKey: wsSubKeys.update,
      body: {
        saveStateID: params.saveStateID,
        query: params.query,
      },
    },
    callback,
  );
};

export const wsDeleteQuery: WsFrontendCall<{ saveStateID: string; queryID: number }, wsReturnKey.queryDelete> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.query,
      subKey: wsSubKeys.delete,
      body: {
        saveStateID: params.saveStateID,
        queryID: params.queryID,
      },
    },
    callback,
  );
};
export const wsDeleteQuerySubscription = (callback: ResponseCallback<wsReturnKey.queryDelete>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.queryDelete);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.queryDelete, id);
  };
};

export const wsManualQueryRequest: WsFrontendCall<{ query: string }> = params => {
  Broker.instance().sendMessage({
    key: wsKeys.query,
    subKey: wsSubKeys.manual,
    body: JSON.stringify({ query: params.query }),
  });
};

export function wsQueryTranslationSubscription(callback: ResponseCallback<wsReturnKey.queryStatusTranslationResult>) {
  const id = Broker.instance().subscribe(callback, wsReturnKey.queryStatusTranslationResult);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.queryStatusTranslationResult, id);
  };
}

export function wsQuerySubscription(callback: ResponseCallback<wsReturnKey.queryStatusResult>) {
  const id = Broker.instance().subscribe(callback, wsReturnKey.queryStatusResult);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.queryStatusResult, id);
  };
}
