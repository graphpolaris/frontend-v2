/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { getEnvVariable } from '@/config';
import { UseIsAuthorizedState } from '../store/authSlice';
import { WsMessageBackend2Frontend, WsMessageBody, wsReturnKeyWithML } from 'ts-common';
import { v4 as uuidv4 } from 'uuid';

export type BrokerCallbackFunction = (data: any, status: string, routingKey?: string) => boolean | void;

let keepAlivePing: ReturnType<typeof setInterval>;

/**
 * A broker that handles incoming messages from the backend.
 * It works with routingkeys, a listener can subscribe to messages from the backend with a specific routingkey.
 * Possible routingkeys:
 * - query_result:              Contains an object with nodes and edges or a numerical result.
 * - queryTranslation_result:  Contains the query translated to the database language.
 * - schema_result:             Contains the schema of the users database.
 * - query_status_update:       Contains an update to if a query is being executed.
 * - query_database_error:      Contains the error received from the database.
 * - query_sent:                Contains the message that a query has been send.
 * - gsa_node_result:           Contains a node that has the data for the graph-schema-analytics
 * - gsa_edge_result:           Contains a edge that has the data for the graph-schema-analytics
 */
export class Broker {
  private static singletonInstance: Broker;

  private listeners: Record<string, Record<string, BrokerCallbackFunction>> = {};
  private catchAllListener: BrokerCallbackFunction | undefined;
  private callbackListeners: Record<string, BrokerCallbackFunction> = {};

  private webSocket: WebSocket | undefined;
  private url: string;
  private connected: boolean;
  private authHeader: UseIsAuthorizedState | undefined;

  /** mostRecentMessages is a dictionary with <routingkey, messageObject>. It stores the most recent message for that routingkey. */
  private mostRecentMessages: Partial<Record<wsReturnKeyWithML, WsMessageBackend2Frontend>> = {};

  /** Get the singleton instance of the Broker. */
  public static instance(): Broker {
    if (!this.singletonInstance) this.singletonInstance = new Broker(String(getEnvVariable('BACKEND_WSS_URL')));
    return this.singletonInstance;
  }

  /**
   * Subscribe a listener to messages with the specified routingKey.
   * @param {Function} newListener The listener to subscribe.
   * @param {string} routingKey The routingkey to subscribe to.
   * @param {boolean} consumeMostRecentMessage If true and there is a message for this routingkey available, notify the new listener. Default true.
   */
  public subscribe(
    newListener: BrokerCallbackFunction,
    routingKey: wsReturnKeyWithML,
    key: string = (Date.now() + Math.floor(Math.random() * 100)).toString(),
    consumeMostRecentMessage: boolean = false,
  ): string {
    if (!this.listeners[routingKey]) this.listeners[routingKey] = {};

    // Don't add a listener twice
    if (!(key in this.listeners[routingKey])) {
      this.listeners[routingKey][key] = newListener;

      // Consume the most recent message
      if (consumeMostRecentMessage && routingKey in this.mostRecentMessages) {
        newListener(this.mostRecentMessages[routingKey]!.value, this.mostRecentMessages[routingKey]!.status, routingKey);
      }
    }

    return key;
  }

  /**
   * Subscribe a listener to messages with the specified routingKey.
   * @param {Function} newListener The listener to subscribe.
   * @param {string} routingKey The routingkey to subscribe to.
   * @param {boolean} consumeMostRecentMessage If true and there is a message for this routingkey available, notify the new listener. Default true.
   */
  public subscribeDefault(newListener: BrokerCallbackFunction): void {
    this.catchAllListener = newListener;
  }

  /**
   * Unsubscribes a listener from messages with specified routingkey.
   * @param {string} routingKey The routing key to unsubscribe from
   * @param {string} listener key of the listener to unsubscribe.
   */
  public unSubscribe(routingKey: wsReturnKeyWithML, key: string): void {
    if (this.listeners[routingKey] && key in this.listeners[routingKey]) {
      delete this.listeners[routingKey][key];
    }
  }

  /**
   * Unsubscribes the catch all listener from messages
   */
  public unSubscribeDefault(): void {
    this.catchAllListener = undefined;
  }

  /**
   * Unsubscribes all listeners from messages with specified routingkey.
   * @param {string} routingKey The routing key to unsubscribe from
   */
  public unSubscribeAll(routingKey: wsReturnKeyWithML): void {
    this.listeners[routingKey] = {};
  }

  /** @param domain The domain to make the websocket connection with. */
  public constructor(domain: string) {
    this.url = domain;
    this.connected = false;
  }

  public useAuth(authHeader: UseIsAuthorizedState): Broker {
    this.authHeader = authHeader;
    return this;
  }

  /**
   * Create a websocket to the given URL.
   * @param {string} URL is the URL to which the websocket connection is opened.
   */
  public connect(onOpen: () => void): void {
    // If there already is already a current websocket connection, close it first.
    console.debug('WS connection! Connecting to ' + this.url);
    if (this.webSocket) {
      console.warn('Closing old websocket connection');
      this.close();
    }

    const params = new URLSearchParams(window.location.search);
    // Most of these parameters are only really used in DEV
    const impersonateID = params.get('impersonateID');
    if (impersonateID) params.set('impersonateID', impersonateID);

    if (this.authHeader?.roomID) params.set('roomID', this.authHeader?.roomID ?? '');
    if (this.authHeader?.sessionID) params.set('sessionID', this.authHeader.sessionID);

    // if (this.saveStateID) params.set('saveStateID', this.saveStateID ?? '');
    // if (this.authHeader?.jwt) params.set('jwt', this.authHeader?.jwt ?? '');
    this.webSocket = new WebSocket(this.url + '?' + params.toString());
    this.webSocket.onopen = () => {
      this.connected = true;
      onOpen();
    };
    this.webSocket.onmessage = this.receiveMessage;
    this.webSocket.onerror = this.onError;
    this.webSocket.onclose = this.onClose;

    if (keepAlivePing != null) clearInterval(keepAlivePing);
    keepAlivePing = setInterval(() => {
      this.webSocket?.send('ping');
    }, 30000);
  }

  /** Closes the current websocket connection. */
  public close = (): void => {
    console.warn('Closing WS for some reason');
    if (this.webSocket) this.webSocket.close();
    this.connected = false;
    this.webSocket = undefined;
  };

  /** @returns A boolean which indicates if there currently is a socket connection. */
  public isConnected = (): boolean => {
    return this.connected;
  };

  public attemptReconnect = () => {
    console.warn('Attempting to reconnect WS');

    if (!this.connected || !this.webSocket || this.webSocket.readyState !== 1) {
      this.connect(() => {
        setTimeout(() => Broker.instance().attemptReconnect(), 5000);
      });
    } else {
      console.log('WS reconnected', this.webSocket?.readyState, this.connected);
    }
  };

  /**
   * Websocket connection close event handler.
   * @param {any} event Contains the event data.
   */
  private onClose(event: any): void {
    console.warn('WS connection was closed from the server side', event);
    if (this.webSocket) this.webSocket.close();
    this.connected = false;
    this.webSocket = undefined;
    setTimeout(() => Broker.instance().attemptReconnect(), 5000);
  }

  public sendMessage(message: Omit<WsMessageBody, 'callID'>, callback?: BrokerCallbackFunction): void {
    console.debug('%cSending WS message: ', 'background: #222; color: #bada55', message);

    const uuid = uuidv4();
    const fullMessage = {
      ...message,
      callID: uuid,
    };

    if (callback) {
      this.callbackListeners[uuid] = callback;
    }

    if (message.body) {
      fullMessage.body = message.body;
    }

    if (this.webSocket && this.connected && this.webSocket.readyState === 1) this.webSocket.send(JSON.stringify(fullMessage));
    else
      this.connect(() => {
        if (this.webSocket && this.connected && this.webSocket.readyState === 1) this.webSocket.send(JSON.stringify(fullMessage));
      });
  }

  /**
   * Websocket connection message event handler. Called if a new message is received through the socket.
   * @param {any} event Contains the event data.
   */
  public receiveMessage = (event: MessageEvent<any>) => {
    if (event.data === 'pong') return;

    const jsonObject: WsMessageBackend2Frontend = JSON.parse(event.data);
    const routingKey = jsonObject.type;
    const data = jsonObject.value;
    const status = jsonObject.status;
    const uuid = jsonObject.callID;

    let stop = false; // check in case there is a specific callback listener and, if its response is true, also call the subscriptions
    this.mostRecentMessages[routingKey] = jsonObject;
    if (uuid in this.callbackListeners) {
      stop = this.callbackListeners[uuid](data, status) !== true;
      console.debug(
        '%c' + routingKey + ` WS response WITH CALLBACK`,
        'background: #222; color: #DBAB2F',
        data,
        this.callbackListeners,
        'stop=',
        stop,
      );
      delete this.callbackListeners[uuid];
    }

    if (!stop && this.listeners[routingKey] && Object.keys(this.listeners[routingKey]).length != 0) {
      if (this.catchAllListener) {
        this.catchAllListener(data, status, routingKey);
      }
      Object.values(this.listeners[routingKey]).forEach(listener => listener(data, status, routingKey));
      console.debug('%c' + routingKey + ` WS response`, 'background: #222; color: #DBAB2F', data);
    }
    // If there are no listeners, log the message
    else if (!stop) {
      if (this.catchAllListener) {
        this.catchAllListener(data, status, routingKey);
        console.debug(routingKey, `catch all used for message with routing key`, data);
      } else {
        console.debug('%c' + routingKey + ` no listeners for message with routing key`, 'background: #663322; color: #DBAB2F', data);
      }
    }
  };

  /**
   * Websocket connection error event handler.
   * @param {any} event contains the event data.
   */
  private onError(event: any): void {
    console.error('WS error', event);
  }

  public checkSessionID(sessionID: string) {
    return this.authHeader?.sessionID == sessionID;
  }
}
