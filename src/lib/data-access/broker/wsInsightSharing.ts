import { InsightModel, InsightRequest, ResponseCallback, WsFrontendCall, wsKeys, wsReturnKey, wsSubKeys } from 'ts-common';
import { Broker } from './broker';

export const wsGetInsights: WsFrontendCall<{ saveStateId: string }, wsReturnKey.insightResults> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.insight,
      subKey: wsSubKeys.getAll,
      body: { saveStateId: params.saveStateId },
    },
    callback,
  );
};

export const wsCreateInsight: WsFrontendCall<InsightRequest, wsReturnKey.insightResult> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.insight,
      subKey: wsSubKeys.create,
      body: params,
    },
    (data: InsightModel, status: string) => {
      if (status === 'Bad Request') {
        console.error('Failed to create insight:', data);
        if (callback) callback(data, status);
        return;
      }

      if (!data || typeof data !== 'object') {
        console.error('Invalid response data', data);
        if (callback) callback(null, 'error');
        return;
      }

      if (!data.type || !data.id) {
        console.error('Missing fields in response', data);
        if (callback) callback(null, 'error');
        return;
      }

      if (callback) callback(data, status);
    },
  );
};

export const wsUpdateInsight: WsFrontendCall<{ id: number; insight: InsightRequest; generateEmail: boolean }, wsReturnKey.insightResult> = (
  params,
  callback,
) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.insight,
      subKey: wsSubKeys.update,
      body: { id: params.id, insight: params.insight, generateEmail: params.generateEmail },
    },
    (data: any, status: string) => {
      if (status === 'Bad Request') {
        console.error('Failed to update insight:', data);
      }
      if (callback) callback(data, status);
    },
  );
};

export const wsDeleteInsight: WsFrontendCall<{ id: number }, wsReturnKey.insightResult> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.insight,
      subKey: wsSubKeys.delete,
      body: { id: params.id },
    },
    (data: any, status: string) => {
      if (status === 'Bad Request') {
        console.error('Failed to delete insight:', data);
      }
      if (callback) callback(data, status);
    },
  );
};

export function wsInsightSubscription(callback: ResponseCallback<wsReturnKey.insightResult>) {
  const id = Broker.instance().subscribe(callback, wsReturnKey.insightResult);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.insightResult, id);
  };
}
