import { ProjectModel, ProjectRequest } from 'ts-common/src/model/webSocket/project';
import { Broker } from './broker';
import { SaveState, wsKeys, wsReturnKey, wsSubKeys } from 'ts-common';

type GetProjectResponse = (data: ProjectModel, status: string) => void;
type GetProjectsResponse = (data: ProjectModel[], status: string) => void;

function transformToProjectModel(saveStates: SaveState[]): ProjectModel[] {
  return saveStates
    .filter(ss => ss.project && ss.project.id)
    .map(
      ss =>
        ({
          id: ss.project!.id,
          name: ss.project!.name,
          parentId: ss.project!.parentId ? ss.project!.parentId : null,
          saveStates: [],
          children: [],
          sub_projects: [],
          createdAt: ss.createdAt,
          updatedAt: ss.updatedAt,
        }) as ProjectModel,
    );
}

export function wsGetProject(userID: number, parentID: number, callback?: GetProjectResponse) {
  Broker.instance().sendMessage(
    {
      key: wsKeys.project,
      subKey: wsSubKeys.get,
      body: { userID: userID, parentID: parentID },
    },
    (data: ProjectModel, status: string) => {
      if (callback) {
        callback(data, status);
      }
    },
  );
}

export function wsGetProjects(userID: number, parentID: number | null, callback?: GetProjectsResponse) {
  Broker.instance().sendMessage(
    {
      key: wsKeys.project,
      subKey: wsSubKeys.getAll,
      body: { parentID: parentID },
    },
    (data: ProjectModel[], status: string) => {
      if (callback) callback(data, status);
    },
  );
}

export function wsCreateProject(project: ProjectRequest, callback?: GetProjectResponse) {
  Broker.instance().sendMessage(
    {
      key: wsKeys.project,
      subKey: wsSubKeys.create,
      body: project,
    },
    (data: ProjectModel, status: string) => {
      if (callback) {
        callback(data, status);
      }
    },
  );
}

export function wsUpdateProject(id: number, project: ProjectRequest, callback?: GetProjectResponse) {
  Broker.instance().sendMessage(
    {
      key: wsKeys.project,
      subKey: wsSubKeys.update,
      body: { queryID: id, ...project },
    },
    (data: SaveState, status: string) => {
      const projects = transformToProjectModel([data]);
      if (callback && projects.length > 0) {
        callback(projects[0], status);
      } else if (callback) {
        callback(
          {
            id: -1,
            name: '',
            parentId: null,
            createdAt: '',
            updatedAt: '',
          },
          'error',
        );
      }
    },
  );
}

export function wsDeleteProject(id: number, callback?: (data: { success: boolean }, status: string) => void) {
  Broker.instance().sendMessage(
    {
      key: wsKeys.project,
      subKey: wsSubKeys.delete,
      body: { id: id },
    },
    callback,
  );
}

export function wsProjectSubscription(callback: (data: ProjectModel, status: string) => void) {
  const id = Broker.instance().subscribe((data: SaveState, status: string) => {
    const projects = transformToProjectModel([data]);
    if (projects.length > 0) {
      callback(projects[0], status);
    }
  }, wsReturnKey.project);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.project, id);
  };
}
