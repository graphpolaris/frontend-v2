// All database related API calls

import { ResponseCallback, SchemaRequestMessage, WsFrontendCall, wsKeys, wsReturnKey, wsSubKeys } from 'ts-common';
import { Broker } from './broker';

export const wsSchemaRequest: WsFrontendCall<SchemaRequestMessage, wsReturnKey.schemaResult> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.schema,
      subKey: wsSubKeys.get,
      body: params,
    },
    callback,
  );
};

export const wsSchemaSubscription = (callback: ResponseCallback<wsReturnKey.schemaResult>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.schemaResult);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.schemaResult, id);
  };
};

export const wsSchemaStatsRequest: WsFrontendCall<{ saveStateID: string }> = params => {
  Broker.instance().sendMessage({
    key: wsKeys.schema,
    subKey: wsSubKeys.getSchemaStats,
    body: {
      useCached: false,
      saveStateID: params.saveStateID,
    },
  });
};

export const wsSchemaStatsSubscription = (callback: ResponseCallback<wsReturnKey.schemaStatsResult>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.schemaStatsResult);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.schemaStatsResult, id);
  };
};
