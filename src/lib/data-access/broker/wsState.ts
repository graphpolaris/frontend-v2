import { Broker } from './broker';
import {
  PartialSaveState,
  ResponseCallback,
  SaveState,
  StateSetPolicyRequest,
  WsFrontendCall,
  wsKeys,
  wsReturnKey,
  wsSubKeys,
} from 'ts-common';
import { DatabaseInfo, DatabaseType } from 'ts-common/src/model/webSocket/dbConnection';

export const databaseProtocolMapping: Record<DatabaseType, string[]> = {
  [DatabaseType.Memgraph]: ['bolt://'],
  [DatabaseType.Neo4j]: ['neo4j://', 'neo4j+s://', 'bolt://', 'bolt+s://'],
};

export enum DatabaseStatus {
  untested = 0,
  testing = 1,
  online = 2,
  offline = 3,
}

export const nilUUID = '00000000-0000-0000-0000-000000000000';

export const wsGetState: WsFrontendCall<{ saveStateID: string }, wsReturnKey.saveState> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.state,
      subKey: wsSubKeys.get,
      body: { saveStateID: params.saveStateID },
    },
    callback,
  );
};
export const wsGetStateSubscription = (callback: ResponseCallback<wsReturnKey.saveState>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.saveState);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.saveState, id);
  };
};

export const wsGetStates: WsFrontendCall<null, wsReturnKey.saveStates> = (_, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.state,
      subKey: wsSubKeys.getAll,
      body: null,
    },
    callback,
  );
};
export const wsGetStatesSubscription = (callback: ResponseCallback<wsReturnKey.saveStates>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.saveStates);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.saveStates, id);
  };
};

export const wsCreateState: WsFrontendCall<PartialSaveState, wsReturnKey.saveState> = (request, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.state,
      subKey: wsSubKeys.create,
      body: request,
    },
    callback,
  );
};

export const wsDeleteState: WsFrontendCall<{ saveStateID: string }, wsReturnKey.deleteSaveState> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.state,
      subKey: wsSubKeys.delete,
      body: params,
    },
    callback,
  );
};
export const wsDeleteStateSubscription = (callback: ResponseCallback<wsReturnKey.deleteSaveState>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.deleteSaveState);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.deleteSaveState, id);
  };
};

export const wsTestSaveStateConnection: WsFrontendCall<{ saveStateID: string }, wsReturnKey.testedConnection> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.state,
      subKey: wsSubKeys.testConnection,
      body: { saveStateID: params.saveStateID },
    },
    callback,
  );
};
export const wsTestSaveStateConnectionSubscription = (callback: ResponseCallback<wsReturnKey.testedConnection>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.testedConnection);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.testedConnection, id);
  };
};

export const wsUpdateState: WsFrontendCall<SaveState, wsReturnKey.saveState> = (request, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.state,
      subKey: wsSubKeys.update,
      body: request,
    },
    callback,
  );
};

export const wsTestDatabaseConnection: WsFrontendCall<DatabaseInfo, wsReturnKey.testedConnection> = (dbConnection, callback) => {
  if (!dbConnection) {
    console.warn('dbConnection is undefined on wsTestDatabaseConnection');
    if (callback) callback({ status: 'fail', saveStateID: '' }, 'error');
    return;
  }
  Broker.instance().sendMessage(
    {
      key: wsKeys.dbConnection,
      subKey: wsSubKeys.testConnection,
      body: dbConnection,
    },
    callback,
  );
};

export const wsStateGetPolicy: WsFrontendCall<{ saveStateID: string }, wsReturnKey.saveStatesModels> = (params, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.state,
      subKey: wsSubKeys.getPolicy,
      body: { saveStateID: params.saveStateID },
    },
    callback,
  );
};

export const wsShareSaveState: WsFrontendCall<StateSetPolicyRequest, wsReturnKey.shareSaveState> = (request, callback) => {
  Broker.instance().sendMessage(
    {
      key: wsKeys.state,
      subKey: wsSubKeys.shareState,
      body: request,
    },
    callback,
  );
};

export const wsShareStateSubscription = (callback: ResponseCallback<wsReturnKey.shareSaveState>) => {
  const id = Broker.instance().subscribe(callback, wsReturnKey.shareSaveState);
  return () => {
    Broker.instance().unSubscribe(wsReturnKey.shareSaveState, id);
  };
};
