import React, { ReactNode, useEffect, useImperativeHandle, useRef } from 'react';
import { useImmer } from 'use-immer';
import { useAppDispatch, useConfig } from '../store';
import { removeLastError, removeLastInfo, removeLastSuccess, removeLastWarning } from '../store/configSlice';
import { Icon } from '../../components';

type Message = {
  message: ReactNode;
  className: string;
};

export const DashboardAlerts = (props: { timer?: number }) => {
  const [messages, setMessages] = useImmer<{ data: any; routingKey: string; message: Message; showing: boolean; key: string }[]>([]);
  const timer = props.timer || 4000;
  const config = useConfig();
  const dispatch = useAppDispatch();
  const ref = useRef<any>(null);

  useImperativeHandle(ref, () => ({
    onAdd: (message: Message, data: any, routingKey: string) => {
      const newMessage = {
        data,
        routingKey,
        message: message as Message,
        showing: true,
        key: `${Date.now()}-${Math.random()}`,
      };
      setMessages(draft => {
        draft.unshift(newMessage);
        return draft;
      });
      return newMessage.key;
    },
    onTimeout: (key: string) => {
      setMessages(draft => {
        const idx = draft.findIndex(m => m.key === key);
        if (idx === -1) return draft;
        draft.splice(idx, 1);
        return draft;
      });
    },
    onShowTimeout: (key: string) => {
      setMessages(draft => {
        const idx = draft.findIndex(m => m.key === key);
        if (idx === -1) return draft;
        draft[idx].showing = false;
        return draft;
      });

      setTimeout(() => {
        ref.current.onTimeout(key);
        return true;
      }, 300); // matches the animation of animate-closemenu
    },
  }));

  async function processMessage(message: Message | undefined, data: any, routingKey: string) {
    // Queue the message to be shown for props.time time, then fade it out matching the animation of animate-closemenu fade out time
    if (message && ref?.current) {
      const key = ref.current.onAdd(message, data, routingKey);

      setTimeout(() => {
        ref.current.onShowTimeout(key);
        return true;
      }, timer);
    }
  }

  async function processError() {
    if (config.errors.length > 0) {
      await processMessage(
        {
          message: (
            <>
              <Icon component="icon-[ic--baseline-error-outline]" /> {config.errors?.[config.errors.length - 1] || 'Sever Error'}
            </>
          ),
          className: 'alert-error',
        },
        undefined,
        'error',
      );
      dispatch(removeLastError());
    }
  }

  async function processWarning() {
    if (config.warnings.length > 0) {
      await processMessage(
        {
          message: (
            <>
              <Icon component="icon-[ic--baseline-error-outline]" /> {config.warnings[config.warnings.length - 1]}
            </>
          ),
          className: 'alert-warning',
        },
        undefined,
        'warning',
      );
      dispatch(removeLastWarning());
    }
  }

  async function processInfo() {
    if (config.infos.length > 0) {
      await processMessage(
        {
          message: (
            <>
              <Icon component="icon-[ic--baseline-check-circle-outline]" /> {config.infos[config.infos.length - 1]}
            </>
          ),
          className: 'alert-success bg-primary',
        },
        undefined,
        'info',
      );
      dispatch(removeLastInfo());
    }
  }

  async function processSuccess() {
    if (config.successes.length > 0) {
      await processMessage(
        {
          message: (
            <>
              <Icon component="icon-[ic--baseline-check-circle-outline]" /> {config.successes[config.successes.length - 1]}
            </>
          ),
          className: 'alert-success',
        },
        undefined,
        'success',
      );
      dispatch(removeLastSuccess());
    }
  }

  useEffect(() => {
    processError();
  }, [config.errors]);

  useEffect(() => {
    processWarning();
  }, [config.warnings]);

  useEffect(() => {
    processInfo();
  }, [config.infos]);

  useEffect(() => {
    processSuccess();
  }, [config.successes]);

  return (
    <>
      {messages &&
        messages.map((m, i) => {
          return (
            <div
              key={i}
              className={
                `absolute bottom-5 right-5 w-fit min-w-[20rem] cursor-pointer alert z-50 ` +
                (m.showing ? 'animate-openmenu ' : 'animate-closemenu ') +
                (m.message?.className || '')
              }
              style={{
                transform: `translateY(-${i * 5}rem)`,
              }}
              onClick={e => {
                e.preventDefault();
                e.stopPropagation();
                ref.current.onShowTimeout(m.key);
              }}
            >
              <span className="flex flex-row content-center gap-3 text-light">{m.message.message}</span>
            </div>
          );
        })}
    </>
  );
};
