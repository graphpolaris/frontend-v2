import { getEnvVariable } from '@/config';
import { useAppDispatch, useSessionCache } from '../store';
import { authenticated } from '../store/authSlice';
import { addInfo, addError } from '../store/configSlice';
import { wsShareSaveState } from '../api';
import { getParam, URLParams } from '../api/url';
import { UserAuthenticationHeader } from 'ts-common';

const domain = getEnvVariable('BACKEND_URL');
const userURI = getEnvVariable('BACKEND_USER');

export const fetchSettings: RequestInit = {
  method: 'GET',
  credentials: 'include',
  redirect: 'follow',
};

export const useAuthentication = () => {
  const dispatch = useAppDispatch();
  const session = useSessionCache();

  const handleError = (err: any) => {
    console.error(err);
    dispatch(addError('Failed to copy: ' + (err.message || 'Unknown error occurred')));
  };

  const login = async () => {
    const impersonateID = getParam(URLParams.impersonateID);

    const res = await fetch(`${domain}${userURI}/headers${impersonateID ? '?impersonateID=' + impersonateID : ''}`, fetchSettings);
    const data = await res.json();

    dispatch(
      authenticated({
        username: data.username,
        userID: data.userID,
        jwt: data.jwt,
        authenticated: true,
        roomID: data.roomID,
        sessionID: data.sessionID,
      }),
    );
  };

  const copyToClipboard = async (text: string) => {
    try {
      await navigator.clipboard.writeText(text);
      return true;
    } catch (err) {
      console.error('Failed to copy:', err);
      return false;
    }
  };

  const shareLink = async (): Promise<boolean> => {
    if (!session.currentSaveState) {
      dispatch(addError('No save state to share'));
      return false;
    }

    try {
      return await new Promise((resolve, reject) => {
        wsShareSaveState(
          {
            saveStateId: session.currentSaveState || '',
            users: [
              {
                userId: '*', // Everyone
                sharing: {
                  database: { R: true, W: false },
                  visualization: { R: true, W: false },
                  query: { R: true, W: false },
                  schema: { R: true, W: false },
                  savestate: { R: true, W: false },
                },
              },
            ],
          },
          async data => {
            if (!data) {
              dispatch(addError('Failed to share link'));
              resolve(false);
            } else {
              // Authorization done, now just copy the link
              const shareUrl = window.location.href;
              const copied = await copyToClipboard(shareUrl);

              if (copied) {
                dispatch(addInfo('Link copied to clipboard!'));
                resolve(true);
              } else {
                console.warn('Failed to copy link to clipboard');
                dispatch(addError('Failed to copy link to clipboard'));
                resolve(false);
              }
            }
          },
        );
      });
    } catch (error: any) {
      handleError(error);
      return false;
    }
  };

  return { login, shareLink };
};
