import React, { useMemo } from 'react';
import { Button, Panel } from '../components';
import { useFocus, useSelection } from '@/lib/data-access';
import { resultSetFocus } from '@/lib/data-access/store/interactionSlice';
import { useDispatch } from 'react-redux';
import { Tab, Tabs } from '@/lib/components/tabs';
import { VisualizationSettings } from '@/lib/vis/components/config/VisualizationSettings';
import ResultInspector from './ResultInspector';

export function InspectorPanel() {
  const selection = useSelection();
  const focus = useFocus();
  const dispatch = useDispatch();

  const inspector = useMemo(() => {
    if (focus?.focusType === 'query') {
      return (
        <>
          <VisualizationSettings className="hidden" />
          <ResultInspector />
        </>
      );
    }
    return <VisualizationSettings />;
  }, [focus, selection]);

  return (
    <Panel
      title={
        <div className="flex items-stretch h-7">
          <Tabs>
            {['Visualization', 'Query'].map((tab, i) => (
              <Tab
                key={i}
                activeTab={focus?.focusType === tab.toLowerCase()}
                text={tab}
                onClick={() => dispatch(resultSetFocus({ focusType: tab.toLowerCase() as 'query' | 'visualization' }))}
              ></Tab>
            ))}
          </Tabs>
        </div>
      }
    >
      {inspector}

      <div className="flex flex-col w-full">
        <div className="mt-auto p-2 bg-light">
          <Button
            variantType="primary"
            variant="outline"
            size="xs"
            label="Report an issue"
            onClick={() =>
              window.open(
                'https://app.asana.com/-/login?u=https%3A%2F%2Fform.asana.com%2F%3Fk%3D2QEC88Dl7ETs2wYYWjkMXg%26d%3D1206648675960041&error=01',
                '_blank',
              )
            }
            className="block w-full"
          />
        </div>
      </div>
    </Panel>
  );
}
