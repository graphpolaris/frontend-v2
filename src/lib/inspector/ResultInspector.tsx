import React, { useEffect, useRef } from 'react';
import { select, scaleLinear } from 'd3';
import { useAppDispatch, useGraphQueryResult, useGraphQueryResultMeta, useSelection } from '@/lib/data-access';
import { resultSetSelection } from '@/lib/data-access/store/interactionSlice';
import { visualizationColors } from '@/config';
import { SelectionConfig } from './SelectionConfig';

export default function ResultInspector() {
  const dispatch = useAppDispatch();
  const selection = useSelection();
  const meta = useGraphQueryResultMeta();
  const queryResult = useGraphQueryResult();

  const chartRefNodes = useRef<HTMLDivElement>(null!);
  const chartRefEdges = useRef<HTMLDivElement>(null!);

  useEffect(() => {
    if (!meta) return;

    const renderChart = (data: any[], chartRef: React.RefObject<HTMLDivElement>, total: number, type: string) => {
      const container = chartRef.current;
      if (!container) return;

      const containerWidth = container.getBoundingClientRect().width;
      const width = containerWidth;
      const height = 30;
      const margin = { top: 5, right: 5, bottom: 15, left: 5 };

      select(container).selectAll('*').remove();

      const svg = select(container)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', `translate(${margin.left},${margin.top})`);

      const xScale = scaleLinear().domain([0, 1]).range([0, width]);

      const colorScale =
        type === 'Edge'
          ? (index: number) => `rgb(${220 - index * 20}, ${220 - index * 20}, ${220 - index * 20})` // Grayscale for edges
          : (index: number) => visualizationColors.GPCat.colors[14][index % visualizationColors.GPCat.colors[14].length]; // Original colors for nodes

      let xOffset = 0;

      const bars = svg
        .selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
        .attr('x', d => {
          const currentX = xOffset;
          xOffset += xScale(total > 0 ? d.count / total : 0);
          return currentX;
        })
        .attr('y', 0)
        .attr('width', d => xScale(total > 0 ? d.count / total : 0))
        .attr('height', height)
        .attr('fill', (d, i) => colorScale(i))
        .style('cursor', 'pointer')
        .on('click', (event, d) => {
          if (type === 'Node') {
            const selectedNodes = queryResult.nodes.filter(node => node.label === d.label);
            dispatch(resultSetSelection({ selectionType: 'node', content: selectedNodes }));
          } else if (type === 'Edge') {
            const selectedEdges = queryResult.edges.filter(edge => edge.label === d.label);
            dispatch(resultSetSelection({ selectionType: 'relation', content: selectedEdges }));
          }
        })
        .on('mouseenter', function (_, d) {
          select(this).select('title').style('display', 'block');
        })
        .on('mouseleave', function () {
          select(this).select('title').style('display', 'none');
        });

      bars.append('title').text(d => d.label);

      xOffset = 0;

      const labels = svg
        .selectAll('text')
        .data(data)
        .enter()
        .append('text')
        .attr('x', d => {
          const currentX = xOffset + xScale(total > 0 ? d.count / total : 0) / 2;
          xOffset += xScale(total > 0 ? d.count / total : 0);
          return currentX;
        })
        .attr('y', height + 10) // Position below the bars
        .attr('text-anchor', 'middle')
        .attr('font-size', '10px')
        .attr('fill', '#000')
        .text(d => d.label)
        .style('visibility', d => (xScale(total > 0 ? d.count / total : 0) < 50 ? 'hidden' : 'visible')); // Hide labels if the bar width is too small
    };

    const nodeData = Object.entries(meta.nodes?.types || {}).map(([label, stats]) => ({
      label,
      count: stats.count,
    }));
    const edgeData = Object.entries(meta.edges?.types || {}).map(([label, stats]) => ({
      label,
      count: stats.count,
    }));

    const observerNodes = new ResizeObserver(() => {
      renderChart(nodeData, chartRefNodes, meta.nodes?.count || 0, 'Node');
    });
    const observerEdges = new ResizeObserver(() => {
      renderChart(edgeData, chartRefEdges, meta.edges?.count || 0, 'Edge');
    });

    if (chartRefNodes.current) observerNodes.observe(chartRefNodes.current);
    if (chartRefEdges.current) observerEdges.observe(chartRefEdges.current);

    return () => {
      observerNodes.disconnect();
      observerEdges.disconnect();
    };
  }, [meta, selection]);

  return (
    <div className="overflow-y-auto">
      <div className="flex flex-col p-4 border-b">
        <div className="mb-2">
          <div className="flex justify-between">
            <span className="text-xs font-semibold">Self-Loops:</span>
            <span className="text-xs block">{meta?.topological.self_loops || '0'}</span>
          </div>
          <div className="flex justify-between">
            <span className="text-xs font-semibold">Density:</span>
            <span className="text-xs block"> {meta?.topological?.density?.toFixed(3) || '0'}</span>
          </div>
        </div>
        <div className="flex flex-col space-y-4">
          <div>
            <div className="flex justify-between">
              <span className="text-xs font-semibold">Nodes:</span>
              <span className="text-xs block">
                {selection?.selectionType === 'node' ? selection.content.length : 0} / {meta?.nodes?.count || 0} (selected / all)
              </span>
            </div>
            {meta?.nodes?.count && meta.nodes.count > 0 && <div ref={chartRefNodes} className="w-full h-auto"></div>}
          </div>
          <div>
            <div className="flex justify-between">
              <span className="text-xs font-semibold">Edges:</span>
              <span className="text-xs block">
                {selection?.selectionType === 'relation' ? selection.content.length : 0} / {meta?.edges?.count || 0} (selected / all)
              </span>
            </div>
            {meta?.edges?.count && meta.edges.count > 0 && <div ref={chartRefEdges} className="w-full h-auto"></div>}
          </div>
        </div>
      </div>
      {selection && <SelectionConfig />}
    </div>
  );
}
