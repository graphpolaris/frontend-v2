import React from 'react';
import { useDispatch } from 'react-redux';
import { Button, EntityPill, RelationPill, useSelection } from '..';
import { unSelect } from '@/lib/data-access/store/interactionSlice';
import { ATTRIBUTE_MAX_CHARACTERS } from '@/config';

export const SelectionConfig = () => {
  const dispatch = useDispatch();
  const selection = useSelection();

  if (!selection) return null;

  return (
    <div className="border-b py-2 overflow-auto">
      <div className="flex justify-between items-center px-4 py-2">
        <span className="text-xs font-bold">Selection</span>
        <Button
          variantType="secondary"
          variant="ghost"
          size="xs"
          iconComponent="icon-[ic--baseline-delete]"
          onClick={() => {
            dispatch(unSelect());
          }}
        />
      </div>
      <div className="flex justify-between items-center px-4 py-2">
        <span className="text-xs">Labels</span>
        <div>
          {[...new Set(selection.content.map(item => item.label))].map((label, index) => (
            <div key={index + 'id'} className="">
              <span className="text-xs bg-secondary-100 rounded p-1">{label}</span>
            </div>
          ))}
        </div>
      </div>
      {selection.content.map((item, index) => (
        <React.Fragment key={index + 'id'}>
          <div className="flex justify-between items-center px-4 py-1 gap-1 border-t border-secondary-200 mx-auto">
            <span className="text-xs font-semibold pr-2">ID</span>
            <span className="text-xs">{item._id}</span>
          </div>
          <div key={index + 'label'} className="flex justify-between items-center px-4 py-1 gap-1">
            <span className="text-xs font-semibold pr-2">Label</span>
            {selection.selectionType === 'node' ? (
              <EntityPill title={item.label as string}></EntityPill>
            ) : (
              <RelationPill title={item.label as string}></RelationPill>
            )}
          </div>
          {Object.entries(item.attributes).map(([key, value]) => {
            if (key === 'labels' || key === '_id' || value instanceof Object || String(value).length > ATTRIBUTE_MAX_CHARACTERS)
              return null;
            return (
              <div key={index + key} className="flex justify-between items-center px-4 py-1 gap-1">
                <span className="text-xs font-semibold pr-2 whitespace-nowrap max-w-[6rem]">{String(key)}</span>
                <span className="text-xs break-all">{String(value)}</span>
              </div>
            );
          })}
        </React.Fragment>
      ))}
    </div>
  );
};
