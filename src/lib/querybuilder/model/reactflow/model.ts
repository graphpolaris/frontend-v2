/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { Edge as ReactEdge, NodeProps } from 'reactflow';
import { EntityNodeAttributes, RelationNodeAttributes, LogicNodeAttributes, EntityData, RelationData, LogicData } from 'ts-common';
import { QueryElementTypes } from 'ts-common/src/model/reactflow';

/** List of possible query element types */
export const possibleTypes: string[] = [
  QueryElementTypes.Entity,
  QueryElementTypes.Relation,
  QueryElementTypes.Logic,
  // QueryElementTypes.Attribute,
  // QueryElementTypes.Function,
];

/** All the possible react flow nodes. */
export type SchemaReactflowEntityNode = NodeProps<EntityNodeAttributes>;
export type SchemaReactflowRelationNode = NodeProps<RelationNodeAttributes>;
export type SchemaReactflowLogicNode = NodeProps<LogicNodeAttributes>;

export type SchemaReactflowNode = SchemaReactflowEntityNode | SchemaReactflowRelationNode | SchemaReactflowLogicNode;
export type SchemaReactflowEdge = ReactEdge<any>;
export type SchemaReactflowElement = SchemaReactflowNode | SchemaReactflowEdge;
export type SchemaReactflowNodeData = EntityData | RelationData | LogicData;
