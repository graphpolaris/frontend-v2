import Graph from 'graphology';
import { Node, Edge } from 'reactflow';
import { toHandleId } from '..';

// Takes the querybuilder graph as an input and creates react flow elements for them.
export function createReactFlowElements<T extends Graph>(
  graph: T,
): {
  nodes: Array<Node>;
  edges: Array<Edge>;
} {
  const nodes: Array<Node> = [];
  const edges: Array<Edge> = [];

  graph.forEachNode((node, attributes): void => {
    const position = { x: attributes?.x || 0, y: attributes?.y || 0 };
    const RFNode: Node<typeof attributes> = {
      id: node,
      type: attributes.type,
      position: position,
      data: attributes,
    };
    nodes.push(RFNode);
  });

  // Add the reactflow edges
  graph.forEachEdge((edge, attributes, source, target): void => {
    // connection from attributes don't have visible connection lines
    const RFEdge: Edge<typeof attributes> = {
      id: edge,
      source: source,
      target: target,
      type: 'connection',
      sourceHandle: toHandleId(attributes.sourceHandleData),
      targetHandle: toHandleId(attributes.targetHandleData),
      data: attributes,
      zIndex: 1,
    };
    edges.push(RFEdge);
  });

  return { nodes, edges };
}
