import Graph, { MultiGraph } from 'graphology';
import { Attributes as GAttributes, Attributes } from 'graphology-types';

import { checkForMetaAttributes } from './metaAttributes';
import {
  QueryGraphNodes,
  QueryGraphEdges,
  SchemaAttribute,
  QueryGraphEdgeHandle,
  EntityNodeAttributes,
  RelationNodeAttributes,
  LogicNodeAttributes,
  QueryGraphEdgesOpt,
  SchemaAttributeTypes,
  Handles,
  InputNodeTypeTypes,
  XYPosition,
} from 'ts-common';
import { QueryElementTypes } from 'ts-common';

/** monospace fontsize table */
const widthPerFontsize = {
  6: 3.6167,
  7: 4.2167,
  10: 6.0167,
};

export type AddEdge2GraphologyOptions = {
  // attributeSourceHandle?: string;
  sourceHandleName?: string;
  targetHandleName?: string;
  attributeTargetHandleName?: string;
  attributeTargetHandleType?: string;
};

export class QueryMultiGraphology extends MultiGraph<QueryGraphNodes, QueryGraphEdges, GAttributes> {
  public configureDefaults(attributes: QueryGraphNodes): QueryGraphNodes {
    const { type, name } = attributes;
    if (!type || !name) throw Error('type or name is not defined');
    let { x, y } = attributes;

    // Check if x and y are present, otherwise set them to 0
    if (!x) x = 0;
    if (!y) y = 0;

    // Get the width and height of a node
    const { width, height } = calcWidthHeightOfPill(attributes);

    attributes.x = x;
    attributes.y = y;
    attributes.width = width;
    attributes.height = height;

    if (!attributes.id) attributes.id = 'id_' + (Date.now() + Math.floor(Math.random() * 1000)).toString();

    // Add to the beginning the meta attributes, such as (# Connection)
    attributes.attributes = [...checkForMetaAttributes(attributes).map(a => ({ handleData: a })), ...attributes.attributes];

    return attributes;
  }

  public addPill2Graphology(attributes: QueryGraphNodes, optAttributes: SchemaAttribute[] | undefined = undefined): QueryGraphNodes {
    attributes = this.configureDefaults(attributes);
    if (!attributes.type || !attributes.name || !attributes.id) throw Error('type or name is not defined');

    // fix handles
    const defaultHandleData: QueryGraphEdgeHandle = {
      nodeId: attributes.id,
      nodeName: attributes.name,
      nodeType: attributes.type,
      handleType: Handles.EntityAttribute,
    };
    if (attributes.type === QueryElementTypes.Entity) {
      const entityAttributes = attributes as EntityNodeAttributes;
      entityAttributes.leftRelationHandleId = { ...defaultHandleData, handleType: Handles.EntityLeft };
      entityAttributes.rightRelationHandleId = { ...defaultHandleData, handleType: Handles.EntityRight };
      optAttributes?.forEach(optAttribute => {
        if (!entityAttributes.attributes) entityAttributes.attributes = [];
        entityAttributes.attributes.push({
          handleData: {
            ...defaultHandleData,
            attributeName: optAttribute.name,
            attributeType: optAttribute.type,
            handleType: Handles.EntityAttribute,
          },
        });
      });
    } else if (attributes.type === QueryElementTypes.Relation) {
      const relationAttributes = attributes as RelationNodeAttributes;
      relationAttributes.leftEntityHandleId = { ...defaultHandleData, handleType: Handles.RelationLeft };
      relationAttributes.rightEntityHandleId = { ...defaultHandleData, handleType: Handles.RelationRight };
      optAttributes?.forEach(optAttribute => {
        if (!relationAttributes.attributes) relationAttributes.attributes = [];
        relationAttributes.attributes.push({
          handleData: {
            ...defaultHandleData,
            attributeName: optAttribute.name,
            attributeType: optAttribute.type,
            handleType: Handles.RelationAttribute,
          },
        });
      });
    } else if (attributes.type === QueryElementTypes.Logic) {
      throw Error('using wrong function! use addLogicPill2Graphology instead');
    }

    // Add a node to the graphology object
    this.addNode(attributes.id, { ...attributes });

    // Set the new nodes in the query builder slice TODO: maybe remove for efficiency
    // dispatch(setQuerybuilderNodes(this.export())); // Can't do this due to loop import

    return attributes;
  }

  public addLogicPill2Graphology(attributes: LogicNodeAttributes, inputValues: Record<string, InputNodeTypeTypes> = {}): QueryGraphNodes {
    attributes = this.configureDefaults(attributes) as LogicNodeAttributes;
    if (!attributes.name || !attributes.id) throw Error('type or name is not defined');

    // add default inputs, but only if not there yet
    if (attributes.type === QueryElementTypes.Logic) {
      if (attributes.inputs === undefined || Object.keys(attributes.inputs).length === 0) {
        attributes.logic.inputs.forEach((input, i) => {
          // Setup default non-linked inputs as regular values matching the input expected type
          if (!attributes.inputs) attributes.inputs = {};
          attributes.inputs[input.name] = inputValues?.[input.name] || input.default;
        });
        // (attributes as LogicNodeAttributes).leftEntityHandleId = getHandleId(attributes.id, name, type, Handles.RelationLeft, '');
        // (attributes as LogicNodeAttributes).rightEntityHandleId = getHandleId(attributes.id, name, type, Handles.RelationRight, '');
      }
    } else throw Error('using wrong function! use addPill2Graphology instead');

    // Add a node to the graphology object
    const nodeMergeResult = this.mergeNode(attributes.id, { ...attributes });

    // Set the new nodes in the query builder slice TODO: maybe remove for efficiency
    // dispatch(setQuerybuilderNodes(this.export())); // Can't do this due to loop import

    return attributes;
  }

  public addEdge2Graphology(
    source: QueryGraphNodes,
    target: QueryGraphNodes,
    attributes: QueryGraphEdgesOpt = {},
    options: AddEdge2GraphologyOptions = {},
  ): string | null {
    let sourceAttributeName = '';
    let sourceAttributeType: SchemaAttributeTypes | undefined;
    let targetAttributeName = '';
    let targetAttributeType: SchemaAttributeTypes | undefined;
    let sourceHandleType: Handles = Handles.EntityRight;
    let targetHandleType: Handles = Handles.EntityRight;

    if (source.type === QueryElementTypes.Entity) {
      source = source as EntityNodeAttributes;
      if (options?.sourceHandleName) {
        sourceHandleType = Handles.EntityAttribute;
        sourceAttributeName = options?.sourceHandleName;
        sourceAttributeType = source?.attributes?.find(a => a.handleData.attributeName === sourceAttributeName)?.handleData.attributeType;
      } else {
        sourceHandleType = Handles.EntityRight;
      }
    } else if (source.type === QueryElementTypes.Relation) {
      source = source as RelationNodeAttributes;
      if (options?.sourceHandleName) {
        sourceHandleType = Handles.RelationAttribute;
        sourceAttributeName = options?.sourceHandleName;
        sourceAttributeType = source?.attributes?.find(a => a.handleData.attributeName === sourceAttributeName)?.handleData.attributeType;
      } else {
        sourceHandleType = Handles.RelationRight;
      }
    } else if (source.type === QueryElementTypes.Logic) {
      if (!options.sourceHandleName) throw Error('sourceHandleName is not defined');
      sourceHandleType = Handles.LogicRight;
      sourceAttributeName = options.sourceHandleName;
      sourceAttributeType = (source as LogicNodeAttributes).logic.output.type;
      if (!sourceAttributeType) throw Error(`sourceHandleName ${sourceAttributeName} does not exist!`);
    } else {
      throw Error('source.type is not correctly defined');
    }

    if (target.type === QueryElementTypes.Entity) {
      // if (!!options?.attributeTargetHandle) {
      //   targetAttributeName = Handles.EntityAttribute;
      //   targetAttributeType = options.attributeTargetHandle;
      // } else {
      targetHandleType = Handles.EntityLeft;
      // }
    } else if (target.type === QueryElementTypes.Relation) {
      targetHandleType = Handles.RelationLeft;
    } else if (target.type === QueryElementTypes.Logic) {
      if (!options.targetHandleName) throw Error('targetHandleName is not defined');
      targetHandleType = Handles.LogicLeft;
      targetAttributeName = options.targetHandleName;
      const _target = target as LogicNodeAttributes;
      if (targetAttributeName === _target.logic.input.name) {
        targetAttributeType = _target.logic.input.type;
      } else {
        targetAttributeType = _target.logic.inputs.find(i => i.name === targetAttributeName)?.type;
      }
      if (!targetAttributeType) throw Error(`targetHandleName ${targetAttributeName} does not exist!`);
    } else {
      throw Error('target.type is not correctly defined');
    }

    if (!source.id) throw Error('source.id is not defined');
    if (!target.id) throw Error('target.id is not defined');
    if (!attributes.type) attributes.type = 'connection';
    // fix handles
    attributes.sourceHandleData = {
      nodeId: source.id,
      nodeName: source.name || '',
      nodeType: source.type,
      attributeName: sourceAttributeName,
      attributeType: sourceAttributeType,
      handleType: sourceHandleType,
    };
    attributes.targetHandleData = {
      nodeId: target.id,
      nodeName: target.name || '',
      nodeType: target.type,
      attributeName: targetAttributeName,
      attributeType: targetAttributeType,
      handleType: targetHandleType,
    };

    // Add/edit an edge to the graphology object
    const edgeResult = this.mergeEdge(source.id, target.id, attributes as QueryGraphEdges);

    // Set the new nodes in the query builder slice TODO: maybe remove for efficiency
    // store.dispatch(setQuerybuilderNodes(this.export()));

    return edgeResult[0]; // edge id
  }
}

/** Calculates the width and height of a query builder pill.
 * DEPENDS ON STYLING, if styling changed, change this.
 */
export function calcWidthHeightOfPill(attributes: Attributes): {
  width: number;
  height: number;
} {
  const { type, name } = attributes;

  let w = 0;
  let h = 0;
  switch (type) {
    case 'entity': {
      // calculate width and height of entity pill
      w = Math.min(name.length, 20) * widthPerFontsize[10]; // for fontsize 10px

      const widthOfPillWithoutText = 42.1164; // WARNING: depends on styling
      w += widthOfPillWithoutText;
      h = 20;
      break;
    }
    case 'relation': {
      // calculate width and height of relation pill
      w = Math.min(name.length, 20) * widthPerFontsize[10]; // for fontsize 10px

      const widthOfPillWithoutText = 56.0666; // WARNING: depends on styling
      w += widthOfPillWithoutText;
      h = 20;
      break;
    }
    case 'attribute': {
      // calculate width and height of relation pill
      const pixelsPerChar = widthPerFontsize[6]; // for fontsize 10px
      w = name.length * pixelsPerChar;

      const { datatype, operator } = attributes;
      let value = attributes['value'];
      if (!datatype || !operator) return { width: 0, height: 0 };
      if (!value) value = '?';

      // Add width of operator
      w += operator.length * widthPerFontsize[7];
      // use a max of 10, because max-width is set to 10ch;
      w += Math.min(value.length, 10) * widthPerFontsize[6];

      const widthOfPillWithoutText = 25.6666; // WARNING: depends on styling
      w += widthOfPillWithoutText;
      h = 12;
      break;
    }
  }

  return { width: w, height: h };
}

/** Interface for x and y position of node */
export type NodePosition = XYPosition;

/** Returns from-position of relation node */
export function RelationPosToFromEntityPos(position: XYPosition): NodePosition {
  return { x: position.x - 60, y: position.y - 40 };
}

/** Returns to-position of relation node */
export function RelationPosToToEntityPos(position: XYPosition): NodePosition {
  return { x: position.x + 400, y: position.y };
}

/** Default position; x=0, y=0 */
export const DefaultPosition = { x: 0, y: 0 };
