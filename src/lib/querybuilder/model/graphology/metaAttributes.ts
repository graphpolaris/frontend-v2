import { Handles, QueryElementTypes, QueryGraphEdgeAttribute, QueryGraphEdgeHandle, QueryGraphNodes } from 'ts-common';

const metaAttribute: Record<string, QueryGraphEdgeAttribute> = {
  '(# Connection)': {
    attributeName: '(# Connection)',
    attributeType: 'float',
  },
};

export function checkForMetaAttributes(graphologyAttributes: QueryGraphNodes): QueryGraphEdgeHandle[] {
  const ret: QueryGraphEdgeHandle[] = [];
  const defaultHandleData = {
    nodeId: graphologyAttributes.id,
    nodeName: graphologyAttributes.name || '',
    nodeType: graphologyAttributes.type,
    handleType: graphologyAttributes.type === QueryElementTypes.Entity ? Handles.EntityAttribute : Handles.RelationAttribute,
  };

  // Only include if not already there
  const metaAttributesToInclude = Object.keys(metaAttribute).filter(attributeName => !(attributeName in graphologyAttributes.attributes));
  return metaAttributesToInclude.map(attributeName => ({
    ...defaultHandleData,
    ...metaAttribute[attributeName],
  })) as QueryGraphEdgeHandle[];
}
