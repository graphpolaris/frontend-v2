import { QueryGraphEdgeHandle, NodeAttribute, QueryElementTypes, InputNodeType, Handles } from 'ts-common';
import { SchemaReactflowNode } from './reactflow';

export * from './graphology';
export * from './reactflow';

type ExtraProps = { extra?: string; separator?: string };
export function toHandleId(handleData?: QueryGraphEdgeHandle, separator: string = '__'): string {
  if (!handleData) throw Error('handleData is not defined');
  // if (!extra) extra = '';
  if (!separator) separator = '__';
  return [
    handleData.nodeId,
    handleData.nodeType,
    handleData.nodeName,
    handleData.handleType,
    handleData.attributeName,
    handleData.attributeType,
  ].join(separator);
}
// export function getHandleIdFromGraphology(node: QueryGraphNodes, attribute: NodeAttribute, options: ExtraProps = {}): string {
//   return toHandleId(node.id || '', node.name, node.type, attribute.name, attribute.type, options);
// }
export function handleDataFromReactflowToDataId(
  node: SchemaReactflowNode,
  attribute: NodeAttribute,
  options: ExtraProps = {},
): QueryGraphEdgeHandle {
  if (!node.data.name) throw Error('node.data is not defined');
  return {
    nodeId: node.id,
    nodeName: node.data.name,
    nodeType: node.type as QueryElementTypes,
    attributeName: attribute.handleData.attributeName,
    attributeType: attribute.handleData.attributeType,
    handleType: attribute.handleData.handleType,
  };
}

export function toHandleData(handleId: string, separator: string = '__'): QueryGraphEdgeHandle {
  const [nodeId, nodeType, nodeName, handleType, attributeName, attributeType] = handleId.split(separator);
  const _nodeType = nodeType as QueryElementTypes;
  const _attributeType = attributeType as InputNodeType;
  const _handleType = handleType as Handles;
  return {
    nodeId,
    nodeType: _nodeType,
    nodeName,
    attributeName,
    attributeType: _attributeType,
    handleType: _handleType,
  };
}
