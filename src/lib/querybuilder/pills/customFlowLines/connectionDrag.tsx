import React from 'react';
import { ConnectionLineComponentProps } from 'reactflow';

/**
 * A custom query element to render the line when connecting flow elements.
 * @param {ConnectionLineComponentProps} param0 Source and target coordinates of the edges.
 */
export function ConnectionDragLine({ fromX, fromY, toX, toY }: ConnectionLineComponentProps) {
  return (
    <g>
      <path fill="none" stroke="#222" strokeWidth={2.5} className="animated" d={`M${fromX},${fromY}L ${toX},${toY}`} />
      <circle cx={fromX} cy={fromY} fill="#fff" r={3} stroke="#222" strokeWidth={1.5} />
      <circle cx={toX} cy={toY} fill="#fff" r={3} stroke="#222" strokeWidth={1.5} />
    </g>
  );
}
