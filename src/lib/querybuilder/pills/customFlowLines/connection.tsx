import { EdgeProps, Position, getSmoothStepPath } from 'reactflow';

import './connection.scss';

/**
 * A custom query element edge line component.
 * @param {EdgeProps} param0 The coordinates for the start and end point, the id and the style.
 */
//  export const EntityRFPill = React.memo(({ data }: { data: any }) => {
export function ConnectionLine({ id, sourceX, sourceY, targetX, targetY, style, sourcePosition, targetPosition }: EdgeProps) {
  //Centering the line
  // sourceY -= 3;
  // targetY -= 3;

  // Correct line positions with hardcoded numbers, because react flow lacks this functionality
  // if (sourceHandleId == ) sourceX += 2;

  // if (targetHandleId == Handles.ToAttributeHandle) targetX += 2;

  // let spos: Position = sourcePosition;
  // if (sourceHandleId == handles.relation.fromEntity) {
  //   spos = Position.Left;
  //   sourceX += 7;
  //   sourceY += 3;
  // } else if (sourceHandleId == handles.relation.toEntity) {
  //   spos = Position.Right;
  //   sourceX -= 2;
  //   sourceY -= 3;
  // } else if (
  //   sourceHandleId !== undefined &&
  //   sourceHandleId !== null &&
  //   sourceHandleId.includes('functionHandle')
  // ) {
  //   spos = Position.Top;
  //   sourceX -= 4;
  //   sourceY += 3;
  // }

  if (targetPosition === Position.Left) {
    targetX += 2;
    targetY += 0.5;
  } else if (targetPosition === Position.Right) {
    targetX -= 2;
    targetY -= 0.5;
  }

  // Create smoothstep line
  const path = getSmoothStepPath({
    sourceX: sourceX,
    sourceY: sourceY,
    sourcePosition,
    targetX: targetX,
    targetY: targetY,
    targetPosition,
    offset: Math.abs(targetX - sourceX) / 10,
    borderRadius: Math.abs(targetX - sourceX) / 10,
  });

  return (
    <g>
      <path className="stroke-edge" id={id} fill="none" strokeWidth={1} style={style} d={path[0]} />
    </g>
  );
}
