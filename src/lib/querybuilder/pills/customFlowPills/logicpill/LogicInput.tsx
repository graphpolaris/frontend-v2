import { Input } from '@/lib/components';
import { DropdownInput } from '@/lib/components/inputs/DropdownInput';
import React, { useRef, useEffect } from 'react';

interface LogicInputProps {
  value: string;
  type: string;
  onChangeValue(value: string, execute: boolean): void;
  onExecute(): void;
  options?: Array<string>;
}

export const LogicInput: React.FC<LogicInputProps> = ({ value, onChangeValue, onExecute, options }) => {
  const ref = useRef<HTMLInputElement>(null);

  useEffect(() => {
    setTimeout(() => {
      // need a timeout to make sure the input is rendered and no other interruption happens before focusing
      if (ref?.current) {
        ref.current.focus();
      }
    }, 100);
  }, []);

  if (options)
    return (
      <DropdownInput
        ref={ref}
        value={value}
        label=""
        onMouseDownCapture={e => e.stopPropagation()}
        autocomplete
        inline={false}
        onChange={e => {
          if (onChangeValue) onChangeValue((e as string).replace('\n', ', '), true);
        }}
        options={options}
        onKeyDown={e => {
          if (e.key === 'Enter') onExecute();
        }}
        size="xs"
        className="mb-0 mt-0 rounded-sm"
        type={'dropdown'}
      />
    );
  else
    return (
      <Input
        ref={ref}
        type="text"
        value={value}
        label=""
        onMouseDownCapture={e => e.stopPropagation()}
        onChange={e => onChangeValue(e.replace('\n', ', '), false)}
        onKeyDown={e => {
          if (e.key === 'Enter') onExecute();
        }}
        onBlur={onExecute}
        placeholder="type here"
        size="xs"
        className="mb-0 mt-0 rounded-sm"
      />
    );
};
