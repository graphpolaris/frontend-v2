import { useActiveQuery, useAppDispatch, useGraphQueryResultMeta } from '@/lib/data-access';
import { useContext, useEffect, useMemo, useRef, useState } from 'react';
import { Handle, Position } from 'reactflow';
import { LogicInput } from './LogicInput';
import { LogicPill } from '@/lib/components';
import { QueryBuilderDispatcherContext } from '../../../panel/QueryBuilderDispatcher';
import { DropdownTrigger } from '@/lib/components/dropdowns';
import { SchemaReactflowLogicNode, toHandleId } from '@/lib/querybuilder/model';
import { LogicNodeAttributes, InputNodeTypeTypes, InputNode, Handles, CategoricalStats, defaultGraph } from 'ts-common';
import { styleHandleMap } from '../../utils';
import { setQuerybuilderGraphology, toQuerybuilderGraphology } from '@/lib/data-access/store/sessionSlice';

export function QueryLogicPill(node: SchemaReactflowLogicNode) {
  const dispatch = useAppDispatch();
  const data = node.data;
  const logic = data.logic;
  const { openLogicPillUpdate: openLogicPillChooser } = useContext(QueryBuilderDispatcherContext);

  const output = data.logic.output;
  const inputReference = useRef<HTMLInputElement>(null);
  const activeQuery = useActiveQuery();

  const metadata = useGraphQueryResultMeta();
  const graphologyGraph = useMemo(() => toQuerybuilderGraphology(activeQuery?.graph || defaultGraph()), [activeQuery?.graph]);
  const connectionsToLeft = useMemo(() => activeQuery?.graph.edges.filter(edge => edge.target === node.id) || [], [activeQuery?.graph]);
  const connectionsToRight = useMemo(() => activeQuery?.graph.edges.filter(edge => edge.source === node.id) || [], [activeQuery?.graph]);
  const graphologyNodeAttributes = useMemo<LogicNodeAttributes | undefined>(() => {
    const graphNode = activeQuery?.graph.nodes.find(n => n.key === node.id);
    if (!graphNode) return undefined;
    return graphNode.attributes as LogicNodeAttributes;
  }, [node.id]);
  const graphologyNodeOptions = useMemo<string[] | undefined>(() => {
    const connAttribute = connectionsToLeft[0]?.attributes?.sourceHandleData.attributeName;
    const connName = connectionsToLeft[0]?.attributes?.sourceHandleData.nodeName;
    if (!connName || !connAttribute) return undefined;

    const m = metadata?.nodes.types[connName]?.attributes?.[connAttribute]?.statistics as CategoricalStats | undefined;
    if (!m || !m?.values || m?.values?.length > 50) {
      // In case there are too many values, we don't want to show them all
      return undefined;
    }

    return [...m.values].sort();
  }, [connectionsToLeft, node.id, metadata]);
  const [localInputCache, setLocalInputCache] = useState<Record<string, InputNodeTypeTypes>>({
    ...graphologyNodeAttributes?.inputs,
  });
  const [openDropdown, setOpenDropdown] = useState(false);

  if (!data.id) throw new Error('LogicPill: data.id is undefined');
  const defaultHandleData = {
    nodeId: data.id,
    nodeName: data.name,
    nodeType: data.type,
  };

  const onInputUpdated = (value: string, input: InputNode, idx: number) => {
    if (!graphologyNodeAttributes) throw new Error('LogicPill: graphologyNodeAttributes is undefined');

    const logicNode: LogicNodeAttributes = { ...graphologyNodeAttributes };
    if (!logicNode) throw new Error('LogicPill: logicNode is undefined');

    const logicNodeInputs = { ...logicNode.inputs };
    if (data.inputs[input.name] !== value) {
      logicNodeInputs[input.name] = value;
      logicNode.inputs = logicNodeInputs;
      graphologyGraph.setNodeAttribute<any>(node.id, 'inputs', logicNodeInputs); // FIXME: I'm not sure why TS requires <any> to work here
      dispatch(setQuerybuilderGraphology(graphologyGraph));
    }
  };

  function removeNode() {
    graphologyGraph.dropNode(node.id);
    dispatch(setQuerybuilderGraphology(graphologyGraph));
  }

  useEffect(() => {
    if (inputReference?.current) inputReference.current.focus();
  }, [node.id]);

  // FIXME: This is a temporary fix to prevent the logic pill from rendering when the input is not set
  if (!logic.input) {
    console.error('LogicPill: logic.input is undefined', logic.input);
    return null;
  }

  return (
    <LogicPill
      title={<span>{connectionsToLeft[0]?.attributes?.sourceHandleData.attributeName}</span>}
      handleLeft={
        <Handle
          className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !right-0 !left-0'}
          type="target"
          position={Position.Left}
          id={toHandleId({
            ...defaultHandleData,
            attributeName: logic.input.name,
            attributeType: logic.input.type,
            handleType: Handles.LogicLeft,
          })}
        ></Handle>
      }
    >
      <div className={`py-1 h-fit border-[1px] border-secondary-200 ${data.selected ? 'bg-secondary-400' : 'bg-secondary-100'}`}>
        {/* <div className="m-1 mx-2 text-left">{output.name}</div> */}
        <DropdownTrigger
          popover={false}
          title={output.name}
          variant="ghost"
          size="xs"
          onClick={() => {
            openLogicPillChooser(node);
          }}
        />
        {node.data.logic.inputs.map((input, i) => {
          const connection = connectionsToLeft.find(
            edge =>
              edge?.attributes?.targetHandleData.nodeId === data.id && edge?.attributes?.targetHandleData.attributeName === input.name,
          );

          return (
            <div key={i}>
              <div className="w-full flex">
                {!connection ? (
                  <LogicInput
                    options={graphologyNodeOptions}
                    value={localInputCache?.[input.name] as string}
                    type={input.type}
                    onChangeValue={(value: string, execute: boolean) => {
                      setLocalInputCache({ ...localInputCache, [input.name]: value });
                      if (execute) onInputUpdated(value, input, i);
                    }}
                    onExecute={() => {
                      onInputUpdated(localInputCache?.[input.name] as string, input, i);
                    }}
                  />
                ) : (
                  <span className="px-1 m-0 mx-1 p-0 h-5 w-full rounded-sm border-[1px]">
                    {connection?.attributes?.sourceHandleData.attributeName}
                  </span>
                )}
              </div>
              <Handle
                type={'target'}
                position={Position.Left}
                id={toHandleId({
                  ...defaultHandleData,
                  attributeName: input.name,
                  attributeType: input.type,
                  handleType: Handles.LogicLeft,
                })}
                key={'input' + input.name + input.type}
                style={{ top: `${((i + 1.05) / (node.data.logic.inputs.length + 0.4)) * 100}%` }}
                className={styleHandleMap[input.type] + ''}
              ></Handle>
            </div>
          );
        })}
        {node.data.logic.output && (
          <Handle
            type={'source'}
            position={Position.Right}
            id={toHandleId({
              ...defaultHandleData,
              attributeName: output.name,
              attributeType: output.type,
              handleType: Handles.LogicRight,
            })}
            key={'output' + output.name + output.type}
            style={{ top: `${node.data.logic.inputs.length > 0 ? 75 : 50}%` }}
            className={styleHandleMap?.[output.type]}
          ></Handle>
        )}
      </div>
    </LogicPill>
  );
}
