import { memo, useState, useMemo, useEffect } from 'react';
import { SchemaReactflowRelationNode, toHandleId } from '../../../model';
import { useActiveQuery, useAppDispatch } from '@/lib/data-access';
import { addWarning } from '@/lib/data-access/store/configSlice';
import { PillAttributes } from '../../pillattributes/PillAttributes';
import { Button, DropdownContainer, DropdownTrigger, RelationPill, DropdownItemContainer, DropdownItem, Input } from '@/lib/components';
import { Icon } from '@/lib/components/icon';
import { PopoverContext } from '@/lib/components/layout/Popover';
import { Handle, Position } from 'reactflow';
import { pillWidth } from '@/lib/components/pills/pill.const';
import { isEqual } from 'lodash-es';
import { getDataTypeIcon } from '@/lib/components/DataTypeIcon';
import { RelationNodeAttributes, NodeAttribute, QueryUnionType, defaultGraph } from 'ts-common';
import { attributeShownToggle, setQuerybuilderGraphology, toQuerybuilderGraphology } from '@/lib/data-access/store/sessionSlice';

export const QueryRelationPill = memo((node: SchemaReactflowRelationNode) => {
  const data = node.data;
  const activeQuery = useActiveQuery();
  const graphologyGraph = useMemo(() => toQuerybuilderGraphology(activeQuery?.graph || defaultGraph()), [activeQuery?.graph]);
  const dispatch = useAppDispatch();
  const attributesBeingShown = activeQuery?.attributesBeingShown || [];

  const attributeEdges = useMemo(
    () => activeQuery?.graph.edges.filter(edge => edge.source === node.id && !!edge?.attributes?.sourceHandleData.attributeType) || [],
    [activeQuery?.graph],
  );
  const [openDropdown, setOpenDropdown] = useState(false);

  const [filter, setFilter] = useState<string>('');

  const [depth, setDepth] = useState<{ min: number; max: number }>({
    min: data.depth.min || activeQuery?.settings.depth.min || 1,
    max: data.depth.max || activeQuery?.settings.depth.max || 1,
  });

  // TODO: must do this once design is chosen
  // const [direction, setDirection] = useState<RelationshipHandleArrowType>('right');

  useEffect(() => {
    setDepth({ min: data.depth.min || activeQuery?.settings.depth.min || 1, max: data.depth.max || activeQuery?.settings.depth.max || 1 });
  }, [data.depth]);

  const filteredAttributes = useMemo(() => {
    if (filter == null || filter.length == 0) return data.attributes;

    return data.attributes.filter(attr => {
      return attr.handleData.attributeName?.toLocaleLowerCase().includes(filter.toLocaleLowerCase());
    });
  }, [filter]);

  const onNodeUpdated = () => {
    if (depth.min < 0) {
      dispatch(addWarning('The minimum depth cannot be smaller than 0'));
    } else if (depth.max > 99) {
      dispatch(addWarning('The maximum depth cannot be larger than 99'));
    } else if (depth.min > depth.max) {
      dispatch(addWarning('The minimum depth cannot be larger than the maximum depth'));
    } else {
      graphologyGraph.setNodeAttribute<any>(node.id, 'depth', depth);
      dispatch(setQuerybuilderGraphology(graphologyGraph));
    }
  };

  function removeNode() {
    graphologyGraph.dropNode(node.id);
    dispatch(setQuerybuilderGraphology(graphologyGraph));
  }

  function addAttribute(attribute: NodeAttribute) {
    dispatch(attributeShownToggle(attribute.handleData));
  }

  const unionType = activeQuery?.settings.unionTypes == null ? QueryUnionType.AND : activeQuery?.settings.unionTypes[node.id];

  function isAttributeAdded(attribute: NodeAttribute): boolean {
    return attributesBeingShown.some(x => isEqual(x, attribute.handleData));
  }

  // TODO: must do this once design is chosen
  // const onChangeDirection = () => {
  //   if (direction === 'right') {
  //     setDirection('both');
  //     graphologyGraph.setNodeAttribute<any>(node.id, 'direction', 'both');
  //   } else {
  //     setDirection('right');
  //     graphologyGraph.setNodeAttribute<any>(node.id, 'direction', 'right');
  //   }
  //   dispatch(setQuerybuilderGraphology(graphologyGraph));
  // };

  return (
    <div className="w-fit h-fit pt-3 bg-transparent nowheel">
      <RelationPill
        title={
          <div className="flex flex-row w-full">
            <span className="flex-grow text-justify truncate w-[90%]">{data?.name}</span>
          </div>
        }
        withHandles="horizontal"
        direction={data.direction}
        handleLeft={
          <Handle
            // onDoubleClick={onChangeDirection}
            id={toHandleId(data.leftEntityHandleId)}
            position={Position.Left}
            className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !b-0 !right-0 !left-0'}
            type="target"
          ></Handle>
        }
        handleRight={
          <Handle
            // onDoubleClick={onChangeDirection}
            id={toHandleId(data.rightEntityHandleId)}
            position={Position.Right}
            className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !right-0 !left-0'}
            type="source"
          ></Handle>
        }
      >
        <PillAttributes
          node={node}
          attributes={data?.attributes || []}
          attributeEdges={attributeEdges.map(edge => edge?.attributes)}
          mr={-pillWidth * 0.05}
          unionType={unionType}
        />
      </RelationPill>
    </div>
  );
});

QueryRelationPill.displayName = 'RelationPill';
export default QueryRelationPill;
