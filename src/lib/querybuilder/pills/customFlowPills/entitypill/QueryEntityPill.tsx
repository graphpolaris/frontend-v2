import React, { useMemo, useRef } from 'react';
import { EntityPill } from '@/lib/components/pills';
import { SchemaReactflowEntityNode, toHandleId } from '../../../model';
import { PillAttributes } from '../../pillattributes/PillAttributes';
import { uniqBy } from 'lodash-es';
import { useUpdateNodeInternals, Handle, Position } from 'reactflow';
import { QueryUnionType } from 'ts-common';
import { useActiveQuery } from '@/lib/data-access';

/**
 * Component to render an entity flow element
 * @param {NodeProps} param0 The data of an entity flow element.
 */
export const QueryEntityPill = React.memo((node: SchemaReactflowEntityNode) => {
  const ref = useRef<HTMLDivElement | null>(null);

  const data = node.data;
  if (!data.leftRelationHandleId) throw new Error('EntityFlowElement: data.leftRelationHandleId is undefined');
  if (!data.rightRelationHandleId) throw new Error('EntityFlowElement: data.rightRelationHandleId is undefined');

  const activeQuery = useActiveQuery();
  const attributeEdges = useMemo(
    () => activeQuery?.graph.edges.filter(edge => edge.source === node.id && !!edge?.attributes?.sourceHandleData.attributeType) || [],
    [activeQuery?.graph],
  );

  const uniqueAttributes = useMemo(() => uniqBy(data.attributes, attr => attr.handleData.attributeName), [data.attributes]);
  const unionType = activeQuery?.settings.unionTypes == null ? QueryUnionType.AND : activeQuery?.settings.unionTypes[node.id];

  return (
    <div className="w-fit h-fit nowheel" ref={ref}>
      <EntityPill
        title={
          <div className="flex flex-row justify-between items-center">
            <span className="truncate w-[90%]">{data.name || ''}</span>
          </div>
        }
        withHandles="horizontal"
        handleLeft={
          <Handle
            id={toHandleId(data.leftRelationHandleId)}
            position={Position.Left}
            className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !right-0 !left-0'}
            type="target"
          ></Handle>
        }
        handleRight={
          <Handle
            id={toHandleId(data.rightRelationHandleId)}
            // id="entitySourceRight"
            position={Position.Right}
            className={'!rounded-none !bg-transparent !w-full !h-full !border-0 !right-0 !left-0'}
            type="source"
          ></Handle>
        }
      >
        {data?.attributes && (
          <PillAttributes
            node={node}
            attributes={uniqueAttributes}
            attributeEdges={attributeEdges.map(edge => edge?.attributes)}
            unionType={unionType}
          />
        )}
      </EntityPill>
    </div>
  );
});

QueryEntityPill.displayName = 'QueryEntityPill';

export default QueryEntityPill;
