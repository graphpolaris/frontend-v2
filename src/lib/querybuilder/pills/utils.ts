import { InputNodeType } from 'ts-common';
import styles from './querypills.module.scss';

export const styleHandleMap: Record<InputNodeType, string> = {
  string: styles.handle_logic_string,
  int: styles.handle_logic_int,
  float: styles.handle_logic_float,
  bool: styles.handle_logic_bool,
  date: styles.handle_logic_date,
  time: styles.handle_logic_time,
  datetime: styles.handle_logic_datetime,
  duration: styles.handle_logic_duration,
};
