import { useContext } from 'react';
import { SchemaReactflowEntityNode, SchemaReactflowRelationNode, handleDataFromReactflowToDataId, toHandleId } from '../../model';
import { Handle, Position } from 'reactflow';
import { PillHandle } from '@/lib/components/pills/PillHandle';
import { pillAttributesPadding } from '@/lib/components/pills/pill.const';
import { Button } from '../../..';
import { QueryBuilderDispatcherContext } from '../../panel/QueryBuilderDispatcher';
import { NodeAttribute, QueryUnionType } from 'ts-common';

type PillAttributesItemProps = {
  attribute: NodeAttribute;
  node: SchemaReactflowEntityNode | SchemaReactflowRelationNode;
  className?: string;
  mr?: number;
  icon: string | undefined;
  unionType?: QueryUnionType;
};

export const PillAttributesItem = (props: PillAttributesItemProps) => {
  const { openLogicPillCreate } = useContext(QueryBuilderDispatcherContext);

  if (props.attribute.handleData.attributeName === undefined) {
    throw new Error('attribute.handleData.attributeName is undefined');
  }

  const handleId = toHandleId(handleDataFromReactflowToDataId(props.node, props.attribute));
  const handleType = 'source';

  function shapeForType(unionType: QueryUnionType | undefined) {
    if (unionType == null) return 'square';

    switch (unionType) {
      case QueryUnionType.AND:
        return 'square';
      case QueryUnionType.OR:
        return 'diamond';
    }
  }

  return (
    <div className="px-1.5 py-1 bg-secondary-100 flex justify-between items-center">
      <p className="truncate w-[90%]">{props.attribute.handleData.attributeName}</p>
      <Button
        variantType="secondary"
        variant="ghost"
        size="2xs"
        iconComponent={props.icon}
        onClick={() => {
          openLogicPillCreate(
            {
              nodeId: props.node.id,
              handleId: handleId,
              handleType: handleType,
            },
            {
              x: props.node.xPos + 200,
              y: props.node.yPos + 50,
            },
          );
        }}
      />
      <PillHandle
        mr={-pillAttributesPadding + (props.mr || 0)}
        handleTop="auto"
        position={Position.Right}
        className={`stroke-white${props.className ? ` ${props.className}` : ''}`}
        type={shapeForType(props.unionType)}
      >
        <Handle
          id={handleId}
          type={handleType}
          position={Position.Right}
          className={'!rounded-none !bg-transparent !w-full !h-full !right-0 !left-0 !border-0'}
        ></Handle>
      </PillHandle>
    </div>
  );
};
