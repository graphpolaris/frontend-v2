import { useMemo, useState } from 'react';
import { SchemaReactflowEntityNode, SchemaReactflowRelationNode } from '../../model';
import { isEqual } from 'lodash-es';
import { PillAttributesItem } from './PillAttributesItem';
import { getDataTypeIcon } from '@/lib/components/DataTypeIcon';
import { NodeAttribute, QueryGraphEdges, QueryUnionType } from 'ts-common';
import { useActiveQuery } from '@/lib/data-access';

type PillAttributesProps = {
  node: SchemaReactflowEntityNode | SchemaReactflowRelationNode;
  attributes: NodeAttribute[];
  attributeEdges: (QueryGraphEdges | undefined)[];
  mr?: number;
  className?: string;
  unionType?: QueryUnionType;
};

export const PillAttributes = (props: PillAttributesProps) => {
  const activeQuery = useActiveQuery();
  const attributesBeingShown = activeQuery?.attributesBeingShown || [];

  const attributesOfInterest = useMemo(() => {
    return props.attributes.map(attribute => (attributesBeingShown.findIndex(x => isEqual(x, attribute.handleData)) === -1 ? false : true));
  }, [attributesBeingShown]);

  return (
    <div className={'border-[1px] border-secondary-200 divide-y divide-secondary-200 !z-50'}>
      {attributesOfInterest &&
        attributesOfInterest.map((showing, i) => {
          if (!showing) return null;
          return (
            <PillAttributesItem
              key={props.attributes[i].handleData.attributeName || i}
              node={props.node}
              attribute={props.attributes[i]}
              mr={props.mr}
              className={props.className}
              icon={getDataTypeIcon(props.attributes[i].handleData.attributeType)}
              unionType={props.unionType}
            />
          );
        })}
    </div>
  );
};
