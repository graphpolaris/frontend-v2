import { MultiGraph } from 'graphology';

export function DragRelationPillStarted(id: string, nodes: MultiGraph) {
  // Started dragging relation usecase
}

export function DragRelationPill(id: string, nodes: MultiGraph, dx: number, dy: number) {
  // Code for dragging an relation pill should go here
}

export function DragRelationPillStopped(id: string, nodes: MultiGraph) {
  // Stopped dragging relation pill
}
