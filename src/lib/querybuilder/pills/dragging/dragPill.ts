import { MultiGraph } from 'graphology';
import { DragAttibutePillStopped, DragAttributePill, DragAttributePillStarted } from './dragAttribute';
import { DragAttributesAlong } from './dragAttributesAlong';
import { DragEntityPill, DragEntityPillStarted, DragEntityPillStopped } from './dragEntity';
import { DragRelationPill, DragRelationPillStarted, DragRelationPillStopped } from './dragRelation';
import { XYPosition } from 'ts-common';

export function dragPillStarted(id: string, nodes: MultiGraph) {
  switch (nodes.getNodeAttribute(id, 'type')) {
    case 'attribute':
      DragAttributePillStarted(id, nodes);
      break;
    case 'entity':
      DragEntityPillStarted(id, nodes);
      break;
    case 'relation':
      DragRelationPillStarted(id, nodes);
      break;
  }
}

/**
 * A general drag usecase for any pill, it will select the correct usecase for each pill
 * @param id
 * @param nodes The graphology query builder nodes object
 * @param dx Delta x
 * @param dy Delta y
 * @param position The already updated positioning (dx dy are already applied)
 */
export function movePillTo(id: string, nodes: MultiGraph, dx: number, dy: number, position: XYPosition) {
  // Update the position of the node in the graphology object
  nodes.setNodeAttribute(id, 'x', position.x);
  nodes.setNodeAttribute(id, 'y', position.y);

  // Remove the highlighted attribute from each node
  nodes.forEachNode(node => nodes.removeNodeAttribute(node, 'suggestedForConnection'));

  switch (nodes.getNodeAttribute(id, 'type')) {
    case 'attribute':
      DragAttributePill(id, nodes, dx, dy);
      break;
    case 'entity':
      DragAttributesAlong(id, nodes, dx, dy);
      DragEntityPill(id, nodes, dx, dy);
      break;
    case 'relation':
      DragAttributesAlong(id, nodes, dx, dy);
      DragRelationPill(id, nodes, dx, dy);
      break;
  }
}

export function dragPillStopped(id: string, nodes: MultiGraph) {
  switch (nodes.getNodeAttribute(id, 'type')) {
    case 'attribute':
      DragAttibutePillStopped(id, nodes);
      break;
    case 'entity':
      DragEntityPillStopped(id, nodes);
      break;
    case 'relation':
      DragRelationPillStopped(id, nodes);
      break;
  }

  // Remove all suggestedForConnection attributes
  nodes.forEachNode(node => nodes.removeNodeAttribute(node, 'suggestedForConnection'));
}
