import { MultiGraph } from 'graphology';
import { GetClosestPill } from './getClosestPill';

export function DragAttributePillStarted(id: string, nodes: MultiGraph) {
  // if the attribute is still connected to an entity or relation pill, disconnect
  const es = nodes.outEdges(id);
  es.forEach(e => nodes.dropEdge(e));
}

export function DragAttributePill(id: string, nodes: MultiGraph, dx: number, dy: number) {
  // Get the closes entity or relation node
  const closestNode = GetClosestPill(id, nodes, ['entity', 'relation']);
  // If we found one, highlight it by adding an attribute
  if (closestNode) nodes.setNodeAttribute(closestNode, 'suggestedForConnection', true);
}

export function DragAttibutePillStopped(id: string, nodes: MultiGraph) {
  // If there is currently a node with the suggestedForConnection attribute
  // connect this attribute to it
  nodes.forEachNode((node, { suggestedForConnection }) => {
    if (suggestedForConnection) nodes.addEdge(id, node, { type: 'attribute_connection' });
  });
}
