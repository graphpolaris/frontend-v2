import { MultiGraph } from 'graphology';

export function DragEntityPillStarted(id: string, nodes: MultiGraph) {
  // Started dragging entity usecase
}

export function DragEntityPill(id: string, nodes: MultiGraph, dx: number, dy: number) {
  // Code for dragging an entity pill should go here
}

export function DragEntityPillStopped(id: string, nodes: MultiGraph) {
  // Stopped dragging entity pill
}
