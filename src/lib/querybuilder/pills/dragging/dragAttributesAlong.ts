import Graph from 'graphology';

/**
 * Changes the position of connected attributes.
 * @param id The id of the node which could have attributes connected to it (entity or relation)
 * @param nodes The graphology query builder object
 * @param dx The change in x
 * @param dy The change in y
 * @returns True if any attribute positions were changed
 */
export function DragAttributesAlong(id: string, nodes: Graph, dx: number, dy: number): boolean {
  let didChangeAttributes = false;
  nodes.forEachInNeighbor(id, nb => {
    if (nodes.getNodeAttribute(nb, 'type') == 'attribute') {
      nodes.updateNodeAttribute(nb, 'x', x => x + dx);
      nodes.updateNodeAttribute(nb, 'y', y => y + dy);
      didChangeAttributes = true;
    }
  });

  return didChangeAttributes;
}
