import React, { useCallback, useMemo } from 'react';
import { Connection, Handle, HandleProps, Position, useNodeId, useStore } from 'reactflow';
import { toHandleData, toHandleId } from '../model';
import { Handles, QueryGraphEdgeHandle } from 'ts-common';

const selector = (s: any) => ({
  nodeInternals: s.nodeInternals,
  edges: s.edges,
});

export const FilterHandle = (
  props: {
    handle: QueryGraphEdgeHandle;
    handleTop?: 'auto' | 'fixed';
    hidden?: boolean;
  } & Omit<HandleProps, 'id'> &
    Omit<React.HTMLAttributes<HTMLDivElement>, 'id'>,
) => {
  const outerSize = 8;
  const innerSize = 3;
  const { nodeInternals, edges } = useStore(selector);
  const nodeId = useNodeId();

  const sourceWithLogic = useMemo(() => {
    return (
      props.handle.handleType === Handles.LogicLeft ||
      props.handle.handleType === Handles.LogicRight ||
      props.handle.handleType === Handles.EntityAttribute ||
      props.handle.handleType === Handles.RelationAttribute
    );
  }, [props.handle]);

  const id = useMemo(() => {
    return toHandleId(props.handle);
  }, [props.handle]);

  const isValidConnection = useCallback(
    (connection: Connection) => {
      if (!connection.targetHandle) return false;
      const target = toHandleData(connection.targetHandle);

      // do not allow self connection between two relation nodes
      if (
        props.handle.nodeId === target.nodeId &&
        (props.handle.handleType === Handles.RelationLeft || props.handle.handleType === Handles.RelationRight)
      )
        return false;

      const targetHandleOfTypeLogic =
        target.handleType === Handles.LogicLeft ||
        target.handleType === Handles.LogicRight ||
        target.handleType === Handles.EntityAttribute ||
        target.handleType === Handles.RelationAttribute;

      // only allow if both are logic or non-logic
      if (sourceWithLogic === targetHandleOfTypeLogic) return true;
      else return false;
    },
    [nodeInternals, edges, nodeId, props.handle],
  );

  const style: React.CSSProperties = {
    width: outerSize * 2,
    height: outerSize * 2,
    top: props.handleTop === 'auto' ? `auto` : `calc(2rem - ${outerSize}px)`,
  };
  if (props.position === Position.Left) {
    style.left = outerSize / 2;
  } else {
    style.right = outerSize / 2;
  }

  const handleStyle: React.CSSProperties = {};
  if (props.position === Position.Left) {
    handleStyle.left = 0;
  } else {
    handleStyle.right = 0;
  }

  const innerStyle: React.CSSProperties = { width: innerSize * 2, height: innerSize * 2 };
  innerStyle.top = outerSize / 2 + innerSize / 4;
  if (props.position === Position.Left) {
    innerStyle.left = outerSize / 2 + innerSize / 4;
  } else {
    innerStyle.right = outerSize / 2 + innerSize / 4;
  }

  const innerProps = { ...props };
  delete innerProps.className;
  delete innerProps.style;
  delete innerProps.handleTop;

  return (
    <div className="absolute  " style={style}>
      <Handle
        id={id}
        {...innerProps}
        className={'!rounded-none !bg-transparent !w-full !h-full !border-0'}
        style={handleStyle}
        isValidConnection={isValidConnection}
      ></Handle>
      <div className={'absolute pointer-events-none ' + props.className} style={innerStyle}></div>
    </div>
  );
};
