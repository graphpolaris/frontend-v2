import { useState } from 'react';
import { SchemaReactflowLogicNode, toHandleData } from '../../model';
import { ConnectingNodeDataI } from '../utils/connectorDrop';
import { useDispatch } from 'react-redux';
import { Button, Icon, Tooltip, TooltipContent, TooltipProvider, TooltipTrigger, useActiveQuery } from '../../..';
import { QueryElementTypes, AllLogicDescriptions, AllLogicMap, defaultGraph } from 'ts-common';
import { setQuerybuilderGraphology, toQuerybuilderGraphology } from '@/lib/data-access/store/sessionSlice';

export const QueryBuilderLogicPillsPanel = (props: {
  reactFlowWrapper: HTMLDivElement | null;
  className?: string;
  title?: string;
  onClick: (item: AllLogicDescriptions) => void;
  onDrag?: (item: AllLogicDescriptions) => void;
  connection?: ConnectingNodeDataI | null;
  editNode?: SchemaReactflowLogicNode;
}) => {
  const activeQuery = useActiveQuery();
  const dispatch = useDispatch();
  const graphologyGraph = toQuerybuilderGraphology(activeQuery?.graph || defaultGraph());
  const [selectedOp, setSelectedOp] = useState(-1);
  const [selectedType, setSelectedType] = useState(-1);

  let filterType = props.editNode
    ? props.editNode.data.logic.input.type
    : ((props.connection?.params?.handleId ? toHandleData(props.connection.params.handleId).attributeType : null) as string);
  if (filterType === 'string') filterType = 'string';
  else if (filterType === 'int' || filterType === 'float') filterType = 'number';

  const dataOps = [
    {
      title: 'aggregation',
      description: 'Aggregation Functions',
      icon: 'icon-[ic--baseline-grid-on]',
    },
    {
      title: 'function',
      description: 'Math Functions',
      icon: 'icon-[ic--baseline-functions]',
    },
    {
      title: 'filter',
      description: 'Filter Functions',
      icon: 'icon-[ic--baseline-filter-alt]',
    },
  ];
  const dataTypes = [
    {
      title: 'number',
      description: 'Number',
      icon: 'icon-[ic--baseline-numbers]',
    },
    {
      title: 'string',
      description: 'Text',
      icon: 'icon-[ic--baseline-abc]',
    },
  ].filter(item => !filterType || item.title === filterType);

  const onDragStart = (event: React.DragEvent, value: AllLogicDescriptions) => {
    console.log('drag start');

    event.dataTransfer.setData('application/reactflow', JSON.stringify({ value }));
    event.dataTransfer.effectAllowed = 'move';
    if (props.onDrag) props.onDrag(value);
  };

  const onNewNodeFromPopup = (value: AllLogicDescriptions) => {
    const logic = AllLogicMap[value.key];

    if (props.connection === null || props.connection?.params?.handleId == null) {
      const bounds = props.reactFlowWrapper?.getBoundingClientRect() || { x: 0, y: 0, width: 0, height: 0 };

      const logicNode = graphologyGraph.addLogicPill2Graphology({
        name: value.name,
        type: QueryElementTypes.Logic,
        x: bounds.width / 2,
        y: bounds.height / 2,
        logic: logic,
        attributes: [],
        inputs: {},
      });
    } else {
      const params = props.connection.params;
      const position = props.connection.position;

      const logicNode = graphologyGraph.addLogicPill2Graphology({
        name: value.name,
        type: QueryElementTypes.Logic,
        x: position.x,
        y: position.y,
        logic: logic,
        attributes: [],
        inputs: {},
      });

      if (!logicNode?.id) throw new Error('Logic node has no id');
      if (!logicNode?.name) throw new Error('Logic node has no name');
      if (!params.handleId) throw new Error('Connection has no source or target');

      const sourceHandleData = toHandleData(params.handleId);
      graphologyGraph.addEdge2Graphology(
        graphologyGraph.getNodeAttributes(params.nodeId),
        graphologyGraph.getNodeAttributes(logicNode.id),
        { type: 'connection' },
        { sourceHandleName: sourceHandleData.attributeName, targetHandleName: logic.input.name },
      );
    }

    dispatch(setQuerybuilderGraphology(graphologyGraph));
    props.onClick(value);
  };

  const onEditNodeFromPopup = (value: AllLogicDescriptions) => {
    if (!props.editNode?.id) return;
    const logic = AllLogicMap[value.key];
    const nodeId = props.editNode.id;
    const edges = graphologyGraph.hasNode(nodeId)
      ? graphologyGraph.edges(nodeId).map(edge => ({ id: edge, attributes: graphologyGraph.getEdgeAttributes(edge) }))
      : [];
    console.log('edit node', props.editNode.data, edges);

    // graphologyGraph
    graphologyGraph.addLogicPill2Graphology({
      ...props.editNode.data,
      name: value.name,
      type: QueryElementTypes.Logic,
      logic: logic,
    });

    edges.forEach((edge, i) => {
      graphologyGraph.dropEdge(edge.id);

      if (i >= logic.numExtraInputs + 1) return; // ignore edges not present in new logic pill

      graphologyGraph.addEdge2Graphology(
        graphologyGraph.getNodeAttributes(edge.attributes.sourceHandleData.nodeId),
        graphologyGraph.getNodeAttributes(edge.attributes.targetHandleData.nodeId),
        { type: 'connection' },
        {
          sourceHandleName: edge.attributes.sourceHandleData.attributeName,
          targetHandleName: edge.attributes.targetHandleData.attributeName,
        },
      );
    });

    // graphologyGraph.addEdge2Graphology(
    //   graphologyGraph.getNodeAttributes(params.nodeId),
    //   graphologyGraph.getNodeAttributes(logicNode.id),
    //   { type: 'connection' },
    //   { sourceHandleName: sourceHandleData.attributeName, targetHandleName: logic.input.name },
    // );

    dispatch(setQuerybuilderGraphology(graphologyGraph));
    props.onClick(value);
  };

  const AllLogicMapAvailable = Object.values(AllLogicMap)
    .filter(item => !filterType || item.key.toLowerCase().includes(filterType))
    .filter(item => selectedOp === -1 || item.key.toLowerCase().includes(dataOps?.[selectedOp].title))
    .filter(item => selectedType === -1 || item.key.toLowerCase().includes(dataTypes?.[selectedType].title));

  const dataOpsPresent: { [key: string]: boolean } = {};

  dataOps.forEach(dataOp => {
    const hasMatch = AllLogicMapAvailable.some(item => {
      return item.key.toLowerCase().includes(dataOp.title.toLowerCase());
    });

    dataOpsPresent[dataOp.title] = hasMatch;
  });

  return (
    <div className={props.className + ' card'}>
      {props.title && <h1 className="card-title mb-2 mx-auto">{props.title}</h1>}
      <div className="gap-1 flex justify-center">
        <TooltipProvider delay={50}>
          {dataOps.map((item, index) => (
            <Tooltip key={'QB_Type_' + item.title}>
              <TooltipContent>{item.description}</TooltipContent>
              <TooltipTrigger>
                <Button
                  iconComponent={item.icon}
                  size="sm"
                  variant={selectedOp === index ? 'solid' : 'outline'}
                  disabled={!dataOpsPresent[item.title]}
                  onClick={e => {
                    e.preventDefault();
                    setSelectedOp(index === selectedOp ? -1 : index);
                  }}
                ></Button>
              </TooltipTrigger>
            </Tooltip>
          ))}
          {dataTypes.map((item, index) => (
            <Tooltip key={'QB_Type_' + item.title}>
              <TooltipContent>{item.description}</TooltipContent>
              <TooltipTrigger>
                <Button
                  iconComponent={item.icon}
                  size="sm"
                  variant={selectedType === index ? 'solid' : 'outline'}
                  onClick={e => {
                    e.preventDefault();
                    if (index === selectedType) {
                      setSelectedType(-1);
                    } else {
                      setSelectedType(index);
                    }
                  }}
                ></Button>
              </TooltipTrigger>
            </Tooltip>
          ))}
        </TooltipProvider>
      </div>
      <div className="mt-1 overflow-x-hidden h-[80vh]">
        <TooltipProvider delay={50}>
          <ul className="pb-10 gap-1 flex flex-col w-full h-full">
            {AllLogicMapAvailable.map((item, index) => (
              <Tooltip key={'QB_Options_' + index}>
                <TooltipContent>{item.description}</TooltipContent>
                <TooltipTrigger>
                  <li
                    key={item.key + item.description}
                    className="px-3 py-2 h-fit bg-white border-[1px] border-secondary-500 min-w-72 cursor-pointer"
                  >
                    <span
                      className="flex gap-1"
                      onDragStart={e => onDragStart(e, item)}
                      draggable={true}
                      onClick={() => {
                        if (props.editNode) onEditNodeFromPopup(item);
                        else onNewNodeFromPopup(item);
                      }}
                    >
                      {item.icon && <Icon component={item.icon} size={24} />}
                      <span className="w-full">{item.name}</span>
                      <span className="flex scale-75">
                        {item.key.toLowerCase().includes('filter') && <Icon component="icon-[ic--baseline-filter-alt]" size={20} />}
                        {item.key.toLowerCase().includes('function') && <Icon component="icon-[ic--baseline-functions]" size={20} />}
                        {item.key.toLowerCase().includes('aggregation') && <Icon component="icon-[ic--baseline-grid-on]" size={20} />}
                        {item.key.toLowerCase().includes('number') && <Icon component="icon-[ic--baseline-numbers]" size={20} />}
                        {item.key.toLowerCase().includes('string') && <Icon component="icon-[ic--baseline-abc]" size={20} />}
                      </span>
                    </span>
                  </li>
                </TooltipTrigger>
              </Tooltip>
            ))}
          </ul>
        </TooltipProvider>
      </div>
    </div>
  );
};
