import React from 'react';
import { useAppDispatch, useML } from '@/lib/data-access';
import {
  setCentralityEnabled,
  setCommunityDetectionEnabled,
  setLinkPredictionEnabled,
  setShortestPathEnabled,
} from '@/lib/data-access/store/mlSlice';
import { FormBody, FormTitle } from '@/lib/components/forms';
import { Input } from '@/lib/components/inputs';
import { FeatureEnabled } from '@/lib/components/featureFlags';

export const QueryMLDialog = () => {
  const dispatch = useAppDispatch();
  const ml = useML();

  return (
    <FormBody
      onSubmit={e => {
        e.preventDefault();
      }}
      className="divide-y divide-secondary-200"
    >
      <FormTitle title="Machine Learning Options" className="p-1 text-sm" />

      <div className="flex flex-col p-1 text-sm divide-y divider-secondary-200">
        <FeatureEnabled featureFlag="LINK_PREDICTION">
          <div className="p-1">
            <Input
              size="sm"
              type="boolean"
              label="Link Prediction"
              value={ml.linkPrediction.enabled}
              onChange={() => dispatch(setLinkPredictionEnabled(!ml.linkPrediction.enabled))}
            />
            {ml.linkPrediction.enabled && ml.linkPrediction.result && <span># of predictions: {ml.linkPrediction.result.length}</span>}
            {ml.linkPrediction.enabled && !ml.linkPrediction.result && <span>Loading...</span>}
          </div>
        </FeatureEnabled>

        <FeatureEnabled featureFlag="CENTRALITY">
          <div className="p-1">
            <Input
              size="sm"
              type="boolean"
              label="Centrality"
              value={ml.centrality.enabled}
              onChange={() => dispatch(setCentralityEnabled(!ml.centrality.enabled))}
            />
            {ml.centrality.enabled && Object.values(ml.centrality.result).length > 0 && (
              <span>
                sum of centers:
                {Object.values(ml.centrality.result)
                  .reduce((a, b) => b + a)
                  .toFixed(2)}
              </span>
            )}
            {ml.centrality.enabled && Object.values(ml.centrality.result).length === 0 && <span>No Centers Found</span>}
          </div>
        </FeatureEnabled>

        <FeatureEnabled featureFlag="COMMUNITY_DETECTION">
          <div className="p-1">
            <Input
              size="sm"
              type="boolean"
              label="Community detection"
              value={ml.communityDetection.enabled}
              onChange={() => dispatch(setCommunityDetectionEnabled(!ml.communityDetection.enabled))}
            />
            {ml.communityDetection.enabled && ml.communityDetection.result && (
              <span># of communities: {ml.communityDetection.result.length}</span>
            )}
            {ml.communityDetection.enabled && !ml.communityDetection.result && <span>Loading...</span>}
          </div>
        </FeatureEnabled>

        <FeatureEnabled featureFlag="SHORTEST_PATH">
          <div className="p-1">
            <Input
              size="sm"
              type="boolean"
              label="Shortest path"
              value={ml.shortestPath.enabled}
              onChange={() => dispatch(setShortestPathEnabled(!ml.shortestPath.enabled))}
            />
            {ml.shortestPath.enabled && ml.shortestPath.result?.length > 0 && <span># of hops: {ml.shortestPath.result.length}</span>}
            {ml.shortestPath.enabled && !ml.shortestPath.srcNode && <span>Please select source node</span>}
            {ml.shortestPath.enabled && ml.shortestPath.srcNode && !ml.shortestPath.trtNode && <span>Please select target node</span>}
          </div>
        </FeatureEnabled>
      </div>
    </FormBody>
  );
};
