import { useMemo, useState } from 'react';
import { ConnectingNodeDataI } from '../utils/connectorDrop';
import { useActiveQuery, useActiveSaveState, useSchemaGraph } from '@/lib/data-access';
import { useDispatch } from 'react-redux';
import { toSchemaGraphology } from '@/lib/data-access/store/schemaSlice';
import { SchemaNode, QueryElementTypes, Handles, SchemaEdge, defaultGraph } from 'ts-common';
import { toHandleData } from '../../model';
import { setQuerybuilderGraphology, toQuerybuilderGraphology } from '@/lib/data-access/store/sessionSlice';

export const QueryBuilderRelatedNodesPanel = (props: {
  reactFlowWrapper: HTMLDivElement | null;
  className?: string;
  title?: string;
  onFinished: () => void;
  connection?: ConnectingNodeDataI | null;
}) => {
  const schema = useSchemaGraph();
  const activeQuery = useActiveQuery();
  const activeSS = useActiveSaveState();
  const dispatch = useDispatch();
  const graphologyGraph = toQuerybuilderGraphology(activeQuery?.graph || defaultGraph());
  const schemaGraph = toSchemaGraphology(schema);
  const [selectedType, setSelectedType] = useState<'entity' | 'relation' | 'all'>('all');

  const handleData = props.connection?.params?.handleId ? toHandleData(props.connection.params.handleId) : null;

  const relatedEntities = useMemo<SchemaNode[]>(() => {
    if (!props.connection) return [];
    const type = props.connection.node.type;
    if (type !== QueryElementTypes.Entity && type !== QueryElementTypes.Relation)
      throw new Error('Connection is not an entity or relation');

    if (type === QueryElementTypes.Entity) {
      // find entities that are connected to the current entity
      const nodes = schemaGraph.neighbors(props.connection.node.schemaKey);
      return nodes.map(node => schemaGraph.getNodeAttributes(node) as SchemaNode);
    } else if (type === QueryElementTypes.Relation) {
      // find relations of the current entity
      const attributes = schemaGraph.getEdgeAttributes(props.connection.node.schemaKey);
      return [schemaGraph.getNodeAttributes(attributes.to), schemaGraph.getNodeAttributes(attributes.from)];
    }

    return [];
  }, [schema, activeQuery?.graph, props]);

  const relatedRelations = useMemo<SchemaEdge[]>(() => {
    if (!props.connection) return [];
    const type = props.connection.node.type;
    if (type !== QueryElementTypes.Entity && type !== QueryElementTypes.Relation)
      throw new Error('Connection is not an entity or relation');

    if (type === QueryElementTypes.Entity) {
      // find entities on the edge of the current relation
      const edges = schemaGraph.edges(props.connection.node.schemaKey);
      return edges.map(edge => ({ ...schemaGraph.getEdgeAttributes(edge), label: edge }) as SchemaEdge);
    } else if (type === QueryElementTypes.Relation) {
      // find relations that are connected to the proper neighboring entity of current relation
      const attributes = schemaGraph.getEdgeAttributes(props.connection.node.schemaKey);
      const edges = [...schemaGraph.outboundEdges(attributes.to), ...schemaGraph.inboundEdges(attributes.from)];
      return edges.map(edge => ({ ...schemaGraph.getEdgeAttributes(edge), label: edge }) as SchemaEdge);
    }

    return [];
  }, [schema, activeQuery?.graph, props]);

  const newEntity = (entity: SchemaNode) => {
    if (!props.connection) {
      props.onFinished();
      return;
    }

    const params = props.connection.params;
    const position = props.connection.position;

    const newNode = graphologyGraph.addPill2Graphology(
      {
        type: QueryElementTypes.Entity,
        x: position.x,
        y: position.y,
        name: entity.name,
        schemaKey: entity.name,
        attributes: [],
      },
      schemaGraph.getNodeAttribute(entity.name, 'attributes'),
    );

    if (!newNode?.id) throw new Error('Logic node has no id');
    if (!newNode?.name) throw new Error('Logic node has no name');
    if (!params.handleId) throw new Error('Connection has no source or target');

    if (handleData?.handleType === Handles.EntityRight || handleData?.handleType === Handles.RelationRight)
      graphologyGraph.addEdge2Graphology(graphologyGraph.getNodeAttributes(params.nodeId), newNode);
    else graphologyGraph.addEdge2Graphology(newNode, graphologyGraph.getNodeAttributes(params.nodeId));

    dispatch(setQuerybuilderGraphology(graphologyGraph));
    props.onFinished();
  };
  const newRelation = (relation: SchemaEdge) => {
    if (!props.connection) {
      props.onFinished();
      return;
    }

    const params = props.connection.params;
    const position = props.connection.position;

    const newNode = graphologyGraph.addPill2Graphology(
      {
        type: QueryElementTypes.Relation,
        x: position.x,
        y: position.y,
        schemaKey: relation.label,
        depth: { min: activeQuery?.settings.depth.min || 1, max: activeQuery?.settings.depth.max || -1 },
        name: relation.collection,
        collection: relation.collection,
        attributes: [],
      },
      schemaGraph.getEdgeAttribute(relation.label, 'attributes'),
    );

    if (!newNode?.id) throw new Error('Logic node has no id');
    if (!newNode?.name) throw new Error('Logic node has no name');
    if (!params.handleId) throw new Error('Connection has no source or target');

    if (handleData?.handleType === Handles.EntityRight || handleData?.handleType === Handles.RelationRight)
      graphologyGraph.addEdge2Graphology(graphologyGraph.getNodeAttributes(params.nodeId), newNode);
    else graphologyGraph.addEdge2Graphology(newNode, graphologyGraph.getNodeAttributes(params.nodeId));
    dispatch(setQuerybuilderGraphology(graphologyGraph));
    props.onFinished();
  };

  return (
    <div className={props.className + ' card'}>
      {props.title && <h1 className="card-title mb-7">{props.title}</h1>}
      <div className="overflow-x-hidden h-[75rem] w-full gap-2 pt-3">
        <div className="w-full">
          <button
            className={'btn w-[calc(50%-0.25rem)] mr-2 normal-case text-lg mb-3 ' + (selectedType !== 'relation' ? 'btn-active' : '')}
            onClick={e => {
              e.preventDefault();
              if (selectedType === 'entity') setSelectedType('all');
              else setSelectedType('entity');
            }}
          >
            Related Entities
          </button>
          <button
            className={'btn w-[calc(50%-0.25rem)] text-lg normal-case mb-3 ' + (selectedType !== 'entity' ? 'btn-active' : '')}
            onClick={e => {
              e.preventDefault();
              if (selectedType === 'relation') setSelectedType('all');
              else setSelectedType('relation');
            }}
          >
            Related Relations
          </button>
        </div>
        <div className="w-full flex flex-row gap-2">
          {selectedType !== 'relation' && (
            <ul className="menu p-0 [&_li>*]:rounded-none w-full pb-10">
              {relatedEntities.map(entity => {
                return (
                  <button className="btn btn-sm border-accent-400 normal-case" key={entity.name} onClick={() => newEntity(entity)}>
                    {entity.name}
                  </button>
                );
              })}
            </ul>
          )}
          {selectedType !== 'entity' && (
            <ul className="menu p-0 [&_li>*]:rounded-none w-full pb-10">
              {relatedRelations.map(relation => {
                return (
                  <button className="btn btn-sm border-primary-400 normal-case" key={relation.name} onClick={() => newRelation(relation)}>
                    {relation.name}
                  </button>
                );
              })}
            </ul>
          )}
        </div>
      </div>
    </div>
  );
};
