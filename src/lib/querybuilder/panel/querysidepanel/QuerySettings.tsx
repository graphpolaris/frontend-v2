import { useEffect } from 'react';
import React from 'react';
import { Layouts } from '@/lib/graph-layout';
import { Input } from '@/lib/components/inputs';
import { FormActions } from '@/lib/components';
import { useActiveQuery, useAppDispatch } from '@/lib/data-access';
import { addWarning } from '@/lib/data-access/store/configSlice';
import { QueryBuilderSettings, defaultQuery } from 'ts-common';
import { setQuerybuilderSettings } from '@/lib/data-access/store/sessionSlice';

export const QuerySettings = React.forwardRef<HTMLDivElement, object>((props, ref) => {
  const activeQuery = useActiveQuery();
  const dispatch = useAppDispatch();
  const [state, setState] = React.useState<QueryBuilderSettings>(activeQuery?.settings || defaultQuery().settings);

  useEffect(() => {
    if (!activeQuery) return;
    setState(activeQuery.settings);
  }, [activeQuery?.settings]);

  function submit() {
    if (state.depth.min < 0) {
      dispatch(addWarning('The minimum depth cannot be smaller than 0'));
    } else if (state.depth.max > 99) {
      dispatch(addWarning('The maximum depth cannot be larger than 99'));
    } else if (state.depth.min > state.depth.max) {
      dispatch(addWarning('The minimum depth cannot be larger than the maximum depth'));
    } else {
      dispatch(setQuerybuilderSettings(state));
    }
  }

  return (
    <div className="flex flex-col w-full gap-2 p-2">
      <span className="text-xs font-bold">Query Settings</span>
      <Input
        size="sm"
        type="boolean"
        value={state.autocompleteRelation}
        label="Autocomplete Edges"
        tooltip="When enabled, if you drag a relationship to the query, the query builder will automatically add the entity nodes it is connected to."
        onChange={(value: boolean) => {
          setState({ ...state, autocompleteRelation: value as any });
        }}
      />
      <Input
        type="number"
        label="Min Depth Default"
        size="sm"
        inline
        value={state.depth.min}
        onChange={e => setState({ ...state, depth: { min: e, max: state.depth.max } })}
        placeholder="0"
        min={0}
        max={state.depth.max}
        onKeyDown={e => {
          if (e.key === 'Enter') {
            submit();
          }
        }}
      />
      <Input
        type="number"
        label="Max Depth Default"
        size="sm"
        inline
        value={state.depth.max}
        onChange={e => setState({ ...state, depth: { max: e, min: state.depth.min } })}
        placeholder="0"
        min={state.depth.min}
        max={99}
        onKeyDown={e => {
          if (e.key === 'Enter') {
            submit();
          }
        }}
      />

      <Input
        type="dropdown"
        inline
        label="Default Layout"
        value={state.layout}
        onChange={e => setState({ ...state, layout: e as any })}
        options={['manual', ...Object.entries(Layouts).map(([k, v]) => v)]}
      />

      <FormActions
        size={'sm'}
        onApply={() => {
          submit();
        }}
      />
    </div>
  );
});
