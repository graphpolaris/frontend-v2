import {
  useActiveQuery,
  useActiveSaveState,
  useActiveSaveStateAuthorization,
  useAppDispatch,
  useConfig,
  useQuerybuilderHash,
  useSchemaGraph,
  useSearchResultQB,
} from '@/lib/data-access/store';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import ReactFlow, {
  Background,
  Connection,
  Edge,
  HandleType,
  Node,
  NodeChange,
  NodePositionChange,
  OnConnectStartParams,
  ReactFlowInstance,
  ReactFlowProvider,
  isNode,
  useReactFlow,
} from 'reactflow';
import { Dialog, DialogContent } from '../../components/layout/Dialog';
import { addError } from '../../data-access/store/configSlice';
import { toSchemaGraphology } from '../../data-access/store/schemaSlice';
import { LayoutFactory } from '../../graph-layout';
import { SchemaReactflowLogicNode, createReactFlowElements, toHandleData } from '../model';
import { ConnectionDragLine, ConnectionLine, QueryEntityPill, QueryRelationPill } from '../pills';
import { QueryLogicPill } from '../pills/customFlowPills/logicpill/QueryLogicPill';
import { dragPillStarted, movePillTo } from '../pills/dragging/dragPill';
import styles from './querybuilder.module.scss';
import { QueryBuilderLogicPillsPanel } from './querysidepanel/QueryBuilderLogicPillsPanel';
import { QueryBuilderRelatedNodesPanel } from './querysidepanel/QueryBuilderRelatedNodesPanel';
import { ConnectingNodeDataI } from './utils/connectorDrop';
import { QueryBuilderDispatcherContext } from './QueryBuilderDispatcher';
import { QueryBuilderNav, QueryBuilderToggleSettings } from './QueryBuilderNav';
import html2canvas from 'html2canvas';
import { QueryBuilderContextMenu } from './QueryBuilderContextMenu';
import { QueryElementTypes, AllLogicMap, isLogicHandle, defaultGraph } from 'ts-common';
import { setQuerybuilderGraphology, toQuerybuilderGraphology } from '@/lib/data-access/store/sessionSlice';

export type QueryBuilderProps = {
  onRunQuery?: (useCached: boolean) => void;
};

const nodeTypes = {
  entity: QueryEntityPill,
  relation: QueryRelationPill,
  logic: QueryLogicPill,
};

const edgeTypes = {
  connection: ConnectionLine,
  attribute_connection: ConnectionLine,
};

/**
 * This is the main querybuilder component. It is responsible for holding all pills and fire off the visual part of the querybuilder panel logic
 */
export const QueryBuilderInner = (props: QueryBuilderProps) => {
  const [toggleSettings, setToggleSettings] = useState<QueryBuilderToggleSettings>();
  const reactFlowWrapper = useRef<HTMLDivElement>(null);
  const reactFlowRef = useRef<HTMLDivElement>(null);
  const activeSS = useActiveSaveState();
  const activeQuery = useActiveQuery();
  const saveStateAuthorization = useActiveSaveStateAuthorization();

  const schemaGraph = useSchemaGraph();
  const schema = useMemo(() => toSchemaGraphology(schemaGraph), [schemaGraph]);
  const qbHash = useQuerybuilderHash();
  const config = useConfig();
  const dispatch = useAppDispatch();
  const isDraggingPill = useRef(false);
  const connectingNodeId = useRef<ConnectingNodeDataI | null>(null);
  const editLogicNode = useRef<SchemaReactflowLogicNode | undefined>(undefined);
  const reactFlow = useReactFlow();
  const isEdgeUpdating = useRef(false);
  const isOnConnect = useRef(false);
  const graphologyGraph = useMemo(() => toQuerybuilderGraphology(activeQuery?.graph ?? defaultGraph()), [activeQuery?.graph, qbHash]);
  const elements = useMemo(() => createReactFlowElements(graphologyGraph), [activeQuery?.graph, qbHash]);
  const searchResults = useSearchResultQB();
  const reactFlowInstanceRef = useRef<ReactFlowInstance | null>(null);
  const [allowZoom, setAllowZoom] = useState(true);
  const [contextMenuOpen, setContextMenuOpen] = useState<{
    open: boolean;
    node?: Node;
    position?: { x: number; y: number };
  }>({
    open: false,
  });

  useEffect(() => {
    const searchResultKeys = new Set([...searchResults.nodes.map(node => node.key), ...searchResults.edges.map(edge => edge.key)]);

    elements.nodes.forEach(node => {
      const isNodeInSearchResults = searchResultKeys.has(node?.id);
      if (isNodeInSearchResults && !graphologyGraph.getNodeAttribute(node?.id, 'selected')) {
        graphologyGraph.setNodeAttribute(node?.id, 'selected', true);
      } else if (!isNodeInSearchResults && graphologyGraph.getNodeAttribute(node?.id, 'selected')) {
        graphologyGraph.setNodeAttribute(node?.id, 'selected', false);
      }
    });

    dispatch(setQuerybuilderGraphology(graphologyGraph));
  }, [searchResults]);

  const onInit = (reactFlowInstance: ReactFlowInstance) => {
    setTimeout(() => reactFlow.fitView(), 0);
  };

  /**
   * Clears all nodes in the graph.
   *  TODO: only works if the node is clicked and not moved (maybe use onSelectionChange)
   */
  function onNodesDelete(nodes: Node[]) {
    if (!saveStateAuthorization.query.W) return;
    nodes.forEach(n => {
      graphologyGraph.dropNode(n.id);
    });

    // Dispatch the new graphology object, so reactflow will get re-rendered
    dispatch(setQuerybuilderGraphology(graphologyGraph));
  }

  /**
   * TODO?
   */
  function onNodesChange(nodes: NodeChange[]) {
    nodes.forEach(n => {
      if (n.type !== 'position') {
        // updated = true;
        // graphologyGraph.updateAttributes(n.id, n.data);
      } else {
        const node = n as NodePositionChange;
        // Get the node in the elements list to get the previous location
        const pNode = elements.nodes.find(e => e?.id === node?.id);
        if (!(pNode && isNode(pNode)) || !node?.position) return;
        // This is then used to calculate the delta position
        const dx = node.position.x - pNode.position.x;
        const dy = node.position.y - pNode.position.y;
        // Check if we started dragging, if so, call the drag started usecase
        if (!isDraggingPill.current) {
          dragPillStarted(node.id, graphologyGraph);
          isDraggingPill.current = true;
        }
        // Call the drag usecase
        movePillTo(node.id, graphologyGraph, dx, dy, node.position);
        // Dispatch the new graphology object, so reactflow will get rerendered
        dispatch(setQuerybuilderGraphology(graphologyGraph));
      }
    });
  }

  const onDragOver = (event: React.DragEvent<HTMLDivElement>): void => {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  };

  const onAddSchemaToQueryBuilder = () => {
    if (!activeQuery) return;

    // TODO: incomplete (not being shown) for now due to lack of "OPTIONAL MATCH" support in the backend

    const addedNodes: Record<string, string> = {};
    // loop through all nodes in the schema and add them to the graph
    schemaGraph.nodes.forEach(node => {
      if (!node.key) return;

      const { id } = graphologyGraph.addPill2Graphology(
        {
          type: QueryElementTypes.Entity,
          // x: position.x - mouse_x,
          // y: position.y - mouse_y,
          x: 0,
          y: 0,
          name: node.key,
          schemaKey: node.key,
          attributes: [],
        },
        schema.getNodeAttribute(node.key, 'attributes'),
      );
      if (!id) return;
      addedNodes[node.key] = id;
    });

    // loop all edges in the schema and add them to the graph
    schemaGraph.edges.forEach(edge => {
      if (!edge.key || !edge.source || !edge.target) return;
      const fromNode = graphologyGraph.getNodeAttributes(addedNodes[edge.source]);
      const toNode = graphologyGraph.getNodeAttributes(addedNodes[edge.target]);
      if (!fromNode || !toNode) return;

      const relation = graphologyGraph.addPill2Graphology(
        {
          type: QueryElementTypes.Relation,
          // x: position.x,
          // y: position.y,
          x: 0,
          y: 0,
          depth: { min: activeQuery.settings.depth.min, max: activeQuery.settings.depth.max },
          name: edge.attributes?.collection,
          schemaKey: edge.attributes?.name,
          collection: edge.attributes?.collection,
          attributes: [],
        },
        schema.getEdgeAttribute(edge.key, 'attributes'),
      );

      graphologyGraph.addEdge2Graphology(fromNode, relation, {
        type: 'connection',
        sourceHandleData: toHandleData(edge.source),
        targetHandleData: toHandleData(edge.key),
      });

      graphologyGraph.addEdge2Graphology(relation, toNode, {
        type: 'connection',
        sourceHandleData: toHandleData(edge.key),
        targetHandleData: toHandleData(edge.target),
      });
    });

    dispatch(setQuerybuilderGraphology(graphologyGraph));
  };

  /**
   * The onDrop is called when the user drops an element from the schema onto the QueryBuilder.
   * In the onDrop query elements will be created based on the data stored in the drag event (datastrasfer).
   * @param event Drag event.
   */
  const onDrop = (event: React.DragEvent<HTMLDivElement>): void => {
    if (!activeQuery) {
      console.error('No active query');
      return;
    }

    event.preventDefault();

    // The dropped element should be a valid reactflow element
    const data: string = event.dataTransfer.getData('application/reactflow');
    if (data.length == 0 || !reactFlow) return;

    const mouse_x = parseFloat(event.dataTransfer.getData('mouse_x'));
    const mouse_y = parseFloat(event.dataTransfer.getData('mouse_y'));

    const dragData = JSON.parse(data);
    const position = reactFlow.screenToFlowPosition({
      x: event.clientX,
      y: event.clientY,
    });

    switch (dragData.type) {
      case QueryElementTypes.Entity:
        graphologyGraph.addPill2Graphology(
          {
            type: QueryElementTypes.Entity,
            x: position.x - mouse_x,
            y: position.y - mouse_y,
            name: dragData.name,
            schemaKey: dragData.name,
            attributes: [],
          },
          schema.getNodeAttribute(dragData.name, 'attributes'),
        );

        dispatch(setQuerybuilderGraphology(graphologyGraph));
        break;
      // Creates a relation element and will also create the 2 related entities together with the connections
      case QueryElementTypes.Relation: {
        const relation = graphologyGraph.addPill2Graphology(
          {
            type: QueryElementTypes.Relation,
            x: position.x,
            y: position.y,
            depth: { min: activeQuery.settings.depth.min, max: activeQuery.settings.depth.max },
            name: dragData.collection,
            schemaKey: dragData.label,
            collection: dragData.collection,
            attributes: [],
          },
          schema.getEdgeAttribute(dragData.label, 'attributes'),
        );

        if (config.autoSendQueries) {
          // sendQuery();
        }

        if (activeQuery.settings.autocompleteRelation == true) {
          let fromNodeID: any = null;
          let toNodeID: any = null;
          schema.nodes().forEach((node: string) => {
            if (node === dragData.from) fromNodeID = node;
            if (node === dragData.to) toNodeID = node;

            if (fromNodeID && toNodeID) return;
          });

          if (fromNodeID && toNodeID) {
            const fromNode = graphologyGraph.addPill2Graphology(
              {
                type: QueryElementTypes.Entity,
                x: position.x - 180,
                y: position.y,
                name: fromNodeID,
                schemaKey: fromNodeID,
                attributes: [],
              },
              schema.getNodeAttribute(fromNodeID, 'attributes'),
            );

            const toNode = graphologyGraph.addPill2Graphology(
              {
                type: QueryElementTypes.Entity,
                x: position.x + 250,
                y: position.y,
                name: toNodeID,
                schemaKey: toNodeID,
                attributes: [],
              },
              schema.getNodeAttribute(toNodeID, 'attributes'),
            );

            graphologyGraph.addEdge2Graphology(fromNode, relation, {
              type: 'connection',
              sourceHandleData: toHandleData(fromNodeID),
              targetHandleData: toHandleData(dragData.collection),
            });

            graphologyGraph.addEdge2Graphology(relation, toNode, {
              type: 'connection',
              sourceHandleData: toHandleData(dragData.collection),
              targetHandleData: toHandleData(toNodeID),
            });
          }
        }

        dispatch(setQuerybuilderGraphology(graphologyGraph));
        break;
      }
      default: {
        const logic = AllLogicMap[dragData.value.key];
        const firstLeftLogicInput = logic.input;
        if (!firstLeftLogicInput) return;

        const logicNode = graphologyGraph.addLogicPill2Graphology({
          name: dragData.value.name,
          type: QueryElementTypes.Logic,
          x: position.x,
          y: position.y,
          logic: logic,
          attributes: [],
          inputs: {},
        });

        dispatch(setQuerybuilderGraphology(graphologyGraph));
      }
    }
  };

  const onNodeMouseEnter = (event: React.MouseEvent, node: Node) => {
    setAllowZoom(false);
  };

  const onNodeMouseLeave = (event: React.MouseEvent, node: Node) => {
    setAllowZoom(true);
  };

  const onConnect = useCallback(
    (connection: Connection) => {
      if (!isEdgeUpdating.current) {
        isOnConnect.current = true;
        if (!connection.sourceHandle || !connection.targetHandle) throw new Error('Connection has no source or target');

        graphologyGraph.addEdge(connection.source, connection.target, {
          type: 'connection',
          sourceHandleData: toHandleData(connection.sourceHandle),
          targetHandleData: toHandleData(connection.targetHandle),
        });
        dispatch(setQuerybuilderGraphology(graphologyGraph));
      }
    },
    [activeQuery?.graph],
  );

  const onConnectStart = useCallback(
    (event: React.MouseEvent | React.TouchEvent, params: OnConnectStartParams) => {
      if (!params?.handleId) return;

      const node = graphologyGraph.getNodeAttributes(params.nodeId);
      const handleData = toHandleData(params.handleId);

      connectingNodeId.current = {
        params,
        node,
        position: { x: 0, y: 0 },
        attribute: { handleData: handleData },
      };
    },
    [activeQuery?.graph],
  );

  const onConnectEnd = useCallback(
    (event: any) => {
      const targetIsPane = event.target.classList.contains('react-flow__pane');
      if (isOnConnect.current) {
        isOnConnect.current = false;
        return;
      }

      if (targetIsPane && !isEdgeUpdating.current) {
        let clientX: number = 0;
        let clientY: number = 0;

        if ('touches' in event) clientX = event?.touches?.[0]?.clientX;
        else if ('clientX' in event) clientX = event?.clientX;
        if ('touches' in event) clientY = event?.touches?.[0]?.clientY;
        else if ('clientY' in event) clientY = event?.clientY;

        const position = reactFlow.screenToFlowPosition({ x: clientX, y: clientY });
        if (connectingNodeId?.current) connectingNodeId.current.position = position;

        if (!connectingNodeId?.current?.params?.handleId) {
          dispatch(addError('Connection has no source or target handle id'));
          return;
        } else {
          const data = toHandleData(connectingNodeId.current.params.handleId);
          if (isLogicHandle(data.handleType)) setToggleSettings('logic');
          else setToggleSettings('relatedNodes');
        }

        // setToggleSettings('logic');
      }
    },
    [reactFlow.project],
  );

  const onEdgeUpdateStart = useCallback(() => {
    isEdgeUpdating.current = true;
  }, []);

  const onEdgeUpdate = useCallback(
    (oldEdge: Edge, newConnection: Connection) => {
      if (isEdgeUpdating.current) {
        isEdgeUpdating.current = false;

        if (graphologyGraph.hasEdge(oldEdge.id)) {
          graphologyGraph.dropEdge(oldEdge.id);
        }

        if (!newConnection.sourceHandle || !newConnection.targetHandle) throw new Error('Connection has no source or target');
        graphologyGraph.addEdge(newConnection.source, newConnection.target, {
          type: 'connection',
          sourceHandleData: toHandleData(newConnection.sourceHandle),
          targetHandleData: toHandleData(newConnection.targetHandle),
        });
        dispatch(setQuerybuilderGraphology(graphologyGraph));
      }
    },
    [activeQuery?.graph],
  );

  const onEdgeUpdateEnd = useCallback(
    (event: MouseEvent | TouchEvent, edge: Edge, handleType: HandleType) => {
      if (isEdgeUpdating.current) {
        if (graphologyGraph.hasEdge(edge.id)) {
          graphologyGraph.dropEdge(edge.id);
        }
        dispatch(setQuerybuilderGraphology(graphologyGraph));
      }
      isEdgeUpdating.current = false;
    },
    [activeQuery?.graph],
  );

  function applyLayout() {
    if (!activeQuery) return;
    if (activeQuery.settings.layout !== 'manual') {
      const factory = new LayoutFactory();
      factory.createLayout(activeQuery.settings.layout).layout(graphologyGraph);
      dispatch(setQuerybuilderGraphology(graphologyGraph));
    }
  }

  const fitView = () => {
    if (reactFlowInstanceRef.current) {
      reactFlowInstanceRef.current.fitView();
    }
  };

  const onNodeContextMenu = (event: React.MouseEvent, node: Node) => {
    if (event.shiftKey) return;
    event.preventDefault();
    setContextMenuOpen({ open: true, node: node, position: { x: event.clientX, y: event.clientY } });
  };

  const onMouseDown = useCallback(
    (event: React.MouseEvent | React.TouchEvent) => {
      if (contextMenuOpen.open) {
        setContextMenuOpen({ ...contextMenuOpen, open: false });
      }
    },
    [contextMenuOpen],
  );

  useEffect(() => {
    try {
      applyLayout();
    } catch (e) {
      console.error(e);
    }
    setContextMenuOpen({ ...contextMenuOpen, open: false });
  }, [activeQuery?.settings]);

  const handleScreenshot = async () => {
    if (reactFlowRef.current) {
      try {
        const canvas = await html2canvas(reactFlowRef.current);
        const screenshotUrl = canvas.toDataURL('image/jpeg');
        const link = document.createElement('a');
        link.href = screenshotUrl;
        link.download = 'querybuilder.png';
        link.click();
      } catch (error) {
        dispatch(addError('Screenshot failed.'));
      }
    }
  };

  return (
    <QueryBuilderDispatcherContext.Provider
      value={{
        openLogicPillUpdate: node => {
          editLogicNode.current = node;
          setToggleSettings('logic');
        },
        openLogicPillCreate: (params, position) => {
          if (!params?.handleId) return;

          const node = graphologyGraph.getNodeAttributes(params.nodeId);
          const handleData = toHandleData(params.handleId);
          connectingNodeId.current = {
            params,
            node,
            position: position || { x: 0, y: 0 },
            attribute: { handleData: handleData },
          };
          setToggleSettings('logic');
        },
      }}
    >
      <QueryBuilderContextMenu
        open={contextMenuOpen.open}
        node={contextMenuOpen.node}
        position={contextMenuOpen.position}
        reactFlowWrapper={reactFlowWrapper}
        reactFlow={reactFlow}
        onClose={() => {
          setContextMenuOpen({ ...contextMenuOpen, open: false });
        }}
      />
      <div ref={reactFlowWrapper} className="h-full w-full flex flex-col relative">
        <QueryBuilderNav
          toggleSettings={toggleSettings}
          onFitView={fitView}
          onRunQuery={(useCached: boolean) => {
            if (props.onRunQuery) props.onRunQuery(useCached);
          }}
          onScreenshot={handleScreenshot}
          onLogic={() => {
            if (toggleSettings === 'logic') setToggleSettings(undefined);
            else setToggleSettings('logic');
          }}
        />

        <Dialog
          open={toggleSettings === 'logic'}
          onOpenChange={ret => {
            if (!ret) setToggleSettings(undefined);
          }}
        >
          <DialogContent className="w-fit rounded-none">
            <QueryBuilderLogicPillsPanel
              onClick={v => {
                connectingNodeId.current = null;
                editLogicNode.current = undefined;
                setToggleSettings(undefined);
              }}
              reactFlowWrapper={reactFlowWrapper.current}
              title="Available Logic Pills"
              className="max-h-[75vh] min-h-[45vh]"
              onDrag={() => {}}
              connection={connectingNodeId?.current}
              editNode={editLogicNode.current}
            />
          </DialogContent>
        </Dialog>
        <Dialog
          open={toggleSettings === 'relatedNodes'}
          onOpenChange={ret => {
            if (!ret) setToggleSettings(undefined);
          }}
        >
          <DialogContent>
            <QueryBuilderRelatedNodesPanel
              onFinished={() => {
                connectingNodeId.current = null;
                setToggleSettings(undefined);
              }}
              reactFlowWrapper={reactFlowWrapper.current}
              title="Related nodes available to add to the query"
              className="min-h-[75vh] max-h-[75vh]"
              connection={connectingNodeId?.current}
            />
          </DialogContent>
        </Dialog>
        <svg height={0}>
          <defs>
            <marker id="arrowIn" markerWidth="9" markerHeight="10" refX={0} refY="5" orient="auto">
              <polygon points="0 0, 9 5, 0 10" />
            </marker>
            <marker id="arrowOut" markerWidth="10" markerHeight="10" refX={0} refY="5" orient="auto">
              <polygon points="0 0, 10 5, 0 10" />
            </marker>
          </defs>
        </svg>
        <ReactFlow
          ref={reactFlowRef}
          edges={elements.edges}
          nodes={elements.nodes}
          snapGrid={[10, 10]}
          zoomOnScroll={allowZoom}
          snapToGrid
          nodeTypes={nodeTypes}
          edgeTypes={edgeTypes}
          connectionLineComponent={ConnectionDragLine}
          onMouseDownCapture={onMouseDown}
          // connectionMode={ConnectionMode.Loose}
          onInit={reactFlowInstance => {
            reactFlowInstanceRef.current = reactFlowInstance;
            onInit(reactFlowInstance);
          }}
          onNodesChange={saveStateAuthorization.query.W ? onNodesChange : () => {}}
          onDragOver={saveStateAuthorization.query.W ? onDragOver : () => {}}
          onConnect={saveStateAuthorization.query.W ? onConnect : () => {}}
          onConnectStart={saveStateAuthorization.query.W ? onConnectStart : () => {}}
          onConnectEnd={saveStateAuthorization.query.W ? onConnectEnd : () => {}}
          // onNodeMouseEnter={onNodeMouseEnter}
          // onNodeMouseLeave={onNodeMouseLeave}
          onEdgeUpdate={saveStateAuthorization.query.W ? onEdgeUpdate : () => {}}
          onEdgeUpdateStart={saveStateAuthorization.query.W ? onEdgeUpdateStart : () => {}}
          onEdgeUpdateEnd={saveStateAuthorization.query.W ? onEdgeUpdateEnd : () => {}}
          onDrop={saveStateAuthorization.query.W ? onDrop : () => {}}
          // onContextMenu={onContextMenu}
          onNodeContextMenu={saveStateAuthorization.query.W ? onNodeContextMenu : () => {}}
          // onNodesDelete={onNodesDelete}
          // onNodesChange={onNodesChange}
          deleteKeyCode="Backspace"
          className={styles.reactflow}
          proOptions={{ hideAttribution: true }}
        >
          <Background gap={10} size={0.7} />
        </ReactFlow>
      </div>
    </QueryBuilderDispatcherContext.Provider>
  );
};

export const QueryBuilder = (props: QueryBuilderProps) => {
  return (
    <div className="query-panel flex w-full h-full border bg-light">
      <ReactFlowProvider>
        <QueryBuilderInner {...props} />
      </ReactFlowProvider>
    </div>
  );
};

export default QueryBuilder;
