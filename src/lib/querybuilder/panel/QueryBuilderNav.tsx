import { useMemo, useState } from 'react';
import { ControlContainer, TooltipProvider, Tooltip, TooltipTrigger, Button, TooltipContent, Input } from '@/lib/components';
import { Popover, PopoverTrigger, PopoverContent } from '@/lib/components/popover';
import {
  useActiveQuery,
  useActiveSaveState,
  useActiveSaveStateAuthorization,
  useAppDispatch,
  useGraphQueryResult,
  useML,
} from '../../data-access';
import {
  clearQB,
  setQuerybuilderSettings,
  removeQueryByID,
  setActiveQueryID,
  setQueryName,
  addNewQuery,
} from '../../data-access/store/sessionSlice';
import { QueryMLDialog } from './querysidepanel/QueryMLDialog';
import { QuerySettings } from './querysidepanel/QuerySettings';
import { ManualQueryDialog } from './ManualQueryDialog';
import { wsAddQuery, wsDeleteQuery, wsManualQueryRequest, wsUpdateQuery } from '../../data-access/broker';
import { Tabs, Tab } from '@/lib/components/tabs';
import { addError } from '@/lib/data-access/store/configSlice';

export type QueryBuilderToggleSettings = 'settings' | 'ml' | 'logic' | 'relatedNodes' | undefined;

export type QueryBuilderNavProps = {
  toggleSettings: QueryBuilderToggleSettings;
  onFitView: () => void;
  onRunQuery: (useCached: boolean) => void;
  onScreenshot: () => void;
  onLogic: () => void;
};

export const QueryBuilderNav = (props: QueryBuilderNavProps) => {
  const dispatch = useAppDispatch();
  const activeQuery = useActiveQuery();
  const activeSS = useActiveSaveState();
  const result = useGraphQueryResult();
  const resultSize = useMemo(() => {
    if (!result) return 0;
    return result.nodes.length;
  }, [result]);
  const totalSize = useMemo(() => {
    if (!activeQuery || !result || !activeSS) return 0;

    const nodeCounts = activeQuery.graph.nodes
      .filter(x => x.attributes.type == 'entity')
      .map(x => result.nodeCounts[`${x.key}_count`] ?? 0);

    return nodeCounts.reduce((a, b) => a + b, 0);
  }, [result]);
  const ml = useML();
  const saveStateAuthorization = useActiveSaveStateAuthorization();
  const [editingIdx, setEditingIdx] = useState<{ idx: number; text: string } | null>(null);

  /**
   * Clears all nodes in the graph.
   */
  function clearAllNodes() {
    dispatch(clearQB());
  }

  const mlEnabled = ml.linkPrediction.enabled || ml.centrality.enabled || ml.communityDetection.enabled || ml.shortestPath.enabled;

  const handleManualQuery = (query: string) => {
    wsManualQueryRequest({ query: query });
  };

  if (!activeSS || !activeQuery) {
    console.debug('No active query found in query nav');
    return null;
  }

  function updateQueryName(text: string) {
    if (!activeSS || !activeQuery) return;
    wsUpdateQuery(
      {
        saveStateID: activeSS.id,
        query: { ...activeQuery, name: text },
      },
      (data, state) => {
        if (state !== 'success') {
          addError('Failed to update query');
        }

        dispatch(setQueryName(text));
        setEditingIdx(null);
      },
    );
  }

  return (
    <div className="sticky shrink-0 top-0 left-0 right-0 flex items-stretch justify-start h-7 bg-secondary-100 border-b border-secondary-200 max-w-full">
      <div className="flex items-center px-2">
        <h1 className="text-xs font-semibold text-secondary-600 truncate">Query builder</h1>
      </div>
      <div className="flex items-center px-0.5 gap-1 border-l border-secondary-200">
        <TooltipProvider>
          <Tooltip>
            <TooltipTrigger>
              <Button
                as="a"
                variantType="secondary"
                variant="ghost"
                size="xs"
                iconComponent="icon-[ic--baseline-add]"
                onClick={async () => {
                  wsAddQuery({ saveStateID: activeSS.id }, (query, status) => {
                    if (status !== 'success' || query == null || !query.id || query.id < 0) {
                      console.error('Failed to add query');
                      addError('Failed to add query');
                      return;
                    }

                    console.log('Query added', query);
                    dispatch(addNewQuery(query));
                  });
                }}
              />
            </TooltipTrigger>
            <TooltipContent>
              <p>Add query</p>
            </TooltipContent>
          </Tooltip>
        </TooltipProvider>
      </div>
      <Tabs
        className={`-my-px overflow-x-auto overflow-y-hidden no-scrollbar divide-x divide-secondary-200 border-x ${result.queryingBackend ? 'pointer-events-none' : ''}`}
      >
        {activeSS.queryStates.openQueryArray
          .filter(query => query.id != null)
          .map((query, i) => (
            <Tab
              text=""
              activeTab={query.id === activeQuery?.id}
              key={i}
              onClick={() => {
                if (query.id == null) return;
                dispatch(setActiveQueryID(query.id));
              }}
            >
              <>
                {editingIdx?.idx === i ? (
                  <Input
                    type="text"
                    size="xs"
                    value={editingIdx.text}
                    label=""
                    onChange={e => {
                      setEditingIdx({ idx: i, text: e });
                    }}
                    onBlur={() => {
                      updateQueryName(editingIdx.text);
                    }}
                    onKeyDown={e => {
                      if (e.key === 'Enter') {
                        updateQueryName(editingIdx.text);
                      }
                    }}
                    className="w-20"
                    style={{
                      border: 'none',
                      boxShadow: 'none',
                      background: 'none',
                    }}
                    autoFocus
                  />
                ) : (
                  <div
                    onDoubleClick={e => {
                      e.stopPropagation();
                      dispatch(setActiveQueryID(query.id || -1));
                      setEditingIdx({ idx: i, text: query.name ?? '' });
                    }}
                  >
                    {query.name ?? 'Query'}
                  </div>
                )}
                {activeSS.queryStates.openQueryArray.filter(query => query.id != null).length > 1 && (
                  <Button
                    variantType="secondary"
                    variant="ghost"
                    disabled={!saveStateAuthorization.database?.W}
                    rounded
                    size="3xs"
                    iconComponent="icon-[ic--baseline-close]"
                    onClick={e => {
                      e.stopPropagation();
                      if (query.id !== undefined) {
                        wsDeleteQuery({ saveStateID: activeSS.id, queryID: query.id });
                        dispatch(removeQueryByID(query.id));
                      }
                    }}
                  />
                )}
              </>
            </Tab>
          ))}
      </Tabs>
      <div className="sticky right-0 px-0.5 ml-auto items-center flex truncate">
        <ControlContainer>
          <TooltipProvider>
            <Tooltip>
              <TooltipTrigger>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--baseline-fullscreen]"
                  onClick={props.onFitView}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Fit to screen</p>
              </TooltipContent>
            </Tooltip>
            <Tooltip>
              <TooltipTrigger>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  disabled={!saveStateAuthorization.query.W}
                  iconComponent="icon-[ic--baseline-delete]"
                  onClick={() => {
                    if (saveStateAuthorization.query.W) clearAllNodes();
                  }}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Clear query panel</p>
              </TooltipContent>
            </Tooltip>
            <Tooltip>
              <TooltipTrigger>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--baseline-camera-alt]"
                  onClick={props.onScreenshot}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Capture screen</p>
              </TooltipContent>
            </Tooltip>
            <Popover>
              <PopoverTrigger>
                <Tooltip>
                  <TooltipTrigger>
                    <Button
                      variantType="secondary"
                      variant="ghost"
                      size="xs"
                      disabled={!saveStateAuthorization.query.W}
                      iconComponent="icon-[ic--baseline-settings]"
                      className="query-settings"
                    />
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Query builder settings</p>
                  </TooltipContent>
                </Tooltip>
              </PopoverTrigger>
              <PopoverContent>
                <QuerySettings />
              </PopoverContent>
            </Popover>
            <Tooltip>
              <TooltipTrigger>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--baseline-cached]"
                  onClick={() => props.onRunQuery(true)}
                  onDoubleClick={() => props.onRunQuery(false)}
                />
              </TooltipTrigger>
              <TooltipContent>
                <p>Rerun query</p>
              </TooltipContent>
            </Tooltip>
            <Tooltip>
              <TooltipTrigger asChild>
                <Button
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  disabled={!saveStateAuthorization.query.W}
                  iconComponent="icon-[ic--baseline-difference]"
                  onClick={props.onLogic}
                />
              </TooltipTrigger>
              <TooltipContent disabled={props.toggleSettings === 'logic'}>
                <p>Logic settings</p>
              </TooltipContent>
            </Tooltip>
            <Popover>
              <PopoverTrigger>
                <Tooltip>
                  <TooltipTrigger>
                    <Button
                      variantType={mlEnabled ? 'primary' : 'secondary'}
                      variant={mlEnabled ? 'outline' : 'ghost'}
                      size="xs"
                      disabled={!saveStateAuthorization.query.W}
                      iconComponent="icon-[ic--baseline-lightbulb]"
                    />
                  </TooltipTrigger>
                  <TooltipContent disabled={props.toggleSettings === 'ml'}>
                    {mlEnabled ? (
                      <>
                        <p className="font-bold text-base">Machine learning</p>
                        <p className="mb-2">Algorithms detected the following results:</p>

                        {ml.linkPrediction.enabled && ml.linkPrediction.result && (
                          <>
                            <p className="mt-2 font-semibold">Link prediction</p>
                            <p>{ml.linkPrediction.result.length} links</p>{' '}
                          </>
                        )}
                        {ml.centrality.enabled && Object.values(ml.centrality.result).length > 0 && (
                          <>
                            <p className="mt-2 font-semibold">Centrality</p>
                            <p>
                              {Object.values(ml.centrality.result)
                                .reduce((a, b) => b + a)
                                .toFixed(2)}{' '}
                              sum of centers
                            </p>
                          </>
                        )}
                        {ml.communityDetection.enabled && ml.communityDetection.result && (
                          <>
                            <p className="mt-2 font-semibold">Community detection</p>
                            <p>{ml.communityDetection.result.length} communities</p>
                          </>
                        )}
                        {ml.shortestPath.enabled && (
                          <>
                            <p className="mt-2 font-semibold">Shortest path</p>
                            {ml.shortestPath.result?.length > 0 && <p># of hops: {ml.shortestPath.result.length}</p>}
                            {!ml.shortestPath.srcNode ? (
                              <p>Please select source node</p>
                            ) : (
                              !ml.shortestPath.trtNode && <p>Please select target node</p>
                            )}
                          </>
                        )}
                      </>
                    ) : (
                      <p>Machine learning</p>
                    )}
                  </TooltipContent>
                </Tooltip>
              </PopoverTrigger>
              <PopoverContent>
                <QueryMLDialog />
              </PopoverContent>
            </Popover>
            <Popover placement="bottom">
              <PopoverTrigger>
                <Tooltip>
                  <TooltipTrigger>
                    <Button
                      variantType={activeQuery.settings.limit <= resultSize ? 'primary' : 'secondary'}
                      variant={activeQuery.settings.limit <= resultSize ? 'outline' : 'ghost'}
                      size="xs"
                      disabled={!saveStateAuthorization.query.W}
                      iconComponent="icon-[ic--baseline-filter-alt]"
                    />
                  </TooltipTrigger>
                  <TooltipContent disabled={props.toggleSettings === 'ml'}>
                    <p className="font-bold text-base">Limit</p>
                    <p>Limits the number of edges retrieved from the database.</p>
                    <p>Required to manage performance.</p>
                    <p className={`font-semibold${activeQuery.settings.limit <= resultSize ? ' text-primary-200' : ''}`}>
                      Fetched {resultSize} out of {totalSize} nodes
                    </p>
                  </TooltipContent>
                </Tooltip>
              </PopoverTrigger>
              <PopoverContent>
                <Input
                  type="number"
                  size="sm"
                  label="Limit"
                  value={activeQuery.settings.limit}
                  lazy
                  onChange={e => {
                    dispatch(setQuerybuilderSettings({ ...activeQuery.settings, limit: Number(e) }));
                  }}
                  className={`w-24${activeQuery.settings.limit <= resultSize ? ' border-danger-600' : ''}`}
                  containerClassName="p-2"
                />
              </PopoverContent>
            </Popover>
            <Popover>
              <PopoverTrigger disabled={!saveStateAuthorization.query.W}>
                <Tooltip>
                  <TooltipTrigger>
                    <Button variantType="secondary" variant="ghost" size="xs" iconComponent="icon-[ic--baseline-search]" />
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Manual Query</p>
                  </TooltipContent>
                </Tooltip>
              </PopoverTrigger>
              <PopoverContent>
                <ManualQueryDialog onSubmit={handleManualQuery} />
              </PopoverContent>
            </Popover>
          </TooltipProvider>
        </ControlContainer>
      </div>
    </div>
  );
};
