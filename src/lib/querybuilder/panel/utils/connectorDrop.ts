import { OnConnectStartParams, XYPosition } from 'reactflow';
import { NodeAttribute, QueryGraphNodes } from '../../model';

export type ConnectingNodeDataI = {
  params: OnConnectStartParams;
  position: XYPosition;
  node: QueryGraphNodes;
  attribute: NodeAttribute;
};
