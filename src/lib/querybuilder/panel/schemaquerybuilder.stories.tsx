import React from 'react';
import { Meta } from '@storybook/react';
import { Provider } from 'react-redux';
import { setQuerybuilderGraph, setSchema, store } from '@/lib/data-access/store';
import { SchemaUtils } from '@/lib/schema/schema-utils';
import { Schema } from '@/lib/schema/panel';
import { movieSchemaRaw } from '@/lib/mock-data';
import { QueryBuilder } from '@/lib/querybuilder';
import { QueryMultiGraphology } from '@/lib/querybuilder/model/graphology/utils';

const SchemaAndQueryBuilder = () => {
  return (
    <div
      style={{
        display: 'flex',
        height: '100%',
        flexDirection: 'row',
        width: '100%',
        columnGap: '20px',
      }}
    >
      <div style={{ width: '100%', height: '100%' }}>
        <Schema />
      </div>
      <div style={{ width: '100%', height: '100%' }}>
        <QueryBuilder />
      </div>
    </div>
  );
};

const Component: Meta = {
  component: SchemaAndQueryBuilder,
  title: 'Integration/Schema and QueryBuilder',
  decorators: [
    // using the real store here
    story => (
      <Provider store={store}>
        <div
          style={{
            width: '100%',
            height: '95vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const SchemaAndQueryBuilderInteractivity = {
  play: async () => {
    const dispatch = store.dispatch;
    const schema = SchemaUtils.schemaBackend2Graphology(movieSchemaRaw);

    const graph = new QueryMultiGraphology();
    dispatch(setQuerybuilderGraph(graph.export()));
    dispatch(setSchema(schema.export()));
  },
};

export default Component;
