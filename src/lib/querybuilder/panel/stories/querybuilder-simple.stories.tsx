import React from 'react';
import { setQuerybuilderGraph, setSchema, store } from '@/lib/data-access/store';
import { Meta } from '@storybook/react';
import { Provider } from 'react-redux';
import QueryBuilder from '../QueryBuilder';
import { QueryMultiGraphology } from '../../model';
import { QueryElementTypes } from 'ts-common';
import { SchemaUtils } from '../../../schema/schema-utils';

const Component: Meta<typeof QueryBuilder> = {
  component: QueryBuilder,
  title: 'QueryBuilder/Panel/Simple',
  decorators: [
    story => (
      <Provider store={store}>
        <div
          style={{
            width: '100%',
            height: '95vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const Simple = {
  args: {},
  play: async () => {
    const graph = new QueryMultiGraphology();
    const schema = SchemaUtils.schemaBackend2Graphology({
      nodes: [
        {
          name: 'entity',
          attributes: [
            { name: 'city', type: 'string' },
            { name: 'vip', type: 'bool' },
            { name: 'state', type: 'string' },
            { name: 'customers', type: 'float' },
          ],
        },
      ],
      edges: [
        {
          name: 'entity:entity',
          label: 'entity:entity',
          from: 'entity',
          to: 'entity',
          collection: 'entity2entity',
          attributes: [
            { name: 'arrivalTime', type: 'float' },
            { name: 'departureTime', type: 'float' },
          ],
        },
      ],
    });

    store.dispatch(setSchema(schema.export()));

    const entity1 = graph.addPill2Graphology(
      {
        id: '0',
        type: QueryElementTypes.Entity,
        x: 100,
        y: 100,
        name: 'Airport 1',
        attributes: [],
      },
      schema.getNodeAttribute('entity', 'attributes'),
    );
    const entity2 = graph.addPill2Graphology(
      {
        id: '10',
        type: QueryElementTypes.Entity,
        x: 200,
        y: 200,
        name: 'Airport 2',
        attributes: [],
      },
      schema.getNodeAttribute('entity', 'attributes'),
    );

    // graph.addNode('0', { type: QueryElementTypes.Entity, x: 100, y: 100, name: 'Entity Pill' });
    const relation1 = graph.addPill2Graphology(
      {
        id: '1',
        type: QueryElementTypes.Relation,
        x: 140,
        y: 140,
        name: 'Flight between airports',
        collection: 'Relation Pill',
        depth: { min: 0, max: 1 },
        attributes: [],
      },
      schema.getEdgeAttribute('entity:entity_entityentity', 'attributes'),
    );
    // addPill2Graphology(
    //   '2',
    //   {
    //     type: 'attribute',
    //     x: 170,
    //     y: 160,
    //     name: 'Attr string',
    //     dataType: 'string',
    //     matchType: 'EQ',
    //     value: 'mark',
    //     depth: { min: 0, max: 1 },
    //   },
    //   graph
    // );
    // addPill2Graphology(
    //   '3',
    //   {
    //     type: 'attribute',
    //     x: 170,
    //     y: 170,
    //     name: 'Attr number',
    //     dataType: 'float',
    //     matchType: 'EQ',
    //     depth: { min: 0, max: 1 },
    //   },
    //   graph
    // );
    // addPill2Graphology(
    //   '4',
    //   {
    //     type: 'attribute',
    //     x: 130,
    //     y: 120,
    //     name: 'Attr bool',
    //     dataType: 'bool',
    //     matchType: 'EQ',
    //     value: 'true',
    //     depth: { min: 0, max: 1 },
    //   },
    //   graph
    // );
    graph.addEdge2Graphology(entity1, relation1);
    graph.addEdge2Graphology(relation1, entity2);
    // graph.addEdge('2', '1', { type: 'attribute_connection' });
    // graph.addEdge('3', '1', { type: 'attribute_connection' });
    // graph.addEdge('4', '0', { type: 'attribute_connection' });
    // graph.addEdge('0', '1', {
    //   type: 'entity_relation',
    //   targetHandle: handles.relation.fromEntity,
    // });
    // graph.addEdge('1', '0', {
    //   type: 'entity_relation',
    //   sourceHandle: handles.relation.entity,
    // });
    store.dispatch(setQuerybuilderGraph(graph.export()));
  },
};

export default Component;
