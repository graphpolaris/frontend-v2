import React from 'react';
import { setQuerybuilderGraph, store } from '@/lib/data-access/store';
import { Meta } from '@storybook/react';
import { Provider } from 'react-redux';
import QueryBuilder from '../QueryBuilder';
import { QueryMultiGraphology } from '../../model';
import { QueryElementTypes } from 'ts-common';

const Component: Meta<typeof QueryBuilder> = {
  component: QueryBuilder,
  title: 'QueryBuilder/Panel/SingleRelationship',
  decorators: [
    story => (
      <Provider store={store}>
        <div
          style={{
            width: '100%',
            height: '95vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const SingleRelationship = {
  args: {},
  play: async () => {
    const graph = new QueryMultiGraphology();
    graph.addPill2Graphology({
      type: QueryElementTypes.Relation,
      x: 140,
      y: 140,
      name: 'Relation Pill',
      collection: 'Relation Pill',
      depth: { min: 0, max: 1 },
      attributes: [],
    });
    store.dispatch(setQuerybuilderGraph(graph.export()));
  },
};

export default Component;
