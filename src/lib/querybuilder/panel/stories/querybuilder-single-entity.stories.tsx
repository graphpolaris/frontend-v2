import React from 'react';
import { setQuerybuilderGraph, store } from '@/lib/data-access/store';
import { Meta } from '@storybook/react';
import { Provider } from 'react-redux';
import QueryBuilder from '../QueryBuilder';
import { QueryMultiGraphology } from '../../model';
import { QueryElementTypes } from 'ts-common';

const Component: Meta<typeof QueryBuilder> = {
  component: QueryBuilder,
  title: 'QueryBuilder/Panel/SingleEntity',
  decorators: [
    story => (
      <Provider store={store}>
        <div
          style={{
            width: '100%',
            height: '95vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const SingleEntity = {
  args: {},
  play: async () => {
    const graph = new QueryMultiGraphology();
    graph.addPill2Graphology({
      type: QueryElementTypes.Entity,
      x: 100,
      y: 100,
      name: 'Entity Pill',
      attributes: [],
    });
    store.dispatch(setQuerybuilderGraph(graph.export()));
  },
};

export default Component;
