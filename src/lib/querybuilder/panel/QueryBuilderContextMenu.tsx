import { useMemo, useState, useEffect } from 'react';
import { Popover, PopoverTrigger, DropdownItemContainer, DropdownItem, Input, Icon } from '@/lib/components';
import { ReactFlowInstance, Node } from 'reactflow';
import { useActiveQuery, useAppDispatch, useQuerybuilderHash } from '../..';
import { isEqual } from 'lodash-es';
import { getDataTypeIcon } from '../../components/DataTypeIcon';
import { QueryUnionType, NodeAttribute, defaultGraph, QueryRelationDirection } from 'ts-common';
import {
  attributeShownToggle,
  setQuerybuilderGraphology,
  setQueryUnionType,
  toQuerybuilderGraphology,
} from '@/lib/data-access/store/sessionSlice';

export const QueryBuilderContextMenu = (props: {
  open: boolean;
  position?: { x: number; y: number };
  node?: Node;
  reactFlowWrapper: React.RefObject<HTMLDivElement | null>;
  reactFlow: ReactFlowInstance;
  onClose: () => void;
}) => {
  const [filter, setFilter] = useState<string>('');
  const dispatch = useAppDispatch();
  const activeQuery = useActiveQuery();
  const qbHash = useQuerybuilderHash();
  const unionType =
    activeQuery?.settings.unionTypes == null || props.node == null ? QueryUnionType.AND : activeQuery?.settings.unionTypes[props.node.id];
  const relationDirection =
    props.node && props.node.type === 'relation' ? (props.node.data?.direction ?? QueryRelationDirection.BOTH) : undefined;

  const graphologyGraph = useMemo(() => toQuerybuilderGraphology(activeQuery?.graph || defaultGraph()), [activeQuery?.graph, qbHash]);

  const [divPosition, setDivPosition] = useState<DOMRect>(new DOMRect());
  const [initialPosition, setInitialPosition] = useState<DOMRect>(new DOMRect());

  const state = useMemo(() => {
    if (!divPosition || !initialPosition || !props.node) return;
    let position = { x: 0, y: 0 };
    if (props.position) {
      position = props.position;
    } else {
      position = props.reactFlow.flowToScreenPosition({ x: props.node.data.x, y: props.node.data.y });
    }

    return {
      open: props.open,
      x: position.x - divPosition.x - (initialPosition.x - divPosition.x),
      y: position.y - divPosition.y - (initialPosition.y - divPosition.y) + 10,
    };
  }, [props.open, props.node, divPosition]);

  useEffect(() => {
    if (props.reactFlowWrapper.current == null) return;
    setInitialPosition(props.reactFlowWrapper.current.getBoundingClientRect());

    const resizeObserver = new ResizeObserver(() => {
      if (props.reactFlowWrapper.current == null) return;
      setDivPosition(props.reactFlowWrapper.current.getBoundingClientRect());
    });
    resizeObserver.observe(props.reactFlowWrapper.current);
    return () => {
      if (props.reactFlowWrapper.current == null) return;
      resizeObserver.disconnect(); // clean up
    };
  }, [props.reactFlowWrapper]);

  const filteredAttributes = useMemo<NodeAttribute[]>(() => {
    if (props.node == null) return [];
    if (filter == null || filter.length == 0) return props.node.data.attributes;

    return (props.node.data.attributes as NodeAttribute[]).filter(attr => {
      return attr.handleData.attributeName?.toLocaleLowerCase().includes(filter.toLocaleLowerCase());
    });
  }, [filter, props.node]);

  function isAttributeAdded(attribute: NodeAttribute): boolean {
    return activeQuery?.attributesBeingShown.some(x => isEqual(x, attribute.handleData)) ?? false;
  }

  function addAttribute(attribute: NodeAttribute) {
    dispatch(attributeShownToggle(attribute.handleData));
  }

  function setUnionType(unionType: QueryUnionType) {
    if (!props.node) return;
    dispatch(setQueryUnionType({ nodeId: props.node.id, unionType: unionType }));
  }

  function setRelationDirection(relationDirection: QueryRelationDirection) {
    if (!props.node) return;
    // @ts-expect-error - graphology types are not up to date
    graphologyGraph.setNodeAttribute(props.node.id, 'direction', relationDirection);
    dispatch(setQuerybuilderGraphology(graphologyGraph));
    props.onClose();
  }

  function removeNode() {
    if (!props.node) return;
    const connectedLogicPills = graphologyGraph.neighbors(props.node.id);
    connectedLogicPills.forEach(pill => {
      const attributes = graphologyGraph.getNodeAttributes(pill);
      if (attributes.type === 'logic') {
        graphologyGraph.dropNode(pill);
      }
    });

    graphologyGraph.dropNode(props.node.id);
    dispatch(setQuerybuilderGraphology(graphologyGraph));
    props.onClose();
  }

  return (
    <Popover open={props.open && state !== undefined} interactive={true} showArrow={false} placement="bottom-start">
      <PopoverTrigger x={state ? state.x : 0} y={state ? state.y : 0} />
      <DropdownItemContainer root={props.reactFlowWrapper.current}>
        {props.node && props?.node.type !== 'logic' && (
          <>
            <DropdownItem
              value={'Add/remove attribute'}
              onClick={e => {}}
              submenu={[
                <Input
                  label=""
                  type={'text'}
                  placeholder="Filter"
                  size="xs"
                  className="mb-1 rounded-sm w-full"
                  value={filter}
                  onClick={e => e.stopPropagation()}
                  onChange={v => setFilter(v)}
                />,

                filteredAttributes.map(attr => (
                  <DropdownItem
                    key={attr.handleData.attributeName + attr.handleData.nodeId}
                    value={''}
                    selected={isAttributeAdded(attr)}
                    onClick={_ => addAttribute(attr)}
                    className="w-full"
                  >
                    <div className="flex items-center gap-1">
                      <Icon component={getDataTypeIcon(attr?.handleData?.attributeType)} className="" size={16} />
                      <span>{attr.handleData.attributeName ?? ''}</span>
                    </div>
                  </DropdownItem>
                )),
              ]}
            />
            <DropdownItem
              value="Union type"
              submenu={[
                <DropdownItem
                  value="AND"
                  onClick={_ => setUnionType(QueryUnionType.AND)}
                  selected={props.node ? unionType != QueryUnionType.OR : false} // Also selected when null
                />,
                <DropdownItem
                  value="OR"
                  onClick={_ => setUnionType(QueryUnionType.OR)}
                  selected={props.node ? unionType == QueryUnionType.OR : false}
                />,
              ]}
            />
            {relationDirection && (
              <DropdownItem
                value="Direction"
                submenu={[
                  <DropdownItem
                    value={QueryRelationDirection.BOTH}
                    onClick={_ => setRelationDirection(QueryRelationDirection.BOTH)}
                    selected={relationDirection === QueryRelationDirection.BOTH}
                  />,
                  <DropdownItem
                    value={QueryRelationDirection.RIGHT}
                    onClick={_ => setRelationDirection(QueryRelationDirection.RIGHT)}
                    selected={relationDirection === QueryRelationDirection.RIGHT}
                  />,
                  <DropdownItem
                    value={QueryRelationDirection.LEFT}
                    onClick={_ => setRelationDirection(QueryRelationDirection.LEFT)}
                    selected={relationDirection === QueryRelationDirection.LEFT}
                  />,
                ]}
              />
            )}
            <DropdownItem
              value="Entity statistics"
              submenu={[
                <DropdownItem value="" disabled className="text-muted pointer-events-none">
                  <div className="flex items-between text-neutral-400">
                    <div className="inline-block w-[70px]">Total:</div>
                    <span>{activeQuery?.graph?.nodeCounts ? activeQuery.graph.nodeCounts[props.node.id + '_count'] : 'unknown'}</span>
                  </div>
                </DropdownItem>,
              ]}
            />
          </>
        )}
        <DropdownItem value="Remove" className="text-danger" onClick={e => removeNode()} />
      </DropdownItemContainer>
    </Popover>
  );
};
