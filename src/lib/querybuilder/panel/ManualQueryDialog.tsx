import React, { useState } from 'react';
import { Button } from '../../components';

type ManualQueryDialogProps = {
  onSubmit: (query: string) => void;
};

export const ManualQueryDialog = ({ onSubmit }: ManualQueryDialogProps) => {
  const [query, setQuery] = useState('');

  const handleSubmit = () => {
    if (query.trim() !== '') {
      onSubmit(query);
      setQuery('');
    }
  };

  return (
    <div className="flex flex-col w-full gap-2 p-2">
      <label className="text-xs font-bold">Manual Query</label>
      <textarea
        value={query}
        onChange={e => setQuery(e.target.value)}
        placeholder="Enter your Cypher query"
        className="w-full h-32 border rounded p-2"
      />
      <Button variantType="primary" onClick={handleSubmit}>
        Run Query
      </Button>
    </div>
  );
};
