import React, { createContext } from 'react';
import { SchemaReactflowLogicNode } from '../model';
import { OnConnectStartParams } from 'reactflow';

export const QueryBuilderDispatcherContext = createContext<{
  openLogicPillUpdate: (node: SchemaReactflowLogicNode) => void;
  openLogicPillCreate: (params: OnConnectStartParams, position?: { x: number; y: number }) => void;
}>({
  openLogicPillUpdate: (node: SchemaReactflowLogicNode) => {
    throw new Error('openLogicPillUpdate not implemented');
  },
  openLogicPillCreate: (params: OnConnectStartParams, position?: { x: number; y: number }) => {
    throw new Error('openLogicPillCreate not implemented');
  },
});
