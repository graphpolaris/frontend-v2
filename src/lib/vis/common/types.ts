import { FC } from 'react';
import { GraphQueryResult } from '../../data-access/store/graphQueryResultSlice';
import { SchemaGraph } from '../../schema';
import type { AppDispatch } from '../../data-access';
import { ML, GraphStatistics, NodeQueryResult, EdgeQueryResult, XYPosition } from 'ts-common';

export type VisualizationSettingsType = {
  uuid?: string; // unique identifier for the visualization
  id: string;
  name: string;
  [id: string]: any;
};

export type VISComponentType<T = object> = {
  component: React.FC<VisualizationPropTypes<T>>;
  settingsComponent: FC<VisualizationSettingsPropTypes<T>>;
  settings: T;
  exportImage: () => void;
};

export type VisualizationPropTypes<T = object> = {
  data: GraphQueryResult;
  schema: SchemaGraph;
  ml: ML;
  settings: T & VisualizationSettingsType;
  dispatch: AppDispatch;
  graphMetadata: GraphStatistics;
  updateSettings: (newSettings: any) => void;
  handleHover: (val: any) => void;
  handleSelect: (selection?: { nodes?: NodeQueryResult[]; edges?: EdgeQueryResult[] }) => void;
};

export type VisualizationSettingsPropTypes<T = object> = {
  settings: T & VisualizationSettingsType;
  graphMetadata: GraphStatistics;
  updateSettings: (val: Partial<T>) => void;
};

export type SchemaElements = {
  nodes: NodeQueryResult[];
  edges: EdgeQueryResult[];
  selfEdges: EdgeQueryResult[];
};

export type BoundingBox = {
  topLeft: XYPosition;
  bottomRight: XYPosition;
};

export type NodeQualityDataForEntities = {
  nodeCount: number;
  attributeNullCount: number;
  notConnectedNodeCount: number;
  isAttributeDataIn: boolean; // is true when the data to display has arrived
  onClickCloseButton: () => void;
};

export type NodeQualityDataForRelations = {
  nodeCount: number;
  attributeNullCount: number;
  fromRatio: number; // the ratio of from-entity nodes to nodes that have this relation
  toRatio: number; // the ratio of to-entity nodes to nodes that have this relation
  isAttributeDataIn: boolean; // is true when the data to display has arrived
  onClickCloseButton: () => void;
};

export type NodeQualityPopupNode = NodeQueryResult & {
  data: NodeQualityDataForEntities | NodeQualityDataForRelations;
  nodeID: string; //ID of the node for which the popup is
};

export type AttributeAnalyticsData = {
  nodeType: NodeType;
  nodeID: string;
  attributes: AttributeWithData[];
  isAttributeDataIn: boolean; // is true when the data to display has arrived
  onClickCloseButton: () => void;
  onClickPlaceInQueryBuilderButton: (name: string, type: string) => void;
  searchForAttributes: (id: string, searchbarValue: string) => void;
  resetAttributeFilters: (id: string) => void;
  applyAttributeFilters: (id: string, category: AttributeCategory, predicate: string, percentage: number) => void;
};

export enum AttributeCategory {
  categorical = 'Categorical',
  numerical = 'Numerical',
  other = 'Other',
  undefined = 'undefined',
}

export enum NodeType {
  entity = 'entity',
  relation = 'relation',
}

export type AttributeAnalyticsPopupMenuNode = NodeQueryResult & {
  nodeID: string; //ID of the node for which the popup is
  data: AttributeAnalyticsData;
};

export type AttributeWithData = {
  attribute: any;
  category: AttributeCategory;
  nullAmount: number;
};
