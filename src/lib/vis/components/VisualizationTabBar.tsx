import { useState, useEffect, useRef } from 'react';
import { Button, DropdownContainer, DropdownItem, DropdownItemContainer, DropdownTrigger, Input } from '../../components';
import { Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from '../../components/tooltip';
import { ControlContainer } from '../../components/controls';
import { Tabs, Tab } from '@/lib/components';
import { useActiveSaveStateAuthorization, useAppDispatch, useVisualization } from '../../data-access';
import {
  addVisualization,
  removeVisualization,
  reorderVisState,
  setActiveVisualization,
  updateActiveVisualizationAttributes,
} from '../../data-access/store/visualizationSlice';
import { VisualizationsConfig } from './config';
import { Visualizations } from './VisualizationPanel';
import Sortable from 'sortablejs';
import { VisualizationSettingsType } from '../common';
import { resultSetFocus } from '@/lib/data-access/store/interactionSlice';

export default function VisualizationTabBar(props: { fullSize: () => void; exportImage: () => void; handleSelect: () => void }) {
  const { activeVisualizationIndex, openVisualizationArray } = useVisualization();
  const saveStateAuthorization = useActiveSaveStateAuthorization();
  const [open, setOpen] = useState(false);
  const dispatch = useAppDispatch();
  const [editingIdx, setEditingIdx] = useState<{ idx: number; text: string } | null>(null);

  const tabsRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (!tabsRef.current) return;

    const sortable = new Sortable(tabsRef.current, {
      animation: 150,
      draggable: '[data-type="tab"]',
      ghostClass: 'bg-secondary-300',
      dragClass: 'bg-secondary-100',
      onEnd: evt => {
        if (evt.oldIndex != null && evt.newIndex != null && evt.oldIndex !== evt.newIndex) {
          dispatch(
            reorderVisState({
              id: evt.oldIndex,
              newPosition: evt.newIndex,
            }),
          );
        }
      },
    });

    return () => {
      sortable.destroy();
    };
  }, [dispatch, openVisualizationArray]);

  /**
   * User can export image with Ctrl+S
   */
  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if ((e.ctrlKey || e.metaKey) && e.key === 's') {
        e.preventDefault();
        props.exportImage();
      }
    };

    window.addEventListener('keydown', handleKeyDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [props]);

  const onSelect = async (idx?: number) => {
    if (idx === undefined) return;
    dispatch(setActiveVisualization(idx));
  };

  const onDelete = (idx: number) => {
    dispatch(removeVisualization(idx));
    props.handleSelect();
  };

  function updateVisualizationName(vis: VisualizationSettingsType, text: string) {
    const newSettings = JSON.parse(JSON.stringify(vis));
    newSettings.name = text;
    dispatch(updateActiveVisualizationAttributes(newSettings));
    setEditingIdx(null);
  }

  return (
    <div className="absolute shrink-0 top-0 left-0 right-0 flex items-stretch justify-start h-7 bg-secondary-100 border-b border-secondary-200 max-w-full">
      <div className="flex items-center px-2">
        <h1 className="text-xs font-semibold text-secondary-600 truncate">Visualization</h1>
      </div>
      <div className="flex items-center px-0.5 gap-1 border-l border-secondary-200">
        <TooltipProvider>
          <Tooltip>
            <TooltipTrigger>
              <DropdownContainer open={open} onOpenChange={setOpen}>
                <DropdownTrigger disabled={!saveStateAuthorization.database?.W} onClick={() => setOpen(v => !v)}>
                  <Button as="a" variantType="secondary" variant="ghost" size="xs" iconComponent="icon-[ic--baseline-add]" />
                </DropdownTrigger>
                <DropdownItemContainer className="max-h-none">
                  {Object.values(VisualizationsConfig).map(({ id, displayName, icons }) => (
                    <DropdownItem
                      key={id}
                      value={id}
                      label={displayName}
                      className="flex items-center gap-2"
                      onClick={async () => {
                        dispatch(resultSetFocus({ focusType: 'visualization' }));
                        const component = await Visualizations[id]();
                        dispatch(addVisualization({ ...component.default.settings, name: displayName, id }));
                        setOpen(false);
                      }}
                    >
                      {icons.sm && <icons.sm className="h-4 w-4" />}
                    </DropdownItem>
                  ))}
                </DropdownItemContainer>
              </DropdownContainer>
            </TooltipTrigger>
            <TooltipContent>
              <p>Add visualization</p>
            </TooltipContent>
          </Tooltip>
        </TooltipProvider>
      </div>

      {openVisualizationArray.length > 0 && (
        <Tabs ref={tabsRef} className="-my-px overflow-x-auto overflow-y-hidden no-scrollbar divide-x divide-secondary-200 border-x">
          {openVisualizationArray.map((vis, i) => {
            const isActive = activeVisualizationIndex === i;
            const config = VisualizationsConfig[vis.id];
            const IconComponent = config?.icons.sm;

            return (
              <Tab key={vis.uuid} activeTab={isActive} IconComponent={IconComponent} className="group" onClick={() => onSelect(i)} text="">
                <>
                  {editingIdx?.idx === i ? (
                    <Input
                      type="text"
                      size="xs"
                      value={editingIdx.text}
                      label=""
                      onChange={e => {
                        setEditingIdx({ idx: i, text: e });
                      }}
                      onBlur={() => {
                        updateVisualizationName(vis, editingIdx.text);
                      }}
                      onKeyDown={e => {
                        if (e.key === 'Enter') {
                          updateVisualizationName(vis, editingIdx.text);
                        }
                      }}
                      className="w-20"
                      style={{
                        border: 'none',
                        boxShadow: 'none',
                        background: 'none',
                      }}
                      autoFocus
                    />
                  ) : (
                    <div
                      onDoubleClick={e => {
                        e.stopPropagation();
                        // FIXME: dispatch(setActiveQueryID(query.id || -1));
                        setEditingIdx({ idx: i, text: vis.name ?? '' });
                      }}
                    >
                      {vis.name}
                    </div>
                  )}
                  <Button
                    variantType="secondary"
                    variant="ghost"
                    disabled={!saveStateAuthorization.database?.W}
                    rounded
                    size="3xs"
                    iconComponent="icon-[ic--baseline-close]"
                    className={!isActive ? 'opacity-50 group-hover:opacity-100 group-focus-within:opacity-100' : ''}
                    onClick={e => {
                      e.stopPropagation();
                      onDelete(i);
                    }}
                  />
                </>
              </Tab>
            );
          })}
        </Tabs>
      )}

      {openVisualizationArray.length > 0 && (
        <div className="shrink-0 sticky right-0 px-0.5 ml-auto flex">
          <ControlContainer>
            <TooltipProvider>
              <Tooltip>
                <TooltipTrigger asChild>
                  <Button
                    variantType="secondary"
                    variant="ghost"
                    size="xs"
                    iconComponent="icon-[ic--baseline-fullscreen]"
                    onClick={props.fullSize}
                  />
                </TooltipTrigger>
                <TooltipContent>
                  <p>Full screen</p>
                </TooltipContent>
              </Tooltip>
              <Tooltip>
                <TooltipTrigger asChild>
                  <Button
                    variantType="secondary"
                    variant="ghost"
                    size="xs"
                    iconComponent="icon-[ic--baseline-camera-alt]"
                    onClick={props.exportImage}
                  />
                </TooltipTrigger>
                <TooltipContent>
                  <p>Export image [Ctrl+S]</p>
                </TooltipContent>
              </Tooltip>
            </TooltipProvider>
          </ControlContainer>
        </div>
      )}
    </div>
  );
}
