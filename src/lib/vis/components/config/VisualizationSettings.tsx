import React, { Suspense, useEffect, useMemo, useState } from 'react';
import { Input, Visualizations, useAppDispatch, useGraphQueryResultMeta, useVisualization } from '../../..';
import { SettingsHeader } from './components';
import { updateActiveVisualizationAttributes } from '@/lib/data-access/store/visualizationSlice';
import { VisualizationSettingsPropTypes, VisualizationSettingsType } from '../../common';

export function VisualizationSettings(props: { className?: string }) {
  // const manager = VisualizationManager();
  // const activeVisualization = useActiveVisualization();
  const { activeVisualizationIndex, openVisualizationArray } = useVisualization();
  const activeVisualization = useMemo(
    () => openVisualizationArray[activeVisualizationIndex],
    [activeVisualizationIndex, openVisualizationArray],
  );
  const graphMetadata = useGraphQueryResultMeta();

  const dispatch = useAppDispatch();

  const [component, setComponent] = useState<
    | undefined
    | {
        component: React.FC<VisualizationSettingsPropTypes>;
        id: string;
      }
  >(undefined);

  useEffect(() => {
    loadVisualization(activeVisualization);
  }, [activeVisualization]);

  const loadVisualization = async (vis?: VisualizationSettingsType) => {
    if (!vis) {
      setComponent(undefined);
      return;
    }

    const componentModule = await Visualizations[vis.id]();
    const component = componentModule.default;

    setComponent({
      component: component.settingsComponent,
      id: vis.id,
    });
  };

  const updateSettings = (newSettings: Record<string, any>) => {
    if (activeVisualization) {
      dispatch(updateActiveVisualizationAttributes(newSettings));
    }
  };

  return (
    <div className={`flex flex-col w-full overflow-auto${props.className ? ` ${props.className}` : ''}`}>
      <div className="text-sm p-2 flex flex-col gap-1">
        {activeVisualization && graphMetadata && (
          <>
            <SettingsHeader name="Visualization Settings" />
            <Suspense fallback={<div>Loading...</div>}>
              {component && component.component && activeVisualization && component.id === activeVisualization.id && (
                <component.component settings={activeVisualization} graphMetadata={graphMetadata} updateSettings={updateSettings} />
              )}
            </Suspense>
          </>
        )}
      </div>
    </div>
  );
}
