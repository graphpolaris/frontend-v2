import React, { ReactNode } from 'react';

type SettingsContainerProps = {
  children: ReactNode;
};

export function SettingsContainer({ children }: SettingsContainerProps) {
  return <div className="">{children}</div>;
}

type SettingsHeaderProps = {
  name: string;
  icon?: ReactNode;
  onClickIcon?: () => void;
};

export function SettingsHeader({ name, icon, onClickIcon }: SettingsHeaderProps) {
  return (
    <div className="flex justify-between items-center">
      <span className="text-sm font-bold">{name}</span>
      {icon && icon}
    </div>
  );
}
