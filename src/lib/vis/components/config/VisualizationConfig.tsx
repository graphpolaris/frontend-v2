import { JsonIconSm, JsonIconMd, JsonIconLg } from '../../icons/JsonIcon';
import { MatrixIconSm, MatrixIconMd, MatrixIconLg } from '../../icons/MatrixIcon';
import { MapIconSm, MapIconMd, MapIconLg } from '../../icons/MapIcon';
import { NodeLinkIconSm, NodeLinkIconMd, NodeLinkIconLg } from '../../icons/NodeLinkIcon';
import { PaohIconSm, PaohIconMd, PaohIconLg } from '../../icons/PaohIcon';
import { SemSubsIconSm, SemSubsIconMd, SemSubsIconLg } from '../../icons/SemSubsIcon';
import { TableIconSm, TableIconMd, TableIconLg } from '../../icons/TableIcon';
import { Vis0DIconSm, Vis0DIconMd, Vis0DIconLg } from '../../icons/Vis0DIcon';
import { Vis1DIconSm, Vis1DIconMd, Vis1DIconLg } from '../../icons/Vis1DIcon';

export type IconSize = 'sm' | 'md' | 'lg';

export type VisualizationMeta = {
  id: string;
  displayName: string;
  description: string;
  icons: Record<IconSize, React.FC<React.SVGProps<SVGSVGElement>>>;
};

export const VisualizationsConfig: Record<string, VisualizationMeta> = {
  RawJSONVis: {
    id: 'RawJSONVis',
    displayName: 'JSON',
    description: '(Raw) Data Export',
    icons: { sm: JsonIconSm, md: JsonIconMd, lg: JsonIconLg },
  },
  MatrixVis: {
    id: 'MatrixVis',
    displayName: 'Matrix',
    description: 'Overview & Details',
    icons: { sm: MatrixIconSm, md: MatrixIconMd, lg: MatrixIconLg },
  },
  MapVis: {
    id: 'MapVis',
    displayName: 'Map',
    description: 'Geographical Features',
    icons: { sm: MapIconSm, md: MapIconMd, lg: MapIconLg },
  },
  NodeLinkVis: {
    id: 'NodeLinkVis',
    displayName: 'Node-Link',
    description: 'General Patterns and Connections',
    icons: { sm: NodeLinkIconSm, md: NodeLinkIconMd, lg: NodeLinkIconLg },
  },
  PaohVis: {
    id: 'PaohVis',
    displayName: 'PaohVis',
    description: 'Paths and Connections',
    icons: { sm: PaohIconSm, md: PaohIconMd, lg: PaohIconLg },
  },
  SemanticSubstratesVis: {
    id: 'SemanticSubstratesVis',
    displayName: 'Semantic Substrates',
    description: 'Node/Edge Attribute Exploration',
    icons: { sm: SemSubsIconSm, md: SemSubsIconMd, lg: SemSubsIconLg },
  },
  TableVis: {
    id: 'TableVis',
    displayName: 'Table',
    description: 'Node Attribute Statistics and Details',
    icons: { sm: TableIconSm, md: TableIconMd, lg: TableIconLg },
  },
  Vis0D: {
    id: 'Vis0D',
    displayName: 'KPI',
    description: 'Display a single key performance indicator value',
    icons: { sm: Vis0DIconSm, md: Vis0DIconMd, lg: Vis0DIconLg },
  },
  Vis1D: {
    id: 'Vis1D',
    displayName: 'Basic 1D Chart',
    description: 'Visualize data distributions with simple 1D charts',
    icons: { sm: Vis1DIconSm, md: Vis1DIconMd, lg: Vis1DIconLg },
  },
};
