import React from 'react';
import { SelectionStateI, unSelect } from '@/lib/data-access/store/interactionSlice';
import { useDispatch } from 'react-redux';
import { Button, EntityPill, useSelection } from '../../..';
import { SettingsHeader } from './components';

export const SelectionConfig = () => {
  const selection = useSelection();
  const dispatch = useDispatch();

  if (!selection) return null;

  return (
    <div className="border-b py-2 overflow-auto">
      <div className="flex justify-between items-center px-4 py-2">
        <span className="text-xs font-bold">Selection</span>
        <Button
          variantType="secondary"
          variant="ghost"
          size="xs"
          iconComponent="icon-[ic--baseline-delete]"
          onClick={() => {
            dispatch(unSelect());
          }}
        />
      </div>
      {selection.content.map((item, index) => (
        <React.Fragment key={index + 'id'}>
          <div className="flex justify-between items-center px-4 py-1 gap-1">
            <span className="text-xs font-semibold pr-2">ID</span>
            <span className="text-xs">{item._id}</span>
          </div>
          <div key={index + 'label'} className="flex justify-between items-center px-4 py-1 gap-1">
            <span className="text-xs font-semibold pr-2">Label</span>
            <EntityPill title={item.attributes['labels'] as string}></EntityPill>
          </div>
          {Object.entries(item.attributes).map(([key, value]) => {
            if (key === 'labels' || key === '_id' || value instanceof Object) return null;
            return (
              <div key={index + key} className="flex justify-between items-center px-4 py-1 gap-1">
                <span className="text-xs font-semibold pr-2 whitespace-nowrap max-w-[6rem]">{String(key)}</span>
                <span className="text-xs break-all">{String(value)}</span>
              </div>
            );
          })}
        </React.Fragment>
      ))}
    </div>
  );
};
