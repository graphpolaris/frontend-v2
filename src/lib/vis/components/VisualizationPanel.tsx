import React, { Suspense, useEffect, useState } from 'react';
import {
  useActiveQuery,
  useActiveSaveState,
  useAppDispatch,
  useGraphQueryResult,
  useGraphQueryResultMeta,
  useML,
  useSchemaGraph,
  useVisualization,
} from '@/lib/data-access';
import VisualizationTabBar from './VisualizationTabBar';
import { Recommender, NoData, Querying } from '../views';
import { resultSetFocus, resultSetSelection, unSelect } from '../../data-access/store/interactionSlice';
import { updateVisualization, addVisualization } from '../../data-access/store/visualizationSlice';
import { VisualizationPropTypes, VISComponentType } from '../common';
import { ErrorBoundary } from '../../components/errorBoundary';
import { addError } from '../../data-access/store/configSlice';
import { canViewFeature } from '../../components/featureFlags';
import { NodeQueryResult, EdgeQueryResult } from 'ts-common';
import { PopoverProvider } from '@/lib';

type PromiseFunc = () => Promise<{ default: VISComponentType<any> }>;
export const Visualizations: Record<string, PromiseFunc> = {
  ...(canViewFeature('TABLEVIS') && { TableVis: () => import('../visualizations/tablevis/tablevis') }),
  ...(canViewFeature('PAOHVIS') && { PaohVis: () => import('../visualizations/paohvis/paohvis') }),
  ...(canViewFeature('RAWJSONVIS') && { RawJSONVis: () => import('../visualizations/rawjsonvis/rawjsonvis') }),
  ...(canViewFeature('NODELINKVIS') && { NodeLinkVis: () => import('../visualizations/nodelinkvis/nodelinkvis') }),
  ...(canViewFeature('MATRIXVIS') && { MatrixVis: () => import('../visualizations/matrixvis/matrixvis') }),
  ...(canViewFeature('SEMANTICSUBSTRATESVIS') && {
    SemanticSubstratesVis: () => import('../visualizations/semanticsubstratesvis/semanticsubstratesvis'),
  }),
  ...(canViewFeature('MAPVIS') && { MapVis: () => import('../visualizations/mapvis/mapvis') }),
  ...(canViewFeature('VIS0D') && { Vis0D: () => import('../visualizations/vis0D/Vis0D') }),
  ...(canViewFeature('VIS1D') && { Vis1D: () => import('../visualizations/vis1D/Vis1D') }),
};

export const VISUALIZATION_TYPES: string[] = Object.keys(Visualizations);

export const VisualizationPanel = ({ fullSize }: { fullSize: () => void }) => {
  const activeQuery = useActiveQuery();
  const graphQueryResult = useGraphQueryResult();
  const activeSaveState = useActiveSaveState();
  const dispatch = useAppDispatch();
  const { activeVisualizationIndex, openVisualizationArray } = useVisualization();
  const ml = useML();
  const schema = useSchemaGraph();
  const graphMetadata = useGraphQueryResultMeta();
  const [viz, setViz] = useState<{ component: React.FC<VisualizationPropTypes>; id: string; exportImage: () => void } | undefined>(
    undefined,
  );

  useEffect(() => {
    if (openVisualizationArray.length > 0) {
      loadVisualization();
    }
  }, [activeVisualizationIndex, openVisualizationArray.length, activeSaveState]);

  const loadVisualization = async (add = false) => {
    if (activeVisualizationIndex === -1) {
      setViz(undefined);
      return;
    }

    const activeVisualization = openVisualizationArray[activeVisualizationIndex];

    if (!activeVisualization) {
      setViz(undefined);
      return;
    }

    const componentModule = await Visualizations[activeVisualization.id]();
    const component = componentModule.default;

    if (add) {
      dispatch(addVisualization(component.settings));
    }
    // Extract exportImage function or default to no-op
    const exportImage = component.exportImage || (() => {});

    setViz({ component: component.component, id: activeVisualization.id, exportImage });
  };

  const handleSelect = (selection?: { nodes?: NodeQueryResult[]; edges?: EdgeQueryResult[] }) => {
    if (selection?.nodes && selection.nodes.length > 0) {
      dispatch(resultSetSelection({ selectionType: 'node', content: selection.nodes }));
      dispatch(resultSetFocus({ focusType: 'query' }));
    } else dispatch(unSelect());
  };

  const updateSettings = (newSettings: Record<string, any>) => {
    if (activeVisualizationIndex) {
      const updatedSettings = { ...openVisualizationArray[activeVisualizationIndex], ...newSettings };
      dispatch(updateVisualization({ id: activeVisualizationIndex, settings: updatedSettings }));
    }
  };
  const exportImage = viz?.exportImage || (() => {});

  return (
    <div className="relative pt-7 vis-panel h-full w-full flex flex-col border bg-light">
      <div className="grow overflow-y-auto" style={graphQueryResult.nodes.length === 0 ? { overflow: 'hidden' } : {}}>
        {graphQueryResult.queryingBackend ? (
          <Querying />
        ) : graphQueryResult.nodes.length === 0 ? (
          <NoData dataAvailable={!activeQuery || activeQuery.graph.nodes.length > 0} />
        ) : openVisualizationArray.length === 0 ? (
          <Recommender />
        ) : (
          <div className="w-full h-full flex">
            <ErrorBoundary
              fallback={<div>Something went wrong</div>}
              onError={() => {
                dispatch(addError('Something went wrong while trying to load the visualization'));
                setViz(undefined);
              }}
            >
              {!!viz &&
                activeVisualizationIndex !== -1 &&
                openVisualizationArray?.[activeVisualizationIndex] &&
                viz.id === openVisualizationArray[activeVisualizationIndex].id &&
                graphMetadata && (
                  <viz.component
                    data={graphQueryResult}
                    schema={schema}
                    ml={ml}
                    settings={openVisualizationArray[activeVisualizationIndex]}
                    dispatch={dispatch}
                    handleSelect={handleSelect}
                    graphMetadata={graphMetadata}
                    updateSettings={updateSettings}
                    handleHover={() => {}}
                  />
                )}
            </ErrorBoundary>
          </div>
        )}
      </div>
      <PopoverProvider>
        <VisualizationTabBar fullSize={fullSize} exportImage={exportImage} handleSelect={handleSelect} />
      </PopoverProvider>
    </div>
  );
};
