import { Edge, Node } from '@/lib/data-access';

export type ArcEdge = Edge & {
  x1: number;
  x2: number;
  thickness: number;
  fill: string;
};

export type ArcNode = Node & {
  x: number;
  y: number;
  size: number;
  fill: string;
  label: string;
};
