import { ArcEdge, ArcNode } from '../arcplotvis.types';

import { arc, select, selectAll } from 'd3';
import { config } from 'process';
import React from 'react';
import { configStyle } from '../arcplotvis';

interface ArcPlotD3LinkProps {
  nodes: ArcNode[];
  edges: ArcEdge[];
  height: number;
}

const EDGE_THICKNESS_MIN = 1;
const EDGE_THICKNESS_MAX = 10;

const ArcVisLinks: React.FC<ArcPlotD3LinkProps> = ({ nodes, edges, height }) => {
  const τ = 2 * Math.PI; // http://tauday.com/tau-manifesto

  const [thicknessScale, setThicknessScale] = React.useState<any>({});
  const [opacityScale, setOpacityScale] = React.useState<any>({});

  /**
   * I made two versions here, one using the arcPathBuilder and the other using the arcConstructor.
   * The arcPathBuilder is a d3 function that creates an arc path based on the input data.
   * The arcConstructor is a custom function that creates an arc path based on the input data.
   *
   * The idea was that they should obey the window/canvas height and not crop of the arcs. But I did not get there.
   * TODO: Fix the arcConstructor to obey the window/canvas height and not crop of the arcs.
   */
  const arcPathBuilder = arc()
    .startAngle(-τ / 4)
    .endAngle(τ / 4)
    .innerRadius((d: any) => {
      const arcHeight = 0.5 * Math.abs(d.x2 - d.x1);
      return arcHeight - d.thickness / 2;
    })
    .outerRadius((d: any) => {
      const arcHeight = 0.5 * Math.abs(d.x2 - d.x1);
      return arcHeight + d.thickness / 2;
    });

  const arcConstructor = (d: ArcEdge) => {
    const start = d.x1;
    const end = d.x2;
    const inflexion = 1;
    const inverse = start < end;
    return {
      ...d,
      inverse,
      d: [
        'M',
        start,
        height / 2,
        'A', // This means we're going to build an elliptical arc
        inflexion,
        ',', // Next 2 lines are the coordinates of the inflexion point. Height of this point is proportional with start - end distance
        inflexion,
        0,
        0,
        ',',
        1,
        end,
        ',',
        height / 2,
      ] // We always want the arc on top. So if end is before start, putting 0 here turn the arc upside down.
        .join(' '),
    };
  };

  const selectionLabels = selectAll('.arcLinks');
  selectionLabels.each((d, i, nodes) => {
    select(nodes[i])
      .transition()
      .delay(Math.random() * configStyle.HOVER_TRANSITION_DURATION)
      .duration(configStyle.HOVER_TRANSITION_DURATION * 0.25)
      .style('opacity', 1);
  });

  return (
    <>
      {edges.map((edge: ArcEdge) => {
        if (!edge) return null;
        // const arcPath = arcPathBuilder(edge) || '';
        const arcPath2 = arcConstructor(edge) || '';
        // if (!thicknessScale) return null;
        return (
          <g className={`arcLinks from-${edge.from} to-${edge.to}`} key={edge._id} opacity={0}>
            {/* <path key={edge._id} transform={`translate(${(edge.x1 + edge.x2) / 2}, 0)`} d={arcPath} fill={edge.fill} stroke="none" /> */}
            <path
              className="link"
              transform={`translate(0, -${height / 2})`}
              d={arcPath2.d}
              stroke={edge.fill}
              strokeOpacity={0.2}
              strokeWidth={edge.thickness}
              fill="none"
              // strokeOpacity={opacityScale(Math.abs(edge.x1 - edge.x2))}
              // strokeWidth={thicknessScale(edge.thickness) || 1}
            />
          </g>
        );
      })}
    </>
  );
};

export default ArcVisLinks;
