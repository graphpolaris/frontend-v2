import React from 'react';

interface ArcPlotD3LineProps {
  height: number;
  start: number;
  end: number;
}

const ArcVisLine: React.FC<ArcPlotD3LineProps> = ({ start, end, height = 100 }) => {
  return (
    <>
      <line x1={start} y1={height} x2={end} y2={height} stroke="black" />
    </>
  );
};

export default ArcVisLine;
