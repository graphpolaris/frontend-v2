import { select, selectAll } from 'd3';
import React from 'react';
import { configStyle } from '../arcplotvis';
import styles from '../arcplotvis.module.scss';
import { ArcNode } from '../arcplotvis.types';

interface ArcPlotD3NodeProps {
  nodes: ArcNode[];
  drawLabels: boolean | undefined;
  iconStyle: 'circle' | 'square' | 'triangle' | undefined;

  handleMouseEnter: (event: React.MouseEvent<SVGGElement, MouseEvent>) => void | undefined;
  handleMouseOut: (event: React.MouseEvent<SVGGElement, MouseEvent>) => void | undefined;
}

const ArcVisNodes: React.FC<ArcPlotD3NodeProps> = ({
  nodes,
  handleMouseEnter = () => {},
  handleMouseOut = () => {},
  drawLabels = true,
  iconStyle = 'circle',
}) => {
  const selectionLabels = selectAll('.arcLabel');
  selectionLabels.each((d, i, nodes) => {
    const node = select(nodes[i]);

    node
      .transition()
      .delay(Math.random() * configStyle.HOVER_TRANSITION_DURATION)
      .duration(configStyle.HOVER_TRANSITION_DURATION * 0.25)
      .style('opacity', 1);
  });

  return (
    <>
      {nodes.map((node: ArcNode, indexItem: number) => {
        return (
          // <g class className="arcLabel" key={node._id} opacity={0}>
          <g
            className={'arcLabel node-' + node._id + ' cursor-pointer'} //this order needs to stay like this or it will break the handleMouseEnter code
            onMouseEnter={handleMouseEnter}
            onMouseOut={handleMouseOut}
            key={node._id}
            opacity={0}
          >
            {iconStyle === 'circle' && <circle cx={node.x} cy={node.y} r={node.size} fill={node.fill} />}
            {iconStyle === 'square' && (
              <rect x={node.x - node.size} y={node.y - node.size} width={node.size * 2} height={node.size * 2} fill={node.fill} />

              /**
               * very interesting concept below for playing around:
               * can turn into a barchart version of the arcnodes
               * The node.size * X defines the height of the bar
               *
               * TODO: Investigate barchart version of arcnodes
               */
              // <rect x={node.x - node.size} y={node.y - node.size * 2} width={node.size} height={node.size * 2} fill={node.fill} />
            )}
            {iconStyle === 'triangle' && (
              <polygon
                points={`${node.x},${node.y - node.size} ${node.x - node.size},${node.y + node.size} ${node.x + node.size},${node.y + node.size}`}
                fill={node.fill}
              />
            )}
            {drawLabels && (
              <text
                x={node.x}
                y={node.y + 20}
                transform={`rotate(45 ${node.x + 5} ${node.y + 15})`}
                fontSize={node.size}
                style={{ fontFamily: 'monospace' }}
                className={`${styles.label}`}
              >
                {node.label}
              </text>
            )}
          </g>
        );
      })}
    </>
  );
};

export default ArcVisNodes;
