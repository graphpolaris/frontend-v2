import { configureStore } from '@reduxjs/toolkit';
import { Meta } from '@storybook/react';
import { Provider } from 'react-redux';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import { mockData } from '../../../mock-data';
import ArcPlotComponent from './arcplotvis';

const Mockstore = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
  },
});

const Component: Meta<typeof ArcPlotComponent.component> = {
  title: 'Visualizations/ArcPlotVis',
  component: ArcPlotComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const SmallVillainDoubleArchData = {
  args: {
    ...(await mockData.smallVillainDoubleArchQueryResults()),
    updateSettings: () => {},
    settings: {
      ...ArcPlotComponent.settings,
    },
  },
};

export const SmallVillainData = {
  args: {
    ...(await mockData.smallVillainQueryResults()),
    updateSettings: () => {},
    settings: {
      ...ArcPlotComponent.settings,
    },
  },
};

export const GotCharacter2CharacterData = {
  args: {
    ...(await mockData.gotCharacter2Character()),
    updateSettings: () => {},
    settings: {
      ...ArcPlotComponent.settings,
    },
  },
};

export const movie_recommendationPersonActedInMovieQueryResultData = {
  args: {
    ...(await mockData.movie_recommendationPersonActedInMovieQueryResult()),
    updateSettings: () => {},
    settings: {
      ...ArcPlotComponent.settings,
    },
  },
};

// TODO!: Data no correct shape
/*
export const LargeData = {
  args: {
    ...(await mockData.mockLargeQueryResults()),
    updateSettings: () => {},
    settings: {
      ...ArcPlotComponent.settings,
    },
  },

  // args: {
  //   ...(await mockData.marieBoucherSample()),
  //   updateSettings: () => {},
  //   schema: marieBoucherSampleSchemaRaw,
  //   settings: {
  //     ...PaohVisComponent.settings,
  //     rowNode: 'merchant',
  //     columnNode: 'merchant',
  //     attributeRowShow: ['_id', '# Connections', 'name2', 'birth'],
  //   },
  // },
};
  */
export const Empty = {
  args: {
    data: {
      nodes: [],
      edges: [],
    },
    settings: {
      ...ArcPlotComponent.settings,
    },
  },
};

export default Component;
