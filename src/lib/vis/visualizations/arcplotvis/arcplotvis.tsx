import { Input } from '@/lib/components/inputs';
import { Edge, Node } from '@/lib/data-access';
import { GraphStatistics } from '@/lib/statistics';
import { SettingsContainer } from '@/lib/vis/components/config';
import { scaleOrdinal, schemeCategory10, select, selectAll } from 'd3';
import React, { useEffect, useMemo, useRef } from 'react';
import { VISComponentType, VisualizationPropTypes, VisualizationSettingsPropTypes } from '../../common';
import { ArcEdge, ArcNode } from './arcplotvis.types';
import ArcVisLine from './components/arcplotd3line';
import ArcVisLinks from './components/arcplotd3links';
import ArcVisNodes from './components/arcplotd3nodes';

export type ArcVisProps = {
  iconStyle?: 'circle' | 'square' | 'triangle' | undefined;
  ordering?: string;
  showLabels?: boolean;
  labelattribute?: string;
  sizeAttributeSelector?: string;
};

const settings: ArcVisProps = {
  iconStyle: 'circle',
  ordering: 'Label',
  showLabels: true,
  labelattribute: 'test',
  sizeAttributeSelector: 'test',
};

export const configStyle = {
  colorText: 'hsl(var(--clr-sec--800))',
  colorTextUnselect: 'hsl(var(--clr-sec--400))',
  colorLinesHyperEdge: 'hsl(var(--clr-black))',
  HOVER_TRANSITION_DURATION: 350,
};

const margin = { top: 20, right: 20, bottom: 20, left: 20 };

export const ArcPlotVis = React.memo(({ data, settings, graphMetadata }: VisualizationPropTypes<ArcVisProps>) => {
  console.log(data, settings, graphMetadata);
  const svgRef = useRef<SVGSVGElement>(null);
  const [dimensions, setDimensions] = React.useState({ width: 0, height: 0 });

  useEffect(() => {
    const handleResize = () => {
      const width = svgRef.current?.clientWidth;
      const height = svgRef.current?.clientHeight;

      const dim = { width: width || 0, height: height || 0 };
      setDimensions(dim);
    };

    window.addEventListener('resize', handleResize);
    handleResize(); // Call the handleResize function initially

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const getNodeOrdering = (node: Node, index: number) => {
    if (!settings.ordering) throw new Error('No ordering selected in settings: ' + settings.toString());

    const OFFSET = 15;

    switch (settings.ordering.toLowerCase()) {
      case 'label':
        if (!node.label || node.label.length === 0) throw new Error('Node has no label or label is empty');
        return node.label.charCodeAt(0) * 10 + index * OFFSET;
      case 'group':
        return margin.left + Math.random() * (dimensions.width - margin.right);
      case 'frequency':
        return margin.left + Math.random() * (dimensions.width - margin.right);
      default:
        return margin.left + Math.random() * (dimensions.width - margin.right);
    }
  };

  function getNodeLabel(node: Node): string {
    if (!settings.labelattribute) throw new Error('No label attribute selected in settings: ' + settings.toString());

    switch (settings.labelattribute.toLowerCase()) {
      case 'label':
        return node.label;
      case 'type':
        return node.attributes['type'] as string;
      default: {
        if (!node.label || node.label.length === 0) throw new Error('Node has no label or label is empty');
        // console.log('node get label', node);
        return node.label;
      }
    }
  }

  function getNodeSize(node: Node): number {
    if (!settings.sizeAttributeSelector) throw new Error('No label attribute selected in settings: ' + settings.toString());

    return Math.random() * 10;

    // switch (settings.labelattribute) {
    //   case 'name':
    //     return node.label;
    //   case 'type':
    //     return node.attributes['Type'] as string;
    //   default: {
    //     if (!node.label || node.label.length === 0) throw new Error('Node has no label or label is empty');
    //     // console.log('node get label', node);
    //     return node.label;
    //   }
    // }
  }

  const [dataNodes, dataLinks] = useMemo(() => {
    const color = scaleOrdinal(schemeCategory10).domain(data.nodes.map(node => node.label));
    const colorScale = scaleOrdinal(schemeCategory10).domain(data.edges.map(edge => edge.attributes.Type as string));

    const dataNodes = data.nodes.map((node, index) => {
      const x = getNodeOrdering(node, index);
      return {
        ...node,
        x: x,
        y: dimensions.height / 2,
        size: getNodeSize(node),
        fill: color(node.label),
        label: getNodeLabel(node),
      } as ArcNode;
    }) as ArcNode[];

    const dataLinks = data.edges.map((edge: Edge) => {
      const fromNode = dataNodes.find((node: ArcNode) => node._id === edge.from);
      const toNode = dataNodes.find((node: ArcNode) => node._id === edge.to);
      // const fromNode = dataNodes.find((node: ArcNode) => node._id === edge.from);
      // const toNode = dataNodes.find((node: ArcNode) => node._id === edge.to);

      if (!fromNode || !toNode) {
        // throw new Error('Node not found; ArcEdge cannot be constructred');

        // alternatively, we could just (silently or not) skip the edge
        console.error('Node not found; ArcEdge cannot be constructred; Skipping this edge.');
        return null;
      }
      return {
        ...edge,
        x1: fromNode.x,
        x2: toNode.x,
        thickness: fromNode.size,
        fill: colorScale(edge.attributes['Type'] as string),
      } as ArcEdge;
    });

    return [dataNodes as ArcNode[], dataLinks as ArcEdge[]]; // MIB: no idea why typescript needs this cast so explicitly
  }, [data, settings, dimensions]);

  const handleMouseEnterNodes = (event: React.MouseEvent<SVGGElement, MouseEvent>) => {
    const targetClassList = (event.currentTarget as SVGGElement).classList;
    const hoveredID = targetClassList[1].replace('node-', '');
    // all elements - unselect
    selectAll('.arcLabel').selectAll('text').attr('fill', configStyle.colorTextUnselect);
    selectAll('.arcLabel').selectAll('circle').attr('fill', configStyle.colorTextUnselect);
    selectAll('.arcLabel').selectAll('rect').attr('fill', configStyle.colorTextUnselect);
    selectAll('.arcLabel').selectAll('polygon').attr('fill', configStyle.colorTextUnselect);

    // select element - select hovered
    const targetElement = selectAll('.' + targetClassList[1]);
    targetElement.selectAll('text').attr('fill', configStyle.colorText).style('opacity', 1);
    targetElement.selectAll('circle').attr('fill', configStyle.colorText);
    targetElement.selectAll('rect').attr('fill', configStyle.colorText);
    targetElement.selectAll('polygon').attr('fill', configStyle.colorText);

    selectAll('.arcLinks').selectAll('path').attr('stroke', configStyle.colorTextUnselect).attr('stroke-opacity', 0.1);
    const selectionEdges = selectAll('.arcLinks');
    selectionEdges.each((d, i, nodes) => {
      const gElement = select(nodes[i]);
      const targetClassList = gElement.attr('class').split(' ');

      const from = targetClassList[1].replace('from-', '');
      const to = targetClassList[2].replace('to-', '');

      if (from === hoveredID || to === hoveredID) {
        gElement
          .selectAll('path')
          .transition()
          .duration(configStyle.HOVER_TRANSITION_DURATION)
          .attr('stroke', configStyle.colorLinesHyperEdge)
          .attr('stroke-opacity', 1);
      }
    });
  };

  const handleMouseOutNodes = (event: React.MouseEvent<SVGGElement, MouseEvent>) => {
    // all elements - unselect
    // selectAll('.arcLabel').selectAll('text').attr('fill', configStyle.colorText);

    const selectionLabels = selectAll('.arcLabel');
    selectionLabels.each((d, i, nodes) => {
      const gElement = select(nodes[i]);
      const targetClassList = gElement.attr('class').split(' ');

      // console.log('targetClassList', targetClassList);

      const node = dataNodes.find((node: ArcNode) => node._id === targetClassList[1].replace('node-', ''));
      if (!node) throw new Error('Node not found; ArcNode cannot be color-reset after hover');

      gElement.selectAll('circle').attr('fill', node.fill);
      gElement.selectAll('rect').attr('fill', node.fill);
      gElement.selectAll('polygon').attr('fill', node.fill);
    });

    const selectionEdges = selectAll('.arcLinks');
    selectionEdges.each((d, i, nodes) => {
      const gElement = select(nodes[i]);
      const targetClassList = gElement.attr('class').split(' ');

      const edge = findEdgeByIDs(targetClassList[1], targetClassList[2]);
      if (!edge) throw new Error('Edge not found; ArcEdge cannot be color-reset after hover ');
      gElement
        .selectAll('path')
        .transition()
        .duration(configStyle.HOVER_TRANSITION_DURATION)
        .attr('stroke', edge.fill)
        .attr('stroke-opacity', 0.2);
    });
  };

  const findEdgeByIDs = (from: string, to: string) => {
    return dataLinks.find(edge => edge.from === from.replace('from-', '') && edge.to === to.replace('to-', ''));
  };

  return (
    <svg ref={svgRef} className="h-full w-full">
      {<ArcVisLine height={dimensions.height / 2} start={margin.left} end={dimensions.width - margin.right} />}
      {
        <g transform={`translate(0, ${dimensions.height / 2})`}>
          <ArcVisLinks nodes={dataNodes} edges={dataLinks} height={dimensions.height} />
        </g>
      }
      {
        <ArcVisNodes
          nodes={dataNodes}
          handleMouseEnter={handleMouseEnterNodes}
          handleMouseOut={handleMouseOutNodes}
          iconStyle={settings.iconStyle}
          drawLabels={settings.labels}
        />
      }
    </svg>
  );
});

const ArcPlotVisSettings = ({ settings, graphMetadata, updateSettings }: VisualizationSettingsPropTypes<ArcVisProps>) => {
  return (
    <SettingsContainer>
      <Input
        type="dropdown"
        label="Select an Ordering"
        value={settings.ordering}
        options={['Group', 'Frequency', 'Name']}
        onChange={val => updateSettings({ ordering: val.toString() })}
      />
      <Input
        type="dropdown"
        label="Icon style"
        value={settings.iconStyle}
        options={['circle', 'square', 'triangle']}
        onChange={val => updateSettings({ iconStyle: val as any })}
      />

      <Input
        type="boolean"
        label="Show Label"
        value={settings.showLabels !== undefined ? settings.showLabels : true}
        onChange={val => updateSettings({ showLabels: val as boolean })}
      />
      <Input
        type="dropdown"
        label="Label"
        value={settings.labelattribute}
        options={['circle', 'square', 'triangle']}
        onChange={val => updateSettings({ iconStyle: val as any })}
      />
      {/* <Input type="checkbox" label="Draw Labels" value={configuration.labels} onChange={(val) => updateSettings({ labels: val })} /> */}
    </SettingsContainer>
  );
};

// remove ts ignore when added to Visualization Dict, because displayname needs to be key of it
// eslint-disable-next-line react-refresh/only-export-components
export const ArcVisComponent: VISComponentType<ArcVisProps> = {
  component: ArcPlotVis,
  settingsComponent: ArcPlotVisSettings,
  settings: settings,
  exportImage: () => {},
};
export default ArcVisComponent;
