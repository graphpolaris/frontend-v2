import { ChoroplethLayer } from './choropleth-layer/ChoroplethLayer';
import { HeatLayer } from './heatmap-layer/HeatLayer';
import { NodeLinkLayer } from './nodelink-layer/NodeLinkLayer';
import { NodeLinkOptions } from './nodelink-layer/NodeLinkOptions';
import { HeatLayerOptions } from './heatmap-layer/HeatLayerOptions';
import { ChoroplethOptions } from './choropleth-layer/ChoroplethOptions';
import { TileLayer, BitmapLayer } from 'deck.gl';
import { MapProps } from '../mapvis';
import { LayerSettingsComponentType } from '../mapvis.types';
import { MAP_PROVIDER } from '../config';

export type LayerTypes = 'nodelink' | 'heatmap' | 'choropleth';

export const layerTypes: Record<LayerTypes, any> = {
  nodelink: NodeLinkLayer,
  heatmap: HeatLayer,
  choropleth: ChoroplethLayer,
};

export type MapLayerSettingsPropTypes = LayerSettingsComponentType<MapProps> & {
  spatialAttributes: { [id: string]: string[] };
};

export const layerSettings: Record<string, React.FC<MapLayerSettingsPropTypes>> = {
  nodelink: NodeLinkOptions,
  heatmap: HeatLayerOptions,
  choropleth: ChoroplethOptions,
};

export const createBaseMap = () => {
  return new TileLayer({
    data: MAP_PROVIDER,
    minZoom: 0,
    maxZoom: 19,
    tileSize: 256,

    renderSubLayers: (props: any) => {
      const {
        bbox: { west, south, east, north },
      } = props.tile;

      return new BitmapLayer(props, {
        data: undefined,
        image: props.data,
        bounds: [west, south, east, north],
      });
    },
  });
};
