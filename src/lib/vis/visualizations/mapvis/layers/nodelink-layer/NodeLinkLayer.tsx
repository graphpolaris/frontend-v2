import { CompositeLayer, Layer, Viewport } from 'deck.gl';
import { LineLayer, ScatterplotLayer, TextLayer, SolidPolygonLayer } from '@deck.gl/layers';
import { CompositeLayerType, Coordinate, LayerProps, EDGE_COLOR_DEFAULT } from '../../mapvis.types';
import { BrushingExtension, CollisionFilterExtension } from '@deck.gl/extensions';
import { scaleLinear, ScaleLinear, color, interpolateRgb } from 'd3';
import { nodeColorRGB } from '../../utils';
import { CategoricalStats, EdgeQueryResult, NodeQueryResult } from 'ts-common';
import { MapEdgeData } from '../../mapvis.types';
import { getCategories } from './NodeLinkOptions';

interface ColorScales {
  [label: string]: ScaleLinear<string, string>;
}

interface NodeLinkLayerState {
  colorScales: ColorScales;
  [key: string]: any;
}

export class NodeLinkLayer extends CompositeLayer<CompositeLayerType> {
  static type = 'nodelink';
  private _layers: Record<string, Layer> = {};

  state: NodeLinkLayerState = {
    colorScales: {},
  };

  constructor(props: LayerProps) {
    super(props);
  }

  updateState({ props, changeFlags }: { props: any; changeFlags: any }) {
    // TODO: Remove any here
    if (changeFlags.propsOrDataChanged) {
      const colorScales: ColorScales = {};
      const nodesSettings = props.settings?.[NodeLinkLayer.type]?.nodes;
      const nodesMetadata = props.graphMetadata?.nodes?.types;

      if (nodesSettings && nodesMetadata) {
        Object.keys(nodesSettings).forEach(label => {
          const nodeSettings = nodesSettings[label];
          const nodeDistribution = nodesMetadata[label]?.attributes;

          if (nodeSettings?.colorByAttribute && nodeDistribution) {
            const colorAttribute = nodeSettings.colorAttribute;
            const attributeData = nodeDistribution[colorAttribute];

            if (nodeSettings.colorAttributeType === 'number' && attributeData) {
              colorScales[label] = this.setNumericalColor(
                attributeData.statistics.min,
                attributeData.statistics.max,
                nodeSettings.colorScale,
              );
            }
          }
        });
      }

      const edgesSettings = props.settings?.[NodeLinkLayer.type]?.edges;
      const edgesMetadata = props.graphMetadata?.edges?.types;
      if (edgesSettings && edgesMetadata) {
        Object.keys(edgesSettings).forEach(label => {
          const edgeSettings = edgesSettings[label];
          const edgeDistribution = edgesMetadata[label]?.attributes;

          if (edgeSettings?.colorByAttribute && edgeDistribution) {
            const colorAttribute = edgeSettings.colorAttribute;
            const attributeData = edgeDistribution[colorAttribute];

            if (edgeSettings.colorAttributeType === 'number' && attributeData) {
              colorScales[label] = this.setNumericalColor(
                attributeData.statistics.min,
                attributeData.statistics.max,
                edgeSettings.colorScale,
              );
            }
          }
        });
      }

      this.setState({ colorScales });
    }

    return changeFlags.propsOrDataChanged || changeFlags.somethingChanged;
  }

  setNumericalColor(min: number, max: number, colorScale: string) {
    return scaleLinear<string>().domain([min, max]).range(['white', colorScale]).interpolate(interpolateRgb);
  }

  rgbStringToArray(rgbString: string): [number, number, number] {
    const rgb = color(rgbString);
    if (!rgb || !rgb.formatRgb()) return [0, 0, 0];
    const match = rgb.formatRgb().match(/\d+/g);
    if (!match || match.length !== 3) return [0, 0, 0];
    const [r, g, b] = match.map(Number) as [number, number, number];
    return [r, g, b];
  }

  getEdgeColor(d: EdgeQueryResult) {
    const graphMetadata = this.props.graphMetadata;
    const layerSettings = this.props.settings[NodeLinkLayer.type];

    if (layerSettings?.edges?.[d.label]?.colorByAttribute) {
      const attribute = layerSettings?.edges[d.label].colorAttribute;
      let attributeValue = d.attributes[attribute] as any;

      if (['string', 'categorical', 'boolean'].includes(layerSettings?.edges[d.label].colorAttributeType)) {
        const i = getCategories(graphMetadata, 'edges', d.label, attribute).indexOf(String(attributeValue));
        return nodeColorRGB(layerSettings?.edges[d.label]?.colorMapping?.[attributeValue] ?? i);
      } else if (layerSettings?.edges[d.label].colorAttributeType === 'number') {
        if (typeof attributeValue === 'string') {
          const numericValue = parseFloat(attributeValue.replace(/[^0-9.]/g, ''));
          if (!isNaN(numericValue)) {
            attributeValue = numericValue;
          }
        }
        const colorScale = this.state.colorScales[d.label];
        return this.rgbStringToArray(colorScale(attributeValue));
      }
    }
    return EDGE_COLOR_DEFAULT;
  }

  renderLayers() {
    const { data, settings, getNodeLocation, ml, graphMetadata, selected } = this.props;
    const layerSettings = settings[NodeLinkLayer.type];

    const brushingExtension = new BrushingExtension();
    const collisionFilter = new CollisionFilterExtension();

    const nodeLocations = data.nodes.reduce((acc: Record<string, Coordinate>, node: NodeQueryResult) => {
      const pos = getNodeLocation(node._id);
      if (pos && (pos[0] !== 0 || pos[1] !== 0)) {
        acc[node._id] = pos;
      }
      return acc;
    }, {});

    const hiddenNodes = new Set(data.nodes.filter(node => layerSettings.nodes[node.label]?.hidden).map(node => node._id));
    const showArrows = layerSettings?.showArrows;

    graphMetadata.edges.labels.forEach((label: string) => {
      const layerId = `${label}-edges-line`;
      const arrowPolygons: any[] = [];

      const parseEdge = (edge: EdgeQueryResult) => {
        if (edge == null || edge?.attributes?.hidden) return [];
        if (!nodeLocations[edge.from] || !nodeLocations[edge.to]) return [];

        const [sx, sy] = nodeLocations[edge.from];
        const [tx, ty] = nodeLocations[edge.to];

        const [wsx, wsy] = this.context.viewport.projectPosition([sx, sy, 1]);
        // eslint-disable-next-line prefer-const
        let [wtx, wty, wtz] = this.context.viewport.projectPosition([tx, ty, 1]);

        const hidden = hiddenNodes.has(edge.from) || hiddenNodes.has(edge.to);

        if (showArrows) {
          // Perpendicular normalized vector
          let ax = wtx - wsx;
          let ay = wty - wsy;
          const amag = Math.sqrt(ax * ax + ay * ay);
          if (amag != 0) {
            ax /= amag;
            ay /= amag;
          }

          const nodeTo = data.nodes.find(n => n._id == edge.to)!;
          const nodeRadius = wtz * getNodeSize(nodeTo, label);

          if (wtx - wsx != 0) wtx -= (nodeRadius / amag) * (wtx - wsx);
          if (wty - wsy != 0) wty -= (nodeRadius / amag) * (wty - wsy);

          // Draw arrow heads
          let px = wty - wsy;
          let py = -(wtx - wsx);
          const pmag = Math.sqrt(px * px + py * py);
          if (pmag != 0) {
            px /= pmag;
            py /= pmag;
          }

          arrowPolygons.push({
            _id: edge._id + '_arrow',
            label: edge.label,
            vectors: { wtx, wty, px, py, ax, ay },
            attributes: edge.attributes,
            hidden: hidden,
          });

          // Return the edge line so both the arrow and edge are rendered.
          return {
            _id: edge._id,
            label: edge.label,
            attributes: edge.attributes,
            from: [sx, sy, 0] as [number, number, number],
            to: this.context.viewport.unprojectPosition([wtx, wty]),
            hidden: hidden,
          };
        } else {
          return {
            _id: edge._id,
            label: edge.label,
            attributes: edge.attributes,
            from: [sx, sy, 0] as [number, number, number],
            to: [tx, ty, 0] as [number, number, number],
            hidden: hidden,
          };
        }
      };
      const edgeData = data.edges.flatMap(parseEdge).filter(edge => {
        return edge != null && edge.from && edge.to && !edge.hidden;
      });

      this._layers[layerId] = new LineLayer({
        id: layerId,
        data: edgeData,
        visible: !layerSettings?.edges[label]?.hidden,
        pickable: true,
        getWidth: layerSettings?.edges[label]?.width || 1,
        getSourcePosition: d => d.from,
        getTargetPosition: d => d.to,
        getColor: (d: EdgeQueryResult) => this.getEdgeColor(d),
        extensions: [brushingExtension],
        brushingEnabled: layerSettings?.enableBrushing,
      });

      // Create a new layer for filled arrow triangles if any exist.
      if (arrowPolygons.length > 0) {
        const arrowLayerId = `${label}-arrows`;
        this._layers[arrowLayerId] = new SolidPolygonLayer({
          id: arrowLayerId,
          data: arrowPolygons.filter(p => !p.hidden),
          visible: !layerSettings?.edges[label]?.hidden,
          pickable: true,
          // each data object has polygon property containing 3 points
          getPolygon: d => {
            const { wtx, wty, px, py, ax, ay } = d.vectors;
            const arrowSize = 10;
            const arrowRatio = 1.75;
            const arrowSizeScaled = arrowSize / 2 ** this.context.viewport.zoom;

            const tip = this.context.viewport.unprojectPosition([wtx, wty]);
            const point1 = this.context.viewport.unprojectPosition([
              wtx + (px - ax * arrowRatio) * arrowSizeScaled,
              wty + (py - ay * arrowRatio) * arrowSizeScaled,
            ]);
            const pointMid = this.context.viewport.unprojectPosition([wtx - ax * arrowSizeScaled * 0.8, wty - ay * arrowSizeScaled * 0.8]);
            const point2 = this.context.viewport.unprojectPosition([
              wtx + (-px - ax * arrowRatio) * arrowSizeScaled,
              wty + (-py - ay * arrowRatio) * arrowSizeScaled,
            ]);

            return [tip, point1, pointMid, point2];
          },
          getFillColor: (d: EdgeQueryResult) => this.getEdgeColor(d),
        });

        // ensure that arrow heads are rerendered every zoom event. Quite a performance hit!
        this.context.deck!.setProps({
          onViewStateChange: () => {
            this.setNeedsUpdate();
          },
        });
      }
    });

    if (ml?.linkPrediction?.enabled) {
      this._layers['link_prediction'] = new LineLayer({
        id: 'link-prediction-layer',
        data: ml.linkPrediction.result,
        pickable: false,
        getWidth: 1,
        getSourcePosition: d => d.from,
        getTargetPosition: d => d.to,
        getColor: d => [0, 0, 0],
      });
    }

    graphMetadata.nodes.labels.forEach((label: string) => {
      const layerId = `${label}-nodes-scatterplot`;
      const textLayerId = `${label}-label-target`;

      const nodes = data.nodes.filter(node => nodeLocations[node._id] && node.label === label);

      this._layers[layerId] = new ScatterplotLayer({
        id: layerId,
        visible: !layerSettings?.nodes[label]?.hidden,
        data: nodes,
        pickable: true,
        getFillColor: d => {
          if (layerSettings?.nodes[label].colorByAttribute) {
            const attribute = layerSettings?.nodes[label].colorAttribute;
            let attributeValue = d.attributes[attribute];
            if (['string', 'categorical', 'boolean'].includes(layerSettings?.nodes[label].colorAttributeType)) {
              return nodeColorRGB(
                layerSettings?.nodes[label]?.colorMapping[attributeValue] ??
                  (graphMetadata.nodes.types[label].attributes[attribute].statistics as CategoricalStats).values.indexOf(attributeValue),
              );
            } else if (layerSettings?.nodes[label].colorAttributeType === 'number') {
              if (typeof attributeValue === 'string') {
                const numericValue = parseFloat(attributeValue.replace(/[^0-9.]/g, ''));
                if (!isNaN(numericValue)) {
                  attributeValue = numericValue;
                }
              }
              const colorScale = this.state.colorScales[label];
              return this.rgbStringToArray(colorScale(attributeValue));
            }
          }
          return nodeColorRGB(layerSettings?.nodes[label].color);
        },
        getPosition: (d: NodeQueryResult) => getNodeLocation(d._id),
        // Dynamically compute node radius based on its degree
        getRadius: (d: NodeQueryResult) => getNodeSize(d, label),
        radiusMinPixels: layerSettings?.nodeSizeMultiplier ? 2 : 5,
        getLineWidth: (d: NodeQueryResult) => (selected && selected.some(sel => sel._id === d._id) ? 2 : 1),
        lineWidthUnits: 'pixels',
        stroked: true,
        updateTriggers: {
          getIcon: [selected],
          getRadius: [layerSettings?.nodes[label]?.size, data.edges],
          getFillColor: [this.state.colorScales],
        },
      });

      this._layers[textLayerId] = new TextLayer({
        id: textLayerId,
        data: nodes,
        getPosition: (d: NodeQueryResult) => getNodeLocation(d._id),
        getText: (d: NodeQueryResult) => d.label,
        getSize: (d: NodeQueryResult) => (getNodeSize(d, label) * 2.5) / d.label.length,
        getAlignmentBaseline: 'center',
        getRadius: 10,
        radiusScale: 20,
        getColor: [255, 255, 255],
        sizeUnits: 'meters',
        sizeMaxPixels: 64,
        characterSet: 'auto',
        fontFamily: 'InterVariable',
        billboard: false,
        getAngle: () => 0,
        collisionGroup: 'textLabels',
        extensions: [collisionFilter],
        collisionEnabled: layerSettings?.collisionEnabled ?? true,
        getCollisionPriority: () => 100,
        collisionTestProps: { sizeScale: 5 },
      });
    });

    function getNodeSize(d: NodeQueryResult, label: string) {
      const baseSize = layerSettings?.nodes[label]?.size ?? 40;
      const relationCount = data.edges.reduce((acc, edge) => {
        if (edge.from === d._id || edge.to === d._id) {
          return acc + ((edge.attributes?.Count as number) ?? (edge.attributes?.count as number) ?? 1);
        }
        return acc;
      }, 0);
      return Math.min(baseSize + relationCount * (layerSettings?.nodeSizeMultiplier ?? 0), 500000);
    }

    function getEdgeLocation(edge: EdgeQueryResult, viewport: Viewport) {
      const locationFrom = viewport.projectPosition([...getNodeLocation(edge.from), 1]);
      const locationTo = viewport.projectPosition([...getNodeLocation(edge.to), 1]);

      const result = viewport.unprojectPosition([(locationFrom[0] + locationTo[0]) / 2, (locationFrom[1] + locationTo[1]) / 2]);

      return [result[0], result[1]] as Coordinate;
    }

    function getEdgeAngle(edge: EdgeQueryResult, viewport: Viewport) {
      const locationFrom = viewport.projectPosition([...getNodeLocation(edge.from), 1]);
      const locationTo = viewport.projectPosition([...getNodeLocation(edge.to), 1]);

      const rads = Math.atan2(locationTo[1] - locationFrom[1], locationTo[0] - locationFrom[0]);
      const degrees = Math.abs(rads * (180 / Math.PI)) % 360;

      if (degrees > 90 && degrees < 270) {
        return rads * (180 / Math.PI) + 180;
      } else {
        return rads * (180 / Math.PI);
      }
    }

    function getEdgeLabel(edge: EdgeQueryResult, edgeSettings: MapEdgeData) {
      let attribute;
      try {
        attribute = edgeSettings.labelAttributes?.[edge.label];
      } catch (e) {
        return edge.attributes.type ?? '';
      }

      if (attribute == null || attribute == 'None') {
        return '';
      }

      if (attribute == 'Default' || attribute == null) {
        return edge.label ?? '';
      }

      const value = edge.attributes[attribute];

      if (Array.isArray(value)) {
        return value.join(', ');
      }

      if (typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean') {
        return String(value);
      }

      if (typeof value === 'object' && Object.keys(value as object).length != 0) {
        return JSON.stringify(value);
      }

      return '';
    }

    graphMetadata.edges.labels.forEach((label: string) => {
      const textLayerId = `${label}-edge-labels`;

      const layerSettings = settings?.[NodeLinkLayer.type];
      const edgeSettings = layerSettings?.edges?.[label] || {};
      const edges = data.edges.filter(
        (edge: EdgeQueryResult) =>
          nodeLocations[edge.from] && nodeLocations[edge.to] && edge.to != edge.from && edge.label === label && !edgeSettings.hidden,
      );

      this._layers[textLayerId] = new TextLayer({
        id: textLayerId,
        data: edges,
        getPosition: (d: EdgeQueryResult) => getEdgeLocation(d, this.context.viewport),
        getText: (d: EdgeQueryResult) => String(getEdgeLabel(d, edgeSettings)) || '',
        getSize: (d: EdgeQueryResult) => 12,
        getAlignmentBaseline: 'center',
        getColor: EDGE_COLOR_DEFAULT,
        outlineColor: [255, 255, 255, 128],
        outlineWidth: 8,
        fontSettings: { sdf: true },
        characterSet: 'auto',
        fontFamily: 'InterVariable',
        fontWeight: 700,
        billboard: false,
        getAngle: d => getEdgeAngle(d, this.context.viewport),
      });
    });

    return Object.values(this._layers);
  }
}

NodeLinkLayer.layerName = 'NodeLink';
