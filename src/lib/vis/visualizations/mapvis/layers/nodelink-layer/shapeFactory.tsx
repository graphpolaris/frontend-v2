function svgToDataURL(svg: string): string {
  return `data:image/svg+xml;charset=utf-8,${encodeURIComponent(svg)}`;
}

export const createIcon = (type: string, color: [number, number, number], selected: boolean = false) => {
  const strokeWidth = selected ? 8 : 2;

  switch (type) {
    case 'diamond':
      return svgToDataURL(diamond(color, strokeWidth));

    case 'star':
      return svgToDataURL(star(color, strokeWidth));

    case 'circle':
      return svgToDataURL(circle(color, strokeWidth));

    case 'square':
      return svgToDataURL(square(color, strokeWidth));

    case 'triangle':
      return svgToDataURL(triangle(color, strokeWidth));

    case 'location':
      return svgToDataURL(location(color, strokeWidth));

    default:
      return svgToDataURL(diamond(color, strokeWidth));
  }
};

const diamond = (rgbColor: [number, number, number], strokeWidth: number) => {
  return `<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">
            <polygon points="12,2 22,12 12,22 2,12" fill="rgb(${rgbColor[0]}, ${rgbColor[1]}, ${rgbColor[2]})" stroke="black" stroke-width="${strokeWidth}" />
          </svg>`;
};

const star = (rgbColor: [number, number, number], strokeWidth: number) => {
  return `<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">
            <polygon points="12,2 14,8 20,8 15,12 18,18 12,14 6,18 9,12 4,8 10,8" fill="rgb(${rgbColor[0]}, ${rgbColor[1]}, ${rgbColor[2]})" stroke="black" stroke-width="${strokeWidth}" />
          </svg>`;
};

const triangle = (rgbColor: [number, number, number], strokeWidth: number) => {
  return `<svg
        width="24"
        height="24"
        xmlns="http://www.w3.org/2000/svg"
        fill-rule="evenodd"
        clip-rule="evenodd"
      >
        <polygon points="12,2 22,22 2,22" fill="rgb(${rgbColor[0]}, ${rgbColor[1]}, ${rgbColor[2]})" stroke="black" stroke-width="${strokeWidth}" />
      </svg>`;
};

const circle = (rgbColor: [number, number, number], strokeWidth: number) => {
  return `<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">
            <circle cx="12" cy="12" r="12" fill="rgb(${rgbColor[0]}, ${rgbColor[1]}, ${rgbColor[2]})" stroke="black" stroke-width="${strokeWidth}" />
          </svg>`;
};

const square = (rgbColor: [number, number, number], strokeWidth: number) => {
  return `<svg
        width="24"
        height="24"
        xmlns="http://www.w3.org/2000/svg"
        fill-rule="evenodd"
        clip-rule="evenodd"
      >
        <path d="M0 0h24v24h-24z" fill="rgb(${rgbColor[0]}, ${rgbColor[1]}, ${rgbColor[2]})" stroke="black" stroke-width="${strokeWidth}" />
      </svg>`;
};

const location = (rgbColor: [number, number, number], strokeWidth: number) => {
  return `<svg width="34" height="34" xmlns="http://www.w3.org/2000/svg">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g transform="translate(-125.000000, -643.000000)">
                    <g transform="translate(37.000000, 169.000000)">
                        <g transform="translate(78.000000, 468.000000)">
                            <g transform="translate(10.000000, 6.000000)">
                                <path d="M14,0 C21.732,0 28,5.641 28,12.6 C28,23.963 14,36 14,36 C14,36 0,24.064 0,12.6 C0,5.641 6.268,0 14,0 Z" fill="rgb(${rgbColor[0]}, ${rgbColor[1]}, ${rgbColor[2]})" stroke="black" stroke-width="${strokeWidth}"></path>
                                <circle fill="#0C0058" fill-rule="nonzero" cx="14" cy="14" r="7"></circle>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
          </svg>`;
};
