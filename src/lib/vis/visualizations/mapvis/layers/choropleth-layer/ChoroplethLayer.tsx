import React from 'react';
import { CompositeLayer, Layer } from 'deck.gl';
import { ArcLayer, GeoJsonLayer } from '@deck.gl/layers';
import { netherlands } from '../../../../../mock-data/geo-json';
import { Node, LayerProps, CompositeLayerType, Coordinate } from '../../mapvis.types';
import { geoCentroid, geoContains } from 'd3';
import * as d3 from 'd3';

const colorScales: Record<string, any> = {
  green: d3.interpolateGreens,
  blue: d3.interpolateBlues,
  red: d3.interpolateReds,
  orange: d3.interpolateOranges,
  purple: d3.interpolatePurples,
};

export class ChoroplethLayer extends CompositeLayer<CompositeLayerType> {
  static layerName = 'choropleth';
  private _layers: Record<string, Layer> = {};

  constructor(props: LayerProps) {
    super(props);
  }

  updateState({ props, oldProps, changeFlags }: { props: any; oldProps: any; context: any; changeFlags: any }) {
    if (changeFlags.dataChanged) {
      const geojsonData = this.concatData(netherlands, props.data.nodes);
      this.setState({ geojsonData });
    }

    if (changeFlags.propsChanged) this.extractColorInformation();
  }

  concatData(geojson: any, nodes: Node[]) {
    const updatedGeojson = { ...geojson };

    nodes.forEach((node: Node) => {
      const coordinates: Coordinate = this.props.getNodeLocation(node.id);

      if (coordinates) {
        updatedGeojson.features.forEach((feature: any) => {
          if (geoContains(feature, coordinates)) {
            feature.properties.nodes = feature.properties.nodes ?? [];
            feature.properties.nodes.push(node.id);

            feature.properties.townships.forEach((township: any) => {
              if (geoContains(township, coordinates)) {
                township.properties.nodes = township.properties.nodes ?? [];
                township.properties.nodes.push(node.id);
              }
            });
          }
        });
      }
    });

    return updatedGeojson;
  }

  setColorScale(min: number, max: number) {
    const { settings } = this.props;
    const colorSettings = settings[ChoroplethLayer.layerName]?.colorScale ?? 'blue';
    const colorScale = colorScales[colorSettings];
    const sequentialScale = d3.scaleSequential(colorScale).domain([min, max]);
    this.setState({ colorScale: sequentialScale });
  }

  extractColorInformation() {
    const { settings, data } = this.props;
    const { geojsonData } = this.state;

    let min = Infinity;
    let max = -Infinity;

    const updateMinMax = (value: number) => {
      if (min > value) min = value;
      if (max < value) max = value;
    };

    (geojsonData as any).features.forEach((feature: any) => {
      const nodes = feature.properties?.nodes ?? [];

      switch (settings[ChoroplethLayer.layerName].coloringStrategy) {
        case 'Node count':
          updateMinMax(nodes.length);
          break;

        case 'Edge count':
          updateMinMax(data.edges.filter(edge => nodes.includes(edge.from) && nodes.includes(edge.to)).length);
          break;

        case 'Incoming edges':
          updateMinMax(data.edges.filter(edge => nodes.includes(edge.to)).length);
          break;

        case 'Outgoing edges':
          updateMinMax(data.edges.filter(edge => nodes.includes(edge.from)).length);
          break;

        case 'Connected edges':
          updateMinMax(data.edges.filter(edge => nodes.includes(edge.from) || nodes.includes(edge.to)).length);
          break;

        case 'Attribute':
          break;

        default:
          break;
      }
    });

    this.setColorScale(min, max);
  }

  getColorFromScale(value: number): [number, number, number] {
    const { colorScale } = this.state;
    if (!colorScale) return [100, 0, 0];

    // @ts-expect-error d3 types are not up to date
    const color = d3.rgb(colorScale(value));
    return [color.r, color.g, color.b];
  }

  getColor(polygon: any): [number, number, number] {
    const { data, settings } = this.props;

    const nodes = polygon.properties?.nodes ?? [];

    switch (settings[ChoroplethLayer.layerName].coloringStrategy) {
      case 'Node count':
        return this.getColorFromScale(nodes.length);

      case 'Edge count':
        return this.getColorFromScale(data.edges.filter(edge => nodes.includes(edge.from) && nodes.includes(edge.to)).length);

      case 'Incoming edges':
        return this.getColorFromScale(data.edges.filter(edge => nodes.includes(edge.to)).length);

      case 'Outgoing edges':
        return this.getColorFromScale(data.edges.filter(edge => nodes.includes(edge.from)).length);

      case 'Connected edges':
        return this.getColorFromScale(data.edges.filter(edge => nodes.includes(edge.from) || nodes.includes(edge.to)).length);

      case 'Attribute':
        return [0, 0, 0];

      default:
        return [0, 0, 0];
    }
  }

  renderLayers() {
    const { geojsonData } = this.state;
    const { hoverObject, getNodeLocation, data, settings } = this.props;
    const layerSettings = settings[ChoroplethLayer.layerName];

    (geojsonData as any).features.forEach((feature: any) => {
      const layerId = `${feature.properties.name}-area`;

      this._layers[layerId] = new GeoJsonLayer({
        id: `feature-layer-${feature.properties.name}`,
        data: feature,
        opacity: layerSettings?.opacity ?? 0.3,
        pickable: true,
        stroked: true,
        filled: true,
        extruded: false,
        lineWidthScale: 0.5,
        lineWidthMinPixels: 1,
        getLineWidth: () => 1,
        getLineColor: () => [220, 220, 220],
        getFillColor: (d: any) => this.getColor(d),
        updateTriggers: {
          getFillColor: [layerSettings.coloringStrategy, layerSettings.colorScale, layerSettings.opacity],
        },
      });
    });

    if (hoverObject && layerSettings.enableBrushing) {
      const nodes = (hoverObject as { properties?: { nodes: string[] } }).properties?.nodes ?? [];
      if (nodes) {
        const filteredEdges = data.edges.filter(edge => nodes.includes(edge.from) || nodes.includes(edge.to));
        // @ts-expect-error d3 types are not up to date
        const centroid = geoCentroid(hoverObject);

        if (filteredEdges.length > 0) {
          this._layers['hovered-edges'] = new ArcLayer({
            id: 'hovered-edges',
            data: filteredEdges,
            pickable: true,
            getWidth: () => 0.5,
            getSourcePosition: () => centroid,
            getTargetPosition: (d: any) => getNodeLocation(d.to),
            getSourceColor: (d: any) => layerSettings.edges[d.label].color,
            getTargetColor: (d: any) => layerSettings.edges[d.label].color,
          });
        }
      }
    }

    return Object.values(this._layers);
  }
}
