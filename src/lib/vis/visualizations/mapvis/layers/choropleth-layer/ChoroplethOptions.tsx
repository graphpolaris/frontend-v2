import React, { useEffect } from 'react';
import { ColorPicker } from '@/lib/components/colorComponents/colorPicker';
import { MapProps } from '../../mapvis';
import { DropdownColorLegend, EntityPill, Input, RelationPill } from '@/lib/components';
import { LayerSettingsComponentType } from '../../mapvis.types';
import { Accordion, AccordionBody, AccordionHead, AccordionItem } from '@/lib/components/accordion';

const areaColoringStrategies = ['Node count', 'Edge count', 'Incoming edges', 'Outgoing edges', 'Connected edges', 'Attribute'];

export type coloringStrategiesType = 'Node count' | 'Edge count' | 'Incoming edges' | 'Outgoing edges' | 'Connected edges' | 'Attribute';

export function ChoroplethOptions({
  settings,
  graphMetadata,
  spatialAttributes,
  updateLayerSettings,
  updateSpatialAttribute,
}: LayerSettingsComponentType<MapProps>) {
  const layerType = 'choropleth';
  const layerSettings = settings[layerType];

  useEffect(() => {
    if (!layerSettings) {
      const initialSettingsObject = {
        coloringStrategy: 'Node count',
        colorScale: 'orange',
        opacity: 0.8,
        nodes: {},
        edges: {},
      };

      graphMetadata.nodes.labels.forEach(node => {
        initialSettingsObject.nodes = {
          ...initialSettingsObject.nodes,
          [node]: {
            color: [0, 0, 0],
            hidden: false,
            fixed: true,
            min: 0,
            max: 10,
            sizeAttribute: '',
          },
        };
      });

      graphMetadata.edges.labels.forEach(edge => {
        initialSettingsObject.edges = {
          ...initialSettingsObject.edges,
          [edge]: {
            color: [0, 0, 0],
            onHover: true,
          },
        };
      });

      updateLayerSettings({ ...initialSettingsObject });
    }
  }, [graphMetadata, layerType, settings, updateLayerSettings]);

  return (
    layerSettings && (
      <div>
        <Accordion defaultOpenAll={true}>
          <AccordionItem className="mt-2">
            <AccordionHead className="flex items-center">
              <span className="font-semibold">General</span>
            </AccordionHead>
            <AccordionBody>
              <p className="text-bold">Area color</p>
              <Input
                inline
                label="Based on"
                type="dropdown"
                value={layerSettings?.coloringStrategy}
                options={areaColoringStrategies}
                onChange={val => updateLayerSettings({ coloringStrategy: val as coloringStrategiesType })}
              />
              <DropdownColorLegend value={settings?.colorScale} onChange={val => updateLayerSettings({ colorScale: val })} />
              <Input
                label="Opacity"
                type="slider"
                min={0}
                max={1}
                step={0.05}
                unit="%"
                value={layerSettings?.opacity ?? 0.8}
                onChange={val => updateLayerSettings({ opacity: val as number })}
              />
            </AccordionBody>
          </AccordionItem>

          {graphMetadata.nodes.labels.map(nodeType => {
            const nodeSettings = layerSettings?.nodes?.[nodeType] || {};

            return (
              <AccordionItem className="mt-2" key={nodeType}>
                <AccordionHead className="flex items-center">
                  <EntityPill title={nodeType} />
                </AccordionHead>

                <AccordionBody>
                  <div>
                    <Input
                      inline
                      label="Latitude"
                      type="dropdown"
                      value={settings?.location[nodeType]?.lat}
                      options={[...spatialAttributes[nodeType]]}
                      disabled={spatialAttributes[nodeType].length < 1}
                      onChange={val => updateSpatialAttribute(nodeType, 'lat', val as string)}
                    />
                    <Input
                      inline
                      label="Longitude"
                      type="dropdown"
                      value={settings?.location[nodeType]?.lon}
                      options={[...spatialAttributes[nodeType]]}
                      disabled={spatialAttributes[nodeType].length < 1}
                      onChange={val => updateSpatialAttribute(nodeType, 'lon', val as string)}
                    />
                  </div>
                </AccordionBody>
              </AccordionItem>
            );
          })}

          {graphMetadata.edges.labels.map(edgeType => {
            const edgeSettings = layerSettings?.edges?.[edgeType] || {};

            return (
              <AccordionItem key={edgeType} className="mt-2">
                <AccordionHead className="flex items-center">
                  <RelationPill title={edgeType} />
                </AccordionHead>

                <AccordionBody>
                  <div></div>
                </AccordionBody>
              </AccordionItem>
            );
          })}
        </Accordion>
      </div>
    )
  );
}
