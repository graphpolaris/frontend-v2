import React from 'react';
import { CompositeLayer, HeatmapLayer, Layer } from 'deck.gl';
import { CompositeLayerType, LayerProps } from '../../mapvis.types';
import { Node } from '@/lib/data-access';

export class HeatLayer extends CompositeLayer<CompositeLayerType> {
  static type = 'heatmap';
  private _layers: Record<string, Layer> = {};

  constructor(props: LayerProps) {
    super(props);
  }

  updateState({ changeFlags }: { changeFlags: any }) {
    return changeFlags.propsOrDataChanged || changeFlags.somethingChanged;
  }

  renderLayers() {
    const { data, settings, getNodeLocation, setLayerIds, graphMetadata } = this.props;
    const layerSettings = settings[HeatLayer.type];

    const layerIds: string[] = [];

    graphMetadata.nodes.labels.forEach((label: string) => {
      const layerId = `${label}-nodes-heatmaplayer`;
      layerIds.push(layerId);

      this._layers[layerId] = new HeatmapLayer<Node>({
        id: layerId,
        data: data.nodes.filter((node: Node) => node.label === label),
        visible: !layerSettings.nodes[label].hidden,
        getPosition: (d: any) => getNodeLocation(d._id),
        getWeight: (d: any) => layerSettings.nodes[label].size,
        radiusPixels: layerSettings.nodes[label].size,
        aggregation: 'SUM',
      });
    });

    setLayerIds(layerIds);

    return Object.values(this._layers);
  }
}

HeatLayer.layerName = 'Heatmap';
