import React, { useEffect } from 'react';
import { MapProps } from '../../mapvis';
import { EntityPill, Input } from '@/lib/components';
import { LayerSettingsComponentType } from '../../mapvis.types';
import { Accordion, AccordionBody, AccordionHead, AccordionItem } from '@/lib/components/accordion';

export function HeatLayerOptions({
  settings,
  graphMetadata,
  updateLayerSettings,
  spatialAttributes,
  updateSpatialAttribute,
}: LayerSettingsComponentType<MapProps>) {
  const layerType = 'heatmap';
  const layerSettings = settings[layerType];

  useEffect(() => {
    if (!layerSettings) {
      const initialSettingsObject = { nodes: {}, edges: {} };

      graphMetadata.nodes.labels.forEach(node => {
        initialSettingsObject.nodes = {
          ...initialSettingsObject.nodes,
          [node]: {
            size: 10,
            hidden: false,
          },
        };
      });

      graphMetadata.edges.labels.forEach(edge => {
        initialSettingsObject.edges = {
          ...initialSettingsObject.edges,
          [edge]: {},
        };
      });

      updateLayerSettings({ ...initialSettingsObject });
    }
  }, [graphMetadata, layerType, settings, updateLayerSettings]);

  return (
    layerSettings && (
      <div>
        <Accordion defaultOpenAll={true}>
          {graphMetadata.nodes.labels.map(nodeType => {
            const nodeSettings = layerSettings?.nodes?.[nodeType] || {};

            return (
              <AccordionItem className="mt-2" key={nodeType}>
                <AccordionHead className="flex items-center">
                  <EntityPill title={nodeType} />
                </AccordionHead>

                <AccordionBody>
                  <div>
                    <Input
                      label="Hidden"
                      type="boolean"
                      value={nodeSettings?.hidden ?? false}
                      onChange={val => {
                        updateLayerSettings({
                          nodes: { ...layerSettings.nodes, [nodeType]: { ...nodeSettings, hidden: val } },
                        });
                      }}
                    />
                    <Input
                      inline
                      label="Latitude"
                      type="dropdown"
                      value={settings?.location[nodeType]?.lat}
                      options={[...spatialAttributes[nodeType]]}
                      disabled={spatialAttributes[nodeType].length < 1}
                      onChange={val => updateSpatialAttribute(nodeType, 'lat', val as string)}
                    />
                    <Input
                      inline
                      label="Longitude"
                      type="dropdown"
                      value={settings?.location[nodeType]?.lon}
                      options={[...spatialAttributes[nodeType]]}
                      disabled={spatialAttributes[nodeType].length < 1}
                      onChange={val => updateSpatialAttribute(nodeType, 'lon', val as string)}
                    />
                    <Input
                      label="Size"
                      type="slider"
                      min={0}
                      max={40}
                      step={1}
                      value={nodeSettings?.size}
                      onChange={val => {
                        updateLayerSettings({
                          nodes: { ...layerSettings.nodes, [nodeType]: { ...nodeSettings, size: Number(val) } },
                        });
                      }}
                    />
                  </div>
                </AccordionBody>
              </AccordionItem>
            );
          })}
        </Accordion>
      </div>
    )
  );
}
