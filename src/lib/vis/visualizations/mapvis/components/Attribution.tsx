import React from 'react';

export function Attribution() {
  return (
    <div className="absolute right-0 bottom-0 p-1 bg-secondary-200 bg-opacity-75 text-xs">
      {'© '}
      <a className="underline" href="http://www.openstreetmap.org/copyright" target="_blank" rel="noopener noreferrer">
        OpenStreetMap
      </a>
    </div>
  );
}
