import React, { useEffect, useMemo } from 'react';
import { SettingsContainer } from '../../../components/config';
import { layerSettings, LayerTypes, layerTypes } from '../layers';
import { Input } from '../../../..';
import { VisualizationSettingsPropTypes } from '../../../common';
import { MapProps } from '../mapvis';
import { LayerSettingsType } from '../mapvis.types';

export const MapSettings = ({ settings, graphMetadata, updateSettings }: VisualizationSettingsPropTypes<MapProps>) => {
  // For backwards compatibility with older saveStates, we migrate information from settings.nodes to settings.location
  // FIXME: this can be removed once all systems have updated their saveStates.
  if (!('location' in settings)) {
    settings = JSON.parse(JSON.stringify(settings)); // Undo Object.preventExtensions()
    settings.location = Object.entries(settings.nodes)
      .map(([k, v]) => [
        k,
        {
          lat: (v as any).lat,
          lon: (v as any).lon,
        },
      ])
      .reduce((obj, [k, v]) => ({ ...obj, [k as string]: v }), {});
  }

  const DataLayerSettings = settings.layer && layerSettings?.[settings.layer];

  const updateLayerSettings = (updatedKeyValue: Partial<LayerSettingsType>) => {
    updateSettings({
      ...settings,
      [settings.layer]: {
        ...settings[settings.layer],
        ...updatedKeyValue,
      },
    });
  };

  const spatialAttributes = useMemo(() => {
    return Object.fromEntries(
      graphMetadata.nodes.labels.map(node => [
        node,
        Object.entries(graphMetadata.nodes.types[node].attributes)
          .filter(([, value]) => value.attributeType === 'number')
          .map(([key]) => key),
      ]),
    );
  }, [graphMetadata]);

  const updateSpatialAttribute = (label: string, attribute: 'lat' | 'lon', value: string) => {
    updateSettings({
      ...settings,
      location: {
        ...settings.location,
        [label]: {
          ...settings.location[label],
          [attribute]: value,
        },
      },
    });
  };

  useEffect(() => {
    function isLatitude(attr: string) {
      return ['lat', 'latitude'].some(x => attr.toLowerCase().includes(x));
    }
    function isLongitude(attr: string) {
      return ['lon', 'lng', 'long', 'longitude'].some(x => attr.toLowerCase().includes(x));
    }

    // Autodetect a lat or lon attribute if not already set
    graphMetadata.nodes.labels.forEach(node => {
      if ((!settings.location[node]?.lat || !settings.location[node]?.lon) && node in spatialAttributes) {
        const lat = spatialAttributes[node].find(isLatitude);
        const lon = spatialAttributes[node].find(isLongitude);
        if (lat && lon) {
          updateSettings({
            location: {
              ...settings.location,
              [node]: { ...settings.location[node], lat, lon },
            },
          });
        }
      }
    });
  }, [spatialAttributes, settings, updateSettings]);

  return (
    <SettingsContainer>
      <Input
        label="Data layer"
        type="dropdown"
        inline
        value={settings.layer}
        options={Object.keys(layerTypes)}
        onChange={val => updateSettings({ layer: val as LayerTypes })}
      />
      {DataLayerSettings && !!spatialAttributes && (
        <DataLayerSettings
          graphMetadata={graphMetadata}
          settings={settings}
          updateLayerSettings={updateLayerSettings}
          spatialAttributes={spatialAttributes}
          updateSpatialAttribute={updateSpatialAttribute}
        />
      )}
    </SettingsContainer>
  );
};
