import React from 'react';
import { Icon } from '@/lib/components';
import { BoundingBoxType } from '../mapvis.types';
import { SearchBar } from './SearchBar';

type Props = {
  isSearching: boolean;
  setIsSearching: (val: boolean) => void;
  setSearchResult: (val: any) => void;
  setSelectingRectangle: (val: boolean) => void;
  flyToBoundingBox: (minLat: number, maxLat: number, minLon: number, maxLon: number) => void;
};

export function ActionBar({ isSearching, setIsSearching, setSearchResult, setSelectingRectangle, flyToBoundingBox }: Props) {
  return (
    <div>
      <div className="absolute left-0 top-0 m-1">
        <div className="cursor-pointer p-1 pb-0 bg-light shadow-md rounded" onClick={() => setSelectingRectangle(true)}>
          <Icon component="icon-[ic--baseline-highlight-alt]" />
        </div>
        <div className="cursor-pointer p-1 mt-1 pb-0 bg-light shadow-md rounded" onClick={() => setIsSearching(!isSearching)}>
          <Icon component="icon-[ic--outline-search]" />
        </div>
      </div>
      {isSearching && (
        <SearchBar
          onSearch={(boundingBox: BoundingBoxType, locationInfo) => {
            flyToBoundingBox(...boundingBox);
            setIsSearching(false);
            setSearchResult(locationInfo);
          }}
        />
      )}
    </div>
  );
}
