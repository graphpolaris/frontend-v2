import React from 'react';
import { NodeType } from '../../nodelinkvis/types';
import { GeoJsonType } from '../mapvis.types';
import { SearchResultType } from '../mapvis.types';
import { PopoverProvider } from '@/lib/components';

export type NodelinkPopupProps = {
  type: 'node' | 'area' | 'location';
  data: NodeType | GeoJsonType | SearchResultType;
};

const isGeoJsonType = (data: NodeType | GeoJsonType | SearchResultType): data is GeoJsonType => {
  return (data as GeoJsonType).properties !== undefined;
};

const MapNodeDetails = (node: NodeType) => (
  <div>
    {Object.keys(node.attributes).length === 0 ? (
      <div className="flex justify-center items-center h-full">
        <span>No attributes</span>
      </div>
    ) : (
      Object.entries(node.attributes).map(([k, v]) => (
        <div key={k} className="flex flex-row gap-1 items-center min-h-5">
          <span className="font-semibold truncate min-w-[40%]">{k}</span>
          <span className="ml-auto text-right truncate grow-1 flex items-center">
            {v !== undefined && (typeof v !== 'object' || Array.isArray(v)) && v != '' ? (
              <span className="ml-auto text-right truncate">{typeof v === 'number' ? v.toLocaleString('de-DE') : v.toString()}</span>
            ) : (
              <div
                className={`ml-auto mt-auto h-4 w-12 border-[1px] solid border-gray`}
                style={{
                  background:
                    'repeating-linear-gradient(-45deg, transparent, transparent 6px, #eaeaea 6px, #eaeaea 8px), linear-gradient(to bottom, transparent, transparent)',
                }}
              ></div>
            )}
          </span>
        </div>
      ))
    )}
  </div>
);
const MapChoroplethDetails = (geoJson: GeoJsonType) => (
  <div>
    <div className="flex flex-row gap-3">
      <span>Area&nbsp;id: </span>
      <span className="ml-auto max-w-[10rem] text-right truncate">
        <span>{geoJson.properties?.regioFacetId ?? 'N/A'}</span>
      </span>
    </div>
    <div className="flex flex-row gap-3">
      <span>Nodes in area: </span>
      <span className="ml-auto max-w-[10rem] text-right truncate">
        <span>{geoJson.properties?.nodes?.length ?? 0}</span>
      </span>
    </div>
    <div className="flex flex-row gap-3">
      <span>Townships in area: </span>
      <span className="ml-auto max-w-[10rem] text-right truncate">
        <span>{geoJson.properties?.townships?.length ?? 0}</span>
      </span>
    </div>
  </div>
);

const renderLocationDetails = (location: SearchResultType) => (
  <div>
    <div className="flex flex-row gap-3">
      <span>Name: </span>
      <span className="ml-auto max-w-[10rem] text-right truncate">
        <span>{location?.display_name}</span>
      </span>
    </div>
    <div className="flex flex-row gap-3">
      <span>Type: </span>
      <span className="ml-auto max-w-[10rem] text-right truncate">
        <span>{location?.addresstype}</span>
      </span>
    </div>
    <div className="flex flex-row gap-3">
      <span>Coordinate: </span>
      <span className="ml-auto max-w-[10rem] text-right truncate">
        <span>
          [{location?.lon}, {location?.lat}]
        </span>
      </span>
    </div>
  </div>
);

export const MapDataInspector = (props: NodelinkPopupProps) => {
  const { type, data } = props;

  return (
    <PopoverProvider delay={100}>
      <div className="text-[0.9rem] min-w-[10rem]">
        <div className="p-0">
          <div className="px-2.5 text-[0.8rem]">
            {type === 'node'
              ? data && 'attributes' in data
                ? MapNodeDetails(data as NodeType)
                : null
              : data && isGeoJsonType(data)
                ? MapChoroplethDetails(data as GeoJsonType)
                : renderLocationDetails(data as SearchResultType)}
          </div>
          <div className="h-[1px] w-full"></div>
        </div>
      </div>
    </PopoverProvider>
  );
};
