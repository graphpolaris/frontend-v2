import React, { useState } from 'react';
import { Button, Input } from '@/lib/components';
import { useAppDispatch } from '@/lib/data-access';
import { addError } from '@/lib/data-access/store/configSlice';
import { BoundingBoxType } from '../mapvis.types';

interface SearchBarProps {
  onSearch: (boundingBox: BoundingBoxType, locationInfo: any) => void;
}

export const SearchBar: React.FC<SearchBarProps> = ({ onSearch }) => {
  const dispatch = useAppDispatch();
  const [query, setQuery] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const handleSearch = async () => {
    setIsLoading(true);

    try {
      const response = await fetch(`https://nominatim.openstreetmap.org/search?q=${encodeURIComponent(query)}&format=json`);
      const data = await response.json();
      if (data.length > 0) {
        const { boundingbox } = data[0];
        if (boundingbox) {
          onSearch(boundingbox.map(parseFloat), data[0]);
        }
      } else {
        dispatch(addError('No results found'));
      }
    } catch (error) {
      dispatch(addError('Error fetching coordinates'));
    }

    setIsLoading(false);
  };

  return (
    <div className="absolute bottom-0 left-1/2 transform -translate-x-1/2 z-50 m-1 p-2 bg-light shadow-md rounded w-full max-w-xl">
      <div className="flex gap-2 items-center">
        <Input type="text" size="xs" value={query} onChange={value => setQuery(value)} />
        <Button label="Search" size="xs" onClick={handleSearch} disabled={isLoading} />
      </div>
    </div>
  );
};
