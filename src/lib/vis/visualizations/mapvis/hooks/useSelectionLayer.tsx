import { useMemo } from 'react';
import { SelectionLayer } from '@deck.gl-community/editable-layers';

export const useSelectionLayer = (selectingRectangle: boolean, layerIds: string[], onSelect: (pickingInfos: any[]) => void) => {
  return useMemo(() => {
    return (
      selectingRectangle &&
      new SelectionLayer({
        id: 'selection',
        selectionType: 'rectangle',
        onSelect: ({ pickingInfos }: { pickingInfos: any[] }) => onSelect(pickingInfos),
        layerIds: layerIds,
        getTentativeFillColor: () => [22, 37, 67, 100],
      })
    );
  }, [selectingRectangle, layerIds, onSelect]);
};
