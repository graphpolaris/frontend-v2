import { useMemo } from 'react';
import { LocationInfo, Coordinate } from '../mapvis.types';
import { Node } from '@/lib/data-access';

export const useCoordinateLookup = (nodes: Node[], locationSettings: Record<string, LocationInfo>) => {
  return useMemo(() => {
    return nodes.reduce(
      (acc, node) => {
        const latitude = locationSettings?.[node.label]?.lat
          ? (node?.attributes?.[locationSettings[node.label].lat as any] as string)
          : undefined;
        const longitude = locationSettings?.[node.label]?.lon
          ? (node?.attributes?.[locationSettings[node.label].lon as any] as string)
          : undefined;

        if (latitude !== undefined && longitude !== undefined) {
          acc[node._id] = [parseFloat(longitude), parseFloat(latitude)];
        }

        return acc;
      },
      {} as { [id: string]: Coordinate },
    );
  }, [nodes, locationSettings]);
};
