import { configureStore } from '@reduxjs/toolkit';
import { Meta } from '@storybook/react';
import { Provider } from 'react-redux';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import MapComponent from './mapvis';
import { mockData } from '../../../mock-data/query-result/mockData';

const Mockstore = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
  },
});

const Component: Meta<typeof MapComponent.component> = {
  title: 'Visualizations/MapVis',
  component: MapComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const NodeLink = {
  args: {
    ...(await mockData.mockMobilityQueryResult()),
    settings: {
      layer: 'nodelink',
      location: { parkings: { lat: 'lat', lon: 'long' } },
      nodelink: {
        enableBrushing: false,
        nodes: {
          parkings: {
            colorByAttribute: false,
            colorAttribute: undefined,
            colorAttributeType: undefined,
            hidden: false,
            shape: 'circle',
            color: [6, 147, 227],
            size: 10,
          },
        },
        edges: {
          rides: {
            hidden: false,
            width: 1,
            sizeAttribute: '',
            fixed: true,
            color: [6, 147, 227],
          },
        },
      },
    },
    handleSelect: () => {},
  },
};

// TODO!: mapvis.heatmaps is not working
export const Heatmap = {
  args: {
    ...(await mockData.mockMobilityQueryResult()),
    settings: {
      layer: 'heatmap',
      location: { parkings: { lat: 'lat', lon: 'long' } },
      heatmap: {
        nodes: {
          parkings: {
            size: 10,
            hidden: false,
          },
        },
        edges: {
          rides: {},
        },
      },
    },
    handleSelect: () => {},
  },
};

export const Choropleth = {
  args: {
    ...(await mockData.mockMobilityQueryResult()),
    settings: {
      layer: 'choropleth',
      location: { parkings: { lat: 'lat', lon: 'long' } },
      choropleth: {
        coloringStrategy: 'Node count',
        colorScale: 'orange',
        opacity: 0.8,
        nodes: {
          parkings: {
            color: [0, 0, 0],
            hidden: false,
            fixed: true,
            min: 0,
            max: 10,
            sizeAttribute: '',
          },
        },
        edges: {
          rides: {
            color: [0, 0, 0],
            onHover: true,
          },
        },
      },
    },
    handleSelect: () => {},
  },
};

export default Component;
