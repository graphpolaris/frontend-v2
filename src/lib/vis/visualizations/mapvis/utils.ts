import { visualizationColors } from '@/config';
import { NodeType } from '../nodelinkvis/types';
import { GeoJsonType } from './mapvis.types';
import { SearchResultType } from './mapvis.types';

export function nodeColorRGB(num?: number): [number, number, number] {
  const colorVal = nodeColorHex(num);
  const hex = colorVal.replace(/^#/, '');
  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);
  return [r, g, b];
}

export function nodeColorHex(num?: number): string {
  let colorVal = visualizationColors.GPCat.colors[14][(num ?? 0) % visualizationColors.GPCat.colors[14].length];
  if (colorVal == null) colorVal = visualizationColors.GPCat.colors[14][0];
  return colorVal;
}

export const isGeoJsonType = (data: NodeType | GeoJsonType | SearchResultType): data is GeoJsonType => {
  return (data as GeoJsonType).properties !== undefined;
};

export const rgbToHex = (r: number, g: number, b: number): string => {
  const toHex = (n: number) => n.toString(16).padStart(2, '0');
  return `#${toHex(r)}${toHex(g)}${toHex(b)}`;
};
