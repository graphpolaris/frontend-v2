import { CompositeLayer } from 'deck.gl';
import { VisualizationPropTypes, VisualizationSettingsType } from '../../common';
import { MapProps } from './mapvis';
import { GraphStatistics, NodeQueryResult } from 'ts-common';

export type Coordinate = [number, number];

export type LocationInfo = { lat: string; lon: string };

export type MapNodeData = {
  color: number;
  hidden: boolean;
  fixed: boolean;
  min: number;
  max: number;
  radius: number;
  collapsed: boolean;
  lat?: string;
  lon?: string;
  shape: string;
  size: number;
  sizeAttribute?: string;
  colorByAttribute?: boolean;
  colorAttribute?: string | undefined;
  colorAttributeType?: string | undefined;
  colorScale: string;
  colorMapping?: { [label: string]: number };
};

export type MapEdgeData = {
  hidden: boolean;
  fixed: boolean;
  min: number;
  max: number;
  radius: number;
  collapsed: boolean;
  size: number;
  width: number;
  sizeAttribute?: string;
  enableBrushing?: boolean;
  colorByAttribute?: boolean;
  colorAttribute?: string | undefined;
  colorAttributeType?: string | undefined;
  colorScale: string;
  colorMapping?: { [label: string]: number };
  labelAttributes?: { [edgeType: string]: string };
};

export const EDGE_COLOR_DEFAULT: [number, number, number] = [132, 150, 155];

export type LayerSettingsType = {
  nodes: Record<string, MapNodeData>;
  edges: Record<string, MapEdgeData>;
  coloringStrategy?: string;
  colorScale?: string;
  opacity?: number;
  [id: string]: any;
};

export type LayerSettingsComponentType<T = object> = {
  settings: T & VisualizationSettingsType;
  graphMetadata: GraphStatistics;
  spatialAttributes: { [k: string]: string[] };
  updateSpatialAttribute: (label: string, attribute: 'lat' | 'lon', value: string) => void;
  updateLayerSettings: (val: Partial<LayerSettingsType>) => void;
};

export interface LayerProps {
  [key: string]: any;
}

export type CompositeLayerType = VisualizationPropTypes<MapProps> & {
  selected: any[];
  hoverObject: NodeQueryResult | null;
  getNodeLocation: (d: string) => Coordinate;
  flyToBoundingBox: (minLat: number, maxLat: number, minLon: number, maxLon: number, options?: { [key: string]: any }) => void;
  setLayerIds: (val: string[]) => void;
};

export type Layer = {
  id: string;
  component: CompositeLayer<CompositeLayerType>;
  settings: MapProps & VisualizationSettingsType;
};

export type Node = {
  id: string;
  attributes: {
    long: number;
    lat: number;
    [key: string]: any;
  };
  connectedEdges: string[];
  [key: string]: any;
};

export type Edge = {
  id: string;
  from: string;
  to: string;
  attributes: {
    [key: string]: any;
  };
  [key: string]: any;
};

export type GeoJsonType = {
  properties: {
    name: string;
    [id: string]: any;
  };
  [id: string]: any;
};

export type SearchResultType = {
  addresstype: string;
  boundingbox: number[];
  class: string;
  display_name: string;
  name: string;
  lat: string;
  lon: string;
  type: string;
  x?: number;
  y?: number;
};

export type BoundingBoxType = [number, number, number, number];
