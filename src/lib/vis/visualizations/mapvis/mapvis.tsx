import React, { useEffect, useCallback, useState, useRef, forwardRef, RefObject, useImperativeHandle } from 'react';
import DeckGL from '@deck.gl/react';
import { CompositeLayer, FlyToInterpolator, MapViewState, WebMercatorViewport } from '@deck.gl/core';
import { CompositeLayerType, Coordinate, LayerSettingsType, LocationInfo, SearchResultType } from './mapvis.types';
import { VISComponentType, VisualizationPropTypes } from '../../common';
import { layerTypes, createBaseMap, LayerTypes } from './layers';
import { geoCentroid } from 'd3';
import { Attribution, ActionBar, MapDataInspector, MapSettings } from './components';
import { useSelectionLayer, useCoordinateLookup } from './hooks';
import { NodeDetails } from '@/lib/components/nodeDetails';
import { Popover, PopoverContent, PopoverProvider, PopoverTrigger } from '@/lib/components/popover';
import { isGeoJsonType, nodeColorHex, rgbToHex } from './utils';
import { NodeType } from '../nodelinkvis/types';
import { ChoroplethLayer } from './layers/choropleth-layer/ChoroplethLayer';
import { Map } from 'react-map-gl';
import { getEnvVariable } from '@/config';
import { NodeQueryResult } from 'ts-common';

export type MapProps = {
  layer: LayerTypes;
  location: Record<string, LocationInfo>;
} & Partial<Record<LayerTypes, LayerSettingsType>>;

const settings: MapProps = {
  layer: 'nodelink',
  location: {},
};

const INITIAL_VIEW_STATE: MapViewState = {
  latitude: 52.1006,
  longitude: 5.6464,
  zoom: 1,
  bearing: 0,
  pitch: 0,
};

const FLY_SPEED = 1000;

export const MapVis = forwardRef((props: VisualizationPropTypes<MapProps>, refExternal) => {
  const ref = useRef<HTMLDivElement>(null);
  const [viewport, setViewport] = useState<Record<string, any>>(INITIAL_VIEW_STATE);
  const [hoverObject, setHoverObject] = useState<NodeQueryResult | null>(null);
  const [selected, setSelected] = useState<any[]>([]);
  const [selectingRectangle, setSelectingRectangle] = useState<boolean>(false);
  const [layerIds, setLayerIds] = useState<string[]>([]);
  const [isSearching, setIsSearching] = useState<boolean>(false);
  const [searchResult, setSearchResult] = useState<SearchResultType | undefined>(undefined);

  const coordinateLookup = useCoordinateLookup(props.data.nodes, props.settings.location);
  const [shouldExport, setShouldExport] = useState<boolean>(false);

  const captureImage = () => {
    const canvas = ref.current?.querySelector('canvas');

    if (canvas) {
      canvas.toBlob(blob => {
        if (blob) {
          const link = document.createElement('a');
          link.download = 'map-visualization.png';
          link.href = URL.createObjectURL(blob);
          link.click();
        } else {
          console.error('Failed to capture canvas');
        }
      }, 'image/png');
    } else {
      console.error('Canvas element not found');
    }
  };

  const exportImageInternal = () => {
    requestAnimationFrame(() => {
      captureImage();
      props.updateSettings({ export2PNG: false });
    });
  };
  // !FIXME I have to wrap exportImageInternal() with a useEffect otherwise didnt work, by just calling exportImageInternal() from outside
  useEffect(() => {
    if (props.settings.export2PNG) {
      exportImageInternal();
    }
  }, [props.settings.export2PNG]);

  useEffect(() => {
    if (shouldExport) {
      exportImageInternal();
      setShouldExport(false);
    }
  }, [shouldExport]);

  useImperativeHandle(refExternal, () => ({
    exportImageInternal: () => setShouldExport(true),
  }));

  const getFittedViewport = useCallback(
    (minLat: number, maxLat: number, minLon: number, maxLon: number) => {
      const viewportWebMercator = new WebMercatorViewport(viewport).fitBounds(
        [
          [minLon, minLat],
          [maxLon, maxLat],
        ],
        { padding: 20 },
      );
      const { zoom, longitude, latitude } = viewportWebMercator;
      return { zoom, longitude, latitude };
    },
    [viewport],
  );

  const flyToBoundingBox = useCallback(
    (minLat: number, maxLat: number, minLon: number, maxLon: number, options = {}) => {
      const fittedViewport = getFittedViewport(minLat, maxLat, minLon, maxLon);
      setViewport(prevViewport => ({
        ...prevViewport,
        ...options,
        ...fittedViewport,
        transitionDuration: FLY_SPEED,
        transitionInterpolator: new FlyToInterpolator(),
      }));
    },
    [getFittedViewport],
  );

  const [dataLayer, setDataLayer] = useState<{ component: CompositeLayer<CompositeLayerType>; id: string } | null>(null);

  useEffect(() => {
    if (!props.settings.layer) {
      setDataLayer(null);
      return;
    }

    const layer = {
      id: props.settings.layer,
      component: props.settings.layer ? layerTypes?.[props.settings.layer] : layerTypes.nodelink,
    };

    const layerProps: CompositeLayerType = {
      ...props,
      selected: selected,
      hoverObject: hoverObject,
      getNodeLocation: (d: string) => coordinateLookup[d],
      flyToBoundingBox: flyToBoundingBox,
      setLayerIds: (val: string[]) => setLayerIds(val),
    };
    if (dataLayer && dataLayer.id === layer.id) {
      setDataLayer({ component: dataLayer.component.clone(layerProps), id: props.settings.layer });
    } else {
      setDataLayer({ component: new layer.component(layerProps), id: props.settings.layer });
      setSelected([]);
    }
  }, [props.settings.layer, props.data, selected, hoverObject, props.settings]);

  const selectionLayer = useSelectionLayer(selectingRectangle, layerIds, (pickingInfos: any[]) => {
    const nodes: NodeQueryResult[] = [];
    const edges: any[] = [];
    pickingInfos.forEach(({ object }) => {
      if (object._id) {
        if (object.from && object.to) {
          edges.push(object);
        } else {
          nodes.push(object);
        }
      }
    });
    setSelected(nodes.map(node => node._id));
    props.handleSelect({ nodes, edges });
    setSelectingRectangle(false);
  });

  const coordinateToXY = useCallback(
    (coordinate: Coordinate) => {
      const [longitude, latitude] = coordinate;
      return new WebMercatorViewport(viewport).project([longitude, latitude]);
    },
    [viewport],
  );

  useEffect(() => {
    if (selected.length > 0) {
      const updatedSelected = selected.map(node => {
        let x, y;
        if (props.settings.layer === 'nodelink' && node.lon && node.lat) {
          const coordinate: Coordinate = [parseFloat(node.lon), parseFloat(node.lat)];
          [x, y] = coordinateToXY(coordinate);
        } else if (props.settings.layer === 'choropleth') {
          const centroid = geoCentroid(node);
          if (centroid) {
            [x, y] = coordinateToXY([centroid[0], centroid[1]]);
          }
        }
        return { ...node, x, y };
      });
      setSelected(updatedSelected);
    }
  }, [viewport]);

  const handleClick = useCallback(
    (object: any) => {
      if (props.data) {
        // Deselect everything if no object is clicked
        if (!object) {
          props.handleSelect();
          setSelected([]);
          setSearchResult(undefined);
          return;
        }

        // Handle clicking on a node (when it's not a feature in the choropleth layer)
        if (
          Object.prototype.hasOwnProperty.call(object, 'attributes') &&
          Object.prototype.hasOwnProperty.call(object, '_id') &&
          Object.prototype.hasOwnProperty.call(object, 'label')
        ) {
          const objectLocation: Coordinate = coordinateLookup[object._id];
          props.handleSelect({ nodes: [object] });

          if (objectLocation) {
            const [x, y] = coordinateToXY(objectLocation);
            setSelected([
              {
                ...object,
                x,
                y,
                lon: objectLocation[0],
                lat: objectLocation[1],
                selectedType: 'node',
              },
            ]);
          }
        }

        // Handle clicking on a geographic feature (Choropleth layer)
        if (object.type === 'Feature' && props.settings.layer === 'choropleth') {
          // Ensure the current dataLayer is of ChoroplethLayer type
          if (dataLayer?.component instanceof ChoroplethLayer) {
            const choroplethLayer = dataLayer.component;

            // Get the color of the selected area using the ChoroplethLayer's getColor method
            const selectedColor = choroplethLayer.getColor(object);

            const centroid = geoCentroid(object);
            if (centroid) {
              const [x, y] = coordinateToXY(centroid);
              setSelected([
                {
                  ...object,
                  x,
                  y,
                  selectedType: 'area',
                  color: selectedColor,
                },
              ]);
            }
            // If the feature contains node IDs, handle selecting the corresponding nodes
            const ids = object.properties.nodes;
            if (ids && ids.length > 0) {
              const nodes = props.data.nodes.filter(node => ids.includes(node._id));
              props.handleSelect({ nodes: [...nodes] });
            }
          }
        }
      }
    },
    [props, coordinateLookup, coordinateToXY, dataLayer],
  );

  useEffect(() => {
    if (searchResult) {
      const coordinate: Coordinate = [parseFloat(searchResult.lon), parseFloat(searchResult.lat)];
      const [x, y] = coordinateToXY(coordinate);

      if (searchResult.x !== x || searchResult.y !== y) {
        setSearchResult(prev => (prev ? { ...prev, x, y } : undefined));
      }
    }
  }, [viewport, searchResult, coordinateToXY]);

  let mapboxToken = getEnvVariable('MAPBOX_TOKEN');
  if (mapboxToken == '') mapboxToken = undefined;
  const layers = mapboxToken != null ? [dataLayer?.component, selectionLayer] : [createBaseMap(), dataLayer?.component, selectionLayer];

  return (
    <div className="w-full h-full flex-grow relative overflow-hidden" ref={ref}>
      <DeckGL
        layers={layers}
        controller={true}
        initialViewState={viewport}
        onViewStateChange={({ viewState }) => setViewport(viewState)}
        onClick={({ object }) => handleClick(object)}
        onHover={({ object }) => setHoverObject(object !== undefined ? object : null)}
      >
        {mapboxToken != null ? (
          <Map antialias={true} attributionControl={false} mapStyle="mapbox://styles/mapbox/streets-v9" mapboxAccessToken={mapboxToken} />
        ) : null}
      </DeckGL>

      <ActionBar
        isSearching={isSearching}
        setIsSearching={setIsSearching}
        setSearchResult={setSearchResult}
        setSelectingRectangle={setSelectingRectangle}
        flyToBoundingBox={flyToBoundingBox}
      />
      {selected.length > 0 &&
        selected.map((node, index) => (
          <Popover key={index} open={true} interactive={false} boundaryElement={ref as RefObject<HTMLElement>} showArrow={true}>
            <PopoverTrigger x={node.x} y={node.y} />
            <PopoverContent>
              <NodeDetails
                name={
                  node.selectedType === 'node'
                    ? (node as NodeType)?._id
                    : isGeoJsonType(node)
                      ? node.properties?.name
                      : node.selectedType === 'location'
                        ? (node as SearchResultType)?.name
                        : 'N/A'
                }
                colorHeader={
                  node.selectedType === 'node'
                    ? nodeColorHex(props?.settings?.nodelink?.nodes?.[node.label]?.color)
                    : node.selectedType === 'area'
                      ? rgbToHex(node.color[0], node.color[1], node.color[2])
                      : 'hsl(var(--clr-node))'
                }
              >
                <MapDataInspector type={node.selectedType} data={{ ...node }} key={node._id} />
              </NodeDetails>
            </PopoverContent>
          </Popover>
        ))}
      {searchResult && (
        <Popover open={true} boundaryElement={ref as RefObject<HTMLElement>} showArrow={true}>
          <PopoverTrigger x={searchResult.x} y={searchResult.y} />
          <PopoverContent>
            <NodeDetails name={searchResult.name} colorHeader="hsl(var(--clr-node))">
              <MapDataInspector type="location" data={{ ...searchResult }} key={searchResult.name} />
            </NodeDetails>
          </PopoverContent>
        </Popover>
      )}
      <Attribution />
    </div>
  );
});

const mapRef = React.createRef<{ exportImageInternal: () => void }>();

const MapComponent: VISComponentType<MapProps> = {
  component: React.forwardRef((props: VisualizationPropTypes<MapProps>, ref) => <MapVis {...props} ref={mapRef} />),
  settingsComponent: MapSettings,
  settings: settings,
  exportImage: () => {
    if (mapRef.current) {
      mapRef.current.exportImageInternal();
    } else {
      console.error('Map reference is not set.');
    }
  },
};
export default MapComponent;
