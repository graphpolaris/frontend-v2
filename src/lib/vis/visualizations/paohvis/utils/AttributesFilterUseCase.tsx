/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { FilterInfo, PaohvisFilters } from '../types';
import { AxisType, isNotInGroup } from './ResultNodeLinkParserUseCase';
import { boolPredicates, numberPredicates, textPredicates } from '../models/FilterPredicates';
import { cloneDeep } from 'lodash-es';
import { EdgeQueryResult, NodeQueryResult } from 'ts-common';

/** This is used to filter the data for Paohvis. */
export default class AttributeFilterUsecase {
  /** Applies all filters to the query result. */
  public static applyFilters(nodes: NodeQueryResult[], edges: EdgeQueryResult[], paohvisFilters: PaohvisFilters) {
    //apply the filters to the nodes
    let filteredNodes: NodeQueryResult[] = cloneDeep(nodes);

    //do replace
    paohvisFilters.nodeFilters.forEach(filter => (filteredNodes = this.filterAxis(filteredNodes, filter)));

    //filter out the unused edges
    const nodeIds = getIds(filteredNodes);
    let filteredEdges = filterUnusedEdges(nodeIds, edges);

    //apply the filters to the edges
    paohvisFilters.edgeFilters.forEach(filter => (filteredEdges = this.filterAxis(filteredEdges, filter)));

    //filter out unused nodes
    filteredNodes = filterUnusedNodes(filteredNodes, filteredEdges);

    return { nodes: filteredNodes, edges: filteredEdges };
  }

  /**
   * Gets the correct predicate that belongs to the predicateName and
   * filters the given array on the specified target group on the given attribute with the predicate and given value,
   * @param axisNodesOrEdges is the array that should be filtered.
   * @param filter is the filter that should be applied.
   * @returns the array filtered with the correct predicate.
   */
  public static filterAxis<T extends NodeQueryResult | EdgeQueryResult>(axisNodesOrEdges: T[], filter: FilterInfo): T[] {
    if (typeof filter.value == 'boolean') return filterBoolAttributes(axisNodesOrEdges, filter);

    if (typeof filter.value == 'number') return filterNumberAttributes(axisNodesOrEdges, filter);

    if (typeof filter.value == 'string') return filterTextAttributes(axisNodesOrEdges, filter);

    throw new Error('Filter on this type is not supported.');
  }
}

/** Filters the given array on the designated boolean attribute with the given predicate. */

function filterBoolAttributes<T extends NodeQueryResult | EdgeQueryResult>(axisNodesOrEdges: T[], filter: FilterInfo): T[] {
  const predicate = boolPredicates[filter.predicateName];
  if (predicate == undefined) throw new Error('Predicate does not exist');

  const resultNodesOrEdges = axisNodesOrEdges.filter(nodeOrEdge => {
    const currentAttribute = nodeOrEdge.attributes[filter.attributeName] as boolean;
    return isNotInGroup(nodeOrEdge, filter.targetGroup) || predicate(currentAttribute, filter.value);
  });

  return resultNodesOrEdges;
}

/** Filters the given array on the designated number attribute with the given predicate. */
function filterNumberAttributes<T extends NodeQueryResult | EdgeQueryResult>(axisNodesOrEdges: T[], filter: FilterInfo): T[] {
  const predicate = numberPredicates[filter.predicateName];
  if (predicate == undefined) throw new Error('Predicate does not exist');

  const resultNodesOrEdges = axisNodesOrEdges.filter(nodeOrEdge => {
    const currentAttribute = nodeOrEdge.attributes[filter.attributeName] as number;
    return isNotInGroup(nodeOrEdge, filter.targetGroup) || predicate(currentAttribute, filter.value);
  });

  return resultNodesOrEdges;
}

/** Filters the given array on the designated string attribute with the given predicate. */
function filterTextAttributes<T extends NodeQueryResult | EdgeQueryResult>(axisNodesOrEdges: T[], filter: FilterInfo): T[] {
  const predicate = textPredicates[filter.predicateName];
  if (predicate == undefined) throw new Error('Predicate does not exist');

  const resultNodesOrEdges = axisNodesOrEdges.filter(nodeOrEdge => {
    const currentAttribute = nodeOrEdge.attributes[filter.attributeName] as string;
    return isNotInGroup(nodeOrEdge, filter.targetGroup) || predicate(currentAttribute, filter.value);
  });

  return resultNodesOrEdges;
}

/** Gets the ids from the given nodes or edges array. */
export function getIds(nodesOrEdges: AxisType[]): string[] {
  const result: string[] = [];
  nodesOrEdges.forEach(nodeOrEdge => result.push(nodeOrEdge._id || 'unknown id'));
  return result;
}

export function getAttrListing(nodesOrEdges: AxisType[]): string[] {
  const result: string[] = [];
  nodesOrEdges.forEach(nodeOrEdge => result.push(nodeOrEdge._id || 'unknown id'));
  return result;
}

/**
 * Filters out the edges that are from or to a node that is not being used anymore.
 * @param ids are the ids of the nodes.
 * @param edges are the edges that should be filtered.
 * @returns a filtered array where all edges are used.
 */
export function filterUnusedEdges(ids: string[], edges: EdgeQueryResult[]): EdgeQueryResult[] {
  const filteredEdges: EdgeQueryResult[] = [];
  edges.forEach(edge => ids.includes(edge.from) && ids.includes(edge.to) && filteredEdges.push(edge));
  return filteredEdges;
}

/** Filters out the nodes that are not used by any edge. */
function filterUnusedNodes(nodes: NodeQueryResult[], edges: EdgeQueryResult[]): NodeQueryResult[] {
  const filteredNodes: NodeQueryResult[] = [];
  const frequencyDict: Record<string, number> = {};
  nodes.forEach(node => (frequencyDict[node._id] = 0));

  edges.forEach(edge => {
    frequencyDict[edge.from] += 1;
    frequencyDict[edge.to] += 1;
  });

  nodes.forEach(node => frequencyDict[node._id] > 0 && filteredNodes.push(node));

  return filteredNodes;
}
