import * as d3 from 'd3';

// Define the interface if not already defined

interface Data2RenderI {
  name: string;
  typeData: string;
  data: { category: string; count: number }[];
  numUniqueElements: number;
}

// Define the function to process each column
export const processDataColumn = (dataColumn: string, firstRowData: any, data: any[]): Data2RenderI => {
  const newData2Render: Data2RenderI = {
    name: dataColumn,
    typeData: firstRowData.type[dataColumn] || 'string',
    data: [],
    numUniqueElements: 0,
  };

  let categoryCounts;

  if (
    newData2Render.typeData === 'string' ||
    newData2Render.typeData === 'date' ||
    newData2Render.typeData === 'duration' ||
    newData2Render.typeData === 'datetime' ||
    newData2Render.typeData === 'time'
  ) {
    const groupedData = d3.group(data, d => d.attribute[dataColumn]);
    categoryCounts = Array.from(groupedData, ([category, items]) => ({
      category: category as string,
      count: items.length,
    }));

    newData2Render.numUniqueElements = categoryCounts.length;
    newData2Render.data = categoryCounts;
  } else if (newData2Render.typeData === 'bool') {
    const groupedData = d3.group(data, d => d.attribute[dataColumn]);

    categoryCounts = Array.from(groupedData, ([category, items]) => ({
      category: category as string,
      count: items.length,
    }));

    newData2Render.numUniqueElements = categoryCounts.length;
    newData2Render.data = categoryCounts;
  } else if (newData2Render.typeData === 'int' || newData2Render.typeData === 'float') {
    categoryCounts = data.map(obj => ({
      category: 'placeholder', // add something
      count: obj.attribute[dataColumn] as number, // fill values of data
    }));

    newData2Render.numUniqueElements = categoryCounts.length;
    newData2Render.data = categoryCounts;
  } else {
    // there is also array type, when considering labels
    const groupedData = d3.group(data, d => (d.attribute[dataColumn] as any)?.[0]);

    categoryCounts = Array.from(groupedData, ([category, items]) => ({
      category: category as string,
      count: items.length,
    }));

    newData2Render.numUniqueElements = categoryCounts.length;
  }

  return newData2Render;
};
