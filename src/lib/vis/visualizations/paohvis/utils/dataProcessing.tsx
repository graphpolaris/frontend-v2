import { PaohVisProps } from '../paohvis';
import { PaohvisData, HyperEdgeRange, HyperEdgeI } from '../types';
import AttributeFilterUsecase, { getIds } from './AttributesFilterUseCase';
import { countRepetition } from './utils';
import { Edge, GraphQueryResult, Node } from '@/lib/data-access';

type AugmentedNode = Node & {
  reduced_ids: string[];
};

function parseNodes(nodes: Node[]): string[] {
  const rowNodes = nodes;
  const rowLabels = getIds(rowNodes);

  //make dictionary for finding the index of a row
  const yNodesIndexDict: Record<string, number> = {};
  let yNodeIndexCounter = 0;
  for (let i = 0; i < rowNodes.length; i++) {
    yNodesIndexDict[rowNodes[i]._id] = yNodeIndexCounter;
    yNodeIndexCounter++;
  }

  return rowLabels;
}

function parseHyperEdgeRanges(
  nodesColumn: AugmentedNode[],
  nodesRow: AugmentedNode[],
  edges: Edge[],
  rowInfo: string[],
  nodeIdIndex: { [id: string]: number },
): [HyperEdgeRange[], { [key: string]: number }] {

  if (nodesColumn.length == 0 || nodesRow.length == 0) return [[], {}];

  const resultHyperEdgeRanges: HyperEdgeRange[] = nodesColumn.map(element => {
    const rangeText: any = +element._id;
    const _id = element._id;
    const degree = 0;
    const hyperEdges: HyperEdgeI = {
      indices: [],
    };

    return {
      rangeText,
      hyperEdges,
      _id,
      reduced_ids: element.reduced_ids,
      degree,
    };
  });

  const processedPairs = new Set<string>(); // To avoid processing pairs

  // loop through all edges - and creates hyperedge structure (column order/x)
  for (let i = 0; i < edges.length; i++) {
    const edge = edges[i];

    const pairString = `${edge.from}-${edge.to}`;
    if (!processedPairs.has(pairString)) {
      const hyperEdgeRange = resultHyperEdgeRanges.find(range => range._id === edge.to)?.hyperEdges;
      hyperEdgeRange?.indices.push(nodeIdIndex[edge.from]);
      processedPairs.add(pairString);
    }
  }

  // Sequential order in indices and count degree ( remove undefined = nodes without connection )
  resultHyperEdgeRanges.forEach(element => {
    element.degree = element.hyperEdges.indices.filter(num => {
      return typeof num === 'number';
    }).length;
  });

  resultHyperEdgeRanges.forEach(element => {
    element.hyperEdges.indices.sort((a, b) => a - b);
  });

  // Count nodes degree from structure
  const repetitionCounts: { [key: number]: number } = {};

  for (let indexX = 0; indexX < resultHyperEdgeRanges.length; indexX++) {
    countRepetition(resultHyperEdgeRanges[indexX].hyperEdges.indices, repetitionCounts);
  }

  const repetitionCountLabel: { [key: string]: number } = {};
  // get nodes ids
  rowInfo.forEach((arrayElement, index) => {
    repetitionCountLabel[arrayElement] = repetitionCounts[index] === undefined ? 0 : repetitionCounts[index];
  });

  resultHyperEdgeRanges.sort((a, b) => a.rangeText - b.rangeText);

  return [resultHyperEdgeRanges, repetitionCountLabel];
}

function processNodesSelectAndMerge(nodes: AugmentedNode[], selectedNodeLabel: string, skipReduce: boolean, attributeShow: string[]): [AugmentedNode[], Set<string>, Record<string, string>, Record<string, string[]>, Record<string, string[]>]{
  
  const replaceNodeIds: Record<string, string> = {};
  const reducedReplaceNodeIds: Record<string, string[]> = {};
  const hashAttributesToIdMap: Record<string, string[]> = {};

  const newNodes: AugmentedNode[] = nodes
    .filter(node => {
      // some dataset do not have label field
      let labelNode = '';
      if (node.label !== undefined) {
        labelNode = node.label;
      } else {
        const idParts = node._id.split('/');
        labelNode = idParts[0];
      }
      return labelNode === selectedNodeLabel;
    })
    .map(node => {
      const attributes = skipReduce
        ? node.attributes
        : Object.fromEntries(
            Object.keys(node.attributes)
              .filter(k => new Set(attributeShow).has(k))
              .map(k => [k, node.attributes[k]]),
          );

      // Build table of repetitions for aggregation step
      const hash = JSON.stringify(attributes);
      if (hash in hashAttributesToIdMap) {
        hashAttributesToIdMap[hash].push(node._id);
      } else {
        hashAttributesToIdMap[hash] = [node._id];
      }

      return { ...node, attributes: attributes, reduced_ids: [node._id] };
    });

    // Make id->id replace map
    Object.entries(hashAttributesToIdMap).forEach(([k, v]) => {
      if (v.length <= 1) return;

      v.slice(1).forEach(id => {
        replaceNodeIds[id] = v[0];
        if (!(v[0] in reducedReplaceNodeIds)) reducedReplaceNodeIds[v[0]] = [id];
        else reducedReplaceNodeIds[v[0]].push(id);
      });
    });

    const nodesIds: Set<string> = new Set(newNodes.map(node => node._id));

    return [newNodes, nodesIds, replaceNodeIds, reducedReplaceNodeIds, hashAttributesToIdMap]

}

export function parseQueryResult(
  queryResult: GraphQueryResult,
  configuration: PaohVisProps,
  relationTo: string,
  mergeData: boolean,
): PaohvisData {
  
  const skipReduceRow = !mergeData;
  const skipReduceColumn = !mergeData;
  const nodesHaveSameLabel = configuration.rowNode === configuration.columnNode;

  const paohvisFilters = { nodeFilters: [], edgeFilters: [] }; // fill it with something
  const filteredData = AttributeFilterUsecase.applyFilters(queryResult.nodes, queryResult.edges, paohvisFilters);

  const resultRow = processNodesSelectAndMerge(filteredData.nodes, configuration.rowNode, skipReduceRow, configuration.attributeRowShow)
  const resultColumn = processNodesSelectAndMerge(filteredData.nodes, configuration.columnNode, skipReduceColumn, configuration.attributeColumnShow)

  let nodesRow = resultRow[0]
  const [rowNodesIds, replaceRowNodeIds, reducedReplaceRowNodeIds] = resultRow.slice(1,4) as [Set<string>, Record<string, string>, Record<string, string[]>]
  let nodesColumn = resultColumn[0]
  const [columnNodesIds, replaceColumnNodeIds, reducedReplaceColumnNodeIds] = resultColumn.slice(1,4) as [Set<string>, Record<string, string>, Record<string, string[]>]

  if (mergeData) {
    const toRemoveIdsRow = new Set(Object.keys(replaceRowNodeIds));
    nodesRow = nodesRow
      .filter(node => !toRemoveIdsRow.has(node._id))
      .map(node => ({ ...node, reduced_ids: [node._id, ...(reducedReplaceRowNodeIds?.[node._id] || [])] }));

    const toRemoveIdsColumn = new Set(Object.keys(replaceColumnNodeIds));
    nodesColumn = nodesColumn
      .filter(node => !toRemoveIdsColumn.has(node._id))
      .map(node => ({ ...node, reduced_ids: [node._id, ...(reducedReplaceColumnNodeIds?.[node._id] || [])] }));
  }

  const augmentedNodes = [...nodesRow, ...nodesColumn];
  const augmentedEdges: Edge[] = [];
  
  filteredData.edges.forEach(edge => {

    if(nodesHaveSameLabel) {
      const from1 = edge.from
      const to1 = edge.to
      const from2 = edge.to
      const to2 = edge.from

      augmentedEdges.push({
        _id: edge._id,
        from: replaceRowNodeIds[from1] || from1,
        to: replaceColumnNodeIds[to1] || to1,
        label: edge.label,
        attributes: edge.attributes,
      })

      augmentedEdges.push({
        _id: edge._id + "_2",
        from: replaceRowNodeIds[from2] || from2,
        to: replaceColumnNodeIds[to2] || to2,
        label: edge.label + "_2",
        attributes: edge.attributes,
      })

    }else{
      const from = rowNodesIds.has(edge.from) ? edge.from : edge.to; // rows
      const to = columnNodesIds.has(edge.to) ? edge.to : edge.from; // columns

      augmentedEdges.push({
        _id: edge._id,
        from: replaceRowNodeIds[from] || from,
        to: replaceColumnNodeIds[to] || to,
        label: edge.label,
        attributes: edge.attributes,
      })
    }
    
  });

  const nodeIdIndex: { [id: string]: number } = nodesRow.reduce((acc, node, index) => ({ ...acc, [node._id]: index }), {});


  //parse nodes
  const rowInfo: string[] = parseNodes(nodesRow);

  let nodeListAttr: any[] = [];
  nodeListAttr = nodesRow.map(node => node._id);

  const [resultHyperEdgeRanges, rowsDegree] = parseHyperEdgeRanges(
    nodesColumn,
    nodesRow,
    augmentedEdges,
    rowInfo,
    nodeIdIndex,
  );

  return {
    rowLabels: nodeListAttr,
    hyperEdgeRanges: resultHyperEdgeRanges,
    rowDegrees: rowsDegree,
    nodes: augmentedNodes,
    edges: augmentedEdges,
  };
}
