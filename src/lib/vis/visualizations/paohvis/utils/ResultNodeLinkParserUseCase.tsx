/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import { EdgeQueryResult, NodeQueryResult } from 'ts-common';
import { GraphQueryResult } from '../../../../data-access/store';

export type AxisType = NodeQueryResult | EdgeQueryResult;

/** Gets the group to which the node/edge belongs */
export function getGroupName(axisType: AxisType): string {
  // FIXME: only works in arangodb
  return axisType.label;
}

/** Returns true if the given id belongs to the target group. */
export function isNotInGroup(nodeOrEdge: AxisType, targetGroup: string): boolean {
  return getGroupName(nodeOrEdge) != targetGroup;
}

/** Checks if a query result form the backend contains valid NodeLinkResultType data.
 * @param {any} jsonObject The query result object received from the frontend.
 * @returns True and the jsonObject will be casted, false if the jsonObject did not contain all the data fields.
 */
export function isNodeLinkResult(jsonObject: any): jsonObject is GraphQueryResult {
  if (typeof jsonObject === 'object' && jsonObject !== null && 'nodes' in jsonObject && 'edges' in jsonObject) {
    if (!Array.isArray(jsonObject.nodes) || !Array.isArray(jsonObject.edges)) return false;

    const validNodes = jsonObject.nodes.every((node: any) => 'id' in node && 'attributes' in node);
    const validEdges = jsonObject.edges.every((edge: any) => 'from' in edge && 'to' in edge);

    return validNodes && validEdges;
  } else return false;
}

/** Returns a record with a type of the nodes as key and a number that represents how many times this type is present in the nodeLinkResult as value. */
export function getNodeTypes(nodeLinkResult: GraphQueryResult): Record<string, number> {
  const types: Record<string, number> = {};

  nodeLinkResult.nodes.forEach(node => {
    const type = getGroupName(node);
    if (types[type] != undefined) types[type]++;
    else types[type] = 0;
  });

  return types;
}

export type UniqueEdge = {
  from: string;
  to: string;
  count: number;
  attributes: Record<string, any>;
};

export class ParseToUniqueEdges {
  /**
   * Parse a message (containing query result) edges to unique edges.
   * @param {Link[]} queryResultEdges Edges from a query result.
   * @param {boolean} isLinkPredictionData True if parsing LinkPredictionData, false otherwise.
   * @returns {UniqueEdge[]} Unique edges with a count property added.
   */
  public static parse(queryResultEdges: EdgeQueryResult[], isLinkPredictionData: boolean): UniqueEdge[] {
    // Edges to be returned
    const edges: UniqueEdge[] = [];

    // Collect the edges in map, to only keep unique edges
    // And count the number of same edges
    const edgesMap = new Map<string, number>();
    const attriMap = new Map<string, Record<string, any>>();
    if (queryResultEdges != null) {
      if (!isLinkPredictionData) {
        for (let j = 0; j < queryResultEdges.length; j++) {
          const newLink = queryResultEdges[j].from + ':' + queryResultEdges[j].to;
          edgesMap.set(newLink, (edgesMap.get(newLink) || 0) + 1);
          attriMap.set(newLink, queryResultEdges[j].attributes);
        }

        edgesMap.forEach((count, key) => {
          const fromTo = key.split(':');
          edges.push({
            from: fromTo[0],
            to: fromTo[1],
            count: count,
            attributes: attriMap.get(key) ?? [],
          });
        });
      } else {
        for (let i = 0; i < queryResultEdges.length; i++) {
          edges.push({
            from: queryResultEdges[i].from,
            to: queryResultEdges[i].to,
            count: queryResultEdges[i].attributes.jaccard_coefficient as number,
            attributes: queryResultEdges[i].attributes,
          });
        }
      }
    }
    return edges;
  }
}
