import { SchemaAttribute, SchemaGraph } from '../../../../schema';
import {
  AttributeNames,
  EntitiesFromSchema,
  RelationsFromSchema,
  ConnectionFromTo,
  IdConnections,
  GraphData,
  RowInformation,
  Data2RenderI,
} from '../types';
import { MultiGraph } from 'graphology';
import { NodeQueryResult } from '@/lib/data-access/store/graphQueryResultSlice';
import { group } from 'd3';
import { Sizes, sizesArray } from '@/lib/components/icon';

/**
 * Takes a schema result and calculates all the entity names, and relation names and attribute names per entity.
 * Used by PAOHvis to show all possible options to choose from when adding a new PAOHvis visualization or when filtering.
 * @param {SchemaResultType} schemaResult A new schema result from the backend.
 * @returns {EntitiesFromSchema} All entity names, and relation names and attribute names per entity.
 */
export function calculateAttributesAndRelations(schemaResult: SchemaGraph): EntitiesFromSchema {
  const attributesPerEntity: Record<string, AttributeNames> = calculateAttributes(schemaResult);
  const relationsPerEntity: Record<string, string[]> = calculateRelations(schemaResult);

  return {
    entityNames: schemaResult.nodes.map(node => node?.attributes?.name || 'ERROR'),
    attributesPerEntity,
    relationsPerEntity,
  };
}

/**
 * Takes a schema result and calculates all the attribute names per entity.
 * @param {SchemaResultType} schemaResult A new schema result from the backend.
 * @returns {Record<string, AttributeNames>} All attribute names per entity.
 */
export function calculateAttributes(schemaResult: SchemaGraph): Record<string, AttributeNames> {
  const attributesPerEntity: Record<string, AttributeNames> = {};
  // Go through each entity.
  schemaResult.nodes.forEach(node => {
    if (node?.attributes?.name === undefined) {
      return;
    }

    // Extract the attribute names per datatype for each entity.
    const textAttributeNames: string[] = [];
    const boolAttributeNames: string[] = [];
    const numberAttributeNames: string[] = [];

    node.attributes.attributes.forEach(attr => {
      if (attr.type == 'string') textAttributeNames.push(attr.name);
      else if (attr.type == 'int' || attr.type == 'float') numberAttributeNames.push(attr.name);
      else boolAttributeNames.push(attr.name);
    });

    // Create a new object with the arrays with attribute names per datatype.
    attributesPerEntity[node.attributes.name] = {
      textAttributeNames,
      boolAttributeNames,
      numberAttributeNames,
    };
  });
  return attributesPerEntity;
}

/**
 * Takes a schema result and calculates all the relation names per entity.
 * @param {SchemaResultType} schemaResult A new schema result from the backend.
 * @returns {Record<string, AttributeNames>} All relation (from and to) names per entity.
 */
export function calculateRelations(schemaResult: SchemaGraph): Record<string, string[]> {
  const relationsPerEntity: Record<string, string[]> = {};
  schemaResult.edges.forEach(edge => {
    if (edge?.attributes === undefined) {
      return;
    }

    // Extract the from-node-name (collection name) from every relation.
    if (relationsPerEntity[edge.attributes.from]) relationsPerEntity[edge.attributes.from].push(edge.attributes.collection);
    else relationsPerEntity[edge.attributes.from] = [edge.attributes.collection];
    // Extract the to-node-name (collection name) from every relation.
    if (relationsPerEntity[edge.attributes.to]) relationsPerEntity[edge.attributes.to].push(edge.attributes.collection);
    else relationsPerEntity[edge.attributes.to] = [edge.attributes.collection];
  });
  return relationsPerEntity;
}

/**
 * Takes a schema result and calculates all the relation collection names, and relation names and attribute names per relation.
 * Used by PAOHvis to show all possible options to choose from when adding a new PAOHvis visualization or when filtering.
 * @param {SchemaResultType} schemaResult A new schema result from the backend.
 * @returns {EntitiesFromSchema} All entity names, and relation names and attribute names per entity.
 */
export function calculateAttributesFromRelation(schemaResult: SchemaGraph): RelationsFromSchema {
  const relationCollections: string[] = [];
  const attributesPerRelation: Record<string, AttributeNames> = {};
  const nameOfCollectionPerRelation: Record<string, string> = {};
  // Go through each relation.
  schemaResult.edges.forEach(edge => {
    if (edge?.attributes === undefined) {
      return;
    }

    if (!nameOfCollectionPerRelation[edge.attributes.collection]) {
      relationCollections.push(edge.attributes.collection);
      nameOfCollectionPerRelation[edge.attributes.collection] = `${edge.attributes.from}:${edge.attributes.to}:${edge.attributes.name}`;
    }
    // Extract the attribute names per datatype for each relation.
    const textAttributeNames: string[] = [];
    const boolAttributeNames: string[] = [];
    const numberAttributeNames: string[] = [];
    /*
    edge.attributes.attributes.forEach((attr: SchemaAttribute) => {
      if (attr.type == 'string') textAttributeNames.push(attr.name);
      else if (attr.type == 'int' || attr.type == 'float') numberAttributeNames.push(attr.name);
      else boolAttributeNames.push(attr.name);
    });
    */
    // Create a new object with the arrays with attribute names per datatype.
    attributesPerRelation[edge.attributes.collection] = {
      textAttributeNames,
      boolAttributeNames,
      numberAttributeNames,
    };
  });
  return {
    relationCollection: relationCollections,
    relationNames: nameOfCollectionPerRelation,
    attributesPerRelation: attributesPerRelation,
  };
}

/**
 * Filters duplicate elements from array with a hashtable.
 * From https://stackoverflow.com/questions/9229645/remove-duplicate-values-from-js-array */
export function uniq(element: number[]) {
  const seen: Record<number, boolean> = {};
  return element.filter(function (item) {
    return Object.prototype.hasOwnProperty.call(seen, item) ? false : (seen[item] = true);
  });
}

export function getUniqueArrays(arrays: number[][]): { uniqueArrays: number[][]; indicesUnique: number[] } {
  const uniqueArrays: number[][] = [];
  const uniqueArrayStrings: Set<string> = new Set();
  const indicesUnique: number[] = [];

  for (let i = 0; i < arrays.length; i++) {
    const array = arrays[i];
    const arrayString = array.join(',');
    if (!uniqueArrayStrings.has(arrayString)) {
      uniqueArrayStrings.add(arrayString);
      uniqueArrays.push(array);
      indicesUnique.push(i);
    }
  }

  return { uniqueArrays, indicesUnique };
}

export function findConnectionsNodes(
  queryIDs: string[],
  originIDs: string[],
  graphStructure: MultiGraph,
  labelNode: string,
  edges: NodeQueryResult[],
): ConnectionFromTo[] {
  const neighborMap: IdConnections = {};
  const neighborMapNo: IdConnections = {};

  const tempSetAttributes: any = [];
  const tempSetAttributesSs: any = [];
  originIDs.forEach(nodeId => {
    const tempSet: Set<string> = new Set();
    const tempSetNo: any = [];
    graphStructure.forEachNeighbor(nodeId, (neighbor, attributes) => {
      if (attributes.label != labelNode) {
        graphStructure.forEachNeighbor(neighbor, (neighbor2, attributes2) => {
          if (queryIDs.includes(neighbor2) && neighbor2 !== nodeId) {
            if (!tempSet.has(neighbor2)) {
              tempSet.add(neighbor2);
              tempSetNo.push(neighbor2);
              tempSetAttributesSs.push({ from: nodeId, to: neighbor2, attributes });
              tempSetAttributes.push(attributes);
            }
          }
        });
      } else {
        if (queryIDs.includes(neighbor) && neighbor !== nodeId) {
          tempSet.add(neighbor);
          tempSetNo.push(neighbor);
          tempSetAttributes.push(attributes);
          tempSetAttributesSs.push({ from: nodeId, to: neighbor, attributes });
        }
      }
    });
    neighborMap[nodeId] = Array.from(tempSet);
    neighborMapNo[nodeId] = tempSetNo;
  });

  return tempSetAttributesSs;
}

export function calcTextWidthAndStringText2(
  textArray: string | string[],
  maxLengthAllowed: number,
  tailwindStyle: string,
): [string[], number] {
  const labels = Array.isArray(textArray) ? textArray : [textArray];

  //const truncatedLabels: { truncatedText: string; truncatedWidth: number }[] = [];
  const truncatedLabels: string[] = [];
  let maxWidth = 0.0;

  labels.forEach((rowLabel, index) => {
    let truncatedText = rowLabel;
    let truncatedWidth = 0;
    while (truncatedText.length > 0) {
      const tempElement = document.createElement('span');
      tempElement.style.position = 'absolute';
      tempElement.style.visibility = 'hidden';
      tempElement.style.pointerEvents = 'none';
      tempElement.style.whiteSpace = 'nowrap';
      tempElement.style.font = getComputedStyle(document.body).font;

      tempElement.className = tailwindStyle;

      tempElement.textContent = truncatedText + '...';
      document.body.appendChild(tempElement);
      truncatedWidth = tempElement.getBoundingClientRect().width;
      document.body.removeChild(tempElement);

      if (truncatedWidth <= maxLengthAllowed) {
        break;
      }
      truncatedText = truncatedText.slice(0, -1);
    }

    if (truncatedWidth > maxWidth) {
      maxWidth = truncatedWidth;
    }

    if (truncatedText !== rowLabel) {
      truncatedLabels.push(truncatedText + '...');
    } else {
      truncatedLabels.push(truncatedText);
    }
  });

  //return truncatedLabels;
  return [truncatedLabels, maxWidth];
}

export function calcTextWidthAndStringText(
  textArray: string | string[],
  maxLengthAllowed: number,
  tailwindStyle: string,
): [string[], number] {
  const labels = Array.isArray(textArray) ? textArray : [textArray];

  //const truncatedLabels: { truncatedText: string; truncatedWidth: number }[] = [];
  const truncatedLabels: string[] = [];
  let maxWidth = 0.0;

  const tempElement = document.createElement('span');
  tempElement.style.position = 'absolute';
  tempElement.style.visibility = 'hidden';
  tempElement.style.pointerEvents = 'none';
  tempElement.style.whiteSpace = 'nowrap';
  tempElement.className = tailwindStyle;
  tempElement.style.font = getComputedStyle(document.body).font;

  labels.forEach((rowLabel, index) => {
    let truncatedText = rowLabel;
    let truncatedWidth = 0;
    while (truncatedText.length > 0) {
      /*
      const tempElement = document.createElement('span');
      tempElement.style.position = 'absolute';
      tempElement.style.visibility = 'hidden';
      tempElement.style.pointerEvents = 'none';
      tempElement.style.whiteSpace = 'nowrap';
      tempElement.style.font = getComputedStyle(document.body).font;

      tempElement.className = tailwindStyle;
      */
      tempElement.textContent = truncatedText + '...';
      document.body.appendChild(tempElement);
      truncatedWidth = tempElement.getBoundingClientRect().width;
      document.body.removeChild(tempElement);

      if (truncatedWidth <= maxLengthAllowed) {
        break;
      }
      truncatedText = truncatedText.slice(0, -1);
    }

    if (truncatedWidth > maxWidth) {
      maxWidth = truncatedWidth;
    }

    if (truncatedText !== rowLabel) {
      truncatedLabels.push(truncatedText + '...');
    } else {
      truncatedLabels.push(truncatedText);
    }
  });

  //return truncatedLabels;
  return [truncatedLabels, maxWidth];
}

export function wrapperForEdge(data: IdConnections, attributes: any): ConnectionFromTo[] {
  const keysData = Object.keys(data);
  const resultsDas: ConnectionFromTo[] = [];
  const results = keysData.forEach((item, index) => {
    const r = data[item].forEach(itemConnected => {
      resultsDas.push({ from: item, to: itemConnected, attributes: attributes[index] });
    });
  });
  return resultsDas;
}

export const buildGraphology = (data: GraphData): MultiGraph => {
  const graph = new MultiGraph();

  const nodeMap = new Map<string, NodeQueryResult>();
  data.nodes.forEach(node => {
    nodeMap.set(node._id, node);
  });

  const nodeEntries = data.nodes.map(node => ({
    key: node._id,
    attributes: {
      ...node.attributes,
      label: node.label,
    },
  }));

  graph.import({ nodes: nodeEntries });

  const validEdgeEntries = data.edges
    .filter((edge: any) => nodeMap.has(edge.from) && nodeMap.has(edge.to))
    .map((edge: any) => ({
      source: edge.from,
      target: edge.to,
    }));

  graph.import({ edges: validEdgeEntries });

  return graph;
};

export function sortByIndicesLength(
  data: { hyperEdges: { frequencies: number[]; indices: number[] }[]; nameToShow: string; rangeText: string }[],
  order: 'ascending' | 'descending',
): { hyperEdges: { frequencies: number[]; indices: number[] }[]; nameToShow: string; rangeText: string }[] {
  return data.sort((a, b) => {
    const lengthA = a.hyperEdges[0].indices.length;
    const lengthB = b.hyperEdges[0].indices.length;
    if (order === 'ascending') {
      return lengthA - lengthB;
    } else {
      return lengthB - lengthA;
    }
  });
}

export function sortByIndicesLength2(
  data: { hyperEdges: { indices: number[] }; _id: string; rangeText: any; degree: number }[],
  order: 'ascending' | 'descending',
): { hyperEdges: { indices: number[] }; _id: string; rangeText: any; degree: number }[] {
  return data.sort((a, b) => {
    const lengthA = a.hyperEdges.indices.length;
    const lengthB = b.hyperEdges.indices.length;
    if (order === 'ascending') {
      return lengthA - lengthB;
    } else {
      return lengthB - lengthA;
    }
  });
}

export function sortByRangeText2(
  data: { hyperEdges: { indices: number[] }; _id: string; rangeText: any; degree: number }[],
  order: 'ascending' | 'descending',
): { hyperEdges: { indices: number[] }; _id: string; rangeText: any; degree: number }[] {
  return data.sort((a, b) => {
    const rangeTextA = a.rangeText.toLowerCase(); // Convert to lowercase for case-insensitive sorting
    const rangeTextB = b.rangeText.toLowerCase();

    // Compare rangeText values based on the sorting order
    if (order === 'ascending') {
      return rangeTextA.localeCompare(rangeTextB); // Sort in ascending order
    } else {
      return rangeTextB.localeCompare(rangeTextA); // Sort in descending order
    }
  });
}

export function countRepetition(array: number[], counts: { [key: number]: number }) {
  // Count repetition of each element in the array
  array.forEach(element => {
    if (counts[element]) {
      counts[element]++;
    } else {
      counts[element] = 1;
    }
  });
}

export function sortObjectsByDegree(
  objects: { name: string; degree: number; name2Show: string }[],
  order: 'ascending' | 'descending' = 'ascending',
): { name: string; degree: number; name2Show: string }[] {
  // Create a copy of the objects array with original indices
  const objectsWithIndices = objects.map((obj, index) => ({ ...obj, originalIndex: index }));
  const orderAlphabetical = 'ascending';
  // Sort the objects array based on degree and name
  const sortedObjects = objectsWithIndices.slice().sort((a, b) => {
    if (a.degree !== b.degree) {
      if (order === 'ascending') {
        return a.degree - b.degree;
      } else {
        return b.degree - a.degree;
      }
    } else {
      // If degrees are equal, sort by name
      if (orderAlphabetical === 'ascending') {
        return a.name.localeCompare(b.name);
      } else {
        return b.name.localeCompare(a.name);
      }
    }
  });

  // Construct the index array based on the original indices
  const indexArray = sortedObjects.map(obj => obj.originalIndex);

  return sortedObjects.map(obj => ({ name: obj.name, degree: obj.degree, name2Show: obj.name2Show }));
}

export function getTransformIndices(array1: { name: string; degree: number }[], array2: { name: string; degree: number }[]): number[] {
  const indices: number[] = [];

  for (let i = 0; i < array1.length; i++) {
    const obj1 = array1[i];
    const index = array2.findIndex(obj => obj.name === obj1.name);
    if (index !== -1) {
      indices.push(index);
    }
  }

  return indices;
}

export function sortByRangeText(
  data: { hyperEdges: { frequencies: number[]; indices: number[] }[]; nameToShow: string; rangeText: string }[],
  order: 'ascending' | 'descending',
): { hyperEdges: { frequencies: number[]; indices: number[] }[]; nameToShow: string; rangeText: string }[] {
  return data.sort((a, b) => {
    const rangeTextA = a.rangeText.toLowerCase(); // Convert to lowercase for case-insensitive sorting
    const rangeTextB = b.rangeText.toLowerCase();

    // Compare rangeText values based on the sorting order
    if (order === 'ascending') {
      return rangeTextA.localeCompare(rangeTextB); // Sort in ascending order
    } else {
      return rangeTextB.localeCompare(rangeTextA); // Sort in descending order
    }
  });
}

export function sortObjectsByName2Show2(
  objects: { name: string; degree: number; name2Show: string }[],
  order: 'ascending' | 'descending' = 'ascending',
): { name: string; degree: number; name2Show: string }[] {
  return objects.slice().sort((a, b) => {
    // Compare name2Show alphabetically
    if (order === 'ascending') {
      return a.name2Show.localeCompare(b.name2Show);
    } else {
      return b.name2Show.localeCompare(a.name2Show);
    }
  });
}

export function sortObjectsByName2Show(
  objects: { name: string; degree: number; name2Show: string }[],
  order: 'ascending' | 'descending' = 'ascending',
): { name: string; degree: number; name2Show: string }[] {
  return objects.slice().sort((a, b) => {
    const numA = parseInt(a.name2Show.split('/')[1]);
    const numB = parseInt(b.name2Show.split('/')[1]);

    // If both ids contain numbers, compare numerically
    if (!isNaN(numA) && !isNaN(numB)) {
      if (order === 'ascending') {
        return numA - numB;
      } else {
        return numB - numA;
      }
    } else {
      // If one or both don't contain numbers, compare alphabetically
      if (order === 'ascending') {
        return a.name2Show.localeCompare(b.name2Show);
      } else {
        return b.name2Show.localeCompare(a.name2Show);
      }
    }
  });
}

export function areMoreElements(array: number[], indices: number[], direction: string): boolean {
  if (direction === 'below') {
    return indices.filter(index => index > array[1]).length !== 0;
  } else {
    return indices.filter(index => index < array[0]).length !== 0;
  }
}

export function intersectionElements(array: number[], indices: number[]): { y0: number; y1: number; valid: boolean } {
  /*
  check where are the indices of each hyperedege respect to pagination indices range:
  caseA: both points inside
  caseB: one more below other in 
  caseD: one more above other in 
  caseE: both out range
  */
  const moreAbove = areMoreElements(array, indices, 'above');
  const moreBelow = areMoreElements(array, indices, 'below');
  const pointsInside = indices.filter(index => index >= array[0] && index < array[1]);
  if (moreAbove == false && moreBelow == false) {
    if (pointsInside.length == 0 || pointsInside.length == 1) {
      return { y0: 0, y1: 0, valid: false };
    } else {
      return { y0: pointsInside[0], y1: pointsInside[pointsInside.length - 1], valid: true };
    }
  } else if (moreAbove == false && moreBelow == true) {
    if (pointsInside.length == 0) {
      return { y0: 0, y1: 0, valid: false };
    } else {
      return { y0: pointsInside[0], y1: array[1] - 0.5, valid: true };
    }
  } else if (moreAbove == true && moreBelow == false) {
    if (pointsInside.length == 0) {
      return { y0: 0, y1: 0, valid: false };
    } else {
      return { y0: array[0] - 0.5, y1: pointsInside[pointsInside.length - 1], valid: true };
    }
  } else if (moreAbove == true && moreBelow == true) {
    return { y0: array[0] - 0.5, y1: array[1] - 0.5, valid: true };
  }
  // to return something
  return { y0: 0, y1: 0, valid: false };
}

export const sortIndices = (rowInformation: RowInformation, headerName: string, sortingOrder: 'asc' | 'desc'): number[] => {
  // Find the index corresponding to the given header

  const index = rowInformation.findIndex(row => row.header === headerName);

  if (index === -1) {
    // Header not found, return an empty array
    return [];
  }

  return [...rowInformation[index].data.keys()].sort((a, b) => {
    const aValue = rowInformation[index].data[a];
    const bValue = rowInformation[index].data[b];
    if (typeof aValue === 'string' && typeof bValue === 'string') {
      return sortingOrder === 'asc' ? aValue.localeCompare(bValue) : bValue.localeCompare(aValue);
    } else {
      const numA = Number(aValue);
      const numB = Number(bValue);
      if (!isNaN(numA) && !isNaN(numB)) {
        return sortingOrder === 'asc' ? numA - numB : numB - numA;
      }
      return 0;
    }
  });
};

export const sortRowInformation = (rowInformation: RowInformation, sortedIndices: number[]): RowInformation => {
  return rowInformation.map(row => {
    // Sort the data array based on sortedIndices
    const sortedData = sortedIndices.map(index => row.data[index]);
    return { ...row, data: sortedData };
  });
};

export function findTypeByName(attributes: SchemaAttribute[], queryName: string): string | undefined {
  for (const attribute of attributes) {
    if (attribute.name === queryName) {
      return attribute.type;
    }
  }
  return undefined; // Return undefined if no match is found
}

export const processDataColumn = (dataColumn: string, firstRowData: any, data: any[]): Data2RenderI => {
  const newData2Render: Data2RenderI = {
    name: dataColumn,
    typeData: firstRowData.type[dataColumn] || 'string',
    data: [],
    numUniqueElements: 0,
  };

  let categoryCounts;

  if (
    newData2Render.typeData === 'string' ||
    newData2Render.typeData === 'date' ||
    newData2Render.typeData === 'duration' ||
    newData2Render.typeData === 'datetime' ||
    newData2Render.typeData === 'time'
  ) {
    const groupedData = group(data, d => d.attribute[dataColumn]);
    categoryCounts = Array.from(groupedData, ([category, items]) => ({
      category: category as string,
      count: items.length,
    }));

    newData2Render.numUniqueElements = categoryCounts.length;
    newData2Render.data = categoryCounts;
  } else if (newData2Render.typeData === 'bool') {
    const groupedData = group(data, d => d.attribute[dataColumn]);

    categoryCounts = Array.from(groupedData, ([category, items]) => ({
      category: category as string,
      count: items.length,
    }));

    newData2Render.numUniqueElements = categoryCounts.length;
    newData2Render.data = categoryCounts;
  } else if (newData2Render.typeData === 'int' || newData2Render.typeData === 'float') {
    categoryCounts = data.map(obj => ({
      category: 'placeholder', // add something
      count: obj.attribute[dataColumn] as number, // fill values of data
    }));

    newData2Render.numUniqueElements = categoryCounts.length;
    newData2Render.data = categoryCounts;
  } else {
    // there is also array type, when considering labels
    const groupedData = group(data, d => (d.attribute[dataColumn] as any)?.[0]);

    categoryCounts = Array.from(groupedData, ([category, items]) => ({
      category: category as string,
      count: items.length,
    }));

    newData2Render.numUniqueElements = categoryCounts.length;
  }

  return newData2Render;
};
export function getClosestSize(rowHeight: number): Sizes {
  return sizesArray.reduce((prev, curr) => (Math.abs(curr - rowHeight) < Math.abs(prev - rowHeight) ? curr : prev));
}
