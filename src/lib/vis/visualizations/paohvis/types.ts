/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */
import { EdgeQueryResult, NodeQueryResult, NodeAttributes } from '@/lib/data-access/store/graphQueryResultSlice';

/** The data that is needed to make the complete Paohvis table. */
export type PaohvisData = {
  rowLabels: string[];
  hyperEdgeRanges: HyperEdgeRange[];
  rowDegrees: { [key: string]: number };
  nodes: NodeQueryResult[];
  edges: EdgeQueryResult[];

  //maxRowLabelWidth: number;
};
export type PaohvisDataPaginated = {
  pageData: PaohvisData;
  data: PaohvisData;
  originalData: PaohvisData;
  //maxRowLabelWidth: number;
};

export type RowInformation = {
  width: number;
  header: string;
  data: any[];
}[];

export type LinesHyperEdges = {
  y0: number;
  y1: number;
  valid: boolean;
};

/** The ranges of the hyperEdges consisting of the name of the range with all adequate hyperEdges. */
export type HyperEdgeI = {
  indices: number[];
};

export type HyperEdgeRange = {
  rangeText: number;
  hyperEdges: HyperEdgeI;
  _id: string;
  reduced_ids: string[];
  degree: number;
};

/** All information for the axes. This is used in the parser */
export type PaohvisAxisInfo = {
  selectedAttribute: Attribute;
  relation: Relation;
  isYAxisEntityEqualToRelationFrom: boolean;
};

/** The type of an Attribute. This is used to make the HyperEdgeRanges in the parser. */
export type Attribute = {
  name: string;
  type: ValueType;
  origin: AttributeOrigin;
};

/** The type of the value of an attribute. This is primarily used to make the HyperEdgeRanges in the parser. */
export enum ValueType {
  text = 'text',
  bool = 'bool',
  number = 'number',
  noAttribute = 'No attribute',
}

/** Attributes can come from entities or relations. This is primarily used to make the HyperEdgeRanges in the parser. */
export enum AttributeOrigin {
  relation = 'Relation',
  entity = 'Entity',
  noAttribute = 'No attribute',
}

/** The type of a relation.  This is primarily used to make the HyperEdges in the parser. */
export type Relation = {
  collection: string;
  from: string;
  to: string;
};

/** All information from the nodes. This is used in the parser. */
export type PaohvisNodeInfo = {
  // The rowlabels (ids from the nodes on the y-axis).
  rowLabels: string[];
  // Dictionary for finding the index of a row.
  yNodesIndexDict: Record<string, number>;
  // Dictionary for finding the attributes that belong to the nodes on the x-axis.
  xNodesAttributesDict: Record<string, any>;
};

/** The entities with names and attribute parameters from the schema. */
export type EntitiesFromSchema = {
  entityNames: string[];
  attributesPerEntity: Record<string, AttributeNames>;
  relationsPerEntity: Record<string, string[]>;
};

/** The relations with (collection-)names and attribute parameters from the schema. */
export type RelationsFromSchema = {
  relationCollection: string[];
  relationNames: Record<string, string>;
  attributesPerRelation: Record<string, AttributeNames>;
};

/** The names of attributes per datatype. */
export type AttributeNames = {
  textAttributeNames: string[];
  numberAttributeNames: string[];
  boolAttributeNames: string[];
};

/** The options to order the nodes. */
export enum NodeOrder {
  alphabetical = 'Alphabetical',
  degree = 'Degree (amount of hyperedges)',
  byGroup = 'By Group',
}

/** All information on the ordering of PAOHvis. */
export type PaohvisNodeOrder = {
  orderBy: NodeOrder;
  isReverseOrder: boolean;
};

/** All PAOHvis filters grouped by the filters on nodes and edges. */
export type PaohvisFilters = {
  nodeFilters: FilterInfo[];
  edgeFilters: FilterInfo[];
};

/** The information of one filter on PAOHvis. */
export type FilterInfo = {
  targetGroup: string;
  attributeName: string;
  value: any;
  predicateName: string;
};

/** The options where you can filter on. */
export enum FilterType {
  xaxis = 'X-axis',
  yaxis = 'Y-axis',
  edge = 'Edges',
}

/** Entities can come from the 'from' or the 'to' part of a relation. */
export enum EntityOrigin {
  from = 'From',
  to = 'To',
}

export interface AugmentedEdgeAttributes {
  attributes: NodeAttributes;
  from: string;
  to: string;
  id: string;
  label: string;
}

export interface GraphData {
  name?: string;
  nodes: NodeQueryResult[];
  edges: NodeQueryResult[];
}

export interface ConnectionFromTo {
  to: string;
  from: string;
  attributes: NodeAttributes;
}
export interface IdConnections {
  [key: string]: string[];
}

export interface Data2RenderI {
  name: string;
  typeData: string;
  data: { category: string; count: number }[];
  numUniqueElements: number;
}
