import { Meta } from '@storybook/react';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import {
  big2ndChamberSchemaRaw,
  marieBoucherSampleSchemaRaw,
  simpleSchemaAirportRaw,
  mockData,
  recommendationsWithAttributesRaw,
} from '../../../mock-data';
import PaohVisComponent from './paohvis';

const Mockstore = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
  },
});

const Component: Meta<typeof PaohVisComponent.component> = {
  title: 'Visualizations/Paohvis',
  component: PaohVisComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const TestWithMarieBoucherSample = {
  args: {
    ...(await mockData.marieBoucherSample()),
    updateSettings: () => {},
    schema: marieBoucherSampleSchemaRaw,
    settings: {
      ...PaohVisComponent.settings,
      rowNode: 'merchant',
      columnNode: 'merchant',
      attributeRowShow: ['_id', '# Connections', 'name2', 'birth'],
    },
  },
};

export const TestWithRecommendationPersonActedInMovieQueryResult = {
  args: {
    ...(await mockData.movie_recommendationPersonActedInMovieQueryResult()),
    updateSettings: () => {},
    ml: {},
    schema: recommendationsWithAttributesRaw,
    settings: {
      ...PaohVisComponent.settings,
      rowNode: 'Movie',
      columnNode: 'Actor',
      attributeRowShow: ['_id', '# Connections'],
    },
  },
};

// TODO!: Dataset doesnt have current processing structure
/*
export const TestWithBig2ndChamber = {
  args: {
    ...(await mockData.big2ndChamberQueryResult()),
    updateSettings: () => {},
    schema: big2ndChamberSchemaRaw,
    settings: {
      ...PaohVisComponent.settings,
      rowNode: 'commissies',
      columnNode: 'kamerleden',
      attributeRowShow: ['_id', '# Connections', 'naam', 'topic'],
    },
  },
};
*/

export const TestWithAirport = {
  args: {
    ...(await mockData.bigMockQueryResults()),
    updateSettings: () => {},
    schema: simpleSchemaAirportRaw,
    settings: {
      ...PaohVisComponent.settings,
      rowNode: 'airports',
      columnNode: 'airports',
    },
  },
};
/*
export const TestWithRecommendationsActorMovie = {
  args: {
    data: mockRecommendationsActorMovie,
    schema: marieBoucherSampleSchemaRaw,
    settings: PaohVisComponent.settings,
  },
};
*/
export default Component;
