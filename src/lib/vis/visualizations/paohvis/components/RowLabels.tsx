import React, { useEffect, useState, useMemo } from 'react';
import { CustomLine } from './CustomLine';
import { RowInformation } from '../types';
import { getClosestSize } from '../utils/utils';
import { Icon } from '@/lib/components/icon';
import { Tooltip, TooltipTrigger, TooltipContent, TooltipProvider } from '@/lib/components/tooltip';

interface RowLabelsProps {
  dataRows: RowInformation;
  rowHeight: number;
  yOffset: number;
  rowLabelColumnWidth: number;
  classTopTextColumns: string;
  sortState: string;
  headerState: string;
  configStyle: { [key: string]: string };
  numColumns: number;
  onMouseEnterRowLabels: (event: React.MouseEvent<SVGGElement, MouseEvent>) => void;
  onMouseLeaveRowLabels: () => void;
  handleClickHeaderSorting: (event: React.MouseEvent<SVGGElement, MouseEvent>) => void;
}

export const RowLabels = ({
  dataRows,
  rowHeight,
  yOffset,
  rowLabelColumnWidth,
  classTopTextColumns,
  sortState,
  headerState,
  configStyle,
  numColumns,
  onMouseEnterRowLabels,
  onMouseLeaveRowLabels,
  handleClickHeaderSorting,
}: RowLabelsProps) => {
  const accumulatedWidthHeadersTotalValue = dataRows.reduce((sum, item) => sum + item.width, 0);
  const accumulatedWidthHeaders = [0];
  let sum = 0;
  dataRows.forEach((row, index) => {
    sum += row.width;
    if (index !== dataRows.length - 1) {
      accumulatedWidthHeaders.push(sum);
    }
  });
  const [isHovered, setIsHovered] = useState(false);
  const [hoverRowIndex, setHoverRowIndex] = useState<number | null>(null);
  const [iconComponents, setIconComponents] = useState<{ [key: number]: string | undefined }[]>(Array(dataRows.length).fill({}));
  const [iconColors, setIconColors] = useState<string[]>([]);
  const adjustedFontSize = useMemo(() => {
    return Math.max(0.4, (rowHeight * 0.75) / 20);
  }, [rowHeight]);

  useEffect(() => {
    const iconColorsTemporal: string[] = [];

    const updatedIconComponents = dataRows.map((row, indexRows) => {
      let iconComponent: string | undefined = undefined;
      if (row.header == headerState) {
        switch (sortState) {
          case 'asc':
            iconComponent = 'icon-[ic--baseline-arrow-upward]';
            break;
          case 'desc':
            iconComponent = 'icon-[ic--baseline-arrow-downward]';
            break;
          case 'original':
            iconComponent = 'icon-[ic--baseline-sort]';
            break;
          default:
            iconComponent = undefined;
        }
        iconColorsTemporal.push(configStyle.colorText);
      } else {
        iconComponent = undefined;
        iconColorsTemporal.push('hsl(var(--clr-sec--500))');
      }

      if (indexRows == hoverRowIndex && headerState != row.header) {
        iconComponent = 'icon-[ic--baseline-sort]';
      }

      return { [indexRows]: iconComponent };
    });

    setIconColors(iconColorsTemporal);
    setIconComponents(updatedIconComponents);
  }, [sortState, headerState, hoverRowIndex]);

  return (
    <>
      <g key={'rowLabelsInformation'} className="rowLabelsInformation">
        
        {dataRows[0] && dataRows[0].data.map((rowLabel, indexRows) => (
            <g key={'parent_' + indexRows}>
              <g key={'content_' + indexRows} transform={'translate(0,' + (yOffset + rowHeight + indexRows * rowHeight) + ')'}>
                <g className={'rowsLabel row-' + indexRows} onMouseEnter={onMouseEnterRowLabels} onMouseLeave={onMouseLeaveRowLabels}>
                  <TooltipProvider delay={300}>
                    {dataRows.map((row, index) => (
                      <Tooltip key={'gRowTableTooltip-' + index}>
                        <TooltipTrigger asChild>
                          <g key={'gRowTable-' + index} transform={'translate(' + accumulatedWidthHeaders[index] + ',0)'}>
                            {row.data[indexRows] !== undefined &&
                            row.data[indexRows] !== '' &&
                            (typeof row.data[indexRows] !== 'object' || Array.isArray(row.data[indexRows])) ? (
                              <>
                                <rect
                                  width={row.width}
                                  height={rowHeight}
                                  fill={indexRows % 2 === 0 ? 'hsl(var(--clr-sec--50))' : 'hsl(var(--clr-sec--0))'}
                                  strokeWidth={0}
                                ></rect>

                                <foreignObject x="0" y="0" width={row.width} height={rowHeight}>
                                  <div className="w-full h-full flex justify-center items-start">
                                    <span className={`${classTopTextColumns}`} style={{ fontSize: `${adjustedFontSize}rem` }}>
                                      {row.data[indexRows].toString()}
                                    </span>
                                  </div>
                                </foreignObject>
                              </>
                            ) : (
                              <rect
                                width={row.width}
                                height={rowHeight}
                                fill={'url(#diagonal-lines)'}
                                strokeWidth={0}
                              ></rect>
                            )}
                          </g>
                        </TooltipTrigger>
                        <TooltipContent>
                          <div>
                            <span className="overflow-hidden text-ellipsis whitespace-normal max-w-[300px] max-h-[100px] block">
                              {row.data[indexRows] !== undefined &&
                              row.data[indexRows] !== '' &&
                              (typeof row.data[indexRows] !== 'object' || Array.isArray(row.data[indexRows]))
                                ? row.data[indexRows].toString()
                                : 'NoData'}
                            </span>
                          </div>
                        </TooltipContent>
                      </Tooltip>
                    ))}
                  </TooltipProvider>
                </g>
                <CustomLine
                  x1={0}
                  x2={rowLabelColumnWidth}
                  y1={0}
                  y2={0}
                  strokeWidth={0.025 * rowHeight}
                  className={configStyle.colorLinesGridByClass}
                />
                {indexRows === dataRows[0].data.length - 1 && (
                  <CustomLine
                    x1={0}
                    x2={rowLabelColumnWidth}
                    y1={rowHeight}
                    y2={rowHeight}
                    strokeWidth={0.025 * rowHeight}
                    className={configStyle.colorLinesGridByClass}
                  />
                )}
              </g>

              <g
                key={'content_rectangleGrid' + indexRows}
                transform={'translate(' + (accumulatedWidthHeadersTotalValue) + ',' + (yOffset + rowHeight + indexRows * rowHeight) + ')'}
              >
                <rect
                  width={rowHeight * numColumns}
                  height={rowHeight}
                  fill={indexRows % 2 === 0 ? 'hsl(var(--clr-sec--50))' : 'hsl(var(--clr-sec--0))'}
                  strokeWidth={0}
                ></rect>
              </g>
            </g>
          ))}

        <g key={'rowInformationHeaders'} className={'rowInformationHeaders'}>
          {dataRows.map((row, indexRows) => (
            <g key={'headersContent-' + indexRows}>
              <g
                key={'gHeadersRows-' + indexRows}
                className={`headersRows-${row.header} cursor-pointer group`}
                transform={'translate(' + accumulatedWidthHeaders[indexRows] + ',' + (yOffset) + ')'}
                onClick={event => {
                  handleClickHeaderSorting(event);
                }}
                onMouseEnter={event => {
                  setHoverRowIndex(indexRows);
                  setIsHovered(true);
                }}
                onMouseLeave={event => {
                  setIsHovered(sortState !== 'original');
                  setHoverRowIndex(null);
                }}
              >
                <rect
                  width={row.width}
                  height={rowHeight}
                  className="fill-secondary-200 group-hover:fill-secondary-300"
                  opacity={1.0}
                  strokeWidth={0}
                ></rect>
                <foreignObject x="0" y="0" width={row.width - rowHeight} height={rowHeight}>
                  <div className="w-full h-full flex justify-left">
                    <span className={`${classTopTextColumns}`} style={{ fontSize: `${adjustedFontSize}rem` }}>
                      {row.header !== undefined && (typeof row.header !== 'object' || Array.isArray(row.header))
                        ? (row.header as any).toString()
                        : ' '}
                    </span>
                  </div>
                </foreignObject>

                {iconComponents[indexRows] !== undefined && iconComponents[indexRows][indexRows] !== undefined && isHovered && (
                  <foreignObject x={row.width - rowHeight} y={0} width={row.width} height={rowHeight}>
                    <div className="flex items-center justify-right w-full h-full">
                      <Icon
                        component={iconComponents[indexRows][indexRows]}
                        size={getClosestSize(rowHeight)}
                        color={iconColors[indexRows]}
                      />
                    </div>
                  </foreignObject>
                )}
                
              </g>
              <CustomLine
                key={'row-verticalLines-' + indexRows}
                x1={accumulatedWidthHeaders[indexRows]}
                x2={accumulatedWidthHeaders[indexRows]}
                y1={yOffset}
                y2={yOffset + rowHeight + dataRows[0].data.length * rowHeight}
                strokeWidth={0.025 * rowHeight}
                className="fill-secondary-200"
              />
            </g>
          ))}
        </g>
        
      </g>
    </>
  );
};
