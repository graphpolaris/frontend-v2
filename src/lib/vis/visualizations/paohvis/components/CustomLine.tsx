import React from 'react';

interface LineProps {
  x1: number;
  x2: number;
  y1: number;
  y2: number;
  strokeWidth: number;
  fill?: string;
  className?: string;
}

export const CustomLine: React.FC<LineProps> = props => {
  return (
    <rect
      x={props.x1}
      width={props.x2 + props.strokeWidth - props.x1}
      y={props.y1}
      height={props.y2 + props.strokeWidth - props.y1}
      fill={props.fill}
      className={props.className}
    />
  );
};
