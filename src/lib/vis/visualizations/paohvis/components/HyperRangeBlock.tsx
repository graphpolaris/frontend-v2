import React, { useEffect, useState, useMemo } from 'react';
import { CustomLine } from './CustomLine';
import { LinesHyperEdges, PaohvisDataPaginated, RowInformation } from '../types';
import { Icon } from '@/lib/components/icon';
import { Tooltip, TooltipTrigger, TooltipContent, TooltipProvider } from '@/lib/components/tooltip';
import { getClosestSize } from '../utils/utils';

interface HyperEdgeRangesBlockProps {
  dataModel: PaohvisDataPaginated;
  dataLinesHyperEdges: LinesHyperEdges[];
  rowHeight: number;
  yOffset: number;
  rowLabelColumnWidth: number;
  classTopTextColumns: string;
  currentPageRows?: {
    startIndexRow: number;
    endIndexRow: number;
  } | null;
  onMouseEnterHyperEdge: (event: React.MouseEvent<SVGGElement, MouseEvent>) => void;
  onMouseLeaveHyperEdge: () => void;
  sortState: string;
  numRows: number;
  columnHeaderInformation: RowInformation;
  widthColumns: number;
  headerState: string;
  configStyle: { [key: string]: string };
  handleClickHeaderSorting: (event: React.MouseEvent<SVGGElement, MouseEvent>) => void;
}

export const HyperEdgeRangesBlock: React.FC<HyperEdgeRangesBlockProps> = ({
  dataModel,
  dataLinesHyperEdges: dataLinesHyperedges,
  rowHeight,
  yOffset,
  rowLabelColumnWidth,
  classTopTextColumns,
  currentPageRows,
  widthColumns,
  configStyle,
  numRows,
  sortState,
  headerState,
  columnHeaderInformation,
  onMouseEnterHyperEdge,
  onMouseLeaveHyperEdge,
  handleClickHeaderSorting,
}) => {
  const accumulatedWidthHeaders = useMemo(() => {
    const accumulatedWidths: number[] = [0];
    let sum = 0;

    columnHeaderInformation.forEach((column, index) => {
      sum += column.width;
      if (index !== columnHeaderInformation.length - 1) {
        accumulatedWidths.push(sum);
      }
    });

    return accumulatedWidths;
  }, [columnHeaderInformation]);
  const adjustedFontSize = useMemo(() => {
    // text-sm: 0.75 rem, rowHeight: 20
    return Math.max(0.4, (rowHeight * 0.75) / 20);
  }, [rowHeight]);

  const [isHovered, setIsHovered] = useState(false);
  const [hoverRowIndex, setHoverRowIndex] = useState<number | null>(null);
  const [iconComponents, setIconComponents] = useState<{ [key: number]: string | undefined }[]>(
    Array(columnHeaderInformation.length).fill({}),
  );
  const [iconColors, setIconColors] = useState<string[]>([]);

  useEffect(() => {
    const iconColorsTemporal: string[] = [];

    const updatedIconComponents = columnHeaderInformation.map((row, indexRows) => {
      let iconComponent: string | undefined = undefined;
      if (row.header == headerState) {
        switch (sortState) {
          case 'asc':
            iconComponent = 'icon-[ic--baseline-arrow-upward]';
            break;
          case 'desc':
            iconComponent = 'icon-[ic--baseline-arrow-downward]';
            break;
          case 'original':
            iconComponent = 'icon-[ic--baseline-sort]';

            break;
          default:
            iconComponent = undefined;
        }
        iconColorsTemporal.push(configStyle.colorText);
      } else {
        iconComponent = undefined;
        iconColorsTemporal.push('hsl(var(--clr-sec--500))');
      }

      if (indexRows == hoverRowIndex && headerState != row.header) {
        iconComponent = 'icon-[ic--baseline-sort]';
      }
      return { [indexRows]: iconComponent };
    });

    setIconColors(iconColorsTemporal);
    setIconComponents(updatedIconComponents);
  }, [sortState, headerState, hoverRowIndex]);
  const widthHyperEdge = 0.075 * rowHeight;
  return (
    <>
      <g key={'hyperEdgeInformationTop'} className="hyperEdgeInformation">
        
        <g key={'hyperEdgeLinesRows'} transform={`translate(${rowLabelColumnWidth},${yOffset + rowHeight})`}>
          {[...Array(numRows)].map((_, i) => (
            <React.Fragment key={`horizontalLineRows-${i}`}>
              {i === 0 && (
                <CustomLine
                  key={`horizontalLineRowsTop-${i}`}
                  className={`horizontalLineRowsTop-${i} fill-secondary-300`}
                  x1={0}
                  x2={dataModel.pageData.hyperEdgeRanges.length * rowHeight}
                  y1={-yOffset-rowHeight}
                  y2={-yOffset-rowHeight}
                  strokeWidth={0.025 * rowHeight}
                />
              )}
              {i === numRows - 1 && (
                <CustomLine
                  key={`horizontalLineRowsExtra-${i}`}
                  x1={0}
                  x2={dataModel.pageData.hyperEdgeRanges.length * rowHeight}
                  y1={rowHeight * (i + 1)}
                  y2={rowHeight * (i + 1)}
                  strokeWidth={0.025 * rowHeight}
                  className={configStyle.colorLinesGridByClass}
                />
              )}

              <CustomLine
                key={`horizontalLineRowsBottom-${i}`}
                x1={0}
                x2={dataModel.pageData.hyperEdgeRanges.length * rowHeight}
                y1={rowHeight * i}
                y2={rowHeight * i}
                strokeWidth={0.025 * rowHeight}
                className={configStyle.colorLinesGridByClass}
              />
            </React.Fragment>
          ))}
        </g>
        
        <g
          key={'hyperEdgeInformation'}
          className="hyperEdgeInformation text-columns "
          transform={'translate(' + rowLabelColumnWidth + ',' + widthColumns + ')'}
        >
          <TooltipProvider delay={300}>
            {columnHeaderInformation[0] &&
              columnHeaderInformation[0].data.map((rowLabel, indexRows) => (
                <g
                  key={'colsLabel col-' + indexRows}
                  className={'colsLabel col-' + indexRows}
                  transform={'translate(' + indexRows * rowHeight + ',0)rotate(-90,0,0)'}
                >
                  {columnHeaderInformation.map((row, index) => (
                    <Tooltip key={'tooltip-col-' + index}>
                      <TooltipTrigger asChild>
                        <g key={'text-col-' + index} transform={'translate(' + (accumulatedWidthHeaders[index]) + ',0)'}>
                          {row.data[indexRows] !== undefined &&
                          row.data[indexRows] !== '' &&
                          (typeof row.data[indexRows] !== 'object' || Array.isArray(row.data[indexRows])) ? (
                            <>
                              <foreignObject x="0" y="0" width={0 === index ? row.width + rowHeight : row.width} height={rowHeight}>
                                <div className="w-full h-full flex justify-center items-start">
                                  <span
                                    className={`${classTopTextColumns}`}
                                    style={{
                                      fontSize: `${adjustedFontSize}rem`,
                                    }}
                                  >
                                    {row.data[indexRows].toString()}
                                  </span>
                                </div>
                              </foreignObject>
                            </>
                          ) : (
                            <rect
                              width={row.width}
                              height={rowHeight}
                              fill={'url(#diagonal-lines)'}
                              strokeWidth={0}
                            ></rect>
                          )}
                        </g>
                      </TooltipTrigger>
                      <TooltipContent>
                        <div>
                          <span className="overflow-hidden text-ellipsis whitespace-normal max-w-[300px] max-h-[100px] block">
                            {row.data[indexRows] !== undefined &&
                            row.data[indexRows] !== '' &&
                            (typeof row.data[indexRows] !== 'object' || Array.isArray(row.data[indexRows]))
                              ? row.data[indexRows].toString()
                              : 'NoData'}
                          </span>
                        </div>
                      </TooltipContent>
                    </Tooltip>
                  ))}
                </g>
              ))}
          </TooltipProvider>
        </g>

        <g
          key={'columnInformationHeaders'}
          className={'columnInformationHeaders'}
          transform={'translate(' + (rowLabelColumnWidth - rowHeight) + ',' + (widthColumns + rowHeight) + ')'}
        >
          {columnHeaderInformation.map((headerData, headerIndex) => (
            <g
              key={'gHeadersColumns-' + headerIndex}
              className={`headersCols-${headerData.header} cursor-pointer group`}
              transform={'translate(' + 0 + ',' + (-accumulatedWidthHeaders[headerIndex] - rowHeight) + ')rotate(-90, 0,0)'}
              onClick={event => {
                handleClickHeaderSorting(event);
              }}
              onMouseEnter={event => {
                setHoverRowIndex(headerIndex);
                setIsHovered(true);
              }}
              onMouseLeave={event => {
                setIsHovered(sortState !== 'original');
                setHoverRowIndex(null);
              }}
            >
              <rect
                width={headerData.width}
                height={rowHeight}
                className="fill-secondary-200 group-hover:fill-secondary-300"
                opacity={1.0}
                strokeWidth={0}
              ></rect>

              <foreignObject x="0" y="0" width={headerData.width - rowHeight} height={rowHeight}>
                <div className="w-full h-full flex justify-left">
                  <span className={`${classTopTextColumns}`} style={{ fontSize: `${adjustedFontSize}rem` }}>
                    {headerData.header !== undefined && (typeof headerData.header !== 'object' || Array.isArray(headerData.header))
                      ? (headerData.header as any).toString()
                      : ' '}
                  </span>
                </div>
              </foreignObject>

              {iconComponents[headerIndex] !== undefined && iconComponents[headerIndex][headerIndex] !== undefined && isHovered && (
                <foreignObject x={headerData.width - rowHeight} y="0" width={headerData.width} height={rowHeight}>
                  <div className="flex items-center justify-right w-full h-full">
                    <Icon
                      component={iconComponents[headerIndex][headerIndex]}
                      className="inline-flex"
                      size={getClosestSize(rowHeight)}
                      color={iconColors[headerIndex]}
                    />
                  </div>
                </foreignObject>
              )}
              <CustomLine
                x1={headerData.width}
                x2={headerData.width}
                y1={0}
                y2={rowHeight * (dataModel.pageData.hyperEdgeRanges.length + 1)}
                strokeWidth={0.025 * rowHeight}
                className={configStyle.colorLinesGridByClass}
              />
            </g>
          ))}
        </g>
        
        {dataModel.pageData.hyperEdgeRanges.map((hyperEdgeRange, indexHyperEdgeRange) => (
          <React.Fragment key={indexHyperEdgeRange}>
            <g
              className={'hyperEdgeBlockLinesRef hyperEdgeLines-col-' + indexHyperEdgeRange}
              transform={'translate(' + (rowLabelColumnWidth + indexHyperEdgeRange * rowHeight) + ',' + (widthColumns + rowHeight) + ')rotate(-90,0,0)'}
            >
              <CustomLine
                x1={-rowHeight * numRows}
                x2={widthColumns + rowHeight}
                y1={0}
                y2={0}
                strokeWidth={0.025 * rowHeight}
                className={configStyle.colorLinesGridByClass}
              />
            </g>
            {indexHyperEdgeRange === dataModel.pageData.hyperEdgeRanges.length - 1 && (
              <g
                key={'hyperEdgeBlockLinesRef-' + indexHyperEdgeRange}
                className={'hyperEdgeBlockLinesRef hyperEdgeLines-col-' + indexHyperEdgeRange}
                transform={
                  'translate(' +
                  (rowLabelColumnWidth + (dataModel.pageData.hyperEdgeRanges.length - 1) * rowHeight) +
                  ',' +
                  (widthColumns + rowHeight) +
                  ')rotate(-90,0,0)'
                }
              >
                <CustomLine
                  x1={-rowHeight * numRows}
                  x2={widthColumns + rowHeight}
                  y1={rowHeight}
                  y2={rowHeight}
                  strokeWidth={0.025 * rowHeight}
                  className={configStyle.colorLinesGridByClass}
                />
              </g>
            )}
          </React.Fragment>
        ))}

        {dataModel.pageData.hyperEdgeRanges.map((hyperEdgeRange, indexHyperEdgeRange) => (
          <React.Fragment key={`fragment-${indexHyperEdgeRange}`}>
            <g
              className={'hyperEdgeBlock hyperEdge-col-' + indexHyperEdgeRange}
              key={'hyperEdgeBlockddd hyperEdge-col-' + indexHyperEdgeRange}
              onMouseEnter={onMouseEnterHyperEdge}
              onMouseLeave={onMouseLeaveHyperEdge}
            >
              <g
                key={'groupBlockExtra-' + indexHyperEdgeRange}
                transform={`translate(${rowLabelColumnWidth + indexHyperEdgeRange * rowHeight + 0.5 * rowHeight - widthHyperEdge / 2.0},${yOffset + 0.5 * rowHeight + rowHeight})`}
              >
                {currentPageRows && dataLinesHyperedges[indexHyperEdgeRange]?.valid && (
                  <CustomLine
                    key={'hyperRangeBlockLine line_' + indexHyperEdgeRange}
                    x1={0}
                    y1={(dataLinesHyperedges[indexHyperEdgeRange].y0 - currentPageRows.startIndexRow) * rowHeight}
                    x2={0}
                    y2={(dataLinesHyperedges[indexHyperEdgeRange].y1 - currentPageRows.startIndexRow) * rowHeight}
                    strokeWidth={widthHyperEdge}
                    className={configStyle.colorLinesGridByClass}
                  />
                )}
              </g>

              <g
                className={'hyperEdgeBlockCircles'}
                key={'hyperEdgeBlockCircles-' + indexHyperEdgeRange}
                transform={`translate(${rowLabelColumnWidth + indexHyperEdgeRange * rowHeight + 0.5 * rowHeight},${yOffset + 0.5 * rowHeight + rowHeight})`}
              >
                {currentPageRows &&
                  hyperEdgeRange.hyperEdges.indices
                    .filter(valueIndex => valueIndex >= currentPageRows.startIndexRow && valueIndex < currentPageRows.endIndexRow)
                    .map((valueIndex: number, indexColumn: number) => (
                      <circle
                        key={'circleBlock-' + valueIndex + '_' + indexColumn}
                        className={`circle-${valueIndex}`}
                        cx={0}
                        cy={(valueIndex - currentPageRows.startIndexRow) * rowHeight}
                        r={rowHeight * 0.25}
                        fill="white"
                        strokeWidth={0.025 * rowHeight}
                        stroke="black"
                      />
                    ))}
              </g>
            </g>
          </React.Fragment>
        ))}
      </g>
    </>
  );
};
