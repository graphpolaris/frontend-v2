import React, { useEffect, useRef, useState, useMemo, forwardRef, useImperativeHandle } from 'react';
import { PaohvisDataPaginated, RowInformation, LinesHyperEdges } from './types';
import { parseQueryResult } from './utils/dataProcessing';

import { RowLabels } from './components/RowLabels';
import { HyperEdgeRangesBlock } from './components/HyperRangeBlock';

import { sortRowInformation, sortIndices, intersectionElements } from './utils/utils';
import { select, selectAll } from 'd3';

import { SettingsContainer } from '@/lib/vis/components/config';
import { Input } from '@/lib/components/inputs';
import { VisualizationPropTypes, VISComponentType, VisualizationSettingsPropTypes } from '../../common';
import { Button } from '@/lib/components/buttons';
import { EntityPill } from '@/lib/components/pills/Pill';
import { cloneDeep } from 'lodash-es';
import { useImmer } from 'use-immer';
import { Accordion, AccordionBody, AccordionHead, AccordionItem } from '@/lib/components/accordion';
import html2canvas from 'html2canvas';
import { NodeQueryResult } from 'ts-common';

export interface PaohVisHandle {
  exportImageInternal: () => void;
}

export type PaohVisProps = {
  rowHeight: number;
  data?: PaohvisDataPaginated;
  rowNode: string;
  columnNode: string;
  attributeRowShow: string[];
  attributeColumnShow: string[];
  numRowsDisplay: number;
  numColumnsDisplay: number;
  rowJumpAmount: number;
  colJumpAmount: number;
  mergeData: boolean;
  nodeList: string[];
};

const settings: PaohVisProps = {
  rowHeight: 20,
  rowNode: '',
  columnNode: '',
  attributeRowShow: ['_id', '# Connections'],
  attributeColumnShow: ['_id', '# Connections'],
  numRowsDisplay: 30,
  numColumnsDisplay: 30,
  rowJumpAmount: 3,
  colJumpAmount: 3,
  mergeData: false,
  nodeList: [],
};

const PaohVis = forwardRef<PaohVisHandle, VisualizationPropTypes<PaohVisProps>>(
  ({ data, graphMetadata, schema, settings, updateSettings }, refExternal) => {
    // row states
    const [numRowsVisible, setNumRowsVisible] = useState<number>(0);
    const [informationRowOriginal, setInformationRowOriginal] = useState<RowInformation>([]); // rows original, no sorted no pagination
    const [informationRowAllData, setInformationRowAllData] = useState<RowInformation>([]); // rows that will be rendered, sorted but not sliced used by pagination
    const [informationRow, setInformationRow] = useState<RowInformation>([]); // rows that will be rendered, sorted and sliced by pagination

    // - sorting variables
    const [previousHeaderRow, setPreviousHeaderRow] = useState<string>('none');
    const [sortingOrderRow, setSortingOrderRow] = useState<'asc' | 'desc' | 'original'>('original');
    const [permutationIndicesRow, setPermutationIndicesRow] = useState<number[]>([]);

    // columns states
    const [numColsVisible, setNumColsVisible] = useState<number>(0);
    const [informationColumnOriginal, setInformationColumnOriginal] = useState<RowInformation>([]); // columns original, no sorted no pagination
    const [informationColumnAllData, setInformationColumnAllData] = useState<RowInformation>([]); // columns that will be rendered, sorted but not sliced used by pagination
    const [informationColumn, setInformationColumn] = useState<RowInformation>([]); // columns that will be rendered, sorted and sliced by pagination

    // - sorting variables
    const [previousHeaderColumn, setPreviousHeaderColumn] = useState<string>('none');
    const [sortingOrderColumn, setSortingOrderColumn] = useState<'asc' | 'desc' | 'original'>('original');
    const [permutationIndicesColumn, setPermutationIndicesColumn] = useState<number[]>([]);

    // lines hyperEdge
    const [lineHyperEdges, setLineHyperEdges] = useState<LinesHyperEdges[]>([]);

    // render states
    const svgRef = useRef<SVGSVGElement>(null);
    const divRef = useRef<HTMLDivElement>(null);

    // states track order headers attributes
    const prevDisplayAttributesColumns = useRef<string[]>();

    // information hyperedgesBlock
    // dataModel renders bounded by pagination
    const [dataModel, setDataModel] = useImmer<PaohvisDataPaginated>({
      pageData: {
        rowLabels: [],
        hyperEdgeRanges: [],
        rowDegrees: {},
        nodes: [],
        edges: [],
      },
      data: {
        rowLabels: [],
        hyperEdgeRanges: [],
        rowDegrees: {},
        nodes: [],
        edges: [],
      },
      originalData: {
        rowLabels: [],
        hyperEdgeRanges: [],
        rowDegrees: {},
        nodes: [],
        edges: [],
      },
    });

    const [widthTotalRowInformation, setWidthTotalRowInformation] = useState<number>(0);
    const [widthTotalColumnInformation, setWidthTotalColumnInformation] = useState<number>(0);

    const classTopTextColumns = 'text-secondary-800 mx-1 overflow-hidden whitespace-nowrap text-ellipsis';

    const configStyle: { [key: string]: string } = {
      colorText: 'hsl(var(--clr-sec--800))',
      colorTextUnselect: 'hsl(var(--clr-sec--400))',
      colorLinesHyperEdge: 'hsl(var(--clr-black))',
      colorLinesGrid: 'hsl(var(--clr-sec--300))',
      colorLinesGridByClass: 'fill-secondary-300',
    };

    const configPaohvis = useMemo(
      () => ({
        rowHeight: 30,
        hyperEdgeRanges: 30,
        rowsMaxPerPage: settings.numRowsDisplay,
        columnsMaxPerPage: settings.numColumnsDisplay,
        maxSizeTextColumns: 120,
        maxSizeTextRows: 120,
        maxSizeTextID: 70,
        sizeIcons: 16,
      }),
      [settings],
    );

    const [currentPageColumns, setCurrentPageColumns] = useState<{
      startIndexColumn: number;
      endIndexColumn: number;
    } | null>({
      startIndexColumn: 0,
      endIndexColumn: Math.min(configPaohvis.columnsMaxPerPage, dataModel.data.hyperEdgeRanges.length),
    });

    const [currentPageRows, setCurrentPageRows] = useState<{
      startIndexRow: number;
      endIndexRow: number;
    } | null>({
      startIndexRow: 0,
      endIndexRow: Math.min(configPaohvis.rowsMaxPerPage, dataModel.data.rowLabels.length),
    });

    /////////////
    // Effects //
    /////////////

    useEffect(() => {
      if (!svgRef.current) return;
      const resizeObserver = new ResizeObserver(() => {});
      resizeObserver.observe(svgRef.current);
      return () => resizeObserver.disconnect(); // clean up
    }, []);

    useEffect(() => {
      setSortingOrderRow('original');
      setSortingOrderColumn('original');
    }, []);

    useEffect(() => {
      if (
        graphMetadata &&
        settings.columnNode !== '' &&
        settings.rowNode !== '' &&
        graphMetadata.nodes.types[settings.rowNode] &&
        graphMetadata.nodes.types[settings.columnNode]
      ) {
        ////////////////////////
        // configure settings //
        ////////////////////////

        const columnNodeAttributes = Object.keys(graphMetadata.nodes.types[settings.columnNode].attributes);
        const firstColumnLabels = columnNodeAttributes.slice(0, 2);
        const rowNodeAttributes = Object.keys(graphMetadata.nodes.types[settings.rowNode].attributes);
        const firstRowLabels = rowNodeAttributes.slice(0, 2);

        if (firstColumnLabels && firstRowLabels) {
          if (settings.attributeColumnShow.includes('_id')) {
            updateSettings({ attributeColumnShow: [...firstColumnLabels, '# Connections'] });
          }
          if (settings.attributeRowShow.includes('_id')) {
            updateSettings({ attributeRowShow: [...firstRowLabels, '# Connections'] });
          }
        }

        ////////////////////
        // configure data //
        ////////////////////

        // get new data

        const labelEdge = graphMetadata.edges.labels[0] || '';
        const edgeSchema = schema.edges.find(obj => String(obj.key).includes(labelEdge));
        const toNode = edgeSchema?.target as string;
        const newData = parseQueryResult(data, settings as PaohVisProps, toNode, settings.mergeData);

        // to keep order of new attributes
        prevDisplayAttributesColumns.current = [...columnNodeAttributes]; // unused for now

        // initialize rows/cols data

        const rowNodesRaw = data.nodes
          .filter(obj => obj['label'].includes(settings.rowNode))
          .reduce((acc: { [_id: string]: NodeQueryResult }, node) => {
            acc[node._id] = node;
            return acc;
          }, {});
        const columnNodesRaw = data.nodes
          .filter(obj => obj['label'].includes(settings.columnNode))
          .reduce((acc: { [_id: string]: NodeQueryResult }, node) => {
            acc[node._id] = node;
            return acc;
          }, {});

        // sort the row/columnNodes to match the newData order
        const rowNodes = newData.rowLabels.map(rl => rowNodesRaw[rl]);
        const columnNodes = newData.hyperEdgeRanges.map(he => columnNodesRaw[he._id]);

        // - columns
        const informationColumnOriginal: { header: string; data: any[]; width: number }[] = Object.entries(
          graphMetadata.nodes.types[settings.columnNode].attributes,
        ).map(([k, v]) => {
          const mappedData = columnNodes.map(node => node.attributes[k]);
          return {
            header: k,
            data: mappedData,
            width: configPaohvis.maxSizeTextColumns,
          };
        });

        const columnsIdDegree: { [_id: string]: number } = newData.hyperEdgeRanges.reduce((acc: { [_id: string]: number }, node) => {
          acc[node._id] = node.degree;
          return acc;
        }, {});

        informationColumnOriginal.push({
          header: '# Connections',
          data: columnNodes.map(node => columnsIdDegree[node._id]),
          width: configPaohvis.maxSizeTextColumns,
        });

        informationColumnOriginal.push({
          header: '_id',
          data: columnNodes.map(node => node._id),
          width: configPaohvis.maxSizeTextID,
        });

        // - rows
        const informationRowOriginal: { header: string; data: any[]; width: number }[] = Object.entries(
          graphMetadata.nodes.types[settings.rowNode].attributes,
        ).map(([k, v]) => {
          const mappedData = rowNodes.map(node => node.attributes[k]);

          return {
            header: k,
            data: mappedData,
            width: configPaohvis.maxSizeTextRows,
          };
        });

        const idsRows = rowNodes.map(obj => obj._id);

        informationRowOriginal.push({
          header: '# Connections',
          data: idsRows.map(id => newData.rowDegrees[id]),
          width: configPaohvis.maxSizeTextRows,
        });

        informationRowOriginal.push({
          header: '_id',
          data: idsRows,
          width: configPaohvis.maxSizeTextID,
        });

        // initialize pagination
        const currentPageRows = {
          startIndexRow: 0,
          endIndexRow: Math.min(configPaohvis.rowsMaxPerPage, newData.rowLabels.length),
        };

        const currentPageColumns = {
          startIndexColumn: 0,
          endIndexColumn: Math.min(configPaohvis.columnsMaxPerPage, newData.hyperEdgeRanges.length),
        };

        const numRowsVisible = currentPageRows.endIndexRow - currentPageRows.startIndexRow;
        const numColsVisible = currentPageColumns.endIndexColumn - currentPageColumns.startIndexColumn;

        // state updates

        setInformationColumnOriginal(informationColumnOriginal);
        setInformationRowOriginal(informationRowOriginal);

        const sortIndicesColumn = computeSortingIndices(informationColumnOriginal, previousHeaderColumn, sortingOrderColumn);
        const sortIndicesRow = computeSortingIndices(informationRowOriginal, previousHeaderRow, sortingOrderRow);

        setPermutationIndicesColumn(sortIndicesColumn);
        setPermutationIndicesRow(sortIndicesRow);

        setNumRowsVisible(numRowsVisible);
        setNumColsVisible(numColsVisible);
        setCurrentPageRows(currentPageRows);
        setCurrentPageColumns(currentPageColumns);

        setDataModel({
          pageData: {
            rowLabels: newData.rowLabels.slice(0, configPaohvis.rowsMaxPerPage),
            hyperEdgeRanges: newData.hyperEdgeRanges.slice(0, configPaohvis.columnsMaxPerPage),
            rowDegrees: newData.rowDegrees,
            nodes: newData.nodes,
            edges: newData.edges,
          },
          data: newData,
          originalData: newData,
        });
      }
    }, [
      data,
      schema,
      graphMetadata,
      settings.rowNode,
      settings.columnNode,
      settings.attributeRowShow,
      settings.attributeColumnShow,
      settings.mergeData,
    ]);

    // apply sorting
    useEffect(() => {
      const sortedColumnInformation = sortRowInformation(informationColumnOriginal, permutationIndicesColumn);
      const sortedRowInformation = sortRowInformation(informationRowOriginal, permutationIndicesRow);
      setInformationColumnAllData(sortedColumnInformation);
      setInformationRowAllData(sortedRowInformation);
    }, [informationColumnOriginal, informationRowOriginal, permutationIndicesColumn, permutationIndicesRow]);

    // trigger pagination if numRows/numColumns change
    useEffect(() => {
      onPageChangeRows(0);
    }, [configPaohvis.rowsMaxPerPage]);
    useEffect(() => {
      onPageChangeColumns(0);
    }, [configPaohvis.columnsMaxPerPage]);

    // apply pagination
    useEffect(() => {
      const sortedColumnInformationSliced = informationColumnAllData.map(row => ({
        ...row,
        data: row.data.slice(currentPageColumns?.startIndexColumn, currentPageColumns?.endIndexColumn),
      }));

      const sortedColumnInformationSlicedFiltered = sortedColumnInformationSliced.filter(row =>
        settings.attributeColumnShow.includes(row.header),
      );

      const sortedRowInformationSliced = informationRowAllData.map(row => ({
        ...row,
        data: row.data.slice(currentPageRows?.startIndexRow, currentPageRows?.endIndexRow),
      }));

      const sortedRowInformationSlicedFiltered = sortedRowInformationSliced.filter(row => settings.attributeRowShow.includes(row.header));

      setInformationColumn(sortedColumnInformationSlicedFiltered);
      setInformationRow(sortedRowInformationSlicedFiltered);
    }, [informationColumnAllData, informationRowAllData, currentPageColumns, currentPageRows, numColsVisible, numRowsVisible]);

    // update representation for rendering
    useEffect(() => {
      const dataModelOriginalCopy = cloneDeep(dataModel.originalData);

      // hyperEdges nodes
      // - sort rows according to permutations indices
      for (let indexColOrder = 0; indexColOrder < dataModel.originalData.hyperEdgeRanges.length; indexColOrder++) {
        for (
          let indexRowsIndices = 0;
          indexRowsIndices < dataModel.originalData.hyperEdgeRanges[indexColOrder].hyperEdges.indices.length;
          indexRowsIndices++
        ) {
          dataModelOriginalCopy.hyperEdgeRanges[indexColOrder].hyperEdges.indices[indexRowsIndices] = permutationIndicesRow.indexOf(
            dataModel.originalData.hyperEdgeRanges[indexColOrder].hyperEdges.indices[indexRowsIndices],
          );
        }

        // sort indices - correct render
        dataModelOriginalCopy.hyperEdgeRanges[indexColOrder].hyperEdges.indices = dataModelOriginalCopy.hyperEdgeRanges[
          indexColOrder
        ].hyperEdges.indices.sort((a: number, b: number) => a - b);
      }

      const sortedArrayDataModelColumns = permutationIndicesColumn
        .map(index => dataModelOriginalCopy.hyperEdgeRanges[index])
        .filter(d => !!d);

      const sortedArrayDataModelFilteredColumns = sortedArrayDataModelColumns.slice(
        currentPageColumns?.startIndexColumn,
        currentPageColumns?.endIndexColumn,
      );

      // hyperedges lines
      let newLinePositions;
      if (currentPageRows?.startIndexRow !== undefined && currentPageRows?.endIndexRow !== undefined) {
        newLinePositions = sortedArrayDataModelFilteredColumns.map(hyperEdgeRange => {
          return intersectionElements([currentPageRows?.startIndexRow, currentPageRows?.endIndexRow], hyperEdgeRange.hyperEdges.indices);
        });
      } else {
        newLinePositions = sortedArrayDataModelFilteredColumns.map(hyperEdgeRange => {
          return intersectionElements(
            [0, Math.min(configPaohvis.rowsMaxPerPage, dataModel.data.rowLabels.length)],
            hyperEdgeRange.hyperEdges.indices,
          );
        });
      }

      const totalWidthColumnInformation = informationColumn.reduce((acc, row) => acc + row.width, 0);
      const totalWidthRowInformation = informationRow.reduce((acc, row) => acc + row.width, 0);

      setWidthTotalColumnInformation(totalWidthColumnInformation);
      setWidthTotalRowInformation(totalWidthRowInformation);

      setLineHyperEdges(newLinePositions);

      // data model
      setDataModel(draft => {
        draft.pageData.hyperEdgeRanges = sortedArrayDataModelFilteredColumns;
        draft.data.hyperEdgeRanges = sortedArrayDataModelColumns;
      });
    }, [informationColumn, informationRow]);

    const computedSizesSvg = useMemo(() => {
      let tableWidth = 0;
      let tableWidthWithExtraColumnLabelWidth = 0;

      dataModel.pageData.hyperEdgeRanges.forEach(hyperEdgeRange => {
        const columnWidth = 1 * settings.rowHeight;
        tableWidth += columnWidth;

        if (tableWidth > tableWidthWithExtraColumnLabelWidth) tableWidthWithExtraColumnLabelWidth = tableWidth;
      });

      let finalTableWidth = tableWidthWithExtraColumnLabelWidth;
      finalTableWidth += widthTotalRowInformation;
      finalTableWidth += finalTableWidth * 0.01;

      return {
        tableWidthWithExtraColumnLabelWidth: finalTableWidth,
        colWidth: widthTotalColumnInformation,
      };
    }, [
      dataModel,
      settings.rowHeight,
      widthTotalColumnInformation,
      widthTotalRowInformation,
      settings.attributeColumnShow,
      settings.attributeRowShow,
    ]);

    ////////////////////
    // Event handlers //
    ////////////////////

    const onMouseEnterRowLabels = (event: React.MouseEvent<SVGGElement, MouseEvent>) => {
      const targetClassList = (event.currentTarget as SVGGElement).classList;
      // all elements - unselect
      selectAll('.rowsLabel').selectAll('span').style('color', configStyle.colorTextUnselect);

      selectAll('.' + targetClassList[1])
        .selectAll('span')
        .style('color', configStyle.colorText);

      // all hyperedges - unselect
      const hyperEdgeBlock = selectAll('.hyperEdgeBlock');
      hyperEdgeBlock.selectAll('circle').attr('stroke-opacity', '.3');
      hyperEdgeBlock.selectAll('line').attr('opacity', '.3');
      selectAll('.text-columns').selectAll('span').style('color', configStyle.colorTextUnselect);

      // get row selected
      const rowSelection: number = parseInt(targetClassList[1].substring('row-'.length), 10);

      // get circles on the same row
      selectAll('.circle-' + (rowSelection + (currentPageRows?.startIndexRow ?? 0))).each(function (d, i) {
        // get all hyperedges which are connected those circles
        const hyperEdge = (select(this).node() as Element)?.parentNode?.parentNode;
        if (hyperEdge instanceof Element) {
          const classList = Array.from(hyperEdge.classList);
          // text columns
          selectAll('.col-' + classList[1].substring('hyperEdge-col-'.length))
            .selectAll('span')
            .style('color', configStyle.colorText);

          // hypererdge
          select('.' + classList[1])
            .selectAll('circle')
            .attr('fill', 'hsl(var(--clr-acc))')
            .attr('stroke-opacity', '1');

          select('.' + classList[1])
            .selectAll('line')
            .attr('opacity', '1');

          // text rows
          selectAll('.' + classList[1])
            .select('.hyperEdgeBlockCircles')
            .selectAll('circle')
            .each(function () {
              const circleInside: number = parseInt(select(this).attr('class').substring('circle-'.length), 10);
              selectAll('.row-' + (circleInside - (currentPageRows?.startIndexRow ?? 0)))
                .selectAll('span')
                .style('color', configStyle.colorText);
            });
        }
      });
    };

    const onMouseLeaveRowLabels = () => {
      selectAll('.rowsLabel').selectAll('span').style('color', configStyle.colorText);
      const hyperEdgeBlock = selectAll('.hyperEdgeBlock');
      hyperEdgeBlock.selectAll('circle').attr('stroke-opacity', '1');
      hyperEdgeBlock.selectAll('circle').attr('fill', 'white');
      hyperEdgeBlock.selectAll('line').attr('opacity', '1');
      selectAll('.colsLabel').selectAll('span').style('color', configStyle.colorText);
    };

    const onMouseEnterHyperEdge = (event: React.MouseEvent<SVGGElement, MouseEvent>) => {
      const targetClassList = (event.currentTarget as SVGGElement).classList;
      // all elements
      const hyperEdgeBlock = selectAll('.hyperEdgeBlock');
      // all elements: hyperedges
      hyperEdgeBlock.selectAll('circle').attr('stroke-opacity', '.3');
      hyperEdgeBlock.selectAll('line').attr('opacity', '.3');
      // all elements: column text and row text
      selectAll('.colsLabel').selectAll('span').style('color', configStyle.colorTextUnselect);
      selectAll('.rowsLabel').selectAll('span').style('color', configStyle.colorTextUnselect);

      // selected elements
      const hyperEdgeSelected = select('.' + targetClassList[1]);
      hyperEdgeSelected.selectAll('circle').attr('fill', 'hsl(var(--clr-acc))');
      hyperEdgeSelected.selectAll('circle').attr('stroke-opacity', '1');
      hyperEdgeSelected.selectAll('line').attr('opacity', '1');

      // selected elements col text
      const columnSelection = targetClassList[1].substring('hyperEdge-'.length);

      selectAll('.' + columnSelection)
        .selectAll('span')
        .style('color', configStyle.colorText);

      // selected elements nodes text
      hyperEdgeSelected.selectAll('circle').each(function (d, i) {
        const className = select(this).attr('class');

        const index = className.split('circle-')[1];
        if (currentPageRows) {
          const indexNumber = parseInt(index);
          const rowSelector = `.row-${indexNumber - currentPageRows.startIndexRow}`;
          select(rowSelector).selectAll('span').style('color', configStyle.colorText);
        }
      });
    };

    const onMouseLeaveHyperEdge = () => {
      // all elements
      selectAll('.colsLabel').selectAll('span').style('color', configStyle.colorText);
      selectAll('.rowsLabel').selectAll('span').style('color', configStyle.colorText);

      const hyperEdgeBlock = selectAll('.hyperEdgeBlock');
      hyperEdgeBlock.selectAll('circle').attr('stroke-opacity', '1');
      hyperEdgeBlock.selectAll('circle').attr('fill', 'white');
      hyperEdgeBlock.selectAll('line').attr('opacity', '1');
    };

    const handleClickHeaderSorting = (event: React.MouseEvent<SVGGElement, MouseEvent>) => {
      // get target header
      let targetHeader = (event.currentTarget as SVGGElement).classList[0].replace('headersRows-', '');
      targetHeader = targetHeader == '#' ? '# Connections' : targetHeader;

      // set sorting orders. Tracks header change, new header changes to asc
      let newSortingOrder: 'asc' | 'desc' | 'original';

      if (targetHeader !== previousHeaderRow) {
        newSortingOrder = 'desc';
      } else {
        switch (sortingOrderRow) {
          case 'asc':
            newSortingOrder = 'original';
            break;
          case 'desc':
            newSortingOrder = 'asc';
            break;
          case 'original':
            newSortingOrder = 'desc';
            break;
        }
      }

      const permutationIndicesRow = computeSortingIndices(informationRowOriginal, targetHeader, newSortingOrder);

      if (newSortingOrder == 'original') {
        // reset previous state
        setPreviousHeaderRow('none');
      } else {
        setPreviousHeaderRow(targetHeader);
      }
      setSortingOrderRow(newSortingOrder);
      setPermutationIndicesRow(permutationIndicesRow);
    };

    const handleClickHeaderSortingColumns = (event: React.MouseEvent<SVGGElement, MouseEvent>) => {
      // get target header
      let targetHeader = (event.currentTarget as SVGGElement).classList[0].replace('headersCols-', '');

      targetHeader = targetHeader == '#' ? '# Connections' : targetHeader;
      // set sorting orders. Tracks header change, new header changes to asc
      let newSortingOrder: 'asc' | 'desc' | 'original';

      if (targetHeader !== previousHeaderColumn) {
        newSortingOrder = 'desc';
      } else {
        switch (sortingOrderColumn) {
          case 'asc':
            newSortingOrder = 'original';
            break;
          case 'desc':
            newSortingOrder = 'asc';
            break;
          case 'original':
            newSortingOrder = 'desc';
            break;
        }
      }

      const permutationIndicesColumn = computeSortingIndices(informationColumnOriginal, targetHeader, newSortingOrder);

      if (newSortingOrder == 'original') {
        // reset previous state
        setPreviousHeaderColumn('none');
      } else {
        setPreviousHeaderColumn(targetHeader);
      }
      setSortingOrderColumn(newSortingOrder);
      setPermutationIndicesColumn(permutationIndicesColumn);
    };

    const computeSortingIndices = (information: RowInformation, targetHeader: string, sortingOrder: 'asc' | 'desc' | 'original') => {
      // get permutations indices
      let sortedIndices: number[];
      switch (sortingOrder) {
        case 'asc':
          sortedIndices = sortIndices(information, targetHeader, 'asc');
          break;
        case 'desc':
          sortedIndices = sortIndices(information, targetHeader, 'desc');
          break;
        case 'original':
          sortedIndices = [];
          break;

        default:
          sortedIndices = [];
          break;
      }

      if (sortedIndices.length == 0 && information.length > 0) {
        sortedIndices = Array.from({ length: information[0].data.length }, (_, idx) => idx);
      }

      return sortedIndices;
    };

    const onWheel = (event: React.WheelEvent<SVGSVGElement>) => {
      if (event.deltaY !== 0) {
        if (event.shiftKey) onPageChangeColumns(event.deltaY > 0 ? settings.colJumpAmount : -settings.colJumpAmount);
        else onPageChangeRows(event.deltaY > 0 ? settings.rowJumpAmount : -settings.rowJumpAmount);
      }

      if (event.deltaX !== 0) {
        onPageChangeColumns(event.deltaX > 0 ? settings.colJumpAmount : -settings.colJumpAmount);
      }
    };

    const onPageChangeColumns = (delta: number) => {
      const startIndexColumn = (currentPageColumns?.startIndexColumn || 0) + delta;
      if (startIndexColumn < 0 || startIndexColumn + 10 > dataModel.data.hyperEdgeRanges.length) return;
      const endIndexColumn = Math.min(startIndexColumn + configPaohvis.columnsMaxPerPage, dataModel.data.hyperEdgeRanges.length);

      setNumColsVisible(endIndexColumn - startIndexColumn);

      setCurrentPageColumns({
        startIndexColumn: startIndexColumn,
        endIndexColumn: endIndexColumn,
      });
    };

    const onPageChangeRows = (delta: number) => {
      const startIndexRow = (currentPageRows?.startIndexRow || 0) + delta;
      if (startIndexRow < 0 || startIndexRow + 10 > dataModel.data.rowLabels.length) return;
      const endIndexRow = Math.min(startIndexRow + configPaohvis.rowsMaxPerPage, dataModel.data.rowLabels.length);

      setNumRowsVisible(endIndexRow - startIndexRow);

      setCurrentPageRows({
        startIndexRow: startIndexRow,
        endIndexRow: endIndexRow,
      });
    };

    const exportImageInternal = () => {
      if (divRef.current) {
        const options = {
          backgroundColor: '#FFFFFF',
          foreignObjectRendering: false,
          removeContainer: false,
        };
        html2canvas(divRef.current, options).then(canvas => {
          const pngData = canvas.toDataURL('image/png');
          const a = document.createElement('a');
          a.href = pngData;
          a.download = 'paohvis.png';
          a.click();
        });
      } else {
        console.error('The referenced div is null.');
      }
    };

    useImperativeHandle(refExternal, () => ({
      exportImageInternal,
    }));

    return (
      <div
        className="overflow-hidden"
        ref={divRef}
        style={{
          width: '100%',
          height: '99%',
        }}
      >
        <svg
          className="m-1 overflow-hidden font-data"
          ref={svgRef}
          style={{
            width: '100%',
            height: '99%',
          }}
          onWheel={onWheel}
        >
          <defs>
            <pattern id="diagonal-lines" patternUnits="userSpaceOnUse" width="8" height="8" patternTransform="rotate(45)">
              <rect width="6" height="8" fill="transparent"></rect>
              <rect x="6" width="2" height="8" fill="#eaeaea"></rect>
            </pattern>
          </defs>

          <RowLabels
            dataRows={informationRow}
            rowHeight={settings.rowHeight}
            yOffset={computedSizesSvg.colWidth}
            rowLabelColumnWidth={widthTotalRowInformation}
            classTopTextColumns={classTopTextColumns}
            onMouseEnterRowLabels={onMouseEnterRowLabels}
            onMouseLeaveRowLabels={onMouseLeaveRowLabels}
            handleClickHeaderSorting={handleClickHeaderSorting}
            sortState={sortingOrderRow}
            headerState={previousHeaderRow}
            configStyle={configStyle}
            numColumns={numColsVisible}
          />

          <HyperEdgeRangesBlock
            dataModel={dataModel}
            dataLinesHyperEdges={lineHyperEdges}
            rowHeight={settings.rowHeight}
            yOffset={computedSizesSvg.colWidth}
            rowLabelColumnWidth={widthTotalRowInformation}
            classTopTextColumns={classTopTextColumns}
            currentPageRows={currentPageRows}
            widthColumns={widthTotalColumnInformation}
            columnHeaderInformation={informationColumn}
            configStyle={configStyle}
            onMouseEnterHyperEdge={onMouseEnterHyperEdge}
            onMouseLeaveHyperEdge={onMouseLeaveHyperEdge}
            numRows={numRowsVisible}
            sortState={sortingOrderColumn}
            handleClickHeaderSorting={handleClickHeaderSortingColumns}
            headerState={previousHeaderColumn}
          />
        </svg>
      </div>
    );
  },
);

const PaohSettings = ({ settings, graphMetadata, updateSettings }: VisualizationSettingsPropTypes<PaohVisProps>) => {
  const rowNodeInformation = useMemo(() => {
    let attributes: string[] = [];
    let nodeCount = 0;

    if (settings.rowNode) {
      const nodeType = graphMetadata.nodes.types[settings.rowNode];

      if (nodeType) {
        nodeCount = nodeType.count;

        if (nodeType.attributes) {
          attributes = Object.keys(nodeType.attributes);
          attributes.unshift('# Connections');
        }
      }
    }

    return { attributes, nodeCount };
  }, [settings.rowNode, graphMetadata]);

  const columnsNodeInformation = useMemo(() => {
    let attributes: string[] = [];
    let nodeCount = 0;

    if (settings.columnNode) {
      const nodeType = graphMetadata.nodes.types[settings.columnNode];

      if (nodeType) {
        nodeCount = nodeType.count;

        if (nodeType.attributes) {
          attributes = Object.keys(nodeType.attributes);
          attributes.unshift('# Connections');
        }
      }
    }

    return { attributes, nodeCount };
  }, [settings.columnNode, graphMetadata]);

  useEffect(() => {
    if (graphMetadata && graphMetadata.nodes) {
      updateSettings({ nodeList: graphMetadata.nodes.labels });
    }
  }, [graphMetadata]);

  useEffect(() => {
    if (!settings.nodeList) return;

    let rowNode = settings.rowNode;
    let columnNode = settings.columnNode;
    const availableNodes = [...settings.nodeList];

    const indexRowNode = availableNodes.indexOf(rowNode);
    const indexColumnNode = availableNodes.indexOf(columnNode);

    if (indexRowNode === -1) {
      rowNode = '';
    } else {
      availableNodes.splice(indexRowNode, 1);
    }
    if (indexColumnNode === -1) {
      columnNode = '';
    } else {
      availableNodes.splice(indexColumnNode, 1);
    }

    if (availableNodes.length > 0 && rowNode === '') {
      rowNode = availableNodes.pop()!;
    }
    if (availableNodes.length > 0 && columnNode === '') {
      columnNode = availableNodes.pop()!;
    }

    updateSettings({ rowNode: rowNode, columnNode: columnNode });
  }, [settings.nodeList]);

  return (
    <SettingsContainer>
      <div className="overflow-x-hidden">
        <div>
          <span className="text-xs font-semibold">Node used in Row</span>
          <Input
            className="w-full text-justify justify-start"
            type="dropdown"
            value={settings.rowNode}
            options={settings.nodeList}
            onChange={val => updateSettings({ rowNode: val as string })}
            overrideRender={
              <EntityPill
                title={
                  <div className="flex flex-row justify-between items-center cursor-pointer">
                    <span>{settings.rowNode || ''}</span>
                    <Button variantType="secondary" variant="ghost" size="2xs" iconComponent="icon-[ic--baseline-arrow-drop-down]" />
                  </div>
                }
              />
            }
          ></Input>
        </div>

        <Accordion>
          <AccordionItem>
            <AccordionHead>
              <span className="font-semibold">attributes: </span>
            </AccordionHead>
            <AccordionBody>
              <Input
                type="checkbox"
                value={settings.attributeRowShow}
                options={rowNodeInformation.attributes}
                onChange={(val: string[] | string) => {
                  const updatedVal = Array.isArray(val) ? val : [val];
                  updateSettings({ attributeRowShow: updatedVal });
                }}
                disabled={rowNodeInformation.attributes.map(d => ['_id', '# Connections'].includes(d))}
              />
            </AccordionBody>
          </AccordionItem>
        </Accordion>

        <div>
          <span className="text-xs font-semibold">Node used in Column</span>
          <Input
            className="w-full text-justify justify-start"
            type="dropdown"
            value={settings.columnNode}
            options={settings.nodeList}
            onChange={val => updateSettings({ columnNode: val as string })}
            overrideRender={
              <EntityPill
                title={
                  <div className="flex flex-row justify-between items-center cursor-pointer">
                    <span>{settings.columnNode || ''}</span>
                    <Button variantType="secondary" variant="ghost" size="2xs" iconComponent="icon-[ic--baseline-arrow-drop-down]" />
                  </div>
                }
              />
            }
          />
        </div>

        <Accordion>
          <AccordionItem>
            <AccordionHead>
              <span className="font-semibold">attributes: </span>
            </AccordionHead>
            <AccordionBody>
              <Input
                type="checkbox"
                value={settings.attributeColumnShow}
                options={columnsNodeInformation.attributes}
                onChange={(val: string[] | string) => {
                  const updatedVal = Array.isArray(val) ? val : [val];
                  updateSettings({ attributeColumnShow: updatedVal });
                }}
                disabled={columnsNodeInformation.attributes.map(d => ['_id', '# Connections'].includes(d))}
              />
            </AccordionBody>
          </AccordionItem>
        </Accordion>

        <Input
          type="slider"
          label="Row height"
          value={settings.rowHeight}
          onChange={val => updateSettings({ rowHeight: val })}
          min={8}
          max={30}
          step={1}
        />

        <Input
          type="slider"
          label="# Rows"
          value={settings.numRowsDisplay}
          onChange={val => updateSettings({ numRowsDisplay: val })}
          min={5}
          max={rowNodeInformation.nodeCount}
          step={1}
        />
        <Input
          type="slider"
          label="# Columns"
          value={settings.numColumnsDisplay}
          onChange={val => updateSettings({ numColumnsDisplay: val })}
          min={5}
          max={columnsNodeInformation.nodeCount}
          step={1}
        />
        <Input
          type="number"
          label="Row jump sensitivity"
          value={settings.rowJumpAmount}
          onChange={val => updateSettings({ rowJumpAmount: val })}
          containerClassName="pt-2"
        />
        <Input
          type="number"
          label="Column jump sensitivity"
          value={settings.colJumpAmount}
          onChange={val => updateSettings({ colJumpAmount: val })}
          containerClassName="pt-2"
        />
        <Input
          type="boolean"
          label="Merge Data"
          value={settings.mergeData}
          onChange={val => updateSettings({ mergeData: val })}
          className="pt-2"
        />
      </div>
    </SettingsContainer>
  );
};

const paohvisRef = React.createRef<{ exportImageInternal: () => void }>();

export const PaohVisComponent: VISComponentType<PaohVisProps> = {
  component: React.forwardRef((props: VisualizationPropTypes<PaohVisProps>, ref) => <PaohVis {...props} ref={paohvisRef} />),
  settingsComponent: PaohSettings,
  settings: settings,
  exportImage: () => {
    if (paohvisRef.current) {
      paohvisRef.current.exportImageInternal();
    } else {
      console.error('Paohvis reference is not set.');
    }
  },
};

export default PaohVisComponent;
