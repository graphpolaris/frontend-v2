import React from 'react';
import { Meta } from '@storybook/react';
import { graphQueryResultSlice, schemaSlice, visualizationSlice, searchResultSlice } from '../../../data-access/store';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { mockData } from '../../../mock-data';
import { NodeLinkComponent } from './nodelinkvis';
import { configSlice } from '@/lib/data-access/store/configSlice';

const Mockstore = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
    config: configSlice.reducer,
    searchResults: searchResultSlice.reducer,
  },
});

const Component: Meta<typeof NodeLinkComponent.component> = {
  title: 'Visualizations/NodeLinkVis',
  component: NodeLinkComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const TestWithData = {
  args: {
    ...(await mockData.smallVillainQueryResults()),
    ml: {},
    settings: NodeLinkComponent.settings,
    dispatch: () => {},
  },
};

export const TestWithDoubleArchData = {
  args: {
    ...(await mockData.smallVillainDoubleArchQueryResults()),
    ml: {},
    settings: NodeLinkComponent.settings,
    dispatch: () => {},
  },
};

export const TestWithNoData = {
  args: {
    data: {
      nodes: [],
      edges: [],
    },
    ml: {},
    settings: NodeLinkComponent.settings,
    dispatch: () => {},
  },
};

// TODO!: Dataset doesnt have current processing structure
/*
export const TestWithBig2ndChamber = {
  args: {
    ...(await mockData.big2ndChamberQueryResult()),
    ml: {},
    settings: NodeLinkComponent.settings,
    dispatch: () => {},
  },
};
*/
export const TestWithSmallFlights = {
  args: {
    ...(await mockData.smallFlightsQueryResults()),
    ml: {},
    settings: NodeLinkComponent.settings,
    dispatch: () => {},
  },
};

// TODO!: Dataset doesnt have current processing structure
/*
export const TestWithLargeQueryResult = {
  args: {
    ...(await mockData.mockLargeQueryResults()),
    ml: {},
    settings: NodeLinkComponent.settings,
    dispatch: () => {},
  },
};
*/

export const TestWithRecommendationPersonActedInMovieQueryResult = {
  args: {
    ...(await mockData.movie_recommendationPersonActedInMovieQueryResult()),
    ml: {},
    settings: NodeLinkComponent.settings,
    dispatch: () => {},
  },
};

export const TestWithSlackReactionToThreadedMessageQueryResult = {
  args: {
    ...(await mockData.slack_slackReactionToThreadedMessageQueryResult()),
    ml: {},
    settings: NodeLinkComponent.settings,
    dispatch: () => {},
  },
};

export default Component;
