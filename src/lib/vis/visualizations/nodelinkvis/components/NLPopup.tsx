import React from 'react';
import { TooltipProvider } from '@/lib/components/tooltip';
import { ATTRIBUTE_MAX_CHARACTERS } from '@/config';

const formatNumber = (number: number) => {
  return number.toLocaleString('de-DE');
};

export type NLPopUpProps = {
  data: Record<string, any>;
};

export const NLPopUp: React.FC<NLPopUpProps> = ({ data }) => {
  return (
    <TooltipProvider delay={100}>
      <div className={`px-2`}>
        {Object.keys(data).length === 0 ? (
          <div className="flex justify-center items-center h-full">
            <span>No attributes</span>
          </div>
        ) : (
          Object.entries(data).map(([k, v]) => {
            if (v.toString().length > ATTRIBUTE_MAX_CHARACTERS) return;
            return (
              <div className="flex flex-row gap-1 items-center min-h-5" key={k}>
                <span className={`font-semibold truncate min-w-[40%]`}>{k}</span>
                <span className="ml-auto text-right truncate grow-1 flex items-center">
                  {v !== undefined && (typeof v !== 'object' || Array.isArray(v)) && v != '' ? (
                    <span className="ml-auto text-right truncate">{typeof v === 'number' ? formatNumber(v) : v.toString()}</span>
                  ) : (
                    <div
                      className={`ml-auto mt-auto h-4 w-12 border-[1px] solid border-gray`}
                      style={{
                        background:
                          'repeating-linear-gradient(-45deg, transparent, transparent 6px, #eaeaea 6px, #eaeaea 8px), linear-gradient(to bottom, transparent, transparent)',
                      }}
                    ></div>
                  )}
                </span>
              </div>
            );
          })
        )}
      </div>
    </TooltipProvider>
  );
};
