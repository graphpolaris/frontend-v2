import { forceCenter, forceCollide, forceLink, forceManyBody, forceRadial, forceSimulation } from 'd3';
import { GraphType, EdgeType, NodeType } from '../types';

export const simulation = forceSimulation<NodeType, EdgeType>();

/** StartSimulation starts the d3 forcelink simulation. This has to be called after the simulation is initialized. */
export function startSimulation(graph: GraphType, windowSize: { width: number; height: number }): void {
  simulation
    .alpha(2)
    .alphaDecay(0.02)
    .force(
      'link',
      forceLink<NodeType, EdgeType>()
        .id((d: any) => d.id)
        .strength((link, i, links) => (link.mlEdge ? 0.001 : 1))
        .distance(25),
    )
    .force('charge', forceManyBody().strength(-4).distanceMax(200).distanceMin(0))
    .force('center', forceCenter(windowSize.width / 2, windowSize.height / 2).strength(0.02))
    .force('collide', forceCollide().strength(0.2).radius(15).iterations(1)) // Force that avoids circle overlapping
    .force('attract', forceRadial(50, windowSize.width / 2, windowSize.height / 2).strength(0.002));
  simulation.nodes(graph.nodes);
  simulation.force<d3.ForceLink<NodeType, EdgeType>>('link')?.links(graph.edges);
  simulation.alphaTarget(0).restart();
}
