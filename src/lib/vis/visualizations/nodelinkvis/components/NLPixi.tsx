import { Popover, PopoverContent, PopoverTrigger, PopoverProvider } from '@/lib/components/popover';
import { NodeDetails } from '@/lib/components/nodeDetails';
import { useConfig } from '@/lib/data-access/store';
import { Theme } from '@/lib/data-access/store/configSlice';
import { dataColors, visualizationColors } from '@/config';
import { MultiGraph } from 'graphology';
import { Viewport } from 'pixi-viewport';
import {
  Application,
  Container,
  FederatedPointerEvent,
  Graphics,
  PointData,
  RenderTexture,
  Sprite,
  Text,
  Texture,
  type StrokeStyle,
} from 'pixi.js';
import { forwardRef, RefObject, useEffect, useImperativeHandle, useMemo, useRef, useState } from 'react';
import { useML, useSearchResultData } from '../../../../data-access';
import { AllLayoutAlgorithms, GraphologyForceAtlas2Webworker, LayoutFactory, Layouts, LayoutTypes } from '../../../../graph-layout';
import { NodelinkVisProps } from '../nodelinkvis';
import { GraphType, GraphTypeD3, EdgeType, EdgeTypeD3, NodeType, NodeTypeD3 } from '../types';
import { NLPopUp } from './NLPopup';
import { nodeColor, nodeColorHex } from './utils';
import { useAsyncMemo } from '@/utils';
import { ForceEdgeBundling, type Point } from './edgeBundling';
import { canViewFeature } from '@/lib/components/featureFlags';

type Props = {
  onClick: (event?: { node: NodeTypeD3; pos: PointData }) => void;
  // onHover: (data: { node: NodeType; pos: PointData }) => void;
  // onUnHover: (data: { node: NodeType; pos: PointData }) => void;
  highlightNodes: NodeType[];
  configuration: NodelinkVisProps;
  currentShortestPathEdges?: EdgeType[];
  highlightedLinks?: EdgeType[];
  graph: GraphType;
  layoutAlgorithm: LayoutTypes;
  showPopupsOnHover: boolean;
  edgeBundlingEnabled: boolean;
};

type LayoutState = 'reset' | 'running' | 'paused';

//////////////////
// MAIN COMPONENT
//////////////////

let metaEdges: Record<string, EdgeType> | null = null;
export const NLPixi = forwardRef((props: Props, refExternal) => {
  const [quickPopup, setQuickPopup] = useState<{ node: NodeType; pos: PointData } | undefined>();
  const [popups, setPopups] = useState<{ node: NodeTypeD3; pos: PointData }[]>([]);

  const globalConfig = useConfig();

  useEffect(() => {
    update();
  }, [globalConfig.currentTheme]);

  let edgeBundling: Point[][];
  const ref = useRef<HTMLDivElement>(null);
  const canvas = useRef<HTMLCanvasElement>(null);
  const app = useAsyncMemo(async () => {
    if (canvas.current == null) return null;
    const app = new Application();
    await app.init({
      backgroundAlpha: 0,
      antialias: true,
      autoDensity: true,
      autoStart: false, // necessary to prevent white screen bug on chrome/firefox
      eventMode: 'auto',
      resolution: window.devicePixelRatio || 2,
      canvas: canvas.current as HTMLCanvasElement,
    });
    return app;
  }, [canvas]);

  useEffect(() => {
    if (app == null) return;

    setup();
  }, [app]);

  const nodeLayer = useMemo(() => new Container(), []);
  const edgeLabelLayer = useMemo(() => {
    const container = new Container();
    container.alpha = 0;
    container.renderable = false;
    return container;
  }, []);
  const nodeLabelLayer = useMemo(() => {
    const container = new Container();
    container.alpha = 0;
    container.renderable = false;
    return container;
  }, []);

  const nodeMap = useRef(new Map<string, Sprite>());
  const edgeGfx = new Graphics();
  const edgeLabelMap = useRef(new Map<string, Text>());
  const nodeLabelMap = useRef(new Map<string, Text>());
  const viewport = useRef<Viewport>(null);
  const layoutState = useRef<LayoutState>('reset');
  const layoutStoppedCount = useRef(0);
  const mouseInCanvas = useRef<boolean>(false);
  const [dragging, setDragging] = useState<boolean>(false);
  const isSetup = useRef(false);
  const ml = useML();
  const searchResults = useSearchResultData();
  const graph = useRef<GraphTypeD3>({ nodes: [], edges: [] });

  const layoutAlgorithm = useRef(new LayoutFactory().createLayout<AllLayoutAlgorithms>(Layouts.DAGRE));

  // const cull = new Cull();
  // let cullDirty = useRef(true);

  const _glyphTexture = RenderTexture.create({});
  const _selectedTexture = RenderTexture.create({});

  const getTexture = (renderTexture: RenderTexture, selected: boolean = false): Texture => {
    const size = config.NODE_RADIUS * (1 / responsiveScale);
    const lineWidth = selected ? 12 : 6;
    renderTexture.resize(size + lineWidth, size + lineWidth);

    const graphics = new Graphics();

    if (props.configuration.nodes?.shape.type == 'circle') {
      graphics.circle(size / 2 + lineWidth / 2, size / 2 + lineWidth / 2, size / 2);
    } else {
      graphics.rect(lineWidth, lineWidth, size - lineWidth, size - lineWidth);
    }
    graphics.fill({
      color: 0xffffff,
      alpha: 1,
    });

    graphics.stroke({
      width: lineWidth,
      color: 0x4e586a,
    });

    app?.renderer.render({ target: renderTexture, container: graphics });

    return renderTexture as Texture;
  };

  // Pixi viewport zoom scale, but discretized to single decimal.
  const [responsiveScale, setResponsiveScale] = useState(1);

  useEffect(() => {
    if (nodeMap.current.size === 0) return;

    graph.current.nodes.forEach(node => {
      const sprite = nodeMap.current.get(node._id) as Sprite;
      const nodeMeta = props.graph.nodes[node._id];
      sprite.texture = (sprite as any).selected ? selectedTexture : glyphTexture;

      // To calculate the scale, we:
      // 1) Determine the node radius, with a minimum of 5. If not available, we default to 5.
      // 2) Get the ratio with respect to the typical size of the node (divide by NODE_RADIUS).
      // 3) Scale this ratio by the current scale factor.
      let scale = (Math.max(nodeMeta.radius || 5, 5) / config.NODE_RADIUS) * 2;
      scale *= responsiveScale;
      sprite.scale.set(scale, scale);
    });

    if (graph.current.nodes.length > config.LABEL_MAX_NODES) return;

    // Change font size at specific scale intervals
    const fontSize =
      responsiveScale <= 0.1 ? 15 : responsiveScale <= 0.2 ? 22.5 : responsiveScale <= 0.4 ? 30 : responsiveScale <= 0.6 ? 37.5 : 45;

    const strokeWidth = fontSize / 2;
    edgeLabelMap.current.forEach(text => {
      text.style.fontSize = fontSize;
      (text.style.stroke as StrokeStyle).width = strokeWidth;
      text.resolution = Math.ceil(0.5 / responsiveScale);
    });

    nodeLabelMap.current.forEach(text => {
      text.style.fontSize = fontSize * (2 / 3);
      text.resolution = Math.ceil(1 / responsiveScale);
    });

    graph.current.nodes.forEach((node: any) => {
      updateNodeLabel(node);
    });
  }, [responsiveScale, props.configuration.nodes?.shape?.type]);

  const [config, setConfig] = useState({
    width: 1000,
    height: 1000,

    LABEL_MAX_NODES: 1000,

    LAYOUT_ALGORITHM: Layouts.FORCEATLAS2WEBWORKER as LayoutTypes,

    NODE_RADIUS: 70,
    // NODE_BORDER_LINE_WIDTH: 1.0,
    // NODE_BORDER_LINE_WIDTH_SELECTED: 5.0, // if selected and normal width are different the thicker line will be still in the gfx
    // NODE_BORDER_COLOR_DEFAULT: dataColors.neutral[70],
    // NODE_BORDER_COLOR_SELECTED: dataColors.orange[60],

    LINE_COLOR_DEFAULT: dataColors.neutral[40],
    LINE_COLOR_SELECTED: visualizationColors.GPSelected.colors[1],
    LINE_COLOR_ML: dataColors.blue[60],
    LINE_WIDTH_DEFAULT: 0.8,
  });

  const glyphTexture = useMemo(() => {
    if (app == null) return Texture.EMPTY;
    return getTexture(_glyphTexture);
  }, [app, responsiveScale, props.configuration.nodes?.shape?.type]);

  const selectedTexture = useMemo(() => {
    if (app == null) return Texture.EMPTY;
    return getTexture(_selectedTexture, true);
  }, [app, responsiveScale, props.configuration.nodes?.shape?.type]);

  useEffect(() => {
    setConfig(lastConfig => {
      return {
        ...lastConfig,
        LAYOUT_ALGORITHM: props.layoutAlgorithm || lastConfig.LAYOUT_ALGORITHM,
      };
    });
  }, [props.layoutAlgorithm, props.configuration, props.configuration.edgeBundlingEnabled]);

  useEffect(() => {
    if (nodeMap.current.size == 0 || props.graph.edges == null) {
      metaEdges = null;
      return;
    }

    const edgesCopy = JSON.parse(JSON.stringify(props.graph.edges)) as Record<string, EdgeType>;
    metaEdges = Object.fromEntries(
      Object.entries(edgesCopy).map(([key, edge]) => {
        const sourceId = edge.source as string;
        const targetId = edge.target as string;
        const source = nodeMap.current.get(sourceId) as NodeTypeD3 | undefined;
        const target = nodeMap.current.get(targetId) as NodeTypeD3 | undefined;

        edge._source = source;
        edge._target = target;

        return [key, edge];
      }),
    );
  }, [props.graph.edges, nodeMap.current.size]);

  const imperative = useRef<any>(null);

  const mouseClickThreshold = 200; // Time between mouse up and down events that is considered a click, and not a drag.

  useImperativeHandle(imperative, () => ({
    onMouseDown(event: FederatedPointerEvent) {
      if (props.configuration.showPopUpOnHover) return;

      (event as any).mouseDownTimeStamp = event.timeStamp;
      setDragging(true);
    },

    onMouseUpNode(event: FederatedPointerEvent) {
      if (props.configuration.showPopUpOnHover) return;

      // If its a short click (not a drag) on the stage but not on a node: clear the selection and remove all popups.
      const holdDownTime = event.timeStamp - (event as any).mouseDownTimeStamp;
      if (holdDownTime > mouseClickThreshold) {
        return;
      }

      const sprite = event.target as Sprite;
      const node = (sprite as any).node as NodeTypeD3;

      if (event.shiftKey) {
        setPopups([...popups, { node: node, pos: toGlobal(node) }]);
      } else {
        setPopups([{ node: node, pos: toGlobal(node) }]);
        for (const popup of popups) {
          const sprite = nodeMap.current.get(popup.node._id) as Sprite;
          sprite.texture = glyphTexture;
          (sprite as any).selected = false;
        }
      }

      sprite.texture = selectedTexture;
      (sprite as any).selected = true;
      setDragging(false);

      props.onClick({ node: node, pos: toGlobal(node) });

      event.stopPropagation();
    },

    onMouseUpStage(event: FederatedPointerEvent) {
      if (props.configuration.showPopUpOnHover) return;

      // If its a short click (not a drag) on the stage but not on a node: clear the selection and remove all popups.
      const holdDownTime = event.timeStamp - (event as any).mouseDownTimeStamp;
      if (holdDownTime < mouseClickThreshold) {
        for (const popup of popups) {
          const sprite = nodeMap.current.get(popup.node._id) as Sprite;
          sprite.texture = glyphTexture;
          (sprite as any).selected = false;
        }
        setPopups([]);
        props.onClick();
      }
    },

    onHover(event: FederatedPointerEvent) {
      if (!props.configuration.showPopUpOnHover) return;

      const sprite = event.target as Sprite;
      const node = (sprite as any).node as NodeTypeD3;
      if (
        mouseInCanvas.current &&
        viewport?.current &&
        !viewport?.current?.pause &&
        node &&
        popups.filter(p => p.node._id === node._id).length === 0
      ) {
        setQuickPopup({ node: props.graph.nodes[node._id], pos: toGlobal(node) });
      }
    },
    onUnHover() {
      if (!props.configuration.showPopUpOnHover) return;

      setQuickPopup(undefined);
    },
    onMoved(viewport: Viewport) {
      if (props.configuration.showPopUpOnHover) return;

      for (const popup of popups) {
        if (popup.node.x == null || popup.node.y == null) continue;
        popup.pos.x = viewport.position.x + popup.node.x * viewport.scale.x;
        popup.pos.y = viewport.position.y + popup.node.y * viewport.scale.y;
      }
      setPopups([...popups]);
    },
    onZoom() {
      const scale = viewport.current!.scale.x;

      if (scale > 2) {
        const scale = 1 / viewport.current!.scale.x; // starts from 0.5 down to 0.
        setResponsiveScale(scale < 0.05 ? 0.1 : scale < 0.1 ? 0.2 : scale < 0.2 ? 0.4 : scale < 0.3 ? 0.6 : 0.8);
      } else {
        setResponsiveScale(1);
      }

      if (graph.current.nodes.length < config.LABEL_MAX_NODES) {
        edgeLabelLayer.alpha = scale > 2 ? Math.min(1, (scale - 2) * 3) : 0;

        if (edgeLabelLayer.alpha > 0) {
          edgeLabelLayer.renderable = true;
        } else {
          edgeLabelLayer.renderable = false;
        }

        nodeLabelLayer.alpha = scale > 5 ? Math.min(1, (scale - 5) * 3) : 0;
        if (nodeLabelLayer.alpha > 0) {
          nodeLabelLayer.renderable = true;
        } else {
          nodeLabelLayer.renderable = false;
        }
      }
    },

    getEdgeWidth() {
      return props.configuration.edges.width.width || config.LINE_WIDTH_DEFAULT;
    },

    getBackgroundColor() {
      // Colors corresponding to .bg-light class
      return globalConfig.currentTheme === Theme.dark ? 0x121621 : 0xffffff;
    },

    resize() {
      if (app == null) return;

      const width = ref?.current?.clientWidth || 1000;
      const height = ref?.current?.clientHeight || 1000;
      app.renderer.resize(width, height);
      if (viewport.current) {
        viewport.current.screenWidth = width;
        viewport.current.worldWidth = width;
        viewport.current.worldHeight = height;
        viewport.current.screenHeight = height;
      }
    },
    getShowArrows() {
      return props.configuration.showArrows;
    },
    getShowMultipleEdges() {
      return props.configuration.showMultipleEdges;
    },
    getEdgeAttributes() {
      return props.configuration.edges.labelAttributes;
    },
    getNodeAttributes() {
      return props.configuration.nodes.labelAttributes;
    },
    getEdgeBundlingEnabled() {
      return (canViewFeature('EDGE_BUNDLING') && props.configuration.edgeBundlingEnabled) ?? false;
    },
  }));

  useImperativeHandle(refExternal, () => ({
    exportImage() {
      const captureImage = () => {
        const canvas = ref.current?.querySelector('canvas') as HTMLCanvasElement;
        if (canvas) {
          canvas.toBlob(blob => {
            if (blob) {
              const imageUrl = URL.createObjectURL(blob);
              const whiteCanvas = document.createElement('canvas');
              whiteCanvas.width = canvas.width;
              whiteCanvas.height = canvas.height;
              const ctx = whiteCanvas.getContext('2d');
              if (ctx) {
                // Draw a white background
                ctx.fillStyle = 'white';
                ctx.fillRect(0, 0, whiteCanvas.width, whiteCanvas.height);

                // Draw the original canvas image on top
                const img = new Image();
                img.src = imageUrl;
                img.onload = () => {
                  ctx.drawImage(img, 0, 0);

                  // Now export the combined image
                  const finalImage = whiteCanvas.toDataURL('image/png');

                  const link = document.createElement('a');
                  link.href = finalImage;
                  link.download = 'nodelinkvis.png';
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);

                  // Revoke the object URL to free up memory
                  URL.revokeObjectURL(imageUrl);
                };
                img.onerror = err => {
                  console.error('Failed to load image', err);
                };
              } else {
                console.error('2D context not found on the new canvas');
              }
            } else {
              console.error('Failed to convert canvas to Blob');
            }
          }, 'image/png');
        } else {
          console.error('Canvas element not found');
        }
      };

      const renderCanvas = () => {
        requestAnimationFrame(() => {
          captureImage();
        });
      };

      renderCanvas();
    },
  }));

  useEffect(() => {
    if (!ref.current) return;
    const resizeObserver = new ResizeObserver(() => {
      imperative.current?.resize();
    });
    resizeObserver.observe(ref.current.parentElement!.parentElement!.parentElement!);
    return () => {
      if (!isSetup.current) return;
      resizeObserver.disconnect(); // clean up
    };
  }, []);

  function toGlobal(node: NodeTypeD3): PointData {
    if (viewport?.current) {
      // const rect = ref.current?.getBoundingClientRect();
      const rect = { x: 0, y: 0 };
      const x = (rect?.x || 0) + (node.x || 0);
      const y = (rect?.y || 0) + (node.y || 0);
      return viewport.current.toScreen(x, y);
    } else return { x: 0, y: 0 };
  }

  const updateNode = (node: NodeTypeD3) => {
    const gfx = nodeMap.current.get(node._id);
    if (!gfx) return;

    // Update texture when selected
    const nodeMeta = props.graph.nodes[node._id];
    if (nodeMeta == null) return;

    const texture = (gfx as any).selected ? selectedTexture : glyphTexture;
    gfx.texture = texture;

    // Cluster colors
    if (nodeMeta?.cluster) {
      gfx.tint = nodeMeta.cluster >= 0 ? nodeColor(nodeMeta.cluster) : 0x000000;
    } else {
      gfx.tint = nodeColor(nodeMeta.type);
    }

    gfx.position.set(node.x, node.y);

    // if (!item.position) {
    //   item.position = new Point(node.x, node.y);
    // } else {
    //   item.position.set(node.x, node.y);
    // }
    // Update attributes position if they exist
    // if (node.gfxAttributes) {
    //   const x = node.x - node.gfxAttributes.width / 2;
    //   const y = node.y - node.gfxAttributes.height - 20;
    //   if (!node.gfxAttributes?.position) node.gfxAttributes.position = new Point(x, y);
    //   else {
    //     node.gfxAttributes.position.set(x, y);
    //   }
    // }
  };

  const getNodeLabel = (nodeMeta: NodeType) => {
    let attribute;
    try {
      attribute = imperative.current.getNodeAttributes()[nodeMeta.label];
    } catch (e) {
      return nodeMeta.label ?? '';
    }

    if (attribute == 'Default' || attribute == null) {
      return nodeMeta.label ?? '';
    }

    const value = nodeMeta.attributes[attribute];

    if (Array.isArray(value)) {
      return value.join(', ');
    }

    if (typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean') {
      return String(value);
    }

    if (typeof value === 'object' && Object.keys(value).length != 0) {
      return JSON.stringify(value);
    }

    return '-';
  };

  const getEdgeLabel = (edgeMeta: EdgeType) => {
    let attribute;
    try {
      attribute = imperative.current.getEdgeAttributes()[edgeMeta.attributes.type];
    } catch (e) {
      return edgeMeta.attributes.type ?? '';
    }

    if (attribute == 'None') {
      return '';
    }

    if (attribute == 'Default' || attribute == null) {
      return edgeMeta.attributes.type ?? '';
    }

    const value = edgeMeta.attributes[attribute];

    if (Array.isArray(value)) {
      return value.join(', ');
    }

    if (typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean') {
      return String(value);
    }

    if (typeof value === 'object' && Object.keys(value).length != 0) {
      return JSON.stringify(value);
    }

    return '';
  };

  const createNode = (node: NodeTypeD3, selected?: boolean) => {
    const nodeMeta = props.graph.nodes[node._id];

    // check if node is already drawn, and if so, delete it
    if (node && node?._id && nodeMap.current.has(node._id)) {
      nodeMap.current.delete(node._id);
    }
    // Do not draw node if it has no position
    if (node.x === undefined || node.y === undefined) return;

    const texture = glyphTexture;
    const sprite = new Sprite(texture);

    sprite.tint = nodeColor(nodeMeta.type);
    const scale = (Math.max(nodeMeta.radius || 5, 5) / config.NODE_RADIUS) * 2;
    sprite.scale.set(scale, scale);
    sprite.anchor.set(0.5, 0.5);
    sprite.cullable = true;

    sprite.eventMode = 'static';
    sprite.on('mousedown', e => imperative.current?.onMouseDown(e));
    sprite.on('mouseup', e => imperative.current?.onMouseUpNode(e));
    sprite.on('mouseover', e => imperative.current?.onHover(e));
    sprite.on('mouseout', e => imperative.current?.onUnHover(e));

    nodeMap.current.set(node._id, sprite);
    nodeLayer.addChild(sprite);

    updateNode(node);
    (sprite as any).node = node;

    // Node label
    const attribute = getNodeLabel(nodeMeta);
    const text = new Text({
      text: attribute,
      style: {
        fontSize: 20,
        fill: 0xffffff,
        wordWrap: true,
        breakWords: true,
        wordWrapWidth: config.NODE_RADIUS + 5,
        align: 'center',
      },
    });
    text.eventMode = 'none';
    text.cullable = true;
    text.anchor.set(0.5, 0.5);
    text.scale.set(0.1, 0.1);
    nodeLabelMap.current.set(node._id, text);
    nodeLabelLayer.addChild(text);

    updateNodeLabel(node);

    return sprite;
  };

  const createEdgeLabel = (edge: EdgeTypeD3) => {
    // check if edge is already drawn, and if so, delete it
    if (edge && edge?._id && edgeLabelMap.current.has(edge._id)) {
      edgeLabelMap.current.delete(edge._id);
    }

    const edgeMeta = metaEdges?.[edge._id];
    if (edgeMeta == null) return;

    const label = getEdgeLabel(edgeMeta);
    const text = new Text({
      text: label,
      style: {
        fontSize: 60,
        fill: config.LINE_COLOR_DEFAULT,
        stroke: {
          color: imperative.current.getBackgroundColor(),
          width: 30,
        },
      },
    });
    text.cullable = true;
    text.anchor.set(0.5, 0.5);
    text.scale.set(0.1, 0.1);
    edgeLabelMap.current.set(edge._id, text);
    edgeLabelLayer.addChild(text);

    updateEdgeLabel(edge);

    return text;
  };

  const updateEdge = (edge: EdgeTypeD3, edgeBundle?: Point[]) => {
    const multiple = imperative.current.getShowMultipleEdges()
      ? graph.current.edges.filter(
          x => (x.source == edge.source && x.target == edge.target) || (x.source == edge.target && x.target == edge.source),
        ).length
      : 0;

    const edgeMeta = metaEdges?.[edge._id];
    if (edgeMeta == null) return;

    const { style, color, alpha } = getEdgeStyle(edgeMeta);

    const sx = edgeMeta._source!.x as number;
    const sy = edgeMeta._source!.y as number;
    let tx = edgeMeta._target!.x as number;
    let ty = edgeMeta._target!.y as number;

    const arrow = imperative.current.getShowArrows();
    let ax, ay;
    // If drawing arrows, draw the line to the edge of the node, not to the middle.
    if (arrow) {
      ax = tx - sx;
      ay = ty - sy;
      const amag = Math.sqrt(ax * ax + ay * ay);
      ax /= amag;
      ay /= amag;

      const nodeRadius = 5;
      tx -= (nodeRadius / amag) * (tx - sx);
      ty -= (nodeRadius / amag) * (ty - sy);
    }

    // Draw the edge
    // - Self-loops
    if (edge.source === edge.target && edgeMeta._target!.x != null && edgeMeta._target!.y != null) {
      const selfLoopSize = 30;
      edgeGfx
        .moveTo(edgeMeta._source!.x || 0, edgeMeta._source!.y || 0)
        .bezierCurveTo(
          edgeMeta._target!.x - selfLoopSize,
          edgeMeta._target!.y - selfLoopSize,
          edgeMeta._target!.x + selfLoopSize,
          edgeMeta._target!.y - selfLoopSize,
          edgeMeta._target!.x,
          edgeMeta._target!.y,
          0.9,
        )
        .stroke({
          width: style,
          color: color,
          alpha: alpha,
        });
      return;
    }

    // - Regular edge
    if (edgeBundle != null) {
      edgeGfx.moveTo(edgeBundle[0].x, edgeBundle[0].y);

      edgeBundle.forEach(p => {
        edgeGfx.lineTo(p.x, p.y);
      });

      edgeGfx.stroke({
        width: style,
        color: color,
        alpha: alpha,
      });
    } else if (imperative.current.getShowMultipleEdges() && multiple > 1) {
      // Perpendicular vector
      let px = ty - sy;
      let py = -(tx - sx);

      // Normalize
      const pmag = Math.sqrt(px * px + py * py);
      px /= pmag;
      py /= pmag;

      for (let i = 0; i < multiple; i++) {
        const offsetSize = 5;
        const step = i / (multiple - 1) - 0.5;

        // Offset
        const ox = step * px * offsetSize;
        const oy = step * py * offsetSize;

        edgeGfx
          .moveTo(sx + ox, sy + oy)
          .lineTo(tx + ox, ty + oy)
          .stroke({
            width: style,
            color: color,
            alpha: alpha,
          });
      }
    } else {
      edgeGfx.moveTo(sx, sy).lineTo(tx, ty).stroke({
        width: style,
        color: color,
        alpha: alpha,
      });
    }

    // - Draw arrow heads
    if (arrow && ax != null && ay != null) {
      const arrowSize = 2;
      const arrowRatio = 2;

      let px = ty - sy;
      let py = -(tx - sx);
      const pmag = Math.sqrt(px * px + py * py);
      px /= pmag;
      py /= pmag;

      // -- Arrow head line 1
      const arrow1_x = px - ax * arrowRatio;
      const arrow1_y = py - ay * arrowRatio;

      edgeGfx
        .moveTo(tx, ty)
        .lineTo(tx + arrow1_x * arrowSize, ty + arrow1_y * arrowSize)
        .stroke({
          width: style,
          color: color,
          alpha: alpha,
        });

      // -- Arrow head line 2
      const arrow2_x = -px - ax * arrowRatio;
      const arrow2_y = -py - ay * arrowRatio;

      edgeGfx
        .moveTo(tx, ty)
        .lineTo(tx + arrow2_x * arrowSize, ty + arrow2_y * arrowSize)
        .stroke({
          width: style,
          color: color,
          alpha: alpha,
        });
    }
  };

  const getEdgeStyle = (edgeMeta: EdgeType) => {
    // let color = edge.color || 0x000000;
    let color = config.LINE_COLOR_DEFAULT;
    let style = imperative.current.getEdgeWidth();
    let alpha = edgeMeta.alpha || 1;
    if (edgeMeta.mlEdge) {
      color = config.LINE_COLOR_ML;
      if (edgeMeta.value > ml.communityDetection.jaccard_threshold) {
        style = edgeMeta.value * 1.8;
      } else {
        style = 0;
        alpha = 0.2;
      }
    } else if (props.highlightedLinks && props.highlightedLinks.includes(edgeMeta)) {
      if (edgeMeta.mlEdge && ml.communityDetection.jaccard_threshold) {
        if (edgeMeta.value > ml.communityDetection.jaccard_threshold) {
          color = dataColors.magenta[50];
          // 0xaa00ff;
          style = edgeMeta.value * 1.8;
        }
      } else {
        color = dataColors.red[70];
        // color = 0xff0000;
        style = 1.0;
      }
    } else if (props.currentShortestPathEdges && props.currentShortestPathEdges.includes(edgeMeta)) {
      color = dataColors.green[50];
      // color = 0x00ff00;
      style = 3.0;
    }

    // Conditional alpha for search results
    if (searchResults.nodes.length > 0 || searchResults.edges.length > 0) {
      // FIXME: searchResults.edges should be a hashmap to improve performance.
      const isLinkInSearchResults = searchResults.edges.some(resultEdge => resultEdge.id === false); //FIXME: needs to match edge._id
      alpha = isLinkInSearchResults ? 1 : 0.05;
    }

    return { style, color, alpha };
  };

  const updateEdgeLabel = (edge: EdgeTypeD3) => {
    if (graph.current.nodes.length > config.LABEL_MAX_NODES || viewport.current!.scale.x < 2) return;

    const text = edgeLabelMap.current.get(edge._id);
    if (!text) return;

    const _source = edge.source;
    const _target = edge.target;

    if (!_source || !_target) {
      return;
    }

    const source = nodeMap.current.get(edge.source as string) as Sprite;
    const target = nodeMap.current.get(edge.target as string) as Sprite;

    const edgeMeta = metaEdges?.[edge._id];
    if (edgeMeta == null) return;

    text.text = getEdgeLabel(edgeMeta);

    text.x = (source.x + target.x) / 2;
    text.y = (source.y + target.y) / 2;

    const length = Math.hypot(target.x - source.x, target.y - source.y);

    // Skip rendering labels on very short edges
    if (length < text.width + 10) {
      // 10 to account for size of node
      text.alpha = 0;
      return;
    } else {
      text.alpha = 1;
    }

    const rads = Math.atan2(target.y - source.y, target.x - source.x);
    text.rotation = rads;

    const degrees = Math.abs(text.angle % 360);

    // Rotate edge labels to always be legible
    if (degrees > 90 && degrees < 270) {
      text.rotation = rads + Math.PI;
    } else {
      text.rotation = rads;
    }

    (text.style.stroke as StrokeStyle).color = imperative.current.getBackgroundColor();
  };

  const updateNodeLabel = (node: NodeTypeD3) => {
    if (graph.current.nodes.length > config.LABEL_MAX_NODES || viewport.current!.scale.x < 2) return;
    const text = nodeLabelMap.current.get(node._id) as Text | undefined;
    if (text == null) return;

    if (node.x) text.x = node.x;
    if (node.y) text.y = node.y;

    const nodeMeta = props.graph.nodes[node._id];
    const originalText = getNodeLabel(nodeMeta);

    text.text = originalText; // This is required to ensure the text size check (next line) works

    if (text.width / text.scale.x <= 90 && text.height / text.scale.y <= 90) {
      text.text = originalText;
    } else {
      // Change character limit at specific scale intervals
      const charLimit = responsiveScale > 0.2 ? 15 : responsiveScale > 0.1 ? 30 : 75;
      text.text = `${originalText.slice(0, charLimit)}…`;
    }

    text.alpha = text.width / text.scale.x <= 90 && text.height / text.scale.y <= 90 ? 1 : 0;
  };

  // const text = edgeLabelMap.current.get(edge._id);
  //   if (!text) return;

  //   const source = edge.source as NodeTypeD3;
  //   const target = edge.target as NodeTypeD3;

  //   if (source.x == null || source.y == null || target.x == null || target.y == null) return;

  //   text.x = (source.x + target.x) / 2;
  //   text.y = (source.y + target.y) / 2;

  //   const rads = Math.atan2(target.y - source.y, target.x - source.x);
  //   const degrees = Math.abs(text.angle % 360);

  //   // Rotate edge labels to always be legible
  //   if (degrees > 90 && degrees < 270) {
  //     text.rotation = rads + Math.PI;
  //   } else {
  //     text.rotation = rads;
  //   }

  useEffect(() => {
    return () => {
      if (!isSetup.current) return;

      nodeMap.current.clear();
      edgeLabelMap.current.clear();
      app?.destroy();

      const layout = layoutAlgorithm.current as GraphologyForceAtlas2Webworker;
      if (layout?.cleanup != null) layout.cleanup();
    };
  }, []);

  useEffect(() => {
    if (props.graph && ref.current && ref.current.children.length > 0 && imperative.current) {
      if (isSetup.current) update(false);
    }
  }, [config, globalConfig.currentTheme]);

  useEffect(() => {
    if (props.graph) {
      graph.current.nodes.forEach(node => {
        const gfx = nodeMap.current.get(node._id);
        if (!gfx) return;
        const isNodeInSearchResults = searchResults.nodes.some(resultNode => resultNode.id === node._id);

        gfx.alpha = isNodeInSearchResults || searchResults.nodes.length === 0 ? 1 : 0.05;
      });
    }
  }, [searchResults]);

  const tick = () => {
    if (app == null) return;
    if (layoutState.current === 'paused') {
      if (edgeBundling == null && imperative.current?.getEdgeBundlingEnabled()) {
        edgeBundling = ForceEdgeBundling()
          .nodes(
            graph.current.nodes.reduce((a, b) => {
              return { ...a, [b._id]: { x: b.x, y: b.y } };
            }, {}),
          )
          // @ts-expect-error - edgeBundling is not null
          .edges(graph.current.edges)();
      } else {
        return;
      }
    }
    if (layoutState.current === 'reset') layoutStoppedCount.current = 0;

    if (props.graph) {
      if (!layoutAlgorithm.current) return;

      let stopped = 0;

      const widthHalf = app.renderer.width / 2;
      const heightHalf = app.renderer.height / 2;
      graph.current.nodes.forEach((node, i) => {
        const gfx = nodeMap.current.get(node._id);
        if (!gfx || node.x === undefined || node.y === undefined) {
          stopped += 1;
          return;
        }

        const position = layoutAlgorithm.current.getNodePosition(node._id);

        if (!position || Math.abs(node.x - position.x - widthHalf) + Math.abs(node.y - position.y - heightHalf) < 5) {
          stopped += 1;
        }

        if (layoutAlgorithm.current.provider === 'Graphology') {
          // this is a dirty hack to fix the graphology layout being out of bounds
          node.x = position.x + widthHalf;
          node.y = position.y + heightHalf;
        } else {
          node.x = position.x;
          node.y = position.y;
        }

        gfx.position.copyFrom(node as PointData);

        updateNodeLabel(node);
      });

      if (stopped === graph.current.nodes.length) {
        layoutStoppedCount.current = layoutStoppedCount.current + 1;
        if (layoutStoppedCount.current > 500) {
          layoutState.current = 'paused';
          (layoutAlgorithm.current as GraphologyForceAtlas2Webworker)?._layout?.stop();
          console.debug('NL layout paused');
        }
      } else {
        layoutStoppedCount.current = 0;
      }
      if (layoutState.current === 'reset') {
        layoutState.current = 'running';
      }

      // Draw the edges
      edgeGfx.clear();

      if (props.graph != null && nodeMap.current.size !== 0 && metaEdges != null) {
        if (graph.current.edges.length > 2500) {
          // If many many edges, only update edges roughy 1/3th the time, dont draw labels, dont do edge bundling.
          if (Math.random() > 0.3) {
            graph.current.edges.forEach((link, i) => {
              updateEdge(link);
            });
          }
        } else {
          graph.current.edges.forEach((link, i) => {
            if (edgeBundling != null && imperative.current.getEdgeBundlingEnabled()) {
              updateEdge(link, edgeBundling[i]); // FIXME: edgeBundling omits self-loops, index may not always match exactly!
            } else {
              updateEdge(link);
            }
            updateEdgeLabel(link);
          });
        }
      }

      // Move Popovers while layouter is running and moving nodes
      imperative.current?.onMoved(viewport.current);
    }
  };

  const update = (forceClear = false) => {
    if (!props.graph || !ref.current) return;
    if (app == null) return;
    if (!isSetup.current) return;

    if (props.graph) {
      if (forceClear) {
        nodeMap.current.clear();
        edgeGfx.clear();
        nodeLayer.removeChildren();
        edgeLabelLayer.removeChildren();
        nodeLabelLayer.removeChildren();
      }

      nodeMap.current.forEach((gfx, id) => {
        if (!graph.current.nodes.find(node => node._id === id)) {
          nodeLayer.removeChild(gfx);
          gfx.destroy();
          nodeMap.current.delete(id);
        }
      });

      edgeLabelMap.current.forEach((text, id) => {
        if (!graph.current.edges.find(link => link._id === id)) {
          edgeLabelLayer.removeChild(text);
          text.destroy();
          edgeLabelMap.current.delete(id);
        }
      });

      edgeGfx.clear();

      graph.current.nodes.forEach(node => {
        if (!forceClear && nodeMap.current.has(node._id)) {
          const old = nodeMap.current.get(node._id);

          node.x = old?.x || node.x;
          node.y = old?.y || node.y;
          updateNode(node);
          updateNodeLabel(node);
        } else {
          createNode(node);
        }
      });

      if (graph.current.nodes.length < config.LABEL_MAX_NODES) {
        graph.current.edges.forEach(link => {
          if (!forceClear && edgeLabelMap.current.has(link._id)) {
            updateEdgeLabel(link);
          } else {
            createEdgeLabel(link);
          }
        });
      }

      // // update text colour (written after nodes so that text appears on top of nodes)
      //   nodes.forEach((node: NodeType) => {
      //   if (node.gfxAttributes !== undefined) {
      //       const selected = node.selected === true;
      //       node.gfxAttributes.destroy();
      //       createAttributes(node);
      //       if (selected) {
      //       showAttributes(node);
      //       }
      //   }
      //   });

      // force.startSimulation(props.graph, ref.current.getBoundingClientRect());
      // force.simulation.on('tick', () => {});
      layoutState.current = 'reset';
      if (forceClear || layoutAlgorithm.current.algorithm !== config.LAYOUT_ALGORITHM) setupLayout(forceClear);
    }
  };

  /**
   * SetNodeGraphics is an initializing function. It attaches the nodes and links to the simulation.
   * It creates graphic objects and adds these to the PIXI containers. It also clears both of these of previous nodes and links.
   * @param graph The graph returned from the database and that is parsed into a nodelist and edgelist.
   */
  const setup = () => {
    if (app == null || isSetup.current) return;
    nodeLayer.removeChildren();
    edgeLabelLayer.removeChildren();
    app.stage.removeChildren();

    if (!props.graph) throw Error('Graph is undefined');

    //Setup d3 graph structure
    graph.current = {
      nodes: Object.values(props.graph.nodes).map(n => ({ _id: n._id, x: n.defaultX, y: n.defaultY })),
      edges: Object.values(props.graph.edges).map(l => ({
        _id: l.id,
        source: l.source,
        target: l.target,
      })),
    };

    const size = ref.current?.getBoundingClientRect();
    viewport.current = new Viewport({
      screenWidth: size?.width || 1000,
      screenHeight: size?.height || 1000,
      worldWidth: size?.width || 1000,
      worldHeight: size?.height || 1000,
      stopPropagation: true,
      events: app.renderer.events, // the interaction module is important for wheel to work properly when renderer.view is placed or scaled
    });
    app.stage.addChild(viewport.current);
    // activate plugins
    viewport.current.drag().pinch().wheel({ smooth: 2 }).animate({}).decelerate({ friction: 0.75 });

    viewport.current.addChild(edgeGfx);
    viewport.current.addChild(edgeLabelLayer);
    viewport.current.addChild(nodeLayer);
    viewport.current.addChild(nodeLabelLayer);
    viewport.current.on('moved', event => {
      imperative.current.onMoved(event.viewport);
    });
    viewport.current.on('drag-end', _ => {
      setDragging(false);
    });
    viewport.current.on('zoomed', _ => {
      imperative.current.onZoom();
    });

    app.stage.eventMode = 'dynamic';
    app.stage.on('mousedown', e => imperative.current.onMouseDown(e));
    app.stage.on('mouseup', e => imperative.current.onMouseUpStage(e));

    nodeMap.current.clear();
    edgeGfx.clear();

    app.ticker.add(tick);

    // NOTE: this fixes a weird bug that every once in a while results in a white screen in Chrome
    app.ticker.start();

    imperative.current?.resize();

    isSetup.current = true;
    update(true);
  };

  const setupLayout = (forceClear: boolean) => {
    const layoutFactory = new LayoutFactory();
    layoutAlgorithm.current = layoutFactory.createLayout(config.LAYOUT_ALGORITHM);

    if (!layoutAlgorithm) throw Error('LayoutAlgorithm is undefined');

    const graphologyGraph = new MultiGraph();
    graph.current.nodes.forEach(node => {
      if (forceClear) graphologyGraph.addNode(node._id, { size: props.graph.nodes[node._id].radius || 5 });
      else
        graphologyGraph.addNode(node._id, {
          size: props.graph.nodes[node._id].radius || 5,
          x: node.x || 0,
          y: node.y || 0,
        });
    });

    graph.current.edges.forEach(link => {
      graphologyGraph.addEdge(link.source, link.target);
    });
    const boundingBox = { x1: 0, x2: app!.renderer.screen.width, y1: 0, y2: app!.renderer.screen.height };

    if (forceClear) {
      const startingLayout = layoutFactory.createLayout('Graphology_random');
      startingLayout.layout(graphologyGraph, boundingBox);
    }
    layoutAlgorithm.current.layout(graphologyGraph, boundingBox);
  };

  // export image

  return (
    <>
      {popups.map(popup => (
        <Popover key={popup.node._id} open={true} interactive={!dragging} boundaryElement={ref as RefObject<HTMLElement>} showArrow={true}>
          <PopoverTrigger x={popup.pos.x} y={popup.pos.y} />
          <PopoverContent>
            <NodeDetails name={popup.node._id} colorHeader={nodeColorHex(props.graph.nodes[popup.node._id].type)}>
              <NLPopUp data={props.graph.nodes[popup.node._id].attributes} />
            </NodeDetails>
          </PopoverContent>
        </Popover>
      ))}
      {quickPopup != null && (
        <Popover key={quickPopup.node._id} open={true} boundaryElement={ref as RefObject<HTMLElement>} showArrow={true}>
          <PopoverTrigger x={quickPopup.pos.x} y={quickPopup.pos.y} />
          <PopoverContent>
            <NodeDetails name={quickPopup.node._id} colorHeader={nodeColorHex(props.graph.nodes[quickPopup.node._id].type)}>
              <NLPopUp data={props.graph.nodes[quickPopup.node._id].attributes} />
            </NodeDetails>
          </PopoverContent>
        </Popover>
      )}
      <div
        className="h-full w-full overflow-hidden"
        ref={ref}
        onMouseEnter={e => {
          mouseInCanvas.current = true;
        }}
        onMouseOut={e => {
          mouseInCanvas.current = false;
        }}
      >
        <canvas ref={canvas} />
      </div>
    </>
  );
});
