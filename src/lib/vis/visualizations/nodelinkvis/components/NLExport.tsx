import React from 'react';
import jsPDF from 'jspdf';
import { Container, IRenderer } from 'pixi.js';

/** Exports the nodelink diagram as a pdf for downloading. */
async function exportToPDF(renderer: IRenderer, stage: Container): Promise<void> {
  const b = await renderer.extract.canvas(stage).convertToBlob?.({ type: 'image/png' });
  if (!b) return;
  const pdf = new jsPDF();
  if (b) {
    pdf.addImage(URL.createObjectURL(b), 'JPEG', 0, 0, 100, 100, '', 'NONE', 0);
    pdf.save('diagram.pdf');
  } else {
    console.log('null blob in exportToPDF');
  }
}

/** Exports the nodelink diagram as a png for downloading. */
async function exportToPNG(renderer: IRenderer, stage: Container): Promise<void> {
  const b = await renderer.extract.canvas(stage).convertToBlob?.({ type: 'image/png' });
  if (b) {
    const a = document.createElement('a');
    document.body.append(a);
    a.download = 'diagram';
    a.href = URL.createObjectURL(b);
    a.click();
    a.remove();
  } else {
    console.log('null blob in exportToPNG');
  }
}

const useExport = () => {
  return { exportToPDF, exportToPNG };
};
