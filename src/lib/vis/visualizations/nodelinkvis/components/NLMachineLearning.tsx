import { useState } from 'react';
import { GraphType, EdgeType, NodeType } from '../types';
import { ML } from '../../../../data-access/store/mlSlice';

export function processLinkPrediction(ml: ML, graph: GraphType): GraphType {
  if (ml === undefined || ml.linkPrediction === undefined) return graph;

  if (ml.linkPrediction.enabled) {
    const allNodeIds = new Set(Object.keys(graph.nodes));
    ml.linkPrediction.result.forEach(link => {
      if (allNodeIds.has(link.from) && allNodeIds.has(link.to)) {
        const toAdd: EdgeType = {
          id: link.from + ':LP:' + link.to, // TODO: this only supports one link between two nodes
          name: 'Link Prediction',
          source: link.from,
          target: link.to,
          value: link.attributes.jaccard_coefficient as number,
          mlEdge: true,
          color: 0x000000,
          attributes: {},
        };
        graph.edges[toAdd.id] = toAdd;
      }
    });
  }
  return graph;
}

export function processCommunityDetection(ml: ML, graph: GraphType): GraphType {
  if (ml === undefined || ml.communityDetection === undefined) return graph;

  if (ml.communityDetection.enabled) {
    const allNodeIdMap = new Map<string, number>();
    ml.communityDetection.result.forEach((idSet, i) => {
      idSet.forEach(id => {
        allNodeIdMap.set(id, i);
      });
    });

    Object.keys(graph.nodes).forEach(nodeId => {
      if (allNodeIdMap.has(nodeId)) {
        graph.nodes[nodeId].cluster = allNodeIdMap.get(nodeId);
      } else {
        graph.nodes[nodeId].cluster = -1;
      }
    });
  } else {
    Object.keys(graph.nodes).forEach(nodeId => {
      graph.nodes[nodeId].cluster = undefined;
    });
  }
  return graph;
}

export function processML(ml: ML, graph: GraphType): GraphType {
  let ret = processLinkPrediction(ml, graph);
  ret = processCommunityDetection(ml, ret);
  return ret;
}

export const useNLMachineLearning = (props: {
  graph: GraphType;
  highlightedNodes: NodeType[];
  jaccardThreshold: number;
  numberOfMlClusters: number;
}) => {
  const [shortestPathEdges, setShortestPathEdges] = useState<EdgeType[]>([]);

  /**
   * The actual drawing of the shortest path is done in the ticked method
   * This recalculates what should be shown and adds it to a list currentShortestPathEdges
   * Small note; the order in which nodes are clicked matters.
   * Also turns off highlightLinks
   * */
  function showShortestPath(): void {
    const shortestPathNodes: NodeType[] = [];
    props.highlightedNodes.forEach(node => {
      if (node.shortestPathData != undefined) {
        shortestPathNodes.push(node);
      }
    });
    if (shortestPathNodes.length < 2) {
      setShortestPathEdges([]);
    }
    let index = 0;
    let allPaths: EdgeType[] = [];
    while (index < shortestPathNodes.length - 1) {
      const shortestPathData = shortestPathNodes[index].shortestPathData;
      if (shortestPathData === undefined) {
        console.warn('Something went wrong with shortest path calculation');
      } else {
        const path: string[] = shortestPathData[shortestPathNodes[index + 1]._id];
        allPaths = allPaths.concat(getShortestPathEdges(path));
      }
      index++;
    }
    setShortestPathEdges(allPaths);
  }

  /**
   * Gets the edges corresponding to the shortestPath.
   * @param pathString The path as a string.
   * @returns The path as a LinkType[]
   * @deprecated This function is not working anymore
   */
  function getShortestPathEdges(pathString: string[]): EdgeType[] {
    try {
      const newPath: EdgeType[] = [];
      let index = 0;
      while (index < pathString.length) {
        if (pathString[index + 1] == undefined) {
          index++;
          continue;
        }
        const edgeFound = false;
        Object.keys(props.graph.edges).forEach(key => {
          const link = props.graph.edges[key];
          // if (
          //   false // FIXME: This is not working anymore
          //   // (pathString[index] == source.id && pathString[index + 1] == target.id) ||
          //   // (pathString[index] == source && pathString[index + 1] == target) ||
          //   // (pathString[index + 1] == source.id && pathString[index] == target.id) ||
          //   // (pathString[index + 1] == source && pathString[index] == target)
          // ) {
          //   newPath.push(link);
          //   edgeFound = true;
          // }
        });
        if (!edgeFound) {
          console.warn('skipped path: ' + pathString[index] + ' ' + pathString[index + 1]);
        }
        index++;
      }
      return newPath;
    } catch {
      return [];
    }
  }

  //MACHINE LEARNING--------------------------------------------------------------------------------------------------
  //   /**
  //    * updates the JacccardThresh value.
  //    * This is called in the component
  //    * This makes testing purposes easier and makes sure you dont have to read out the value 2000 times,
  //    * but only when you change the value.
  //    */
  //   function updateJaccardThreshHold(): void {
  //     const slider = document.getElementById('Slider');
  //     props.jaccardThreshold = Number(slider?.innerText);
  //   }

  //   /** initializeUniqueAttributes fills the uniqueAttributeValues with data from graph scheme analytics.
  //    * @param attributeData NodeAttributeData returned by graph scheme analytics.
  //    * @param attributeDataType Routing key.
  //    */
  //   function initializeUniqueAttributes(attributeData: AttributeData, attributeDataType: string): void {
  //     if (attributeDataType === 'gsa_node_result') {
  //       const entity = attributeData as NodeAttributeData;
  //       entity.attributes.forEach((attribute) => {
  //         if (attribute.type === AttributeCategory.categorical) {
  //           const nameAttribute = attribute.name;
  //           const valuesAttribute = attribute.uniqueCategoricalValues;
  //           // check if not null
  //           if (valuesAttribute) {
  //             this.uniqueAttributeValues[nameAttribute] = valuesAttribute;
  //           }
  //         }
  //       });
  //     }
  //   }

  /**
   * resetClusterOfNodes is a function that resets the cluster of the nodes that are being customised by the user,
   * after a community detection algorithm, where the cluster of these nodes could have been changed.
   */
  const resetClusterOfNodes = (type: number): void => {
    Object.keys(props.graph.nodes).forEach(key => {
      const node = props.graph.nodes[key];
      if (node.cluster == type) {
        node.cluster = props.numberOfMlClusters;
      }
      if (node.type == type) {
        node.cluster = node.type;
      }
    });
  };

  return {
    shortestPathEdges,
    showShortestPath,
    resetClusterOfNodes,
  };
};
