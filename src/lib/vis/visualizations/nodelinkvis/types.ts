/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

import * as PIXI from 'pixi.js';
import { NodeQueryResult } from 'ts-common';

/** Types for the nodes and links in the node-link diagram. */
export type GraphType = {
  nodes: Record<string, NodeType>; // _id -> node
  edges: Record<string, EdgeType>; // _id -> link
  // linkPrediction?: boolean;
  // shortestPath?: boolean;
  // communityDetection?: boolean;
  // numberOfMlClusters?: number;
};

export type GraphTypeD3 = {
  nodes: NodeTypeD3[];
  edges: EdgeTypeD3[];
};

/** The interface for a node in the node-link diagram */
export interface NodeType extends NodeQueryResult {
  _id: string;

  // Number to determine the color of the node
  label: string;
  type: number;
  attributes: Record<string, any>;
  cluster?: number;
  clusterAccoringToMLData?: number;
  shortestPathData?: Record<string, string[]>;

  // Node that is drawn.
  radius: number;
  // Text to be displayed on top of the node.
  gfxtext?: PIXI.Text;
  gfxAttributes?: PIXI.Graphics;
  selected?: boolean;
  isShortestPathSource?: boolean;
  isShortestPathTarget?: boolean;
  index?: number;

  // The text that will be shown on top of the node if selected.
  displayInfo?: string;
  defaultX?: number;
  defaultY?: number;
}

export type NodeTypeD3 = d3.SimulationNodeDatum & { _id: string };

/** The interface for a link in the node-link diagram */
export type EdgeType = {
  // The thickness of a line
  id: string;
  value: number;
  name: string;
  // To check if an edge is calculated based on a ML algorithm
  mlEdge: boolean;
  color: number;
  alpha?: number;
  source: string;
  target: string;
  _source?: NodeTypeD3;
  _target?: NodeTypeD3;
  attributes: Record<string, any>;
};

export type EdgeTypeD3 = d3.SimulationLinkDatum<NodeTypeD3> & { _id: string };

/**collectionNode holds 1 entry per node kind (so for example a MockNode with name "parties" and all associated attributes,) */
export type TypeNode = {
  name: string; //Collection name
  attributes: string[]; //attributes. This includes all attributes found in the collection
  type: number | undefined; //number that represents collection of node, for colorscheme
  visualizations: Visualization[]; //The way to visualize attributes of this Node kind
};

export type CommunityDetectionNode = {
  cluster: number; //group as used by colouring scheme
};

/**Visualization holds the visualization method for an attribute */
export type Visualization = {
  attribute: string; //attribute type      (e.g. 'age')
  vis: string; //visualization type  (e.g. 'radius')
};

/** possible colors to pick from*/
export type Colors = {
  name: string;
};

/**AssignedColors is a simple holder for color selection  */
export type AssignedColors = {
  collection: number | undefined; //number of the collection (type or group)
  color: string; //color in hex
  default: string; //default color, for easy switching back
};
