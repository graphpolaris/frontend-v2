import React, { useEffect, useRef, useState, forwardRef, useImperativeHandle, useMemo } from 'react';
import { GraphType, EdgeType, NodeType, NodeTypeD3 } from './types';
import { NLPixi } from './components/NLPixi';
import { parseQueryResult } from './components/query2NL';
import { useImmer } from 'use-immer';
import { setShortestPathSource, setShortestPathTarget } from '../../../data-access/store/mlSlice';
import { Layouts, LayoutTypes } from '../../../graph-layout/types';
import { Input } from '@/lib/components/inputs';
import { SettingsContainer } from '@/lib/vis/components/config';
import { EntityPill } from '@/lib/components/pills/Pill';
import { nodeColorHex } from './components/utils';
import { NodeQueryResult, ML } from 'ts-common';
import { type PointData } from 'pixi.js';
import { VisualizationPropTypes, VISComponentType, VisualizationSettingsPropTypes } from '../../common';
import { canViewFeature } from '@/lib/components/featureFlags';

// For backwards compatibility with older saveStates, we migrate information from settings.nodes to settings.location
// FIXME: this can be removed once all systems have updated their saveStates.
function patchLegacySettings(settings: NodelinkVisProps): NodelinkVisProps {
  if (!('nodes' in settings)) {
    settings = JSON.parse(JSON.stringify(settings)); // Undo Object.preventExtensions()

    settings.nodes = {
      shape: {
        type: (settings as any).shapes.shape,
        similar: (settings as any).shapes.similar,
        shapeMap: undefined,
      },
      labelAttributes: {},
    };
    settings.edges.labelAttributes = {};
  }
  return settings;
}

export interface NodeLinkVisHandle {
  exportImageInternal: () => void;
}

export type NodelinkVisProps = {
  id: string;
  name: string;
  layout: LayoutTypes;
  showPopUpOnHover: boolean;
  nodes: {
    shape: {
      similar: boolean;
      type: 'circle' | 'rectangle';
      shapeMap: { [id: string]: 'circle' | 'rectangle' } | undefined;
    };
    labelAttributes: { [nodeType: string]: string };
  };
  edges: {
    width: {
      similar: boolean;
      width: number;
    };
    labelAttributes: { [edgeType: string]: string };
  };
  nodeList: string[];
  showArrows: boolean;
  showMultipleEdges: boolean;
  edgeBundlingEnabled: boolean;
};

const settings: NodelinkVisProps = {
  id: 'NodeLinkVis',
  name: 'NodeLinkVis',
  layout: Layouts.FORCEATLAS2WEBWORKER,
  showPopUpOnHover: false,
  nodes: {
    shape: {
      similar: true,
      type: 'circle',
      shapeMap: undefined,
    },
    labelAttributes: {},
  },
  edges: {
    width: { similar: true, width: 0.8 },
    labelAttributes: {},
  },
  nodeList: [],
  showArrows: false,
  showMultipleEdges: true,
  edgeBundlingEnabled: false,
};

const NodeLinkVis = forwardRef<NodeLinkVisHandle, VisualizationPropTypes<NodelinkVisProps>>(
  ({ data, ml, dispatch, settings, handleSelect }, refExternal) => {
    const ref = useRef<HTMLDivElement>(null);
    const nlPixiRef = useRef<any>(null);
    const [graph, setGraph] = useImmer<GraphType | undefined>(undefined);
    const [highlightNodes, setHighlightNodes] = useState<NodeType[]>([]);
    const [highlightedLinks, setHighlightedLinks] = useState<EdgeType[]>([]);

    settings = patchLegacySettings(settings);

    useEffect(() => {
      if (data) {
        setGraph(parseQueryResult(data, ml));
      }
    }, [data, ml]);

    const onClickedNode = (event?: { node: NodeTypeD3; pos: PointData }, ml?: ML) => {
      if (graph) {
        if (!event?.node) {
          if (handleSelect) handleSelect();
          return;
        }

        const node = event.node;
        const nodeMeta = graph.nodes[node._id];
        if (handleSelect) handleSelect({ nodes: [nodeMeta as NodeQueryResult] });

        if (ml && ml.shortestPath?.enabled) {
          setGraph(draft => {
            const _node = draft?.nodes[node._id];
            if (!_node) return draft;

            if (!ml.shortestPath.srcNode) {
              _node.isShortestPathSource = true;
              dispatch(setShortestPathSource(node._id));
            } else if (ml.shortestPath.srcNode === node._id) {
              _node.isShortestPathSource = false;
              dispatch(setShortestPathSource(undefined));
            } else if (!ml.shortestPath.trtNode) {
              _node.isShortestPathTarget = true;
              dispatch(setShortestPathTarget(node._id));
            } else if (ml.shortestPath.trtNode === node._id) {
              _node.isShortestPathTarget = false;
              dispatch(setShortestPathTarget(undefined));
            } else {
              _node.isShortestPathSource = true;
              _node.isShortestPathTarget = false;
              dispatch(setShortestPathSource(node._id));
              dispatch(setShortestPathTarget(undefined));
            }
            return draft;
          });
        }
      }
    };

    const exportImageInternal = () => {
      nlPixiRef.current.exportImage();
    };

    useImperativeHandle(refExternal, () => ({
      exportImageInternal,
    }));

    if (!graph) return null;

    return (
      <NLPixi
        ref={nlPixiRef}
        graph={graph}
        configuration={settings}
        highlightNodes={highlightNodes}
        highlightedLinks={highlightedLinks}
        onClick={event => {
          onClickedNode(event, ml);
        }}
        layoutAlgorithm={settings.layout}
        showPopupsOnHover={settings.showPopUpOnHover}
        edgeBundlingEnabled={settings.edgeBundlingEnabled}
      />
    );
  },
);

const NodelinkSettings = ({ settings, graphMetadata, updateSettings }: VisualizationSettingsPropTypes<NodelinkVisProps>) => {
  useEffect(() => {
    if (graphMetadata && graphMetadata.nodes && graphMetadata.nodes.labels.length > 0) {
      updateSettings({ nodeList: graphMetadata.nodes.labels });
    }
  }, [graphMetadata]);

  if (!settings.nodeList) return null;

  return (
    <SettingsContainer>
      <div className="mb-4 text-xs">
        <h1 className="font-bold">General</h1>
        <div className="m-1 flex flex-col space-y-2 mb-2">
          <h4 className="font-semibold">Nodes Labels:</h4>
          {settings.nodeList.map((item, index) => (
            <div className="flex m-1 items-center" key={item}>
              <div className="w-3/4 mr-6">
                <EntityPill title={item} />
              </div>
              <div className="w-1/2">
                <div className={`h-5 w-5 border-2 border-sec-300`} style={{ backgroundColor: nodeColorHex(index + 1) }}></div>
              </div>
            </div>
          ))}
        </div>
        <Input
          type="dropdown"
          label="Layout"
          size="sm"
          inline={false}
          value={settings.layout}
          options={Object.values(Layouts) as string[]}
          onChange={val => updateSettings({ layout: val as LayoutTypes })}
        />
        <Input
          type="boolean"
          label="Show pop-up on hover"
          value={settings.showPopUpOnHover}
          onChange={val => updateSettings({ showPopUpOnHover: val })}
        />
      </div>

      <div className="mb-4">
        <h1 className="font-bold">Nodes</h1>
        <div>
          <span className="text-xs font-semibold">Shape</span>
          <Input
            type="dropdown"
            label="Shape"
            value={settings.nodes.shape.type}
            options={[{ circle: 'Circle' }, { rectangle: 'Square' }]}
            onChange={val =>
              updateSettings({
                nodes: {
                  ...settings.nodes,
                  shape: {
                    ...settings.nodes.shape,
                    type: val as 'circle' | 'rectangle',
                  },
                },
              })
            }
          />
        </div>
      </div>

      <div>
        <h1 className="font-bold">Edges</h1>
        <div>
          <span className="text-xs font-semibold">Edge width</span>
          <Input
            type="slider"
            label="Width"
            size="sm"
            className="my-1"
            value={settings.edges.width.width}
            onChangeConfirmed={val => updateSettings({ edges: { ...settings.edges, width: { ...settings.edges.width, width: val } } })}
            min={0.1}
            max={4}
            step={0.1}
          />
        </div>
      </div>
      <div>
        <h1 className="font-bold">Labels</h1>
        {Object.entries(graphMetadata.edges.types).map(([label, type]) => (
          <Input
            type="dropdown"
            size="sm"
            key={label}
            label={label}
            value={settings.edges.labelAttributes ? settings.edges.labelAttributes[label] || 'Default' : undefined}
            options={['Default', 'None', ...Object.keys(type.attributes).filter(x => x != 'Type')]}
            onChange={val =>
              updateSettings({
                edges: {
                  ...settings.edges,
                  labelAttributes: { ...settings.edges.labelAttributes, [label]: val as string },
                },
              })
            }
          />
        ))}
      </div>
      <div>
        <Input type="boolean" label="Show arrows" value={settings.showArrows} onChange={val => updateSettings({ showArrows: val })} />
      </div>
      <div>
        <Input
          type="boolean"
          label="Show multiple edges"
          value={settings.showMultipleEdges}
          onChange={val => updateSettings({ showMultipleEdges: val })}
        />
      </div>
      {canViewFeature('EDGE_BUNDLING') ? (
        <div>
          <Input
            type="boolean"
            label="Edge bundling"
            value={settings.edgeBundlingEnabled}
            onChange={val => updateSettings({ edgeBundlingEnabled: val })}
          />
        </div>
      ) : null}
    </SettingsContainer>
  );
};
const nodeLinkVisRef = React.createRef<{ exportImageInternal: () => void }>();

export const NodeLinkComponent: VISComponentType<NodelinkVisProps> = {
  component: React.forwardRef((props: VisualizationPropTypes<NodelinkVisProps>, ref) => <NodeLinkVis {...props} ref={nodeLinkVisRef} />),
  settingsComponent: NodelinkSettings,
  settings: patchLegacySettings(settings),
  exportImage: () => {
    if (nodeLinkVisRef.current) {
      nodeLinkVisRef.current.exportImageInternal();
    } else {
      console.error('NodeLink reference is not set.');
    }
  },
};

export default NodeLinkComponent;
