import React, { useState, useEffect } from 'react';
import ReactJSONView, { ThemeKeys } from 'react-json-view';
import { VisualizationPropTypes, VISComponentType, VisualizationSettingsPropTypes } from '../../common';
import { SettingsContainer } from '@/lib/vis/components/config';
import { Input } from '@/lib/components/inputs';
import { useConfig } from '@/lib/data-access/store';
import { Theme } from '@/lib/data-access/store/configSlice';

export interface RawJSONVisProps {
  theme: string;
  iconStyle: 'circle' | 'triangle' | 'square' | undefined;
}

const settings: RawJSONVisProps = {
  theme: 'bright:inverted',
  iconStyle: 'circle',
};

export const RawJSONVis = React.memo(({ data, settings }: VisualizationPropTypes) => {
  const config = useConfig();

  const [theme, setTheme] = useState<ThemeKeys>(settings.theme || 'bright:inverted');

  useEffect(() => {
    if (theme.startsWith('bright')) {
      setTheme(config.theme === Theme.dark ? 'bright' : 'bright:inverted');
    }
  }, [config.theme]);

  if (config.theme === Theme.dark && theme === 'bright:inverted') {
    setTheme('bright');
  }

  return (
    <ReactJSONView
      src={data}
      collapsed={1}
      quotesOnKeys={false}
      style={{ padding: '20px', flexGrow: 1 }}
      theme={theme}
      iconStyle={settings.iconStyle}
      enableClipboard={true}
    />
  );
});

const RawJSONSettings = ({ settings, updateSettings }: VisualizationSettingsPropTypes<RawJSONVisProps>) => {
  return (
    <SettingsContainer>
      <Input
        type="dropdown"
        label="Select a theme"
        value={settings.theme}
        options={['bright:inverted', 'monokai', 'ocean']}
        onChange={val => updateSettings({ theme: val as any })}
      />
      <Input
        type="dropdown"
        label="Icon style"
        value={settings.iconStyle}
        options={['circle', 'square', 'triangle']}
        onChange={val => updateSettings({ iconStyle: val as any })}
      />
    </SettingsContainer>
  );
};

export const RawJSONComponent: VISComponentType<RawJSONVisProps> = {
  component: RawJSONVis,
  settingsComponent: RawJSONSettings,
  settings: settings,
  exportImage: () => {
    alert('Not yet supported');
  },
};
export default RawJSONComponent;
