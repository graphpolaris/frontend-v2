import React from 'react';
import { Meta } from '@storybook/react';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { mockData } from '../../../mock-data';
import RawJSONComponent from './rawjsonvis';
import { configSlice } from '@/lib/data-access/store/configSlice';

const Mockstore = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
    config: configSlice.reducer,
  },
});

const Component: Meta<typeof RawJSONComponent.component> = {
  title: 'Visualizations/RawJSONVIS',
  component: RawJSONComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const SimpleData = {
  args: {
    ...(await mockData.smallVillainQueryResults()),
    settings: RawJSONComponent.settings,
  },
};

export const LargeData = {
  args: {
    ...(await mockData.mockLargeQueryResults()),
    settings: RawJSONComponent.settings,
  },
};

export const Empty = {
  args: {
    data: {
      nodes: [],
      edges: [],
    },
    settings: RawJSONComponent.settings,
  },
};

export default Component;
