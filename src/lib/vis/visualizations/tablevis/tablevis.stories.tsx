import { Meta } from '@storybook/react';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { big2ndChamberSchemaRaw, mockData, typesMockSchemaRaw } from '../../../mock-data';
import { simpleSchemaAirportRaw } from '../../../mock-data/schema/simpleAirportRaw';
import TableComponent from './tablevis';

const Mockstore = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
  },
});

const Component: Meta<typeof TableComponent.component> = {
  title: 'Visualizations/TableVis',
  component: TableComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

export const TestWithAirport = {
  args: {
    ...(await mockData.bigMockQueryResults()),
    schema: simpleSchemaAirportRaw,
    settings: {
      ...TableComponent.settings,
      displayAttributes: ['city', 'country', 'lat', 'long', 'name', 'state', 'vip'],
      selectedEntity: 'airports',
    },
  },
};

export const TestWithBig2ndChamber = {
  args: {
    ...(await mockData.big2ndChamberQueryResult()),
    schema: big2ndChamberSchemaRaw,
    settings: {
      ...TableComponent.settings,
      displayAttributes: ['naam', 'anc', 'img', 'leeftijd', 'height', 'partij', 'woonplaats'],
      selectedEntity: 'kamerleden',
    },
  },
};
export const TestWithTypesMock = {
  args: {
    ...(await mockData.typesMockQueryResults()),
    schema: typesMockSchemaRaw,
    settings: {
      ...TableComponent.settings,
      displayAttributes: ['name', 'age', 'height', 'sacked', 'birthdate', 'startingSchedule', 'commutingDuration', 'firstLogin'],
      selectedEntity: 'worker',
    },
  },
};

export const TestUnMatchHeadersData = {
  args: {
    ...(await mockData.testUnMatchHeadersDataResults()),
    schema: typesMockSchemaRaw,
    settings: {
      ...TableComponent.settings,
      displayAttributes: ['name', 'age', 'height'],
      selectedEntity: 'worker',
    },
  },
};

export default Component;
