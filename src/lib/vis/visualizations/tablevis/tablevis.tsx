import { Accordion, AccordionBody, AccordionHead, AccordionItem } from '@/lib/components/accordion';
import { Button } from '@/lib/components/buttons';
import { Input } from '@/lib/components/inputs';
import { EntityPill, RelationPill } from '@/lib/components/pills/Pill';
import { useSearchResultData } from '@/lib/data-access';
import { SettingsContainer } from '@/lib/vis/components/config';
import html2canvas from 'html2canvas';
import React, { forwardRef, useEffect, useImperativeHandle, useMemo, useRef } from 'react';
import { VISComponentType, VisualizationPropTypes, VisualizationSettingsPropTypes } from '../../common';
import { AugmentedNodeAttributes, Table } from './components/Table';
import styles from '../../../components/buttons/buttons.module.scss';

export interface TableVisHandle {
  exportImageInternal: () => void;
}

export type TableProps = {
  id: string;
  name: string;
  showBarPlot: boolean;
  itemsPerPage: number;
  displayAttributes: string[];
  selectedEntity: string;
  maxBarsCount: number;
};

const settings: TableProps = {
  id: 'TableVis',
  name: 'TableVis',
  itemsPerPage: 10,
  showBarPlot: true,
  displayAttributes: [],
  selectedEntity: '',
  maxBarsCount: 10,
};

export const TableVis = forwardRef<TableVisHandle, VisualizationPropTypes<TableProps>>(
  ({ data, schema, settings, graphMetadata }, refExternal) => {
    const searchResults = useSearchResultData();
    const ref = useRef<HTMLDivElement>(null);

    const displayAttributesSorted = useMemo<string[]>(() => {
      if (settings.displayAttributes.length != 0) {
        return [...settings.displayAttributes].sort((a, b) => a.localeCompare(b));
      }
      return settings.displayAttributes;
    }, [settings.displayAttributes]);

    const attributesArray = useMemo<AugmentedNodeAttributes[]>(() => {
      const displayAttributesSorted = [...settings.displayAttributes].sort((a, b) => a.localeCompare(b));

      const nodesLabels = graphMetadata.nodes.labels;

      let dataNodes = [];
      if (nodesLabels.includes(settings.selectedEntity)) {
        dataNodes = (searchResults?.nodes?.length ?? 0) === 0 ? data.nodes : searchResults.nodes;
      } else {
        dataNodes = data.edges;
      }

      return (
        dataNodes
          .filter(node => {
            // some dataset do not have label field
            let labelNode = '';
            if (node.label !== undefined) {
              labelNode = node.label;
            } else {
              const idParts = node._id.split('/');
              labelNode = idParts[0];
            }
            return labelNode === settings.selectedEntity;
          })
          ///.filter((obj) => obj.similarity === undefined || obj.similarity >= similiarityThreshold)
          .map(node => {
            // get attributes filtered and sorted
            const filteredAttributes = Object.fromEntries(
              Object.entries(node.attributes)
                .filter(([attr]) => settings.displayAttributes.includes(attr))
                .sort(([attrA], [attrB]) => settings.displayAttributes.indexOf(attrA) - settings.displayAttributes.indexOf(attrB)),
            );

            // doubled types structure to handle discrepancies in schema object in sb and dev env.

            const types =
              schema.nodes.find((n: any) => {
                const labelNode = node.label;
                return labelNode === n.key;
              })?.attributes?.attributes ??
              schema.nodes.find((n: any) => {
                const labelNode = node.label;

                return labelNode === n.name;
              })?.attributes;

            if (types) {
              return {
                attribute: filteredAttributes,
                type: Object.fromEntries(types.map((t: any) => [t.name, t.type])),
              };
            } else {
              return {
                attribute: filteredAttributes,
                type: {},
              };
            }
          })
      );
    }, [data.nodes, settings.selectedEntity, settings.displayAttributes, searchResults]);

    const exportImageInternal = () => {
      if (ref.current) {
        const clonedElement = ref.current.cloneNode(true) as HTMLDivElement;
        clonedElement.classList.add(styles.exported);

        document.body.appendChild(clonedElement);

        html2canvas(clonedElement).then(canvas => {
          document.body.removeChild(clonedElement);
          const pngData = canvas.toDataURL('image/png');
          const a = document.createElement('a');
          a.href = pngData;
          a.download = 'tablevis.png';
          a.click();
        });
      } else {
        console.error('The referenced div is null.');
      }
    };

    useImperativeHandle(refExternal, () => ({
      exportImageInternal,
    }));

    return (
      <div className="h-full w-full" ref={ref}>
        {attributesArray.length > 0 && (
          <Table
            data={attributesArray}
            itemsPerPage={settings.itemsPerPage}
            showBarPlot={settings.showBarPlot}
            showAttributes={displayAttributesSorted}
            selectedEntity={settings.selectedEntity}
            maxBarsCount={settings.maxBarsCount}
          />
        )}
      </div>
    );
  },
);

const TableSettings = ({ settings, graphMetadata, updateSettings }: VisualizationSettingsPropTypes<TableProps>) => {
  //!TODO: displayAttributes settings are not preserved when loading state

  useEffect(() => {
    if (graphMetadata?.nodes?.labels?.length > 0) {
      if (settings.selectedEntity === '' || !graphMetadata.nodes.labels.includes(settings.selectedEntity))
        updateSettings({ selectedEntity: graphMetadata.nodes.labels[0] });
    } else {
      updateSettings({ selectedEntity: '' });
    }
  }, [graphMetadata]);

  const selectedNodeAttributes = useMemo(() => {
    if (settings.selectedEntity) {
      const labelNodes = graphMetadata.nodes.labels;
      const labelRelationship = graphMetadata.edges.labels;
      if (labelNodes.includes(settings.selectedEntity)) {
        const nodeType = graphMetadata.nodes.types[settings.selectedEntity];

        if (nodeType && nodeType.attributes) {
          return Object.keys(nodeType.attributes).sort((a, b) => a.localeCompare(b));
        }
      } else if (labelRelationship.includes(settings.selectedEntity)) {
        const edgesType = graphMetadata.edges.types[settings.selectedEntity];

        if (edgesType && edgesType.attributes) {
          return Object.keys(edgesType.attributes).sort((a, b) => a.localeCompare(b));
        }
      }
    }
    return [];
  }, [settings.selectedEntity, graphMetadata]);

  useEffect(() => {
    if (graphMetadata && graphMetadata.nodes && graphMetadata.nodes.labels.length > 0) {
      updateSettings({ displayAttributes: selectedNodeAttributes });
    }
  }, [selectedNodeAttributes, graphMetadata]);

  return (
    <SettingsContainer>
      <div className="my-2">
        <Input
          className="w-full text-justify justify-center"
          type="dropdown"
          value={settings.selectedEntity}
          options={[...graphMetadata.nodes.labels, ...graphMetadata.edges.labels]}
          onChange={val => updateSettings({ selectedEntity: val as string })}
          overrideRender={
            graphMetadata.nodes.labels.includes(settings.selectedEntity) ? (
              <EntityPill
                title={
                  <div className="flex flex-row justify-between items-center cursor-pointer">
                    <span>{settings.selectedEntity || ''}</span>
                    <Button variantType="secondary" variant="ghost" size="2xs" iconComponent="icon-[ic--baseline-arrow-drop-down]" />
                  </div>
                }
              />
            ) : graphMetadata.edges.labels.includes(settings.selectedEntity) ? (
              <RelationPill
                title={
                  <div className="flex flex-row justify-between items-center cursor-pointer">
                    <span>{settings.selectedEntity || ''}</span>
                    <Button variantType="secondary" variant="ghost" size="2xs" iconComponent="icon-[ic--baseline-arrow-drop-down]" />
                  </div>
                }
              />
            ) : (
              <span></span>
            )
          }
        ></Input>
        <div className="my-2">
          <Input type="boolean" label="Show barplot" value={settings.showBarPlot} onChange={val => updateSettings({ showBarPlot: val })} />
        </div>
        <div className="my-2">
          <Input
            type="dropdown"
            label="Items per page"
            value={settings.itemsPerPage}
            onChange={val => updateSettings({ itemsPerPage: val as number })}
            options={[10, 25, 50, 100]}
          />
        </div>
        <div className="my-2">
          <Input
            type="number"
            label="Max Bars in Bar Plots"
            value={settings.maxBarsCount}
            onChange={val => updateSettings({ maxBarsCount: val })}
          />
        </div>
        <Accordion>
          <AccordionItem>
            <AccordionHead>
              <span className="text-sm">Attributes to display:</span>
            </AccordionHead>
            <AccordionBody>
              <Input
                type="checkbox"
                value={settings.displayAttributes}
                options={selectedNodeAttributes}
                onChange={(val: string[] | string) => {
                  const updatedVal = Array.isArray(val) ? val : [val];
                  updateSettings({ displayAttributes: updatedVal });
                }}
              />
            </AccordionBody>
          </AccordionItem>
        </Accordion>
      </div>
    </SettingsContainer>
  );
};
const tableRef = React.createRef<{ exportImageInternal: () => void }>();

export const TableComponent: VISComponentType<TableProps> = {
  component: React.forwardRef((props: VisualizationPropTypes<TableProps>, ref) => <TableVis {...props} ref={tableRef} />),
  settingsComponent: TableSettings,
  settings: settings,
  exportImage: () => {
    if (tableRef.current) {
      tableRef.current.exportImageInternal();
    } else {
      console.error('Map reference is not set.');
    }
  },
};

export default TableComponent;
