import React, { useState, useEffect, useRef } from 'react';
import { Pagination } from '@/lib/components/pagination';
import { BarPlot } from '@/lib/components/charts/barplot';
import { Icon } from '@/lib/components';
import { group } from 'd3';
import { NodeAttributes, SchemaAttributeTypes } from 'ts-common';

export type AugmentedNodeAttributes = { attribute: NodeAttributes; type: Record<string, SchemaAttributeTypes> };

export type TableProps = {
  data: AugmentedNodeAttributes[];
  itemsPerPage: number;
  showBarPlot: boolean;
  showAttributes: string[];
  selectedEntity: string;
  maxBarsCount: number;
};
type Data2RenderI = {
  name: string;
  typeData: SchemaAttributeTypes;
  data: { category: string; count: number }[];
  numElements: number;
  showBarPlot?: boolean;
};

const THRESHOLD_WIDTH = 50;
const NODATASTRING = 'NoData';

export const Table = ({ data, itemsPerPage, showBarPlot, showAttributes, selectedEntity, maxBarsCount }: TableProps) => {
  const maxUniqueValues = 29;
  const barPlotNumBins = 10;
  const fetchAttributes = 0;
  const [sortedData, setSortedData] = useState<AugmentedNodeAttributes[]>(data);
  const [sortOrder, setSortOrder] = useState<'asc' | 'desc'>('asc');
  const [sortColumn, setSortColumn] = useState<string | null>(null);
  const [currentPage, setCurrentPage] = useState<{
    page: number;
    startIndex: number;
    endIndex: number;
    currentData: AugmentedNodeAttributes[];
  } | null>(null);
  const [data2Render, setData2Render] = useState<Data2RenderI[]>([]);

  const totalPages = Math.ceil(sortedData.length / itemsPerPage);

  const [columnWidths, setColumnWidths] = useState<number[]>([]);
  const thRefs = useRef<(HTMLTableCellElement | null)[]>([]);

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      const widths = thRefs.current
        .filter(ref => ref !== null)
        .map(ref => {
          if (ref) {
            return ref.getBoundingClientRect().width;
          }
          return 0;
        });
      setColumnWidths(widths);
    }, 100);

    return () => clearTimeout(timeoutId);
  }, [thRefs.current]);

  useEffect(() => {
    if (sortColumn !== null) {
      const sorted = [...data].sort((a, b) => {
        if (sortOrder === 'asc') {
          return (a.attribute as any)[sortColumn] < (b.attribute as any)[sortColumn] ? -1 : 1;
        } else {
          return (a.attribute as any)[sortColumn] > (b.attribute as any)[sortColumn] ? -1 : 1;
        }
      });
      setSortedData(sorted);
    }
  }, [sortOrder, data, sortColumn]);

  useEffect(() => {
    onPageChange(1);
  }, [sortColumn, sortOrder, itemsPerPage]);

  const onPageChange = (page: number) => {
    const startIndex = (page - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const currentData = sortedData.slice(startIndex, endIndex);

    setCurrentPage({
      page: page,
      startIndex: startIndex,
      endIndex: endIndex,
      currentData: currentData,
    });
  };

  const toggleSort = (column: string) => {
    if (sortColumn === column) {
      if (sortOrder === 'asc') {
        setSortOrder('desc');
        setSortedData(sortedData.reverse()); // Reverse the sorted data
      } else if (sortOrder === 'desc') {
        setSortColumn(null);
        setSortOrder('asc');
        setSortedData(data); // Reset to the original order
      }
    } else {
      setSortColumn(column);
      setSortOrder('asc');
      const sorted = [...data].sort((a, b) => {
        return (a.attribute as any)[column] < (b.attribute as any)[column] ? -1 : 1;
      });
      setSortedData(sorted);
    }
  };

  useEffect(() => {
    setSortedData(data);
    setCurrentPage({
      page: 1,
      startIndex: 0,
      endIndex: itemsPerPage,
      currentData: data.slice(0, itemsPerPage),
    });

    // Reset sorting state
    setSortColumn(null);
    setSortOrder('asc');
  }, [data, itemsPerPage]);

  // Barplot on headers data preparation

  // Data structure to feed the keys-barplot ( name data2Render )
  // Three options:
  // 1) numeric->data to histogram barplot
  // 2) string with unique values < maxUniqueValues -> data binned barplot
  // 3) string with unique values > maxUniqueValues -> show text unique values

  useEffect(() => {
    if (!currentPage || currentPage?.currentData?.length <= 0) return;
    // TODO !FIXME: remove firstRowData and get type from schema information
    let categoryCounts = [];
    const firstRowData = data[data.length > fetchAttributes ? fetchAttributes : 0];
    const _data2Render = showAttributes.map((dataColumn: string, i) => {
      const newData2Render: Data2RenderI = {
        name: dataColumn,
        typeData: firstRowData.type[dataColumn] || 'string',
        data: [],
        numElements: 0,
      };
      if (
        firstRowData.type[dataColumn] === 'string' ||
        firstRowData.type[dataColumn] === 'date' ||
        firstRowData.type[dataColumn] === 'duration' ||
        firstRowData.type[dataColumn] === 'datetime' ||
        firstRowData.type[dataColumn] === 'time' ||
        typeof data[0].attribute[dataColumn] === 'string'
      ) {
        const groupedData = group(data, d => {
          const value = d.attribute[dataColumn];
          if (typeof value === 'object' && value !== null) {
            if (Object.keys(value).length === 0) {
              return NODATASTRING;
            } else {
              return value;
            }
          } else {
            if (value === undefined || value === '') {
              return NODATASTRING;
            } else if (Array.isArray(value)) {
              return value.toString();
            } else {
              return value;
            }
          }
        });
        categoryCounts = Array.from(groupedData, ([category, items]) => ({
          category: category as string,
          count: items.length,
        }));

        if (categoryCounts.length > maxUniqueValues || categoryCounts.length <= 1 || !categoryCounts.some(c => c.count > 1)) {
          newData2Render.numElements = categoryCounts.length;
          newData2Render.showBarPlot = false;
        } else {
          newData2Render.numElements = categoryCounts.length;
          newData2Render.data = categoryCounts;
          newData2Render.showBarPlot = true;
        }
        // boolean
      } else if (firstRowData.type[dataColumn] === 'bool') {
        const groupedData = group(data, d => d.attribute[dataColumn]);

        categoryCounts = Array.from(groupedData, ([category, items]) => ({
          category: category as string,
          count: items.length,
        }));

        newData2Render.numElements = categoryCounts.length;
        newData2Render.data = categoryCounts;
        newData2Render.showBarPlot = true;

        // number: float and int
      } else if (firstRowData.type[dataColumn] === 'int' || firstRowData.type[dataColumn] === 'float') {
        const isArrayOfFloats: boolean =
          Array.isArray(data[0].attribute[dataColumn]) && data[0].attribute[dataColumn].every((val: any) => typeof val === 'number');

        if (!isArrayOfFloats) {
          categoryCounts = data.map(obj => ({
            category: 'placeholder', // add something
            count: obj.attribute[dataColumn] as number, // fill values of data
          }));

          newData2Render.numElements = categoryCounts.length;
          newData2Render.data = categoryCounts;
          newData2Render.showBarPlot = true;
        } else {
          const groupedData = group(data, d => (d.attribute[dataColumn] as any)?.[0]);
          categoryCounts = Array.from(groupedData, ([category, items]) => ({
            category: category as string,
            count: items.length,
          }));

          newData2Render.numElements = categoryCounts.length;
          newData2Render.showBarPlot = false;
        }
      } else {
        // there is also array type, when considering labels
        const groupedData = group(data, d => (d.attribute[dataColumn] as any)?.[0]);
        categoryCounts = Array.from(groupedData, ([category, items]) => ({
          category: category as string,
          count: items.length,
        }));

        newData2Render.numElements = categoryCounts.length;
        newData2Render.showBarPlot = false;
      }

      return newData2Render;
    });

    const _data2RenderSorted = _data2Render.sort((a, b) => a.name.localeCompare(b.name));
    setData2Render(_data2RenderSorted);
  }, [currentPage, data, sortedData, selectedEntity, maxBarsCount, showAttributes]);

  return (
    <>
      {currentPage && currentPage?.currentData?.length > 0 && data2Render?.length > 0 && (
        <div className="h-full flex flex-col">
          <div className="w-full relative overflow-x-auto">
            <table className={`mx-auto table-fixed text-sm bg-secondary-100 font-data`}>
              <thead>
                <tr className="p-0 border-0">
                  {showAttributes.map((item, index) => (
                    <th
                      className="px-0 py-0 font-semibold border-light group hover:bg-secondary-300 bg-secondary-200 text-left overflow-x-hidden truncate capitalize cursor-pointer"
                      key={index + item}
                      ref={el => {
                        thRefs.current[index] = el;
                      }}
                      onClick={() => toggleSort(item)}
                    >
                      <div className="flex flex-row max-w-full gap-1 justify-between p-1">
                        <span className="shrink overflow-hidden text-ellipsis">{item}</span>
                        <div
                          className={
                            sortColumn === item ? 'opacity-100 text-primary' : 'opacity-0 group-hover:opacity-100 text-secondary-400'
                          }
                        >
                          <Icon
                            component={
                              sortColumn === item
                                ? sortOrder === 'asc'
                                  ? 'icon-[ic--baseline-arrow-downward]'
                                  : 'icon-[ic--baseline-arrow-upward]'
                                : 'icon-[ic--baseline-sort]'
                            }
                            size={20}
                            className={`${sortColumn === item && sortOrder ? 'text-secondary-800' : 'text-secondary-500'}`}
                          />
                        </div>
                      </div>
                    </th>
                  ))}
                </tr>
                <tr>
                  {showAttributes.map((item, index) => (
                    <th className="border-light bg-light max-w-[20rem] border-r-2 text-left" key={index}>
                      <div className="th p-0" key={index + item}>
                        <div className="h-full w-full overflow-hidden">
                          {data2Render[index] &&
                            (showBarPlot && data2Render[index].showBarPlot && columnWidths[index] > THRESHOLD_WIDTH ? (
                              data2Render[index]?.typeData === 'int' || data2Render[index]?.typeData === 'float' ? (
                                <BarPlot
                                  typeBarPlot="numerical"
                                  numBins={barPlotNumBins}
                                  data={data2Render[index].data}
                                  marginPercentage={{ top: 0.1, right: 0, left: 0, bottom: 0 }}
                                  className="h-[4rem] max-h-[4rem]"
                                  name={data2Render[index].name}
                                />
                              ) : (
                                <BarPlot
                                  typeBarPlot="categorical"
                                  numBins={barPlotNumBins}
                                  data={data2Render[index].data}
                                  marginPercentage={{ top: 0.1, right: 0, left: 0, bottom: 0 }}
                                  className="h-[4rem] max-h-[4rem]"
                                  maxBarsCount={maxBarsCount}
                                  name={data2Render[index].name}
                                />
                              )
                            ) : (
                              <div className="font-normal mx-auto flex flex-row items-start justify-center w-full gap-1 text-center text-secondary-700 p-1">
                                <div className="flex items-center space-x-1 whitespace-nowrap">
                                  <span className="text-2xs">Unique values:</span>
                                  <span className="text-xs font-medium">{data2Render[index]?.numElements}</span>
                                </div>
                              </div>
                            ))}
                        </div>
                      </div>
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {currentPage.currentData.map((item, rowIndex) => (
                  <tr key={rowIndex} className="border-b border-secondary-200 hover:bg-secondary-200">
                    {showAttributes.map((col, colIndex) => {
                      const isEmpty =
                        item.attribute[col] === undefined ||
                        ((typeof item.attribute[col] !== 'object' || Array.isArray(item.attribute[col])) &&
                          (item.attribute[col] as any).toString().trim() === '') ||
                        (typeof item.attribute[col] === 'object' &&
                          item.attribute[col] !== null &&
                          Object.keys(item.attribute[col] as object).length === 0);

                      let style = {};
                      let className = `px-4 py-2 ${isEmpty ? '' : 'border-light'} px-1.5 py-1.5 m-0 overflow-x-hidden truncate max-w-[20rem] border-r-2 text-left`;

                      if (item.type[col] === 'string') {
                        className += ' text-left overflow-ellipsis overflow-hidden';
                      }
                      if (item.type[col] === 'bool') {
                        className += ' text-center';
                      }

                      if (item.type[col] === 'int' || item.type[col] === 'float') {
                        className += ' text-right';
                      }

                      if (isEmpty) {
                        className += ' border-[1px] solid border-white';
                        style = {
                          background:
                            'repeating-linear-gradient(-45deg, transparent, transparent 6px, #eaeaea 6px, #eaeaea 8px), linear-gradient(to bottom, transparent, transparent)',
                        };
                      } else {
                        className += ' border-light';
                      }

                      return (
                        <td className={className} key={colIndex} style={style}>
                          {!isEmpty ? (item.attribute?.[col] as any)?.toString() : ''}
                        </td>
                      );
                    })}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <Pagination
            currentPage={currentPage.page}
            totalPages={totalPages}
            onPageChange={onPageChange}
            itemsPerPageInput={itemsPerPage}
            numItemsArrayReal={currentPage.startIndex + currentPage.currentData.length}
            totalItems={sortedData.length}
            className="mt-auto"
          />
        </div>
      )}
    </>
  );
};

export default Table;
