import React, { useEffect, useRef, useState, useImperativeHandle, forwardRef } from 'react';
import { useImmer } from 'use-immer';
import { GraphQueryResult } from '../../../data-access/store';
import { LinkType, NodeType } from './types';
import { MatrixPixi } from './components/MatrixPixi';
import { VisualizationPropTypes, VISComponentType, VisualizationSettingsPropTypes } from '../../common';
import { Input } from '@/lib/components/inputs';
import { SettingsContainer } from '@/lib/vis/components/config';

export interface MatrixVisProps {
  marks: string;
  color: string;
}

const settings: MatrixVisProps = {
  marks: 'rect',
  color: 'blue',
};

export interface MatrixVisHandle {
  exportImageInternal: () => void;
}

const MatrixVis = forwardRef<MatrixVisHandle, VisualizationPropTypes<MatrixVisProps>>(({ data, ml, settings }, refExternal) => {
  const ref = useRef<HTMLDivElement>(null);
  const [graph, setGraph] = useImmer<GraphQueryResult | undefined>(undefined);
  const [highlightNodes, setHighlightNodes] = useState<NodeType[]>([]);
  const [highlightedLinks, setHighlightedLinks] = useState<LinkType[]>([]);

  const matrixPixiRef = useRef<any>(null);

  useEffect(() => {
    if (data) {
      setGraph(data);
    }
  }, [data, ml]);

  const exportImageInternal = () => {
    matrixPixiRef.current.exportImage();
  };

  useImperativeHandle(
    refExternal,
    () => ({
      exportImageInternal,
    }),
    [],
  );

  return (
    <>
      <div className="h-full w-full overflow-hidden" ref={ref}>
        <MatrixPixi
          ref={matrixPixiRef}
          graph={graph}
          highlightNodes={highlightNodes}
          highlightedLinks={highlightedLinks}
          settings={settings}
        />
      </div>
    </>
  );
});

const MatrixSettings = ({ settings, updateSettings }: VisualizationSettingsPropTypes<MatrixVisProps>) => {
  return (
    <SettingsContainer>
      <Input
        type="dropdown"
        label="Configure marks"
        value={settings.marks}
        options={['rect', 'circle']}
        onChange={val => updateSettings({ marks: val as string })}
      />
    </SettingsContainer>
  );
};

const matrixVisRef = React.createRef<{ exportImageInternal: () => void }>();

export const MatrixVisComponent: VISComponentType<MatrixVisProps> = {
  component: React.forwardRef((props: VisualizationPropTypes<MatrixVisProps>, ref) => <MatrixVis {...props} ref={matrixVisRef} />),
  settingsComponent: MatrixSettings,
  settings: settings,
  exportImage: () => {
    if (matrixVisRef.current) {
      matrixVisRef.current.exportImageInternal();
    } else {
      console.error('MatrixVis reference is not set.');
    }
  },
};

export default MatrixVisComponent;
