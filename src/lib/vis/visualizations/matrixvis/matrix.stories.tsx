import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { mockData } from '../../../mock-data';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import MatrixVisComponent from './matrixvis';
import { configSlice } from '@/lib/data-access/store/configSlice';

const Component: Meta<typeof MatrixVisComponent.component> = {
  title: 'Visualizations/MatrixVis',
  component: MatrixVisComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

const Mockstore: any = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
    config: configSlice.reducer,
  },
});

export const TestWithData = {
  layout: 'fullscreen',
  args: {
    ...(await mockData.smallVillainQueryResults()),
    ml: {},
    settings: MatrixVisComponent.settings,
  },
};

export const TestWithNoData = {
  args: {
    data: {
      nodes: [],
      edges: [],
    },
    ml: {},
    settings: MatrixVisComponent.settings,
  },
};

export const TestWithRecommendationPersonActedInMovieQueryResult = {
  args: {
    ...(await mockData.movie_recommendationPersonActedInMovieQueryResult()),
    ml: {},
    settings: MatrixVisComponent.settings,
    dispatch: () => {},
  },
};

/*
export const TestWithBig2ndChamber = {
  args: {
    ...(await mockData.big2ndChamberQueryResult()),
    ml: {},
    settings: MatrixVisComponent.settings,
  },
};

export const TestWithSmallFlights = {
  args: {
    ...(await mockData.smallFlightsQueryResults()),
    ml: {},
    settings: MatrixVisComponent.settings,
  },
};

export const TestWithLargeQueryResult = {
  args: {
    ...(await mockData.mockLargeQueryResults()),
    ml: {},
    settings: MatrixVisComponent.settings,
  },
};
*/

export default Component;
