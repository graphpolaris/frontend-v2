import { Text } from 'pixi.js';

export class TextLabel extends Text {
  static readonly RANDOM_SCALE_FACTOR = 10.0;

  constructor(label: string, config: any) {
    super(label);

    // this.style = { fontSize: this.finalTextSize * this.bigRandomScaleFactor, fill: 0x000000, align: 'center' };
    this.style = {
      fontSize: config.LABEL_FONT_SIZE * TextLabel.RANDOM_SCALE_FACTOR,
      fontFamily: config.LABEL_FONT_FAMILY,
      fill: config.LABEL_FILL,
      align: config.LABEL_ALIGN,
    };
    this.scale.x = 1 / TextLabel.RANDOM_SCALE_FACTOR;
    this.scale.y = 1 / TextLabel.RANDOM_SCALE_FACTOR;
    this.rotation = (Math.PI * 3) / 2;
    this.eventMode = 'none';
    this.name = 'Text_' + label;
    this.cacheAsBitmap = true;

    //   // from the PixiJS documention: Setting a text object's scale to > 1.0 will result in blurry/pixely display,
    //   // because the text is not re-rendered at the higher resolution needed to look sharp -
    //   // it's still the same resolution it was when generated. To deal with this, you can render at a higher
    //   // initial size and down-scale, instead.
    //   const basicText = new Text(columnOrder[j], { fontSize: finalTextSize * bigRandomScaleFactor, fill: 0x000000, align: 'center' });
    //   basicText.position.set(j * config.cellWidth + config.cellWidth / 4, config.textOffsetY - 5);
    //   basicText.scale.set(finalTextSize / (bigRandomScaleFactor * bigRandomScaleFactor));
    //   basicText.rotation = (Math.PI * 3) / 2;
    //   basicText.eventMode = 'none';
    //   basicText.name = 'Text_' + columnOrder[j];
  }
}
