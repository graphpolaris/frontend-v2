import { BitmapText } from 'pixi.js';

export class BitmapTextLabel extends BitmapText {
  static readonly LABEL_FONT_FAMILY = 'HelveticaRegular';
  static readonly LABEL_FONT_SIZE = 12;
  static readonly LABEL_COLOR = 0x333333;

  constructor(label: string) {
    super(label, {
      fontName: BitmapTextLabel.LABEL_FONT_FAMILY,
      fontSize: BitmapTextLabel.LABEL_FONT_SIZE,
      align: 'right',
      tint: BitmapTextLabel.LABEL_COLOR,
    });

    this.rotation = (Math.PI * 3) / 2;
    this.eventMode = 'none';
    this.name = 'Text_' + label;
  }
}
