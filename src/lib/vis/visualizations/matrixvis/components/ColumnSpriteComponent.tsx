import { Edge, Node } from '@/lib/data-access';
import { dataColors, tailwindColors } from '@/config';
import { Sprite, Texture } from 'pixi.js';

export const createColumn = (
  id: number,
  nodesCol: Node[],
  nodesRow: Node[],
  edges: Edge[],
  colorScale: any,
  cellWidth: number,
  cellHeight: number,
) => {
  const sprite = new Sprite(Texture.WHITE);
  sprite.name = 'col_' + id;
  sprite.eventMode = 'static';
  // button.eventMode = 'static';
  sprite.cursor = 'pointer';
  const edgesForThisColumn = edges.filter(edge => {
    return edge.from === nodesCol[id]._id || edge.to === nodesCol[id]._id;
  });
  const bgCellColor = dataColors.neutral['5'];
  const fgCellColor = tailwindColors.accent.DEFAULT;
  let color = bgCellColor;
  for (let i = 0; i < nodesRow.length; i++) {
    const node = nodesRow[i];
    if (
      edgesForThisColumn.filter(edge => {
        return edge.from === node._id || edge.to === node._id;
      }).length > 0
    ) {
      color = fgCellColor;
    } else {
      color = bgCellColor;
    }
    sprite.tint = color;
    sprite.width = cellWidth;
    sprite.height = cellHeight;
    sprite.position.set(id * cellHeight, i * cellWidth);
    // sprite.acceleration = new Point(0);
    // TODO get
    // gfx.beginFill(colorScale(node.type)));
    // sprite.lineStyle(2, 0xffffff);
    // sprite.drawRect(0, i * cellWidth, cellWidth, cellHeight);
    // sprite.endFill();
  }
  return sprite;
};
