export const ColumnLabelTrack = ({ columnLabels, config }: { columnLabels: string[]; config: any }) => {
  return (
    <>
      <div className="grid grid-flow-col auto-cols-max h-80">
        {columnLabels.map((label, index) => {
          return (
            <div className="-rotate-90 font-data text-sm w-4" key={index}>
              {label}
            </div>
          );
        })}
      </div>
    </>
  );
};
