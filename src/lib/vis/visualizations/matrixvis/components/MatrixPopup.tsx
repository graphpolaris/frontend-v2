import { PointData } from 'pixi.js';
import { NodeType } from '../types';

export type NodelinkPopupProps = {
  data: { node: NodeType; pos: PointData };
  onClose: () => void;
};

export const NLPopup = (props: NodelinkPopupProps) => {
  const node = props.data.node;

  return (
    <div
      className="absolute card card-bordered z-50 bg-white rounded-none text-[0.9rem] min-w-[10rem]"
      // style={{ top: props.data.pos.y + 10, left: props.data.pos.x + 10 }}
      style={{ transform: 'translate(' + (props.data.pos.x + 20) + 'px, ' + (props.data.pos.y + 10) + 'px)' }}
    >
      <div className="card-body p-0">
        <span className="px-2.5 pt-2">
          <span>Node</span>
          <span className="float-right">{node.id}</span>
        </span>
        <div className="h-[1px] w-full bg-offwhite-300"></div>
        <div className="px-2.5 text-[0.8rem]">
          {node.attributes &&
            Object.entries(node.attributes).map(([k, v], i) => {
              return (
                <div key={k} className="flex flex-row gap-3">
                  <span className="">{k}: </span>
                  <span className="ml-auto max-w-[10rem] text-right truncate">
                    <span title={JSON.stringify(v)}>{JSON.stringify(v)}</span>
                  </span>
                </div>
              );
            })}
          {node.cluster && (
            <p>
              Cluster: <span className="float-right">{node.cluster}</span>
            </p>
          )}
        </div>
        <div className="h-[1px] w-full"></div>
      </div>
    </div>
  );
};
