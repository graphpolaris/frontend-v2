import { Edge, GraphQueryResult, Node, useML } from '@/lib/data-access';
import { dataColors, visualizationColors } from '@/config';
import { Viewport } from 'pixi-viewport';
import { Application, ColorSource, Container, FederatedPointerEvent, Graphics, PointData, Point, Text } from 'pixi.js';
import { useEffect, useRef, useState, useMemo, useImperativeHandle, forwardRef } from 'react';
import { LinkType, NodeType } from '../types';
import { NLPopup } from './MatrixPopup';

import { Actions, Interpolations } from 'pixi-actions';

import Color from 'color';
import { createColumn } from './ColumnGraphicsComponent';
import { ReorderingManager } from './ReorderingManager';
import {
  select,
  selectAll,
  range,
  scaleLinear,
  scaleBand,
  type ScaleBand,
  axisTop,
  axisLeft,
  easeCubicOut,
  type Selection,
  type BaseType,
  type Axis,
} from 'd3';
import { MatrixVisProps } from '../matrixvis';
import { Theme } from '@/lib/data-access/store/configSlice';
import { useConfig } from '@/lib/data-access/store';
import html2canvas from 'html2canvas';
import { Target } from 'pixi-actions/dist/actions/TargetedAction';

const styleMatrixSize = 50;

type Props = {
  // onClick: (node: NodeType, pos: PointData) => void;
  // onHover: (data: { node: NodeType; pos: PointData }) => void;
  // onUnHover: (data: { node: NodeType; pos: PointData }) => void;
  highlightNodes: NodeType[];
  currentShortestPathEdges?: LinkType[];
  highlightedLinks?: LinkType[];
  graph?: GraphQueryResult;
  settings: MatrixVisProps;
};

const columnsContainer = new Container();

//////////////////
// MAIN COMPONENT
//////////////////

export const MatrixPixi = forwardRef((props: Props, refExternal) => {
  const config = {
    textOffsetX: 50,
    textOffsetY: 50,
    visMapping: [] as any[], // TODO type

    cellHeight: 100,
    cellWidth: 100,
  };

  const globalConfig = useConfig();

  useEffect(() => {
    if (props.graph && internalRef.current && imperative.current) {
      if (isSetup.current === false) setup();
      else update();
    }
  }, [props.graph, globalConfig.theme]);

  let columnOrder: string[] = [];
  let rowOrder: string[] = [];

  const [quickPopup, setQuickPopup] = useState<{ node: NodeType; pos: PointData } | undefined>();
  const [popups, setPopups] = useState<{ node: NodeType; pos: PointData }[]>([]);
  // const [columnOrder, setColumnOrder] = useState<string[]>([]);

  const viewport = useRef<Viewport>();
  const internalRef = useRef<HTMLDivElement>(null);
  const canvas = useRef<HTMLCanvasElement>(null);
  const svg = useRef<SVGSVGElement>(null);
  const isSetup = useRef(false);
  const ml = useML();

  const app = useMemo(
    () =>
      new Application({
        backgroundAlpha: 0,
        antialias: true,
        autoDensity: true,
        eventMode: 'auto',
        resolution: window.devicePixelRatio || 2,
        view: canvas.current as HTMLCanvasElement,
      }),
    [canvas.current],
  );

  useEffect(() => {
    if (typeof refExternal === 'function') {
      refExternal(internalRef.current);
    } else if (refExternal) {
      (refExternal as React.MutableRefObject<HTMLDivElement | null>).current = internalRef.current;
    }
  }, [refExternal]);

  const imperative = useRef<any>(null);
  useImperativeHandle(imperative, () => ({
    getBackgroundColor() {
      // Colors corresponding to .bg-light class
      return globalConfig.theme === Theme.dark ? 0x121621 : 0xffffff;
    },
  }));
  useImperativeHandle(refExternal, () => ({
    exportImage() {
      const captureImage = () => {
        const element = internalRef.current; // The container that holds both canvas and SVG

        if (element) {
          html2canvas(element, {
            backgroundColor: '#FFFFFF', // Set background color to white
          })
            .then(canvas => {
              const finalImage = canvas.toDataURL('image/png');

              // Download the final image
              const link = document.createElement('a');
              link.href = finalImage;
              link.download = 'matrixvis.png';
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
            })
            .catch(error => {
              console.error('Error capturing image:', error);
            });
        } else {
          console.error('Container element not found');
        }
      };

      const renderCanvas = () => {
        requestAnimationFrame(() => {
          captureImage();
        });
      };

      renderCanvas();
    },
  }));

  function resize() {
    const width = internalRef?.current?.clientWidth || 1000;
    const height = internalRef?.current?.clientHeight || 1000;

    app?.renderer.resize(width, height);
    if (viewport.current) {
      viewport.current.screenWidth = width;
      viewport.current.worldWidth = width;
      viewport.current.worldHeight = height;
      viewport.current.screenHeight = height;
    }

    if (props.graph) {
      setup();
    }
    app?.render();
  }

  useEffect(() => {
    return () => {
      app.destroy();
    };
  }, []);

  useEffect(() => {
    if (!internalRef.current) return;
    const resizeObserver = new ResizeObserver(() => {
      resize();
    });
    resizeObserver.observe(internalRef.current);
    return () => resizeObserver.disconnect();
  }, []);

  // TODO implement search results
  // useEffect(() => {
  //   if (props.graph) {
  //     props.graph.nodes.forEach((node: NodeType) => {
  //       const gfx = nodeMap.current.get(node.id);
  //       if (!gfx) return;
  //       const isNodeInSearchResults = searchResults.nodes.some((resultNode) => resultNode.id === node.id);
  //       gfx.alpha = isNodeInSearchResults || searchResults.nodes.length === 0 ? 1 : 0.05;
  //     });

  //     props.graph.links.forEach((link: LinkType) => {
  //       const gfx = linkMap.current.get(link.id);
  //       if (!gfx) return;
  //       const isLinkInSearchResults = searchResults.edges.some((resultEdge) => resultEdge.id === link.id);
  //       gfx.alpha = isLinkInSearchResults || searchResults.edges.length === 0 ? 1 : 0.05;
  //     });
  //   }
  // }, [searchResults]);

  function onButtonDown(event: FederatedPointerEvent) {
    console.debug(
      event.currentTarget,
      // graph.nodes.find((node) => node.id === event.currentTarget.name)
    );
  }

  const update = (forceClear = false) => {
    setPopups([]);
    if (!props.graph || !internalRef.current) return;

    if (props.graph) {
      if (forceClear) {
        columnsContainer.removeChildren();
      }
    }
  };

  const onHoverColumn = (event: any, currentNode: Node | null | undefined) => {
    if (Actions.actions.length > 0) {
      // Don't hover if reorder animation are running, causes glitches.
      return;
    }

    if (!currentNode) {
      columnsContainer.children.forEach(col => {
        col.alpha = 1;
      });
      selectAll('.axis .tick').style('opacity', 1);

      return;
    }

    columnsContainer.children.forEach(col => {
      col.alpha = 0.25;
    });
    event.currentTarget.alpha = 1;

    if (!props.graph) return;

    const edgesForThisColumn = props.graph.edges.filter(edge => {
      return edge.from === currentNode._id || edge.to === currentNode._id;
    });

    selectAll('.axis.left .tick').style('opacity', d => {
      const highlight = edgesForThisColumn.some(edge => edge.from == d || edge.to == d);
      return highlight ? 1 : 0.25;
    });

    selectAll('.axis.top .tick').style('opacity', d => {
      const highlight = edgesForThisColumn.some(edge => edge.from == d || edge.to == d);
      return highlight ? 1 : 0.25;
    });
  };

  const reorderColumns = (columnOrder: string[]) => {
    if (Actions.actions.length > 0) {
      // Don't reorder if previous animation is still running, causes glitches.
      return;
    }

    // Animate columns
    const columns = columnsContainer.children;
    const columnPositions = columnOrder.map((_, i) => new Point(config.textOffsetX + i * config.cellWidth, columns[i].position.y));

    for (let i = 0; i < columns.length; i++) {
      const column = columns[i];
      if (!column.name) throw new Error('column has no name');
      const index = columnOrder.indexOf(column.name);
      if (index === -1) throw new Error('column not found in columnOrder');

      const newPosition = columnPositions[index];
      Actions.moveTo(columns[i] as Target, newPosition.x, newPosition.y, 1, Interpolations.pow2out).play();
    }

    // Animate column axis
    scaleColumns.domain(columnOrder);
    axisColumns.scale(scaleColumns);

    animatingAxes = true;
    const selectionTop = select(svg.current).select('.axis.top') as Selection<SVGGElement, unknown, BaseType, unknown>;
    selectionTop
      .transition()
      .duration(1000)
      .ease(easeCubicOut)
      .on('end', () => (animatingAxes = false))
      .call(axisColumns);
  };

  const reorderRows = (edges: Edge[], rowOrder: string[], columnOrder: string[]) => {
    if (Actions.actions.length > 0) {
      // Don't reorder if previous animation is still running, causes glitches.
      return;
    }

    // Rearrange rows
    columnsContainer.removeChildren();
    setupColumns(edges, columnOrder, rowOrder);
    const cols = columnOrder.flatMap(x => props.graph?.nodes.filter(n => n._id == x) ?? []);
    setupColumnInteractivity(cols);

    // Animate row axis
    scaleColumns.domain(columnOrder);
    scaleRows.domain(rowOrder);

    animatingAxes = true;
    const selectionLeft = select(svg.current).select('.axis.left') as Selection<SVGGElement, unknown, BaseType, unknown>;
    selectionLeft
      .transition()
      .duration(1000)
      .ease(easeCubicOut)
      .on('end', () => (animatingAxes = false))
      .call(axisRows);
  };

  const setup = () => {
    if (!props.graph) throw Error('Graph is undefined; setup not possible');

    if (svg.current != null) {
      select(svg.current).selectAll('*').remove();
    }

    columnsContainer.removeChildren();
    app.stage.removeChildren();

    const size = internalRef.current?.getBoundingClientRect();
    if (viewport.current == null) {
      viewport.current = new Viewport({
        screenWidth: size?.width || 1000,
        screenHeight: size?.height || 1000,
        worldWidth: size?.width || 1000,
        worldHeight: size?.height || 1000,
        stopPropagation: true,
        events: app.renderer.events, // the interaction module is important for wheel to work properly when renderer.view is placed or scaled
      });
    }
    app.stage.addChild(viewport.current);

    viewport.current.addChild(columnsContainer);

    const groupByType = props.graph.nodes.reduce(
      (group: any, node: Node) => {
        if (!group[node.label]) group[node.label] = [];
        group[node.label].push(node);
        return group;
      },
      {} as { [key: string]: Node[] },
    );

    // order groupByType by size
    const ordered = Object.entries(groupByType).sort((a: any[], b: any[]) => b[1].length - a[1].length);

    let cols = [] as Node[];
    let rows = [] as Node[];
    if (ordered.length == 2) {
      cols = ordered[0][1] as Node[];
      rows = ordered[1][1] as Node[];
    } else if (ordered.length == 1) {
      cols = ordered[0][1] as Node[];
      rows = ordered[0][1] as Node[];
    } else {
      // show text that there are no nodes on the viewport
      const finalTextSize = 25;
      const bigRandomScaleFactor = 10;
      const basicText = new Text('Result is empty', {
        fontSize: finalTextSize * bigRandomScaleFactor,
        fill: 0x000000,
        align: 'center',
      });
      basicText.position.set(viewport.current?.screenWidth / 2 || 1000, viewport.current?.screenHeight / 2 || 1000);
      basicText.scale.set(finalTextSize / (bigRandomScaleFactor * bigRandomScaleFactor));
      basicText.eventMode = 'none';
      viewport.current.addChild(basicText);
      return;
    }

    columnOrder = range(0, cols.length).map(d => cols[d]._id);
    rowOrder = range(0, rows.length).map(d => rows[d]._id);
    const reorderingManager = new ReorderingManager(props.graph);
    const newOrdering = reorderingManager.reorderMatrix('count', columnOrder, rowOrder);
    columnOrder = newOrdering.columnOrder;
    rowOrder = newOrdering.rowOrder;

    config.cellWidth = Math.max((size?.width || 1000) / props.graph.nodes.length, (size?.height || 1000) / props.graph.nodes.length);
    config.cellHeight = config.cellWidth;

    setupVisualizationEncodingMapping(props.settings);

    if (svg.current != null && canvas.current != null) {
      select(svg.current)
        .attr('width', canvas.current.clientWidth)
        .attr('height', canvas.current.clientHeight)
        .style('position', 'absolute')
        .style('top', 0)
        .style('left', 0)
        .style('pointer-events', 'none');
    }

    setupColumns(props.graph.edges, columnOrder, rowOrder);
    setupColumnAxis(columnOrder, cols[0].label);
    setupColumnInteractivity(cols);
    setupRowAxis(rowOrder, rows[0].label);

    console.debug('setup matrixvis with graph:', props.graph);

    // activate plugins
    viewport.current.drag().pinch().wheel().animate({}).decelerate({ friction: 0.75 });

    app.ticker.add(tick);
    isSetup.current = true;

    update();
  };

  let scaleColumns: ScaleBand<string>;
  let scaleRows: ScaleBand<string>;

  const setupVisualizationEncodingMapping = (settings: MatrixVisProps) => {
    if (!props.graph) throw new Error('Graph is undefined; cannot setup matrix');
    const visMapping = []; // TODO type

    const colorNeutralString = globalConfig.theme === Theme.dark ? dataColors.neutral['80'] : dataColors.neutral['15'];
    const colorNeutral = Color(colorNeutralString);

    // make adjacency
    // const adjacenyScale = scaleLinear([dataColors.neutral['5'], tailwindColors.entity.DEFAULT]);
    const adjacenyScale = scaleLinear([colorNeutral, visualizationColors.GPCat.colors[14][1]]);
    visMapping.push({
      attribute: 'adjacency',
      encoding: settings.marks,
      colorScale: adjacenyScale,
      renderFunction: function (i: number, color: ColorSource, gfxContext: Graphics) {
        // Clear locally
        gfxContext.beginFill(imperative.current.getBackgroundColor(), 1);
        gfxContext.drawRect(0, i * config.cellHeight, config.cellWidth, config.cellHeight);
        gfxContext.endFill();

        gfxContext.beginFill(color, 1);
        // TODO get
        // gfx.beginFill(colorScale(node.type)));

        const inset = 0.5;
        if (this.encoding === 'rect') {
          gfxContext.drawRect(inset, i * config.cellHeight + inset, config.cellWidth - 2 * inset, config.cellHeight - 2 * inset);
        }
        if (this.encoding === 'circle') {
          gfxContext.drawCircle(
            config.cellWidth / 2 + inset,
            i * config.cellHeight + config.cellHeight / 2 + inset,
            config.cellWidth / 2 - inset,
          );
        }
        gfxContext.endFill();

        return gfxContext;
      },
    });

    config.visMapping = visMapping;
  };

  function setupColumns(edges: Edge[], columnOrder: string[], rowOrder: string[]) {
    const visMapping = config.visMapping[0];

    if (!visMapping) throw new Error('Cannot setup matrix without visMapping');

    for (let j = 0; j < columnOrder.length; j++) {
      const oneColumn = new Container();
      oneColumn.name = columnOrder[j];

      const edgesForThisColumn = edges.filter(edge => {
        return edge.from === oneColumn.name || edge.to === oneColumn.name;
      });

      const col = createColumn(rowOrder, edgesForThisColumn, config.visMapping, config.cellWidth, config.cellHeight);
      oneColumn.addChild(col);
      oneColumn.position.set(j * config.cellWidth + config.textOffsetX, config.textOffsetY);

      columnsContainer.addChild(oneColumn);
      oneColumn.alpha = 0;
      Actions.sequence(Actions.delay(j * 0.005 * Math.random()), Actions.fadeIn(oneColumn, 0.8, Interpolations.pow2out)).play();
    }
  }

  const setupColumnInteractivity = (cols: Node[]) => {
    for (let j = 0; j < columnsContainer.children.length; j++) {
      const oneColumn = columnsContainer.children[j];
      oneColumn.interactive = true;
      oneColumn.on('pointerdown', onButtonDown);
      oneColumn.on('pointerover', event => {
        const col = cols.find(col => col._id === oneColumn.name);
        onHoverColumn(event, col);
      });
      oneColumn.on('pointerout', event => {
        onHoverColumn(event, null);
      });
    }
  };

  const setupColumnAxis = (columnOrder: string[], label: string) => {
    if (scaleColumns == null) scaleColumns = scaleBand();
    scaleColumns.domain(columnOrder);

    if (svg.current == null) return;
    const selection = select(svg.current);
    const g = selection.append('g').attr('class', 'axis top').attr('transform', `translate(0, ${config.textOffsetY})`);
    g.append('text')
      .attr('class', 'label')
      .style('text-anchor', 'middle')
      .style('alignment-baseline', 'hanging')
      .style('fill', globalConfig.theme === Theme.dark ? 'white' : 'black')
      .style('font-size', '14')
      .text(label);

    // Click handler for reordering columns
    const axisTopHandle = internalRef.current?.querySelector(`.axisTop`) as HTMLDivElement;
    axisTopHandle.addEventListener('click', () => {
      if (!props.graph) throw new Error('Graph is undefined; cannot reorder matrix');

      const reorderingManager = new ReorderingManager(props.graph);
      const newOrdering = reorderingManager.reorderMatrix('count', columnOrder, rowOrder);
      columnOrder = newOrdering.columnOrder.reverse();

      reorderColumns(columnOrder);
    });

    // Create d3 axis object
    axisColumns = axisTop(scaleColumns)
      .tickSizeOuter(0)
      .tickFormat((d: string, i: number) => {
        const minimumWidth = 25;
        const range = scaleColumns.range()[1] - scaleColumns.range()[0];
        let skip = Math.round((minimumWidth * columnOrder.length) / range);
        skip = Math.max(1, skip);

        return i % skip === 0 ? d : '';
      });

    // Ensure axis is updated
    updateColumnAxis();
  };

  let axisColumns: Axis<string>;
  let axisRows: Axis<string>;

  let animatingAxes = false;
  const updateColumnAxis = () => {
    if (viewport.current == null || canvas.current == null) return;

    const initialWidth = columnOrder.length * config.cellWidth + 2 * config.textOffsetX;

    scaleColumns.range([
      viewport.current.position.x + viewport.current.scale.x * config.textOffsetX,
      viewport.current.position.x + viewport.current.scale.x * (initialWidth - config.textOffsetX),
    ]);

    if (svg.current == null) return;

    if (animatingAxes) return;

    const selectionTop = select(svg.current).select('.axis.top') as Selection<SVGGElement, unknown, BaseType, unknown>;
    if (!selectionTop.empty()) {
      selectionTop.call(axisColumns);
      selectionTop
        .select('text.label')
        .attr('x', function () {
          const halfWidth = (this as SVGTextElement).getBBox().width / 2;
          return Math.max(
            config.textOffsetX + halfWidth,
            Math.min((svg.current?.clientWidth ?? 0) - halfWidth, (scaleColumns.range()[1] + scaleColumns.range()[0]) / 2),
          );
        })
        .attr('y', -config.textOffsetY + 7);
      selectionTop.select('.domain').style('display', 'none');
    }
  };

  function shuffle(array: any[]) {
    let m = array.length,
      t,
      i;

    // While there remain elements to shuffle…
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);

      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }

    return array;
  }

  function setupRowAxis(rowOrder: string[], label: string) {
    if (scaleRows == null) scaleRows = scaleBand();
    scaleRows.domain(rowOrder);

    if (svg.current == null) return;
    const selection = select(svg.current);
    const g = selection.append('g').attr('class', 'axis left').attr('transform', `translate(${config.textOffsetX}, 0)`);

    g.append('text')
      .attr('class', 'label')
      .style('text-anchor', 'middle')
      .style('alignment-baseline', 'hanging')
      .style('fill', globalConfig.theme === Theme.dark ? 'white' : 'black')
      .style('font-size', '14')
      .text(label);

    // Click handler for reordering columns
    const axisLeftHandle = internalRef.current?.querySelector(`.axisLeft`) as HTMLDivElement;
    axisLeftHandle.addEventListener('click', () => {
      if (!props.graph) throw new Error('Graph is undefined; cannot reorder matrix');

      rowOrder = shuffle(rowOrder);
      reorderRows(props?.graph.edges, rowOrder, columnOrder);
    });

    // Create d3 axis object
    axisRows = axisLeft(scaleRows)
      .tickSizeOuter(0)
      .tickFormat((d: string, i: number) => {
        const minimumHeight = 20;
        const range = scaleRows.range()[1] - scaleRows.range()[0];
        let skip = Math.round((minimumHeight * rowOrder.length) / range);
        skip = Math.max(1, skip);

        return i % skip === 0 ? d : '';
      });

    // Ensure axis is updated
    updateRowAxis();
  }

  const updateRowAxis = () => {
    if (viewport.current == null || canvas.current == null) return;

    const initialHeight = rowOrder.length * config.cellHeight + 2 * config.textOffsetY;

    scaleRows.range([
      viewport.current.position.y + viewport.current.scale.y * config.textOffsetY,
      viewport.current.position.y + viewport.current.scale.y * (initialHeight - config.textOffsetY),
    ]);

    if (svg.current == null) return;

    if (animatingAxes) return;

    const selectionLeft = select(svg.current).select('.axis.left') as Selection<SVGGElement, unknown, BaseType, unknown>;
    if (!selectionLeft.empty()) {
      selectionLeft.call(axisRows);
      selectionLeft.select('text.label').attr('transform', function () {
        const halfHeight = (this as SVGTextElement).getBBox().width / 2;
        const y = Math.max(
          config.textOffsetY + halfHeight,
          Math.min((internalRef.current?.clientHeight ?? 0) - halfHeight, (scaleRows.range()[1] + scaleRows.range()[0]) / 2),
        );

        return `translate(
            ${-config.textOffsetX + 7},
            ${y})rotate(-90)
          `;
      });
      selectionLeft.select('.domain').style('display', 'none');
    }
  };

  const tick = (delta: number) => {
    if (props.graph) {
      updateColumnAxis();
      updateRowAxis();
      Actions.tick(delta / 60);
    }
  };

  return (
    <>
      {popups.map(popup => (
        <NLPopup onClose={() => {}} data={popup} key={popup.node.id} />
      ))}
      {quickPopup && <NLPopup onClose={() => {}} data={quickPopup} />}
      <div ref={internalRef} className={`h-full w-full overflow-hidden relative matrix`}>
        <canvas ref={canvas}></canvas>
        <div
          className={`axisLeft`}
          style={{
            position: 'absolute',
            top: styleMatrixSize,
            left: 0,
            bottom: 0,
            width: styleMatrixSize,
            backdropFilter: 'blur(10px)',
            background: globalConfig.theme === Theme.dark ? 'rgba(0,0,0,0.2)' : 'rgba(255,255,255, 0.5)',
            boxShadow: globalConfig.theme === Theme.dark ? '1px 0px 0px 0px rgba(255,255,255,0.2)' : '1px 0px 0px 0px rgba(0,0,0,0.2)',
          }}
        ></div>
        <div
          className={`axisTop`}
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            height: styleMatrixSize,
            backdropFilter: 'blur(10px)',
            background: globalConfig.theme === Theme.dark ? 'rgba(0,0,0,0.2)' : 'rgba(255,255,255, 0.5)',
            boxShadow:
              globalConfig.theme === Theme.dark
                ? `${styleMatrixSize}px 1px 0px 0px rgba(255,255,255,0.2)`
                : `${styleMatrixSize}px 1px 0px 0px rgba(0,0,0,0.2)`,
          }}
        ></div>
        <svg
          ref={svg}
          style={{
            clipPath: `polygon(0% 0%, 0% 100%, 100% 100%, 100% 0%, ${styleMatrixSize}px 0%, ${styleMatrixSize}px ${styleMatrixSize}px, 0% ${styleMatrixSize}px)`,
          }}
        ></svg>
      </div>
    </>
  );
});
