import { Edge } from '@/lib/data-access';
import { Graphics } from 'pixi.js';

export const createColumn = (
  orderRow: string[],
  edgesForThisColumn: Edge[],
  visMapping: any[], // TODO type
  cellWidth: number,
  cellHeight: number,
) => {
  const currentVisMapping = visMapping[0];
  let gfx = new Graphics();
  gfx.eventMode = 'static';
  if (!currentVisMapping) {
    return gfx;
  }

  let color = null;
  for (let i = 0; i < orderRow.length; i++) {
    const rowOrderID = orderRow[i];

    const inOutEdge = edgesForThisColumn.filter(edge => {
      return edge.from === rowOrderID || edge.to === rowOrderID;
    });

    const inEdges = edgesForThisColumn.filter(edge => {
      return edge.to === rowOrderID;
    });

    const outEdges = edgesForThisColumn.filter(edge => {
      return edge.from === rowOrderID;
    });

    /**
     * maybe this should inverted: make it a visitor
     * pattern and let the visMapping decide how ot implment all this
     **/
    if (currentVisMapping.attribute === 'adjacency') {
      color = currentVisMapping.colorScale(Math.min(inOutEdge.length, 1));
      gfx = currentVisMapping.renderFunction(i, color, gfx);
    } else if (currentVisMapping.attribute === 'label') {
      if (outEdges.length > 0) {
        const thisEdge = outEdges[0];
        color = currentVisMapping.colorScale(thisEdge.label);
      }
      gfx.beginFill(color, 1);
      // gfx.beginFill(colorScale(node.type)));
      gfx.lineStyle(2, 0xffffff);
      gfx.drawRect(0, i * cellWidth, cellWidth, cellHeight);
      gfx.endFill();
    } else {
      if (outEdges.length > 0) {
        const thisEdge = outEdges[0];
        const value = byString(thisEdge, currentVisMapping.attribute);

        color = currentVisMapping.colorScale(value);
      }
      gfx.beginFill(color, 1);
      // gfx.beginFill(colorScale(node.type)));
      gfx.lineStyle(2, 0xffffff);
      gfx.drawRect(0, i * cellWidth, cellWidth, cellHeight);
      gfx.endFill();
    }
  }

  return gfx;
};

const byString = (o: any, accessor: string) => {
  accessor = accessor.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
  accessor = accessor.replace(/^\./, ''); // strip a leading dot
  const a = accessor.split('.');
  for (let i = 0, n = a.length; i < n; ++i) {
    const k = a[i];
    if (k in o) {
      o = o[k];
    } else {
      return;
    }
  }
  return o;
};
