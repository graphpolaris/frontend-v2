import { GraphQueryResult } from '@/lib/data-access';
import * as reorder from 'reorder.js';

export class ReorderingManager {
  private leafOrder = reorder.optimal_leaf_order().distance(reorder.distance.manhattan);
  graph: GraphQueryResult;

  constructor(graph: GraphQueryResult) {
    this.graph = graph;
  }

  private computeNameOrder = (columnOrder: string[], rowOrder: string[]) => {
    const columnOrderSorted = columnOrder.sort((a, b) => a.localeCompare(b));
    const rowOrderSorted = rowOrder.sort((a, b) => a.localeCompare(b));

    return { columnOrder: columnOrderSorted, rowOrder: rowOrderSorted };
  };

  private computeCountOrder = (columnOrder: string[], rowOrder: string[]) => {
    let columnOrderSorted: { id: string; count: number }[] = [];
    let rowOrderSorted: { id: string; count: number }[] = [];

    for (let i = 0; i < columnOrder.length; i++) {
      const oneColumn = columnOrder[i];
      const edgesForThisColumn = this.graph.edges.filter(edge => {
        return edge.from === oneColumn || edge.to === oneColumn;
      });

      columnOrderSorted.push({ id: oneColumn, count: edgesForThisColumn.length });
    }

    for (let j = 0; j < rowOrder.length; j++) {
      const oneRow = rowOrder[j];
      const edgesForThisRow = this.graph.edges.filter(edge => {
        return edge.from === oneRow || edge.to === oneRow;
      });

      rowOrderSorted.push({
        id: oneRow,
        count: edgesForThisRow.length,
      });
    }

    columnOrderSorted = columnOrderSorted.sort((a, b) => b.count - a.count);
    rowOrderSorted = rowOrderSorted.sort((a, b) => b.count - a.count);

    return { columnOrder: columnOrderSorted.map(a => a.id), rowOrder: rowOrderSorted.map(a => a.id) };
  };

  private computeLeaforder = (columnOrder: string[], rowOrder: string[]) => {
    const adjacency: number[][] = this.constructAdjacencyMatrix(this.graph, columnOrder, rowOrder);

    const order = this.leafOrder.reorder(adjacency).map(i => columnOrder[i]);

    console.debug('computed leaforder', order);

    return { columnOrder: order, rowOrder: rowOrder };
  };

  private constructAdjacencyMatrix(graph: GraphQueryResult, columnOrder: string[], rowOrder: string[]): number[][] {
    const adjacency: number[][] = [];
    for (let i = 0; i < columnOrder.length; i++) {
      const oneColumn = columnOrder[i];
      const edgesForThisColumn = graph.edges.filter(edge => {
        return edge.from === oneColumn || edge.to === oneColumn;
      });

      const rowOrderIDs = [];
      for (let j = 0; j < rowOrder.length; j++) {
        const oneRow = rowOrder[j];
        if (
          edgesForThisColumn.filter(edge => {
            return edge.from === oneRow || edge.to === oneRow;
          }).length > 0
        ) {
          rowOrderIDs.push(j);
        }
      }
      adjacency.push(rowOrderIDs);
    }

    return adjacency;
  }

  private computeBarycenter = (columnOrder: string[], rowOrder: string[]) => {
    const reorderAny = reorder as any;

    const reorderGraph = this.getReorderJSGraph(this.graph, columnOrder, rowOrder);
    const barycenter = reorderAny.barycenter_order(reorderGraph);

    const improved = reorderAny.adjacent_exchange(reorderGraph, barycenter[0], barycenter[1]);

    const columnOrderSorted = improved[0].map((i: number) => columnOrder[i]).filter((a: string) => columnOrder.includes(a));
    const rowOrderSorted = improved[0].map((i: number) => rowOrder[i]).filter((a: string) => rowOrder.includes(a));

    console.debug('improved barycenter', improved, columnOrderSorted, rowOrderSorted);

    return { columnOrder: columnOrderSorted, rowOrder: rowOrderSorted };
  };

  private getReorderJSGraph = (graph: GraphQueryResult, columnOrder: string[], rowOrder: string[]) => {
    const reorderAny = reorder as any;

    const nodesTemp = graph.nodes.map(
      node =>
        ({
          ...node,
          index: 0,
          weight: 0,
        }) as any,
    );

    const nodes = columnOrder.map(id => nodesTemp.find(node => node.id === id));
    const graphReorderJs = reorderAny.graph().nodes(nodes);

    const edges = graph.edges.map(edge => {
      const source = nodes.find(node => node.id === edge.from);
      const target = nodes.find(node => node.id === edge.to);

      if (!source || !target) {
        return new Error('Source or target not found from edge list');
      }
      return {
        ...edge,
        source,
        target,
      } as any;
    });

    graphReorderJs.links(edges);
    graphReorderJs.init();

    return graphReorderJs;
  };

  private computeRCM = (columnOrder: string[], rowOrder: string[]) => {
    const reorderAny = reorder as any;
    const reorderGraph = this.getReorderJSGraph(this.graph, columnOrder, rowOrder);

    const order = reorderAny.reverse_cuthill_mckee_order(reorderGraph);
    const columnOrderSorted = order.map((i: number) => columnOrder[i]).filter((a: string) => columnOrder.includes(a));
    const rowOrderSorted = order.map((i: number) => rowOrder[i]).filter((a: string) => rowOrder.includes(a));

    console.debug('improved rcm', order, columnOrderSorted, rowOrderSorted);

    return { columnOrder: columnOrderSorted, rowOrder: rowOrderSorted };
  };

  private computeSpectal = (columnOrder: string[], rowOrder: string[]) => {
    const reorderAny = reorder as any;
    const reorderGraph = this.getReorderJSGraph(this.graph, columnOrder, rowOrder);

    const order = reorderAny.spectral_order(reorderGraph);
    const columnOrderSorted = order.map((i: number) => columnOrder[i]).filter((a: string) => columnOrder.includes(a));
    const rowOrderSorted = order.map((i: number) => rowOrder[i]).filter((a: string) => rowOrder.includes(a));

    console.debug('improved spectral', order, columnOrderSorted, rowOrderSorted);

    return { columnOrder: columnOrderSorted, rowOrder: rowOrderSorted };
  };

  private computeBFS = (columnOrder: string[], rowOrder: string[]) => {
    const reorderAny = reorder as any;
    const reorderGraph = this.getReorderJSGraph(this.graph, columnOrder, rowOrder);

    const order = reorderAny.bfs_order(reorderGraph);

    const columnOrderSorted = order.map((i: number) => columnOrder[i]).filter((a: string) => columnOrder.includes(a));
    const rowOrderSorted = order.map((i: number) => rowOrder[i]).filter((a: string) => rowOrder.includes(a));

    console.debug('improved bfs', order, columnOrderSorted, rowOrderSorted);

    return { columnOrder: columnOrderSorted, rowOrder: rowOrderSorted };
  };

  private computePCA = (columnOrder: string[], rowOrder: string[]) => {
    const reorderAny = reorder as any;

    const columnOrderUnSorted = columnOrder.map((id: string) => this.graph.nodes.find(node => node._id === id));

    const order = reorderAny.pca_order(columnOrderUnSorted);
    const columnOrderSorted = order.map((i: number) => columnOrder[i]);

    console.debug('improved pca', order, columnOrderSorted, rowOrder);

    return { columnOrder: columnOrderSorted, rowOrder: rowOrder };
  };

  public reorderMatrix = (orderingname = 'leafordering', columnOrder: string[], rowOrder: string[]) => {
    switch (orderingname.toLowerCase()) {
      case 'leafordering': {
        return this.computeLeaforder(columnOrder, rowOrder);
      }
      case 'name': {
        return this.computeNameOrder(columnOrder, rowOrder);
      }
      case 'count': {
        return this.computeCountOrder(columnOrder, rowOrder);
      }
      case 'barycenter': {
        return this.computeBarycenter(columnOrder, rowOrder);
      }
      case 'rcm': {
        return this.computeRCM(columnOrder, rowOrder);
      }
      case 'spectral': {
        return this.computeSpectal(columnOrder, rowOrder);
      }
      // case 'pca': {
      //   return this.computePCA(columnOrder, rowOrder);
      // }
      // case 'bfs': {
      //   return this.computeBFS(columnOrder, rowOrder);
      // }
      case 'none': {
        return { columnOrder, rowOrder };
      }
      case 'identity': {
        return { columnOrder, rowOrder };
      }
      default: {
        return this.computeLeaforder(columnOrder, rowOrder);
      }
    }
  };
}
