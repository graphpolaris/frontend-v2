import { GraphQueryResult } from '@/lib/data-access';
import { plotTypeOptions } from './components/MakePlot';

export interface Vis1DProps {
  plotType: (typeof plotTypeOptions)[number]; // plotly plot type
  title: string; // title of the plot
  selectedEntity: string; // node label to plot
  xAxisLabel: string;
  yAxisLabel: string;
  zAxisLabel?: string;
  showAxis: boolean;
  groupData?: string;
  stack: boolean;
}

export enum Vis1DHistogramAggregation {
  COUNT = 'count',
  DEGREE = 'degree',
}
export const vis1DHistogramAggregationOptions = Object.values(Vis1DHistogramAggregation);

export interface Vis1DVisHandle {
  exportImageInternal: () => void;
}

export const getAttributeValues = (
  query: GraphQueryResult,
  selectedEntity: string,
  attributeKey: string | number | undefined,
  groupBy?: string,
): (string | number)[] => {
  if (!selectedEntity || !attributeKey) {
    return [];
  }

  if (attributeKey == ' ') {
    return [];
  }

  const attValues = query.nodes
    .filter(item => item.label === selectedEntity)
    .map(item => {
      // Check if the attribute exists, return its value if it does, or an empty string otherwise
      return [
        item._id,
        item.attributes && attributeKey in item.attributes && item.attributes[attributeKey] != ''
          ? (item.attributes[attributeKey] as string | number)
          : 'NoData',
      ];
    });
  const uniqueValues = Array.from(new Set(attValues.map(item => item[1])));

  if (!groupBy) {
    return uniqueValues;
  }

  if (groupBy === 'count') {
    const agg = Object.entries(
      attValues
        .map(item => item[1])
        .reduce(
          (acc, item) => {
            acc[item] = (acc[item] || 0) + 1;
            return acc;
          },
          {} as Record<string, number>,
        ),
    );
    const ret = agg
      .map(item => ({ id: item[0], value: item[1], position: uniqueValues.indexOf(item[0]) }))
      .sort((a, b) => a.position - b.position)
      .map(item => item.value);
    return ret;
  } else if (groupBy === 'degree') {
    const attValuesMap = Object.fromEntries(attValues);
    const ids = attValues.map(item => item[0]);
    const degree = query.edges.reduce(
      (acc, item) => {
        if (ids.includes(item.from)) {
          acc[attValuesMap[item.from]] = (acc[attValuesMap[item.from]] || 0) + 1;
        } else if (ids.includes(item.to)) {
          acc[attValuesMap[item.to]] = (acc[attValuesMap[item.to]] || 0) + 1;
        }
        return acc;
      },
      {} as Record<string, number>,
    );
    return attValues
      .map(item => ({ id: item[0], value: degree[item[1]] || 0, position: uniqueValues.indexOf(item[0]) }))
      .sort((a, b) => a.position - b.position)
      .map(item => item.value);
  } else {
    console.error('Invalid groupBy value', groupBy);
    throw new Error('Invalid groupBy value');
  }
};
