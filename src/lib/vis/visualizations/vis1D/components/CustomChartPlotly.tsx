import React, { useRef, useEffect, useState } from 'react';
import Plot from 'react-plotly.js';
import { PreparePlot } from './MakePlot';

export const CustomChartPlotly: React.FC<{ plot: PreparePlot }> = ({ plot }) => {
  const internalRef = useRef<HTMLDivElement>(null);
  const [divSize, setDivSize] = useState({ width: 0, height: 0 });
  const [hoveredPoint, setHoveredPoint] = useState<{
    left: number;
    top: number;
    xValue: number;
    yValue: number;
  } | null>(null);

  useEffect(() => {
    const handleResize = () => {
      if (internalRef.current) {
        const { width, height } = internalRef.current.getBoundingClientRect();
        setDivSize({ width, height });
      }
    };

    handleResize();
    window.addEventListener('resize', handleResize);
    if (internalRef.current) {
      new ResizeObserver(handleResize).observe(internalRef.current);
    }

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const handleHover = (event: any) => {
    const { points } = event;
    if (points.length) {
      const point = points[0];
      const plotRect = internalRef.current?.getBoundingClientRect();

      if (plotRect) {
        const xIndex = point.xaxis.d2p(point.x);
        const yIndex = point.yaxis.d2p(point.y);

        setHoveredPoint({
          left: plotRect.left + xIndex,
          top: plotRect.top + yIndex,
          xValue: point.x,
          yValue: point.y,
        });
      }
    }
  };

  const handleUnHover = () => {
    setHoveredPoint(null);
  };

  // !TODO: implement pattern fill for nonData
  /*
  useEffect(() => {
    const svg = document.querySelector('svg');
    if (svg) {
      // Create or find the `defs` section
      let defs = svg.querySelector('defs');
      if (!defs) {
        defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
        svg.insertBefore(defs, svg.firstChild);
      }

      // Check if the pattern already exists
      let pattern = defs.querySelector('#diagonalHatch');
      if (!pattern) {
        // Create the diagonal hatch pattern
        pattern = document.createElementNS('http://www.w3.org/2000/svg', 'pattern');
        pattern.setAttribute('id', 'diagonalHatch');
        pattern.setAttribute('width', '6');
        pattern.setAttribute('height', '6');
        pattern.setAttribute('patternTransform', 'rotate(45)');
        pattern.setAttribute('patternUnits', 'userSpaceOnUse');

        const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        rect.setAttribute('width', '2');
        rect.setAttribute('height', '6');
        rect.setAttribute('fill', '#cccccc');

        pattern.appendChild(rect);
        defs.appendChild(pattern);
      }

      //const bars = select('.points').selectAll('path').nodes();
      const bars = document.querySelectorAll('.points path');
      //console.log(bars);
      if (plotType === 'histogram') {
        bars.forEach((bar, index) => {
          const customData = (plotData[0] as any).customdata[index];
          //console.log(select(bar), customData, customData == 'nonData');
          select(bar).style('fill', 'rgb(250, 0, 0)');

          if (customData == 'nonData') {
            //select(bar).style('fill', 'url(#diagonalHatch)');
          }
          //console.log(bar);
        });
      }
    }
  }, [plotData]);
  */
  return (
    <div className="h-full w-full flex items-center justify-center overflow-hidden relative" ref={internalRef}>
      <Plot
        data={plot.plotData}
        config={{
          responsive: true,
          scrollZoom: false,
          displayModeBar: false,
          displaylogo: false,
        }}
        layout={{
          ...plot.layout,
          width: divSize.width,
          height: divSize.height,
          dragmode: false,
        }}
        onHover={handleHover}
        onUnhover={handleUnHover}
      />
      {/*

      {hoveredPoint && (
        <div>
          <Tooltip open={true} showArrow={true}>
            <TooltipTrigger />
            <TooltipContent
              style={{
                position: 'absolute',
                left: hoveredPoint.left,
                top: hoveredPoint.top,
                transform: 'translate(-50%, 0%)',
                transform: 'translate(-50%, 0%)',
              }}
            >
              <div>
                <strong>{hoveredPoint.xValue}</strong>: {hoveredPoint.yValue}
              </div>
            </TooltipContent>
          </Tooltip>
        </div>
      )}
        */}
    </div>
  );
};
