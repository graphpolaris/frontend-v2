import { scaleQuantize, scaleOrdinal } from 'd3';
import { PlotType } from 'plotly.js';
import { visualizationColors } from 'ts-common';
import { Vis1DHistogramAggregation, vis1DHistogramAggregationOptions } from '../model';

export const plotTypeOptions = ['bar', 'scatter', 'line', 'histogram', 'pie'] as const;
export type SupportedPlotType = (typeof plotTypeOptions)[number];

const getCSSVariableHSL = (varName: string) => {
  const rootStyles = getComputedStyle(document.documentElement);
  const hslValue = rootStyles.getPropertyValue(varName).trim().replace('deg', '');
  return `hsl(${hslValue})`;
};

const groupByTime = (xAxisData: string[], groupBy: string, additionalVariableData?: (string | number)[]) => {
  // Function to parse the date-time string into a JavaScript Date object
  const parseDate = (dateStr: string) => {
    // Remove nanoseconds part and use just the standard "YYYY-MM-DD HH:MM:SS" part
    const cleanedDateStr = dateStr.split('.')[0];
    return new Date(cleanedDateStr);
  };

  // Grouping logic
  const groupedData = xAxisData.reduce(
    (acc, dateStr, index) => {
      const date = parseDate(dateStr);
      let groupKey: string;

      if (groupBy === 'yearly') {
        groupKey = date.getFullYear().toString(); // Group by year (e.g., "2012")
      } else if (groupBy === 'quarterly') {
        const month = date.getMonth() + 1; // Adjust month for zero-indexed months
        const quarter = Math.floor((month - 1) / 3) + 1; // Calculate quarter (Q1-Q4)
        groupKey = `${date.getFullYear()}-Q${quarter}`;
      } else if (groupBy === 'monthly') {
        // Group by month, e.g., "2012-07"
        groupKey = `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}`;
      } else {
        // Default case: group by year (or some other grouping logic)
        groupKey = date.getFullYear().toString();
      }

      // Initialize the group if it doesn't exist
      if (!acc[groupKey]) {
        acc[groupKey] = additionalVariableData
          ? typeof additionalVariableData[0] === 'number'
            ? 0 // Initialize sum for numbers
            : [] // Initialize array for strings
          : 0; // Initialize count for no additional data
      }

      // Aggregate additional variable if provided
      if (additionalVariableData) {
        if (typeof additionalVariableData[index] === 'number') {
          acc[groupKey] = (acc[groupKey] as number) + (additionalVariableData[index] as number);
        } else if (typeof additionalVariableData[index] === 'string') {
          acc[groupKey] = [...(acc[groupKey] as string[]), additionalVariableData[index] as string];
        }
      } else {
        // Increment the count if no additionalVariableData
        acc[groupKey] = (acc[groupKey] as number) + 1;
      }

      return acc;
    },
    {} as Record<string, number | string[]>,
  );

  // Extract grouped data into arrays for Plotly
  const xValuesGrouped = Object.keys(groupedData);
  const yValuesGrouped = Object.values(groupedData);

  return { xValuesGrouped, yValuesGrouped };
};

const computeStringTickValues = (xValues: any[], maxTicks: number, maxLabelLength: number): any[] => {
  const truncatedValues = xValues.map(label => (label.length > maxLabelLength ? `${label.slice(0, maxLabelLength)}…` : label));

  return truncatedValues;
};

export type PreparePlot = { plotData: Partial<Plotly.PlotData>[]; layout: Partial<Plotly.Layout> };
export const preparePlotData = (params: {
  xAxisData: (string | number)[];
  plotType: SupportedPlotType;
  yAxisData?: (string | number)[];
  zAxisData?: (string | number)[];
  xAxisLabel: string;
  yAxisLabel: string;
  zAxisLabel?: string;
  showAxis: boolean;
  groupBy?: string;
  stack?: boolean;
}): PreparePlot => {
  const { xAxisData, plotType, yAxisData, zAxisData, xAxisLabel, yAxisLabel, zAxisLabel, showAxis, groupBy, stack } = params;
  const primaryColor = getCSSVariableHSL('--clr-sec--400');
  const lengthLabelsX = 7; // !TODO computed number of elements based
  const lengthLabelsY = 8; // !TODO computed number of elements based
  const mainColors = visualizationColors.GPCat.colors[14];

  const sharedTickFont = {
    family: 'Inter',
    size: 11,
    color: '#374151', // !TODO get GP value
  };

  let xValues: (string | number)[] = [];
  let yValues: (string | number)[] = [];

  let colorScale: any;
  let colorDataZ: string[] = [];
  let colorbar: any = {};

  if (zAxisData && zAxisData.length > 0 && typeof zAxisData[0] === 'number') {
    const mainColorsSeq = visualizationColors.GPSeq.colors[9];
    const numericZAxisData = zAxisData.filter((item): item is number => typeof item === 'number');
    const zMin = numericZAxisData.reduce((min, val) => (val < min ? val : min), zAxisData[0]);
    const zMax = numericZAxisData.reduce((max, val) => (val > max ? val : max), zAxisData[0]);

    // !TODO: option to have a linear or quantize scale
    colorScale = scaleQuantize<string>().domain([zMin, zMax]).range(mainColorsSeq);

    colorDataZ = zAxisData?.map(item => colorScale(item) || primaryColor);

    colorbar = {
      title: 'Color Legend',
      tickvals: [zMin, zMax],
      ticktext: [`${zMin}`, `${zMax}`],
    };
  } else {
    const uniqueZAxisData = Array.from(new Set(zAxisData));

    if (zAxisData && uniqueZAxisData) {
      colorScale = scaleOrdinal<string>().domain(uniqueZAxisData.map(String)).range(mainColors);

      colorDataZ = zAxisData?.map(item => colorScale(String(item)) || primaryColor);
      const sortedDomain = uniqueZAxisData.sort();
      colorbar = {
        title: 'Color Legend',
        tickvals: sortedDomain,
        ticktext: sortedDomain.map(val => String(val)),
        tickmode: 'array',
      };
    }
  }

  if (!groupBy || vis1DHistogramAggregationOptions.includes(groupBy as Vis1DHistogramAggregation)) {
    if (xAxisData.length !== 0 && yAxisData && yAxisData.length !== 0) {
      xValues = xAxisData;
      yValues = yAxisData;
    } else if (xAxisData.length !== 0 && (!yAxisData || yAxisData.length === 0)) {
      xValues = xAxisData;
      yValues = xAxisData.map((_, index) => index + 1);
    } else if (xAxisData.length === 0 && yAxisData && yAxisData.length !== 0) {
      xValues = yAxisData.map((_, index) => index + 1);
      yValues = yAxisData;
    }
  } else {
    if (groupBy) {
      if (yAxisData && yAxisData.length !== 0) {
        const { xValuesGrouped, yValuesGrouped } = groupByTime(xAxisData as string[], groupBy, yAxisData);
        xValues = xValuesGrouped;
        yValues = yValuesGrouped.flat();
      } else {
        const { xValuesGrouped, yValuesGrouped } = groupByTime(xAxisData as string[], groupBy);
        xValues = xValuesGrouped;
        yValues = yValuesGrouped.flat();
      }
    } else {
      xValues = xAxisData;
      yValues = xAxisData.map((_, index) => index + 1);
    }
  }

  let sortedLabels: string[] = [];
  let sortedFrequencies = [];

  let truncatedXLabels: string[] = [];
  let truncatedYLabels: string[] = [];
  let yAxisRange: number[] = [];

  if (typeof xValues[0] === 'string') {
    truncatedXLabels = computeStringTickValues(xValues, 2, lengthLabelsX);
  }

  if (typeof yValues[0] === 'string' && (plotType === 'scatter' || plotType === 'line')) {
    truncatedYLabels = computeStringTickValues(yValues, 2, lengthLabelsY);
  }
  const plotData = (() => {
    switch (plotType) {
      case 'bar':
        if (typeof xAxisData[0] === 'string' && groupBy == undefined) {
          const frequencyMap = xAxisData.reduce(
            (acc, item) => {
              acc[item] = (acc[item] || 0) + 1;
              return acc;
            },
            {} as Record<string, number>,
          );

          const sortedEntries = Object.entries(frequencyMap).sort((a, b) => b[1] - a[1]);

          sortedLabels = sortedEntries.map(([label]) => String(label));
          sortedFrequencies = sortedEntries.map(([, frequency]) => frequency);

          // !TODO: y ranges: max value showed is rounded, eg 54 -> 50
          // need to specify tickvales and ticktext

          const maxYValue = Math.max(...sortedFrequencies);
          yAxisRange = [0, maxYValue];

          return [
            {
              type: 'bar' as PlotType,
              x: xValues,
              y: yValues,
              marker: {
                color: colorDataZ?.length != 0 ? colorDataZ : primaryColor,
              },
              customdata: sortedLabels,
              // hovertemplate: '<b>%{customdata}</b>: %{y}<extra></extra>',
            },
          ];
        } else {
          return [
            {
              type: 'bar' as PlotType,
              x: xValues,
              y: yValues,
              marker: { color: primaryColor },
              customdata: xValues,
              // hovertemplate: '<b>%{customdata}</b>: %{y}<extra></extra>',
            },
          ];
        }

      case 'scatter':
        return [
          {
            type: 'scatter' as PlotType,
            x: xValues,
            y: yValues,
            mode: 'markers' as const,
            marker: {
              color: zAxisData && zAxisData.length > 0 ? colorDataZ : primaryColor,
              size: 7,
              stroke: 1,
            },
            customdata:
              xValues.length === 0
                ? yValues.map(y => `Y: ${y}`)
                : yValues.length === 0
                  ? xValues.map(x => `X: ${x}`)
                  : xValues.map((x, index) => {
                      const zValue = zAxisData && zAxisData.length > 0 ? zAxisData[index] : null;
                      return zValue ? `X: ${x} | Y: ${yValues[index]} | Color: ${zValue}` : `X: ${x} | Y: ${yValues[index]}`;
                    }),
            hovertemplate: '<b>%{customdata}</b><extra></extra>',
          },
        ];
      case 'line':
        return [
          {
            type: 'scatter' as PlotType,
            x: xValues,
            y: yValues,
            mode: 'lines' as const,
            line: { color: primaryColor },
            customdata: xValues.map(label => (label === 'undefined' || label === 'null' || label === '' ? 'nonData' : '')),
            hovertemplate: '<b>%{customdata}</b><extra></extra>',
          },
        ];
      case 'histogram':
        if (typeof xAxisData[0] === 'string') {
          if (zAxisData && zAxisData?.length > 0) {
            // const sortedData = xAxisData
            //   .map((value, index) => ({ x: value, y: yAxisData ? yAxisData[index] : null }))
            //   .sort((a, b) => (a.x > b.x ? 1 : -1));

            // const sortedLabels = sortedData.map(item => item.x);
            // const sortedFrequencies = sortedData.map(item => item.y);

            const frequencyMap = xAxisData.reduce(
              (acc, item, index) => {
                const color = zAxisData ? colorScale(zAxisData[index]) : primaryColor;

                if (!acc[item]) {
                  acc[item] = {
                    count: 0,
                    colors: [],
                    zValues: [],
                    zValueCounts: {},
                  };
                }

                acc[item].count = acc[item].count + ((yAxisData?.[index] as number) ?? 1);
                acc[item].colors.push(color);
                acc[item].zValues.push(zAxisData[index].toString());
                // Group and count zValues
                const zValue = zAxisData[index] || '(Empty)';
                acc[item].zValueCounts[zValue] = (acc[item].zValueCounts[zValue] || 0) + ((yAxisData?.[index] as number) ?? 1);

                return acc;
              },
              {} as Record<
                string,
                {
                  count: number;
                  colors: string[];
                  zValues: string[];
                  zValueCounts: Record<string, number>; // To store grouped counts
                }
              >,
            );
            const colorToLegendName = new Map();
            const sortedCategories = Object.entries(frequencyMap).sort((a, b) => b[1].count - a[1].count);

            const tracesByColor: Record<string, { x: string[]; y: number[] }> = {};

            sortedCategories.forEach(([label, { colors, zValues, zValueCounts }]) => {
              colors.forEach((color, idx) => {
                const zValue = zValues[idx];

                if (!colorToLegendName.has(color)) {
                  colorToLegendName.set(color, zValue);
                }

                if (!tracesByColor[color]) {
                  tracesByColor[color] = { x: [], y: [] };
                }
                tracesByColor[color].x.push(label);
                tracesByColor[color].y.push(zValueCounts[zValue]);
              });
            });

            sortedLabels = sortedCategories.map(element => element[0]);

            const traces = Array.from(colorToLegendName.entries()).map(([color, legendName]) => {
              const colorData = tracesByColor[color];
              const categoryCountMap: Record<string, number> = {};

              sortedLabels.forEach(label => {
                categoryCountMap[label] = frequencyMap[label].count;
              });
              const yValues = colorData.x.map((label, idx) => {
                const totalCount = categoryCountMap[label];
                const countForColor = colorData.y[idx];
                return stack ? (countForColor / totalCount) * 100 : countForColor;
              });

              const customdata = colorData.x.map((label, idx) => {
                const colorTranslation = colorToLegendName.get(color) === ' ' ? '(Empty)' : colorToLegendName.get(color);
                const percentage = ((100 * frequencyMap[label].zValueCounts[colorTranslation]) / frequencyMap[label].count).toFixed(1);
                return [label, !stack ? frequencyMap[label]?.zValueCounts[colorTranslation] || 0 : percentage, colorTranslation || ' '];
              });
              return {
                x: colorData.x,
                y: yValues,
                type: 'bar' as PlotType,
                name: legendName,
                marker: { color: color },
                customdata: customdata,
                hovertemplate:
                  '<b>X: %{customdata[0]}</b><br>' + '<b>Y: %{customdata[1]}</b><br>' + '<b>Color: %{customdata[2]}</b><extra></extra>',
                ...(stack ? { stackgroup: 'one' } : {}),
              };
            });

            return traces;
          } else {
            const sortedData = yAxisData
              ? yAxisData.map((value, index) => ({ y: value, x: xAxisData ? xAxisData[index] : null })).sort((a, b) => (a.y < b.y ? 1 : -1))
              : [];

            const sortedLabels = sortedData.map(item => item.x);
            const sortedFrequencies = sortedData.map(item => item.y);

            return [
              {
                type: 'bar' as PlotType,
                x: sortedLabels,
                y: sortedFrequencies,
                text: sortedFrequencies.length < 20 ? sortedFrequencies.map(String) : [],
                marker: { color: primaryColor },
                customdata: sortedLabels.map(label => xAxisLabel + ' ' + label),
                hovertemplate: '<b>%{customdata}</b>: %{y}<extra></extra>',
              },
            ];
          }
        } else {
          if (zAxisData && zAxisData?.length > 0) {
            const binCount = 20; // Number of bins (you can make this configurable)
            const numericXAxisData = xAxisData.map(val => Number(val)).filter(val => !isNaN(val));

            const xMin = numericXAxisData.reduce((min, val) => Math.min(min, val), Infinity);
            const xMax = numericXAxisData.reduce((max, val) => Math.max(max, val), -Infinity);

            const binSize = (xMax - xMin) / binCount;

            // Create bins
            const bins = Array.from({ length: binCount }, (_, i) => ({
              range: [xMin + i * binSize, xMin + (i + 1) * binSize],
              count: 0,
              zValueCounts: {} as Record<string, number>, // To track zAxisData counts per bin
            }));

            // Assign data points to bins
            numericXAxisData.forEach((xValue, index) => {
              const zValue = zAxisData ? zAxisData[index] || '(Empty)' : '(Empty)';
              const binIndex = Math.floor((xValue - xMin) / binSize);
              const bin = bins[Math.min(binIndex, bins.length - 1)]; // Ensure the last value falls into the final bin

              bin.count++;
              bin.zValueCounts[zValue] = (bin.zValueCounts[zValue] || 0) + 1;
            });

            const colorToLegendName = new Map();
            const tracesByColor: Record<string, { x: string[]; y: number[] }> = {};

            bins.forEach((bin, binIndex) => {
              const binLabel = `[${bin.range[0].toFixed(1)}, ${bin.range[1].toFixed(1)})`;

              Object.entries(bin.zValueCounts).forEach(([zValue, count]) => {
                const color = zAxisData ? colorScale(zValue) : primaryColor;

                if (!colorToLegendName.has(color)) {
                  colorToLegendName.set(color, zValue);
                }

                if (!tracesByColor[color]) {
                  tracesByColor[color] = { x: [], y: [] };
                }

                tracesByColor[color].x.push(binLabel);
                tracesByColor[color].y.push(stack ? (count / bin.count) * 100 : count);
              });
            });

            const traces = Array.from(colorToLegendName.entries()).map(([color, legendName]) => {
              const colorData = tracesByColor[color];
              const customdata = colorData.x.map((binLabel, idx) => {
                const countForColor = colorData.y[idx];
                const percentage = stack ? countForColor.toFixed(1) + '%' : countForColor.toFixed(0);
                return [binLabel, countForColor, percentage, legendName];
              });

              return {
                x: colorData.x,
                y: colorData.y,
                type: 'bar' as PlotType,
                name: legendName,
                marker: { color },
                customdata,
                autobinx: true,
                hovertemplate:
                  '<b>Bin: %{customdata[0]}</b><br>' +
                  '<b>Count/Percentage: %{customdata[2]}</b><br>' +
                  '<b>Group: %{customdata[3]}</b><extra></extra>',
                ...(stack ? { stackgroup: 'one' } : {}),
              };
            });

            return traces;
          } else {
            // No zAxisData, simple histogram logic
            return [
              {
                type: 'histogram' as PlotType,
                x: xAxisData,
                marker: { color: primaryColor },
                customdata: xAxisData,
              },
            ];
          }
        }
      case 'pie':
        return [
          {
            type: 'pie' as PlotType,
            labels: xValues.map(String),
            values: xAxisData,
            marker: { colors: mainColors },
          },
        ];
      default:
        return [];
    }
  })();

  const layout: Partial<Plotly.Layout> = {
    barmode: 'stack',
    xaxis: {
      title: {
        text: showAxis ? (xAxisLabel ? xAxisLabel : '') : '',
        standoff: 30,
      },
      tickfont: sharedTickFont,
      showgrid: false,
      visible: showAxis,
      ...(typeof xAxisData[0] === 'string' || (plotType === 'histogram' && sortedLabels.length > 0)
        ? { type: 'category', categoryarray: sortedLabels, categoryorder: 'array' }
        : {}),
      showline: true,
      zeroline: false,
      tickvals: typeof xValues[0] == 'string' ? xValues : undefined,
      ticktext: typeof xValues[0] == 'string' ? truncatedXLabels : undefined,
    },

    yaxis: {
      showgrid: false,
      visible: showAxis,
      showline: true,
      zeroline: false,
      tickfont: sharedTickFont,
      title: {
        text: showAxis ? (yAxisLabel ? yAxisLabel : '') : '',
        standoff: 30,
      },
      tickvals: typeof yValues[0] === 'string' && (plotType === 'scatter' || plotType === 'line') ? yValues : undefined,
      ticktext: typeof yValues[0] === 'string' && (plotType === 'scatter' || plotType === 'line') ? truncatedYLabels : undefined,
    },
    font: {
      family: 'Inter',
      size: 12,
      color: '#374151',
    },
    hoverlabel: {
      bgcolor: 'rgba(255, 255, 255, 0.8)',
      // className: 'text-dark',
      bordercolor: 'rgba(0, 0, 0, 0.2)',
      font: {
        family: 'Inter',
        size: 13,
        color: '#374151',
      },
    },
  };
  return { plotData, layout };
};
