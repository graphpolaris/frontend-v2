import React, { useState, useRef, useMemo, useImperativeHandle, forwardRef, useEffect } from 'react';
import { VisualizationPropTypes, VISComponentType, VisualizationSettingsPropTypes } from '../../common';
import { SettingsContainer } from '@/lib/vis/components/config';
import html2canvas from 'html2canvas';
import { CustomChartPlotly } from './components/CustomChartPlotly';
import { Input } from '@/lib/components/inputs';
import { EntityPill } from '@/lib/components/pills/Pill';
import { Button } from '@/lib/components/buttons';
import { Vis1DVisHandle, Vis1DProps, getAttributeValues, vis1DHistogramAggregationOptions } from './model';
import { plotTypeOptions, preparePlotData } from './components/MakePlot';

const defaultSettings: Vis1DProps = {
  plotType: 'bar',
  title: '',
  selectedEntity: '',
  xAxisLabel: '',
  yAxisLabel: '',
  zAxisLabel: '',
  showAxis: true,
  groupData: undefined,
  stack: false,
};

const Vis1D = forwardRef<Vis1DVisHandle, VisualizationPropTypes<Vis1DProps>>(({ data, settings }, refExternal) => {
  const internalRef = useRef<HTMLDivElement>(null);

  useImperativeHandle(refExternal, () => ({
    exportImageInternal() {
      const captureImage = () => {
        const element = internalRef.current;
        if (element) {
          html2canvas(element, {
            backgroundColor: '#FFFFFF',
          })
            .then(canvas => {
              const finalImage = canvas.toDataURL('image/png');
              const link = document.createElement('a');
              link.href = finalImage;
              link.download = 'Vis1D.png';
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
            })
            .catch(error => {
              console.error('Error capturing image:', error);
            });
        } else {
          console.error('Container element not found');
        }
      };

      const renderCanvas = () => {
        requestAnimationFrame(() => {
          captureImage();
        });
      };

      renderCanvas();
    },
  }));

  const xAxisData = useMemo(
    () => getAttributeValues(data, settings.selectedEntity, settings.xAxisLabel),
    [data, settings.selectedEntity, settings.xAxisLabel],
  );
  const yAxisData = useMemo(() => {
    if (settings.plotType === 'histogram') {
      return getAttributeValues(
        data,
        settings.selectedEntity,
        settings.xAxisLabel, // Note: x not y!
        settings.groupData,
      );
    }
    return getAttributeValues(data, settings.selectedEntity, settings.yAxisLabel);
  }, [data, settings.selectedEntity, settings.yAxisLabel, settings.plotType, settings.groupData]);
  const zAxisData = useMemo(
    () => getAttributeValues(data, settings.selectedEntity, settings.zAxisLabel),
    [data, settings.selectedEntity, settings.zAxisLabel],
  );

  return (
    <div className="h-full w-full flex items-center justify-center overflow-hidden" ref={internalRef}>
      <CustomChartPlotly
        plot={preparePlotData({
          xAxisData,
          plotType: settings.plotType,
          yAxisData,
          zAxisData,
          xAxisLabel: settings.xAxisLabel,
          yAxisLabel: settings.yAxisLabel,
          zAxisLabel: settings.zAxisLabel,
          showAxis: settings.showAxis,
          groupBy: settings.groupData,
          stack: settings.stack,
        })}
      />
    </div>
  );
});

const Vis1DSettings = ({ settings, graphMetadata, updateSettings }: VisualizationSettingsPropTypes<Vis1DProps>) => {
  // !TODO: include relationship
  const mutablePlotTypes = [...plotTypeOptions];
  const [attributeOptions, setAttributeOptions] = useState<string[]>([]);

  useEffect(() => {
    if (graphMetadata && graphMetadata.nodes && graphMetadata.nodes.labels.length > 0) {
      if (settings.selectedEntity === '') {
        updateSettings({ selectedEntity: graphMetadata.nodes.labels[0] });
      } else {
        const labelNodes = graphMetadata.nodes.labels;
        if (!labelNodes.includes(settings.selectedEntity)) {
          updateSettings({ selectedEntity: graphMetadata.nodes.labels[0] });
        }
      }
    }
  }, [graphMetadata]);

  useEffect(() => {
    if (graphMetadata && graphMetadata.nodes && graphMetadata.nodes.labels.length > 0 && settings.selectedEntity != '') {
      const labelNodes = graphMetadata.nodes.labels;
      if (labelNodes.includes(settings.selectedEntity)) {
        const newAttributeOptions = Object.keys(graphMetadata.nodes.types[settings.selectedEntity].attributes);
        if (settings.xAxisLabel === '') {
          updateSettings({ xAxisLabel: newAttributeOptions[0] });

          // !TODO: instead of contain "datum" chekc type: if it is date
          if (newAttributeOptions[0].includes('Datum')) {
            updateSettings({ groupData: 'yearly' });
          } else {
            updateSettings({ groupData: undefined });
          }
        }
        newAttributeOptions.unshift(' ');
        setAttributeOptions(newAttributeOptions);
      }
    }
  }, [graphMetadata, settings.selectedEntity]);

  return (
    <SettingsContainer>
      <div className="p-1">
        <Input
          className="w-full text-justify justify-start mb-2"
          type="dropdown"
          label=""
          value={settings.selectedEntity}
          options={graphMetadata.nodes.labels}
          onChange={val => updateSettings({ selectedEntity: val as string })}
          overrideRender={
            <EntityPill
              title={
                <div className="flex flex-row justify-between items-center cursor-pointer">
                  <span>{settings.selectedEntity || ''}</span>
                  <Button variantType="secondary" variant="ghost" size="2xs" iconComponent="icon-[ic--baseline-arrow-drop-down]" />
                </div>
              }
            />
          }
        />
        <div className="mb-2">
          <Input type="text" label="Title" value={settings.title} onChange={value => updateSettings({ title: value as string })} />
        </div>
        <div className="mb-2">
          <Input
            type="dropdown"
            label="Chart"
            value={settings.plotType as string}
            options={mutablePlotTypes}
            onChange={value => {
              updateSettings({ plotType: value as (typeof plotTypeOptions)[number] });
              if (value === 'histogram') {
                updateSettings({ yAxisLabel: '', groupData: 'count' });
              } else if (value === 'bar' || value === 'pie') {
                updateSettings({ yAxisLabel: '', groupData: undefined });
              }
            }}
          />
        </div>
        <div className="mb-2">
          <Input
            type="dropdown"
            label="X-axis:"
            value={settings.xAxisLabel}
            options={attributeOptions}
            onChange={value => {
              const valueString = value as string;
              updateSettings({ xAxisLabel: valueString });

              if (!valueString.includes('Datum')) {
                updateSettings({ groupData: undefined });
              } else {
                updateSettings({ groupData: 'monthly' });
              }
            }}
          />
        </div>
        {(settings.plotType === 'scatter' || settings.plotType === 'line') && (
          <div className="mb-2">
            <Input
              type="dropdown"
              label="Y-axis:"
              value={settings.yAxisLabel}
              options={attributeOptions}
              onChange={value => {
                updateSettings({ yAxisLabel: value as string });
              }}
            />
          </div>
        )}
        {settings.plotType === 'histogram' && (
          <div className="mb-2">
            <Input
              type="dropdown"
              label="Aggregation:"
              value={settings.groupData}
              options={vis1DHistogramAggregationOptions}
              onChange={value => {
                updateSettings({ groupData: value as string });
              }}
            />
          </div>
        )}
        {(settings.plotType === 'bar' || settings.plotType === 'scatter' || settings.plotType === 'histogram') && (
          <div className="mb-2">
            <Input
              type="dropdown"
              label="Color:"
              value={settings.zAxisLabel}
              options={attributeOptions}
              onChange={value => {
                updateSettings({ zAxisLabel: value as string });
              }}
            />
          </div>
        )}
        {settings.plotType === 'histogram' && (
          <div className="mb-2">
            <Input type="boolean" label="Normalize: " value={settings.stack} onChange={val => updateSettings({ stack: val })} />
          </div>
        )}
        {settings.xAxisLabel?.includes('Datum') && (
          <div className="mb-2">
            <Input
              type="dropdown"
              label="Group Time:"
              value={settings.groupData}
              options={['', 'monthly', 'quarterly', 'yearly']}
              onChange={value => {
                updateSettings({ groupData: value as string });
              }}
            />
          </div>
        )}
        <div className="mb-2">
          <Input type="boolean" label="Show axis" value={settings.showAxis} onChange={val => updateSettings({ showAxis: val })} />
        </div>
      </div>
    </SettingsContainer>
  );
};

const Vis1DRef = React.createRef<Vis1DVisHandle>();

export const Vis1DComponent: VISComponentType<Vis1DProps> = {
  component: React.forwardRef((props: VisualizationPropTypes<Vis1DProps>, ref) => <Vis1D {...props} ref={Vis1DRef} />),
  settingsComponent: Vis1DSettings,
  settings: defaultSettings,
  exportImage: () => {
    if (Vis1DRef.current) {
      Vis1DRef.current.exportImageInternal();
    } else {
      console.error('1Dvis reference is not set.');
    }
  },
};

export default Vis1DComponent;
