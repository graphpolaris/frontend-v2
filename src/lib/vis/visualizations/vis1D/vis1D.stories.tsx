import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import Vis1DComponent from './Vis1D';

const Component: Meta<typeof Vis1DComponent.component> = {
  title: 'Visualizations/1DVis',
  component: Vis1DComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

const Mockstore: any = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
  },
});

const mockData = {
  nodes: [
    {
      id: 1,
      label: 'Person',
      attributes: {
        born: 1923,
        name: 'John',
      },
    },
    {
      id: 2,
      label: 'Person',
      attributes: {
        born: 1933,
        name: 'May',
      },
    },
    {
      id: 3,
      label: 'Person',
      attributes: {
        born: 1943,
        name: 'Tom',
      },
    },
  ],
  edges: [],
};

export const TestBarplot = {
  args: {
    data: mockData,
    ml: {},
    settings: {
      ...Vis1DComponent.settings,
      plotType: 'bar',
      title: 'Bar Plot - Born Years',
      selectedEntity: 'Person',
      xAxisLabel: 'born',
    },
  },
};

export const TestScatterplot = {
  args: {
    data: mockData,
    ml: {},
    settings: {
      ...Vis1DComponent.settings,
      plotType: 'scatter',
      title: 'Scatter Plot - Born Years',
      selectedEntity: 'Person',
      xAxisLabel: 'born',
      yAxisLabel: 'born',
    },
  },
};

export const TestLineplot = {
  args: {
    data: mockData,
    ml: {},
    settings: {
      ...Vis1DComponent.settings,
      plotType: 'line',
      title: 'Line Plot - Born Years',
      selectedEntity: 'Person',
      xAxisLabel: 'born',
      yAxisLabel: 'born',
    },
  },
};

export const TestHistogram = {
  args: {
    data: mockData,
    ml: {},
    settings: {
      ...Vis1DComponent.settings,
      plotType: 'histogram',
      title: 'Histogram - Born Years',
      selectedEntity: 'Person',
      xAxisLabel: 'born',
      stack: true,
    },
  },
};

export const TestPiechart = {
  args: {
    data: mockData,
    ml: {},
    settings: {
      ...Vis1DComponent.settings,
      plotType: 'pie',
      title: 'Pie Chart - Born Years',
      selectedEntity: 'Person',
      xAxisLabel: 'born',
    },
  },
};

export default Component;
