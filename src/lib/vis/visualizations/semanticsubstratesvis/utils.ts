import { visualizationColors } from '@/config';
import { MultiGraph } from 'graphology';
import { GraphData } from './components/types';
import { NodeQueryResult } from '@/lib/data-access/store/graphQueryResultSlice';

export const noDataRange = [-1, 1];
export const noSelection = 'None';

const buildGraphology = (data: GraphData): MultiGraph => {
  const graph = new MultiGraph();

  const nodeMap = new Map<string, NodeQueryResult>();
  data.nodes.forEach(node => {
    nodeMap.set(node._id, node);
  });

  const nodeEntries = data.nodes.map(node => ({
    key: node._id,
    attributes: {
      ...node.attributes,
      label: node.label,
    },
  }));

  graph.import({ nodes: nodeEntries });

  const validEdgeEntries = data.edges
    .filter(edge => nodeMap.has(edge.from) && nodeMap.has(edge.to))
    .map(edge => ({
      source: edge.from,
      target: edge.to,
    }));

  graph.import({ edges: validEdgeEntries });

  return graph;
};

const numColorsCategorical = visualizationColors.GPCat.colors[14].length;
export const isColorCircleFix = false;

let config: any = {};
if (isColorCircleFix) {
  config = {
    circle: {
      fillClr: 'hsl(var(--clr-sec--500))',
      strokeClr: 'hsl(var(--clr-sec--600))',
    },
    edges: {
      stroke: 'bg-primary-800',
      strokeWidth: '2',
      strokeOpacity: '0.7',
    },

    brush: {
      fillClr: 'hsl(var(--clr-sec--100))',
      strokeClr: 'black',
    },
  };
} else {
  config = {
    circle: {
      fillClr: 'NO_USED',
      strokeClr: 'hsl(var(--clr-sec--500))',
    },
    edges: {
      stroke: 'hsl(var(--clr-sec--300))',
      strokeWidth: '2',
      strokeOpacity: '0.7',
    },
    brush: {
      fillClr: 'hsl(var(--clr-sec--100))',
      strokeClr: 'black',
    },
  };
}

const marginAxis = 0.2;

export { buildGraphology, numColorsCategorical, config, marginAxis };
