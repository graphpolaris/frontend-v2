import { UserSelection, RegionData, AugmentedNodeAttributes, connectionFromTo, IdConnections, edgeVisibility } from './types';
import { extent } from 'd3';
import { RefObject } from 'react';
import { visualizationColors } from '@/config';
import { MultiGraph } from 'graphology';

export function findConnectionsNodes(
  queryIDs: string[],
  originIDs: string[],
  graphStructure: MultiGraph,
  labelNode: string,
  invert: boolean = false,
): [connectionFromTo[], string[]] {
  const neighborMap: IdConnections = {};

  originIDs.forEach(nodeId => {
    const tempSet: Set<string> = new Set();
    graphStructure.forEachNeighbor(nodeId, (neighbor, attributes) => {
      if (attributes.label != labelNode) {
        graphStructure.forEachNeighbor(neighbor, (neighbor2, attributes2) => {
          if (queryIDs.includes(neighbor2) && neighbor2 !== nodeId) {
            tempSet.add(neighbor2);
          }
        });
      } else {
        if (queryIDs.includes(neighbor) && neighbor !== nodeId) {
          tempSet.add(neighbor);
        }
      }
    });
    neighborMap[nodeId] = Array.from(tempSet);
  });

  const edgeStrings = wrapperForEdge(neighborMap, invert);
  const edgeStrings2 = wrapperForEdgeString(neighborMap, invert);

  return [edgeStrings, edgeStrings2];
}
export function filterArray(ids1: string[], ids2: string[], targetArray: string[], delimiter: string): string[] {
  return targetArray.filter(item => {
    const [firstPart, secondPart] = item.split(delimiter);

    const ids1Match = ids1.length === 0 || ids1.some(id => firstPart == id);
    const ids2Match = ids2.length === 0 || ids2.some(id => secondPart == id);

    return ids1Match && ids2Match;
  });
}

export function filterArray2(ids1: string[], ids2: string[], targetArray: string[]): string[] {
  const filteredWithIds1 = targetArray.filter(item => {
    return ids1.every(id => item.includes(id));
  });

  return filteredWithIds1.filter(item => {
    return ids2.every(id => item.includes(id));
  });
}

export function getRegionData(nodes: AugmentedNodeAttributes[], regionUserSelection: UserSelection): RegionData {
  // when one of the fields is an array the elemnts in ConfigPanel are joined with "-".
  // then regionUserSelection.attributeAsRegionSelection will be a string of the elements joined by "-"
  // that is why item.attributes[regionUserSelection.attributeAsRegion] is join with ("-")

  let filteredData: AugmentedNodeAttributes[] = [];

  if (!regionUserSelection.attributeAsRegion) {
    filteredData = nodes.filter((item: AugmentedNodeAttributes) => {
      return item.label === regionUserSelection.nodeName;
    });
  } else {
    filteredData = nodes.filter((item: AugmentedNodeAttributes) => {
      return (
        item.label === regionUserSelection.nodeName &&
        item.attributes &&
        regionUserSelection.attributeAsRegion &&
        (Array.isArray(item.attributes[regionUserSelection.attributeAsRegion])
          ? (item.attributes[regionUserSelection.attributeAsRegion] as string[]).join('-') ===
            regionUserSelection.attributeAsRegionSelection
          : item.attributes[regionUserSelection.attributeAsRegion] === regionUserSelection.attributeAsRegionSelection)
      );
    });
  }

  if (filteredData.length === 0) filteredData = [];

  const idData: string[] = filteredData.map(item => item._id);
  let xAxis: any[] = [];
  if (regionUserSelection.placement.xAxis != '') {
    xAxis = filteredData.map(item => {
      const value = item.attributes[regionUserSelection.placement.xAxis as keyof typeof item.attributes];
      return value;
    });
  }

  let yAxis: any[] = [];
  if (regionUserSelection.placement.yAxis != '') {
    yAxis = filteredData.map(item => {
      const value = item.attributes[regionUserSelection.placement.yAxis as keyof typeof item.attributes];
      return value;
    });
  }

  const regionData = {
    name: regionUserSelection.name,
    xData: xAxis,
    yData: yAxis,
    idData: idData,
    colorNodes: regionUserSelection.placement.colorNodes,
    colorNodesStroke: regionUserSelection.placement.colorNodesStroke,
    colorBrush: regionUserSelection.placement.colorFillBrush,
    colorBrushStroke: regionUserSelection.placement.colorStrokeBrush,
    nodeName: regionUserSelection.nodeName as string,
    attributeName: regionUserSelection.attributeAsRegion as string,
    attributeSelected: regionUserSelection.attributeAsRegionSelection as string,
    xAxisName: regionUserSelection.placement.xAxis as string,
    yAxisName: regionUserSelection.placement.yAxis as string,
    label: filteredData?.[0]?.label || nodes[0].label,
  };

  return regionData;
}

export function setExtension(margin: number, data: number[]): [number, number] {
  const extentData: [number, number] = extent(data) as [number, number];

  if (extentData[0] >= 0.0 && extentData[1] > 0.0) {
    return [extentData[0] * (1.0 - margin), extentData[1] * (1.0 + margin)];
  } else if (extentData[0] <= 0.0 && extentData[1] >= 0.0) {
    return [extentData[0] * (1.0 + margin), extentData[1] * (1.0 + margin)];
  } else {
    return [extentData[0] * (1.0 + margin), extentData[1] * (1.0 - margin)];
  }
}

export function getUniqueValues(arr: any[]): any[] {
  return [...new Set(arr)];
}

export function findSimilarElements(array1: string[], array2: string[]): string[] {
  return array1.filter(element => array2.includes(element));
}

export function wrapperForEdge(data: IdConnections, invert: boolean = false): connectionFromTo[] {
  const keysData = Object.keys(data);
  const resultsDas: connectionFromTo[] = [];
  const results = keysData.forEach(item => {
    const r = data[item].forEach(itemConnected => {
      if (!invert) {
        resultsDas.push({ from: item, to: itemConnected });
      } else {
        resultsDas.push({ from: itemConnected, to: item });
      }
    });
  });
  return resultsDas;
}

export function wrapperForEdgeString(data: IdConnections, invert: boolean = false): string[] {
  const keysData = Object.keys(data);
  const resultsDas: string[] = [];
  const results = keysData.forEach(item => {
    const r = data[item].forEach(itemConnected => {
      if (!invert) {
        resultsDas.push(item + '_fromto_' + itemConnected);
      } else {
        resultsDas.push(itemConnected + '_fromto_' + item);
      }
    });
  });
  return resultsDas;
}

export function wrapperEdgeVisibility(data: connectionFromTo[]): edgeVisibility[] {
  const transformedArray: edgeVisibility[] = data.map(function (item) {
    const from_stringModified = item.from.replace('/', '_');
    const to_stringModified = item.to.replace('/', '_');
    const elementString = `${from_stringModified}_fromto_${to_stringModified}`;
    return { _id: elementString, from: true, to: true, visibility: true };
  });

  return transformedArray;
}

export function updateConnections(regionA: RefObject<string[][]>, regionB: RefObject<string[][]>, nameEdges: RefObject<string[]>) {
  if (regionA.current && regionB.current && nameEdges.current) {
    let edgeVisible: string[] = [];
    for (let idxEdges = 0; idxEdges < regionB.current.length; idxEdges++) {
      if (regionA.current[idxEdges].length != 0 && regionB.current[idxEdges].length != 0) {
        edgeVisible = findSimilarElements(regionA.current[idxEdges], regionB.current[idxEdges]);
      } else if (regionA.current[idxEdges].length == 0 && regionB.current[idxEdges].length != 0) {
        edgeVisible = regionB.current[idxEdges];
      } else if (regionA.current[idxEdges].length != 0 && regionB.current[idxEdges].length == 0) {
        edgeVisible = regionA.current[idxEdges];
      } else if (regionA.current[idxEdges].length == 0 && regionB.current[idxEdges].length == 0) {
        edgeVisible = [];
      }
    }
  } else {
    console.error('Either regionA or regionB is null.');
  }
}

export function partiallyContainedElements(arr1: string[], arr2: string[]): string[] {
  const partiallyContained: string[] = [];

  for (const str1 of arr1) {
    for (const str2 of arr2) {
      if (str2.includes(str1) || str1.includes(str2)) {
        partiallyContained.push(str2);
      }
    }
  }

  return partiallyContained;
}

export function isNumeric(str: string): boolean {
  if (typeof str !== 'string') return false;
  return !isNaN(Number(str)) && !isNaN(parseFloat(str));
}

export function calcTextWidth(textArray: string | string[], maxLengthAllowed: number, tailwindStyle: string): string[] {
  const labels = Array.isArray(textArray) ? textArray : [textArray];

  const truncatedLabels: string[] = [];

  labels.forEach((rowLabel, index) => {
    let truncatedText = rowLabel || '';
    let truncatedWidth = 0;

    while (truncatedText.length > 0) {
      const tempElement = document.createElement('span');
      tempElement.style.position = 'absolute';
      tempElement.style.visibility = 'hidden';
      tempElement.style.pointerEvents = 'none';
      tempElement.style.whiteSpace = 'nowrap';
      tempElement.style.font = getComputedStyle(document.body).font;

      tempElement.className = tailwindStyle;

      tempElement.textContent = truncatedText;
      document.body.appendChild(tempElement);

      truncatedWidth = tempElement.getBoundingClientRect().width;
      document.body.removeChild(tempElement);

      if (truncatedWidth <= maxLengthAllowed) {
        break;
      }

      truncatedText = truncatedText.slice(0, -1);
    }

    if (truncatedText !== rowLabel) {
      truncatedLabels.push(truncatedText + '...');
    } else {
      truncatedLabels.push(truncatedText);
    }
  });

  return truncatedLabels;
}

export function calcTextWidthCanvas(rowLabels: string[], maxLengthAllowed: number): string[] {
  rowLabels.forEach((rowLabel, index) => {
    let truncatedText = rowLabel;
    let truncatedWidth = 0;

    const c = document.createElement('canvas');
    const ctx = c.getContext('2d') as CanvasRenderingContext2D;
    ctx.font = getComputedStyle(document.body).font;
    while (truncatedText.length > 0) {
      truncatedWidth = ctx.measureText(truncatedText).width;

      if (truncatedWidth <= maxLengthAllowed) {
        // If the truncated text fits within the allowed width, break the loop
        break;
      }

      // Remove the last character and try again
      truncatedText = truncatedText.slice(0, -1);
    }

    if (truncatedText !== rowLabel) {
      // If the label was truncated, update the original rowLabels array
      rowLabels[index] = truncatedText + '...';
    }
  });

  return rowLabels;
}

export function nodeColorHex(num: number) {
  //let entityColors = Object.values(visualizationColors.GPSeq.colors[9]);
  const col = visualizationColors.GPCat.colors[14][num % visualizationColors.GPCat.colors[14].length];
  return col;
}
