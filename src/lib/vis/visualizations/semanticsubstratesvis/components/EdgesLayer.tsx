import React, { useRef, useMemo } from 'react';
import { DataConnection, VisualRegionConfig, VisualEdgesConfig, DataPoint } from './types';

export type EdgesLayerProps = {
  dataConnections: DataConnection[];
  visualConfig: React.MutableRefObject<VisualEdgesConfig>;
  visualScatterplot: VisualRegionConfig;
  data1: DataPoint[];
  data2: DataPoint[];
  nameEdges: string;
};

export type KeyedEdgesLayerProps = EdgesLayerProps & {
  key: string;
};

interface dataPointEdge {
  start: number[];
  end: number[];
}

function calculateDistance(x1: number, y1: number, x2: number, y2: number): number {
  const deltaX = x2 - x1;
  const deltaY = y2 - y1;
  const distance = Math.sqrt(deltaX ** 2 + deltaY ** 2);

  return distance;
}

function edgeGenerator(dataPoint: dataPointEdge): string {
  /*
                     QuadracticPoint
                            |
                            |
    fromPoint --------- QuadinBetween ------------------------ toPoint

  */

  const [startX, startY]: number[] = dataPoint.start;
  const [endX, endY]: number[] = dataPoint.end;

  // compute distance
  const distance = calculateDistance(startX, startY, endX, endY);
  //const distance = calculateDistance(endX,endY,startX,startY);

  // unit vector

  const xUnit = (startX - endX) / distance;
  const yUnit = (startY - endY) / distance;

  // Get QuadracticPointOnline
  const marginQuadinBetween = 0.75;
  const percentageDistancePerdpendicular = 0.1;

  const xQuadinBetween = startX - xUnit * marginQuadinBetween * distance;
  const yQuadinBetween = startY - yUnit * marginQuadinBetween * distance;

  // Get QuadracticPoint
  const xQuadracticPoint = xQuadinBetween - yUnit * percentageDistancePerdpendicular * distance;
  const yQuadracticPoint = yQuadinBetween + xUnit * percentageDistancePerdpendicular * distance;

  const path = `M ${startX}, ${startY} Q ${xQuadracticPoint},${yQuadracticPoint} ${endX} ${endY}`;

  return path;
}

const EdgesLayer: React.FC<EdgesLayerProps> = ({ dataConnections, visualConfig, data1, data2, nameEdges, visualScatterplot }) => {
  const svgRef = useRef(null);

  const [dataVis, dataEdgeIds] = useMemo(() => {
    const data1_id = data1.map(item => item.id);
    const data1_x = data1.map(item => item.x);
    const data1_y = data1.map(item => item.y);

    const data2_id = data2.map(item => item.id);
    const data2_x = data2.map(item => item.x);
    const data2_y = data2.map(item => item.y);

    const heightRegion = visualConfig.current.configRegion.height;

    const svgToRegion1 = [visualScatterplot.margin.left, visualConfig.current.configRegion.margin.top + 0 * heightRegion];
    const svgToRegion2 = [visualScatterplot.margin.left, visualConfig.current.configRegion.margin.top + 1 * heightRegion];

    const dataVis: dataPointEdge[] = [];
    const dataEdgeIds: string[] = [];

    dataConnections.forEach((value: DataConnection) => {
      const indexID_region1 = data1_id.findIndex(idInstance => idInstance === value.from);

      const startX_region1 = data1_x[indexID_region1] + svgToRegion1[0];
      const startY_region1 = data1_y[indexID_region1] + svgToRegion1[1];

      const indexID_region2 = data2_id.findIndex(idInstance => idInstance === value.to);
      const startX_region2 = data2_x[indexID_region2] + svgToRegion2[0];
      const startY_region2 = data2_y[indexID_region2] + svgToRegion2[1] + visualConfig.current.offsetY;

      dataVis.push({ start: [startX_region1, startY_region1], end: [startX_region2, startY_region2] });

      let from_stringModified = value.from.replace('/', '_');
      const to_stringModified = value.to.replace('/', '_');

      if (!isNaN(parseInt(from_stringModified))) {
        from_stringModified = 'idAdd_' + from_stringModified;
      }

      dataEdgeIds.push(`${from_stringModified}_fromto_${to_stringModified}`);
    });
    return [dataVis, dataEdgeIds];
  }, [dataConnections, visualConfig, data1, data2, nameEdges]);

  return (
    <svg
      ref={svgRef}
      width={visualScatterplot.width}
      height={visualConfig.current.height}
      //preserveAspectRatio="xMidYMid meet"
      //viewBox={`0 0 ${visualScatterplot.width} ${visualConfig.current.height}`}
    >
      <g className={nameEdges}>
        {dataVis.map((edgeData, index) => (
          <path
            key={dataEdgeIds[index]}
            d={edgeGenerator(edgeData)}
            className={dataEdgeIds[index]}
            fill="none"
            stroke={visualConfig.current.stroke}
            strokeWidth={visualConfig.current.strokeWidth}
            strokeOpacity={visualConfig.current.strokeOpacity}
          />
        ))}
      </g>
    </svg>
  );
};

export default EdgesLayer;
