import React, { useEffect, useRef, useState, useMemo } from 'react';
import {
  select,
  scaleBand,
  axisBottom,
  scaleLinear,
  forceX,
  forceY,
  brush,
  forceCollide,
  format,
  axisLeft,
  forceSimulation,
  Axis,
  NumberValue,
  Selection,
  ScaleBand,
  ScaleLinear,
} from 'd3';
import { VisualRegionConfig, RegionData, DataPoint, DataPointXY } from './types';
import { calcTextWidth } from './utils';
import { Icon } from '@/lib/components/icon';
import { EntityPill } from '@/lib/components/pills/Pill';
import { noDataRange } from '../utils';

export type ScatterplotProps = {
  data: RegionData;
  visualConfig: VisualRegionConfig;
  xScaleRange: string[] | number[];
  yScaleRange: string[] | number[];
  onBrushUpdate: (idElements: string[], selectedElement: string) => void;
  onBrushClear: (selectedElement: string) => void;
  onResultJitter: (data: DataPoint[], idScatterplot: number) => void;
};

export type KeyedScatterplotProps = ScatterplotProps & {
  key: number;
};

function computeRadiusPoints(width: number, numPoints: number): number {
  const radius: number = numPoints >= 170 ? width * 0.0032 : width * 0.004;

  return radius;
}

export const Scatterplot: React.FC<ScatterplotProps> = ({
  data,
  visualConfig,
  xScaleRange,
  yScaleRange,
  onBrushUpdate,
  onBrushClear,
  onResultJitter,
}) => {
  const svgRef = useRef(null);
  const groupMarginRef = useRef<SVGGElement>(null as any);
  const brushRef = useRef<SVGGElement>(null as any);

  const [textXLabel, setTextXLabel] = useState('');
  const [textYLabel, setTextYLabel] = useState('');

  const idScatterplot = useMemo((): number => {
    return parseInt(data.name.split('_')[1], 10);
  }, [data.name]);

  const configStyle = useMemo(() => {
    return {
      colorText: 'hsl(var(--clr-sec--800))',
      colorTextUnselect: 'hsl(var(--clr-sec--400))',
      colorLinesHyperEdge: 'hsl(var(--clr-black))',
    };
  }, []);

  const styleTextXaxisLabel = useMemo(
    () => ({
      classTextXAxis: 'font-data font-secondary font-semibold text-right capitalize text-xs',
      x: 1.01 * visualConfig.widthMargin + visualConfig.margin.right,
      y: visualConfig.heightMargin + 1.25 * visualConfig.margin.bottom,
      textAnchor: 'start',
      dominantBaseline: 'middle',
      maxLengthText: 90,
    }),
    [visualConfig.widthMargin, visualConfig.margin.right, visualConfig.heightMargin, visualConfig.margin.bottom],
  );

  useEffect(() => {
    const maxLengthAllowedAxisY: number = 85;
    const maxLengthAllowedAxisX: number = 50;

    const svg = select(svgRef.current);

    const groupMargin = select(groupMarginRef.current).attr(
      'transform',
      `translate(${visualConfig.margin.left},${visualConfig.margin.top})`,
    );

    const maxComputations = 200;
    let tickCount = 0;
    let dataCircles: DataPointXY[] = [];

    let dataCirclesXtemp: number[] = [];
    let dataCirclesYtemp: number[] = [];
    let xOffset: number = 0;
    let yOffset: number = 0;

    let xAxis: Axis<string> | ((selection: Selection<SVGGElement, unknown, null, undefined>) => void) | Axis<NumberValue>;
    let yAxis;

    let xAxisType: string = 'none'; //'linear', 'band','none'
    let yAxisType: string = 'none'; //'linear', 'band','none'

    let xScaleTemp: ScaleBand<string> | ScaleLinear<number, number>;
    let yScaleTemp: ScaleBand<string> | ScaleLinear<number, number>;

    // for updating axis.
    let yAxisGroup = groupMargin.select<SVGGElement>(`.${data.name}yAxis`);
    if (yAxisGroup.empty()) {
      yAxisGroup = groupMargin.append('g').attr('class', `${data.name}yAxis`);
    }

    let xAxisGroup = groupMargin.select<SVGGElement>(`.${data.name}xAxis`);
    if (xAxisGroup.empty()) {
      xAxisGroup = groupMargin
        .append('g')
        .attr('class', `${data.name}xAxis`)
        .attr('transform', `translate(0, ${visualConfig.heightMargin})`);
    }

    if (!data.xAxisName && !data.yAxisName) {
      dataCirclesXtemp = Array(data.xData.length).fill(visualConfig.widthMargin * 0.5); // place dots at the center of the svg
      dataCirclesYtemp = Array(data.yData.length).fill(visualConfig.heightMargin * 0.5);
      dataCircles = data.xData.map((value, index) => ({ x: dataCirclesXtemp[index], y: dataCirclesYtemp[index] }));

      const radius = computeRadiusPoints(visualConfig.width, data.xData.length);

      const simulation = forceSimulation<DataPointXY>(dataCircles)
        .force('x', forceX<DataPointXY>(d => d.x).strength(0.1))
        .force('y', forceY<DataPointXY>(d => d.y).strength(4))
        .force('collide', forceCollide(radius * 1.25).strength(2));

      const circles = groupMargin.selectAll('circle').data(dataCircles);

      circles
        .enter()
        .append('circle')
        .attr('class', (d, i) => `${data.idData[i]}`)
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
        .attr('r', radius)
        .attr('stroke', data.colorNodesStroke)
        .attr('fill', data.colorNodes);

      circles.exit().remove();

      simulation.on('tick', function () {
        groupMargin
          .selectAll<SVGCircleElement, DataPointXY>('circle')
          .attr('cx', (d: DataPointXY) => d.x)
          .attr('cy', (d: DataPointXY) => d.y);

        tickCount++;
        if (tickCount > maxComputations) {
          const dataSimulation: DataPoint[] = dataCircles.map(({ x, y }, i) => ({
            x,
            y,
            id: data.idData[i],
          }));
          onResultJitter(dataSimulation, idScatterplot);

          simulation.stop();
        }
      });

      xScaleTemp = scaleBand<string>()
        .domain(xScaleRange as string[])
        .range([0, visualConfig.widthMargin])
        .paddingOuter(0);

      yScaleTemp = scaleBand<string>()
        .domain(yScaleRange as string[])
        .range([visualConfig.heightMargin, 0])
        .paddingOuter(0);

      xAxisType = 'none';
      yAxisType = 'none';
      yAxis = axisLeft(yScaleTemp).tickValues([]);
      xAxis = axisBottom(xScaleTemp).tickValues([]);
      yAxisGroup.call(yAxis);
      xAxisGroup.attr('transform', 'translate(0,' + visualConfig.heightMargin + ')').call(xAxis);
    } else if (!!data.xAxisName && !!data.yAxisName) {
      if (typeof data.xData[0] != 'number') {
        xScaleTemp = scaleBand<string>()
          .domain(xScaleRange as string[])
          .range([0, visualConfig.widthMargin])
          .paddingOuter(0);
        xAxisType = 'band';

        xOffset = 0.5 * xScaleTemp.bandwidth();

        dataCirclesXtemp = data.xData.map((value, index) => {
          const scaledValue = xScaleTemp(value);
          if (scaledValue !== undefined) {
            return scaledValue + xOffset;
          } else {
            return 0;
          }
        });
        const TextTicks = calcTextWidth(xScaleRange as string[], maxLengthAllowedAxisX, styleTextXaxisLabel.classTextXAxis);

        xAxis = axisBottom(xScaleTemp)
          .tickFormat((d, i) => TextTicks[i])
          .tickSizeOuter(0);

        xAxisGroup
          .attr('transform', 'translate(0,' + visualConfig.heightMargin + ')')
          .call(xAxis)
          .selectAll('text')
          .style('text-anchor', 'start')
          .attr('x', '10')
          .attr('y', '0')
          .attr('dy', '0')
          .style('dominant-baseline', styleTextXaxisLabel.dominantBaseline)
          .attr('transform', 'rotate(90)');
      } else {
        xScaleTemp = scaleLinear<number>()
          .domain(xScaleRange as number[])
          .range([0, visualConfig.widthMargin]);
        xAxisType = 'linear';
        dataCirclesXtemp = data.xData.map((value, index) => {
          const scaledValue = xScaleTemp(value);

          if (scaledValue !== undefined) {
            return scaledValue;
          } else {
            return 0;
          }
        });

        const [minValueX, maxValueX]: number[] = xScaleTemp.domain();
        const averageMinMaxX: number = Math.round((minValueX + maxValueX) / 2.0);

        xAxis = axisBottom(xScaleTemp)
          .tickValues([minValueX, 0.5 * (minValueX + averageMinMaxX), averageMinMaxX, 0.5 * (maxValueX + averageMinMaxX), maxValueX])
          .tickFormat(format('.2s'));

        xAxisGroup.attr('transform', 'translate(0,' + visualConfig.heightMargin + ')').call(xAxis);
      }

      if (typeof data.yData[0] != 'number') {
        yScaleTemp = scaleBand<string>()
          .domain(yScaleRange as string[])
          .range([visualConfig.heightMargin, 0])
          .paddingOuter(0);
        yAxisType = 'band';
        yOffset = 0.5 * yScaleTemp.bandwidth();

        dataCirclesYtemp = data.yData.map((value, index) => {
          //const scaledValue = typeof value === 'number' ? yScaleTemp(value.toString()) : yScaleTemp(value);
          const scaledValue = yScaleTemp(value);

          if (scaledValue !== undefined) {
            return scaledValue + yOffset;
          } else {
            return 0;
          }
        });

        const textTicks = calcTextWidth(yScaleRange as string[], maxLengthAllowedAxisY, styleTextXaxisLabel.classTextXAxis);

        yAxis = axisLeft(yScaleTemp)
          .tickFormat((d, i) => textTicks[i])
          .tickSizeOuter(0);
        yAxisGroup.call(yAxis).selectAll('text');
      } else {
        yScaleTemp = scaleLinear<number>()
          .domain(yScaleRange as number[])
          .range([visualConfig.heightMargin, 0]);
        yAxisType = 'linear';
        dataCirclesYtemp = data.yData.map((value, index) => {
          const scaledValue = yScaleTemp(value);

          if (scaledValue !== undefined) {
            return scaledValue;
          } else {
            return 0;
          }
        });

        const [minValueX, maxValueX]: number[] = yScaleTemp.domain();
        const averageMinMaxX: number = Math.round((minValueX + maxValueX) / 2.0);

        yAxis = axisLeft(yScaleTemp)
          .tickValues([minValueX, 0.5 * (minValueX + averageMinMaxX), averageMinMaxX, 0.5 * (maxValueX + averageMinMaxX), maxValueX])
          .tickFormat(format('.2s'));

        yAxisGroup.call(yAxis);
      }

      dataCircles = data.xData.map((value, index) => ({ x: dataCirclesXtemp[index], y: dataCirclesYtemp[index] }));
      const radius = computeRadiusPoints(visualConfig.width, data.xData.length);

      const circles = groupMargin.selectAll('circle').data(dataCircles);
      circles
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
        .attr('r', radius)
        .attr('stroke', data.colorNodesStroke)
        .attr('fill', data.colorNodes);

      circles
        .enter()
        .append('circle')
        .attr('class', (d, i) => `${data.idData[i]}`)
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
        .attr('r', radius)
        .attr('stroke', data.colorNodesStroke)
        .attr('fill', data.colorNodes);

      circles.exit().remove();

      const dataSimulation: DataPoint[] = dataCircles.map(({ x, y }, i) => ({
        x: x,
        y: y,
        id: data.idData[i],
      }));

      onResultJitter(dataSimulation, idScatterplot);
    } else if (data.yAxisName) {
      if (typeof data.yData[0] != 'number') {
        yScaleTemp = scaleBand<string>()
          .domain(yScaleRange as string[])
          .range([visualConfig.heightMargin, 0])
          .paddingOuter(0);

        yOffset = 0.5 * yScaleTemp.bandwidth();
        yAxisType = 'band';
        dataCirclesYtemp = data.yData.map((value, index) => {
          //const scaledValue = typeof value === 'number' ? yScaleTemp(value.toString()) : yScaleTemp(value);
          const scaledValue = yScaleTemp(value);

          if (scaledValue !== undefined) {
            return scaledValue + yOffset;
          } else {
            return 0;
          }
        });

        const textTicks = calcTextWidth(yScaleRange as string[], maxLengthAllowedAxisY, styleTextXaxisLabel.classTextXAxis);

        yAxis = axisLeft(yScaleTemp)
          .tickFormat((d, i) => textTicks[i])
          .tickSizeOuter(0);
        yAxisGroup.call(yAxis).selectAll('text');
      } else {
        yScaleTemp = scaleLinear<number>()
          .domain(yScaleRange as number[])
          .range([visualConfig.heightMargin, 0]);
        yAxisType = 'linear';
        dataCirclesYtemp = data.yData.map((value, index) => {
          const scaledValue = yScaleTemp(value);

          if (scaledValue !== undefined) {
            return scaledValue;
          } else {
            return 0;
          }
        });

        const [minValueX, maxValueX]: number[] = yScaleTemp.domain();
        const averageMinMaxX: number = Math.round((minValueX + maxValueX) / 2.0);

        yAxis = axisLeft(yScaleTemp)
          .tickValues([minValueX, 0.5 * (minValueX + averageMinMaxX), averageMinMaxX, 0.5 * (maxValueX + averageMinMaxX), maxValueX])
          .tickFormat(format('.2s'));

        yAxisGroup.call(yAxis);
      }

      xScaleTemp = scaleLinear<number, number>()
        .domain(noDataRange as number[])
        .range([0, visualConfig.widthMargin]);

      const valueXMiddle = visualConfig.widthMargin / 2;
      dataCircles = data.yData.map((value, index) => ({ x: valueXMiddle, y: dataCirclesYtemp[index] }));

      const radius = computeRadiusPoints(visualConfig.width, data.yData.length);
      const simulation = forceSimulation<DataPointXY>(dataCircles)
        .force('x', forceX<DataPointXY>(d => d.x).strength(0.1))
        .force('y', forceY<DataPointXY>(d => d.y).strength(4))
        .force('collide', forceCollide(radius * 1.25).strength(0.5));

      const circles = groupMargin.selectAll('circle').data(dataCircles);
      circles
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
        .attr('r', radius)
        .attr('stroke', data.colorNodesStroke)
        .attr('fill', data.colorNodes);
      circles
        .enter()
        .append('circle')
        .attr('class', (d, i) => `${data.idData[i]}`)
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
        .attr('r', radius)
        .attr('stroke', data.colorNodesStroke)
        .attr('fill', data.colorNodes);
      circles.exit().remove();

      simulation.on('tick', function () {
        groupMargin
          .selectAll<SVGCircleElement, DataPointXY>('circle')
          .attr('cx', (d: DataPointXY) => d.x)
          .attr('cy', (d: DataPointXY) => d.y);

        tickCount++;
        if (tickCount > maxComputations) {
          const dataSimulation: DataPoint[] = dataCircles.map(({ x, y }, i) => ({
            x,
            y,
            id: data.idData[i],
          }));
          onResultJitter(dataSimulation, idScatterplot);

          simulation.stop();
        }
      });

      xAxis = axisBottom(xScaleTemp).tickValues([]);
      xAxisGroup.attr('transform', 'translate(0,' + visualConfig.heightMargin + ')').call(xAxis);
    } else if (data.xAxisName) {
      if (typeof data.xData[0] != 'number') {
        xScaleTemp = scaleBand<string>()
          .domain(xScaleRange as string[])
          .range([0, visualConfig.widthMargin])
          .paddingOuter(0);

        xOffset = 0.5 * xScaleTemp.bandwidth();
        xAxisType = 'band';
        dataCirclesXtemp = data.xData.map((value, index) => {
          const scaledValue = xScaleTemp(value);

          if (scaledValue !== undefined) {
            return scaledValue + xOffset;
          } else {
            return 0;
          }
        });

        const textTicks = calcTextWidth(xScaleRange as string[], maxLengthAllowedAxisX, styleTextXaxisLabel.classTextXAxis);

        xAxis = axisBottom(xScaleTemp)
          .tickFormat((d, i) => textTicks[i])
          .tickSizeOuter(0);
        xAxisGroup
          .attr('transform', 'translate(0,' + visualConfig.heightMargin + ')')
          .call(xAxis)
          .selectAll('text')
          .style('text-anchor', 'start')
          .attr('x', '10')
          .attr('y', '0')
          .attr('dy', '0')
          .style('dominant-baseline', styleTextXaxisLabel.dominantBaseline)
          .attr('transform', 'rotate(90)');
      } else {
        xScaleTemp = scaleLinear<number>()
          .domain(xScaleRange as number[])
          .range([0, visualConfig.widthMargin]);
        xAxisType = 'linear';
        dataCirclesXtemp = data.xData.map((value, index) => {
          const scaledValue = xScaleTemp(value);
          if (scaledValue !== undefined) {
            return scaledValue;
          } else {
            return 0;
          }
        });

        const [minValueX, maxValueX]: number[] = xScaleTemp.domain();
        const averageMinMaxX: number = Math.round((minValueX + maxValueX) / 2.0);

        xAxis = axisBottom(xScaleTemp)
          .tickValues([minValueX, 0.5 * (minValueX + averageMinMaxX), averageMinMaxX, 0.5 * (maxValueX + averageMinMaxX), maxValueX])
          .tickFormat(format('.2s'));
        xAxisGroup.attr('transform', 'translate(0,' + visualConfig.heightMargin + ')').call(xAxis);
      }

      yScaleTemp = scaleLinear<number>()
        .domain(noDataRange as number[])
        .range([visualConfig.heightMargin, 0]);

      const valueYMiddle = visualConfig.heightMargin / 2;
      dataCircles = data.xData.map((value, index) => ({ x: dataCirclesXtemp[index], y: valueYMiddle }));

      const radius = computeRadiusPoints(visualConfig.width, data.xData.length);

      const simulation = forceSimulation<DataPointXY>(dataCircles)
        .force('x', forceX<DataPointXY>(d => d.x).strength(4))
        .force('y', forceY<DataPointXY>(d => d.y).strength(0.1))
        .force('collide', forceCollide(radius * 1.25).strength(0.5));

      const circles = groupMargin.selectAll('circle').data(dataCircles);
      circles
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
        .attr('r', radius)
        .attr('stroke', data.colorNodesStroke)
        .attr('fill', data.colorNodes);
      circles
        .enter()
        .append('circle')
        .attr('class', (d, i) => `${data.idData[i]}`)
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
        .attr('r', radius)
        .attr('stroke', data.colorNodesStroke)
        .attr('fill', data.colorNodes);
      circles.exit().remove();

      simulation.on('tick', function () {
        groupMargin
          .selectAll<SVGCircleElement, DataPointXY>('circle')
          .attr('cx', (d: DataPointXY) => d.x)
          .attr('cy', (d: DataPointXY) => d.y);

        tickCount++;
        if (tickCount > maxComputations) {
          const dataSimulation: DataPoint[] = dataCircles.map(({ x, y }, i) => ({
            x,
            y,
            id: data.idData[i],
          }));

          onResultJitter(dataSimulation, idScatterplot);

          simulation.stop();
        }
      });

      yAxis = axisLeft(yScaleTemp).tickValues([]);
      yAxisGroup.call(yAxis).selectAll('text');
    }

    const textLabelAxis = calcTextWidth(data.xAxisName, styleTextXaxisLabel.maxLengthText, styleTextXaxisLabel.classTextXAxis);
    setTextXLabel(textLabelAxis[0]);

    const textLabelYAxis = calcTextWidth(data.yAxisName, styleTextXaxisLabel.maxLengthText, styleTextXaxisLabel.classTextXAxis);
    setTextYLabel(textLabelYAxis[0]);

    // axis style
    svg.selectAll('.tick text').attr('class', 'font-data').style('stroke', 'none').style('fill', configStyle.colorText);

    // BRUSH LOGIC
    const myBrush: any = brush()
      .extent([
        [0, 0],
        [visualConfig.widthMargin, visualConfig.heightMargin],
      ])
      .on('brush', brushed)
      .on('end', function (event: any) {
        if (event.selection === null) {
          onBrushClear(data.name);
          xAxisGroup.selectAll('.tick text').style('fill', configStyle.colorText);
          yAxisGroup.selectAll('.tick text').style('fill', configStyle.colorText);
        }
      });

    let selectedDataIds: string[] = [];

    function brushed(event: any) {
      if (event.selection) {
        const [[x0, y0], [x1, y1]] = event.selection;

        dataCircles.forEach((d, i) => {
          if (d.x >= x0 && d.x <= x1 && d.y >= y0 && d.y <= y1) {
            selectedDataIds.push(data.idData[i]);
          }
        });

        onBrushUpdate(selectedDataIds, data.name);
        selectedDataIds = [];

        // new stuff

        xAxisGroup.selectAll('.tick text').style('fill', configStyle.colorTextUnselect);
        xAxisGroup.selectAll('.tick text').each(function (d: any) {
          if (xAxisType != 'none') {
            const xValueFromD = xScaleTemp(d);

            if (xValueFromD != undefined) {
              select(this).style(
                'fill',
                xValueFromD >= x0 - xOffset && xValueFromD <= x1 - xOffset ? configStyle.colorText : configStyle.colorTextUnselect,
              );
            }
          }
        });

        yAxisGroup.selectAll('.tick text').style('fill', configStyle.colorTextUnselect);

        yAxisGroup.selectAll('.tick text').each(function (d: any) {
          if (yAxisType != 'none') {
            const yValueFromD = yScaleTemp(d);

            if (yValueFromD != undefined) {
              select(this).style(
                'fill',
                yValueFromD >= y0 - yOffset && yValueFromD <= y1 - yOffset ? configStyle.colorText : configStyle.colorTextUnselect,
              );
            }
          }
        });
      }
    }

    const brushGroup = select(brushRef.current);
    brushGroup
      .attr('transform', `translate(${visualConfig.margin.left},${visualConfig.margin.top})`)
      .attr('class', 'brushingElem_' + data.name);

    brushGroup.call(myBrush);
    brushGroup
      .select('.selection')
      .style('fill', data.colorBrush)
      .style('fill-opacity', 0.5)
      .style('stroke', data.colorBrushStroke)
      .style('stroke-width', 2);

    brushGroup.call(myBrush.move, null);

    brushGroup.selectAll('.handle').style('stroke', 'none');
    brushGroup.selectAll('.overlay').style('stroke', 'none');
  }, [data, visualConfig, xScaleRange, yScaleRange]);

  return (
    <div className="w-full">
      <div className="flex absolute mx-20 my-2 w-22 items-center gap-1">
        <EntityPill title={data.nodeName} />
        <div>
          {data.attributeName && (
            <div className="flex items-center gap-1">
              <Icon component="icon-[ic--baseline-arrow-forward]" size={16} color="text-secondary-300" />
              <span className="text-secondary-700 text-sm m-0.5">{data.attributeName}</span>
              {data.attributeSelected && (
                <div className="flex items-center gap-1">
                  <Icon component="icon-[ic--baseline-arrow-forward]" size={16} color="text-secondary-300" />
                  <span className="text-secondary-700 text-sm m-0.5">{data.attributeSelected}</span>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
      <div className="w-full flex flex-row justify-center">
        <svg ref={svgRef} width={visualConfig.width} height={visualConfig.height}>
          <text
            x={styleTextXaxisLabel.x}
            y={styleTextXaxisLabel.y}
            dominantBaseline={styleTextXaxisLabel.dominantBaseline}
            className={styleTextXaxisLabel.classTextXAxis}
          >
            {textXLabel}
          </text>

          <text
            x={styleTextXaxisLabel.x * 0.05}
            y={styleTextXaxisLabel.y * 0.5}
            textAnchor={styleTextXaxisLabel.textAnchor}
            dominantBaseline={styleTextXaxisLabel.dominantBaseline}
            className={styleTextXaxisLabel.classTextXAxis}
          >
            {textYLabel}
          </text>
          <g ref={groupMarginRef} />
          <g ref={brushRef} />
        </svg>
      </div>
    </div>
  );
};
