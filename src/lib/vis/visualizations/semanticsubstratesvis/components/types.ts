import { NodeAttributes } from '@/lib/data-access/store/graphQueryResultSlice';

export interface NodesGraphology {
  key: string;
  attributes: object[];
}

export interface DataFromPanel {
  id: number;
  settingsOpen: boolean;
  data: DataPanelConfig;
}

export interface EdgesGraphology {
  key: string;
  attributes: object[];
}

export interface GraphData {
  name?: string;
  nodes: AugmentedNodeAttributes[];
  edges: AugmentedEdgeAttributes[];
}

export interface UserSelection {
  name: string;
  nodeName?: string;
  attributeAsRegion?: string;
  attributeAsRegionSelection?: string;
  placement: {
    xAxis?: string;
    yAxis?: string;
    colorNodes: string;
    colorNodesStroke: string;
    colorFillBrush: string;
    colorStrokeBrush: string;
  };
}

export interface DataPoint {
  x: number;
  y: number;
  id: string;
}

export interface DataPointXY {
  x: number;
  y: number;
}

export interface connectionFromTo {
  to: string;
  from: string;
}

export interface edgeVisibility {
  _id: string;
  to: boolean;
  from: boolean;
  visibility: boolean;
}

export interface idConnectionsObjects {
  from: IdConnections;
  to: IdConnections;
}

export interface IdConnections {
  [key: string]: string[];
}

export interface DataConnection {
  from: string;
  to: string;
}

export interface Node {
  id: string;
  label: string;
  attributes: object[];
  _key: string;
  _rev: string;
}

export interface AugmentedNodeAttributes {
  _id: string;
  attributes: NodeAttributes;
  label: string;
}

export interface AugmentedEdgeAttributes {
  attributes: NodeAttributes;
  from: string;
  to: string;
  id: string;
  label: string;
}

export interface DataPanelConfig {
  entitySelected?: string;
  attributeSelected?: string;
  attributeValueSelected?: string;
  xAxisSelected?: string;
  yAxisSelected?: string;
}

export interface Edge {
  from: string;
  id: string;
  attributes: object[];
  label: string;
  _key: string;
  _rev: string;
}

export interface VisualRegionConfig {
  marginPercentage: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  margin: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  width: number;
  widthPercentage: number;
  height: number;
  widthMargin: number;
  heightMargin: number;
}

export interface VisualEdgesConfig {
  width: number;
  height: number;
  configRegion: VisualRegionConfig;
  offsetY: number;
  stroke: string;
  strokeWidth: number;
  strokeOpacity: number;
}

export interface RegionData {
  name: string;
  xData: any[];
  yData: any[];
  idData: string[];
  colorNodes: string;
  colorNodesStroke: string;
  colorBrush: string;
  colorBrushStroke: string;
  nodeName: string;
  attributeName: string;
  attributeSelected: string;
  xAxisName: string;
  yAxisName: string;
  label: string;
}
