import { Meta } from '@storybook/react';
import { SemSubstrVisComponent } from './semanticsubstratesvis';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { mockData } from '../../../mock-data';

const Mockstore = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
  },
});

const Component: Meta<typeof SemSubstrVisComponent.component> = {
  title: 'Visualizations/SemanticSubstrates',
  component: SemSubstrVisComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

// TODO!: needs to fix edge labels
export const TestWithBig2ndChamber = {
  args: {
    ...(await mockData.big2ndChamberQueryResult()),
    settings: {
      ...SemSubstrVisComponent.settings,
      showColor: true,
      dataPanels: [
        {
          id: 0,
          settingsOpen: true,
          data: {
            entitySelected: 'kamerleden',
            attributeSelected: '',
            attributeValueSelected: '',
            xAxisSelected: 'woonplaats',
            yAxisSelected: '',
          },
        },
        {
          id: 1,
          settingsOpen: true,
          data: {
            entitySelected: 'commissies',
            attributeSelected: '',
            attributeValueSelected: '',
            xAxisSelected: 'year',
            yAxisSelected: '',
          },
        },
      ],
    },
  },
};

export const TestWithRecommendationsActorMovie = {
  args: {
    ...(await mockData.mockRecommendationsActorMovie()),
    settings: {
      ...SemSubstrVisComponent.settings,
      showColor: true,
      dataPanels: [
        {
          id: 0,
          settingsOpen: true,
          data: {
            entitySelected: 'Movie',
            attributeSelected: '',
            attributeValueSelected: '',
            xAxisSelected: 'year',
            yAxisSelected: 'revenue',
          },
        },
        {
          id: 1,
          settingsOpen: true,
          data: {
            entitySelected: 'Actor',
            attributeSelected: '',
            attributeValueSelected: '',
            xAxisSelected: 'tmdbId',
            yAxisSelected: '',
          },
        },
      ],
    },
  },
};

export const TestWithGOTcharacter2character = {
  args: {
    ...(await mockData.gotCharacter2Character()),
    settings: {
      ...SemSubstrVisComponent.settings,
      showColor: true,
      dataPanels: [
        {
          id: 0,
          settingsOpen: true,
          data: {
            entitySelected: 'Character',
            attributeSelected: '',
            attributeValueSelected: '',
            xAxisSelected: 'degree',
            yAxisSelected: '',
          },
        },
        {
          id: 1,
          settingsOpen: true,
          data: {
            entitySelected: 'Character',
            attributeSelected: '',
            attributeValueSelected: '',
            xAxisSelected: 'community',
            yAxisSelected: '',
          },
        },
      ],
    },
  },
};

export default Component;
