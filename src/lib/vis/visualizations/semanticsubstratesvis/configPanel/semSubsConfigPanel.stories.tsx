import type { Meta, StoryObj } from '@storybook/react';
import { SemSubsConfigPanel } from '.';

const metaPillAttributes: Meta<typeof SemSubsConfigPanel> = {
  component: SemSubsConfigPanel,
  title: 'Visualizations/SemanticSubstrates/configpanel',
  decorators: [story => <div className="flex items-center justify-center w-26 m-11">{story()}</div>],
};

export default metaPillAttributes;

type Story = StoryObj<typeof SemSubsConfigPanel>;

export const userAddsData: Story = {
  args: {
    dataFromPanel: {
      id: 0,
      settingsOpen: true,
      data: {
        entitySelected: 'kamerleden',
        attributeSelected: 'Partij',
        attributeValueSelected: 'D66',
        xAxisSelected: 'Leeftijd',
        yAxisSelected: 'Woonplaats',
      },
    },
    graphMetaData: {
      nodes: {
        labels: ['kamerleden', 'commissies'],
        types: {
          kamerleden: {
            attributes: {
              Partij: {
                attributeType: 'string',
                statistics: { values: ['D66', 'VVD', 'PVV', 'DierenPartij'], uniqueItems: 4, mode: 'D66', count: 4 },
              },
              Woonplaats: {
                attributeType: 'string',
                statistics: { values: ['Amsterdam', 'Rotterdam', 'Den Haag'], uniqueItems: 3, mode: 'Amsterdam', count: 3 },
              },
            },
            count: 0,
          },
          commissies: {
            attributes: {
              Partij: { attributeType: 'string', statistics: { values: ['D66', 'VVD'], uniqueItems: 2, mode: 'D66', count: 2 } },
              Woonplaats: { attributeType: 'string', statistics: { values: ['Amsterdam'], uniqueItems: 1, mode: 'Amsterdam', count: 1 } },
            },
            count: 0,
          },
        },
        count: 0,
      },
      topological: {
        density: 0,
        self_loops: 0,
      },
      edges: {
        count: 0,
        labels: [],
        types: {},
      },
    },
    colorNode: '#FFA500',
    isFirstPanel: false,
    onUpdateData: updatedData => console.log('Updated data:', updatedData),
    onCollapse: isOpen => console.log('Panel collapsed:', isOpen),
    onDelete: () => console.log('Panel deleted'),
  },
};
