import React, { useState, useEffect } from 'react';
import { Button } from '@/lib/components/buttons';
import { Icon } from '@/lib/components/icon';
import { DataFromPanel, DataPanelConfig } from '../components/types';
import { EntityPillSelector } from '@/lib/components/selectors/entityPillSelector';
import { Input } from '@/lib/components';
import { CategoricalStats, GraphStatistics } from 'ts-common';

export type SemSubsConfigPanelProps = {
  dataFromPanel: DataFromPanel;
  graphMetaData: GraphStatistics;
  colorNode: string;
  onUpdateData: (data: Partial<DataPanelConfig>) => void;
  onCollapse: (isOpen: boolean) => void;
  onDelete: () => void;
  isFirstPanel: boolean;
};

export const SemSubsConfigPanel: React.FC<SemSubsConfigPanelProps> = ({
  dataFromPanel,
  graphMetaData,
  colorNode,
  isFirstPanel,
  onCollapse,
  onUpdateData,
  onDelete,
}) => {
  const [stateConfigPanelOptions, setStateConfigPanelOptions] = useState<{
    entitySelectedOptions: string[];
    attributeSelectedOptions: (undefined | string)[];
    attributeValueSelectedOptions: any[];
    xAxisSelectedOptions: (undefined | string)[];
    yAxisSelectedOptions: (undefined | string)[];
  }>({
    entitySelectedOptions: [],
    attributeSelectedOptions: [],
    attributeValueSelectedOptions: [],
    xAxisSelectedOptions: [],
    yAxisSelectedOptions: [],
  });

  const [allowToSelectEntity, setAllowToSelectEntity] = useState(false);
  const data = dataFromPanel.data;
  const [isNotCollapsed, setIsNotCollapsed] = useState(true);

  const handleButtonCollapseSubsratedPanel = () => {
    //onCollapse(!dataFromPanel.settingsOpen);
    //setAllowToSelectEntity(!allowToSelectEntity);
    setIsNotCollapsed(!isNotCollapsed);
  };

  // data processing

  useEffect(() => {
    if (graphMetaData) {
      setStateConfigPanelOptions(prevState => ({
        ...prevState,
        entitySelectedOptions: graphMetaData.nodes.labels,
      }));
    }
  }, [graphMetaData]);

  useEffect(() => {
    if (!data.entitySelected) return;
    if (!graphMetaData.nodes.types[data.entitySelected]) return;

    onCollapse(true);

    // field values of nummerical data are empty. Need to have access to raw data. For now just use categorical
    const categoricalKeys: (undefined | string)[] = [];

    categoricalKeys.push(undefined);

    for (const key in graphMetaData.nodes.types[data.entitySelected].attributes) {
      const stats = graphMetaData.nodes.types[data.entitySelected].attributes[key].statistics as CategoricalStats;
      const values = stats?.values;
      if (values && values.length > 0 && values[0].toString() !== '[object Object]') {
        if (
          Object.prototype.hasOwnProperty.call(graphMetaData.nodes.types[data.entitySelected].attributes, key) &&
          graphMetaData.nodes.types[data.entitySelected].attributes[key].attributeType === 'string'
        ) {
          categoricalKeys.push(key as string);
        }
      }
    }

    setStateConfigPanelOptions(prevState => ({
      ...prevState,
      attributeSelectedOptions: categoricalKeys,
    }));

    onUpdateData({
      attributeSelected: undefined,
      attributeValueSelected: undefined,
      xAxisSelected: undefined,
      yAxisSelected: undefined,
    });
  }, [data.entitySelected]);

  useEffect(() => {
    if (!data.entitySelected || !data.attributeSelected) return;

    if (data.attributeSelected && graphMetaData.nodes.types[data.entitySelected].attributes[data.attributeSelected]) {
      const stats = graphMetaData.nodes.types[data.entitySelected].attributes[data.attributeSelected].statistics as CategoricalStats;
      const arrayAttributeValues = stats?.values;
      if (arrayAttributeValues !== undefined) {
        if (arrayAttributeValues[0].toString() != '[object Object]') {
          setStateConfigPanelOptions(prevState => ({
            ...prevState,
            attributeValueSelectedOptions: arrayAttributeValues,
          }));

          onUpdateData({
            attributeValueSelected: undefined,
          });
        }
      }
    }
  }, [data.attributeSelected]);

  useEffect(() => {
    if (!!data.entitySelected && graphMetaData.nodes.types[data.entitySelected]) {
      const attributes = Object.keys(graphMetaData.nodes.types[data.entitySelected].attributes);

      const attributesForAxis: (string | undefined)[] = [];

      attributesForAxis.push(undefined);
      attributesForAxis.push(...attributes);

      setStateConfigPanelOptions(prevState => ({
        ...prevState,
        xAxisSelectedOptions: attributesForAxis,
        yAxisSelectedOptions: attributesForAxis,
      }));
    }
  }, [data.entitySelected]);

  useEffect(() => {
    if (!data.attributeSelected) {
      onUpdateData({ attributeValueSelected: undefined });
    }
  }, [data.attributeSelected]);

  return (
    <div className="flex flex-col w-full">
      <div className="flex my-2 items-center">
        <Button
          onClick={handleButtonCollapseSubsratedPanel}
          variantType="secondary"
          variant="ghost"
          size="xs"
          iconComponent={dataFromPanel.settingsOpen ? 'icon-[ic--baseline-arrow-drop-down]' : 'icon-[ic--baseline-arrow-right]'}
        />

        <EntityPillSelector
          selectedNode={data.entitySelected}
          dropdownNodes={stateConfigPanelOptions.entitySelectedOptions}
          onSelectOption={function (option: string): void {
            onUpdateData({ entitySelected: option });
          }}
        />

        <div className="flex justify-center items-center ml-auto">
          <div className="grow-0 shrink-0 w-6 h-6 flex justify-center items-center">
            <div className={`h-4 w-4 border border-secondary-150 rounded-sm`} style={{ backgroundColor: colorNode }}></div>
            {/* TODO: change this to color selector */}
          </div>
          <Button
            variantType="secondary"
            variant="ghost"
            size="xs"
            disabled={isFirstPanel}
            iconComponent="icon-[ic--baseline-delete-outline]"
            onClick={onDelete}
          />
        </div>
      </div>
      {isNotCollapsed && (
        <div className="ml-4 gap-2 flex flex-col">
          <div className="flex justify-between items-center gap-1">
            <Icon component="icon-[ic--baseline-subdirectory-arrow-right]" size={16} color="text-secondary-300" />
            <Input
              size="xs"
              type="dropdown"
              label="X-axis"
              value={data.xAxisSelected || 'None'}
              disabled={!data.entitySelected}
              options={stateConfigPanelOptions.xAxisSelectedOptions.map(option => option || 'None')}
              onChange={(option: string | number) => {
                onUpdateData({ xAxisSelected: String(option) });
              }}
            />
          </div>

          <div className="flex justify-between items-center gap-1">
            <Icon component="icon-[ic--baseline-subdirectory-arrow-right]" size={16} color="text-secondary-300" />
            <Input
              size="xs"
              type="dropdown"
              inline
              label="Y-axis"
              disabled={!data.entitySelected}
              value={data.yAxisSelected || 'None'}
              options={stateConfigPanelOptions.yAxisSelectedOptions.map(option => option || 'None')}
              onChange={(option: string | number) => {
                onUpdateData({ yAxisSelected: String(option) });
              }}
            />
          </div>
          <div className="flex justify-between items-center gap-1">
            <Icon component="icon-[ic--baseline-subdirectory-arrow-right]" size={16} color="text-secondary-300" />
            <Input
              size="xs"
              type="dropdown"
              label="Attribute"
              disabled={!data.entitySelected}
              value={data.attributeSelected || 'None'}
              options={stateConfigPanelOptions.attributeSelectedOptions.map(option => option || 'None')}
              onChange={(option: string | number) => {
                onUpdateData({ attributeSelected: String(option) });
              }}
            />
          </div>

          <div className="flex justify-between items-center ml-3 gap-1">
            <Icon component="icon-[ic--baseline-subdirectory-arrow-right]" size={16} color="text-secondary-300" />
            <Input
              size="xs"
              type="dropdown"
              disabled={!data.attributeSelected}
              label="Value"
              value={data.attributeValueSelected || 'None'}
              options={stateConfigPanelOptions.attributeValueSelectedOptions.map(option => option || 'None')}
              onChange={(option: string | number) => {
                onUpdateData({ attributeValueSelected: String(option) });
              }}
            />
          </div>
        </div>
      )}
    </div>
  );
};
