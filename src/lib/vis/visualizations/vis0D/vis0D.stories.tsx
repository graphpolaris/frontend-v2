import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { graphQueryResultSlice, schemaSlice, visualizationSlice } from '../../../data-access/store';
import Vis0DComponent from './Vis0D';

const Component: Meta<typeof Vis0DComponent.component> = {
  title: 'Visualizations/0DVis',
  component: Vis0DComponent.component,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div
          style={{
            width: '100%',
            height: '100vh',
          }}
        >
          {story()}
        </div>
      </Provider>
    ),
  ],
};

const Mockstore: any = configureStore({
  reducer: {
    schema: schemaSlice.reducer,
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
  },
});

export const TestWithData = {
  args: {
    data: {
      nodes: [
        { id: 'node1', label: 'Node 1' },
        { id: 'node2', label: 'Node 2' },
      ],
      edges: [
        {
          source: 'node1',
          target: 'node2',
          label: 'Edge between Node 1 and Node 2',
        },
      ],
    },
    ml: {},
    settings: {
      ...Vis0DComponent.settings,
      title: 'KPI Count Movie Votes',
      selectedEntity: 'Movie', // The selected entity (must exist in the graphMetadata structure).
      selectedAttribute: 'votes', // An attribute from the entity's attributes list.
      selectedStat: 'count', // A valid statistic for the selected attribute.
    },
    graphMetadata: {
      nodes: {
        count: 2,
        labels: ['Movie', 'Person'],
        types: {
          Movie: {
            count: 1,
            attributes: {
              votes: {
                type: 'number',
                statistics: {
                  count: 12345,
                  min: 0,
                  max: 1000,
                  avg: 450,
                },
              },
              rating: {
                type: 'number',
                statistics: {
                  count: 12345,
                  min: 1,
                  max: 10,
                  avg: 7.5,
                },
              },
            },
          },
          Person: {
            count: 1,
            attributes: {
              age: {
                type: 'number',
                statistics: {
                  count: 100,
                  min: 18,
                  max: 70,
                  avg: 35,
                },
              },
            },
          },
        },
      },
      edges: {
        count: 1,
        labels: ['ACTED_IN'],
        types: {
          ACTED_IN: {
            count: 1,
            attributes: {
              role: {
                type: 'string',
                statistics: {
                  uniqueCount: 1,
                },
              },
            },
          },
        },
      },
    },
  },
};

export const TestWithNoData = {
  args: {
    data: {
      nodes: [123123, 1312, 21313],
      edges: [],
    },
    ml: {},
    settings: Vis0DComponent.settings,
  },
};

export default Component;
