import React, { useRef, useImperativeHandle, forwardRef, useEffect, useState, Fragment } from 'react';
import { VisualizationPropTypes, VISComponentType, VisualizationSettingsPropTypes } from '../../common';
import { SettingsContainer } from '@/lib/vis/components/config';
import html2canvas from 'html2canvas';
import { Input } from '@/lib/components/inputs';
import { EntityPill, RelationPill } from '@/lib/components/pills/Pill';
import { Button } from '@/lib/components/buttons';
import { useActiveQuery, useActiveSaveState, useQuerybuilderHash } from '@/lib/data-access';
export interface Vis0DProps {
  title: string;
  selectedEntity: string;
  showTotal: boolean;
  selectedAttribute: string;
  selectedStat: string;
}

const settings: Vis0DProps = {
  title: '',
  selectedEntity: '',
  showTotal: true,
  selectedAttribute: '',
  selectedStat: '',
};

export interface Vis0DVisHandle {
  exportImageInternal: () => void;
}

const formatNumber = (number: number) => {
  return number.toLocaleString('de-DE');
};

const Vis0D = forwardRef<Vis0DVisHandle, VisualizationPropTypes<Vis0DProps>>(({ data, settings, graphMetadata }, refExternal) => {
  const [statRender, setStatRender] = useState<number | undefined>(undefined);
  const activeQuery = useActiveQuery();

  const internalRef = useRef<HTMLDivElement>(null);
  useImperativeHandle(refExternal, () => ({
    exportImageInternal() {
      const captureImage = () => {
        const element = internalRef.current;
        if (element) {
          html2canvas(element, {
            backgroundColor: '#FFFFFF',
          })
            .then(canvas => {
              const finalImage = canvas.toDataURL('image/png');
              const link = document.createElement('a');
              link.href = finalImage;
              link.download = 'Vis0D.png';
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
            })
            .catch(error => {
              console.error('Error capturing image:', error);
            });
        } else {
          console.error('Container element not found');
        }
      };

      const renderCanvas = () => {
        requestAnimationFrame(() => {
          captureImage();
        });
      };

      renderCanvas();
    },
  }));

  useEffect(() => {
    if (settings.selectedEntity != '' && graphMetadata.nodes.types && settings.selectedAttribute != '' && settings.selectedStat != '') {
      const nodesLabels = graphMetadata.nodes.labels;
      const edgesLabels = graphMetadata.edges.labels;

      let attributes: string[] = [];
      if (nodesLabels.includes(settings.selectedEntity)) {
        attributes = Object.keys(graphMetadata.nodes.types[settings.selectedEntity].attributes);
      } else if (edgesLabels.includes(settings.selectedEntity)) {
        attributes = Object.keys(graphMetadata.edges.types[settings.selectedEntity].attributes);
      }

      if (attributes.length === 0 || !attributes.includes(settings.selectedAttribute)) {
        setStatRender(undefined);
        return;
      }

      if (settings.showTotal) {
        let statValue = 0;
        if (nodesLabels.includes(settings.selectedEntity)) {
          const keys = activeQuery?.graph.nodes.filter(node => node.attributes.schemaKey === settings.selectedEntity).map(node => node.key);
          if (!keys || keys.length === 0) return;
          statValue = keys.reduce((acc, key) => {
            acc += activeQuery?.graph?.nodeCounts?.[key + '_count'] ?? 0;
            return acc;
          }, 0);
        } else {
          statValue = graphMetadata.edges.types[settings.selectedEntity].count;
        }
        setStatRender(statValue);
        return;
      }

      // Actual stat calculation
      let statsAvailable = [];

      if (nodesLabels.includes(settings.selectedEntity)) {
        statsAvailable = Object.keys(graphMetadata.nodes.types[settings.selectedEntity].attributes[settings.selectedAttribute].statistics);
      } else {
        statsAvailable = Object.keys(graphMetadata.edges.types[settings.selectedEntity].attributes[settings.selectedAttribute].statistics);
      }

      if (statsAvailable.includes(settings.selectedStat)) {
        let statValue = 0;
        if (nodesLabels.includes(settings.selectedEntity)) {
          statValue = (
            graphMetadata.nodes.types[settings.selectedEntity].attributes[settings.selectedAttribute].statistics as Record<string, number>
          )[settings.selectedStat];
        } else {
          statValue = (
            graphMetadata.edges.types[settings.selectedEntity].attributes[settings.selectedAttribute].statistics as Record<string, number>
          )[settings.selectedStat];
        }

        setStatRender(statValue);
      } else {
        setStatRender(undefined);
      }
    }
  }, [settings.selectedEntity, settings.selectedAttribute, settings.selectedStat, settings.showTotal]);

  return (
    <div className="h-full w-full flex flex-col items-center justify-center overflow-hidden" ref={internalRef}>
      {settings.title && <span className="text-3xl text-center mb-4">{settings.title}</span>}
      {statRender === undefined ? (
        <span className="text-4xl text-center">Select 0D data</span>
      ) : (
        <span className="text-8xl text-center">{formatNumber(statRender)}</span>
      )}
    </div>
  );
});

const Vis0DSettings = ({ settings, graphMetadata, updateSettings }: VisualizationSettingsPropTypes<Vis0DProps>) => {
  const ss = useActiveSaveState();
  const [attributeOptions, setAttributeOptions] = useState<string[]>([]);
  const [statsOptions, setStatsOptions] = useState<string[]>([]);
  useEffect(() => {
    if (graphMetadata && graphMetadata.nodes && graphMetadata.nodes.labels.length > 0) {
      if (settings.selectedEntity === '') {
        const firstEntity = graphMetadata.nodes.labels[0];

        const attributesFirstEntity = Object.keys(graphMetadata.nodes.types[firstEntity].attributes);
        setAttributeOptions(attributesFirstEntity);
        const selectedAttribute = attributesFirstEntity[0];

        const attributeSelectedStatistics = graphMetadata.nodes.types[firstEntity].attributes[selectedAttribute].statistics;

        const notNaNStats = Object.keys(attributeSelectedStatistics).filter(key => {
          const value = attributeSelectedStatistics[key as keyof typeof attributeSelectedStatistics];
          return typeof value === 'number' && !isNaN(value);
        });

        setStatsOptions(notNaNStats as string[]);
        updateSettings({
          selectedEntity: firstEntity,
          selectedAttribute: selectedAttribute,
          selectedStat: notNaNStats[0],
        });
      }

      if (settings.selectedEntity != null && attributeOptions.length == 0 && graphMetadata.nodes.types[settings.selectedEntity] != null) {
        const attributesFirstEntity = Object.keys(graphMetadata.nodes.types[settings.selectedEntity].attributes);
        setAttributeOptions(attributesFirstEntity);
      }

      if (
        settings.selectedEntity != null &&
        settings.selectedAttribute &&
        statsOptions.length == 0 &&
        graphMetadata.nodes?.types?.[settings.selectedEntity]?.attributes?.[settings.selectedAttribute] != null
      ) {
        const attributeSelectedStatistics =
          graphMetadata.nodes.types[settings.selectedEntity].attributes[settings.selectedAttribute].statistics;

        const notNaNStats = Object.keys(attributeSelectedStatistics).filter(key => {
          const value = attributeSelectedStatistics[key as keyof typeof attributeSelectedStatistics];
          return typeof value === 'number' && !isNaN(value);
        });

        setStatsOptions(notNaNStats as string[]);
      }
    }
  }, [graphMetadata]);

  useEffect(() => {
    if (
      settings.selectedEntity != '' &&
      settings.selectedAttribute != '' &&
      graphMetadata &&
      graphMetadata.nodes &&
      graphMetadata.nodes.labels.length > 0
    ) {
      const nodesLabels = graphMetadata.nodes.labels;
      const edgesLabels = graphMetadata.edges.labels;

      // attribute management
      let attributesFirstEntity: string[] = [];
      if (nodesLabels.includes(settings.selectedEntity)) {
        attributesFirstEntity = Object.keys(graphMetadata.nodes.types[settings.selectedEntity].attributes);
      } else if (edgesLabels.includes(settings.selectedEntity)) {
        attributesFirstEntity = Object.keys(graphMetadata.edges.types[settings.selectedEntity].attributes);
      }
      setAttributeOptions(attributesFirstEntity);
      let selectedAttribute = '';

      if (
        settings.selectedAttribute === '' ||
        !attributesFirstEntity.includes(settings.selectedAttribute) ||
        attributesFirstEntity.length === 0
      ) {
        selectedAttribute = attributesFirstEntity[0];
        updateSettings({ selectedAttribute: selectedAttribute });
      } else {
        selectedAttribute = settings.selectedAttribute;
      }

      // stat management
      let attributeSelectedStatistics: Record<string, number> = {};

      if (nodesLabels.includes(settings.selectedEntity)) {
        attributeSelectedStatistics = graphMetadata.nodes.types[settings.selectedEntity].attributes[selectedAttribute].statistics as Record<
          string,
          number
        >;
      } else if (edgesLabels.includes(settings.selectedEntity)) {
        attributeSelectedStatistics = graphMetadata.edges.types[settings.selectedEntity].attributes[selectedAttribute].statistics as Record<
          string,
          number
        >;
      }

      const notNaNStats = Object.keys(attributeSelectedStatistics).filter(key => {
        const value = attributeSelectedStatistics[key as keyof typeof attributeSelectedStatistics];
        // !TODO: include string stats
        return !isNaN(value);
      });

      setStatsOptions(notNaNStats as string[]);

      if (settings.selectedStat == '' || !notNaNStats.includes(settings.selectedStat)) {
        updateSettings({ selectedStat: notNaNStats[0] });
      } else {
        updateSettings({ selectedStat: settings.selectedStat });
      }
    }
  }, [settings.selectedEntity, settings.selectedAttribute]);

  return (
    <SettingsContainer>
      <div className="p-1 flex flex-col gap-1">
        <Input
          className="mb-2"
          type="text"
          label="Title"
          value={settings.title}
          onChange={value => updateSettings({ title: value as string })}
        />
        <Input
          label=""
          className="w-full text-justify justify-start mb-2"
          type="dropdown"
          value={settings.selectedEntity}
          options={[...graphMetadata.nodes.labels, ...graphMetadata.edges.labels]}
          onChange={val => updateSettings({ selectedEntity: val as string })}
          overrideRender={
            graphMetadata.nodes.labels.includes(settings.selectedEntity) ? (
              <EntityPill
                title={
                  <div className="flex flex-row justify-between items-center cursor-pointer">
                    <span>{settings.selectedEntity || ''}</span>
                    <Button variantType="secondary" variant="ghost" size="2xs" iconComponent="icon-[ic--baseline-arrow-drop-down]" />
                  </div>
                }
              />
            ) : (
              <RelationPill
                title={
                  <div className="flex flex-row justify-between items-center cursor-pointer">
                    <span>{settings.selectedEntity || ''}</span>
                    <Button variantType="secondary" variant="ghost" size="2xs" iconComponent="icon-[ic--baseline-arrow-drop-down]" />
                  </div>
                }
              />
            )
          }
        ></Input>
        <Input
          label="KPI"
          className="w-full text-justify justify-start"
          type="dropdown"
          options={[{ totalNodes: 'Total number of nodes' }, { stat: 'Attribute statistic' }]}
          value={settings.showTotal ? 'totalNodes' : 'stat'}
          onChange={val => {
            if (val == 'totalNodes') updateSettings({ showTotal: true });
            if (val == 'stat') updateSettings({ showTotal: false });
          }}
        ></Input>
        {!settings.showTotal && (
          <>
            <Input
              label="Attribute"
              className="w-full text-justify justify-start"
              type="dropdown"
              value={settings.selectedAttribute}
              options={attributeOptions}
              onChange={val => updateSettings({ selectedAttribute: val as string })}
            ></Input>
            <Input
              label="Stat"
              className="w-full text-justify justify-start"
              type="dropdown"
              value={settings.selectedStat}
              options={statsOptions}
              onChange={val => updateSettings({ selectedStat: val as string })}
            ></Input>
          </>
        )}
      </div>
    </SettingsContainer>
  );
};

const Vis0DRef = React.createRef<Vis0DVisHandle>();

export const Vis0DComponent: VISComponentType<Vis0DProps> = {
  component: React.forwardRef((props: VisualizationPropTypes<Vis0DProps>, ref) => <Vis0D {...props} ref={Vis0DRef} />),
  settingsComponent: Vis0DSettings,
  settings: settings,
  exportImage: () => {
    if (Vis0DRef.current) {
      Vis0DRef.current.exportImageInternal();
    } else {
      console.error('0Dvis reference is not set.');
    }
  },
};

export default Vis0DComponent;
