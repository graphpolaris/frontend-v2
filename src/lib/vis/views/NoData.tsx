import React from 'react';
import { Button } from '../../components';

type Props = { dataAvailable: boolean; error: boolean };

export function NoData({ dataAvailable, error }: Props) {
  return (
    <div className="flex justify-center items-center h-full">
      <div className="max-w-lg mx-auto text-left">
        <p className="text-xl font-normal text-secondary-600">No data available to be shown</p>
        {error ? (
          <div className="m-3 self-center text-center flex h-full flex-col justify-center">
            <p className="text-xl font-bold text-error">An error occurred while fetching data!</p>
            <p className="">Please retry or contact your Database's Administrator</p>
          </div>
        ) : dataAvailable ? (
          <p>Query resulted in empty dataset</p>
        ) : (
          <div>
            <p>Query for data to visualize</p>
            <Button
              variantType="primary"
              variant="outline"
              label="Learn how to query data"
              size="sm"
              iconComponent="icon-[ic--outline-info]"
              onClick={() => window.open('https://graphpolaris.com', '_blank')}
            />
          </div>
        )}
      </div>
    </div>
  );
}
