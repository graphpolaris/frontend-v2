import { useState } from 'react';
import Info from '../../components/info';
import { addVisualization } from '../../data-access/store/visualizationSlice';
import { useActiveSaveStateAuthorization, useAppDispatch } from '../../data-access';
import { Visualizations } from '../components/VisualizationPanel';
import { VisualizationsConfig } from '../components/config/VisualizationConfig';
import { resultSetFocus } from '@/lib/data-access/store/interactionSlice';

export function Recommender() {
  const dispatch = useAppDispatch();
  const saveStateAuthorization = useActiveSaveStateAuthorization();
  const [visualizationDescriptions] = useState(Object.values(VisualizationsConfig));

  return (
    <div className="p-4">
      <span className="text-md">Select a visualization</span>
      <div className="grid gap-3 my-2" style={{ gridTemplateColumns: 'repeat(auto-fit, minmax(260px, 1fr))' }}>
        {visualizationDescriptions.map(({ id, displayName, description, icons }) => {
          const IconComponent = icons.lg;

          return (
            <div
              key={id}
              className={`group flex flex-row gap-1.5 items-start rounded-md relative p-2 border h-18 ${saveStateAuthorization.visualization.W ? 'cursor-pointer hover:bg-secondary-100' : 'cursor-not-allowed opacity-50'}`}
              onClick={async e => {
                e.preventDefault();
                if (!saveStateAuthorization.visualization.W) {
                  console.debug('User blocked from editing query due to being a viewer');
                  return;
                }
                dispatch(resultSetFocus({ focusType: 'visualization' }));
                const component = await Visualizations[id]();
                dispatch(addVisualization({ ...component.default.settings, name: displayName, id }));
              }}
            >
              <div className="text-secondary-500 group-hover:text-secondary-700">
                {IconComponent && <IconComponent className="h-auto" />}
              </div>
              <div className="grow min-w-0">
                <div className="flex flex-row text-sm font-semibold justify-between w-full">
                  <h3 className="pt-1 truncate">{displayName}</h3>
                  <div className="shrink-0 text-secondary-700">
                    <Info tooltip="Here an explanation" placement="top" />
                  </div>
                </div>
                <div className="text-xs text-secondary-500 line-clamp-2 leading-snug">{description}</div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
