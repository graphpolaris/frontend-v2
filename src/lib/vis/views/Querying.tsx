import React from 'react';
import { LoadingSpinner } from '../../components';

export function Querying() {
  return (
    <div className="w-full h-full flex flex-col items-center justify-center overflow-hidden">
      <LoadingSpinner>Querying backend...</LoadingSpinner>
    </div>
  );
}
