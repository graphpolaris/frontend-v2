import React from 'react';

interface IconProps {
  className?: string;
}

export const Vis1DIconSm: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="16"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <rect x="11" y="9" width="3" height="6" fill="currentColor" />
    <rect x="6" y="1" width="3" height="14" fill="currentColor" />
    <rect x="1" y="5" width="3" height="10" fill="currentColor" />
  </svg>
);
export const Vis1DIconMd: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <rect x="16" y="14" width="4" height="6" fill="currentColor" />
    <rect x="10" y="3" width="4" height="17" fill="currentColor" />
    <rect x="4" y="8" width="4" height="12" fill="currentColor" />
  </svg>
);
export const Vis1DIconLg: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="44"
    height="44"
    viewBox="0 0 44 44"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <rect
      x="29"
      y="25"
      width="7"
      height="11"
      className="transform origin-bottom [transform-box:fill-box] transition-all duration-200 group-hover:scale-y-150"
      fill="currentColor"
    />
    <rect
      className="transform origin-bottom [transform-box:fill-box] transition-all duration-200 group-hover:scale-y-75"
      x="18"
      y="6"
      width="7"
      height="30"
      fill="currentColor"
    />
    <rect
      x="7"
      y="15"
      width="7"
      height="21"
      className="transform origin-bottom [transform-box:fill-box] transition-all duration-200 group-hover:scale-y-125 group-hover:fill-accent"
      fill="currentColor"
    />
  </svg>
);
