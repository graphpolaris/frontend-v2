import React from 'react';

interface IconProps {
  className?: string;
}

export const TableIconSm: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="16"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path fill="currentColor" d="M5 2h1v12H5z" />
    <path fill="currentColor" d="M1 8V7h14v1zm0 3v-1h14v1z" />
    <path fill="currentColor" d="M10 2h1v12h-1z" />
    <path fill="currentColor" fillRule="evenodd" d="M14 3H2v10h12V3ZM1 2v12h14V2H1Z" clipRule="evenodd" />
    <path fill="currentColor" d="M1 2h14v3H1V2Z" />
  </svg>
);
export const TableIconMd: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path fill="currentColor" fillRule="evenodd" clipRule="evenodd" d="M21 5H3V19H21V5ZM2 4V20H22V4H2Z" />
    <rect fill="currentColor" x="2" y="11" width="20" height="1" />
    <path fill="currentColor" d="M2 16V15H22V16H2Z" />
    <rect fill="currentColor" x="2" y="4" width="20" height="4" />
    <rect fill="currentColor" x="8" y="4" width="1" height="16" />
    <rect fill="currentColor" x="15" y="4" width="1" height="16" />
  </svg>
);
export const TableIconLg: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="44"
    height="44"
    viewBox="0 0 44 44"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M28 14C28 21.3333 28 28.6667 28 36" stroke="currentColor" strokeWidth="1.5" />
      <path d="M16 14C16 21.3333 16 28.6667 16 36" stroke="currentColor" strokeWidth="1.5" />
      <rect
        className="transition-opacity duration-300 group-hover:opacity-40 fill-accent"
        opacity="0"
        x="6"
        y="21"
        width="32"
        height="7"
        fill="currentColor"
      />
      <path
        className="transition-color duration-200 group-hover:stroke-accent"
        d="M5 28C16.3333 28 27.6667 28 39 28"
        stroke="currentColor"
        strokeWidth="1.5"
      />
      <path
        className="transition-color duration-200 group-hover:stroke-accent"
        d="M5 21C16.3333 21 27.6667 21 39 21"
        stroke="currentColor"
        strokeWidth="1.5"
      />
      <rect x="5.75" y="8.75" width="32.5" height="26.5" stroke="currentColor" strokeWidth="1.5" />
      <path d="M5 14C16.3333 14 27.6667 14 39 14" stroke="currentColor" strokeWidth="1.5" />
      <rect x="6" y="9" width="32" height="5" fill="currentColor" />
    </svg>
  </svg>
);
