import React from 'react';

interface IconProps {
  className?: string;
}

export const MatrixIconSm: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="16"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path stroke="currentColor" d="M.5.5h14v14H.5z" />
    <path fill="currentColor" d="M13 2h-2v2h2V2Z" />
    <path fill="currentColor" d="M10 2H8v2h2V2ZM7 2H5v2h2V2ZM4 2H2v2h2V2Zm9 3h-2v2h2V5Z" opacity=".15" />
    <path fill="currentColor" d="M10 5H8v2h2V5Z" />
    <path fill="currentColor" d="M7 5H5v2h2V5ZM4 5H2v2h2V5Zm9 3h-2v2h2V8Zm-3 0H8v2h2V8ZM7 8H5v2h2V8Z" opacity=".15" />
    <path fill="currentColor" d="M4 8H2v2h2V8Z" />
    <path fill="currentColor" d="M13 11h-2v2h2v-2Z" opacity=".15" />
    <path fill="currentColor" d="M10 11H8v2h2v-2Z" />
    <path fill="currentColor" d="M7 11H5v2h2v-2Zm-3 0H2v2h2v-2Z" opacity=".15" />
  </svg>
);
export const MatrixIconMd: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path stroke="currentColor" d="M3.5 3.5h17v17h-17z" />
    <path fill="currentColor" d="M16 5h-2v2h2V5Z" opacity=".15" />
    <path fill="currentColor" d="M19 5h-2v2h2V5Z" />
    <path fill="currentColor" d="M13 5h-2v2h2V5Zm-3 0H8v2h2V5ZM7 5H5v2h2V5Zm9 3h-2v2h2V8Zm3 0h-2v2h2V8Z" opacity=".15" />
    <path fill="currentColor" d="M13 8h-2v2h2V8Z" />
    <path fill="currentColor" d="M10 8H8v2h2V8ZM7 8H5v2h2V8Z" opacity=".15" />
    <path fill="currentColor" d="M16 11h-2v2h2v-2Z" />
    <path fill="currentColor" d="M19 11h-2v2h2v-2Zm-6 0h-2v2h2v-2Zm-3 0H8v2h2v-2Z" opacity=".15" />
    <path fill="currentColor" d="M7 11H5v2h2v-2Z" />
    <path fill="currentColor" d="M16 14h-2v2h2v-2Zm0 3h-2v2h2v-2Zm3-3h-2v2h2v-2Zm0 3h-2v2h2v-2Zm-6-3h-2v2h2v-2Z" opacity=".15" />
    <path fill="currentColor" d="M13 17h-2v2h2v-2Z" />
    <path fill="currentColor" d="M10 14H8v2h2v-2Zm0 3H8v2h2v-2Zm-3-3H5v2h2v-2Zm0 3H5v2h2v-2Z" opacity=".15" />
  </svg>
);
export const MatrixIconLg: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="44"
    height="44"
    viewBox="0 0 44 44"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <rect x="4.25" y="4.25" width="35.5" height="35.5" className="transition-all duration-200" stroke="currentColor" strokeWidth="1.5" />
    <path fill="currentColor" className="transition-all duration-200 group-hover:opacity-30" d="M37 7H31V13H37V7Z" />
    <path
      fill="currentColor"
      className="transition-all duration-200 group-hover:opacity-30 group-hover:fill-accent"
      opacity="0.15"
      d="M29 7H23V13H29V7Z"
    />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M21 7H15V13H21V7Z" />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M13 7H7V13H13V7Z" />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M37 15H31V21H37V15Z" />
    <path fill="currentColor" className="transition-color duration-200 group-hover:fill-accent" d="M29 15H23V21H29V15Z" />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M21 15H15V21H21V15Z" />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M13 15H7V21H13V15Z" />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M37 23H31V29H37V23Z" />
    <path
      fill="currentColor"
      className="transition-all duration-200 group-hover:opacity-30 group-hover:fill-accent"
      opacity="0.15"
      d="M29 23H23V29H29V23Z"
    />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M21 23H15V29H21V23Z" />
    <path fill="currentColor" className="transition-all duration-200 group-hover:opacity-30" d="M13 23H7V29H13V23Z" />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M37 31H31V37H37V31Z" />
    <path fill="currentColor" className="transition-color duration-200 group-hover:fill-accent" d="M29 31H23V37H29V31Z" />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M21 31H15V37H21V31Z" />
    <path fill="currentColor" className="transition-opacity duration-200 group-hover:opacity-10" opacity="0.15" d="M13 31H7V37H13V31Z" />
  </svg>
);
