import React from 'react';

interface IconProps {
  className?: string;
}

export const PaohIconSm: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="16"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path d="M13.5 2.5V13.5" stroke="currentColor" />
    <circle cx="13.5" cy="13.75" r="1.75" fill="currentColor" />
    <circle cx="13.5" cy="2.25" r="1.75" fill="currentColor" />
    <path d="M8 2.5V9.5" stroke="currentColor" />
    <circle cx="8" cy="2.25" r="1.75" fill="currentColor" />
    <circle cx="8" cy="9.75" r="1.75" fill="currentColor" />
    <path d="M2.5 2.5V9.5" stroke="currentColor" />
    <circle cx="2.5" cy="2.25" r="1.75" fill="currentColor" />
    <circle cx="2.5" cy="9.75" r="1.75" fill="currentColor" />
  </svg>
);
export const PaohIconMd: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path d="M18.5005 5.5V18.5" stroke="currentColor" />
    <circle cx="18.5" cy="5.5" r="2" fill="currentColor" />
    <circle cx="18.5" cy="18.5" r="2" fill="currentColor" />
    <path d="M12 5.5V14" stroke="currentColor" />
    <circle cx="12" cy="5.5" r="2" fill="currentColor" />
    <circle cx="12" cy="14" r="2" fill="currentColor" />
    <path d="M5.49963 5.5V14" stroke="currentColor" />
    <circle cx="5.5" cy="5.5" r="2" fill="currentColor" />
    <circle cx="5.5" cy="14" r="2" fill="currentColor" />
  </svg>
);
export const PaohIconLg: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="44"
    height="44"
    viewBox="0 0 44 44"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path d="M35 12C35 18.6667 35 25.3333 35 32" stroke="currentColor" strokeWidth="1.5" />
    <circle
      className="transition-colors duration-300 group-hover:fill-transparent"
      cx="35"
      cy="9"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-colors duration-300 group-hover:fill-transparent"
      cx="35"
      cy="35"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <path
      className="transition-all duration-300 group-hover:stroke-accent"
      d="M22 12C22 15.6667 22 19.3333 22 23"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-colors duration-300 group-hover:fill-accent group-hover:stroke-accent"
      cx="22"
      cy="9"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-colors duration-300 group-hover:fill-accent group-hover:stroke-accent"
      cx="22"
      cy="26"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <path d="M9 12C9 15.6667 9 19.3333 9 23" stroke="currentColor" strokeWidth="1.5" />
    <circle
      className="transition-colors duration-300 group-hover:fill-transparent"
      cx="9"
      cy="9"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-colors duration-300 group-hover:fill-transparent"
      cx="9"
      cy="26"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
  </svg>
);
