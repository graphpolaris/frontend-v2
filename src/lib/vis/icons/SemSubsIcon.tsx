import React from 'react';

interface IconProps {
  className?: string;
}

export const SemSubsIconSm: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="16"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path d="M2.25 2.75V2.75C3.62716 5.7093 4.44884 8.89647 4.6741 12.1527L4.75 13.25" stroke="currentColor" />
    <path d="M8 2.75V2.75C9.2555 5.71765 10.058 8.8571 10.3806 12.0632L10.5 13.25" stroke="currentColor" />
    <circle cx="8.125" cy="2.875" r="1.875" fill="currentColor" />
    <circle cx="10.625" cy="13.375" r="1.875" fill="currentColor" />
    <circle cx="13.875" cy="2.875" r="1.875" fill="currentColor" />
    <circle cx="2.375" cy="2.875" r="1.875" fill="currentColor" />
    <circle cx="4.875" cy="13.375" r="1.875" fill="currentColor" />
  </svg>
);
export const SemSubsIconMd: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path d="M6 6V6C7.65109 9.35298 8.63842 12.9935 8.90765 16.7213L9 18" stroke="currentColor" />
    <circle cx="6" cy="5.5" r="2" fill="currentColor" />
    <circle cx="9" cy="18.5" r="2" fill="currentColor" />
    <path d="M12 6V6C13.6511 9.35298 14.6384 12.9935 14.9076 16.7213L15 18" stroke="currentColor" />
    <circle cx="12" cy="5.5" r="2" fill="currentColor" />
    <circle cx="15" cy="18.5" r="2" fill="currentColor" />
    <circle cx="18" cy="5.5" r="2" fill="currentColor" />
  </svg>
);
export const SemSubsIconLg: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="44"
    height="44"
    viewBox="0 0 44 44"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <circle cx="35" cy="9" r="3" fill="currentColor" stroke="currentColor" strokeWidth="1.5" />
    <path
      className="transform origin-bottom [transform-box:fill-box] transition-all stroke-accent duration-300 group-hover:[stroke-dashoffset:0] group-hover:opacity-100 "
      opacity="0"
      d="M21 9V9C17.6938 16.2736 15.7277 24.0842 15.1962 32.0563L15 35"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeDasharray="40"
      strokeDashoffset="-20"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <path
      className="transition-all duration-300 group-hover:opacity-0"
      d="M22 9V9C25.3062 16.2736 27.2723 24.0842 27.8038 32.0563L28 35"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-color duration-200 group-hover:fill-accent group-hover:stroke-accent"
      cx="22"
      cy="9"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle cx="28" cy="35" r="3" fill="currentColor" stroke="currentColor" strokeWidth="1.5" />
    <path
      className="transition-color duration-200 group-hover:stroke-accent"
      d="M9 9V9C12.3062 16.2736 14.2723 24.0842 14.8038 32.0563L15 35"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-color duration-200 group-hover:fill-accent group-hover:stroke-accent"
      cx="9"
      cy="9"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-color duration-200 group-hover:fill-accent group-hover:stroke-accent"
      cx="15"
      cy="35"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
  </svg>
);
