import React from 'react';

interface IconProps {
  className?: string;
}

export const NodeLinkIconSm: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="16"
    viewBox="0 0 16 16"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path d="M8 2V14" stroke="currentColor" />
    <circle cx="8" cy="14.5" r="1.5" fill="currentColor" />
    <circle cx="8" cy="1.5" r="1.5" fill="currentColor" />
    <path d="M13.1961 5L2.8038 11" stroke="currentColor" />
    <circle cx="2.37085" cy="11.25" r="1.5" fill="currentColor" />
    <circle cx="13.6292" cy="4.75" r="1.5" fill="currentColor" />
    <path d="M13.1961 11L2.8038 5" stroke="currentColor" />
    <circle cx="2.37085" cy="4.75" r="1.5" fill="currentColor" />
    <circle cx="13.6292" cy="11.25" r="1.5" fill="currentColor" />
    <circle cx="8" cy="8" r="2.5" fill="currentColor" />
  </svg>
);
export const NodeLinkIconMd: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path d="M12 4V20" stroke="currentColor" />
    <circle cx="12" cy="20" r="2" fill="currentColor" />
    <circle cx="12" cy="4" r="2" fill="currentColor" />
    <path d="M18.9282 7.99976L5.07182 15.9998" stroke="currentColor" />
    <circle cx="5.07184" cy="15.9998" r="2" fill="currentColor" />
    <circle cx="18.9282" cy="7.99976" r="2" fill="currentColor" />
    <path d="M18.9283 15.9998L5.07194 7.99976" stroke="currentColor" />
    <circle cx="5.07196" cy="7.99976" r="2" fill="currentColor" />
    <circle cx="18.9283" cy="15.9998" r="2" fill="currentColor" />
    <circle cx="12" cy="12" r="3" fill="currentColor" />
  </svg>
);
export const NodeLinkIconLg: React.FC<IconProps> = ({ className = '' }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="44"
    height="44"
    viewBox="0 0 44 44"
    preserveAspectRatio="xMidYMid meet"
    fill="none"
  >
    <path d="M22 22C22 27.3333 22 32.6667 22 38Z" fill="currentColor" />
    <path d="M22 22C22 27.3333 22 32.6667 22 38" stroke="currentColor" strokeWidth="1.5" />
    <path d="M22 6C22 11.3333 22 16.6667 22 22Z" fill="currentColor" />
    <path d="M22 6C22 11.3333 22 16.6667 22 22" stroke="currentColor" strokeWidth="1.5" />
    <circle cx="22" cy="38" r="3" fill="currentColor" stroke="currentColor" strokeWidth="1.5" />
    <circle cx="22" cy="6" r="3" fill="currentColor" stroke="currentColor" strokeWidth="1.5" />
    <path
      className="transition-color duration-300 group-hover:stroke-accent"
      d="M35.8564 14C31.2376 16.6667 26.6188 19.3333 22 22"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <path
      className="transition-color duration-300 group-hover:stroke-accent"
      d="M22 22C17.3812 24.6667 12.7624 27.3333 8.14363 30"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-color duration-300 group-hover:stroke-accent group-hover:fill-accent"
      cx="8.14362"
      cy="30"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-color duration-300 group-hover:stroke-accent group-hover:fill-accent"
      cx="35.8564"
      cy="14"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <path d="M22 22C17.3812 19.3333 12.7623 16.6667 8.14351 14Z" fill="currentColor" />
    <path d="M22 22C17.3812 19.3333 12.7623 16.6667 8.14351 14" stroke="currentColor" strokeWidth="1.5" />
    <path
      className="transition-color duration-300 group-hover:stroke-accent"
      d="M35.8563 30C31.2375 27.3333 26.6188 24.6667 22 22"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle cx="8.14349" cy="14" r="3" fill="currentColor" stroke="currentColor" strokeWidth="1.5" />
    <circle
      className="transition-color duration-300 group-hover:stroke-accent group-hover:fill-accent"
      cx="35.8563"
      cy="30"
      r="3"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
    <circle
      className="transition-color duration-300 group-hover:stroke-accent group-hover:fill-accent"
      cx="22"
      cy="22"
      r="5"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth="1.5"
    />
  </svg>
);
