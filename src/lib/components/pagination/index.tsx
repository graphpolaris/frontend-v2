import React from 'react';
import { Button } from '../buttons';

export type PaginationProps = {
  currentPage: number;
  totalPages: number;
  onPageChange: (page: number) => void;
  itemsPerPageInput: number;
  numItemsArrayReal: number;
  totalItems: number;
  arrangement?: 'vertical' | 'horizontal';
  className?: string;
};

export const Pagination: React.FC<PaginationProps> = ({
  currentPage,
  totalPages,
  onPageChange,
  itemsPerPageInput,
  numItemsArrayReal,
  totalItems,
  arrangement = 'horizontal',
  className = '',
}) => {
  const firstItem = (currentPage - 1) * itemsPerPageInput + 1;

  const goToPreviousPage = () => {
    if (currentPage > 1) {
      onPageChange(currentPage - 1);
    }
  };

  const goToNextPage = () => {
    if (currentPage < totalPages) {
      onPageChange(currentPage + 1);
    }
  };

  const backIcon = (
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
      <path fill="currentColor" d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z" />
    </svg>
  );
  const forwardIcon = (
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
      <path fill="currentColor" d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z" />
    </svg>
  );

  return (
    <div className={`self-center table-pagination flex flex-col items-center py-2 gap-1.5 ${className}`}>
      <div className="inline-block text-sm">
        <span className="font-semibold">{`${firstItem} - ${firstItem + numItemsArrayReal - 1}`}</span>
        <span className="ml-1">of {totalItems}</span>
      </div>
      <div className={`grid ${arrangement === 'vertical' ? 'grid-rows-3' : 'grid-cols-2'} gap-2`}>
        <Button
          size={'sm'}
          label="Previous"
          variant="outline"
          iconComponent={backIcon}
          onClick={goToPreviousPage}
          disabled={currentPage === 1}
        />
        <Button
          size={'sm'}
          label="Next"
          variant="outline"
          iconComponent={forwardIcon}
          iconPosition="trailing"
          onClick={goToNextPage}
          disabled={currentPage === totalPages || numItemsArrayReal === totalItems}
        />
      </div>
    </div>
  );
};
