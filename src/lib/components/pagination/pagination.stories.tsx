import type { Meta, StoryObj } from '@storybook/react';
import { useState } from 'react';
import { Pagination } from '.';

const metaPagination: Meta<typeof Pagination> = {
  component: Pagination,
  title: 'Components/Pagination',
};

export default metaPagination;
type Story = StoryObj<typeof Pagination>;

export const mainStory: Story = {
  render: args => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [currentPage, setCurrentPage] = useState(1);

    const handlePageChange = (page: number) => {
      setCurrentPage(page);
    };

    return <Pagination {...args} currentPage={currentPage} onPageChange={handlePageChange} />;
  },
  args: {
    currentPage: 1,
    totalPages: 10,
    onPageChange: (page: number) => console.log(`Go to page ${page}`),
    itemsPerPageInput: 10,
    numItemsArrayReal: 100,
    totalItems: 1000,
  },
};
