import React, { ReactElement, ReactNode } from 'react';
import { SVGProps } from 'react';

// Define Sizes and IconProps types
export const sizesArray = [8, 10, 12, 14, 16, 20, 24, 28, 32, 36, 40, 48, 56] as const;
export type Sizes = (typeof sizesArray)[number];

export type IconProps = SVGProps<SVGSVGElement> & {
  component?: ReactNode | ReactElement<any> | string;
  size?: Sizes;
  color?: string;
};

// Icon component definition
export const Icon = ({ component, size = 24, color, ...props }: IconProps) => {
  if (!component) {
    console.error('No icon found');
    return <div></div>;
  }

  const style = { fontSize: size, color, width: size, height: size };
  if (typeof component === 'string') {
    // return <span className={'icon-[material-symbols--fullscreen] absolute text-sm'}></span>;
    return <span className={component + ' ' + props.className} style={style}></span>;
  }

  // Check if component is a valid React element
  if (React.isValidElement(component)) {
    return React.cloneElement(component as ReactElement<any>, {
      style: { ...style, ...(component as ReactElement<any>).props.style },
      width: size,
      height: size,
      ...props,
    });
  }
  /*
  // Check if component is a function (assume it's a custom SVG component)
  if (typeof component === 'function') {
    // Render the custom SVG component directly
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width={size} height={size} viewBox="0 0 32 32" style={style} {...props}>
        {(component as () => ReactNode)()}
      </svg>
    );
  }
  */
  // Default case: render null or fallback
  console.error('Unsupported icon type');
  return null;
};
