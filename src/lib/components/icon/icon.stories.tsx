import { StoryObj, Meta } from '@storybook/react';
import { Icon, sizesArray } from '@/lib/components/icon';

/**
 * # Icon Component
 *
 * This component uses **Google Material Icons** from the [Iconify](https://iconify.design/) library.
 * You can browse all available icons in the **Google Material Icons** set here:
 * [Google Material Icons on Iconify](https://icon-sets.iconify.design/ic/)
 *
 * ## How to use icons:
 * Each icon is referenced using the format: `setCode--iconName`
 * - Example: `'ic--baseline-arrow-outward'`
 * - `"ic"` is the **Google Material Icons** set
 * - `"baseline-arrow-outward"` is the icon name
 *
 * ## Customization:
 * - **Size**: Choose from predefined sizes (e.g., 16, 24, 32)
 * - **Color**: Pick a custom color
 */

const iconOptions = [
  'icon-[ic--baseline-settings]',
  'icon-[ic--baseline-home]',
  'icon-[ic--baseline-search]',
  'icon-[ic--baseline-favorite]',
  'icon-[ic--baseline-expand-more]',
];

export default {
  title: 'Components/Icon',
  component: Icon,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component:
          'The `Icon` component allows you to render icons from the Google Material Icons set using the **Iconify** framework. You can choose a specific icon, set its size, please use className to set a color in the interface, default the icon will inherit the text color (color: currentColor). The use of hardcoded color only in specific usecases.',
      },
    },
  },
  argTypes: {
    /**
     * Select an icon from the Google Material Icons set.
     * Format: `ic--icon-name`
     * See full list here: https://icon-sets.iconify.design/ic/
     */
    component: {
      control: 'select',
      options: iconOptions,
      description:
        'Select an icon from the **Google Material Icons** set. Format: `ic--icon-name`. See the full list [here](https://icon-sets.iconify.design/ic/).',
    },
    /**
     * Icon size selection from predefined values.
     */
    size: {
      control: 'select',
      options: sizesArray,
      description: 'Choose the size of the icon from predefined values. Options: 8, 10, 12, 14, 16, 20, 24, 28, 32, 36, 40, 48, 56.',
    },
    /**
     * Custom color for the icon.
     */
    color: {
      control: 'color',
      description: 'Set a custom color for the icon.',
    },
  },
  decorators: [Story => <div className="p-5 flex gap-4 items-center">{Story()}</div>],
} as Meta;

type Story = StoryObj<typeof Icon>;

/**
 * Default icon story displaying a settings icon.
 */
export const Default: Story = {
  args: {
    component: 'icon-[ic--baseline-settings]',
    size: 24,
    color: '',
  },
  parameters: {
    docs: {
      description: {
        story: 'This story shows the default `Icon` component with a settings icon.',
      },
    },
  },
};

/**
 * Story showcasing multiple icons at once.
 */
export const MultipleIcons = () => (
  <div className="flex gap-4">
    {iconOptions.map(icon => (
      <Icon key={icon} component={icon} size={24} />
    ))}
  </div>
);

MultipleIcons.parameters = {
  docs: {
    description: {
      story: 'This story displays multiple icons at the same time, allowing you to see different variations.',
    },
  },
};
