import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { CheckboxInput } from './CheckboxInput';

const Component: Meta<typeof CheckboxInput> = {
  title: 'Components/Inputs',
  component: CheckboxInput,
  argTypes: { onChange: {} },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const CheckboxInputStory: Story = {
  args: {
    type: 'checkbox',
    label: 'Checkbox component',
    options: ['Option 1', 'Option 2'],
    onChange: value => {},
  },
};

export const CheckboxInputStoryDisabled: Story = {
  args: {
    type: 'checkbox',
    label: 'Checkbox component (all disabled)',
    options: ['Option 1', 'Option 2'],
    disabled: [true, true],
    onChange: value => {},
  },
};

export const CheckboxInputStoryPartiallyDisabled: Story = {
  args: {
    type: 'checkbox',
    label: 'Checkbox component (partially disabled)',
    options: ['Option 1', 'Option 2'],
    disabled: [true, false],
    onChange: value => {},
  },
};
