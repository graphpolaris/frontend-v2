import React, { useState, useEffect } from 'react';
import { Button } from '../buttons';
import Info from '../info';
import { TooltipProvider, Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { NumberProps } from './types';
import styles from './inputs.module.scss';

export const NumberInput = ({
  label,
  placeholder,
  size = 'md',
  value = 0,
  inline = false,
  required = false,
  visible = true,
  variant = 'base',
  errorText,
  validate,
  disabled = false,
  onChange,
  onRevert,
  tooltip,
  info,
  onKeyDown,
  max,
  min,
  className,
  containerClassName,
  lazy = false,
}: NumberProps) => {
  const [isValid, setIsValid] = React.useState<boolean>(true);
  const [inputValue, setInputValue] = useState<number>(value);

  useEffect(() => {
    setInputValue(value);
  }, [value]);

  if (!tooltip && inline && label) tooltip = label;
  return (
    <div className={styles['input'] + `${containerClassName ? ` ${containerClassName}` : ''}`}>
      <TooltipProvider delay={50}>
        <Tooltip>
          <TooltipTrigger
            className={'form-control w-full' + (inline && label ? ' flex flex-row items-center gap-0.5 justify-between' : '')}
          >
            {label && (
              <label className="label p-0 flex-1 min-w-0">
                <span
                  className={`text-${size} text-left font-medium text-secondary-700 line-clamp-2 leading-snug ${required && "after:content-['*'] after:ml-0.5 after:text-danger-500"}`}
                >
                  {label}
                </span>
                {required && isValid ? null : <span className="label-text-alt text-error">{errorText}</span>}
                {info && <Info tooltip={info} />}
              </label>
            )}
            <div className="relative items-center">
              <input
                type="number"
                placeholder={placeholder}
                className={`${size} bg-light border border-secondary-200 placeholder-secondary-400 focus:outline-none block w-full sm:text-sm focus:ring-1 ${
                  isValid ? '' : 'input-error'
                }${className ? ` ${className}` : ''}`}
                value={inputValue.toString()}
                onChange={e => {
                  if (required && validate) {
                    setIsValid(validate(e.target.value));
                  }
                  setInputValue(Number(e.target.value));
                  if (!lazy && onChange) {
                    onChange(Number(e.target.value));
                  }
                }}
                required={required}
                disabled={disabled}
                onKeyDown={e => {
                  if (lazy && e.key === 'Enter') {
                    if (onChange) {
                      onChange(inputValue);
                    }
                  }
                  if (onKeyDown) onKeyDown(e);
                }}
                max={max}
                min={min}
              />
              {lazy && value !== inputValue && (
                <div className="absolute top-0 right-1 h-full flex flex-row items-center">
                  <Button
                    className="hover:bg-success-600 hover:text-white border-none m-0 p-0 h-fit"
                    variant="outline"
                    tooltip="Apply Change"
                    size={size}
                    rounded
                    iconComponent={'icon-[ic--round-check-circle-outline]'}
                    onClick={() => {
                      if (onChange) onChange(inputValue);
                    }}
                  ></Button>
                  <Button
                    className="hover:bg-warning-600 hover:text-white border-none m-0 p-0 h-fit"
                    variant="outline"
                    tooltip="Revert Change"
                    size={size}
                    rounded
                    iconComponent={'icon-[ic--outline-cancel]'}
                    onClick={() => {
                      if (onRevert) onRevert(inputValue);
                      setInputValue(value);
                    }}
                  ></Button>
                </div>
              )}
            </div>
          </TooltipTrigger>
          {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
        </Tooltip>
      </TooltipProvider>
    </div>
  );
};
