import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { RadioInput } from './RadioInput';

const Component: Meta<typeof RadioInput> = {
  title: 'Components/Inputs',
  component: RadioInput,
  argTypes: { onChange: { action: 'changed' } },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const RadioInputStory: Story = (args: any) => {
  const [value, setValue] = useState<string>('');
  return <RadioInput {...args} value={value} onChange={setValue} />;
};

RadioInputStory.args = {
  type: 'radio',
  label: 'Radio component',
  options: ['Option 1', 'Option 2'],
};
