import React, { useState } from 'react';
import Info from '../info';
import { Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { TextProps } from './types';
import styles from './inputs.module.scss';

export const TextInput = ({
  label,
  type,
  placeholder,
  value = '',
  size = 'md',
  required = false,
  visible = true,
  errorText,
  validate,
  disabled = false,
  onChange,
  onClick = () => {},
  inline = false,
  tooltip,
  info,
  className,
  ...props
}: TextProps) => {
  const [isValid, setIsValid] = React.useState<boolean>(true);

  const handleInputChange = (inputValue: string) => {
    if (required && validate) {
      setIsValid(validate(inputValue));
    }
    if (onChange) {
      onChange(inputValue);
    }
  };

  if (!tooltip && inline) tooltip = label;
  return (
    <Tooltip>
      <TooltipTrigger
        className={styles['input'] + ' form-control w-full' + (inline ? ' flex flex-row items-center gap-0.5 justify-between' : '')}
      >
        {label && (
          <label className="label p-0 flex-1 min-w-0">
            <span
              className={`text-${size} font-medium text-secondary-700 line-clamp-2 leading-snug ${required && "after:content-['*'] after:ml-0.5 after:text-danger-500"}`}
            >
              {label}
            </span>
            {required && isValid ? null : <span className="label-text-alt text-error">{errorText}</span>}
            {info && <Info tooltip={info} />}
          </label>
        )}
        <input
          type={visible ? 'text' : 'password'}
          placeholder={placeholder}
          className={
            `${size} bg-light border border-secondary-200 placeholder-secondary-400 w-auto min-w-6 shrink-0 grow-1 focus:outline-none block focus:ring-1 ${
              isValid ? '' : 'input-error'
            }` + (className ? ` ${className}` : '')
          }
          value={value.toString()}
          onChange={e => handleInputChange(e.target.value)}
          onClick={onClick}
          required={required}
          disabled={disabled}
          {...props}
        />
      </TooltipTrigger>
      {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
    </Tooltip>
  );
};
