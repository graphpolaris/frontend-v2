import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { TimeInput } from './TimeInput';

const Component: Meta<typeof TimeInput> = {
  title: 'Components/Inputs',
  component: TimeInput,
  argTypes: { onChange: { action: 'changed' } },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const TimeInputStory: Story = (args: any) => {
  const [value, setValue] = useState<string>('');

  return <TimeInput {...args} value={value} onChange={setValue} />;
};

TimeInputStory.args = {
  type: 'time',
  label: 'Time component',
};
