import { Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { RadioProps } from './types';

export const RadioInput = ({ label, value, options, onChange, tooltip }: RadioProps) => {
  return (
    <Tooltip>
      <TooltipTrigger>
        <label className="label p-0">
          <span className="label-text">{label}</span>
        </label>
        {options.map((option, index) => (
          <label key={index} className="label cursor-pointer w-fit gap-2 px-0 py-1">
            <input
              type="radio"
              name={option}
              checked={value === option}
              onChange={() => {
                if (onChange) {
                  onChange(option);
                }
              }}
              className="radio radio-xs radio-primary"
            />
            <span className="label-text">{option}</span>
          </label>
        ))}
      </TooltipTrigger>
      {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
    </Tooltip>
  );
};
