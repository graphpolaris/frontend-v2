import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { TextInput } from './TextInput';

const Component: Meta<typeof TextInput> = {
  title: 'Components/Inputs',
  component: TextInput,
  argTypes: { onChange: {} },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const TextInputStory: Story = {
  args: {
    type: 'text',
    label: 'Text input',
    placeholder: 'Put here some text',
    required: true,
    onChange: value => {},
  },
};

export const RequiredTextInputStory: Story = {
  args: {
    type: 'text',
    label: 'Text input',
    placeholder: 'Put here some text',
    errorText: 'This field is required',
    validate: value => {
      return value.length > 0;
    },
    onChange: value => {},
  },
};
