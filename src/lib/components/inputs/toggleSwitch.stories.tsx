import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { ToggleSwitchInput } from './ToggleSwitchInput';

const Component: Meta<typeof ToggleSwitchInput> = {
  title: 'Components/Inputs',
  component: ToggleSwitchInput,
  argTypes: { onChange: {} },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const ToggleSwitchInputStory: Story = {
  args: {
    type: 'toggle',
    label: 'Toggle Switch component',
    value: false,
    onChange: value => {},
  },
};
