import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { SliderInput } from './SliderInput';

const Component: Meta<typeof SliderInput> = {
  title: 'Components/Inputs',
  component: SliderInput,
  argTypes: { onChange: {}, unit: {}, showValue: { control: 'boolean' } },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const SliderInputStory: Story = {
  args: {
    type: 'slider',
    label: 'Slider component',
    min: 0,
    max: 1,
    step: 0.1,
    unit: 'px',
    value: 50,
    showValue: true,
    onChange: value => {},
  },
};
