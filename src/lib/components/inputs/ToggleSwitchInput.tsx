import { useState, useEffect } from 'react';
import { Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { ToggleSwitchProps } from './types';

export const ToggleSwitchInput = ({ label, classText, value, tooltip, onChange, position, className }: ToggleSwitchProps) => {
  const [isSelected, setIsSelected] = useState(value);

  useEffect(() => {
    setIsSelected(value);
  }, [value]);

  const handleClick = () => {
    const newValue = !isSelected;
    setIsSelected(newValue);
    if (onChange) {
      onChange(newValue);
    }
  };
  return (
    <Tooltip>
      <TooltipTrigger className={className}>
        <label className="flex items-center space-x-2 rounded-full cursor-pointer hover:bg-secondary-100 pr-3 py-1/2" onClick={handleClick}>
          {position === 'left' && <span className={classText}>{label}</span>}
          <div
            className={`relative  flex w-10 h-5 rounded-full transition-all duration-500 ${isSelected ? 'bg-secondary-800' : 'bg-secondary-300'}`}
          >
            <div
              className={`absolute top-0 left-0 w-5 h-5 rounded-full items-center bg-white transition-all duration-500 shadow-lg border-1 border-gray ${isSelected ? 'translate-x-full' : 'translate-x-0'} `}
            />
          </div>
          {(!position || position === 'right') && <span className={classText}>{label}</span>}
        </label>
      </TooltipTrigger>
      {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
    </Tooltip>
  );
};
