import Info from '../info';
import { Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { BooleanProps } from './types';

export const BooleanInput = ({ label, value, onChange, tooltip, info, size, required, className }: BooleanProps) => {
  return (
    <Tooltip>
      <TooltipTrigger>
        <label className={className + ' py-1 w-full flex justify-between items-center hover:cursor-pointer hover:bg-secondary-50'}>
          {label && (
            <>
              <span
                className={`text-${size} text-left truncate font-medium text-secondary-700 ${required && "after:content-['*'] after:ml-0.5 after:text-danger-500"}`}
              >
                {label}
              </span>
              {info && <Info tooltip={info} />}
            </>
          )}
          <input
            type="checkbox"
            checked={value}
            onChange={event => {
              if (onChange) {
                onChange(event.target.checked);
              }
            }}
            className="checkbox checkbox-xs"
            aria-label={`Toggle ${label}`}
          />
        </label>
      </TooltipTrigger>
      {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
    </Tooltip>
  );
};
