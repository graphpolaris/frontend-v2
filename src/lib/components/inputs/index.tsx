import { BooleanInput } from './BooleanInput';
import { CheckboxInput } from './CheckboxInput';
import { DropdownInput } from './DropdownInput';
import { NumberInput } from './NumberInput';
import { RadioInput } from './RadioInput';
import { SliderInput } from './SliderInput';
import { TextInput } from './TextInput';
import { TimeInput } from './TimeInput';
import { ToggleSwitchInput } from './ToggleSwitchInput';
import {
  InputProps,
  SliderProps,
  TextProps,
  CheckboxProps,
  DropdownProps,
  RadioProps,
  BooleanProps,
  NumberProps,
  TimeProps,
  ToggleSwitchProps,
} from './types';

export const Input = (props: InputProps) => {
  switch (props.type) {
    case 'slider':
      return <SliderInput {...(props as SliderProps)} />;
    case 'text':
      return <TextInput {...(props as TextProps)} />;
    case 'checkbox':
      return <CheckboxInput {...(props as CheckboxProps)} />;
    case 'dropdown':
      return <DropdownInput {...(props as DropdownProps)} />;
    case 'radio':
      return <RadioInput {...(props as RadioProps)} />;
    case 'boolean':
      return <BooleanInput {...(props as BooleanProps)} />;
    case 'number':
      return <NumberInput {...(props as NumberProps)} />;
    case 'time':
      return <TimeInput {...(props as TimeProps)} />;
    case 'toggle':
      return <ToggleSwitchInput {...(props as ToggleSwitchProps)} />;
    default:
      return null;
  }
};
