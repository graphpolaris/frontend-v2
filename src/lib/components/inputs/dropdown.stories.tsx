import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { DropdownInput } from './DropdownInput';

const Component: Meta<typeof DropdownInput> = {
  title: 'Components/Inputs',
  component: DropdownInput,
  argTypes: { onChange: {} },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const DropdownInputStory: Story = {
  args: {
    type: 'dropdown',
    label: 'Select component',
    options: ['Option 1', 'Option 2'],
    disabled: true,
    onChange: value => {},
  },
};
