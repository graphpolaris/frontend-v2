import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { NumberInput } from './NumberInput';

const Component: Meta<typeof NumberInput> = {
  title: 'Components/Inputs',
  component: NumberInput,
  argTypes: { onChange: {} },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const NumberInputStory: Story = {
  args: {
    type: 'number',
    label: 'Number input',
    placeholder: 'Put here a number',
    required: true,
    onChange: value => {},
  },
};

export const RequiredNumberInputStory: Story = {
  args: {
    type: 'number',
    label: 'Number input',
    placeholder: 'Put here a number',
    errorText: 'This field is required',
    validate: value => {
      return value.length > 0;
    },
    onChange: value => {},
  },
};
