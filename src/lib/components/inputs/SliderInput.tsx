import { Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { SliderProps } from './types';
import styles from './slider.module.scss';
import { useState } from 'react';

export const SliderInput = ({
  label,
  value,
  min,
  max,
  step,
  unit,
  showValue = true,
  onChange,
  tooltip,
  className,
  size,
  onChangeConfirmed,
}: SliderProps) => {
  const [currentValue, setCurrentValue] = useState(value);

  return (
    <Tooltip>
      <TooltipTrigger>
        <div className={`${styles['slider']} ${size ? styles[`slider-${size}`] : ''} ${className}`}>
          <label className="label flex flex-row justify-between items-end m-0 p-0">
            <span className="label-text">{label}</span>
            {showValue ? (
              <span className="label-text">
                {currentValue}
                {unit}
              </span>
            ) : null}
          </label>

          <input
            type="range"
            min={min}
            max={max}
            step={step}
            value={currentValue}
            onChange={e => {
              setCurrentValue(parseFloat(e.target.value));
              if (onChange) {
                onChange(parseFloat(e.target.value));
              }
            }}
            onMouseUp={e => {
              if (onChangeConfirmed) {
                onChangeConfirmed(currentValue);
              }
            }}
            aria-labelledby="input-slider"
          />
        </div>
      </TooltipTrigger>
      {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
    </Tooltip>
  );
};
