import { Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { TimeProps } from './types';

export const TimeInput = ({ label, value, onChange, tooltip, showTimezone = true }: TimeProps) => {
  const hours = Array.from({ length: 24 }, (_, i) => i.toString().padStart(2, '0'));
  const minutes = Array.from({ length: 60 }, (_, i) => i.toString().padStart(2, '0'));
  const timezones = ['UTC', 'CET', 'EDT'];

  const splitValue = value.split(':');
  const hour = splitValue[0] || '00';
  const minute = splitValue[1] || '00';
  const timezone = splitValue[2] || 'UTC';

  const handleTimeChange = (newHour: string, newMinute: string) => {
    const updatedTime = `${newHour}:${newMinute}:${timezone}`;
    if (onChange) onChange(updatedTime);
  };

  const handleTimezoneChange = (newTimezone: string) => {
    const updatedTime = `${hour}:${minute}:${newTimezone}`;
    if (onChange) onChange(updatedTime);
  };

  return (
    <Tooltip>
      <TooltipTrigger>
        <label className={`label cursor-pointer w-fit gap-2 px-0 py-1`}>
          <span className="text-sm">{label}</span>
        </label>

        <div className="flex items-center gap-2">
          <select
            value={hour}
            onChange={e => handleTimeChange(e.target.value, minute)}
            className="select select-sm bg-secondary-0 hover:bg-secondary-100"
          >
            {hours.map(hourOption => (
              <option key={hourOption} value={hourOption}>
                {hourOption}
              </option>
            ))}
          </select>
          <span>H</span>
          <span>:</span>

          <select value={minute} onChange={e => handleTimeChange(hour, e.target.value)} className="select select-sm">
            {minutes.map(minuteOption => (
              <option key={minuteOption} value={minuteOption}>
                {minuteOption}
              </option>
            ))}
          </select>
          <span>M</span>
        </div>

        {showTimezone && (
          <div className="flex items-center gap-2 mt-2">
            <label className="text-sm">Timezone:</label>
            <select value={timezone} onChange={e => handleTimezoneChange(e.target.value)} className="select select-sm">
              {timezones.map(timezoneOption => (
                <option key={timezoneOption} value={timezoneOption}>
                  {timezoneOption}
                </option>
              ))}
            </select>
          </div>
        )}
      </TooltipTrigger>
      {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
    </Tooltip>
  );
};
