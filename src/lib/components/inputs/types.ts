export type CommonInputProps<T extends string, V> = {
  label: string;
  type: T;
  value: V;
  tooltip?: string | React.ReactNode;
  required?: boolean;
  info?: string;
  onChange?: (value: V) => void;
  style?: React.CSSProperties;
  className?: string;
  ref?: React.RefObject<HTMLInputElement | null>;
  onMouseDownCapture?: (e: React.MouseEvent<HTMLInputElement>) => void;
  onMouseUpCapture?: (e: React.MouseEvent<HTMLInputElement>) => void;
};

export type SliderProps = CommonInputProps<'slider', number> & {
  min: number;
  max: number;
  step: number;
  showValue?: boolean;
  size?: 'xs' | 'sm' | 'md' | 'xl';
  unit?: string;
  onChangeConfirmed?: (value: number) => void;
};

export type TextProps = CommonInputProps<'text', string> & {
  size?: 'xs' | 'sm' | 'md' | 'xl';
  placeholder?: string;
  errorText?: string;
  visible?: boolean;
  disabled?: boolean;
  inline?: boolean;
  className?: string;
  validate?: (value: any) => boolean;
  onClick?: (e: React.MouseEvent<HTMLInputElement>) => void;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  onKeyUp?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  autoFocus?: boolean;
};

export type NumberProps = CommonInputProps<'number', number> & {
  size?: 'xs' | 'sm' | 'md' | 'xl';
  variant?: 'primary' | 'secondary' | 'danger' | 'warning' | 'success' | 'info' | 'base';
  placeholder?: string;
  errorText?: string;
  visible?: boolean;
  disabled?: boolean;
  inline?: boolean;
  validate?: (value: any) => boolean;
  onRevert?: (value: number) => void;
  onKeyDown?: (event: React.KeyboardEvent<HTMLInputElement>) => void;
  max?: number;
  min?: number;
  containerClassName?: string;
  lazy?: boolean;
};

export type CheckboxProps = CommonInputProps<'checkbox', Array<string>> & {
  options: Array<string>;
  disabled?: Array<boolean>;
};

export type BooleanProps = CommonInputProps<'boolean', boolean> & {
  options?: any;
  size?: 'xs' | 'sm' | 'md' | 'xl';
  inline?: boolean;
};

export type ToggleSwitchProps = CommonInputProps<'toggle', boolean> & {
  classText?: string;
  position?: 'left' | 'right';
};

export type TimeProps = CommonInputProps<'time', string> & {
  tooltip?: string;
  showTimezone: boolean;
};

export type RadioProps = CommonInputProps<'radio', string> & {
  options: Array<string>;
};

export type DropdownProps = CommonInputProps<'dropdown', number | string | undefined> & {
  overrideRender?: React.ReactNode;
  size?: 'xs' | 'sm' | 'md' | 'xl';
  inline?: boolean;
  buttonVariant?: 'primary' | 'outline' | 'ghost';
  disabled?: boolean;
  options: number[] | string[] | { [key: string]: string }[];
  autocomplete?: boolean;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
};

export type InputProps =
  | TextProps
  | SliderProps
  | CheckboxProps
  | DropdownProps
  | RadioProps
  | BooleanProps
  | NumberProps
  | TimeProps
  | ToggleSwitchProps;
