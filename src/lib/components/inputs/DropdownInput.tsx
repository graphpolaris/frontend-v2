import React, { useEffect, useRef } from 'react';
import { DropdownContainer, DropdownTrigger, DropdownItemContainer, DropdownItem } from '../dropdowns';
import Info from '../info';
import { Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { DropdownProps } from './types';
import { TextInput } from './TextInput';

function parsedValue(item: number | string | { [key: string]: string }, key: boolean = false) {
  switch (typeof item) {
    case 'number':
      return item.toString();
    case 'object':
      return key ? Object.keys(item)[0] : Object.values(item)[0];
  }

  return item;
}

function currentValue(value: string | number | undefined, options?: { [key: string]: string }[]) {
  if (options != null && typeof options[0] == 'object') {
    return parsedValue(options.find(x => x[value as string]) as { [key: string]: string });
  }

  return value;
}

const AutocompleteRenderer = ({
  onChange,
  value,
  onKeyDown,
}: {
  onChange: (e: string) => void;
  value: string;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
}) => {
  const ref = useRef<HTMLInputElement>(null);

  return (
    <TextInput
      ref={ref}
      type="text"
      placeholder="type here"
      size="xs"
      className="mb-0 mt-0 rounded-sm"
      value={value as string}
      onChange={onChange as (value: string) => void}
      onKeyDown={onKeyDown}
      onMouseDownCapture={e => e.stopPropagation()}
      label={''}
    />
  );
};

export const DropdownInput = ({
  label,
  value,
  overrideRender,
  options,
  onChange,
  required = false,
  tooltip,
  size = 'sm',
  inline = true,
  disabled = false,
  buttonVariant = 'primary',
  className = '',
  autocomplete = false,
  onKeyDown,
  info,
}: DropdownProps) => {
  const [isDropdownOpen, setIsDropdownOpen] = React.useState<boolean>(false);
  const [filterInput, setFilterInput] = React.useState<string | number | undefined>(value);
  const [filteredOptions, setFilteredOptions] = React.useState<(string | number | { [key: string]: string })[]>(options);

  useEffect(() => {
    setFilteredOptions(options);
  }, [options]);

  const handleInputChange = (e: string) => {
    setFilterInput(e);
    const inputValue = e.toLowerCase();

    if (inputValue === '') {
      setFilteredOptions(options);
      return;
    }

    const newFilteredOptions = options.filter(option => {
      const optionText = parsedValue(option).toLowerCase();
      return optionText.startsWith(inputValue);
    });

    setFilteredOptions(newFilteredOptions);
  };

  function handleKeyDown(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      if (onChange) {
        onChange(filterInput as string);
      }
      if (onKeyDown) {
        onKeyDown(e);
      }
    }
  }

  if (!tooltip && inline) tooltip = label;

  return (
    <Tooltip>
      <TooltipTrigger className={'w-full' + (inline ? ' flex flex-row items-center gap-0.5 justify-between' : '') + ' ' + className}>
        {label && (
          <label className="label p-0 flex-1 min-w-0">
            <span
              className={`text-${size} text-left font-medium text-secondary-700 line-clamp-2 leading-snug ${required && "after:content-['*'] after:ml-0.5 after:text-danger-500"}`}
            >
              {label}
            </span>
            {info && <Info tooltip={info} />}
          </label>
        )}
        <DropdownContainer
          open={isDropdownOpen}
          onOpenChange={ref => {
            setIsDropdownOpen(ref);
          }}
        >
          <DropdownTrigger
            variant={buttonVariant}
            title={overrideRender || currentValue(value, options as { [key: string]: string }[])}
            size={size}
            className="cursor-pointer w-full"
            disabled={disabled}
            onClick={() => {
              setIsDropdownOpen(!isDropdownOpen);
            }}
          >
            {overrideRender ? (
              overrideRender
            ) : autocomplete ? (
              <AutocompleteRenderer onChange={handleInputChange} value={filterInput as string} onKeyDown={handleKeyDown} />
            ) : null}
          </DropdownTrigger>
          {isDropdownOpen && (
            <DropdownItemContainer className="overflow-auto">
              {filteredOptions &&
                filteredOptions.map((item: any, index: number) => (
                  <DropdownItem
                    key={index}
                    value={parsedValue(item)}
                    selected={value === item}
                    onClick={() => {
                      setFilterInput(item);
                      if (onChange) {
                        onChange(parsedValue(item, true));
                      }
                      setIsDropdownOpen(false);
                    }}
                  />
                ))}
            </DropdownItemContainer>
          )}
        </DropdownContainer>
      </TooltipTrigger>
      {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
    </Tooltip>
  );
};
