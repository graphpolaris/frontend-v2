import { Tooltip, TooltipTrigger, TooltipContent } from '../tooltip';
import { CheckboxProps } from './types';

export const CheckboxInput = ({ label, value, options, onChange, tooltip, disabled }: CheckboxProps) => {
  return (
    <Tooltip>
      <TooltipTrigger>
        {label && (
          <label className="label p-0">
            <span className="label-text">{label}</span>
          </label>
        )}
        {options.map((option, index) => (
          <label key={index} className="label cursor-pointer w-fit gap-2 px-0 py-1">
            <input
              type="checkbox"
              name={option}
              checked={Array.isArray(value) && value.includes(option)}
              onClick={event => {
                if (event.ctrlKey) {
                  const updatedValue = (event.target as any).checked ? [option] : value.filter(val => val !== option);
                  if (onChange) {
                    onChange(updatedValue);
                  }
                } else {
                  const updatedValue = (event.target as any).checked ? [...value, option] : value.filter(val => val !== option);
                  if (onChange) {
                    onChange(updatedValue);
                  }
                }
              }}
              onChange={() => {
                // to remove warning
              }}
              className="checkbox checkbox-xs"
              disabled={disabled === undefined ? false : options.length === disabled.length ? disabled[index] : false}
            />
            <span className="label-text">{option}</span>
          </label>
        ))}
      </TooltipTrigger>
      {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
    </Tooltip>
  );
};
