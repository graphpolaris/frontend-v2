import React, { useState } from 'react';
import { Button } from '@/lib/components/buttons';
import { Input } from '@/lib/components/inputs';

interface FieldConfig<T> {
  key: keyof T;
  label: string;
  type: 'text' | 'dropdown';
  options?: string[];
  required?: boolean;
}

interface TableUIProps<T> {
  data: T[];
  onDataChange: (updatedData: T[]) => void;
  fieldConfigs: FieldConfig<T>[];
  dropdownOptions: Record<string, string[]>;
}

export const TableUI = <T extends Record<string, any>>({ data, fieldConfigs, dropdownOptions, onDataChange }: TableUIProps<T>) => {
  const [editingIndex, setEditingIndex] = useState<number | null>(null);
  const [editItem, setEditItem] = useState<T | null>(null);

  const handleAddRow = () => {
    const newItem = {} as T;
    fieldConfigs.forEach(config => {
      newItem[config.key] = config.type === 'dropdown' ? ('' as T[keyof T]) : ('' as T[keyof T]);
    });
    onDataChange([...data, newItem]);
    setEditingIndex(data.length);
    setEditItem(newItem);
  };

  const handleDeleteRow = (index: number) => {
    const updatedData = data.filter((_, i) => i !== index);
    onDataChange(updatedData);
  };

  const handleEditRow = (index: number) => {
    setEditingIndex(index);
    setEditItem(data[index]);
  };

  const handleSaveEdit = () => {
    if (editingIndex !== null && editItem) {
      const updatedData = data.map((item, index) => (index === editingIndex ? editItem : item));
      onDataChange(updatedData);
      setEditingIndex(null);
      setEditItem(null);
    }
  };

  const handleCancelEdit = () => {
    if (editingIndex === data.length - 1) {
      onDataChange(data.slice(0, -1));
    }
    setEditingIndex(null);
    setEditItem(null);
  };

  const handleInputChange = (key: keyof T, value: string) => {
    setEditItem(prev => (prev ? { ...prev, [key]: value } : null));
  };

  return (
    <div className="mt-2 w-full overflow-x-auto">
      <table className="min-w-full bg-light border border-secondary-300 rounded-md">
        <thead>
          <tr className="bg-secondary-100 border-b">
            {fieldConfigs.map(field => (
              <th key={field.key.toString()} className="px-6 py-3 text-left text-xs font-medium uppercase tracking-wider">
                {field.label}
              </th>
            ))}
            <th className="px-6 py-3 text-left text-xs font-medium uppercase tracking-wider">Actions</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
            <tr key={index} className="border-b rounded-md">
              {fieldConfigs.map(field => (
                <td key={field.key.toString()} className="px-6 py-4 text-sm text-light-700">
                  {editingIndex === index ? (
                    field.type === 'dropdown' ? (
                      <Input
                        type="dropdown"
                        label=""
                        options={dropdownOptions[field.key.toString()] || []}
                        value={editItem ? editItem[field.key] : ''}
                        onChange={value => handleInputChange(field.key, value.toString())}
                      />
                    ) : (
                      <Input
                        type="text"
                        size="xs"
                        value={editItem ? editItem[field.key] : ''}
                        required={field.required}
                        onChange={e => handleInputChange(field.key, e)}
                      />
                    )
                  ) : (
                    item[field.key]
                  )}
                </td>
              ))}
              <td className="px-6 py-4 text-sm text-secondary-700">
                <div className="flex space-x-2">
                  {editingIndex === index ? (
                    <>
                      <Button variant="solid" size="sm" label="Save" onClick={handleSaveEdit} />
                      <Button variant="ghost" size="sm" label="Cancel" onClick={handleCancelEdit} />
                    </>
                  ) : (
                    <>
                      <Button
                        variant="ghost"
                        size="lg"
                        iconComponent={'icon-[ic--baseline-delete-outline]'}
                        className="text-danger-500"
                        onClick={() => handleDeleteRow(index)}
                      />
                      <Button
                        variantType="secondary"
                        variant="ghost"
                        size="lg"
                        iconComponent={'icon-[ic--baseline-mode-edit]'}
                        onClick={() => handleEditRow(index)}
                      />
                    </>
                  )}
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
