import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { TableUI } from './TableUI'; // Import the TableUI component from the correct path

const metaTableUI: Meta<typeof TableUI> = {
  component: TableUI,
  title: 'Components/TableUI',
};

export default metaTableUI;

type Story = StoryObj<typeof TableUI>;

interface SampleData {
  name: string;
  age: string;
  email: string;
  role: string;
}

const fieldConfigs = [
  { key: 'name', label: 'Name', type: 'text' as const, required: true },
  { key: 'age', label: 'Age', type: 'text' as const, required: true },
  { key: 'email', label: 'Email', type: 'text' as const, required: true },
  { key: 'role', label: 'Role', type: 'dropdown' as const, options: ['Admin', 'User', 'Guest'] },
];

const dropdownOptions = {
  role: ['Admin', 'User', 'Guest'],
};

export const MainStory: Story = {
  render: args => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [tableData, setTableData] = useState<SampleData[]>([
      { name: 'John Doe', age: '28', email: 'j.doe@email.com', role: 'Admin' },
      { name: 'Jane Smith', age: '34', email: 'j.smith@email.com', role: 'User' },
    ]);

    const handleDataChange = (updatedData: SampleData[]) => {
      setTableData(updatedData);
    };

    return <TableUI {...args} data={tableData} onDataChange={handleDataChange} />;
  },
  args: {
    fieldConfigs: fieldConfigs,
    dropdownOptions: dropdownOptions,
  },
};
