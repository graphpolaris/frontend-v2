import { Icon, Sizes } from '@/lib/components/icon';
import { Tooltip, TooltipContent, TooltipTrigger, TooltipProvider } from '@/lib/components/tooltip';

type Props = {
  tooltip: string;
  placement?: 'top' | 'bottom' | 'left' | 'right';
  size?: Sizes;
};

export default function Info({ tooltip, placement = 'top', size = 14 }: Props) {
  return (
    <TooltipProvider delay={100}>
      <Tooltip placement={placement}>
        <TooltipTrigger>
          <Icon component="icon-[ic--outline-info]" size={size} />
        </TooltipTrigger>
        <TooltipContent>
          <p>{tooltip}</p>
        </TooltipContent>
      </Tooltip>
    </TooltipProvider>
  );
}
