import { StoryObj, Meta } from '@storybook/react';
import { sizesArray } from '@/lib/components/icon';
import Info from '.';

export default {
  title: 'Components/Info',
  component: Info,
  tags: ['autodocs'],
  decorators: [Story => <div className="flex items-center justify-center min-h-52">{Story()}</div>],
  argTypes: {
    placement: {
      control: 'select',
      options: ['top', 'bottom', 'left', 'right'],
    },
    size: {
      control: 'select',
      options: sizesArray,
    },
  },
} as Meta;
type Story = StoryObj<typeof Info>;

const InfoButton: Story = {
  args: {
    tooltip: 'This is an info tooltip',
    size: 14,
  },
};

export const Default = { ...InfoButton };
