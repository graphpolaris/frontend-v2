import React, { useContext, useMemo } from 'react';
import { Position } from 'reactflow';
import { pillHeight, pillWidth, topLineHeight, pillBorderWidth } from './pill.const';
import { PillContext } from './PillContext';

export const PillHandle = (props: {
  handleTop?: 'auto' | 'fixed';
  hidden?: boolean;
  isValidConnection?: boolean;
  children?: React.ReactNode;
  className?: string;
  position: Position;
  type: 'square' | 'diamond' | 'arrowUp' | 'arrowDown';
  mr?: number;
  outerSize?: number;
  innerSize?: number;
}) => {
  const pillContext = useContext(PillContext);
  const outerSize = props.outerSize || (['square', 'diamond'].includes(props.type) ? 6 : 4);
  const innerSize = props.innerSize || (['square', 'diamond'].includes(props.type) ? 4 : 6);

  const style: React.CSSProperties = {
    width: outerSize * 2,
    height: outerSize * 2,
  };
  if (props.position === Position.Left) {
    style.left = -outerSize; // round size
    style.top = props.handleTop === 'auto' ? `auto` : pillHeight / 2 - outerSize;
  } else if (props.position === Position.Right) {
    style.left = pillWidth + (props.mr || 0) - outerSize; // width of pill
    style.top = props.handleTop === 'auto' ? `auto` : pillHeight / 2 - outerSize;
  } else if (props.position === Position.Top) {
    style.left = pillWidth / 2 - outerSize;
    style.top = -outerSize - innerSize / 2 + topLineHeight + pillBorderWidth;
  } else if (props.position === Position.Bottom) {
    style.left = pillWidth / 2 - outerSize;
    style.top = pillHeight - outerSize + innerSize / 2 - topLineHeight;
  }

  const innerStyle: React.CSSProperties = { width: innerSize * 2, height: innerSize * 2 };
  innerStyle.left = outerSize - innerSize;
  innerStyle.top = outerSize - innerSize;
  innerStyle.fill = pillContext.color;

  const innerHandle = useMemo(() => {
    if (props.type === 'square')
      return (
        <svg
          className={'absolute pointer-events-none ' + props.className}
          style={innerStyle}
          width="8"
          height="8"
          viewBox="0 0 8 8"
          fill="none"
        >
          <rect x="0.5" y="0.5" width="7" height="7" className={props.className} />
        </svg>
      );
    if (props.type === 'diamond')
      return (
        <svg
          className={'absolute pointer-events-none ' + props.className}
          style={innerStyle}
          width="8"
          height="8"
          viewBox="0 0 8 8"
          fill="none"
        >
          <rect
            x="0.5"
            y="0.5"
            width="7"
            height="7"
            className={props.className}
            style={{ transformOrigin: 'center center' }}
            transform="rotate(45)"
          />
        </svg>
      );
    if (props.type === 'arrowUp')
      return (
        <svg
          width="14"
          height="7"
          viewBox="0 0 14 7"
          fill="none"
          className={'absolute pointer-events-none ' + props.className}
          style={innerStyle}
        >
          <path d="M14 7H0L7 0L14 7Z" className={props.className} />
        </svg>
      );
    if (props.type === 'arrowDown')
      return (
        <svg
          width="14"
          height="7"
          viewBox="0 0 14 7"
          fill="none"
          className={'absolute pointer-events-none ' + props.className}
          style={innerStyle}
        >
          <path d="M14 0H0L7 7L14 0Z" className={props.className} />
        </svg>
      );
    return <></>;
  }, [props.type, props.className, innerStyle, pillContext.color]);

  return (
    <>
      <div
        className="absolute z-40"
        style={{
          ...style,
          top: style.top as number,
          left: style.left as number,
        }}
      >
        {props.children}
      </div>
      <div className="absolute z-40 pointer-events-none" style={style}>
        {innerHandle}
      </div>
    </>
  );
};
