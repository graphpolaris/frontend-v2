export const pillHeight = 26;
export const pillInnerMargin = 2;
export const pillXPadding = 10;
export const pillAttributesPadding = 2;
export const pillWidth = 154;
export const topLineHeight = 3;
export const pillBorderWidth = 1;
