import React from 'react';
import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { EntityPill } from '../Pill';

const Component: Meta<typeof EntityPill> = {
  title: 'Pills/Pill',
  component: EntityPill,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div>{story()}</div>
      </Provider>
    ),
  ],
};

export default Component;

const Mockstore = configureStore({
  reducer: {},
});

export const EntityPillStory = {
  args: {
    title: 'TestEntity',
    children: <div></div>,
  },
};
