import React from 'react';
import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';

import { Pill } from '../Pill';

const Component: Meta<typeof Pill> = {
  title: 'Pills/Pill',
  component: Pill,
  decorators: [story => <Provider store={Mockstore}>{story()}</Provider>],
};

export default Component;

const Mockstore = configureStore({
  reducer: {},
});

export const Default = {
  args: {
    title: 'TestEntity',
    children: <div></div>,
  },
};
