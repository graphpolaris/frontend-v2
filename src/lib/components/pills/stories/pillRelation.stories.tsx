import React from 'react';
import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { RelationPill } from '../Pill';

const Component: Meta<typeof RelationPill> = {
  title: 'Pills/Pill',
  component: RelationPill,
  decorators: [
    story => (
      <Provider store={Mockstore}>
        <div>{story()}</div>
      </Provider>
    ),
  ],
};

export default Component;

const Mockstore = configureStore({
  reducer: {},
});

export const RelationPillStory = {
  args: {
    title: 'TestEntity',
    children: <div></div>,
  },
};
