import React from 'react';
import { Meta } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';

import { LogicPill } from '../Pill';

const Component: Meta<typeof LogicPill> = {
  title: 'Pills/Pill',
  component: LogicPill,
  decorators: [story => <Provider store={Mockstore}>{story()}</Provider>],
};

export default Component;

const Mockstore = configureStore({
  reducer: {},
});

export const LogicPillStory = {
  args: {
    title: 'TestEntity',
    children: <div></div>,
  },
};
