import React, { useContext, useState } from 'react';
import { pillWidth, pillHeight, pillXPadding, pillInnerMargin, topLineHeight, pillBorderWidth, pillAttributesPadding } from './pill.const';
import { Position } from 'reactflow';
import { PillHandle } from './PillHandle';
import { PillContext } from './PillContext';
import { QueryRelationDirection } from 'ts-common';

export type PillI = {
  onHovered?: (hovered: boolean) => void;
  // onDragged: (dragged: boolean) => void;
  title: string | React.ReactNode;
  children?: React.ReactNode;
  handles?: React.ReactNode;
  style?: React.CSSProperties;
  corner?: 'rounded' | 'square' | 'diamond' | 'diamond-inner' | 'diamond-left' | 'diamond-right';
  direction?: QueryRelationDirection;
  handleUp?: React.ReactNode;
  handleDown?: React.ReactNode;
  handleLeft?: React.ReactNode;
  handleRight?: React.ReactNode;
  className?: string;
  draggable?: boolean;
};

export const Pill = React.memo((props: PillI) => {
  const pillContext = useContext(PillContext);
  const [hovered, setHovered] = useState(false);

  const onMouseEnter = (event: React.MouseEvent) => {
    if (!hovered) {
      setHovered(true);
      if (props.onHovered) props.onHovered(true);
    }
  };

  const onMouseLeave = (event: React.MouseEvent) => {
    if (hovered) {
      setHovered(false);
      if (props.onHovered) props.onHovered(false);
    }
  };

  const width = pillWidth;
  const corner = props.corner || 'rounded';

  const innerContentStyle = {
    minWidth: corner === 'diamond' ? width - pillBorderWidth * 2 : width - pillBorderWidth * 2,
    maxWidth: corner === 'diamond' ? width - pillBorderWidth * 2 : width - pillBorderWidth * 2,
    minHeight: pillHeight - pillBorderWidth * 2,
    maxHeight: pillHeight - pillBorderWidth * 2,
    margin: pillBorderWidth,
    clipPath: 'none',
  };

  const outerContentStyle = {
    minWidth: width,
    maxWidth: width,
    minHeight: pillHeight,
    maxHeight: pillHeight,
    clipPath: 'none',
  };

  if (corner === 'diamond') {
    innerContentStyle.clipPath = 'polygon(0.5% 50%, 4.9% 0%, 95.1% 0%, 99.5% 50%, 95.1% 100%, 4.9% 100%)';
    outerContentStyle.clipPath = 'polygon(0% 50%, 5% 0%, 95% 0%, 100% 50%, 95% 100%, 5% 100%)';
  } else if (corner === 'diamond-inner') {
    innerContentStyle.clipPath = 'polygon(5% 50%, 0% 0%, 100% 0%, 95% 50%, 100% 100%, 0% 100%)';
    outerContentStyle.clipPath = 'polygon(5% 50%, 0% 0%, 100% 0%, 95% 50%, 100% 100%, 0% 100%)';
  } else if (corner === 'diamond-left') {
    innerContentStyle.clipPath = 'polygon(0.5% 50%, 4.9% 0%, 100% 0%, 95% 50%, 100% 100%, 0% 100%)';
    outerContentStyle.clipPath = 'polygon(0% 50%, 5% 0%, 100% 0%, 95% 50%, 100% 100%, 5% 100%)';
  } else if (corner === 'diamond-right') {
    innerContentStyle.clipPath = 'polygon(5% 50%, 0% 0%, 95.1% 0%, 99.5% 50%, 95.1% 100%, 0% 100%)';
    outerContentStyle.clipPath = 'polygon(5% 50%, 0% 0%, 95% 0%, 100% 50%, 95% 100%, 0% 100%)';
  }

  const onDragStart = (event: React.DragEvent<HTMLDivElement>) => {
    const pill = event.target as HTMLDivElement;
    const dragImage = pill.cloneNode(true) as HTMLDivElement;
    dragImage.style.transform = 'translate(0, 0)'; // Removes the background from drag image in Chrome
    document.body.appendChild(dragImage);

    const mouse_x = event.clientX - pill.getBoundingClientRect().left;
    const mouse_y = event.clientY - pill.getBoundingClientRect().top;

    event.dataTransfer.setDragImage(dragImage, mouse_x, mouse_y);
    event.dataTransfer.setData('mouse_x', String(mouse_x));
    event.dataTransfer.setData('mouse_y', String(mouse_y));

    setTimeout(() => {
      dragImage.remove();
    }, 1);
  };

  return (
    <div
      draggable={props.draggable || false}
      onDragStart={props.draggable ? onDragStart : undefined}
      className={(props.className ? props.className + ' ' : '') + 'flex flex-row flex-grow-0 text-xs text-justify'}
    >
      <div
        className={'bg-secondary-200 ' + (corner !== 'square' ? 'rounded' : '')}
        style={{
          ...outerContentStyle,
        }}
      >
        <div
          className={'bg-secondary-100 ' + (corner !== 'square' ? 'rounded-[3px]' : '')}
          style={{
            ...innerContentStyle,
          }}
        >
          {pillContext.color && (
            <div
              className={corner !== 'square' ? 'rounded-t ' : ''}
              style={{ height: topLineHeight, backgroundColor: pillContext.color }}
            ></div>
          )}
          <div
            className={'font-semibold ' + (corner !== 'square' ? 'rounded-b-[3px]' : '')}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
          >
            <div
              className=""
              style={{
                transform: `translateY(${topLineHeight / 3}px)`,
                paddingLeft: pillXPadding + (corner === 'diamond' ? 5 : 0),
                paddingRight: pillXPadding + (corner === 'diamond' ? 5 : 0),
              }}
            >
              {props.title}
            </div>
          </div>
        </div>
      </div>

      <div
        className="absolute -z-10"
        style={{
          top: pillHeight - pillInnerMargin + (corner === 'diamond' ? pillHeight / 2 - pillBorderWidth : 0),
          width: pillWidth - (corner === 'diamond' ? pillWidth * 0.05 : 0),
          paddingLeft: pillAttributesPadding + (corner === 'diamond' ? pillWidth * 0.05 : 0),
          paddingRight: pillAttributesPadding,
        }}
      >
        {props.children}
      </div>
      <div className="absolute z-50 pointer-events-auto bg-black">{props.handles}</div>
    </div>
  );
});

export const EntityPill = React.memo((props: Omit<PillI, 'topColor'> & { withHandles?: 'vertical' | 'horizontal' }) => {
  const handles = !props.withHandles ? undefined : props.withHandles === 'horizontal' ? (
    <>
      <PillHandle position={Position.Left} className={'stroke-secondary-200'} type="square">
        {props.handleLeft}
      </PillHandle>
      <PillHandle position={Position.Right} className={'stroke-secondary-200'} type="square">
        {props.handleRight}
      </PillHandle>
    </>
  ) : (
    <>
      <PillHandle position={Position.Top} className={'stroke-secondary-200 stroke-1'} type="arrowUp">
        {props.handleUp}
      </PillHandle>
      <PillHandle position={Position.Bottom} className={'stroke-secondary-200 stroke-1'} type="arrowDown">
        {props.handleDown}
      </PillHandle>
    </>
  );
  return (
    <PillContext.Provider value={{ color: 'hsl(var(--clr-node))' }}>
      <Pill {...props} corner="rounded" handles={handles} />
    </PillContext.Provider>
  );
});

export const RelationPill = React.memo((props: Omit<PillI, 'topColor'> & { withHandles?: 'vertical' | 'horizontal' }) => {
  const handles = !props.withHandles ? undefined : props.withHandles === 'horizontal' ? (
    <>
      <PillHandle position={Position.Left} className={'stroke-secondary-200'} type="square">
        {props.handleLeft}
      </PillHandle>
      <PillHandle position={Position.Right} className={'stroke-secondary-200'} type="square" mr={-pillBorderWidth}>
        {props.handleRight}
      </PillHandle>
    </>
  ) : (
    <>
      <PillHandle position={Position.Top} className={'stroke-secondary-200 stroke-1'} type="arrowUp">
        {props.handleUp}
      </PillHandle>
      <PillHandle position={Position.Bottom} className={'stroke-secondary-200 stroke-1'} type="arrowDown">
        {props.handleDown}
      </PillHandle>
    </>
  );

  return (
    <PillContext.Provider value={{ color: 'hsl(var(--clr-relation))' }}>
      <Pill
        {...props}
        corner={props.direction === 'Left' ? 'diamond-left' : props.direction === 'Right' ? 'diamond-right' : 'diamond'}
        handles={handles}
      />
    </PillContext.Provider>
  );
});

export const LogicPill = React.memo((props: Omit<PillI, 'topColor'>) => {
  const handles = (
    <PillHandle position={Position.Left} className={'stroke-white'} type="square">
      {props.handleLeft}
    </PillHandle>
  );

  return (
    <PillContext.Provider value={{ color: 'hsl(var(--clr-filter))' }}>
      <Pill {...props} corner="square" handles={handles} />
    </PillContext.Provider>
  );
});
