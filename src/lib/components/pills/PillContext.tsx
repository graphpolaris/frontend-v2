import { createContext } from 'react';

export const PillContext = createContext({
  color: 'hsl(var(--clr-node))',
});
