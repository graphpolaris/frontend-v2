import React, { useState } from 'react';
import { Button } from '../buttons';
import { EntityPill } from '@/lib/components/pills/Pill';
import { DropdownContainer, DropdownItemContainer, DropdownTrigger, DropdownItem } from '../dropdowns';

export type EntityPillSelectorProps = {
  selectedNode?: string;
  dropdownNodes: string[];
  onSelectOption: (option: string) => void;
};

export function EntityPillSelector({ dropdownNodes, onSelectOption, selectedNode }: EntityPillSelectorProps) {
  const [isCollapsed, setIsCollapsed] = useState(true);
  // const [initialNamePill, setInitialNamePill] = useState('Choose a node:');

  const handleButtonClick = () => {
    setIsCollapsed(!isCollapsed);
  };

  const handleOptionClick = (option: string) => {
    setIsCollapsed(true);
    onSelectOption(option);
  };

  return (
    <DropdownContainer placement="bottom">
      <DropdownTrigger title={selectedNode || 'Choose a node:'} size="sm">
        <EntityPill
          className="cursor-pointer"
          title={
            <div className="flex flex-row items-center justify-between pointer-events-none">
              <span>{selectedNode || 'Choose a node:'}</span>
              <Button
                variantType="secondary"
                variant="ghost"
                size="xs"
                iconComponent="icon-[ic--baseline-arrow-drop-down]"
                onClick={handleButtonClick}
              />
            </div>
          }
        />
      </DropdownTrigger>
      <DropdownItemContainer>
        {dropdownNodes
          .map((node, index) => (
            <DropdownItem
              className="my-0 cursor-pointer"
              selected={selectedNode === node}
              onClick={() => handleOptionClick(node)}
              key={'entity_' + index + '-' + node}
              value={node}
            >
              <EntityPill title={node} />
            </DropdownItem>
          ))
          .filter(node => node.props.value !== selectedNode)}
      </DropdownItemContainer>
    </DropdownContainer>
  );
}
