import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { EntityPillSelector } from './entityPillSelector';

const metaPillAttributes: Meta<typeof EntityPillSelector> = {
  component: EntityPillSelector,
  title: 'Components/Selectors/Entity',
  decorators: [story => <div className="flex items-center justify-center m-11 p-11">{story()}</div>],
};

export default metaPillAttributes;

type Story = StoryObj<typeof EntityPillSelector>;

export const entity: Story = {
  args: {
    dropdownNodes: ['kamerleden', 'commissies'],
  },
};
