import React, { useState } from 'react';
import { DropdownTrigger, DropdownContainer, DropdownItemContainer } from '@/lib/components/dropdowns';
import { Icon } from '@/lib/components/icon';

type DropdownTextAndIconProps = {
  value: any;
  onChange: (val: any) => void;
  options: { name: string; type: string }[];
};

const getIconForType = (type: string) => {
  switch (type) {
    case 'numerical':
      return <Icon component="icon-[carbon--string-integer]" size={24} color="hsl(var(--clr-sec--700))" />;
    case 'categorical':
      return <Icon component="icon-[carbon--string-text]" size={24} color="hsl(var(--clr-sec--700))" />;
    case 'temporal':
      return <Icon component="icon-[carbon--calendar]" size={24} color="hsl(var(--clr-sec--700))" />;
    case 'spatial':
      return <Icon component="icon-[carbon--undefined]" size={24} color="hsl(var(--clr-sec--700))" />;
    default:
      return <Icon component="icon-[carbon--undefined]" size={24} color="hsl(var(--clr-sec--700))" />;
  }
};

export const DropdownTextAndIcon = ({ value, onChange, options }: DropdownTextAndIconProps) => {
  const [menuOpen, setMenuOpen] = useState<boolean>(false);

  const handleOptionClick = (option: { name: string; type: string }) => {
    onChange(option);
    setMenuOpen(false);
  };

  const renderOption = (option: { name: string; type: string }, isSelected = false) => (
    <li
      key={option.name}
      onClick={() => handleOptionClick(option)}
      className={`cursor-pointer flex items-center ml-2 h-8 m-2 hover:bg-gray-200 text-sm gap-2 ${isSelected ? 'bg-gray-100' : ''}`}
    >
      <span className="ml-2">{option.name}</span>
      {getIconForType(option.type)}
    </li>
  );

  const currentOption = value || options.find(option => option.name === value?.name) || null;

  return (
    <div className="w-full h-6 relative">
      <DropdownContainer open={menuOpen}>
        <DropdownTrigger
          size="sm"
          aria-expanded={menuOpen}
          aria-haspopup="listbox"
          title={
            currentOption ? (
              <div className="flex items-center gap-2">
                <span>{currentOption.name}</span>
                {getIconForType(currentOption.type)}
              </div>
            ) : (
              <div className="flex items-center gap-2">
                <span>Select an attribute</span>
              </div>
            )
          }
          onClick={() => setMenuOpen(!menuOpen)}
        />
        {menuOpen && (
          <DropdownItemContainer className="absolute w-60 bg-white shadow-lg z-10">
            <ul role="listbox" aria-activedescendant={currentOption ? currentOption.name : undefined}>
              {options.map(option => renderOption(option, option.name === currentOption?.name))}
            </ul>
          </DropdownItemContainer>
        )}
      </DropdownContainer>
    </div>
  );
};
