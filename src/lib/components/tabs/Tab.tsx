import React, { forwardRef } from 'react';
import { ButtonProps } from '@/lib/components/buttons';

type TabTypes = 'inline' | 'rounded' | 'simple';

type ContextType = {
  tabType: TabTypes;
};
const TabContext = React.createContext<ContextType>({
  tabType: 'inline',
});

export const Tabs = forwardRef<
  HTMLDivElement,
  {
    children: React.ReactNode;
    tabType?: TabTypes;
    className?: string;
  }
>((props, ref) => {
  const { children, tabType = 'inline', className = '' } = props;
  let baseClass = '';
  if (tabType === 'inline') {
    baseClass = 'flex items-stretch';
  } else if (tabType === 'rounded') {
    baseClass = 'flex gap-x-1 relative before:w-full before:h-px before:absolute before:bottom-0 before:bg-secondary-200 overflow-hidden';
  } else if (tabType === 'simple') {
    baseClass = 'flex';
  }
  const combinedClass = `${baseClass} ${className}`.trim();
  return (
    <TabContext.Provider value={{ tabType: tabType }}>
      <div ref={ref} className={combinedClass}>
        {children}
      </div>
    </TabContext.Provider>
  );
});

Tabs.displayName = 'Tabs';

export const Tab = ({
  activeTab,
  text,
  IconComponent,
  className = '',
  ...props
}: ButtonProps & {
  activeTab: boolean;
  children?: React.ReactNode;
  text: string;
  IconComponent?: React.FC<React.SVGProps<SVGSVGElement>>;
}) => {
  const context = React.useContext(TabContext);

  if (context.tabType === 'inline') {
    className += ` px-2 gap-1 relative h-full max-w-64 flex-nowrap before:content-['']
    before:absolute before:left-0 before:bottom-0 before:h-[2px] before:w-full
    ${
      activeTab
        ? 'before:bg-primary-500 text-dark'
        : ' text-secondary-600 hover:text-dark hover:bg-secondary-200 hover:bg-opacity-50 before:bg-transparent hover:before:bg-secondary-300'
    }`;
  } else if (context.tabType === 'rounded') {
    className += ` py-1.5 px-3 -mb-px text-sm flex-nowrap text-center border border-secondary-200 rounded-t
    ${activeTab ? 'active z-[2] text-dark bg-light' : 'bg-secondary-100 hover:text-dark border-secondary-100 text-secondary-600'}`;
  } else if (context.tabType === 'simple') {
    className += ` px-2 py-1 gap-1 ${activeTab ? 'active text-dark' : 'text-secondary-500 hover:text-dark'}`;
  }

  return (
    <div
      className={` cursor-pointer relative text-xs font-medium flex items-center justify-start whitespace-nowrap ${className}`}
      {...props}
      tabIndex={0}
      data-type="tab"
    >
      {IconComponent && <IconComponent className="h-4 w-4 shrink-0 pointer-events-none" />}
      {text && <span className="truncate select-none">{text}</span>}
      {props.children}
    </div>
  );
};
