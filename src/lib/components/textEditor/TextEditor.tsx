import { useEffect, useRef, useCallback } from 'react';
import { LexicalComposer } from '@lexical/react/LexicalComposer';
import { RichTextPlugin } from '@lexical/react/LexicalRichTextPlugin';
import { ContentEditable } from '@lexical/react/LexicalContentEditable';
import { LexicalErrorBoundary } from '@lexical/react/LexicalErrorBoundary';
import { SerializedEditorState } from 'lexical';
import { ToolbarPlugin } from './plugins/ToolbarPlugin';
import { PreviewPlugin } from './plugins/PreviewPlugin';
import { InsertVariablesPlugin } from './plugins/InsertVariablesPlugin';
import { ErrorHandler } from './ErrorHandler';
import { Placeholder } from './Placeholder';
import { VariableNode } from './VariableNode';
import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import { EditorButtonPlugin } from './plugins/SaveButtonPlugin';

type TextEditorProps = {
  children: React.ReactNode;
  editorState: SerializedEditorState | null;
  setEditorState: (value: SerializedEditorState) => void;
  showToolbar: boolean;
  placeholder?: string;
  handleSave: (elements: SerializedEditorState, generateEmail: boolean) => void;
  saveDisabled: boolean;
  variableOptions: string[];
};

function InitialStateLoader({ editorState }: { editorState: SerializedEditorState | null }) {
  const [editor] = useLexicalComposerContext();
  const previousEditorStateRef = useRef<string | null>(null);

  useEffect(() => {
    if (!editor || !editorState) return;
    queueMicrotask(() => {
      editor.setEditorState(editor.parseEditorState(editorState));
    });
  }, [editor, editorState]);

  return null;
}

export function TextEditor({
  editorState,
  showToolbar,
  placeholder,
  handleSave,
  saveDisabled,
  children,
  variableOptions,
}: TextEditorProps) {
  const contentEditableRef = useRef<HTMLDivElement>(null);
  const updateTimeoutRef = useRef<NodeJS.Timeout | null>(null);
  const isUpdatingRef = useRef(false);

  useEffect(() => {
    return () => {
      if (updateTimeoutRef.current) {
        clearTimeout(updateTimeoutRef.current);
      }
    };
  }, []);

  const initialConfig = {
    namespace: 'MyEditor',
    onError: ErrorHandler,
    nodes: [VariableNode],
  };

  return (
    <LexicalComposer initialConfig={initialConfig}>
      <div className="editor-toolbar flex items-center bg-secondary-50 rounded mt-4 space-x-2">
        <PreviewPlugin contentEditable={contentEditableRef} />
        {showToolbar && <ToolbarPlugin />}
      </div>
      <div className="border p-2">
        <div className="editor relative">
          <RichTextPlugin
            contentEditable={<ContentEditable className="border p-3 min-h-24 rounded" ref={contentEditableRef} />}
            placeholder={<Placeholder placeholder={placeholder} />}
            ErrorBoundary={LexicalErrorBoundary}
          />
        </div>
        <div className="preview min-h-24 p-3 hidden"></div>
      </div>
      <InsertVariablesPlugin variableOptions={variableOptions} />
      <InitialStateLoader editorState={editorState} />

      <div className="flex flex-row mt-3 justify-between">
        <EditorButtonPlugin
          label="Send Email"
          variant="outline"
          variantType="primary"
          className="px-6"
          onChange={e => handleSave(e, true)}
          disabled={saveDisabled}
        />
        <div className="flex justify-end">
          {children}
          <EditorButtonPlugin label="Save" variantType="primary" onChange={e => handleSave(e, false)} disabled={saveDisabled} />
        </div>
      </div>
    </LexicalComposer>
  );
}
