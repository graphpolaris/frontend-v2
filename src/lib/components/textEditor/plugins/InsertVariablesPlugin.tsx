import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import { $getSelection, type BaseSelection } from 'lexical';
import { Input } from '@/lib/components/inputs';
import { VariableNode, VariableType } from '../VariableNode';
import { useGraphQueryResult, useVisualization } from '@/lib/data-access';

interface InsertVariablesPluginProps {
  variableOptions: string[];
}

export const InsertVariablesPlugin = ({ variableOptions }: InsertVariablesPluginProps) => {
  const [editor] = useLexicalComposerContext();
  const { openVisualizationArray } = useVisualization();

  const visualizationOptions = openVisualizationArray.filter(x => x.id == 'Vis1D').map(x => x.name);

  const onChange = (value: string | number, type: VariableType) => {
    editor.update(() => {
      const editorElement = document.querySelector('.editor > div') as HTMLDivElement;
      editorElement?.focus();
      const selection = $getSelection() as BaseSelection;
      const node = new VariableNode(String(value), type);
      selection.insertNodes([node]);

      // TODO: enable drag and dropping nodes
    });
  };

  const result = useGraphQueryResult();

  //const nodeTypes = Object.keys(result.metaData?.nodes.types || {});
  function optionsForType(type: string) {
    const typeObj = result.metaData?.nodes.types[type] ?? { attributes: null };

    if (!('attributes' in typeObj) || typeObj.attributes == null) {
      return [];
    }

    return Object.entries(typeObj.attributes)
      .map(([k, v]) => Object.keys(v.statistics).map(x => `${k} • ${x}`))
      .flat();
  }

  return (
    <>
      {variableOptions.map(nodeType => (
        <Input
          key={nodeType}
          type="dropdown"
          label={`${nodeType} variable`}
          value=""
          className="insert-variable"
          options={optionsForType(nodeType)}
          onChange={v => onChange(`${nodeType} • ${v}`, VariableType.statistic)}
        />
      ))}
      {visualizationOptions.length > 0 ? (
        <Input
          type="dropdown"
          label={`Visualization`}
          value=""
          className="insert-variable"
          options={visualizationOptions}
          onChange={v => v !== undefined && onChange(v, VariableType.visualization)}
        />
      ) : (
        ''
      )}
    </>
  );
};
