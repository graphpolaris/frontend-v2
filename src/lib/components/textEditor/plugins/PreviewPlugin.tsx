import { JSX, RefObject } from 'react';
import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import { $generateHtmlFromNodes } from '@lexical/html';
import { VariableType } from '../VariableNode';
import { useVisualization, useGraphQueryResult } from '@/lib/data-access';
import { VisualizationSettingsType } from '@/lib/vis/common';

// @ts-expect-error missing import
import { newPlot, toImage } from 'plotly.js/dist/plotly';
import { Vis1DProps, getAttributeValues } from '@/lib/vis/visualizations/vis1D/model';
import { preparePlotData } from '@/lib/vis/visualizations/vis1D/components/MakePlot';

export function PreviewPlugin({ contentEditable }: { contentEditable: RefObject<HTMLDivElement | null> }): JSX.Element {
  const [editor] = useLexicalComposerContext();

  function updatePreview() {
    editor.read(async () => {
      const preview = document.querySelector('.editor + .preview');
      if (preview == null) return;

      const html = $generateHtmlFromNodes(editor as any); // any needed to avoid excessive ts error
      preview.innerHTML = await populateTemplate(html);
    });
  }

  const result = useGraphQueryResult();

  /*
  const getAttributeValues = useCallback(
    (settings: Vis1DProps & VisualizationSettingsType, attributeKey: string | number) => {
      if (!settings.nodeLabel || !attributeKey) {
        return [];
      }

      return result.nodes
        .filter((item) => item.label === settings.nodeLabel && item.attributes && attributeKey in item.attributes)
        .map((item) => item.attributes[attributeKey] as string | number);
    },
    [result],
  );
  */
  const vis = useVisualization();

  async function replaceAllAsync(string: string, regexp: RegExp, replacerFunction: CallableFunction) {
    const replacements = await Promise.all(Array.from(string.matchAll(regexp), match => replacerFunction(...match)));
    let i = 0;
    return string.replace(regexp, () => replacements[i++]);
  }

  async function populateTemplate(html: string) {
    const regex = / *?{\{ *?(\w*?):([\w •]*?) *?\}\} *?/gm;

    return replaceAllAsync(html, regex, async (_: string, _type: string, name: string) => {
      const type = VariableType[_type as keyof typeof VariableType];

      switch (type) {
        case VariableType.statistic: {
          const [nodeType, feature, statistic] = name.split(' • ');
          const node = result.metaData?.nodes.types[nodeType];
          const attribute = node?.attributes[feature].statistics as any;
          if (attribute == null) return '';
          const value = attribute[statistic];
          return ` ${value} `;
        }

        case VariableType.visualization: {
          const activeVisualization = vis.openVisualizationArray.find(x => x.name == name) as Vis1DProps & VisualizationSettingsType;

          if (!activeVisualization) {
            throw new Error('Tried to render non-existing visualization');
          }
          const xAxisData = getAttributeValues(result, activeVisualization.selectedEntity, activeVisualization.xAxisLabel!);
          let yAxisData: (string | number)[] = [];
          let zAxisData: (string | number)[] = [];

          if (activeVisualization.yAxisLabel != null) {
            if (activeVisualization.plotType === 'histogram') {
              yAxisData = getAttributeValues(
                result,
                activeVisualization.selectedEntity,
                activeVisualization.xAxisLabel, // Note: x not y!
                activeVisualization.groupData,
              );
            } else {
              yAxisData = getAttributeValues(result, activeVisualization.selectedEntity, activeVisualization.yAxisLabel);
            }
          }

          if (activeVisualization.zAxisLabel != null) {
            zAxisData = getAttributeValues(result, activeVisualization.selectedEntity, activeVisualization.zAxisLabel);
          }

          const groupBy = activeVisualization.groupData;
          const stack = activeVisualization.stack;
          const showAxis = true;

          const xAxisLabel = '';
          const yAxisLabel = '';
          const zAxisLabel = '';

          const plotType = activeVisualization.plotType;

          const { plotData, layout } = preparePlotData({
            xAxisData,
            plotType,
            yAxisData,
            zAxisData,
            xAxisLabel,
            yAxisLabel,
            zAxisLabel,
            showAxis,
            groupBy,
            stack,
          });

          const layout2 = {
            ...layout,
            width: 600,
            height: 400,
            title: activeVisualization.title,
          };

          // Generate the plot
          const plot = await newPlot(document.createElement('div'), plotData, layout2);

          const dataURI = await toImage(plot);
          return `<img src="${dataURI}" width="300" height="200" alt="${activeVisualization.title}" />`;
        }
      }
    });
  }

  const activeClasses = ['text-blue-600', 'bg-light', 'active'];

  function resetStyling(event: React.MouseEvent) {
    document.querySelectorAll('.toolbar-preview li button').forEach(btn => btn.classList.remove('text-blue-600', 'bg-light'));
    const button = event.target as HTMLButtonElement;
    button.classList.add(...activeClasses);
  }

  function showPreview(event: React.MouseEvent) {
    resetStyling(event);

    const editor = contentEditable.current?.parentElement;
    if (editor == null) return;

    editor.classList.add('hidden');
    document.querySelectorAll('.editor-toolbar > div:not(.toolbar-preview)').forEach(el => el.classList.add('hidden'));
    document.querySelectorAll('.insert-variable').forEach(el => el.classList.add('hidden'));
    const preview = editor.parentElement?.querySelector('.editor + .preview');
    preview?.classList.remove('hidden');

    updatePreview();
  }

  function showEditor(event: React.MouseEvent) {
    resetStyling(event);

    const button = event.target as HTMLButtonElement;
    button.classList.add(...activeClasses);

    const editor = contentEditable.current?.parentElement;
    if (editor == null) return;

    editor.classList.remove('hidden');
    document.querySelectorAll('.editor-toolbar > div:not(.toolbar-preview)').forEach(el => el.classList.remove('hidden'));
    document.querySelectorAll('.insert-variable').forEach(el => el.classList.remove('hidden'));

    editor?.querySelector('div')?.focus();

    const preview = editor.parentElement?.querySelector('.editor + .preview');
    if (preview == null) return;
    preview.classList.add('hidden');
  }

  return (
    <ul
      className="toolbar-preview flex flex-wrap text-sm font-medium text-center text-gray-500 dark:text-gray-400 z-10"
      style={{ transform: 'translate(0px, 2.5px)' }}
    >
      <li className="me-1">
        <button
          onClick={showEditor}
          aria-current="page"
          className="inline-block px-3 py-2 text-blue-600 bg-light border border-b-light border-light-200 rounded-t-lg active"
        >
          Write
        </button>
      </li>
      <li>
        <button onClick={showPreview} className="inline-block px-3 py-2 border border-b-0 border-light-200 rounded-t-lg">
          Preview
        </button>
      </li>
    </ul>
  );
}
