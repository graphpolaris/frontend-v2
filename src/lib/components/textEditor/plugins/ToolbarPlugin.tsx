import React from 'react';
import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import { FORMAT_TEXT_COMMAND } from 'lexical';
import { Button } from '../../buttons';

export function ToolbarPlugin() {
  const [editor] = useLexicalComposerContext();

  const formatBold = () => {
    editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'bold');
  };

  const formatItalic = () => {
    editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'italic');
  };

  const formatUnderline = () => {
    editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'underline');
  };

  return [
    <div key="spacer" style={{ flex: '1 1 auto' }}></div>,
    <Button
      key="bold"
      className="my-2"
      variantType="secondary"
      variant="ghost"
      size="xs"
      iconComponent="icon-[ic--baseline-format-bold]"
      onClick={formatBold}
    />,
    <Button
      key="italic"
      className="my-2"
      variantType="secondary"
      variant="ghost"
      size="xs"
      iconComponent="icon-[ic--baseline-format-italic]"
      onClick={formatItalic}
    />,
    <Button
      key="underline"
      className="my-2 me-2"
      variantType="secondary"
      variant="ghost"
      size="xs"
      iconComponent="icon-[ic--baseline-format-underlined]"
      onClick={formatUnderline}
    />,
  ];
}
