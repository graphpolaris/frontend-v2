import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import { Button, ButtonProps } from '../../buttons';
import { SerializedEditorState } from 'lexical';

export function EditorButtonPlugin(
  props: {
    onChange: (elements: SerializedEditorState) => void;
  } & Exclude<ButtonProps, 'onClick'>,
) {
  const [editor] = useLexicalComposerContext();

  function handleSave() {
    editor.read(() => {
      const data = editor.getEditorState();
      const jsonData = data.toJSON();

      props.onChange(jsonData);
    });
  }

  return <Button className="ml-2" onClick={handleSave} {...props} />;
}
