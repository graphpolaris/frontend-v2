import type { NodeKey, LexicalEditor, DOMExportOutput, SerializedLexicalNode, Spread } from 'lexical';
import { DecoratorNode, EditorConfig } from 'lexical';

export enum VariableType {
  statistic = 'statistic',
  visualization = 'visualization',
}

export type SerializedVariableNode = Spread<
  {
    name: string;
    variableType: VariableType;
  },
  SerializedLexicalNode
>;

export class VariableNode extends DecoratorNode<JSX.Element> {
  __name: string;
  __variableType: VariableType;

  static getType(): string {
    return 'variable-node';
  }

  static clone(node: VariableNode): VariableNode {
    return new VariableNode(node.__name, node.__variableType, node.__key);
  }

  constructor(name: string, type: VariableType, key?: NodeKey) {
    super(key);
    this.__name = name;
    this.__variableType = type;
  }

  setName(name: string) {
    const self = this.getWritable();
    this.__name = name;
  }

  getName(): string {
    const self = this.getLatest();
    return self.__name;
  }

  getTextContent(): string {
    const self = this.getLatest();
    return `{{ ${self.__variableType}:${self.__name} }}`;
  }

  // Import and export

  exportJSON(): SerializedVariableNode {
    return {
      type: this.getType(),
      variableType: this.__variableType,
      name: this.__name,
      version: 1,
    };
  }

  static importJSON(jsonNode: SerializedVariableNode): VariableNode {
    const node = new VariableNode(jsonNode.name, jsonNode.variableType);
    return node;
  }

  // View

  createDOM(config: EditorConfig): HTMLElement {
    const span = document.createElement('span');
    const theme = config.theme;
    const className = theme.image;
    if (className !== undefined) {
      span.className = `${className}`;
    }
    return span;
  }

  updateDOM(): false {
    return false;
  }

  exportDOM(editor: LexicalEditor): DOMExportOutput {
    const self = this.getLatest();

    return {
      element: new Text(self.getTextContent()),
    };
  }

  decorate(): JSX.Element {
    return <div className="variable-node">{this.getName()}</div>;
  }
}
