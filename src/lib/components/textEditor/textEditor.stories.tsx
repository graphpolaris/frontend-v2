import { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { TextEditor } from '.';
import { EditorState } from 'lexical';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { graphQueryResultSlice, visualizationSlice } from '../../../lib/data-access/store';

const Mockstore = configureStore({
  reducer: {
    graphQueryResult: graphQueryResultSlice.reducer,
    visualize: visualizationSlice.reducer,
  },
});

const Component: Meta<typeof TextEditor> = {
  title: 'Components/TextEditor',
  component: TextEditor,
  decorators: [
    Story => (
      <Provider store={Mockstore}>
        <div className="w-52 m-5">{Story()}</div>
      </Provider>
    ),
  ],
};

export default Component;
type Story = StoryObj<typeof Component>;

export const TextEditorStory: Story = (args: any) => {
  const [editorState, setEditorState] = useState<EditorState | undefined>(undefined);

  return (
    <TextEditor
      {...args}
      editorState={editorState}
      setEditorState={setEditorState}
      showToolbar={true}
      placeholder="Start typing your report template..."
    />
  );
};

TextEditorStory.args = {};
