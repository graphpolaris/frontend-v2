import React from 'react';

export function Placeholder({ placeholder }: { placeholder?: string }) {
  return placeholder && <div className="absolute inset-0 pointer-events-none flex p-3 text-secondary-400">{placeholder}</div>;
}
