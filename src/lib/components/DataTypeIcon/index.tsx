import React from 'react';
import { SchemaAttributeTypes } from '../../schema';

const IconMap: Record<SchemaAttributeTypes, string> = {
  float: 'icon-[carbon--string-integer]',
  string: 'icon-[carbon--string-text]',
  int: 'icon-[carbon--string-integer]',
  boolean: 'icon-[carbon--boolean]',
  bool: 'icon-[carbon--boolean]',
  date: 'icon-[carbon--calendar]',
  time: 'icon-[carbon--calendar]',
  datetime: 'icon-[carbon--calendar]',
  duration: 'icon-[carbon--calendar]',
  number: 'icon-[carbon--string-integer]',
  location: 'icon-[carbon--location]',
  array: 'icon-[ic--baseline-data-array]',
};

function getDataTypeIcon(data_type?: SchemaAttributeTypes): string {
  return data_type && IconMap[data_type] ? IconMap[data_type] : 'icon-[carbon--undefined]';
}

export { getDataTypeIcon };
