import React from 'react';
import { Icon } from './icon';

type Props = {
  title?: string;
  screenPosition?: string;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  children: React.ReactNode;
};

const DEFAULT_TITLE = 'Legend';
const DEFAULT_SCREEN_POSITION = 'top-2 right-2';

export const Legend = ({ title = DEFAULT_TITLE, screenPosition = DEFAULT_SCREEN_POSITION, open, setOpen, children }: Props) => {
  const [expand, setExpand] = React.useState<boolean>(true);

  return (
    open && (
      <div className={`absolute ${screenPosition} z-20 bg-light w-60`}>
        <div className="px-4 py-3 flex justify-between align-middle">
          <h4 className="font-bold">{title}</h4>
          <div>
            <button className="btn btn-outline btn-xs" onClick={() => setExpand(!expand)}>
              {expand ? (
                <Icon component="icon-[ic--baseline-expand-less]" size={20} />
              ) : (
                <Icon component="icon-[ic--baseline-expand-more]" size={20} />
              )}
            </button>
            <button className="btn btn-primary btn-xs ml-1" onClick={() => setOpen(false)}>
              <Icon component="icon-[ic--baseline-close]" size={20} />
            </button>
          </div>
        </div>
        {expand && <div className="px-4 pb-3">{children}</div>}
      </div>
    )
  );
};
