// Panel.stories.tsx

import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { Panel } from './Panel';

const metaPanel: Meta<typeof Panel> = {
  component: Panel,
  title: 'Components/Layout/Panel',
};

export default metaPanel;

type Story = StoryObj<typeof Panel>;

export const mainStory: Story = {
  render: args => (
    <Panel {...args}>
      <p>This is the content inside the panel.</p>
    </Panel>
  ),
  args: {
    title: 'Panel Title',
    tooltips: <span>Some tooltip</span>,
  },
};
