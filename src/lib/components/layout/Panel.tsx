import React from 'react';
import { ControlContainer } from '..';

export type Panel = {
  title: string | React.ReactNode;
  tooltips?: React.ReactNode;
  children: React.ReactNode;
  className?: string;
};

export function Panel(props: Panel) {
  return (
    <div className={`flex flex-col border w-full h-full bg-light ${props.className || ''}`}>
      <div className="sticky shrink-0 top-0 flex items-stretch justify-between h-7 bg-secondary-100 border-b border-secondary-200 max-w-full">
        <div className="flex items-center">
          <h1 className="text-xs font-semibold text-secondary-600 px-2 truncate">{props.title}</h1>
        </div>
        {props.tooltips && (
          <div className="shrink-0 sticky right-0 px-0.5 ml-auto items-center flex">
            <ControlContainer>{props.tooltips}</ControlContainer>
          </div>
        )}
      </div>
      {props.children}
    </div>
  );
}

export default Panel;
