import React, { useEffect, useRef, useState } from 'react';

type Props = {
  children: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
  horizontal: boolean;
  divisorSize: number;
  defaultProportion?: number;
  classNameLeft?: string;
  classNameRight?: string;
  maxProportion?: number;
};

function convertRemToPixels(rem: number) {
  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

export const Resizable = ({
  children,
  className,
  style,
  horizontal,
  divisorSize,
  defaultProportion,
  classNameLeft,
  classNameRight,
  maxProportion,
  ...props
}: Props) => {
  const ref = useRef<HTMLDivElement>(null);
  const children2 = children as React.ReactElement[];
  const [firstSize, setFirstSize] = React.useState<number>(0);
  const [secondSize, setSecondSize] = React.useState<number>(0);
  const [dragging, setDragging] = React.useState<boolean>(false);
  const [windowSize, setWindowSize] = useState({ width: window.innerWidth, height: window.innerHeight });

  useEffect(() => {
    const handleResize = () => {
      setWindowSize({ width: window.innerWidth, height: window.innerHeight });
      setTimeout(() => {
        // Sometimes the window size is not updated immediately to the proper amount, so retry after 100ms
        // setWindowSize({ width: window.innerWidth, height: window.innerHeight });
      }, 100);
    };

    window.addEventListener('resize', handleResize);
    if (ref.current) new ResizeObserver(handleResize).observe(ref.current);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  // Store the current proportion of the first area
  const [currentProportion, setCurrentProportion] = useState(defaultProportion || 0.5);

  useEffect(() => {
    if (ref.current) {
      const rect = ref.current.getBoundingClientRect();
      if (horizontal) {
        const newFirstSize = (rect.width - divisorSize) * currentProportion;
        const newSecondSize = (rect.width - divisorSize) * (1 - currentProportion);
        setFirstSize(newFirstSize);
        setSecondSize(newSecondSize);
      } else {
        const newFirstSize = (rect.height - divisorSize) * currentProportion;
        const newSecondSize = (rect.height - divisorSize) * (1 - currentProportion);
        setFirstSize(newFirstSize);
        setSecondSize(newSecondSize);
      }
    }
  }, [ref.current, windowSize]);

  function onMouseDown(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    setDragging(true);
    window.addEventListener('mouseup', onMouseUp);
  }
  const onMouseUp = () => {
    setDragging(false);
    window.removeEventListener('mouseup', onMouseUp);
  };

  function onMouseMove(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    if (dragging) {
      if (ref.current) {
        const rect = ref.current.getBoundingClientRect();
        const relativeX = e.clientX - rect.left;
        const relativeY = e.clientY - rect.top;
        const minSizeX = 16;
        const minSizeY = 28;

        if (horizontal) {
          const newFirstSize = Math.max(minSizeX, relativeX);
          if (maxProportion && newFirstSize / rect.width > maxProportion) return;
          setFirstSize(newFirstSize);
          setSecondSize(Math.max(minSizeX, rect.width - relativeX));
          setCurrentProportion(newFirstSize / rect.width);
        } else {
          const newFirstSize = Math.max(minSizeY, relativeY);
          if (maxProportion && newFirstSize / rect.height > maxProportion) return;
          setFirstSize(newFirstSize);
          setSecondSize(Math.max(minSizeY, rect.height - relativeY));
          setCurrentProportion(newFirstSize / rect.height);
        }
      }
    }
  }
  function onTouchStart(e: React.TouchEvent<HTMLDivElement>) {
    setDragging(true);
  }
  function onTouchMove(e: React.TouchEvent<HTMLDivElement>) {}
  function onTouchEnd(e: React.TouchEvent<HTMLDivElement>) {
    setDragging(false);
  }
  function onTouchCancel(e: React.TouchEvent<HTMLDivElement>) {
    setDragging(false);
  }

  if (!children2[0]) return children2[1];
  if (!children2[1]) return children2[0];

  return (
    <>
      {dragging && <div className="absolute top-0 left-0 w-screen h-screen z-10 cursor-grabbing" onMouseMove={onMouseMove}></div>}
      <div
        className={`w-full h-full flex ${horizontal ? 'flex-row' : 'flex-col'} ${className !== undefined ? className : ''} `}
        style={style}
        {...props}
        ref={ref}
      >
        {firstSize > 0 && (
          <>
            <div
              className={'h-full w-full flex-grow ' + (classNameLeft !== undefined ? classNameLeft : '')}
              style={horizontal ? { maxWidth: firstSize } : { maxHeight: firstSize }}
            >
              {children2[0]}
            </div>
            <div
              className={
                'hover:bg-primary-200 ' + (horizontal ? 'cursor-col-resize' : 'cursor-row-resize') + (dragging ? ' bg-primary-300' : '')
              }
              style={horizontal ? { minWidth: divisorSize } : { minHeight: divisorSize }}
              onMouseDown={onMouseDown}
              onTouchStart={onTouchStart}
              onTouchMove={onTouchMove}
              onTouchEnd={onTouchEnd}
              onTouchCancel={onTouchCancel}
            ></div>
            <div
              className={'h-full w-full flex-grow ' + (classNameRight !== undefined ? classNameRight : '')}
              style={horizontal ? { maxWidth: secondSize } : { maxHeight: secondSize }}
            >
              {children2[1]}
            </div>
          </>
        )}
      </div>
    </>
  );
};
