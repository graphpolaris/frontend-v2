// Dialog.stories.tsx
/* eslint-disable react-hooks/rules-of-hooks */

import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { Dialog, DialogTrigger, DialogContent, DialogHeading, DialogDescription, DialogClose } from './Dialog';

const metaDialog: Meta<typeof Dialog> = {
  component: Dialog,
  title: 'Components/Layout/Dialog',
};

export default metaDialog;

type Story = StoryObj<typeof Dialog>;

export const mainStory: Story = {
  render: args => {
    const [isOpen, setIsOpen] = useState(false);

    const handleToggle = () => setIsOpen(!isOpen);

    return (
      <Dialog {...args} open={isOpen} onOpenChange={setIsOpen}>
        <DialogTrigger asChild>
          <button onClick={handleToggle} className="px-4 py-2 bg-secondary-200 text-white rounded">
            Open Dialog
          </button>
        </DialogTrigger>
        <DialogContent>
          <DialogHeading>Dialog Title</DialogHeading>
          <DialogDescription>This is a description inside the dialog.</DialogDescription>
          <DialogClose>Close</DialogClose>
        </DialogContent>
      </Dialog>
    );
  },
  args: {
    initialOpen: false,
    onOpenChange: (open: boolean) => console.log(`Dialog is ${open ? 'open' : 'closed'}`),
  },
};
