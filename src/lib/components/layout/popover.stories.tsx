// Popover.stories.tsx
/* eslint-disable react-hooks/rules-of-hooks */

import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { Popover, PopoverTrigger, PopoverContent, PopoverHeading, PopoverDescription, PopoverClose } from './Popover';
import { Icon } from '../icon';
const metaPopover: Meta<typeof Popover> = {
  component: Popover,
  title: 'Components/Layout/Popover',
};

export default metaPopover;

type Story = StoryObj<typeof Popover>;

export const mainStory: Story = {
  render: args => {
    const [isOpen, setIsOpen] = useState(false);
    const size = 'md';
    const variant = 'primary';
    const className = 'primary';

    const handleToggle = () => setIsOpen(!isOpen);
    const paddingClass = 'px-2 py-1';
    const textSizeClass = 'text-base';
    const disabled = true;
    const variantClass =
      variant === 'primary' || !variant
        ? 'border bg-light rounded'
        : variant === 'ghost'
          ? 'bg-transparent shadow-none'
          : 'border rounded bg-transparent';
    return (
      <Popover {...args} open={isOpen} onOpenChange={setIsOpen}>
        <PopoverTrigger asChild>
          <div
            className={`inline-flex w-full truncate justify-between items-center gap-x-1.5 ${variantClass} ${textSizeClass} ${paddingClass} text-secondary-900 shadow-sm hover:bg-secondary-50 disabled:bg-secondary-100 disabled:cursor-not-allowed disabled:text-secondary-400 pl-1 truncate cursor-pointer ${disabled ? 'opacity-50 cursor-not-allowed' : ''} ${className || ''}`}
          >
            <span className={`text-${size}`}>{'Popover trigger'}</span>
            <Icon component="icon-[ic--baseline-arrow-drop-down]" size={16} />
          </div>
        </PopoverTrigger>
        <PopoverContent className="p-4 bg-white border rounded shadow-lg">
          <PopoverHeading className="text-lg font-bold">Popover Title</PopoverHeading>
          <PopoverDescription className="mt-2">This is a description inside the popover.</PopoverDescription>
          <PopoverClose className="mt-4 bg-red-500 text-white p-2 rounded">Close</PopoverClose>
        </PopoverContent>
      </Popover>
    );
  },
  args: {
    placement: 'bottom',
    modal: false,
    onOpenChange: (open: boolean) => console.log(`Popover is ${open ? 'open' : 'closed'}`),
  },
};
