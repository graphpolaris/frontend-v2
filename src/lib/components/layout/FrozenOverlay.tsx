export const FrozenOverlay = (props: { children: React.ReactNode; enabled?: boolean }) => {
  if (!props.enabled || !props.children || props.children === '') return null;

  return (
    <div className="absolute h-screen w-screen left-0 top-0 flex justify-center items-center blur-sm pointer-events-none z-50">
      <div className="text-3xl font-bold">{props.children}</div>
    </div>
  );
};
