import React, { useState, useRef, ReactNode } from 'react';
import { Icon } from '../icon';
import { PopoverContent, PopoverTrigger, Popover, PopoverOptions } from '@/lib/components/popover';

export const DropdownContainer = ({ children, ...props }: { children: React.ReactNode } & PopoverOptions) => {
  return (
    <Popover placement="bottom-start" {...props}>
      {children}
    </Popover>
  );
};

type DropdownTriggerProps = {
  title?: string | ReactNode;
  size?: 'xs' | 'sm' | 'md' | 'xl';
  disabled?: boolean;
  variant?: 'primary' | 'ghost' | 'outline';
  className?: string;
  popover?: boolean;
  onClick?: () => void;
  children?: ReactNode;
  noDropdownArrow?: boolean;
};

export function DropdownTrigger({
  title,
  size,
  disabled,
  variant,
  className,
  onClick,
  noDropdownArrow = false,
  popover = true,
  children = undefined,
}: DropdownTriggerProps) {
  const paddingClass = size === 'xs' ? 'py-0' : size === 'sm' ? 'px-1 py-1' : size === 'md' ? 'px-2 py-1' : 'px-4 py-2';
  const textSizeClass = size === 'xs' ? 'text-xs' : size === 'sm' ? 'text-sm' : size === 'md' ? 'text-base' : 'text-lg';

  const disabledClass = disabled ? 'cursor-not-allowed' : 'cursor-pointer';
  const variantClass =
    variant === 'primary' || !variant
      ? 'border bg-light rounded'
      : variant === 'ghost'
        ? 'bg-transparent shadow-none'
        : 'border rounded bg-transparent';
  const inner = children ? (
    React.cloneElement(children as React.ReactElement<any>, {
      disabled: disabled,
    })
  ) : (
    <div
      className={`inline-flex w-full truncate justify-between items-center gap-x-1.5 ${variantClass} ${textSizeClass} ${paddingClass} text-secondary-900 shadow-sm ${
        noDropdownArrow ? `pointer-events-none cursor-default` : ''
      } ${disabled ? ` cursor-not-allowed text-secondary-400 bg-secondary-100` : 'cursor-pointer'}  pl-1 truncate`}
    >
      <span className={`text-${size}`}>{title}</span>
      {!noDropdownArrow && <Icon component="icon-[ic--baseline-arrow-drop-down]" size={16} />}
    </div>
  );

  const handleClick = () => {
    if (!disabled && onClick) {
      onClick();
    }
  };
  if (popover) {
    return (
      <PopoverTrigger className={`${disabled ? 'cursor-not-allowed opacity-50' : ''}`} onClick={handleClick}>
        {inner}
      </PopoverTrigger>
    );
  } else
    return (
      <button className={`w-full ${disabled ? 'cursor-not-allowed opacity-50' : ''}`} onClick={onClick} disabled={disabled}>
        {' '}
        {inner}
      </button>
    );
}

type DropdownItemContainerProps = {
  // align: string;
  className?: string;
  children: ReactNode;
  disabled?: boolean;
  root?: HTMLElement | null;
};

export const DropdownItemContainer = React.forwardRef<HTMLDivElement, DropdownItemContainerProps>(
  ({ children, className, disabled, root }, ref) => {
    if (!children || !React.Children.count(children)) return null;
    return (
      <PopoverContent
        ref={ref}
        className={`w-fit bg-light p-1 rounded focus:outline-none shadow-sm max-w-md max-h-60 ${disabled ? 'cursor-not-allowed opacity-50' : ''} ${
          className || ''
        }`}
        role="menu"
        aria-orientation="vertical"
        aria-labelledby="menu-button"
        tabIndex={-1}
        root={root}
      >
        <ul role="none" className="flex flex-col w-fit">
          {children}
        </ul>
      </PopoverContent>
    );
  },
);

type DropdownItemProps = {
  value: string;
  label?: string;
  disabled?: boolean;
  className?: string;
  onClick?: (value: string) => void;
  submenu?: React.ReactNode;
  selected?: boolean;
  children?: ReactNode;
};

export function DropdownItem({ value, label, disabled, className, onClick, submenu, selected, children }: DropdownItemProps) {
  const itemRef = useRef(null);
  const submenuRef = useRef(null);
  const [isSubmenuOpen, setIsSubmenuOpen] = useState(false);

  return (
    <li
      ref={itemRef}
      style={{ border: 0, listStyleType: 'none' }}
      className={`flex w-full grow cursor-pointer divide-y origin-top-right whitespace-nowrap
        rounded items-center justify-between text-sm gap-1.5 px-1.5 py-1 hover:bg-secondary-100 ${
          className && className
        } ${selected ? 'bg-secondary-400 text-white hover:text-black' : ''}`}
      onClick={() => {
        if (!disabled && onClick) {
          onClick(value);
        }
      }}
      onMouseEnter={() => setIsSubmenuOpen(true)}
      onMouseLeave={() => setIsSubmenuOpen(false)}
    >
      {label || value}
      {submenu != null ? <Icon component="icon-[ic--baseline-arrow-right] ms-2" size={14} /> : ''}
      {submenu && isSubmenuOpen && <DropdownSubmenuContainer ref={submenuRef}>{submenu}</DropdownSubmenuContainer>}
      {children}
    </li>
  );
}

type DropdownSubmenuContainerProps = {
  className?: string;
  children: ReactNode;
  disabled?: boolean;
};

export const DropdownSubmenuContainer = React.forwardRef<HTMLDivElement, DropdownSubmenuContainerProps>(
  ({ children, className, disabled }, ref) => {
    return (
      <div
        ref={ref}
        className={`absolute left-[95%] bg-light p-1 rounded focus:outline-none shadow-sm max-w-md max-h-60 !border overflow-auto ${disabled ? 'cursor-not-allowed opacity-50' : ''} ${
          className || ''
        }`}
        role="menu"
        aria-orientation="vertical"
        aria-labelledby="menu-button"
        tabIndex={-1}
        style={{ transform: 'translate(0px, calc(50% - 19px))' }}
      >
        <ul role="none" className="flex flex-col w-fit">
          {children}
        </ul>
      </div>
    );
  },
);
