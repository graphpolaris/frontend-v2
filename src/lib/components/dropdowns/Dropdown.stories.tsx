/* eslint-disable react-hooks/rules-of-hooks */
// Dropdown.stories.tsx

import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { DropdownContainer, DropdownTrigger, DropdownItemContainer, DropdownItem } from './index';
import { Icon } from '../icon';

const metaDropdown: Meta<typeof DropdownContainer> = {
  component: DropdownContainer,
  title: 'Components/Dropdown',
};

export default metaDropdown;
type Story = StoryObj<typeof DropdownTrigger>;

export const mainStory: Story = {
  render: args => {
    const [isOpen, setIsOpen] = useState(false);

    const handleToggle = () => setIsOpen(!isOpen);

    return (
      <>
        <DropdownContainer {...args} open={isOpen} onOpenChange={setIsOpen}>
          <DropdownTrigger onClick={handleToggle} disabled={args.disabled} size="md" title="Dropdown Trigger">
            <div className="flex items-center border border-black">
              <Icon component="icon-[ic--baseline-arrow-drop-down]" size={16} />
              <span className="ml-2">Trigger</span>
            </div>
          </DropdownTrigger>

          <DropdownItemContainer>
            <DropdownItem value="item-1" onClick={() => alert('Item 1 clicked')}>
              <div className="p-2">Item 1</div>
            </DropdownItem>
            <DropdownItem value="item-2" onClick={() => alert('Item 2 clicked')}>
              <div className="p-2">Item 2</div>
            </DropdownItem>
          </DropdownItemContainer>
        </DropdownContainer>
      </>
    );
  },
  args: {
    disabled: true,
  },
};
