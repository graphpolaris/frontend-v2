import React, { PropsWithChildren } from 'react';
import { Button } from '../buttons';

/** @deprecated */
export const FormDiv = React.forwardRef<HTMLDivElement, PropsWithChildren<{ className?: string; hAnchor?: string; offset?: string }>>(
  (props, ref) => {
    const dialogRef = React.useRef<HTMLDivElement | null>(null);
    const isClicked = React.useRef<boolean>(false);
    const initialPosition = React.useRef<{ x: number; y: number }>({ x: 0, y: 0 });

    React.useEffect(() => {
      if (!dialogRef.current) return;

      const dialog = dialogRef.current;
      const container = document.body;

      const onMouseDown = (e: MouseEvent) => {
        isClicked.current = true;
        initialPosition.current = {
          x: e.clientX - dialog.getBoundingClientRect().left,
          y: e.clientY - dialog.getBoundingClientRect().top,
        };
      };

      const onMouseUp = () => {
        isClicked.current = false;
      };

      const onMouseMove = (e: MouseEvent) => {
        if (!isClicked.current) return;
        const newX = e.clientX - initialPosition.current.x;
        const newY = e.clientY - initialPosition.current.y;
        dialog.style.top = `${newY}px`;
        dialog.style.left = `${newX}px`;
      };

      dialog.addEventListener('mousedown', onMouseDown);
      dialog.addEventListener('mouseup', onMouseUp);
      container.addEventListener('mousemove', onMouseMove);
      container.addEventListener('mouseleave', onMouseUp);

      return () => {
        dialog.removeEventListener('mousedown', onMouseDown);
        dialog.removeEventListener('mouseup', onMouseUp);
        container.removeEventListener('mousemove', onMouseMove);
        container.removeEventListener('mouseleave', onMouseUp);
      };
    }, []);

    return (
      <div
        className={
          'absolute opacity-100 transition-opacity group-hover:opacity-100 z-50 w-fit cursor-move' +
          (props.className ? props.className : '')
        }
        ref={node => {
          dialogRef.current = node;
          if (typeof ref === 'function') {
            ref(node);
          } else if (ref) {
            ref.current = node;
          }
        }}
        style={props.hAnchor === 'left' ? { left: props.offset || 0 } : { right: props.offset || '5rem' }}
      >
        {props.children}
      </div>
    );
  },
);
export const FormCard = (props: PropsWithChildren<{ className?: string }>) => (
  <div className={'border border-secondary-200 bg-light rounded-none p-2' + (props.className ? props.className : '')}>{props.children}</div>
);
export const FormBody = ({
  children,
  ...props
}: PropsWithChildren<React.DetailedHTMLProps<React.FormHTMLAttributes<HTMLFormElement>, HTMLFormElement>>) => (
  <form {...props}>{children}</form>
);
export const FormTitle = ({
  children,
  title,
  onClose,
  className = '',
  ...props
}: PropsWithChildren<{
  title: string;
  onClose?: () => void;
  className?: string;
}>) => {
  return (
    <div className={`font-semibold font-sm flex flex-row justify-between w-full p-2 ${className}`} {...props}>
      <h2>{title}</h2>
      {onClose && <Button rounded variant="ghost" iconComponent="icon-[ic--baseline-close]" onClick={onClose} />}
    </div>
  );
};
export const FormHBar = () => <hr className="border-px w-full my-1"></hr>;
export const FormControl = ({ children }: PropsWithChildren) => <div className="form-control">{children}</div>;
export const FormActions = ({
  onClose,
  onApply,
  size = 'md',
}: {
  onClose?: () => void;
  onApply?: () => void;
  size?: 'sm' | 'md' | 'lg';
}) => (
  <>
    {onClose ? (
      <div className="grid grid-cols-2 gap-2">
        <Button
          variantType="secondary"
          variant="outline"
          label="Cancel"
          size={size}
          onClick={e => {
            e.preventDefault();
            onClose();
          }}
        />
        <Button variantType="primary" label="Apply" size={size} onClick={onApply} className="flex-grow" />
      </div>
    ) : (
      <Button variantType="primary" label="Apply" size={size} onClick={onApply} className="w-full" />
    )}
  </>
);
