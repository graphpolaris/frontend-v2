import * as React from 'react';

/**
 * A custom hook that creates a stable callback reference.
 * Prevents unnecessary re-renders when passing callbacks as dependencies.
 */
export function useCallbackRef<T extends (...args: any[]) => any>(callback: T | undefined): T {
  const callbackRef = React.useRef(callback);

  React.useEffect(() => {
    callbackRef.current = callback;
  });

  return React.useMemo(() => ((...args) => callbackRef.current?.(...args)) as T, []);
}
