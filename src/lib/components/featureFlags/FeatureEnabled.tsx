import { canViewFeature, FeatureFlagName } from '.';
import { ReactNode } from 'react';

export function FeatureEnabled({ featureFlag, children }: { featureFlag: FeatureFlagName; children: ReactNode }) {
  return canViewFeature(featureFlag) ? children : null;
}
