import { getEnvVariable, EnvVarKey } from '@/config';

const getFeatureVariable = (flagId: EnvVarKey): boolean => {
  // TODO: move feature flag storage to database instead of env variables
  const value = getEnvVariable(flagId);
  if (value === undefined) {
    return false;
  }
  return String(value) === 'true';
};

export const ML_FLAGS = {
  LINK_PREDICTION: getFeatureVariable('LINK_PREDICTION'),
  CENTRALITY: getFeatureVariable('CENTRALITY'),
  COMMUNITY_DETECTION: getFeatureVariable('COMMUNITY_DETECTION'),
  SHORTEST_PATH: getFeatureVariable('SHORTEST_PATH'),
};

export const VISUALIZATIONS_FLAGS = {
  RAWJSONVIS: getFeatureVariable('RAWJSONVIS'),
  NODELINKVIS: getFeatureVariable('NODELINKVIS'),
  TABLEVIS: getFeatureVariable('TABLEVIS'),
  PAOHVIS: getFeatureVariable('PAOHVIS'),
  MATRIXVIS: getFeatureVariable('MATRIXVIS'),
  SEMANTICSUBSTRATESVIS: getFeatureVariable('SEMANTICSUBSTRATESVIS'),
  MAPVIS: getFeatureVariable('MAPVIS'),
  VIS0D: getFeatureVariable('VIS0D'),
  VIS1D: getFeatureVariable('VIS1D'),
};

export const FEATURE_FLAGS = {
  ...ML_FLAGS,
  ...VISUALIZATIONS_FLAGS,
  INSIGHT_SHARING: getFeatureVariable('INSIGHT_SHARING'),
  VIEWER_PERMISSIONS: getFeatureVariable('VIEWER_PERMISSIONS'),
  SHARABLE_EXPLORATION: getFeatureVariable('SHARABLE_EXPLORATION'),
  EDGE_BUNDLING: getFeatureVariable('EDGE_BUNDLING'),
} as const satisfies Record<string, boolean>;

export type FeatureFlagName = keyof typeof FEATURE_FLAGS;

export const canViewFeature = (flagId: FeatureFlagName): boolean => {
  return FEATURE_FLAGS[flagId];
};
