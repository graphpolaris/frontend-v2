import React, { ReactNode } from 'react';

type Props = {
  children: ReactNode;
};

export function ControlContainer({ children }: Props) {
  return <div className="top-4 right-4 flex flex-row-reverse justify-between z-50 items-center">{children}</div>;
}
