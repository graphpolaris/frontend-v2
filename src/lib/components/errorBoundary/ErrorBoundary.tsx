import React, { Component, ErrorInfo, ReactNode } from 'react';
import { Button } from '../buttons';
import { useAppDispatch } from '@/lib/data-access';
import { addInfo } from '@/lib/data-access/store/configSlice';

interface ErrorBoundaryProps {
  children: ReactNode;
  fallback?: ReactNode;
  onError?: (error: Error, errorInfo: ErrorInfo) => void;
  dispatch?: ReturnType<typeof useAppDispatch>;
}

interface ErrorBoundaryState {
  hasError: boolean;
  error?: Error;
  errorInfo?: ErrorInfo;
}

class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
    this.handleRetry = this.handleRetry.bind(this);
    this.handleCopyError = this.handleCopyError.bind(this);
  }

  static getDerivedStateFromError(): ErrorBoundaryState {
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    this.setState({ error, errorInfo });
    if (this.props.onError) {
      this.props.onError(error, errorInfo);
    }
  }

  handleRetry() {
    this.setState({ hasError: false, error: undefined, errorInfo: undefined });
  }

  handleCopyError() {
    const errorText = `Error: ${this.state.error?.toString()}\n\nStack trace: ${this.state.errorInfo?.componentStack}`;
    navigator.clipboard.writeText(errorText);
    if (this.props.dispatch) {
      this.props.dispatch(addInfo('Stack trace copied to clipboard'));
    }
  }

  render() {
    if (this.state.hasError) {
      return (
        <div className="flex flex-col w-full h-full">
          {this.props.fallback || <div>Something went wrong. Please try again later.</div>}
          {import.meta.env.GRAPHPOLARIS_VERSION === 'dev' && (
            <div className="overflow-auto max-h-[500px] p-2">
              <div className="flex justify-end mb-2">
                <Button label="Copy Stack Trace" variant="outline" variantType="secondary" size="xs" onClick={this.handleCopyError} />
              </div>
              <pre className="whitespace-pre-wrap break-words text-sm">{this.state.error?.toString()}</pre>
              <pre className="whitespace-pre-wrap break-words text-sm mt-2">{this.state.errorInfo?.componentStack}</pre>
            </div>
          )}
          <Button label="Retry now" variant="outline" variantType="primary" onClick={this.handleRetry} />
        </div>
      );
    }

    return this.props.children;
  }
}

function ErrorBoundaryWithDispatch(props: Omit<ErrorBoundaryProps, 'dispatch'>) {
  const dispatch = useAppDispatch();
  return <ErrorBoundary {...props} dispatch={dispatch} />;
}

export { ErrorBoundaryWithDispatch as ErrorBoundary };
