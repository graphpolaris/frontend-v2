import React, { ReactNode } from 'react';

export type NodeDetailsProps = {
  name: string;
  colorHeader: string;
  children: ReactNode;
};

export const NodeDetails: React.FC<NodeDetailsProps> = ({ name, colorHeader, children }) => {
  return (
    <div className="w-[12rem] divide-y divider-secondary-200">
      <div className="px-2 relative w-full">
        <div className="left-0 top-0 h-full w-1 shrink-0 absolute" style={{ backgroundColor: colorHeader }}></div>
        <div className="px-1 py-1">
          <div className="text-sm font-semibold truncate overflow-hidden whitespace-nowrap">{name}</div>
        </div>
      </div>
      {children && <div className="text-xs p-1">{children}</div>}
    </div>
  );
};
