import type { Meta, StoryObj } from '@storybook/react';
import { NodeDetails, NodeDetailsProps } from '@/lib/components/nodeDetails';
import { SchemaPopUp, SchemaPopUpProps } from '@/lib/schema/pills/nodes/SchemaPopUp/SchemaPopUp';
import { NLPopUp, NLPopUpProps } from '@/lib/vis/visualizations/nodelinkvis/components/NLPopup';

const meta: Meta<typeof NodeDetails> = {
  component: NodeDetails,
  title: 'Components/NodeDetails',
};

export default meta;

type CombinedProps = NodeDetailsProps & SchemaPopUpProps & NLPopUpProps & { attributes: { name: string; type: string }[] };

type Story = StoryObj<CombinedProps>;

export const SchemaNode: Story = {
  render: args => {
    const { name, attributes, colorHeader, numberOfElements } = args;
    const data = attributes.reduce(
      (acc, attr) => {
        if (attr.name && attr.type) {
          acc[attr.name] = attr.type;
        }
        return acc;
      },
      {} as Record<string, any>,
    );

    return (
      <div className="w-1/4 my-10 m-auto flex items-center justify-center">
        <NodeDetails name={name} colorHeader={colorHeader}>
          <SchemaPopUp data={data} numberOfElements={numberOfElements} />
        </NodeDetails>
      </div>
    );
  },
  args: {
    name: 'Person',
    attributes: [
      { name: 'int', type: 'int' },
      { name: 'float', type: 'float' },
      { name: 'date', type: 'date' },
      { name: 'string', type: 'string' },
      { name: 'boolean', type: 'boolean' },
      { name: 'undefined', type: 'undefined' },
    ],
    colorHeader: 'hsl(var(--clr-node))',
    numberOfElements: 1000,
  },
};

export const SchemaRelationship: Story = {
  render: args => {
    const { name, attributes, colorHeader, numberOfElements, connections } = args;
    const data = attributes.reduce(
      (acc, attr) => {
        if (attr.name && attr.type) {
          acc[attr.name] = attr.type;
        }
        return acc;
      },
      {} as Record<string, any>,
    );

    return (
      <div className="w-1/4 my-10 m-auto flex items-center justify-center">
        <NodeDetails name={name} colorHeader={colorHeader}>
          <SchemaPopUp data={data} numberOfElements={numberOfElements} connections={connections} />
        </NodeDetails>
      </div>
    );
  },
  args: {
    name: 'Directed',
    attributes: [
      { name: 'born', type: 'int' },
      { name: 'name', type: 'string' },
      { name: 'description', type: 'string' },
      { name: 'imdb', type: 'string' },
      { name: 'imdbVotes', type: 'int' },
    ],
    colorHeader: '#0676C1',
    numberOfElements: 231230,
    connections: { to: 'Person', from: 'Movie' },
  },
};

export const NodeLinkPopUp: Story = {
  render: args => {
    const { name, data, colorHeader } = args;
    return (
      <div className="w-1/4 my-10 m-auto flex items-center justify-center">
        <NodeDetails name={name} colorHeader={colorHeader}>
          <NLPopUp data={data} />
        </NodeDetails>
      </div>
    );
  },
  args: {
    name: 'Person',
    data: {
      bio: 'From wikipedia was born in usa from a firefighter father',
      name: 'Charlotte Henry',
      born: {},
      imdbRank: 21213,
      imdbVotes: 1213,
      poster: 'https://image.tmdb.org/t/p/w440_and_h660_face/kTKiREs37qd8GUlNI4Koiupwy6W.jpg',
      tmdbId: '94105',
      country: undefined,
      labels: ['Actor', 'Person', 'Human'],
    },
    colorHeader: '#B69AEf',
  },
};
