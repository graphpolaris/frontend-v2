import React, { useState } from 'react';
import { DropdownTrigger, DropdownContainer, DropdownItemContainer } from '@/lib/components/dropdowns';
import ColorLegend from '../colorLegend/index.js';
import { dataColors } from '@/config';

type TailwindColor = {
  colors: string | { [key: number]: string };
  data: { min: number; max: number };
  name: string;
  showAxis: boolean;
};

type TailwindColors = {
  [key: string]: TailwindColor;
};

function generateTailwindColors(dataColors: any) {
  const tailwindColors: TailwindColors = {};

  for (const colorName in dataColors) {
    const colorData = dataColors[colorName];

    if (colorName !== 'black' && colorName !== 'white') {
      tailwindColors[colorName] = {
        colors: colorData,
        data: { min: 0, max: 100 },
        name: `seq:${colorName}`,
        showAxis: false,
      };
    }
  }

  return tailwindColors;
}

type DropdownColorLegendProps = {
  value: any;
  onChange: (val: any) => void;
};

export const DropdownColorLegend = ({ value, onChange }: DropdownColorLegendProps) => {
  const colorStructure = generateTailwindColors(dataColors);
  const [selectedColorLegend, setSelectedColorLegend] = useState<TailwindColor | null>(null);
  const [menuOpen, setMenuOpen] = useState<boolean>(false);

  const handleOptionClick = (option: string) => {
    onChange(option);
    setSelectedColorLegend(colorStructure[option]);
    setMenuOpen(false);
  };

  return (
    <div className="w-200 h-200 relative">
      <DropdownContainer open={menuOpen}>
        <DropdownTrigger
          title={
            <div className="flex items-center h-4">
              {selectedColorLegend ? (
                <ColorLegend
                  colors={selectedColorLegend.colors}
                  data={selectedColorLegend.data}
                  name={selectedColorLegend.name}
                  showAxis={selectedColorLegend.showAxis}
                />
              ) : (
                <p className="ml-2">{value}</p>
              )}
            </div>
          }
          onClick={() => setMenuOpen(!menuOpen)}
        />
        <DropdownItemContainer className="absolute w-60 bg-white shadow-lg z-10">
          <ul>
            {Object.keys(colorStructure).map((option: string, index) => (
              <li key={index} onClick={() => handleOptionClick(option)} className="cursor-pointer flex items-center ml-2 h-4 m-2">
                <ColorLegend
                  key={index.toString() + '_colorLegend'}
                  colors={colorStructure[option].colors}
                  data={colorStructure[option].data}
                  name={colorStructure[option].name}
                  showAxis={colorStructure[option].showAxis}
                />
              </li>
            ))}
          </ul>
        </DropdownItemContainer>
      </DropdownContainer>
    </div>
  );
};

export default DropdownColorLegend;
