import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import DropdownColorLegend from '.';

const Component: Meta<typeof DropdownColorLegend> = {
  title: 'ColorManager/Legend/DropdownGP',
  component: DropdownColorLegend,
  argTypes: { onChange: { action: 'changed' } },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;

type Story = StoryObj<typeof Component>;

export const DropdownColorLegendStory: Story = (args: any) => {
  const [value, setValue] = useState<string>('');

  return <DropdownColorLegend value={value} onChange={setValue} />;
};

DropdownColorLegendStory.args = {};
