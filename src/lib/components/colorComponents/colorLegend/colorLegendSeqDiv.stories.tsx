import React from 'react';
import { Meta } from '@storybook/react';
import ColorLegend from '.';

export default {
  title: 'ColorManager/Legend/ColorLegend',
  component: ColorLegend,
  decorators: [story => <div style={{ margin: '0', width: '300px', height: '50px' }}>{story()}</div>],
} as Meta<typeof ColorLegend>;

export const Default = {
  args: {
    colors: {
      5: 'hsl(220 80% 98%)',
      10: 'hsl(220 71% 96%)',
      20: 'hsl(220 95% 92%)',
      30: 'hsl(220 92% 85%)',
      40: 'hsl(220 94% 75%)',
      50: 'hsl(220 92% 67%)',
      60: 'hsl(220 84% 58%)',
      70: 'hsl(220 79% 49%)',
      80: 'hsl(220 86% 36%)',
      90: 'hsl(220 80% 23%)',
      95: 'hsl(220 84% 17%)',
      100: 'hsl(220 61% 13%)',
    },

    //colors: dataColors.blue,
    showAxis: false,
    data: { min: 0, max: 100 },
    tickCount: 5,
    name: 'seq:blue',
  },
};
