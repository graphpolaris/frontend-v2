import React, { useState } from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { ColorPicker } from '.';

const Component: Meta<typeof ColorPicker> = {
  title: 'ColorManager/Legend/ColorPicker',
  component: ColorPicker,
  argTypes: { onChange: { action: 'changed' } },
  decorators: [Story => <div className="w-52 m-5">{Story()}</div>],
};

export default Component;

type Story = StoryObj<typeof Component>;

export const ColorPickerStory: Story = (args: any) => {
  const [value, setValue] = useState<[number, number, number]>([251, 150, 55]);

  return <ColorPicker value={value} onChange={setValue} />;
};

ColorPickerStory.args = {};
