import { visualizationColors } from '@/config';
import { Popover, PopoverTrigger, PopoverContent } from '@/lib/components/layout/Popover';

type Props = {
  value: [number, number, number];
  onChange: (val: number) => void;
};

export function ColorPicker({ value, onChange }: Props) {
  return (
    value && (
      <div>
        <Popover>
          <PopoverTrigger
            onClick={e => {
              e.stopPropagation();
            }}
          >
            <div className="w-4 h-4 rounded-sm" style={{ backgroundColor: `rgb(${value[0]}, ${value[1]}, ${value[2]})` }} />
          </PopoverTrigger>
          <PopoverContent>
            <div
              className="grid grid-cols-4 gap-2 p-2"
              onClick={e => {
                e.stopPropagation();
              }}
            >
              {visualizationColors.GPCat.colors[14].map((hexColor, i) => {
                return (
                  <div
                    key={hexColor}
                    className="w-4 h-4 rounded-sm cursor-pointer"
                    style={{ backgroundColor: hexColor }}
                    onClick={e => {
                      e.stopPropagation();
                      onChange(i);
                    }}
                  />
                );
              })}
            </div>
          </PopoverContent>
        </Popover>
      </div>
    )
  );
}
