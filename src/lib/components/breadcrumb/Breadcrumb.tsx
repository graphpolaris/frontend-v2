import React from 'react';

interface BreadcrumbProps {
  paths: string[];
  onNavigate: (index: number) => void;
}

export const Breadcrumb: React.FC<BreadcrumbProps> = ({ paths, onNavigate }) => {
  return (
    <div className="flex items-center space-x-2 text-sm font-semibold">
      <button onClick={() => onNavigate(-1)} className="text-secondary-900 hover:text-primary-700">
        Home
      </button>
      {paths.map((path, index) => (
        <React.Fragment key={path}>
          <span className="text-secondary-400 font-bold">{'>'}</span>
          <button
            onClick={() => onNavigate(index)}
            className={`text-secondary-900 font-bold ${index !== paths.length - 1 ? 'hover:text-primary-700' : ''}`}
          >
            {path}
          </button>
        </React.Fragment>
      ))}
    </div>
  );
};
