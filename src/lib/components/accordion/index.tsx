import React, { useState, ReactElement } from 'react';
import { Button } from '../buttons';

type AccordionProps = {
  children: React.ReactNode;
  defaultOpenIndex?: number;
  defaultOpenAll?: boolean;
  className?: string;
};

export function Accordion({ children, defaultOpenIndex, defaultOpenAll = false, className = '' }: AccordionProps) {
  const childrenArray = React.Children.toArray(children);
  const [openIndexes, setOpenIndexes] = useState<number[]>(() => {
    if (defaultOpenAll) {
      return childrenArray.map((_, index) => index);
    } else if (defaultOpenIndex !== undefined) {
      return [defaultOpenIndex];
    } else {
      return [];
    }
  });

  const toggleIndex = (index: number) => {
    setOpenIndexes(currentIndexes =>
      currentIndexes.includes(index) ? currentIndexes.filter(i => i !== index) : [...currentIndexes, index],
    );
  };

  return (
    <div className={`w-full ${className}`}>
      {React.Children.map(children, (child, index) => {
        if (React.isValidElement(child)) {
          return React.cloneElement(child as ReactElement<AccordionItemProps>, {
            isOpen: openIndexes.includes(index),
            onToggle: () => toggleIndex(index),
          });
        }
        return child;
      })}
    </div>
  );
}

type AccordionItemProps = {
  isOpen?: boolean;
  onToggle?: () => void;
  children: React.ReactNode;
  className?: string;
};

export function AccordionItem({ isOpen = false, onToggle, children, className = '' }: AccordionItemProps) {
  return (
    <div className={`w-full ${className}`}>
      {React.Children.map(children, child => {
        if (React.isValidElement(child)) {
          return React.cloneElement(child as ReactElement<AccordionHeadProps | AccordionBodyProps>, {
            isOpen,
            onToggle,
          });
        }
        return child;
      })}
    </div>
  );
}

type AccordionHeadProps = {
  isOpen?: boolean;
  onToggle?: () => void;
  children: React.ReactNode;
  showArrow?: boolean;
  className?: string;
};

export function AccordionHead({ isOpen = false, onToggle, children, showArrow = true, className = '' }: AccordionHeadProps) {
  return (
    <div className={`cursor-pointer flex items-center w-full box-border ${className}`} onClick={onToggle}>
      {showArrow && (
        <Button
          size="2xs"
          iconComponent={!isOpen ? 'icon-[ic--baseline-arrow-right]' : 'icon-[ic--baseline-arrow-drop-down]'}
          variant="ghost"
          className="mr-1"
        />
      )}
      {children}
    </div>
  );
}

type AccordionBodyProps = {
  isOpen?: boolean;
  children: React.ReactNode;
  className?: string;
};

export function AccordionBody({ isOpen = false, children, className = '' }: AccordionBodyProps) {
  return (
    <div
      className={`overflow-hidden transition-max-height duration-300 ease-in-out box-border ml-2 ${isOpen ? 'max-h-screen' : 'max-h-0'} ${className}`}
    >
      {isOpen && <div>{children}</div>}
    </div>
  );
}
