import React from 'react';
import { StoryObj, Meta } from '@storybook/react';
import { Accordion, AccordionItem, AccordionHead, AccordionBody } from '.';
import { EntityPill, RelationPill } from '../pills';

export default {
  title: 'Components/Accordion',
  component: Accordion,
  decorators: [Story => <div className="flex m-5">{Story()}</div>],
} as Meta<typeof Accordion>;

type Story = StoryObj<typeof Accordion>;

export const Default: Story = {
  render: () => (
    <div className="max-w-md mx-auto my-10">
      <Accordion defaultOpenIndex={0} className="w-64">
        <AccordionItem>
          <AccordionHead>
            <EntityPill title="PERSON" />
          </AccordionHead>
          <AccordionBody>This is the content of Section 1.</AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHead>
            <EntityPill title="INDICENT" />
          </AccordionHead>
          <AccordionBody>
            <Accordion>
              <AccordionItem>
                <AccordionHead showArrow={false}>Location info</AccordionHead>
                <AccordionBody>Location info settings</AccordionBody>
              </AccordionItem>
              <AccordionItem>
                <AccordionHead showArrow={false}>Color and shape</AccordionHead>
                <AccordionBody>Color and shape settings</AccordionBody>
              </AccordionItem>
            </Accordion>
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHead>
            <RelationPill title="PERSON_OF" />
          </AccordionHead>
          <AccordionBody>This is the content of Section 3.</AccordionBody>
        </AccordionItem>
      </Accordion>
    </div>
  ),
};
