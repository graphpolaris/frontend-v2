import { Meta, StoryObj } from '@storybook/react';
import { Tooltip, TooltipTrigger, TooltipContent, TooltipProvider } from '@/lib';

export default {
  title: 'Components/Tooltip',
  component: Tooltip,
  tags: ['autodocs'],
  decorators: [
    Story => (
      <div className="p-10">
        <Story />
      </div>
    ),
  ],
  parameters: {
    docs: {
      description: {
        component: 'The **Tooltip** component provides contextual information when users hover over an element. It supports various customization options, including delay, placement, and arrow display.',
      },
    },
  },
  argTypes: {
    placement: {
      control: 'inline-radio',
      options: ['top', 'bottom', 'left', 'right'],
      description: 'Tooltip placement relative to the trigger.',
    },
    groupDelay: {
      control: 'boolean',
      description: 'Wrap the tooltip in a TooltipProvider to apply group delay across multiple tooltips.',
    },
    delayOpen: {
      control: 'number',
      description: 'Delay before the tooltip opens (in milliseconds).',
    },
    delayClose: {
      control: 'number',
      description: 'Delay before the tooltip closes (in milliseconds).',
    },
    showArrow: {
      control: 'boolean',
      description: 'Show an arrow on the tooltip.',
    },

    children: { table: { disable: true } },
    boundaryElement: { table: { disable: true } },
    delay: { table: { disable: true } },
    onOpenChange: { table: { disable: true } },
    open: { table: { disable: true } },
    initialOpen: { table: { disable: true } },
  },
} as Meta;

const Template = (args: any) => {
  return (
    <div className="p-10 flex flex-row gap-1 items-center justify-center">
      {args.groupDelay ? (
        <TooltipProvider delay={{ open: args.delayOpen, close: args.delayClose }}>
          <TooltipInstance {...args} />
          <TooltipInstance {...args} />
          <TooltipInstance {...args} />
        </TooltipProvider>
      ) : (
        <TooltipInstance {...args} />
      )}
    </div>
  );
};

const TooltipInstance = ({ placement, showArrow, delayOpen, delayClose, groupDelay }: any) => (
  <Tooltip
    placement={placement}
    showArrow={showArrow}
    {...(!groupDelay && { delay: { open: delayOpen, close: delayClose } })} // ✅ Remove delay if groupDelay is true
  >
    <TooltipTrigger>
      <button className="border px-3 py-2 hover:text-primary rounded">Hover me</button>
    </TooltipTrigger>
    <TooltipContent>Tooltip at {placement}</TooltipContent>
  </Tooltip>
);

export const Default: StoryObj = {
  render: Template,
  args: {
    placement: 'top',
    showArrow: true,
    delayOpen: 500,
    delayClose: 150,
    groupDelay: false,
  },
  parameters: {
    docs: {
      description: {
        story: 'A simple tooltip that appears when hovering over the trigger element.',
      },
    },
  },
};

export const WithGroupDelay: StoryObj = {
  render: Template,
  args: {
    placement: 'top',
    showArrow: true,
    delayOpen: 500,
    delayClose: 100,
    groupDelay: true,
  },
  parameters: {
    docs: {
      description: {
        story: 'Multiple tooltips wrapped in a TooltipProvider to share the same open/close delay.',
      },
    },
  },
};
