import * as React from 'react';
import {
  useFloating,
  autoUpdate,
  offset,
  flip,
  shift,
  hide,
  arrow,
  useHover,
  useFocus,
  useDismiss,
  useRole,
  useInteractions,
  useMergeRefs,
  FloatingPortal,
  FloatingArrow,
} from '@floating-ui/react';
import type { Placement } from '@floating-ui/react';
import { useDelayGroup, FloatingDelayGroup } from '@floating-ui/react';
import { useControllableState } from '@/lib/components/primitives/useControllableState';

interface TooltipOptions {
  initialOpen?: boolean;
  placement?: Placement;
  open?: boolean;
  onOpenChange?: (open: boolean) => void;
  boundaryElement?: React.RefObject<HTMLElement> | HTMLElement | null | React.RefObject<HTMLDivElement | null>;
  showArrow?: boolean;
  delay?: number | { open?: number; close?: number };
}

export function useTooltip({
  initialOpen = false,
  placement = 'bottom',
  open: controlledOpen,
  onOpenChange: setControlledOpen,
  boundaryElement = null,
  showArrow = true,
  delay,
}: TooltipOptions = {}): {
  open: boolean;
  setOpen: (open: boolean) => void;
  interactions: ReturnType<typeof useInteractions>;
  data: ReturnType<typeof useFloating>;
} {
  const [open, setOpen] = useControllableState({
    prop: controlledOpen,
    defaultProp: initialOpen,
    onChange: setControlledOpen,
  });

  const arrowRef = React.useRef<SVGSVGElement | null>(null);

  const middleware = [
    offset(5),
    flip({
      crossAxis: placement.includes('-'),
      fallbackAxisSideDirection: 'start',
      padding: 5,
    }),
    shift({ padding: 5 }),
  ];

  if (boundaryElement) {
    const boundary = boundaryElement instanceof HTMLElement ? boundaryElement : (boundaryElement?.current ?? undefined);

    if (boundary) {
      const flipMiddleware = middleware.find(m => m.name === 'flip');
      const shiftMiddleware = middleware.find(m => m.name === 'shift');

      if (flipMiddleware) {
        (flipMiddleware as any).options.boundary = boundary;
      }
      if (shiftMiddleware) {
        (shiftMiddleware as any).options.boundary = boundary;
      }
      middleware.push(hide({ boundary }));
    }
  }

  if (showArrow) {
    middleware.push(arrow({ element: arrowRef }));
  }

  const data = useFloating({
    placement,
    open,
    onOpenChange: setOpen,
    whileElementsMounted: autoUpdate,
    middleware,
  });

  (data.refs as any).arrow = arrowRef;

  const context = data.context;
  const { delay: groupDelay } = useDelayGroup(context);

  const hover = useHover(context, {
    move: false,
    enabled: controlledOpen == null,
    delay: delay ?? groupDelay ?? undefined,
  });

  const focus = useFocus(context, { enabled: controlledOpen == null });
  const dismiss = useDismiss(context);
  const role = useRole(context, { role: 'tooltip' });

  const interactions = useInteractions([hover, focus, dismiss, role]);

  return React.useMemo(
    () => ({
      open: open ?? false,
      setOpen,
      interactions,
      data,
    }),
    [open, setOpen, interactions, data],
  );
}

type ContextType = ReturnType<typeof useTooltip> | null;

const TooltipContext = React.createContext<ContextType>(null);

export const useTooltipContext = () => {
  const context = React.useContext(TooltipContext);

  if (context == null) {
    throw new Error('Tooltip components must be wrapped in <Tooltip />');
  }

  return context;
};

export function Tooltip({ children, ...options }: { children: React.ReactNode } & TooltipOptions) {
  // This can accept any props as options, e.g. `placement`,
  // or other positioning options.
  const tooltip = useTooltip(options);

  return <TooltipContext.Provider value={tooltip}>{children}</TooltipContext.Provider>;
}

export const TooltipTrigger = React.forwardRef<HTMLElement, React.HTMLProps<HTMLElement> & { asChild?: boolean; x?: number; y?: number }>(
  function TooltipTrigger({ children, asChild = false, x = null, y = null, ...props }, propRef) {
    const context = useTooltipContext();
    const ref = useMergeRefs([context.data.refs.setReference, propRef]);

    const referenceProps = context.interactions.getReferenceProps(props) as React.HTMLAttributes<HTMLElement>;

    const handleClick: React.MouseEventHandler<HTMLElement> = event => {
      if (context.open) {
        context.setOpen(false);
      }
      if (props.onClick) {
        (props.onClick as React.MouseEventHandler<HTMLElement>)(event);
      }
    };

    React.useEffect(() => {
      if (x && y && context.data.refs.reference.current != null) {
        const element = context.data.refs.reference.current as HTMLElement;
        element.style.position = 'absolute';
        const { x: offsetX, y: offsetY } = element.getBoundingClientRect();
        element.getBoundingClientRect = () =>
          ({
            width: 0,
            height: 0,
            x: offsetX,
            y: offsetY,
            top: y + offsetY,
            left: x + offsetX,
            right: x + offsetX,
            bottom: y + offsetY,
          }) as DOMRect;
        context.data.update();
      }
    }, [x, y]);

    if (asChild && React.isValidElement(children)) {
      const child = children as React.ReactElement<any>;
      const childOnClick = child.props.onClick;
      const mergedOnClick: React.MouseEventHandler<HTMLElement> = event => {
        handleClick(event);
        if (childOnClick) {
          childOnClick(event);
        }
      };

      return React.cloneElement(child, {
        ref,
        ...referenceProps,
        onClick: mergedOnClick,
        'data-state': context.open ? 'open' : 'closed',
      });
    }

    return (
      <div ref={ref} data-state={context.open ? 'open' : 'closed'} {...referenceProps} onClick={handleClick}>
        {children}
      </div>
    );
  },
);

export const TooltipContent = React.forwardRef<HTMLDivElement, React.HTMLProps<HTMLDivElement>>(function TooltipContent(
  { style, className, ...props },
  propRef,
) {
  const context = useTooltipContext();
  const ref = useMergeRefs([context.data.refs.setFloating, propRef]);

  if (!context.open) return null;

  return (
    <FloatingPortal>
      <div
        ref={ref}
        className={`max-w-64 rounded bg-dark dark:bg-secondary-200 px-2 py-1 shadow-md text-xs pointer-events-none
          text-secondary-100 dark:text-secondary-900 animate-in fade-in-0 zoom-in-95 data-[state=closed]:animate-out data-[state=closed]:fade-out-0
          data-[state=closed]:zoom-out-95 data-[side=bottom]:slide-in-from-top-2 data-[side=left]:slide-in-from-right-2
          data-[side=right]:slide-in-from-left-2 data-[side=top]:slide-in-from-bottom-2${className ? ` ${className}` : ''}`}
        style={{
          ...context.data.floatingStyles,
          ...style,
          display: context.data.middlewareData.hide?.referenceHidden ? 'none' : 'block',
        }}
        {...context.interactions.getFloatingProps(props)}
      >
        {props.children}
        {context.data.middlewareData.arrow ? (
          <FloatingArrow
            ref={(context.data.refs as any).arrow}
            width={10}
            height={5}
            tipRadius={2}
            context={context.data.context}
            className={'fill-dark dark:fill-secondary-200'}
          />
        ) : null}
      </div>
    </FloatingPortal>
  );
});

export const TooltipProvider = ({
  delay = { open: 1000, close: 100 },
  children,
}: {
  delay?: number | { open?: number; close?: number };
  children: React.ReactNode;
}) => {
  return <FloatingDelayGroup delay={delay}>{children}</FloatingDelayGroup>;
};
