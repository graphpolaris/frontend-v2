import { useRef } from 'react';

export const Popup = (props: {
  children: React.ReactNode;
  open: boolean;
  hAnchor: 'left' | 'right';
  className?: string;
  offset?: string;
}) => {
  const ref = useRef<HTMLDivElement>(null);

  return (
    <>
      {props.open && (
        <div
          ref={ref}
          className={
            'absolute z-50 max-w-[20rem] bg-light flex flex-col gap-2 animate-openmenu p-0 m-0 ' + (props.className ? props.className : '')
          }
          style={props.hAnchor === 'right' ? { left: props.offset || 0 } : { right: props.offset || 0 }}
        >
          {props.children}
        </div>
      )}
    </>
  );
};
