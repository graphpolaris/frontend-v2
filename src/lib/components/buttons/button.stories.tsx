import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { Button } from '.';

const meta: Meta<typeof Button> = {
  title: 'Components/Button',
  component: Button,
  decorators: [Story => <div className="p-5">{Story()}</div>],
  argTypes: {
    type: {
      control: 'select',
      options: ['primary', 'secondary', 'danger'],
    },
    variant: {
      control: 'select',
      options: ['solid', 'outline', 'ghost'],
    },
    size: {
      control: 'select',
      options: ['xs', 'sm', 'md', 'lg'],
    },
    label: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    block: {
      control: 'boolean',
    },
    rounded: {
      control: 'boolean',
    },
    iconComponent: {
      control: 'select',
      options: [], // TODO
    },
    iconPosition: {
      control: 'select',
      options: ['leading', 'trailing'],
    },
  },
} as Meta;

export default meta;
type Story = StoryObj<typeof Button>;

const BaseStory: Story = {
  args: {
    variantType: 'primary',
    variant: 'solid',
    label: 'Click me',
    size: 'md',
    disabled: false,
    block: false,
    rounded: false,
  },
};

export const Primary = { ...BaseStory };

export const Secondary: Story = {
  args: {
    ...BaseStory.args,
    variantType: 'secondary',
  },
};

export const Danger: Story = {
  args: {
    ...BaseStory.args,
    variantType: 'danger',
  },
};

export const Solid: Story = {
  args: {
    ...BaseStory.args,
    variant: 'solid',
  },
};

export const Outline: Story = {
  args: {
    ...BaseStory.args,
    variant: 'outline',
  },
};

export const Ghost: Story = {
  args: {
    ...BaseStory.args,
    variant: 'ghost',
  },
};

export const IconButton: Story = {
  args: {
    variantType: 'primary',
    variant: 'outline',
    iconComponent: 'icon-[ic--baseline-arrow-back]',
    rounded: true,
  },
};
