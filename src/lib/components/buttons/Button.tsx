import React, { useMemo } from 'react';
import styles from './buttons.module.scss';
import { Icon } from '../icon';
import { Tooltip, TooltipContent, TooltipTrigger } from '../tooltip';

export type ButtonProps = {
  as?: 'button' | 'a' | 'div';
  variantType?: 'primary' | 'secondary' | 'danger';
  variant?: 'solid' | 'outline' | 'ghost';
  size?: '3xs' | '2xs' | 'xs' | 'sm' | 'md' | 'lg' | 'xl' | '2xl';
  label?: string;
  rounded?: boolean;
  disabled?: boolean;
  block?: boolean;
  onClick?: (e: any) => void;
  onDoubleClick?: (e: any) => void;
  href?: string;
  iconComponent?: React.ReactElement | string;
  iconPosition?: 'leading' | 'trailing';
  ariaLabel?: string;
  children?: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
  tooltip?: string;
  onMouseUp?: (e: any) => void;
  onMouseDown?: (e: any) => void;
  onMouseEnter?: (e: any) => void;
  onMouseLeave?: (e: any) => void;
  onMouseOver?: (e: any) => void;
  onMouseOut?: (e: any) => void;
  onMouseUpCapture?: (e: any) => void;
  onMouseDownCapture?: (e: any) => void;
  onMouseEnterCapture?: (e: any) => void;
  onMouseLeaveCapture?: (e: any) => void;
  onMouseOverCapture?: (e: any) => void;
  onMouseOutCapture?: (e: any) => void;

  // Drag and Drop
  onDragStart?: (e: any) => void;
  onDrag?: (e: any) => void;
  onDragEnd?: (e: any) => void;
  onDragEnter?: (e: any) => void;
  onDragExit?: (e: any) => void;
  onDragLeave?: (e: any) => void;
  onDragOver?: (e: any) => void;
  onDragStartCapture?: (e: any) => void;
  onDragCapture?: (e: any) => void;
  onDragEndCapture?: (e: any) => void;
  onDragEnterCapture?: (e: any) => void;
  onDragExitCapture?: (e: any) => void;
  onDragLeaveCapture?: (e: any) => void;
  onDragOverCapture?: (e: any) => void;
  onDrop?: (e: any) => void;
  onDropCapture?: (e: any) => void;
  draggable?: boolean;
};

export const Button = React.forwardRef<HTMLButtonElement | HTMLAnchorElement | HTMLDivElement, ButtonProps>(
  (
    {
      as = 'button',
      variantType = 'secondary',
      variant = 'solid',
      label,
      size = 'md',
      rounded = false,
      disabled = false,
      onClick,
      href,
      block = false,
      iconComponent,
      iconPosition = 'leading',
      ariaLabel,
      className,
      children,
      tooltip,
      ...props
    },
    forwardedRef,
  ) => {
    const typeClass = useMemo(() => {
      switch (variantType) {
        case 'primary':
          return styles['btn-primary'];
        case 'secondary':
          return styles['btn-secondary'];
        case 'danger':
          return styles['btn-danger'];
        default:
          return styles['btn-secondary'];
      }
    }, [variantType]);

    const variantClass = useMemo(() => {
      switch (variant) {
        case 'solid':
          return styles['btn-solid'];
        case 'outline':
          return styles['btn-outline'];
        case 'ghost':
          return styles['btn-ghost'];
        default:
          return styles['btn-solid'];
      }
    }, [variant]);

    const sizeClass = useMemo(() => {
      switch (size) {
        case '3xs':
          return styles['btn-3xs'];
        case '2xs':
          return styles['btn-2xs'];
        case 'xs':
          return styles['btn-xs'];
        case 'sm':
          return styles['btn-sm'];
        case 'md':
          return styles['btn-md'];
        case 'lg':
          return styles['btn-lg'];
        case 'xl':
          return styles['btn-xl'];
        case '2xl':
          return styles['btn-2xl'];
        default:
          return styles['btn-md'];
      }
    }, [size]);

    const iconSize = useMemo(() => {
      switch (size) {
        case '3xs':
          return 12;
        case '2xs':
          return 12;
        case 'xs':
          return 16;
        case 'sm':
          return 16;
        case 'md':
          return 20;
        case 'lg':
          return 20;
        case 'xl':
          return 24;
        case '2xl':
          return 24;
        default:
          return 20;
      }
    }, [size]);

    const blockClass = useMemo(() => (block ? styles['btn-block'] : ''), [block, styles]);
    const roundedClass = useMemo(() => (rounded ? styles['btn-rounded'] : ''), [rounded, styles]);

    const icon = useMemo(() => (iconComponent ? <Icon component={iconComponent} size={iconSize} /> : null), [iconComponent, iconSize]);

    const iconOnlyClass = useMemo(
      () => (iconComponent && !label && !children ? styles['btn-icon-only'] : ''),
      [iconComponent, label, children],
    );

    const disabledClass = useMemo(() => (disabled ? 'cursor-not-allowed' : 'cursor-pointer'), [disabled]);

    const ButtonComponent = as;

    const isAnchor = as === 'a';

    return (
      <Tooltip>
        {tooltip && <TooltipContent>{tooltip}</TooltipContent>}
        <TooltipTrigger>
          <ButtonComponent
            className={`${styles.btn} ${typeClass} ${variantClass} ${sizeClass} ${blockClass} ${roundedClass} ${iconOnlyClass} ${disabledClass} ${className ? className : ''}`}
            onClick={disabled ? undefined : onClick}
            disabled={disabled}
            aria-label={ariaLabel}
            href={isAnchor ? href : undefined}
            ref={forwardedRef as React.RefObject<any>}
            {...props}
          >
            {iconPosition === 'leading' && icon}
            {label && <span>{label}</span>}
            {children && <span>{children}</span>}
            {iconPosition === 'trailing' && icon}
          </ButtonComponent>
        </TooltipTrigger>
      </Tooltip>
    );
  },
);

type ButtonGroupProps = {
  children: React.ReactNode;
};

export function ButtonGroup({ children }: ButtonGroupProps) {
  return <div>{children}</div>;
}

type ButtonItemProps = {
  icon: React.ReactNode;
  onClick: () => void;
};

export function ButtonGroupItem({ icon, onClick }: ButtonItemProps) {
  return (
    <div onClick={onClick} className="rounded-sm bg-secondary-50 p-2">
      <span>{icon}</span>
    </div>
  );
}
