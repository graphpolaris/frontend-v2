import React, { useEffect } from 'react';
import { Button } from '../buttons';
import { Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from '../../components/tooltip';
import { setTheme, Theme } from '@/lib/data-access/store/configSlice';
import { useAppDispatch, useConfig } from '@/lib/data-access/store';

const ColorMode = () => {
  const config = useConfig();
  const dispatch = useAppDispatch();

  // Function to update the body class
  const applyTheme = (themeValue: string) => {
    document.body.className = themeValue;
  };

  // Update the local storage and body class whenever the theme changes
  useEffect(() => {
    if (config.theme) {
      applyTheme(config.theme);
    }
  }, [config.theme]);

  // Function to toggle the theme
  const toggleTheme = () => {
    const themes = [Theme.system, Theme.light, Theme.dark];

    const newTheme = themes[(themes.indexOf(config.theme) + 1) % themes.length];
    dispatch(setTheme(newTheme));
  };
  const iconComponent =
    config.theme === Theme.dark
      ? 'icon-[ic--baseline-dark-mode]'
      : config.theme === Theme.light
        ? 'icon-[ic--baseline-light-mode]'
        : 'icon-[ic--baseline-auto-mode]';

  return (
    <TooltipProvider delay={0}>
      <Tooltip placement={'right'}>
        <TooltipTrigger asChild>
          <Button variant="ghost" size="lg" iconComponent={iconComponent} onClick={toggleTheme} />
        </TooltipTrigger>
        <TooltipContent>
          <p>{`Currently on ${config.theme.split('-')[0]} theme`}</p>
        </TooltipContent>
      </Tooltip>
    </TooltipProvider>
  );
};

export default ColorMode;
