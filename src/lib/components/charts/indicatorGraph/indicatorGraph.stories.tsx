// BarPlot.stories.tsx
import { Meta } from '@storybook/react';
import { IndicatorGraph } from '.';

const Component: Meta<typeof IndicatorGraph> = {
  title: 'Visual charts/Charts/IndicatorGraph',
  component: IndicatorGraph,
  decorators: [story => <div style={{ width: '100%', height: '100%' }}>{story()}</div>],
};

export default Component;

export const Data1 = {
  args: {
    data: 0.1,
    maxValue: 0.5,
    marginPercentage: { top: 0.2, right: 0.015, left: 0.015, bottom: 0.2 },
    ticks: true,
    className: 'border-solid border-2 border-gray-600',
  },
};

export const Data2 = {
  args: {
    data: 0.25,
    maxValue: 0.5,
    marginPercentage: { top: 0.1, right: 0.05, left: 0.05, bottom: 0.1 },
    ticks: true,
    className: 'border-solid border-2 border-gray-600',
  },
};

export const DataOverflow = {
  args: {
    data: 0.25,
    maxValue: 0.15,
    marginPercentage: { top: 0.1, right: 0.05, left: 0.05, bottom: 0.1 },
    ticks: true,
    className: 'border-solid border-2 border-gray-600',
  },
};
