import { useEffect, useMemo, useRef, useState } from 'react';
import { Tooltip, TooltipTrigger, TooltipContent, TooltipProvider } from '@/lib/components/tooltip';
import { scaleLinear, select } from 'd3';

export type IndicatorGraphProps = {
  data: number;
  maxValue: number;
  marginPercentage?: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  className?: string;
  strokeWidth?: number;
  ticks?: boolean;
};

export const IndicatorGraph = ({ maxValue, data, marginPercentage, className, ticks }: IndicatorGraphProps) => {
  const svgRef = useRef<SVGSVGElement | null>(null);
  const groupMarginRef = useRef<SVGGElement | null>(null);

  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });

  useEffect(() => {
    function handleResize() {
      if (svgRef.current) {
        setDimensions({
          width: svgRef.current.getBoundingClientRect().width,
          height: svgRef.current.getBoundingClientRect().height,
        });
      }
    }

    window.addEventListener('resize', handleResize);
    if (svgRef.current) new ResizeObserver(handleResize).observe(svgRef.current);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const configTicks = {
    stroke: 'hsl(var(--clr-sec--500))',
    strokeWidth: '2',
    fontText: 10,
    textAnchor: 'middle',
    ticksVerticalCutoff: 0.85,
  };

  const [dataRects, positionTicks] = useMemo(() => {
    if (!svgRef.current || dimensions.width === 0 || dimensions.height === 0) return [];
    const { width, height } = dimensions;
    const widthSVG: number = width;
    const heightSVG: number = height;

    if (!marginPercentage)
      marginPercentage = {
        top: 0.19,
        right: 0.02,
        bottom: 0.19,
        left: 0.19,
      };
    const margin = {
      top: marginPercentage.top * heightSVG,
      right: marginPercentage.right * widthSVG,
      bottom: marginPercentage.bottom * heightSVG,
      left: marginPercentage.left * widthSVG,
    };
    select(groupMarginRef.current).attr('transform', `translate(${margin.left},${margin.top})`);

    const widthSVGwithinMargin = widthSVG - margin.left - margin.right;
    const heightSVGwithinMargin = heightSVG - margin.top - margin.bottom;
    const xScale = scaleLinear().domain([0, maxValue]).range([0, widthSVGwithinMargin]);
    // ticks position
    const ticksY = margin.bottom / 2;

    const positionTicks = {
      xTicksLeft: margin.left / 2.0,
      xTicksRight: margin.left + widthSVGwithinMargin + margin.right / 2.0,
      yTicks: heightSVGwithinMargin + margin.bottom / 2.0,
    };
    const dataRects = [xScale(data), heightSVGwithinMargin / 2.0, widthSVGwithinMargin, heightSVGwithinMargin * 0.2, ticksY];
    return [dataRects, positionTicks];
  }, [data, dimensions, marginPercentage, ticks]);

  return (
    <TooltipProvider delay={300}>
      <div className={className || ''}>
        <svg ref={svgRef} width="100%" height="100%">
          <g ref={groupMarginRef}>
            {dataRects && dataRects.length > 0 && (
              <>
                <line
                  x1={0}
                  x2={dataRects[2]}
                  y1={dataRects[1]}
                  y2={dataRects[1]}
                  stroke={configTicks.stroke}
                  strokeWidth={configTicks.strokeWidth}
                />
                <line x1={0} x2={0} y1={0} y2={dataRects[1] * 2} stroke={configTicks.stroke} strokeWidth={configTicks.strokeWidth} />
                <line
                  x1={dataRects[2]}
                  x2={dataRects[2]}
                  y1={0}
                  y2={dataRects[1] * 2}
                  stroke={configTicks.stroke}
                  strokeWidth={configTicks.strokeWidth}
                />
                <Tooltip>
                  <TooltipTrigger asChild>
                    <circle
                      cx={dataRects[0]}
                      cy={dataRects[1]}
                      r={dataRects[3]}
                      stroke={'hsl(var(--clr-acc))'}
                      className={'hover:stroke-accent'}
                      fill={'hsl(var(--clr-acc))'}
                    />
                  </TooltipTrigger>
                  <TooltipContent>
                    <div>
                      <strong>Value:</strong> {data}
                      <br />
                    </div>
                  </TooltipContent>
                </Tooltip>
              </>
            )}
          </g>
          {ticks && positionTicks && (
            <>
              <text
                x={positionTicks.xTicksLeft}
                y={positionTicks.yTicks}
                fill={configTicks.stroke}
                fontSize={configTicks.fontText}
                textAnchor={configTicks.textAnchor}
                dominantBaseline={'middle'}
              >
                0
              </text>
              <text
                x={positionTicks.xTicksRight}
                y={positionTicks.yTicks}
                fill={configTicks.stroke}
                fontSize={configTicks.fontText}
                textAnchor={configTicks.textAnchor}
                dominantBaseline={'middle'}
              >
                {maxValue}
              </text>
            </>
          )}
        </svg>
      </div>
    </TooltipProvider>
  );
};
