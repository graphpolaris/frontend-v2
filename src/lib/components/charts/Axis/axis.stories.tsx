// BarPlot.stories.tsx
import React from 'react';
import { Meta } from '@storybook/react';
import AxisComponent from '.';
import { scaleLinear } from 'd3';

export default {
  title: 'Visual charts/Charts/Axis',
  component: AxisComponent,
  decorators: [
    (Story: any) => (
      <div className="w-full h-full flex flex-row justify-center flex-grow">
        <svg className="border border-secondary-300" width="300" height="200">
          <Story />
        </svg>
      </div>
    ),
  ],
} as Meta;

export const CategoricalData = {
  args: {
    scale: scaleLinear().domain([0, 10]).range([200, 0]),
    orientation: 'bottom',
    ticks: 2,
    type: 'categorical',
  },
};
