import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';

export type AxisComponentProps = {
  scale: d3.AxisScale<number | { valueOf(): number }>;
  orientation: 'left' | 'bottom';
  ticks?: number;
  type: string;
  range: number[];
};

const AxisComponent: React.FC<AxisComponentProps> = ({ scale, orientation, ticks, type, range }) => {
  const yAxis1Ref = useRef<SVGGElement>(null);
  //const yAxis2Ref = useRef<SVGGElement>(null);
  const groupRef = useRef<SVGGElement>(null);

  useEffect(() => {
    if (!yAxis1Ref.current || !groupRef.current) return;

    //const axisElement = d3.select(axisRef.current);
    const yAxis1Element = d3.select(yAxis1Ref.current);
    //const yAxis2Element = d3.select(yAxis2Ref.current);

    const groupElement = d3.select(groupRef.current);

    //const axisGenerator = orientation === 'left' ? d3.axisLeft(scale) : orientation === 'bottom' ? d3.axisBottom(scale) : null;

    // Append the axes to their respective containers
    if (type === 'categorical') {
      const yAxis1 = d3.axisBottom(scale); //.tickFormat(d3.format('d')); // to show 0 without decimals

      yAxis1Element.call(yAxis1);
    } else {
      const xAxis = d3
        .axisBottom(scale)
        .tickValues([Math.round((range[0] + range[1]) / 2.0), range[1]])
        .tickFormat(d3.format('.2s'));

      yAxis1Element.call(xAxis);
    }

    // Style axis elements
    groupElement.selectAll('.domain').style('stroke', 'hsl(var(--clr-sec--400))');
    groupElement.selectAll('.tick line').style('stroke', 'hsl(var(--clr-sec--400))');
    groupElement.selectAll('.tick text').attr('class', 'font-data').style('stroke', 'none').style('fill', 'hsl(var(--clr-sec--500))');

    // Call axis generator if available
  }, []);

  return (
    <g ref={groupRef}>
      <g ref={yAxis1Ref} />
    </g>
  );
};

export default AxisComponent;
