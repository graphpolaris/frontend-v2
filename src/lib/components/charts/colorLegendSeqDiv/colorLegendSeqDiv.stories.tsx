import React from 'react';
import { Meta } from '@storybook/react';

import { ColorLegendSeqDiv } from '.';

export default {
  title: 'Visual charts/Charts/ColorLegend',
  component: ColorLegendSeqDiv,
  decorators: [story => <div style={{ margin: '0 auto', width: '600px', height: '300px' }}>{story()}</div>],
} as Meta<typeof ColorLegendSeqDiv>;

export const Default = {
  args: {
    colors: [
      '220deg 80% 98%',
      '220deg 71% 96%',
      '220deg 95% 92%',
      '220deg 92% 85%',
      '220deg 94% 75%',
      '220deg 92% 67%',
      '220deg 84% 58%',
      '220deg 79% 49%',
      '220deg 86% 36%',
      '220deg 80% 23%',
      '220deg 84% 17%',
      '220deg 61% 13%',
    ],
    data: { min: 0, max: 100 },
    tickCount: 5,
    name: 'seq:blue',
  },
};

/*
export const SequentialBlues = Template.bind({});
Default.args = {
  colors: divergenceColors.blueRed,
  data: { min: 0, max: 100 },
  tickCount: 5,
  name: 'div:blue',
};

export const CustomColors = Template.bind({});
CustomColors.args = {
  colors: divergenceColors.blueRed,
  data: { min: 0, max: 100 },
  tickCount: 5,
  name: 'custom-colors',
};
*/
