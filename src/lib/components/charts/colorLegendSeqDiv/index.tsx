import { axisBottom, scaleLinear, select } from 'd3';
import React, { useEffect, useRef } from 'react';

export type ColorLegendSeqDivProps = {
  colors: string[];
  data: { min: number; max: number };
  name: string;
  tickCount?: number; // Optional prop for specifying tick count
};

export const ColorLegendSeqDiv = ({ colors, data, tickCount = 5, name }: ColorLegendSeqDivProps) => {
  const svgRef = useRef<SVGSVGElement | null>(null);
  useEffect(() => {
    if (!svgRef.current) return;

    const widthSVG: number = +svgRef.current.clientWidth;
    const heightSVG: number = +svgRef.current.clientHeight;

    // Parse HSL strings
    const hslValues = colors.map(hslString => {
      const matches = hslString.match(/(\d+)deg (\d+)% (\d+)%/);
      if (matches && matches.length === 4) {
        const [h, s, l] = matches.slice(1).map(Number);
        return `hsl(${h}, ${s}%, ${l}%)`;
      } else {
        return null;
      }
    });

    // Set up SVG container
    const svg = select(svgRef.current);
    svg.selectAll('*').remove(); // Clear previous content

    const marginPercentage = { top: 0.15, right: 0.1, bottom: 0.15, left: 0.1 };
    const margin = {
      top: marginPercentage.top * heightSVG,
      right: marginPercentage.right * widthSVG,
      bottom: marginPercentage.bottom * heightSVG,
      left: marginPercentage.left * widthSVG,
    };

    const groupMargin = svg.append('g').attr('transform', `translate(${margin.left},${margin.top})`);
    const widthSVGwithinMargin: number = widthSVG - margin.left - margin.right;
    const heightSVGwithinMargin: number = heightSVG - margin.top - margin.bottom;

    // Create a gradient
    const gradient = groupMargin
      .append('defs')
      .append('linearGradient')
      .attr('id', `clrGradient_${name}`)
      .attr('x1', '0%')
      .attr('y1', '0%')
      .attr('x2', '100%')
      .attr('y2', '0%');

    // Add color stops to the gradient
    for (let i = 0; i < hslValues.length; i++) {
      gradient
        .append('stop')
        .attr('offset', `${(i / (hslValues.length - 1)) * 100}%`)
        .style('stop-color', hslValues[i] as string);
    }

    groupMargin
      .append('rect')
      .attr('width', widthSVGwithinMargin)
      .attr('height', heightSVGwithinMargin)
      .style('stroke', 'black')
      .style('fill', `url(#clrGradient_${name})`);

    const xScale = scaleLinear().domain([data.min, data.max]).range([0, widthSVGwithinMargin]);

    const xAxis = axisBottom(xScale).ticks(tickCount);

    groupMargin.append('g').attr('class', 'x-axis').attr('transform', `translate(0, ${heightSVGwithinMargin})`).call(xAxis);

    svg.selectAll('.tick text').attr('class', 'font-data text-primary font-semibold').style('stroke', 'none');
  }, [colors, data, tickCount]);

  //return <svg ref={svgRef} className="container" width="100%" height="100%" />;
  return (
    <div className="svg">
      <svg ref={svgRef} className="container" width="100%" height="100%"></svg>
    </div>
  );
};

export default ColorLegendSeqDiv;
