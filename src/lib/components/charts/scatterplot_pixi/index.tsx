import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';
import * as PIXI from 'pixi.js';

export interface ScatterplotProps {
  data: regionData;
  visualConfig: VisualRegionConfig;
  xScale: d3.ScaleLinear<number, number>;
  onBrushUpdate: (idElements: string[]) => void;
  onResultJitter: (data: DataPoint[]) => void;
}

interface DataPointXY {
  x: number;
  y: number;
  radius: number;
  gfx: PIXI.Graphics;
}

export interface DataPoint {
  x: number;
  y: number;
  id: string;
}

export interface VisualRegionConfig {
  marginPercentage: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  margin: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  width: number;
  height: number;
  widthMargin: number;
  heightMargin: number;
}

export interface regionData {
  name: string;
  placementMethod: string;
  xData: number[];
  yData: number[];
  idData: string[];
  colorNodes: string;
  titleRegion: string;
  xAxisName: string;
  label: string;
}

const Scatterplot: React.FC<ScatterplotProps> = ({ data, visualConfig, xScale, onBrushUpdate, onResultJitter }) => {
  const svgRef = useRef(null);
  const brushRef = useRef<SVGGElement>(null as any);

  const pixiContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (!pixiContainerRef.current) return;

    const brushGroup = d3.select(brushRef.current);
    brushGroup.attr('transform', `translate(${visualConfig.margin.left},${visualConfig.margin.top})`).attr('class', 'brushingElem');

    const yScale = d3.scaleLinear().domain([-1, 1]).range([visualConfig.heightMargin, 0]);

    // PIXI
    const app = new PIXI.Application({
      width: visualConfig.width,
      height: visualConfig.height,
      backgroundColor: 0xffffff,
      antialias: true,
    });
    const scatterContainer = new PIXI.Container();
    app.stage.addChild(scatterContainer);

    const dataPoints = data.xData;
    const numPoints = data.xData.length;
    const dataYvalue = 0.0;
    const maxComputations = 10;
    let tickCount = 0;
    const radius = numPoints >= 170 ? visualConfig.width * 0.0042 : visualConfig.width * 0.007;

    const dataCircles: DataPointXY[] = dataPoints.map(value => ({
      x: value,
      y: 0.0,
      radius,
      gfx: new PIXI.Graphics(),
    }));

    dataCircles.forEach(d => {
      const graphics = new PIXI.Graphics();
      graphics.beginFill(0x0000ff);
      graphics.drawCircle(xScale(d.x) + visualConfig.margin.left, yScale(d.y) + visualConfig.margin.top, radius);
      graphics.endFill();
      scatterContainer.addChild(graphics);
      d.gfx = graphics;
    });

    const svg = d3
      .select(svgRef.current)
      .attr('width', visualConfig.width)
      .attr('height', visualConfig.height)
      .style('position', 'absolute')
      .style('top', 0)
      .style('left', 0);

    const groupMargin = svg.append('g').attr('transform', `translate(${visualConfig.margin.left},${visualConfig.margin.top})`);

    svg
      .append('text')
      .attr('x', 1.1 * visualConfig.margin.left)
      .attr('y', visualConfig.margin.top)
      .text(data.titleRegion)
      .attr('class', 'font-data text-primary font-semibold');

    svg
      .append('text')
      .attr('x', visualConfig.widthMargin + visualConfig.margin.right)
      .attr('y', visualConfig.heightMargin + 1.75 * visualConfig.margin.bottom)
      .text(data.xAxisName)
      .attr('class', 'font-data text-primary font-semibold');

    const simulation = d3
      .forceSimulation<DataPointXY>(dataCircles)
      .force('x', d3.forceX(d => xScale(d.x!)).strength(4))
      .force('y', d3.forceY(d => yScale(0.0)).strength(0.1))
      .force('collide', d3.forceCollide(radius * 1.25).strength(0.5));

    const ticked = () => {
      dataCircles.forEach(node => {
        const { x, y, gfx } = node;
        gfx.x = node.x;
        gfx.y = node.y;
      });

      tickCount++;

      if (tickCount > maxComputations) {
        simulation.stop();

        const dataSimulation: DataPoint[] = dataCircles.map(({ x, y }, i) => ({
          x: x - visualConfig.margin.left,
          y: y - visualConfig.margin.top,
          id: data.idData[i],
        }));

        onResultJitter(dataSimulation);
      }
    };

    simulation.on('tick', ticked);

    pixiContainerRef.current.appendChild(app.view as unknown as Node);

    const xAxis = d3.axisBottom(xScale);
    const yAxis = d3.axisLeft(yScale);

    groupMargin
      .append('g')
      .attr('transform', 'translate(0,' + visualConfig.heightMargin + ')')
      .call(xAxis);

    svg.selectAll('.tick text').attr('class', 'font-data text-primary font-semibold').style('stroke', 'none');

    // BRUSH LOGIC
    const brush = d3
      .brush()
      .extent([
        [0, 0],
        [visualConfig.widthMargin, visualConfig.heightMargin],
      ])
      .on('brush', brushed);

    let selectedDataIds: string[] = [];

    function brushed(event: any) {
      if (event.selection) {
        const [[x0, y0], [x1, y1]] = event.selection;

        dataPoints.forEach((d, i) => {
          const x = xScale(d)!;
          const y = yScale(dataYvalue)!;

          if (x >= x0 && x <= x1 && y >= y0 && y <= y1) {
            selectedDataIds.push(data.idData[i]);
          }
        });

        onBrushUpdate(selectedDataIds);
        selectedDataIds = [];
      }
    }

    brushGroup.call(brush);

    return () => {
      app.destroy(true);
    };
  }, [data, visualConfig, xScale]);

  return (
    <div className="h-full w-full" style={{ position: 'relative' }}>
      <div ref={pixiContainerRef}></div>
      <svg ref={svgRef} className="h-full w-full">
        <g ref={brushRef} />
      </svg>
    </div>
  );
};

export default Scatterplot;
