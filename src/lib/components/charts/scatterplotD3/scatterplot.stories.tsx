import React from 'react';
import { Meta } from '@storybook/react';
import Scatterplot, { VisualRegionConfig, regionData, DataPoint } from '.';
import { scaleLinear } from 'd3';

const Component: Meta<typeof Scatterplot> = {
  title: 'Visual charts/Charts/ScatterplotD3',
  component: Scatterplot,
  decorators: [story => <div style={{ width: '100%', height: '100vh' }}>{story()}</div>],
};

export default Component;

const dummyData: regionData = {
  name: 'Sample Data',
  placementMethod: 'Random',
  xData: [1, 2, 3, 3, 2.5, 4, 5, 5.5],
  yData: [0.5, -0.5, 0, 0.7, -0.3],
  idData: ['id1', 'id2', 'id3', 'id4', 'id5'],
  colorNodes: '#3498db',
  titleRegion: 'Scatterplot Example',
  xAxisName: 'X-Axis',
  label: 'Data Points',
};

const dummyData2: regionData = {
  name: 'Sample Data',
  placementMethod: 'Random',
  xData: [43, 36, 49, 46, 54, 54, 43, 33, 28, 39, 51, 43, 44, 32, 49, 53, 43, 40, 35, 41, 30, 32, 41, 31, 43, 44, 35, 34, 56, 31],
  yData: [0.5, -0.5, 0, 0.7, -0.3],
  idData: ['id1', 'id2', 'id3', 'id4', 'id5'],
  colorNodes: '#3498db',
  titleRegion: 'Scatterplot Example',
  xAxisName: 'X-Axis',
  label: 'Data Points',
};

const dummyData3: regionData = {
  name: 'Sample Data',
  placementMethod: 'Random',
  xData: [
    8.1, 7.3, 8, 6.8, 7.8, 7.1, 7.8, 8.5, 8.6, 8.3, 8.2, 8.1, 8.6, 8.4, 7.8, 8, 7.6, 6.1, 7.6, 8.3, 7.4, 7.7, 7.7, 7.3, 8.3, 7.2, 7.8, 7,
    7.2, 8, 7.8, 8.1, 7.9, 7, 8.3, 7.2, 8.3, 7.8, 8, 7.3, 6, 7, 7.4, 7.5, 6.3, 5.5, 7.6, 7.8, 7.1, 7.9, 7.8, 7.9, 7.3, 7.3, 6.6, 6.9, 7,
    7.3, 7.1, 7.3, 6.7, 7.1, 6.6, 6.6, 7.1, 7.2, 7.1, 7.2, 6.8, 6.7, 8, 8.6, 6.9, 7.9, 6.4, 7.9, 7.7, 8.1, 7.2, 7, 7.8, 6.9, 6.5, 7.4, 7.4,
    7.2, 6.2, 5.7, 0, 7.6, 7.5, 8.4, 7.7, 6.6, 6.8, 7.8, 6.3, 7.7, 7.6, 7.7, 8, 7.2, 7.7, 6.6, 8.1, 6.9, 6.8, 6.9, 7.7, 8.1, 7.9, 7.8, 7.8,
    8.3, 8.4, 8.4, 7.9, 7.4, 7.4, 7.9, 7.8, 7.5, 8.1, 7.7, 7.6, 8.2, 8.2, 7.1, 6, 8, 7.1, 7.2, 7.4, 7.3, 7.2, 8, 7.2, 7.4, 6.6, 7.6, 7.6,
    6.5, 4.1, 4.1, 7.2, 7.9, 8, 7.5, 6.9, 5, 8.4, 6.3, 6.7, 7, 7.1, 6.9, 6.1, 8.1, 7, 6.9, 7.2, 7.2, 7.8, 8, 7.3, 7.8, 8, 7.8, 8.2, 7.9,
    7.4,
  ],
  yData: [0.5, -0.5, 0, 0.7, -0.3],
  idData: ['id1', 'id2', 'id3', 'id4', 'id5'],
  colorNodes: '#3498db',
  titleRegion: 'Scatterplot Example',
  xAxisName: 'X-Axis',
  label: 'Data Points',
};

const xExtent: [number, number] = [0, 6];

const xExtent2: [number, number] = [22, 68];

const xExtent3: [number, number] = [0, 10];

// Dummy visual configuration
const configVisualRegion: VisualRegionConfig = {
  marginPercentage: { top: 0.15, right: 0.15, bottom: 0.15, left: 0.15 },
  margin: { top: 0.0, right: 0.0, bottom: 0.0, left: 0.0 },
  width: 700,
  height: 300,
  widthMargin: 0.0,
  heightMargin: 0.0,
};

configVisualRegion.margin = {
  top: configVisualRegion.marginPercentage.top * configVisualRegion.height,
  right: configVisualRegion.marginPercentage.right * configVisualRegion.width,
  bottom: configVisualRegion.marginPercentage.bottom * configVisualRegion.height,
  left: configVisualRegion.marginPercentage.left * configVisualRegion.width,
};

configVisualRegion.widthMargin = configVisualRegion.width - configVisualRegion.margin.right - configVisualRegion.margin.left;
configVisualRegion.heightMargin = configVisualRegion.height - configVisualRegion.margin.top - configVisualRegion.margin.bottom;

// Dummy xScale function
const xScaleShared = scaleLinear()
  .domain(xExtent as [number, number])
  .range([0, configVisualRegion.widthMargin]);

const xScaleShared2 = scaleLinear()
  .domain(xExtent2 as [number, number])
  .range([0, configVisualRegion.widthMargin]);

const xScaleShared3 = scaleLinear()
  .domain(xExtent3 as [number, number])
  .range([0, configVisualRegion.widthMargin]);

// Dummy event handlers
const handleBrushUpdate = (selectedIds: string[]) => {
  console.log('Brush Updated:', selectedIds);
};

const handleResultJitter = (jitteredData: DataPoint[]) => {
  console.log('Result Jitter:', jitteredData);
};

// Set initial args for the story
export const BasicScatterplot = {
  args: {
    data: dummyData,
    visualConfig: configVisualRegion,
    xScale: xScaleShared,
    onBrushUpdate: handleBrushUpdate,
    onResultJitter: handleResultJitter,
  },
};

// Set initial args for the story
export const RealData1 = {
  args: {
    data: dummyData2,
    visualConfig: configVisualRegion,
    xScale: xScaleShared2,
    onBrushUpdate: handleBrushUpdate,
    onResultJitter: handleResultJitter,
  },
};

// Set initial args for the story
export const RealData2 = {
  args: {
    data: dummyData3,
    visualConfig: configVisualRegion,
    xScale: xScaleShared3,
    onBrushUpdate: handleBrushUpdate,
    onResultJitter: handleResultJitter,
  },
};
