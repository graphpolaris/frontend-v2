import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';

export interface ScatterplotProps {
  data: regionData;
  visualConfig: VisualRegionConfig;
  xScale: d3.ScaleLinear<number, number>;
  onBrushUpdate: (idElements: string[]) => void;
  onResultJitter: (data: DataPoint[]) => void;
}

export interface DataPoint {
  x: number;
  y: number;
  id: string;
}
export interface DataPointXY {
  x: number;
  y: number;
}

export interface VisualRegionConfig {
  marginPercentage: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  margin: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  width: number;
  height: number;
  widthMargin: number;
  heightMargin: number;
}

export interface regionData {
  name: string;
  placementMethod: string;
  xData: number[];
  yData: number[];
  idData: string[];
  colorNodes: string;
  titleRegion: string;
  xAxisName: string;
  label: string;
}

const Scatterplot: React.FC<ScatterplotProps> = ({ data, visualConfig, xScale, onBrushUpdate, onResultJitter }) => {
  const svgRef = useRef(null);
  const brushRef = useRef<SVGGElement>(null as any);

  useEffect(() => {
    const svg = d3.select(svgRef.current);

    const groupMargin = svg.append('g').attr('transform', `translate(${visualConfig.margin.left},${visualConfig.margin.top})`);

    const brushGroup = d3.select(brushRef.current);
    brushGroup.attr('transform', `translate(${visualConfig.margin.left},${visualConfig.margin.top})`).attr('class', 'brushingElem');

    const yScale = d3.scaleLinear().domain([-1, 1]).range([visualConfig.heightMargin, 0]);

    const dataPoints = data.xData;
    const dataYvalue = 0.0;
    const maxComputations = 200;
    let tickCount = 0;
    const radius = visualConfig.width * 0.007;
    const dataCircles: DataPointXY[] = dataPoints.map(value => ({ x: value, y: 0 }));

    const simulation = d3
      .forceSimulation<DataPointXY>(dataCircles)
      .force('x', d3.forceX(d => xScale(d.x!)).strength(4))
      .force('y', d3.forceY(d => yScale(0.0)).strength(0.1))
      .force('collide', d3.forceCollide(radius * 1.25).strength(0.5));

    const circles = groupMargin
      .selectAll('circle')
      .data(dataCircles)
      .enter()
      .append('circle')
      .attr('class', (d, i) => `${data.idData[i]}`)
      .attr('cx', (d, i) => xScale(d.x))
      .attr('cy', d => yScale(d.y))
      .attr('r', radius)
      .attr('stroke', 'gray')
      .attr('fill', data.colorNodes);

    simulation.on('tick', function () {
      circles.attr('cx', (d, i) => d.x as number).attr('cy', d => d.y as number);

      tickCount++;
      if (tickCount > maxComputations) {
        const dataSimulation: DataPoint[] = dataCircles.map(({ x, y }, i) => ({
          x,
          y,
          id: data.idData[i],
        }));

        onResultJitter(dataSimulation);

        simulation.stop();
      }
    });

    const xAxis = d3.axisBottom(xScale);
    const yAxis = d3.axisLeft(yScale);

    groupMargin
      .append('g')
      .attr('transform', 'translate(0,' + visualConfig.heightMargin + ')')
      .call(xAxis);

    svg.selectAll('.tick text').attr('class', 'font-data text-primary').style('stroke', 'none');
    svg
      .append('text')
      .attr('x', visualConfig.widthMargin + visualConfig.margin.right)
      .attr('y', visualConfig.heightMargin + 1.75 * visualConfig.margin.bottom)
      .text(data.xAxisName)
      .attr('class', 'font-data text-primary font-semibold');

    groupMargin
      .append('rect')
      .attr('x', 0.0)
      .attr('y', 0.0)
      .attr('width', visualConfig.widthMargin)
      .attr('height', visualConfig.heightMargin)
      .attr('rx', 0)
      .attr('ry', 0)
      .attr('class', 'fill-none stroke-secondary');

    svg
      .append('rect')
      .attr('x', 0.0)
      .attr('y', 0.0)
      .attr('width', visualConfig.width)
      .attr('height', visualConfig.width)
      .attr('rx', 0)
      .attr('ry', 0)
      .attr('class', 'fill-none stroke-secondary');

    svg
      .append('rect')
      .attr('x', 0.0)
      .attr('y', 0.0)
      .attr('width', visualConfig.width)
      .attr('height', visualConfig.width * 1.4)
      .attr('rx', 0)
      .attr('ry', 0)
      .attr('class', 'fill-none stroke-secondary');

    // BRUSH LOGIC
    const brush = d3
      .brush()
      .extent([
        [0, 0],
        [visualConfig.widthMargin, visualConfig.heightMargin],
      ])
      .on('brush', brushed);

    let selectedDataIds: string[] = [];

    function brushed(event: any) {
      if (event.selection) {
        const [[x0, y0], [x1, y1]] = event.selection;

        dataPoints.forEach((d, i) => {
          const x = xScale(d)!;
          const y = yScale(dataYvalue)!;

          if (x >= x0 && x <= x1 && y >= y0 && y <= y1) {
            selectedDataIds.push(data.idData[i]);
          }
        });

        onBrushUpdate(selectedDataIds);
        selectedDataIds = [];
      }
    }

    brushGroup.call(brush);
  }, [data, visualConfig, xScale]);

  return (
    <div className="w-full">
      <div
        className="absolute border-light group bg-secondary-200 text-left overflow-x-hidden truncate capitalize"
        style={{ width: visualConfig.width }}
      >
        <p className="mx-2 w-full">{data.titleRegion}</p>
      </div>
      <svg ref={svgRef} width={visualConfig.width} height={visualConfig.height}>
        <g ref={brushRef} />
      </svg>
    </div>
  );
};

export default Scatterplot;
