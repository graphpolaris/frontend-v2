// BarPlot.stories.tsx
import { Meta } from '@storybook/react';
import { Heatmap1D } from '.';

const Component: Meta<typeof Heatmap1D> = {
  title: 'Visual charts/Charts/Heatmap1D',
  component: Heatmap1D,
  decorators: [story => <div style={{ width: '100%', height: '100%' }}>{story()}</div>],
};

export default Component;

export const Data1 = {
  args: {
    data: [0.1, 0.2, 0.1, 0.4, 0.02, 0.01, 0.0, 0.0, 0.015, 0.1],
    numBins: 40,
    maxValue: 0.5,
    marginPercentage: { top: 0.1, right: 0.05, left: 0.05, bottom: 0.1 },
    ticks: true,
    className: 'border border',
  },
};

export const Data2 = {
  args: {
    data: [0.1, 0.2, 0.1, 0.4, 0.02, 0.01, 0.0],
    numBins: 40,
    maxValue: 0.5,
    marginPercentage: { top: 0.1, right: 0, left: 0, bottom: 0 },
    ticks: true,
  },
};
