import { useEffect, useMemo, useRef, useState } from 'react';
import { Tooltip, TooltipTrigger, TooltipContent, TooltipProvider } from '@/lib/components/tooltip';
import { bin, extent, range, select, scaleQuantize } from 'd3';
import { visualizationColors } from 'ts-common';

export type heatmap1DProps = {
  data: number[];
  numBins: number;
  maxValue: number;
  marginPercentage?: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  className?: string;
  strokeWidth?: number;
  ticks?: boolean;
};

export const Heatmap1D = ({ numBins, maxValue, data, marginPercentage, className, ticks }: heatmap1DProps) => {
  const svgRef = useRef<SVGSVGElement | null>(null);
  const groupMarginRef = useRef<SVGGElement | null>(null);

  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });

  useEffect(() => {
    function handleResize() {
      if (svgRef.current) {
        setDimensions({
          width: svgRef.current.getBoundingClientRect().width,
          height: svgRef.current.getBoundingClientRect().height,
        });
      }
    }

    window.addEventListener('resize', handleResize);
    if (svgRef.current) new ResizeObserver(handleResize).observe(svgRef.current);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const configTicks = {
    stroke: 'hsl(var(--clr-sec--500))',
    strokeWidth: '2',
    fontText: 10,
    textAnchor: 'middle',
    ticksVerticalCutoff: 0.85,
  };

  const [dataRects, positionTicks] = useMemo(() => {
    if (!svgRef.current || dimensions.width === 0 || dimensions.height === 0) return [];
    const { width, height } = dimensions;
    const widthSVG: number = width;
    const heightSVG: number = height;

    const colorArray = visualizationColors.GPSeq.colors[9];

    if (!marginPercentage)
      marginPercentage = {
        top: 0.19,
        right: 0.02,
        bottom: 0.19,
        left: 0.19,
      };
    const margin = {
      top: marginPercentage.top * heightSVG,
      right: marginPercentage.right * widthSVG,
      bottom: marginPercentage.bottom * heightSVG,
      left: marginPercentage.left * widthSVG,
    };
    select(groupMarginRef.current).attr('transform', `translate(${margin.left},${margin.top})`);

    const widthSVGwithinMargin = widthSVG - margin.left - margin.right;
    const heightSVGwithinMargin = heightSVG - margin.top - margin.bottom;

    const extentData = extent(data);
    const [min] = extentData as [number, number];

    const rangeTH: number[] = range(min, maxValue, (maxValue - min) / numBins);
    if (rangeTH.length !== numBins) {
      rangeTH.pop();
    }

    const histogram = bin()
      .value(d => d)
      .domain([min, maxValue])
      .thresholds(rangeTH);

    const bins = histogram(data);

    const extentBins = extent(bins, d => d.length) as [number, number];

    const colorScale = scaleQuantize<string>().domain([0, extentBins[1]]).range(colorArray);
    const widthRect = widthSVGwithinMargin / numBins;
    const positionTicks = {
      xTicksLeft: margin.left / 2.0,
      xTicksRight: margin.left + widthSVGwithinMargin + margin.right / 2.0,
      yTicks: heightSVGwithinMargin + margin.bottom / 2.0,
    };

    const positionRects = bins.map((d, i) => {
      const x0 = d.x0 || 0;
      const x1 = d.x1 || 0;
      parseFloat(x1.toFixed(2));
      return {
        x: widthRect * i,
        y: 0.0,
        width: widthRect,
        height: heightSVGwithinMargin,
        category: `[${parseFloat(x0.toFixed(2))},${parseFloat(x1.toFixed(2))}]`,
        count: d.length,
        fill: colorScale(d.length),
        stokeWidth: 0.0,
        key: `${i}`,
      };
    });
    return [positionRects, positionTicks];
  }, [data, dimensions, marginPercentage, numBins, ticks]);

  return (
    <TooltipProvider delay={300}>
      <div className={className || ''}>
        <svg ref={svgRef} width="100%" height="100%">
          <g ref={groupMarginRef}>
            {dataRects?.map(d => (
              <Tooltip key={d.key}>
                <TooltipTrigger asChild>
                  <rect
                    x={d.x}
                    y={d.y}
                    width={d.width}
                    height={d.height}
                    stroke={'hsl(var(--clr-sec--200))'}
                    strokeWidth={'0.05'}
                    className={'hover:stroke-accent'}
                    fill={d.fill}
                  />
                </TooltipTrigger>
                <TooltipContent>
                  <div>
                    <strong>Range:</strong> {d.category}
                    <br />
                    <strong>Count:</strong> {d.count}
                  </div>
                </TooltipContent>
              </Tooltip>
            ))}
          </g>
          {ticks && positionTicks && (
            <>
              <text
                x={positionTicks.xTicksLeft}
                y={positionTicks.yTicks}
                fill={configTicks.stroke}
                fontSize={configTicks.fontText}
                textAnchor={configTicks.textAnchor}
                dominantBaseline={'middle'}
              >
                0
              </text>
              <text
                x={positionTicks.xTicksRight}
                y={positionTicks.yTicks}
                fill={configTicks.stroke}
                fontSize={configTicks.fontText}
                textAnchor={configTicks.textAnchor}
                dominantBaseline={'middle'}
              >
                {maxValue}
              </text>
            </>
          )}
        </svg>
      </div>
    </TooltipProvider>
  );
};
