import React from 'react';
import { Meta } from '@storybook/react';
import ColorLegendCat from '.';

export default {
  title: 'Visual charts/Charts/ColorLegendCat',
  component: ColorLegendCat,
  decorators: [story => <div style={{ margin: '0 auto', width: '200px', height: '300px' }}>{story()}</div>],
} as Meta<typeof ColorLegendCat>;

export const Default = {
  args: {
    items: [
      { text: 'Category A', color: 'red' },
      { text: 'Category B', color: 'blue' },
      { text: 'Category C', color: 'green' },
    ],
  },
};
