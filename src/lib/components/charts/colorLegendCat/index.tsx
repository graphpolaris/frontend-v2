import React from 'react';

interface LegendItem {
  text: string;
  color: string;
}

export interface LegendProps {
  items: LegendItem[];
}

const ColorLegendCat: React.FC<LegendProps> = ({ items }) => {
  return (
    <div className="flex flex-col items-start space-y-2 font-data bg-light p-4 rounded-lg shadow-md overflow-x-hidden truncate">
      {items.map((item, index) => (
        <div key={index} className="flex items-center space-x-2">
          <div className="w-4 h-4 rounded-full" style={{ backgroundColor: item.color }}></div>
          <span>{item.text}</span>
        </div>
      ))}
    </div>
  );
};

export default ColorLegendCat;
