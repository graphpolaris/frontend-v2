// BarPlot.stories.tsx
import React from 'react';
import { Meta } from '@storybook/react';
import { BarPlot } from '.';

const Component: Meta<typeof BarPlot> = {
  title: 'Visual charts/Charts/BarPlot',
  component: BarPlot,
  decorators: [story => <div style={{ width: '100%', height: '100vh' }}>{story()}</div>],
};

export default Component;

export const CategoricalData = {
  args: {
    data: [
      { category: 'Category A', count: 250 },
      { category: 'NoData', count: 100 },
      { category: 'Category B', count: 20 },
      { category: 'Category C', count: 15 },
    ],
    numBins: 5,
    typeBarPlot: 'categorical',
    marginPercentage: { top: 0.1, right: 0, left: 0, bottom: 0 },
    axis: false,
    name: 'mock1',
  },
};

export const NumericalData = {
  args: {
    data: [
      { category: 'Data Point 1', count: 3 },
      { category: 'Data Point 2', count: 7 },
      { category: 'Data Point 3', count: 2 },
      { category: 'Data Point 4', count: 5 },
      { category: 'Data Point 5', count: 8 },
    ],
    numBins: 5,
    marginPercentage: { top: 0.1, right: 0, left: 0, bottom: 0 },
    typeBarPlot: 'numerical',
    axis: false,
    name: 'mock2',
  },
};
