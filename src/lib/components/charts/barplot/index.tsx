import React, { useEffect, useMemo, useRef, useState } from 'react';
import { Tooltip, TooltipTrigger, TooltipContent, TooltipProvider } from '@/lib/components/tooltip';
import { axisLeft, bin, extent, format, max, range, scaleBand, scaleLinear, select } from 'd3';
import { parseValue } from '@/lib/data-access/store/graphQueryResultSlice';

export type BarPlotProps = {
  data: { category: string; count: number }[];
  numBins: number;
  typeBarPlot: 'numerical' | 'categorical';
  marginPercentage?: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  className?: string;
  maxBarsCount?: number;
  strokeWidth?: number;
  axis?: boolean;
  name: string;
};

export const BarPlot = ({ typeBarPlot, numBins, data, marginPercentage, className, maxBarsCount, axis, name }: BarPlotProps) => {
  const svgRef = useRef<SVGSVGElement | null>(null);
  const groupMarginRef = useRef<SVGGElement | null>(null);

  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });

  useEffect(() => {
    function handleResize() {
      if (svgRef.current) {
        setDimensions({
          width: svgRef.current.getBoundingClientRect().width,
          height: svgRef.current.getBoundingClientRect().height,
        });
      }
    }

    window.addEventListener('resize', handleResize);
    if (svgRef.current) new ResizeObserver(handleResize).observe(svgRef.current);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const dataRects = useMemo(() => {
    if (!svgRef.current || dimensions.width === 0 || dimensions.height === 0) return [];
    const { width, height } = dimensions;
    const widthSVG: number = width;
    const heightSVG: number = height;

    const svgPlot = select(svgRef.current);

    if (!marginPercentage)
      marginPercentage = {
        top: 0.19,
        right: 0.02,
        bottom: 0.19,
        left: 0.19,
      };
    const margin = {
      top: marginPercentage.top * heightSVG,
      right: marginPercentage.right * widthSVG,
      bottom: marginPercentage.bottom * heightSVG,
      left: marginPercentage.left * widthSVG,
    };
    select(groupMarginRef.current).attr('transform', `translate(${margin.left},${margin.top})`);
    const widthSVGwithinMargin = widthSVG - margin.left - margin.right;
    const heightSVGwithinMargin = heightSVG - margin.top - margin.bottom;

    if (typeBarPlot === 'categorical') {
      const defs = svgPlot.append('defs');

      defs
        .append('pattern')
        .attr('id', 'diagonalHatch')
        .attr('width', 6)
        .attr('height', 6)
        .attr('patternTransform', 'rotate(45 0 0)')
        .attr('patternUnits', 'userSpaceOnUse')
        .append('rect')
        .attr('width', 2)
        .attr('height', 6)
        .attr('transform', 'translate(0,0)')
        .attr('fill', '#cccccc');

      const dataFiltered = data.filter(item => item.category !== undefined);
      const dataSorted = dataFiltered.sort((a, b) => b.count - a.count);
      const dataTopCounts = dataSorted.filter((item, i) => !maxBarsCount || i < maxBarsCount);
      const maxCount = max(dataTopCounts, d => d.count) ?? 1;
      const xScale = scaleBand()
        .range([0, widthSVGwithinMargin])
        .domain(dataTopCounts.map(d => d.category))
        .padding(0.1);

      const yScale = scaleLinear().domain([0, maxCount]).range([heightSVGwithinMargin, 0]);

      let yAxis2;
      if (maxCount < 10) {
        yAxis2 = axisLeft(yScale).tickValues([maxCount]).tickFormat(format('d'));
      } else {
        yAxis2 = axisLeft(yScale).tickValues([maxCount]).tickFormat(format('.2s'));
      }

      const scaledData = dataTopCounts.map((d, i) => ({
        x: xScale(d.category) || 0,
        y: yScale(d.count),
        width: xScale.bandwidth(),
        height: heightSVGwithinMargin - yScale(d.count),
        category: d.category,
        count: d.count,
        key: `${name}-${i}`,
      }));

      return scaledData;
    } else {
      const dataCount = data.map(obj => obj.count);

      const extentData = extent(dataCount);
      const [min, max] = extentData as [number, number];

      const xScale = scaleLinear().range([0, widthSVGwithinMargin]).domain([min, max]);

      const rangeTH: number[] = range(min, max, (max - min) / numBins);
      if (rangeTH.length !== numBins) {
        rangeTH.pop();
      }

      const histogram = bin()
        .value(d => d)
        .domain([min, max])
        .thresholds(rangeTH);

      const bins = histogram(dataCount);
      const extentBins = extent(bins, d => d.length) as [number, number];

      const yScale = scaleLinear().range([heightSVGwithinMargin, 0]).domain([0, extentBins[1]]);

      const scaledData = bins.map((d, i) => {
        const x0 = d.x0 || 0;
        const x1 = d.x1 || 0;
        return {
          x: xScale(x0),
          y: yScale(d.length),
          width: xScale(x1) - xScale(x0),
          height: heightSVGwithinMargin - yScale(d.length),
          category: `[${parseValue(x0)}, ${parseValue(x1)}]`,
          count: d.length,
          key: `${name}-${i}`,
        };
      });

      return scaledData;
    }
  }, [data, dimensions, marginPercentage, numBins, typeBarPlot, axis, name]);

  return (
    <TooltipProvider delay={300}>
      <div className={className || ''}>
        <svg ref={svgRef} width="100%" height="100%">
          <g ref={groupMarginRef}>
            {dataRects?.map(d => (
              <Tooltip key={d.key}>
                <TooltipTrigger asChild>
                  <rect
                    x={d.x}
                    y={d.y}
                    width={d.width}
                    height={d.height}
                    stroke={'hsl(var(--clr-sec--400))'}
                    strokeWidth={d.category === 'NoData' ? '0.2' : '0'}
                    fill={d.category === 'NoData' ? 'url(#diagonalHatch)' : 'hsl(var(--clr-sec--400))'}
                    className={'hover:fill-accent'}
                  />
                </TooltipTrigger>
                <TooltipContent>
                  <div>
                    <strong>Category:</strong> {d.category}
                    <br />
                    <strong>Count:</strong> {d.count}
                  </div>
                </TooltipContent>
              </Tooltip>
            ))}
          </g>
        </svg>
      </div>
    </TooltipProvider>
  );
};
