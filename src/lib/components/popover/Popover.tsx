import React, { useEffect } from 'react';
import {
  useFloating,
  autoUpdate,
  offset,
  flip,
  shift,
  hide,
  arrow,
  useClick,
  useHover,
  useFocus,
  useDismiss,
  useRole,
  useInteractions,
  useMergeRefs,
  FloatingPortal,
  FloatingArrow,
} from '@floating-ui/react';
import type { Placement, Alignment } from '@floating-ui/react';
import { Button } from '@/lib';
import { v4 as uuidv4 } from 'uuid';
import { usePopoverGlobalContext } from '@/lib';

export interface PopoverOptions {
  id?: string;
  initialOpen?: boolean;
  placement?: Placement;
  alignment?: Alignment;
  autoAlignment?: boolean;
  open?: boolean;
  onOpenChange?: (open: boolean) => void;
  boundaryElement?: React.RefObject<HTMLElement> | HTMLElement | null;
  showArrow?: boolean;
  interactive?: boolean;
  delay?: number | { open?: number; close?: number };
  activation?: 'click' | 'hover' | 'focus';
  showCloseButton?: boolean;
}

interface PopoverInstanceContextProps {
  open: boolean;
  setOpen: (state: boolean) => void;
  closePopover: () => void;
  interactions: ReturnType<typeof useInteractions>;
  data: ReturnType<typeof useFloating>;
  interactive: boolean;
  showCloseButton?: boolean;
}
const globalOpenPopovers = new Set<string>();

const PopoverInstanceContext = React.createContext<PopoverInstanceContextProps | null>(null);

// Custom hook to access the instance Popover context
export const usePopoverInstanceContext = (): PopoverInstanceContextProps => {
  const context = React.useContext(PopoverInstanceContext);
  if (!context) {
    throw new Error('Popover components must be wrapped in <Popover>.');
  }
  return context;
};

// Hook to handle popover state
export function usePopover({
  id = uuidv4(),
  initialOpen = false,
  placement = 'top',
  alignment,
  autoAlignment = true,
  open: controlledOpen,
  onOpenChange,
  boundaryElement = null,
  showArrow = true,
  interactive = true,
  delay,
  activation = 'click',
}: PopoverOptions = {}) {
  // Access the global Popover context if available
  const popoverGlobal = usePopoverGlobalContext();
  const [uncontrolledOpen, setUncontrolledOpen] = React.useState(initialOpen);

  const hasInitialized = React.useRef(false);
  // Sync initialOpen when component mounts
  useEffect(() => {
    if (initialOpen && !hasInitialized.current) {
      hasInitialized.current = true;
      setUncontrolledOpen(true);
      if (popoverGlobal) {
        popoverGlobal.openPopover(id);
      } else {
        globalOpenPopovers.add(id);
      }
    }
  }, [initialOpen, popoverGlobal, id]);

  const isManaged = popoverGlobal !== null;
  const open = controlledOpen ?? uncontrolledOpen ?? (isManaged ? popoverGlobal.isOpen(id) : false);

  const setOpen = (state: boolean) => {
    if (isManaged && popoverGlobal) {
      // Popover zit in een provider
      if (state) {
        popoverGlobal.openPopover(id);
      } else {
        popoverGlobal.closePopover(id);
      }
    } else {
      if (state) {
        globalOpenPopovers.clear();
        globalOpenPopovers.add(id);
      } else {
        globalOpenPopovers.delete(id);
      }
    }

    setUncontrolledOpen(state);
    onOpenChange?.(state);
  };

  const arrowRef = React.useRef<SVGSVGElement | null>(null);
  const middleware = [
    offset(5),
    shift({ padding: 5 }),
    flip({
      crossAxis: placement.includes('-'),
      flipAlignment: autoAlignment === false ? false : !!alignment,
      padding: 5,
    }),
  ];

  if (boundaryElement) {
    const boundary = boundaryElement instanceof HTMLElement ? boundaryElement : (boundaryElement?.current ?? undefined);
    if (boundary) {
      middleware.push(hide({ boundary }));
    }
  }

  if (showArrow) middleware.push(arrow({ element: arrowRef }));

  const adjustedPlacement = alignment ? (`${placement}-${alignment}` as Placement) : placement;

  const data = useFloating({
    placement: adjustedPlacement,
    open,
    onOpenChange: setOpen,
    whileElementsMounted: autoUpdate,
    middleware,
  });

  // Attach the arrow ref
  (data.refs as any).arrow = arrowRef;

  const click = useClick(data.context, { enabled: activation === 'click' });
  const hover = useHover(data.context, { enabled: activation === 'hover', delay, move: true, restMs: 50 });
  const focus = useFocus(data.context, { enabled: activation === 'focus' });
  const dismiss = useDismiss(data.context, {
    enabled: activation === 'click',

    outsidePress: (event: MouseEvent) => {
      if (isManaged && popoverGlobal) {
        return !(popoverGlobal.isMultiselect() && event.shiftKey);
      }
      return true;
    },
  });

  const role = useRole(data.context, { role: 'dialog' });

  const interactions = useInteractions([click, hover, focus, dismiss, role]);

  return React.useMemo(
    () => ({
      open,
      setOpen,
      closePopover: () => setOpen(false),
      interactions,
      data,
      interactive,
    }),
    [open, setOpen, interactions, data, interactive],
  );
}

/**
 * The `Popover` component displays contextual information or actions in a floating container.
 * It supports various configurations like placement, alignment, activation method, and options to show an arrow or close button.
 */
export function Popover({
  children,
  autoAlignment = true,
  showCloseButton = false,
  initialOpen = false,
  ...options
}: {
  children: React.ReactNode;
  showCloseButton?: boolean;
} & PopoverOptions) {
  const popover = usePopover({ ...options, initialOpen });

  return (
    <PopoverInstanceContext.Provider
      value={{
        ...popover,
        showCloseButton,
      }}
    >
      {children}
    </PopoverInstanceContext.Provider>
  );
}

// PopoverTrigger component
export const PopoverTrigger = React.forwardRef<HTMLElement, React.HTMLProps<HTMLElement> & { asChild?: boolean; x?: number; y?: number }>(
  function PopoverTrigger({ children, asChild = false, x = null, y = null, ...props }, propRef) {
    const context = usePopoverInstanceContext();
    const ref = useMergeRefs([context.data.refs.setReference, propRef]);

    React.useEffect(() => {
      if (x !== null && y !== null && context.data.refs.reference.current != null) {
        const element = context.data.refs.reference.current as HTMLElement;
        element.style.position = 'absolute';
        const { x: offsetX, y: offsetY } = element.getBoundingClientRect();
        element.getBoundingClientRect = () => {
          return {
            width: 0,
            height: 0,
            x: offsetX,
            y: offsetY,
            top: y + offsetY,
            left: x + offsetX,
            right: x + offsetX,
            bottom: y + offsetY,
          } as DOMRect;
        };
        context.data.update();
      }
    }, [x, y, context.data.refs.reference, context.data.update]);

    // `asChild` allows the user to pass any element as the anchor
    if (asChild && React.isValidElement(children)) {
      return React.cloneElement(
        children,
        context.interactions.getReferenceProps({
          ref,
          ...props,
          ...(children as any).props,
          'data-state': context.open ? 'open' : 'closed',
          onClick: (e: React.MouseEvent) => {
            e.stopPropagation();
            context.setOpen(!context.open);
          },
          // Accessibility attributes
          'aria-expanded': context.open,
          'aria-haspopup': 'dialog',
        }),
      );
    }

    return (
      <div
        ref={ref}
        data-state={context.open ? 'open' : 'closed'}
        {...context.interactions.getReferenceProps({
          ...props,
          onClick: (e: React.MouseEvent) => {
            e.stopPropagation();
            context.setOpen(!context.open);
          },
          // Accessibility attributes
          'aria-expanded': context.open,
          'aria-haspopup': 'dialog',
        })}
      >
        {children}
      </div>
    );
  },
);

// PopoverContent component
export const PopoverContent = React.forwardRef<HTMLDivElement, React.HTMLProps<HTMLDivElement> & { root?: HTMLElement | null }>(
  function PopoverContent({ style, className, ...props }, propRef) {
    const context = usePopoverInstanceContext();
    const ref = useMergeRefs([context.data.refs.setFloating, propRef]);

    React.useEffect(() => {
      if (context.open) {
        context.data.update();
      }
    }, [context.open, context.data]);

    if (!context.open) return null;

    return (
      <FloatingPortal root={props.root}>
        <div
          ref={ref}
          className={`max-w-72 sm:max-w-lg rounded bg-light ring-1 ring-secondary-200 shadow-lg animate-in fade-in-0 zoom-in-95
            ${className ? ` ${className}` : ''}`}
          style={{
            ...context.data.floatingStyles,
            ...style,
            display: context.data.middlewareData.hide?.referenceHidden ? 'none' : 'block',
            pointerEvents: context.interactive ? 'auto' : 'none',
          }}
          {...context.interactions.getFloatingProps(props)}
        >
          {context.showCloseButton && (
            <Button
              onClick={() => context.setOpen(false)}
              variantType="secondary"
              rounded
              className="float-right z-10 m-1"
              variant="ghost"
              size="xs"
              iconComponent="icon-[ic--baseline-close]"
              aria-label="Close popover"
            />
          )}

          {props.children}

          {context.data.middlewareData.arrow ? (
            <FloatingArrow ref={(context.data.refs as any).arrow} context={context.data.context} className="fill-secondary-300" />
          ) : null}
        </div>
      </FloatingPortal>
    );
  },
);
