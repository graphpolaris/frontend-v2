// Popover.stories.tsx
import { Meta, StoryObj } from '@storybook/react';
import { Popover, PopoverTrigger, PopoverContent, PopoverProvider } from '@/lib';

export default {
  title: 'Components/Popover',
  component: Popover,
  tags: ['autodocs'],
  decorators: [Story => <div className="p-10">{Story()}</div>],
  argTypes: {
    placement: {
      control: 'inline-radio',
      options: ['top', 'bottom', 'left', 'right'],
      description: 'Popover placement relative to the trigger',
    },
    alignment: {
      control: 'inline-radio',
      options: [undefined, 'start', 'end'],
      description: 'Popover alignment relative to the trigger',
    },
    activation: {
      control: 'inline-radio',
      options: ['click', 'hover', 'focus'],
      description: 'Defines how the popover is triggered',
    },
    showArrow: {
      control: 'boolean',
      description: 'Show an arrow on the popover',
    },
    showCloseButton: {
      control: 'boolean',
      description: 'Show a close button on the popover',
    },
    delayOpen: {
      control: 'number',
      description: 'Delay (hover/focus trigger only) before the popover opens (ms)',
    },
    delayClose: {
      control: 'number',
      description: 'Delay (hover/focus trigger only) before the popover closes (ms)',
    },
    id: {
      control: { disable: true },
      description: 'Unique identifier to manage multiple popovers, can be set manually else auto-generated',
    },
    boundaryElement: {
      control: { disable: true },
      description: 'Element to which the popover is constrained',
    },
    autoAlignment: {
      control: { disable: true },
      description: 'Automatically align the popover based on the available space',
    },
    initialOpen: {
      control: 'boolean',
      description: 'Initial state of the popover',
    },
    children: { table: { disable: true } },
    boundaryElement: { table: { disable: true } },
    interactive: { table: { disable: true } },
    delay: { table: { disable: true } },
    onOpenChange: { table: { disable: true } },
    open: { table: { disable: true } },
    initialOpen: { table: { disable: true } },
  },
} as Meta<typeof Popover>;

type Story = StoryObj<typeof Popover>;

// Template Function (Independent Popovers)
const DefaultTemplate = (args: any) => (
  <div className="min-h-52 flex flex-col gap-1 items-center justify-center">
    <PopoverInstance {...args} />
  </div>
);

// PopoverInstance Component for Independent Story
const PopoverInstance = ({ placement, alignment, activation, showArrow, delayOpen, delayClose, showCloseButton, initialOpen }: any) => (
  <Popover
    placement={placement}
    activation={activation}
    alignment={alignment}
    showArrow={showArrow}
    showCloseButton={showCloseButton}
    initialOpen={initialOpen}
    {...(activation === 'hover' && { delay: { open: delayOpen, close: delayClose } })}
  >
    <PopoverTrigger>
      <button className="border px-3 py-2 hover:text-primary rounded">
        {activation === 'click' ? 'Click me' : activation === 'hover' ? 'Hover me' : 'Focus me'}
      </button>
    </PopoverTrigger>
    <PopoverContent>
      <h3 className={'p-2 border-b border-secondary-200'}>Popover at {placement}</h3>
      <p className="text-xs text-secondary-500 p-2">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec dui id nunc lacinia eleifend. Cras vestibulum.
      </p>
    </PopoverContent>
  </Popover>
);

// Default Story: Independent Popovers
/**
 * The `Popover` component displays contextual information or actions in a floating container.
 * It supports configurations like placement, alignment, activation method, and options to show an arrow or close button.
 */
export const Default: Story = {
  render: DefaultTemplate,
  args: {
    placement: 'top',
    alignment: undefined,
    activation: 'click',
    showArrow: true,
    showCloseButton: false,
    delayOpen: 500,
    delayClose: 150,
    initialOpen: false,
  },
};

// Template for Managed Story (multiselect)
const ManagedTemplate = (args: any) => {
  return (
    <div>
      <PopoverProvider multiselect>
        <div className="min-h-52 flex flex-col gap-4 items-center justify-center">
          <div className="flex space-x-4">
            <PopoverInstanceManaged {...args} id="popover-1" />
            <PopoverInstanceManaged {...args} id="popover-2" />
          </div>
        </div>
      </PopoverProvider>
    </div>
  );
};

// Managed PopoverInstance Component
const PopoverInstanceManaged = ({ id, placement, alignment, activation, showArrow, delayOpen, delayClose, showCloseButton }: any) => (
  <Popover
    id={id}
    placement={placement}
    activation={activation}
    alignment={alignment}
    showArrow={showArrow}
    showCloseButton={showCloseButton}
    {...(activation === 'hover' && { delay: { open: delayOpen, close: delayClose } })}
  >
    <PopoverTrigger>
      <button className="border px-3 py-2 hover:text-primary rounded">{`Open Popover ${id.split('-')[1]}`}</button>
    </PopoverTrigger>
    <PopoverContent>
      <h3 className={'p-2 border-b border-secondary-200'}>
        Popover {id.split('-')[1]} at {placement}
      </h3>
      <p className="text-xs text-secondary-500 p-2">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec dui id nunc lacinia eleifend. Cras vestibulum.
      </p>
    </PopoverContent>
  </Popover>
);

/** Managed Popovers, a `<PopoverProvider multiselect>` will have options like hold shift to select multiple  */
export const Managed: StoryObj<typeof Popover> = {
  render: ManagedTemplate,
  name: 'Managed Popovers',
  args: {
    placement: 'top',
    alignment: undefined,
    activation: 'click',
    showArrow: true,
    showCloseButton: false,
    delay: { open: 500, close: 150 },
  },
};
