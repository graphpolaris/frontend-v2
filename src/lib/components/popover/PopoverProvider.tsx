import React, { useState, useEffect } from 'react';

interface PopoverGlobalContextProps {
  openPopover: (id: string) => void;
  closePopover: (id: string) => void;
  isOpen: (id: string) => boolean;
  isMultiselect: () => boolean;
}

const PopoverGlobalContext = React.createContext<PopoverGlobalContextProps | null>(null);

// Custom hook to access the global Popover context
export const usePopoverGlobalContext = (): PopoverGlobalContextProps | null => {
  return React.useContext(PopoverGlobalContext);
};

// PopoverProvider component
interface PopoverProviderProps {
  children: React.ReactNode;
  multiselect?: boolean;
  multiselectKey?: 'Shift' | 'Alt' | 'Control';
}

export function PopoverProvider({ children, multiselect = false, multiselectKey = 'Shift' }: PopoverProviderProps) {
  const [openPopoverIds, setOpenPopoverIds] = useState<Set<string>>(new Set());
  const [isModifierPressed, setIsModifierPressed] = useState(false);

  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.key === multiselectKey) {
        setIsModifierPressed(true);
      }
    };
    const handleKeyUp = (event: KeyboardEvent) => {
      if (event.key === multiselectKey) {
        setIsModifierPressed(false);
      }
    };

    window.addEventListener('keydown', handleKeyDown);
    window.addEventListener('keyup', handleKeyUp);
    return () => {
      window.removeEventListener('keydown', handleKeyDown);
      window.removeEventListener('keyup', handleKeyUp);
    };
  }, [multiselectKey]);

  const openPopover = (id: string) => {
    setOpenPopoverIds(prev => {
      const newSet = new Set(prev);

      if (multiselect && isModifierPressed) {
        newSet.add(id); // Voeg popover toe zonder anderen te sluiten
      } else {
        prev.forEach(openId => newSet.delete(openId)); // Sluit alle popovers in deze provider
        newSet.add(id);
      }

      return newSet;
    });
  };

  const closePopover = (id: string) => {
    setOpenPopoverIds(prev => {
      const newSet = new Set(prev);
      newSet.delete(id);
      return newSet;
    });
  };

  const isOpen = (id: string) => openPopoverIds.has(id);
  const isMultiselect = () => multiselect;

  return (
    <PopoverGlobalContext.Provider value={{ openPopover, closePopover, isOpen, isMultiselect }}>{children}</PopoverGlobalContext.Provider>
  );
}
