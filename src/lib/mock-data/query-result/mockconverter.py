# %%
import json, os
from pathlib import Path

current_file = Path(os.path.realpath(__file__)).parent
print(current_file)
file = "./mockMobilityQueryResult.json"
# %%
data = json.load(open(file))

"""
# for node in data["nodes"]:
#     node["label"] = node["_id"].split("/")[0]
#     node["_id"] = node["_id"].split("/")[1]
#     del node["_key"]
#     del node["_rev"]

# data["nodes"]

for edge in data["edges"]:
    #     del edge["_key"]
    #     del edge["_rev"]
    #     edge["label"] = edge["_id"].split("/")[0]
    #     edge["_id"] = edge["_id"].split("/")[1]
    #     edge["from"] = edge["from"].split("/")[1]
    #     edge["to"] = edge["to"].split("/")[1]
    edge["from"] = edge["source"]
    edge["to"] = edge["target"]
    del edge["from"]
    del edge["to"]
print(data["edges"][0])

# %%
json.dump(data, open("big2ndChamberQueryResult.json", "w"), indent=2)
"""

for node in data.get("nodes", []):
    node["_id"] = node["_id"].split("/")[1]


for edge in data["edges"]:
    edge["label"] = edge["_id"].split("/")[0]
    edge["_id"] = edge["_id"].split("/")[1]
    edge["from"] = edge["from"].split("/")[1]
    edge["to"] = edge["to"].split("/")[1]
    del edge["_key"]
    del edge["_rev"]


json.dump(data, open(file, "w"), indent=2)
