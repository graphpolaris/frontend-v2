import { graphQueryBackend2graphQuery } from 'ts-common';

const mockDataArray = [
  'big2ndChamberQueryResult',
  'bigMockQueryResults',
  'bigQueryResult',
  'gotCharacter2Character',
  'marieBoucherSample',
  'mockLargeQueryResults',
  'mockMobilityQueryResult',
  'typesMockQueryResults',
  'testUnMatchHeadersDataResults',
  'smallFlightsQueryResults',
  'smallVillainQueryResults',
  'smallVillainDoubleArchQueryResults',
  'mockRecommendationsActorMovie',
  'movie_recommendationPersonActedInMovieQueryResult',
  'slack_slackReactionToThreadedMessageQueryResult',
] as const;
export type MockDataI = (typeof mockDataArray)[number];

export const loadMockData = async (fileName: MockDataI) => {
  const filename = `./${fileName.replace('_', '/')}.json`;
  const json = await import(filename /* @vite-ignore */);
  const { nodes, edges, metaData } = graphQueryBackend2graphQuery(json.default);
  return {
    data: {
      nodes,
      edges,
    },
    graphMetadata: metaData,
  };
};

export const mockData = Object.fromEntries(mockDataArray.map(fileName => [fileName, () => loadMockData(fileName)]));
