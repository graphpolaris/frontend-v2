import { SchemaFromBackend } from 'ts-common';
import { SchemaUtils } from '../../schema/schema-utils';

export const marieBoucherSampleSchemaRaw: SchemaFromBackend = {
  nodes: [
    {
      name: 'merchant',
      attributes: [
        { name: 'name', type: 'string' },
        { name: 'data', type: 'int' },
      ],
    },
  ],
  edges: [
    {
      name: 'transaction',
      label: 'transaction',
      from: 'merchant',
      to: 'merchant',
      collection: 'transaction',
      attributes: [{ name: 'time', type: 'int' }],
    },
  ],
};

export const marieBoucherSampleSchema = SchemaUtils.schemaBackend2Graphology(marieBoucherSampleSchemaRaw);
