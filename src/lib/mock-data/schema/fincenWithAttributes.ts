import { SchemaUtils } from '@/lib/schema/schema-utils';
import { SchemaFromBackend } from 'ts-common';

export const fincenWithAttributesRaw: SchemaFromBackend = {
  nodes: [
    {
      name: 'Entity',
      attributes: [
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'country',
          type: 'string',
        },
      ],
    },
    {
      name: 'Filing',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'end_date',
          type: 'string',
        },
        {
          name: 'amount',
          type: 'int',
        },
        {
          name: 'beneficiary_iso',
          type: 'string',
        },
        {
          name: 'beneficiary_lng',
          type: 'string',
        },
        {
          name: 'begin_date',
          type: 'string',
        },
        {
          name: 'originator_bank',
          type: 'string',
        },
        {
          name: 'beneficiary_lat',
          type: 'string',
        },
        {
          name: 'begin_date_format',
          type: 'string',
        },
        {
          name: 'end_date_format',
          type: 'string',
        },
        {
          name: 'originator_iso',
          type: 'string',
        },
        {
          name: 'beneficiary_bank_id',
          type: 'string',
        },
        {
          name: 'origin_lat',
          type: 'string',
        },
        {
          name: 'number',
          type: 'int',
        },
        {
          name: 'filer_org_name',
          type: 'string',
        },
        {
          name: 'originator_bank_country',
          type: 'string',
        },
        {
          name: 'filer_org_name_id',
          type: 'string',
        },
        {
          name: 'beneficiary_bank',
          type: 'string',
        },
        {
          name: 'beneficiary_bank_country',
          type: 'string',
        },
        {
          name: 'origin_lng',
          type: 'string',
        },
        {
          name: 'originator_bank_id',
          type: 'string',
        },
        {
          name: 'sar_id',
          type: 'string',
        },
      ],
    },
    {
      name: 'Country',
      attributes: [
        {
          name: 'code',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'tld',
          type: 'string',
        },
      ],
    },
  ],
  edges: [
    {
      name: 'FILED',
      label: 'FILED',
      collection: 'FILED',
      from: 'Entity',
      to: 'Filing',
      attributes: [],
    },
    {
      name: 'COUNTRY',
      label: 'COUNTRY',
      collection: 'COUNTRY',
      from: 'Entity',
      to: 'Country',
      attributes: [],
    },
    {
      name: 'CONCERNS',
      label: 'CONCERNS',
      collection: 'CONCERNS',
      from: 'Filing',
      to: 'Entity',
      attributes: [],
    },
    {
      name: 'ORIGINATOR',
      label: 'ORIGINATOR',
      collection: 'ORIGINATOR',
      from: 'Filing',
      to: 'Entity',
      attributes: [],
    },
    {
      name: 'BENEFITS',
      label: 'BENEFITS',
      collection: 'BENEFITS',
      from: 'Filing',
      to: 'Entity',
      attributes: [],
    },
  ],
};

export const fincenWithAttributes = SchemaUtils.schemaBackend2Graphology(fincenWithAttributesRaw);
