import { SchemaUtils } from '@/lib/schema/schema-utils';
import { SchemaFromBackend } from 'ts-common';

export const slackWithAttributesRaw: SchemaFromBackend = {
  nodes: [
    {
      name: 'Owner',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'status_emoji',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
      ],
    },
    {
      name: 'Bot',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
      ],
    },
    {
      name: 'AppUser',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
      ],
    },
    {
      name: 'Emoji',
      attributes: [
        {
          name: 'name',
          type: 'string',
        },
      ],
    },
    {
      name: 'ThreadedMessage',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'subtype',
          type: 'string',
        },
        {
          name: 'type',
          type: 'string',
        },
        {
          name: 'text',
          type: 'string',
        },
      ],
    },
    {
      name: 'PrimaryOwner',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
      ],
    },
    {
      name: 'Team',
      attributes: [],
    },
    {
      name: 'User',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'status_emoji',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'skype',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'status_text_canonical',
          type: 'string',
        },
        {
          name: 'status_text',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'status_emoji',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'status_emoji',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'skype',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'status_text',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'skype',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
      ],
    },
    {
      name: 'Message',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'subtype',
          type: 'string',
        },
        {
          name: 'type',
          type: 'string',
        },
        {
          name: 'text',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'subtype',
          type: 'string',
        },
        {
          name: 'type',
          type: 'string',
        },
        {
          name: 'text',
          type: 'string',
        },
      ],
    },
    {
      name: 'Deleted',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'skype',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
      ],
    },
    {
      name: 'Channel',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'is_archived',
          type: 'bool',
        },
        {
          name: 'purpose',
          type: 'string',
        },
        {
          name: 'is_general',
          type: 'bool',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'topic',
          type: 'string',
        },
        {
          name: 'pagerank',
          type: 'float',
        },
      ],
    },
    {
      name: 'Attachment',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'title_link',
          type: 'string',
        },
        {
          name: 'text',
          type: 'string',
        },
        {
          name: 'thumb_url',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'thumb_width',
          type: 'int',
        },
        {
          name: 'thumb_height',
          type: 'int',
        },
      ],
    },
    {
      name: 'Admin',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'status_emoji',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'status_emoji',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_1024',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'is_custom_image',
          type: 'bool',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'skype',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'status_text',
          type: 'string',
        },
        {
          name: 'image_original',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'real_name',
          type: 'string',
        },
        {
          name: 'color',
          type: 'string',
        },
        {
          name: 'image_32',
          type: 'string',
        },
        {
          name: 'image_48',
          type: 'string',
        },
        {
          name: 'real_name_normalized',
          type: 'string',
        },
        {
          name: 'team',
          type: 'string',
        },
        {
          name: 'image_24',
          type: 'string',
        },
        {
          name: 'display_name_normalized',
          type: 'string',
        },
        {
          name: 'image_512',
          type: 'string',
        },
        {
          name: 'avatar_hash',
          type: 'string',
        },
        {
          name: 'display_name',
          type: 'string',
        },
        {
          name: 'first_name',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'image_72',
          type: 'string',
        },
        {
          name: 'last_name',
          type: 'string',
        },
        {
          name: 'image_192',
          type: 'string',
        },
        {
          name: 'status_expiration',
          type: 'int',
        },
      ],
    },
    {
      name: 'Reaction',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
      ],
    },
    {
      name: 'TimeZone',
      attributes: [
        {
          name: 'id',
          type: 'string',
        },
        {
          name: 'label',
          type: 'string',
        },
      ],
    },
  ],
  edges: [
    {
      name: 'MEMBER_OF',
      label: 'MEMBER_OF',
      collection: 'MEMBER_OF',
      from: 'Admin',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'MEMBER_OF',
      label: 'MEMBER_OF',
      collection: 'MEMBER_OF',
      from: 'Bot',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'MEMBER_OF',
      label: 'MEMBER_OF',
      collection: 'MEMBER_OF',
      from: 'Owner',
      to: 'Channel',
      attributes: [],
    },
    {
      label: 'MEMBER_OF',
      name: 'MEMBER_OF',
      collection: 'MEMBER_OF',
      from: 'PrimaryOwner',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'MEMBER_OF',
      label: 'MEMBER_OF',
      collection: 'MEMBER_OF',
      from: 'User',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'MEMBER_OF',
      label: 'MEMBER_OF',
      collection: 'MEMBER_OF',
      from: 'Deleted',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'REACTION_TYPE',
      label: 'REACTION_TYPE',
      collection: 'REACTION_TYPE',
      from: 'Reaction',
      to: 'Emoji',
      attributes: [],
    },
    {
      name: 'MENTIONS_CHANNEL',
      label: 'MENTIONS_CHANNEL',
      collection: 'MENTIONS_CHANNEL',
      from: 'ThreadedMessage',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'MENTIONS_CHANNEL',
      label: 'MENTIONS_CHANNEL',
      collection: 'MENTIONS_CHANNEL',
      from: 'Message',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'IN_REPLY_TO',
      label: 'IN_REPLY_TO',
      collection: 'IN_REPLY_TO',
      from: 'Message',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'IN_REPLY_TO',
      label: 'IN_REPLY_TO',
      collection: 'IN_REPLY_TO',
      from: 'Message',
      to: 'ThreadedMessage',
      attributes: [],
    },
    {
      name: 'IN_REPLY_TO',
      label: 'IN_REPLY_TO',
      collection: 'IN_REPLY_TO',
      from: 'ThreadedMessage',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'IN_REPLY_TO',
      label: 'IN_REPLY_TO',
      collection: 'IN_REPLY_TO',
      from: 'ThreadedMessage',
      to: 'ThreadedMessage',
      attributes: [],
    },
    {
      name: 'REACTED',
      label: 'REACTED',
      collection: 'REACTED',
      from: 'Owner',
      to: 'Reaction',
      attributes: [],
    },
    {
      name: 'REACTED',
      label: 'REACTED',
      collection: 'REACTED',
      from: 'User',
      to: 'Reaction',
      attributes: [],
    },
    {
      name: 'REACTED',
      label: 'REACTED',
      collection: 'REACTED',
      from: 'Admin',
      to: 'Reaction',
      attributes: [],
    },
    {
      name: 'REACTED',
      label: 'REACTED',
      collection: 'REACTED',
      from: 'PrimaryOwner',
      to: 'Reaction',
      attributes: [],
    },
    {
      name: 'REACTED',
      label: 'REACTED',
      collection: 'REACTED',
      from: 'Deleted',
      to: 'Reaction',
      attributes: [],
    },
    {
      name: 'CREATED',
      label: 'CREATED',
      collection: 'CREATED',
      from: 'PrimaryOwner',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'CREATED',
      label: 'CREATED',
      collection: 'CREATED',
      from: 'Admin',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'CREATED',
      label: 'CREATED',
      collection: 'CREATED',
      from: 'User',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'CREATED',
      label: 'CREATED',
      collection: 'CREATED',
      from: 'Deleted',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'CREATED',
      label: 'CREATED',
      collection: 'CREATED',
      from: 'Owner',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'IN_TIMEZONE',
      label: 'IN_TIMEZONE',
      collection: 'IN_TIMEZONE',
      from: 'Deleted',
      to: 'TimeZone',
      attributes: [],
    },
    {
      name: 'IN_TIMEZONE',
      label: 'IN_TIMEZONE',
      collection: 'IN_TIMEZONE',
      from: 'User',
      to: 'TimeZone',
      attributes: [],
    },
    {
      name: 'IN_TIMEZONE',
      label: 'IN_TIMEZONE',
      collection: 'IN_TIMEZONE',
      from: 'Owner',
      to: 'TimeZone',
      attributes: [],
    },
    {
      name: 'IN_TIMEZONE',
      label: 'IN_TIMEZONE',
      collection: 'IN_TIMEZONE',
      from: 'AppUser',
      to: 'TimeZone',
      attributes: [],
    },
    {
      name: 'IN_TIMEZONE',
      label: 'IN_TIMEZONE',
      collection: 'IN_TIMEZONE',
      from: 'PrimaryOwner',
      to: 'TimeZone',
      attributes: [],
    },
    {
      name: 'IN_TIMEZONE',
      label: 'IN_TIMEZONE',
      collection: 'IN_TIMEZONE',
      from: 'Bot',
      to: 'TimeZone',
      attributes: [],
    },
    {
      name: 'IN_TIMEZONE',
      label: 'IN_TIMEZONE',
      collection: 'IN_TIMEZONE',
      from: 'Admin',
      to: 'TimeZone',
      attributes: [],
    },
    {
      name: 'HAS_ATTACHMENT',
      label: 'HAS_ATTACHMENT',
      collection: 'HAS_ATTACHMENT',
      from: 'Message',
      to: 'Attachment',
      attributes: [],
    },
    {
      name: 'HAS_ATTACHMENT',
      label: 'HAS_ATTACHMENT',
      collection: 'HAS_ATTACHMENT',
      from: 'ThreadedMessage',
      to: 'Attachment',
      attributes: [],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'PrimaryOwner',
      to: 'ThreadedMessage',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'Deleted',
      to: 'ThreadedMessage',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'User',
      to: 'Message',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'Admin',
      to: 'Message',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'User',
      to: 'ThreadedMessage',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'Admin',
      to: 'ThreadedMessage',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'Deleted',
      to: 'Message',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'PrimaryOwner',
      to: 'Message',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'Owner',
      to: 'Message',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'EDITED',
      label: 'EDITED',
      collection: 'EDITED',
      from: 'Owner',
      to: 'ThreadedMessage',
      attributes: [
        {
          name: 'editedAt',
          type: 'int',
        },
        {
          name: 'ts',
          type: 'string',
        },
      ],
    },
    {
      name: 'IN_CHANNEL',
      label: 'IN_CHANNEL',
      collection: 'IN_CHANNEL',
      from: 'Message',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'IN_CHANNEL',
      label: 'IN_CHANNEL',
      collection: 'IN_CHANNEL',
      from: 'ThreadedMessage',
      to: 'Channel',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'User',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'Owner',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'PrimaryOwner',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'Deleted',
      to: 'ThreadedMessage',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'User',
      to: 'ThreadedMessage',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'Admin',
      to: 'ThreadedMessage',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'PrimaryOwner',
      to: 'ThreadedMessage',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'Admin',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'Deleted',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'Bot',
      to: 'ThreadedMessage',
      attributes: [],
    },
    {
      label: 'POSTED',
      name: 'POSTED',
      collection: 'POSTED',
      from: 'Bot',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'POSTED',
      label: 'POSTED',
      collection: 'POSTED',
      from: 'Owner',
      to: 'ThreadedMessage',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'Message',
      to: 'User',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'ThreadedMessage',
      to: 'Admin',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'ThreadedMessage',
      to: 'Bot',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'ThreadedMessage',
      to: 'User',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'Message',
      to: 'Admin',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'ThreadedMessage',
      to: 'Deleted',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'ThreadedMessage',
      to: 'PrimaryOwner',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'Message',
      to: 'Owner',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'Message',
      to: 'PrimaryOwner',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'Message',
      to: 'Bot',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'Message',
      to: 'Deleted',
      attributes: [],
    },
    {
      name: 'MENTIONS_USER',
      label: 'MENTIONS_USER',
      collection: 'MENTIONS_USER',
      from: 'ThreadedMessage',
      to: 'Owner',
      attributes: [],
    },
    {
      name: 'TO_MESSAGE',
      label: 'TO_MESSAGE',
      collection: 'TO_MESSAGE',
      from: 'Reaction',
      to: 'Message',
      attributes: [],
    },
    {
      name: 'TO_MESSAGE',
      label: 'TO_MESSAGE',
      collection: 'TO_MESSAGE',
      from: 'Reaction',
      to: 'ThreadedMessage',
      attributes: [],
    },
  ],
};

export const slackWithAttributes = SchemaUtils.schemaBackend2Graphology(slackWithAttributesRaw);
