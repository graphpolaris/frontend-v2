import { SchemaUtils } from '@/lib/schema/schema-utils';
import { SchemaFromBackend } from 'ts-common';

export const recommendationsWithAttributesRaw: SchemaFromBackend = {
  nodes: [
    {
      name: 'Movie',
      attributes: [
        {
          name: 'movieId',
          type: 'string',
        },
        {
          name: 'imdbId',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'tmdbId',
          type: 'string',
        },
        {
          name: 'year',
          type: 'int',
        },
        {
          name: 'countries',
          type: 'string',
        },
        {
          name: 'languages',
          type: 'string',
        },
        {
          name: 'plot',
          type: 'string',
        },
        {
          name: 'imdbRating',
          type: 'float',
        },
        {
          name: 'imdbVotes',
          type: 'int',
        },
        {
          name: 'released',
          type: 'string',
        },
        {
          name: 'runtime',
          type: 'int',
        },
        {
          name: 'poster',
          type: 'string',
        },
        {
          name: 'revenue',
          type: 'int',
        },
        {
          name: 'budget',
          type: 'int',
        },
        {
          name: 'url',
          type: 'string',
        },
      ],
    },
    {
      name: 'User',
      attributes: [
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'userId',
          type: 'string',
        },
      ],
    },
    {
      name: 'Actor',
      attributes: [
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'imdbId',
          type: 'string',
        },
        {
          name: 'tmdbId',
          type: 'string',
        },
        {
          name: 'poster',
          type: 'string',
        },
        {
          name: 'born',
          type: 'string',
        },
        {
          name: 'died',
          type: 'string',
        },
        {
          name: 'bornIn',
          type: 'string',
        },
        {
          name: 'bio',
          type: 'string',
        },
        {
          name: 'url',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'imdbId',
          type: 'string',
        },
        {
          name: 'tmdbId',
          type: 'string',
        },
        {
          name: 'poster',
          type: 'string',
        },
        {
          name: 'born',
          type: 'string',
        },
        {
          name: 'died',
          type: 'string',
        },
        {
          name: 'bornIn',
          type: 'string',
        },
        {
          name: 'bio',
          type: 'string',
        },
        {
          name: 'url',
          type: 'string',
        },
      ],
    },
    {
      name: 'Director',
      attributes: [
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'imdbId',
          type: 'string',
        },
        {
          name: 'tmdbId',
          type: 'string',
        },
        {
          name: 'poster',
          type: 'string',
        },
        {
          name: 'born',
          type: 'string',
        },
        {
          name: 'died',
          type: 'string',
        },
        {
          name: 'bornIn',
          type: 'string',
        },
        {
          name: 'bio',
          type: 'string',
        },
        {
          name: 'url',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'imdbId',
          type: 'string',
        },
        {
          name: 'tmdbId',
          type: 'string',
        },
        {
          name: 'poster',
          type: 'string',
        },
        {
          name: 'born',
          type: 'string',
        },
        {
          name: 'died',
          type: 'string',
        },
        {
          name: 'bornIn',
          type: 'string',
        },
        {
          name: 'bio',
          type: 'string',
        },
        {
          name: 'url',
          type: 'string',
        },
      ],
    },
    {
      name: 'Genre',
      attributes: [
        {
          name: 'name',
          type: 'string',
        },
      ],
    },
    {
      name: 'Person',
      attributes: [
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'imdbId',
          type: 'string',
        },
        {
          name: 'tmdbId',
          type: 'string',
        },
        {
          name: 'poster',
          type: 'string',
        },
        {
          name: 'born',
          type: 'string',
        },
        {
          name: 'died',
          type: 'string',
        },
        {
          name: 'bornIn',
          type: 'string',
        },
        {
          name: 'bio',
          type: 'string',
        },
        {
          name: 'url',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'imdbId',
          type: 'string',
        },
        {
          name: 'tmdbId',
          type: 'string',
        },
        {
          name: 'poster',
          type: 'string',
        },
        {
          name: 'born',
          type: 'string',
        },
        {
          name: 'died',
          type: 'string',
        },
        {
          name: 'bornIn',
          type: 'string',
        },
        {
          name: 'bio',
          type: 'string',
        },
        {
          name: 'url',
          type: 'string',
        },
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'imdbId',
          type: 'string',
        },
        {
          name: 'tmdbId',
          type: 'string',
        },
        {
          name: 'poster',
          type: 'string',
        },
        {
          name: 'born',
          type: 'string',
        },
        {
          name: 'died',
          type: 'string',
        },
        {
          name: 'bornIn',
          type: 'string',
        },
        {
          name: 'bio',
          type: 'string',
        },
        {
          name: 'url',
          type: 'string',
        },
      ],
    },
  ],
  edges: [
    {
      name: 'ACTED_IN',
      label: 'ACTED_IN',
      collection: 'ACTED_IN',
      from: 'Actor',
      to: 'Movie',
      attributes: [
        {
          name: 'role',
          type: 'string',
        },
      ],
    },
    {
      name: 'ACTED_IN',
      label: 'ACTED_IN',
      collection: 'ACTED_IN',
      from: 'Person',
      to: 'Movie',
      attributes: [
        {
          name: 'role',
          type: 'string',
        },
      ],
    },
    {
      name: 'ACTED_IN',
      label: 'ACTED_IN',
      collection: 'ACTED_IN',
      from: 'Director',
      to: 'Movie',
      attributes: [
        {
          name: 'role',
          type: 'string',
        },
      ],
    },
    {
      name: 'RATED',
      label: 'RATED',
      collection: 'RATED',
      from: 'User',
      to: 'Movie',
      attributes: [
        {
          name: 'rating',
          type: 'float',
        },
        {
          name: 'timestamp',
          type: 'int',
        },
      ],
    },
    {
      name: 'IN_GENRE',
      label: 'IN_GENRE',
      collection: 'IN_GENRE',
      from: 'Movie',
      to: 'Genre',
      attributes: [],
    },
    {
      name: 'DIRECTED',
      label: 'DIRECTED',
      collection: 'DIRECTED',
      from: 'Actor',
      to: 'Movie',
      attributes: [
        {
          name: 'role',
          type: 'string',
        },
      ],
    },
    {
      name: 'DIRECTED',
      label: 'DIRECTED',
      collection: 'DIRECTED',
      from: 'Director',
      to: 'Movie',
      attributes: [
        {
          name: 'role',
          type: 'string',
        },
      ],
    },
    {
      name: 'DIRECTED',
      label: 'DIRECTED',
      collection: 'DIRECTED',
      from: 'Person',
      to: 'Movie',
      attributes: [
        {
          name: 'role',
          type: 'string',
        },
      ],
    },
  ],
};

export const recommendationsSchemaWithAttributes = SchemaUtils.schemaBackend2Graphology(recommendationsWithAttributesRaw);
