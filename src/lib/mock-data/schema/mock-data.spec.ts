import { assert, describe, expect, it, test } from 'vitest';

import { movieSchemaRaw, movieSchema } from '..';
import { northWindSchema, northwindSchemaRaw } from '..';
import { simpleSchema, simpleSchemaRaw } from '..';
import { twitterSchema, twitterSchemaRaw } from '..';

describe('MockData Tests', () => {
  it('should have raw data available movie', () => {
    const graph = movieSchemaRaw;
    expect(graph);
  });

  it('should have raw data available northwind', () => {
    const graph = northwindSchemaRaw;
    expect(graph);
  });

  it('should have raw data available simpleSchemaRaw', () => {
    const graph = simpleSchemaRaw;
    expect(graph);
  });

  it('should have raw data available twitterSchemaRaw', () => {
    const graph = twitterSchemaRaw;
    expect(graph);
  });

  it('should have data available as graphology model movie', () => {
    const graph = movieSchema;
    expect(graph);

    expect(graph.constructor.name.toLowerCase().indexOf('graph') != -1).toBeTruthy();
  });

  it('should have data available as graphology model northwind', () => {
    const graph = northWindSchema;
    expect(graph);

    expect(graph.constructor.name.toLowerCase().indexOf('graph') != -1).toBeTruthy();
  });

  it('should have data available as graphology model simpleSchemaRaw', () => {
    const graph = simpleSchema;
    expect(graph);

    expect(graph.constructor.name.toLowerCase().indexOf('graph') != -1).toBeTruthy();
  });

  it('should have data available as graphology model twitterSchemaRaw', () => {
    const graph = twitterSchema;
    expect(graph);

    expect(graph.constructor.name.toLowerCase().indexOf('graph') != -1).toBeTruthy();
  });
});
