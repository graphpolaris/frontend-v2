import { SchemaUtils } from '@/lib/schema/schema-utils/schemaUtils';
import { SchemaFromBackend } from 'ts-common';

export const movieSchemaWithAttributesRaw: SchemaFromBackend = {
  nodes: [
    {
      name: 'Movie',
      attributes: [
        {
          name: 'tagline',
          type: 'string',
        },
        {
          name: 'title',
          type: 'string',
        },
        {
          name: 'released',
          type: 'int',
        },
        {
          name: 'votes',
          type: 'int',
        },
      ],
    },
    {
      name: 'Person',
      attributes: [
        {
          name: 'born',
          type: 'int',
        },
        {
          name: 'name',
          type: 'string',
        },
      ],
    },
  ],
  edges: [
    {
      name: 'ACTED_IN',
      label: 'ACTED_IN',
      collection: 'ACTED_IN',
      from: 'Person',
      to: 'Movie',
      attributes: [
        {
          name: 'roles',
          type: 'string',
        },
      ],
    },
    {
      name: 'REVIEWED',
      label: 'REVIEWED',
      collection: 'REVIEWED',
      from: 'Person',
      to: 'Movie',
      attributes: [
        {
          name: 'summary',
          type: 'string',
        },
        {
          name: 'rating',
          type: 'int',
        },
      ],
    },
    {
      name: 'PRODUCED',
      label: 'PRODUCED',
      collection: 'PRODUCED',
      from: 'Person',
      to: 'Movie',
      attributes: [],
    },
    {
      name: 'WROTE',
      label: 'WROTE',
      collection: 'WROTE',
      from: 'Person',
      to: 'Movie',
      attributes: [],
    },
    {
      name: 'FOLLOWS',
      label: 'FOLLOWS',
      collection: 'FOLLOWS',
      from: 'Person',
      to: 'Person',
      attributes: [],
    },
    {
      name: 'DIRECTED',
      label: 'DIRECTED',
      collection: 'DIRECTED',
      from: 'Person',
      to: 'Movie',
      attributes: [],
    },
  ],
};

export const movieSchemaWithAttributes = SchemaUtils.schemaBackend2Graphology(movieSchemaWithAttributesRaw);
