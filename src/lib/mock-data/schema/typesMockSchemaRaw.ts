import { SchemaUtils } from '@/lib/schema/schema-utils';
import { SchemaFromBackend } from 'ts-common';

export const typesMockSchemaRaw: SchemaFromBackend = {
  nodes: [
    {
      name: 'worker',
      attributes: [
        {
          name: 'name',
          type: 'string',
        },
        {
          name: 'age',
          type: 'int',
        },
        {
          name: 'height',
          type: 'float',
        },
        {
          name: 'sacked',
          type: 'bool',
        },
        {
          name: 'birthdate',
          type: 'date',
        },
        {
          name: 'startingSchedule',
          type: 'time',
        },
        {
          name: 'commutingDuration',
          type: 'duration',
        },
        {
          name: 'firstLogin',
          type: 'datetime',
        },
      ],
    },
  ],
  edges: [
    {
      name: 'TEAM_UP',
      label: 'TEAM_UP',
      collection: 'TEAM_UP',
      from: 'worker',
      to: 'worker',
      attributes: [],
    },
  ],
};

export const typesMockSchema = SchemaUtils.schemaBackend2Graphology(typesMockSchemaRaw);
