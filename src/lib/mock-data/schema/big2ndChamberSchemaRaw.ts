import { SchemaUtils } from '@/lib/schema/schema-utils';
import { SchemaFromBackend } from 'ts-common';

export const big2ndChamberSchemaRaw: SchemaFromBackend = {
  nodes: [
    {
      name: 'kamerleden',
      attributes: [
        {
          name: 'anc',
          type: 'int',
        },
        {
          name: 'img',
          type: 'string',
        },
        {
          name: 'leeftijd',
          type: 'int',
        },
        {
          name: 'height',
          type: 'int',
        },
        {
          name: 'naam',
          type: 'string',
        },
        {
          name: 'partij',
          type: 'string',
        },
        {
          name: 'woonplaats',
          type: 'string',
        },
      ],
    },
    {
      name: 'commissies',
      attributes: [
        {
          name: 'naam',
          type: 'string',
        },
        {
          name: 'year',
          type: 'int',
        },
        {
          name: 'topic',
          type: 'string',
        },
        {
          name: 'sessions',
          type: 'int',
        },
      ],
    },
  ],
  edges: [
    {
      name: 'WORKED_IN',
      label: 'WORKED_IN',
      collection: 'WORKED_IN',
      from: 'kamerleden',
      to: 'commissies',
      attributes: [],
    },
  ],
};

export const big2ndChamberSchema = SchemaUtils.schemaBackend2Graphology(big2ndChamberSchemaRaw);
