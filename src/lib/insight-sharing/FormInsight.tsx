import { useEffect, useState } from 'react';
import { TextEditor } from '@/lib/components/textEditor';
import { useAppDispatch, useSessionCache, useActiveSaveState } from '@/lib/data-access';
import { Button, Input } from '@/lib/components';
import { addError } from '@/lib/data-access/store/configSlice';
import { SerializedEditorState } from 'lexical';
import { InsightModel, InsightConditionMapReverse, InsightConditionMap } from 'ts-common';

type Props = {
  insight: InsightModel;
  setActive?: (val: number | undefined) => void;
  handleDelete: (insight: InsightModel) => void;
  handleSave: (insight: InsightModel, generateEmail: boolean) => void;
};

const FrequencyOptions = ['Hourly', 'Daily', 'Specific Day of the Week'];
const FrequencyWeekDays = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

export function FormInsight(props: Props) {
  const dispatch = useAppDispatch();
  const session = useSessionCache();
  const activeSS = useActiveSaveState();
  const [localInsight, setLocalInsight] = useState<InsightModel>(props.insight);
  const [editorState, setEditorState] = useState<SerializedEditorState | null>(null);
  const [frequency, setFrequency] = useState<{
    frequency: (typeof FrequencyOptions)[number];
    minute: number;
    hour: number;
    dayOfWeek?: (typeof FrequencyWeekDays)[number];
  }>({
    frequency: 'Specific Day of the Week',
    minute: 0,
    hour: 0,
    dayOfWeek: 'SUN',
  });

  const [valid, setValid] = useState(true);
  const [selectedQueryID, setSelectedQueryID] = useState<{ label: string; queryId: number; entities: string[] }>({
    label: '',
    queryId: 0,
    entities: [],
  });

  const queryOptions = activeSS?.queryStates.openQueryArray.map(query => ({
    label: query.name,
    queryId: query.id,
    entities: query.graph.nodes.filter(element => element.attributes.type === 'entity').map(element => element.attributes.name),
  }));

  useEffect(() => {
    let isMounted = true;

    setLocalInsight(props.insight);

    if (props.insight.queryId) {
      const selected = queryOptions?.find(option => option.queryId === props.insight.queryId);
      setSelectedQueryID({
        label: selected?.label ?? '',
        queryId: selected?.queryId ?? 0,
        entities: selected?.entities ?? [],
      });
    }
    if (props.insight.frequency && props.insight.frequency !== '') {
      const [minute, hour, , , dayOfWeek] = props.insight.frequency.split(' ');
      setFrequency({
        frequency: dayOfWeek === '*' ? (hour === '*' ? 'Hourly' : 'Daily') : 'Specific Day of the Week',
        minute: parseInt(minute),
        hour: hour === '*' ? 0 : parseInt(hour),
        dayOfWeek: dayOfWeek === '*' ? 'SUN' : (dayOfWeek as (typeof FrequencyWeekDays)[number]),
      });
    } else {
      setFrequency({
        frequency: 'Specific Day of the Week',
        minute: 0,
        hour: 0,
        dayOfWeek: 'SUN',
      });
    }

    if (isMounted) {
      if (props.insight.template) {
        try {
          const parsedTemplate = JSON.parse(props.insight.template);
          setEditorState(parsedTemplate);
        } catch (e) {
          setEditorState(null);
          console.error('Failed to parse template:', e);
        }
      } else {
        setEditorState(null);
      }
    }

    return () => {
      isMounted = false;
      setEditorState(null);
    };
  }, [props.insight]);

  useEffect(() => {
    if (!localInsight.name || localInsight.name.trim() === '') {
      setValid(false);
    } else if (frequency.hour < 0 || frequency.hour > 23) {
      setValid(false);
    } else if (frequency.minute < 0 || frequency.minute > 59) {
      setValid(false);
    } else {
      setValid(true);
    }
  }, [localInsight, frequency]);

  const handleSave = async (element: SerializedEditorState, generateEmail: boolean) => {
    if (!localInsight.name || localInsight.name.trim() === '') {
      dispatch(addError('Name is required'));
      return;
    }

    if (!session.currentSaveState) {
      dispatch(addError('No save state selected'));
      return;
    }

    const updatedInsight = { ...localInsight, template: JSON.stringify(element) };
    props.handleSave(updatedInsight, generateEmail);
  };

  const generateCronExpression = (frequency: string, minute: number, hour: number, dayOfWeek?: string) => {
    setFrequency({ frequency, minute, hour, dayOfWeek });

    let cron = '';

    if (frequency === 'Hourly') {
      cron = `${minute} * * * *`;
    } else if (frequency === 'Daily') {
      cron = `${minute} ${hour} * * *`;
    } else if (frequency === 'Specific Day of the Week') {
      cron = `${minute} ${hour} * * ${dayOfWeek}`;
    } else {
      throw new Error('Invalid frequency');
    }

    if (!cron || cron.toLowerCase().includes('nan')) {
      console.error('Invalid cron expression', cron, frequency, minute, hour, dayOfWeek);
      // dispatch(addError('Invalid cron expression'));
      return;
    }

    setLocalInsight({ ...localInsight, frequency: cron });
  };

  const capitalizeFirstLetter = (string: string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  return (
    <div>
      <h2 className="text-lg text-secondary-600 font-bold">
        {capitalizeFirstLetter(props.insight.type)} ID: {props.insight.id}
      </h2>
      <div className="p-2 flex flex-col gap-2">
        <Input label="Name" type="text" value={localInsight.name} onChange={e => setLocalInsight({ ...localInsight, name: e })} />
        <Input
          label="Description"
          type="text"
          value={localInsight.description}
          onChange={e => setLocalInsight({ ...localInsight, description: e })}
        />
        <Input
          label="Recipient(s)"
          type="text"
          value={localInsight.recipients.join(', ')}
          onChange={value => {
            const recipientList = String(value)
              .split(/[, ]/)
              .map(r => r.trim())
              .filter(r => r.length > 0);
            setLocalInsight({ ...localInsight, recipients: recipientList });
          }}
          placeholder="Enter recipient(s)"
          className="w-full"
        />

        <div className="bg-base-200 min-h-[1px] my-2" />
        <h2 className={'font-bold'}>{capitalizeFirstLetter(props.insight.type)}ing Query:</h2>
        {activeSS && (
          <div className="m-auto w-64">
            <Input
              label="Select Query"
              type="dropdown"
              value={selectedQueryID.label ?? ''}
              onChange={val => {
                const selected = queryOptions?.find(option => option.label === val);
                if (selected) {
                  setLocalInsight({
                    ...localInsight,
                    queryId: String(selected.queryId),
                  });

                  setSelectedQueryID({
                    label: selected.label ?? '',
                    queryId: selected.queryId ?? 0,
                    entities: selected.entities ?? [],
                  });
                }
              }}
              options={queryOptions?.map(option => option.label).filter((label): label is string => label !== undefined) ?? []}
              size="md"
            />
          </div>
        )}

        <div>
          <h2 className={'font-bold'}>{capitalizeFirstLetter(props.insight.type)}ing Trigger Settings</h2>

          {selectedQueryID.label !== '' && (
            <>
              <div className="flex justify-around mt-4 mb-6">
                <Input
                  label={`Send by time`}
                  type="toggle"
                  value={localInsight.alarmMode === 'always'}
                  onChange={val => {
                    setLocalInsight({
                      ...localInsight,
                      alarmMode: val ? 'always' : 'disabled',
                    });
                  }}
                  classText={localInsight.alarmMode === 'always' ? 'font-bold' : ''}
                  tooltip={`${capitalizeFirstLetter(localInsight.type)} will always be sent at the specified time`}
                />
                {localInsight.type === 'alert' && (
                  <>
                    <Input
                      label={`Diff ${localInsight.type}`}
                      type="toggle"
                      position="left"
                      value={localInsight.alarmMode === 'diff'}
                      onChange={val => {
                        setLocalInsight({
                          ...localInsight,
                          alarmMode: val ? 'diff' : 'disabled',
                        });
                      }}
                      classText={localInsight.alarmMode === 'diff' ? 'font-bold' : ''}
                      tooltip={`Diff ${localInsight.type} will trigger whenever the result changes between two consecutive runs`}
                    />
                    <Input
                      label={`Conditional ${localInsight.type}`}
                      type="toggle"
                      position="left"
                      value={localInsight.alarmMode === 'conditional'}
                      onChange={val => {
                        setLocalInsight({
                          ...localInsight,
                          alarmMode: val ? 'conditional' : 'disabled',
                          conditionsCheck: val
                            ? [
                                {
                                  nodeLabel: '',
                                  property: '',
                                  statistic: 'Count',
                                  operator: '>',
                                  value: 0,
                                },
                              ]
                            : [],
                        });
                      }}
                      classText={localInsight.alarmMode === 'conditional' ? 'font-bold' : ''}
                      tooltip={`Conditional ${localInsight.type} will trigger based on a condition check, such as the number of nodes of a certain type is greater than 10`}
                    />
                  </>
                )}
              </div>

              {localInsight.alarmMode === 'conditional' && localInsight.conditionsCheck?.length === 1 && (
                <>
                  <div className="m-2 grid grid-cols-1 xl:grid-cols-2 gap-2">
                    <Input
                      label="Node Label"
                      type="dropdown"
                      value={localInsight.conditionsCheck[0].nodeLabel}
                      onChange={value => {
                        const updatedCondition = { ...localInsight.conditionsCheck[0], nodeLabel: String(value) };
                        setLocalInsight({ ...localInsight, conditionsCheck: [updatedCondition] });
                      }}
                      options={selectedQueryID.entities}
                      inline={false}
                      size="md"
                      info="Select the node label to check"
                    />
                    <Input
                      label="Stat"
                      type="dropdown"
                      value={'Count'}
                      onChange={value => {
                        const updatedCondition = { ...localInsight.conditionsCheck[0], statistic: String(value) };
                        setLocalInsight({ ...localInsight, conditionsCheck: [updatedCondition] });
                      }}
                      options={['Count']}
                      inline={false}
                      size="md"
                      info="Select the statistic to check"
                    />
                    <Input
                      label="Comparison"
                      type="dropdown"
                      value={InsightConditionMapReverse[localInsight.conditionsCheck[0].operator]}
                      onChange={value => {
                        const updatedCondition = {
                          ...localInsight.conditionsCheck[0],
                          condition: InsightConditionMap[String(value)],
                        };
                        setLocalInsight({ ...localInsight, conditionsCheck: [updatedCondition] });
                      }}
                      options={Object.keys(InsightConditionMap)}
                      inline={false}
                      size="md"
                      info="Select the comparison operator"
                    />
                    <Input
                      label="Value"
                      type="number"
                      value={localInsight.conditionsCheck[0].value}
                      onChange={value => {
                        const updatedCondition = { ...localInsight.conditionsCheck[0], value: Number(value) };
                        setLocalInsight({ ...localInsight, conditionsCheck: [updatedCondition] });
                      }}
                      info="Enter the value to compare against"
                    />
                  </div>
                </>
              )}

              {localInsight.alarmMode !== 'disabled' && (
                <>
                  <div className="m-2 grid grid-cols-1 xl:grid-cols-2 gap-2">
                    <Input
                      label={'Frequency to trigger the ' + props.insight.type}
                      type="dropdown"
                      value={frequency.frequency}
                      onChange={value => generateCronExpression(value as string, frequency.minute, frequency.hour, frequency.dayOfWeek)}
                      options={FrequencyOptions}
                      inline={false}
                      size="md"
                      info={'Select the frequency to trigger the ' + props.insight.type}
                    />
                    <Input
                      label="Minute"
                      type="number"
                      min={0}
                      max={59}
                      validate={value => value >= 0 && value <= 59}
                      required
                      value={frequency.minute}
                      onChange={value => generateCronExpression(frequency.frequency, value, frequency.hour, frequency.dayOfWeek)}
                      info={'Enter the minute of the hour to trigger the ' + props.insight.type}
                    />
                    {frequency.frequency !== 'Hourly' && (
                      <Input
                        label="Hour"
                        type="number"
                        min={0}
                        max={23}
                        validate={value => value >= 0 && value <= 23}
                        required
                        value={frequency.hour}
                        onChange={value => generateCronExpression(frequency.frequency, frequency.minute, value, frequency.dayOfWeek)}
                        info={'Enter the hour of the day to trigger the ' + props.insight.type}
                      />
                    )}
                    {frequency.frequency === 'Specific Day of the Week' && (
                      <Input
                        label="Day of Week"
                        type="dropdown"
                        value={frequency.dayOfWeek}
                        onChange={value => generateCronExpression(frequency.frequency, frequency.minute, frequency.hour, value as string)}
                        options={FrequencyWeekDays}
                        inline={false}
                        size="md"
                        info={'Select the day of the week to trigger the ' + props.insight.type}
                      />
                    )}
                  </div>
                </>
              )}
            </>
          )}
        </div>

        <div className="min-h-[1px] my-2" />
        <h2 className="font-bold">{capitalizeFirstLetter(props.insight.type)}ing text</h2>
        {selectedQueryID.label !== '' && (
          <TextEditor
            key={`editor-${props.insight.id}`}
            editorState={editorState}
            setEditorState={setEditorState}
            showToolbar={true}
            placeholder={`Start typing your ${props.insight.type} template...`}
            handleSave={handleSave}
            saveDisabled={!valid}
            variableOptions={selectedQueryID.entities}
          >
            <Button
              label="Delete"
              variantType="secondary"
              variant="outline"
              onClick={() => {
                props.handleDelete(props.insight);
              }}
            />
          </TextEditor>
        )}
      </div>
    </div>
  );
}
