import React from 'react';
import { Button, Icon } from '../../components';
import { MonitorType } from './Sidebar';

type Props = {
  setAdding: (val: false | MonitorType) => void;
};

export function StartScreen(props: Props) {
  return (
    <div className="w-full h-full flex justify-center items-center">
      <div className="">
        <span className="text-lg text-secondary-700 font-bold mb-4">Start</span>
        <div>
          <Button
            iconComponent={<Icon component="icon-[ic--outline-alarm]" />}
            label="New report"
            variant="outline"
            className="mb-2"
            onClick={() => {
              props.setAdding('report');
            }}
          />
          <Button
            iconComponent={<Icon component="icon-[ic--baseline-add-alert]" />}
            label="New alert"
            variant="outline"
            onClick={() => {
              props.setAdding('alert');
            }}
          />
        </div>
      </div>
    </div>
  );
}
