import React, { useState } from 'react';
import { Input, Button } from '../../components';
import { MonitorType as InsightType, MonitorType } from './Sidebar';
import { useAppDispatch, useSessionCache } from '../../data-access';
import { addInsight } from '../../data-access/store/insightSharingSlice';
import { wsCreateInsight } from '../../data-access/broker/wsInsightSharing';
import { addError, addSuccess } from '@/lib/data-access/store/configSlice';
import { InsightRequest } from 'ts-common';

type Props = {
  setAdding: (val: false | MonitorType) => void;
  setActive: (val: number | undefined) => void;
  type: InsightType;
};

export function AddItem(props: Props) {
  const [name, setName] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const dispatch = useAppDispatch();
  const session = useSessionCache();

  const handleSave = async () => {
    if (!name.trim()) {
      dispatch(addError('Name is required'));
      return;
    }

    if (!session.currentSaveState) {
      dispatch(addError('No save state selected'));
      return;
    }

    const newInsight: InsightRequest = {
      name,
      description,
      recipients: [],
      template: '',
      frequency: props.type === 'report' ? 'Daily' : '',
      saveStateId: session.currentSaveState,
      type: props.type,
      alarmMode: 'disabled',
    };

    wsCreateInsight(newInsight, (data: any, status: string) => {
      if (status === 'success') {
        dispatch(addInsight(data));
        props.setActive(data.id);
        props.setAdding(false);
        dispatch(addSuccess('Succesfully created ' + props.type));
      } else {
        console.error('Failed to create insight:', data);
        dispatch(addError('Failed to create new ' + props.type));
      }
    });
  };

  return (
    <div>
      <span className="text-lg text-secondary-600 font-bold mb-4">Add a new {props.type}ing service</span>
      <Input type="text" label="Name" value={name} onChange={setName} className="mb-2" />
      <Input type="text" label="Description" value={description} onChange={setDescription} className="mb-2" />
      <Button label="Save" onClick={handleSave} disabled={!name || !description} className="mt-2" />
    </div>
  );
}
