import React, { useEffect } from 'react';
import { Button } from '../../components';
import { Accordion, AccordionBody, AccordionHead, AccordionItem } from '../../components/accordion';
import { useAppDispatch, useInsights } from '../../data-access';
import { setInsights } from '../../data-access/store/insightSharingSlice';
import { useSessionCache } from '../../data-access';
import { wsGetInsights } from '../../data-access/broker/wsInsightSharing';

export type MonitorType = 'report' | 'alert';

type SidebarProps = {
  setAdding: (val: false | MonitorType) => void;
  setActive: (val: number | undefined) => void;
};

export function Sidebar(props: SidebarProps) {
  const dispatch = useAppDispatch();
  const session = useSessionCache();
  const insights = useInsights();

  useEffect(() => {
    if (session.currentSaveState && session.currentSaveState !== '') {
      wsGetInsights({ saveStateId: session.currentSaveState }, (data: any, status: string) => {
        dispatch(setInsights(data));
      });
    }
  }, [session.currentSaveState]);

  return (
    <>
      <span className="text-lg text-secondary-700 font-semibold px-2 py-4">Insight Sharing</span>
      <Accordion defaultOpenAll={true}>
        <AccordionItem className="">
          <AccordionHead className="border-b bg-secondary-50 hover:bg-secondary-100 p-1">
            <div className="w-full flex justify-between">
              <span className="font-semibold">Reports</span>
              <div
                onClick={e => {
                  props.setAdding('report');
                  e.stopPropagation();
                }}
              >
                <Button
                  as={'a'}
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--outline-add]"
                  onClick={() => {}}
                />
              </div>
            </div>
          </AccordionHead>
          <AccordionBody className="ml-0">
            <ul className="space-y-2">
              {insights
                .filter(insight => insight.type === 'report')
                .map(report => (
                  <li
                    key={report.id}
                    className="cursor-pointer p-2 hover:bg-secondary-50 flex items-center gap-2"
                    onClick={() => {
                      props.setAdding(false);
                      props.setActive(report.id);
                    }}
                  >
                    <div className="w-3 h-3 bg-success-500 rounded-full"></div>
                    <span>{report.name}</span>
                  </li>
                ))}
            </ul>
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionHead className="border-b bg-secondary-50 hover:bg-secondary-100 p-1">
            <div className="w-full flex justify-between">
              <span className="font-semibold">Alerts</span>
              <div
                onClick={e => {
                  props.setAdding('alert');
                  e.stopPropagation();
                }}
              >
                <Button
                  as={'a'}
                  variantType="secondary"
                  variant="ghost"
                  size="xs"
                  iconComponent="icon-[ic--outline-add]"
                  onClick={() => {}}
                />
              </div>
            </div>
          </AccordionHead>
          <AccordionBody className="ml-0">
            <ul className="space-y-2">
              {insights
                .filter(insight => insight.type === 'alert')
                .map(alert => (
                  <li
                    key={alert.id}
                    className="cursor-pointer p-2 hover:bg-secondary-50 flex items-center gap-2 "
                    onClick={() => {
                      props.setAdding(false);
                      props.setActive(alert.id);
                    }}
                  >
                    <div className="w-3 h-3 bg-success-500 rounded-full"></div>
                    <span>{alert.name}</span>
                  </li>
                ))}
            </ul>
          </AccordionBody>
        </AccordionItem>
      </Accordion>
    </>
  );
}
