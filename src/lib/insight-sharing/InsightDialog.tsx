import React, { useMemo, useState } from 'react';
import { Dialog, DialogContent } from '../components';
import { MonitorType, Sidebar } from './components/Sidebar';
import { AddItem } from './components/AddItem';
import { useAppDispatch, useInsights } from '..';
import { StartScreen } from './components/StartScreen';
import { FormInsight } from './FormInsight';
import { wsCreateInsight, wsDeleteInsight, wsUpdateInsight } from '../data-access/broker/wsInsightSharing';
import { addInsight, deleteInsight, updateInsight } from '../data-access/store/insightSharingSlice';
import { addSuccess, addError } from '../data-access/store/configSlice';
import { InsightModel, InsightRequest } from 'ts-common';

type Props = {
  open: boolean;
  onClose: () => void;
};

export function InsightDialog(props: Props) {
  const dispatch = useAppDispatch();

  const [adding, setAdding] = useState<false | MonitorType>(false);
  const [active, setActive] = useState<number | undefined>();
  const insights = useInsights();

  const selectedInsight = useMemo(() => insights.find(i => i.id === active), [active, insights]);

  const handleDelete = async (insight: InsightModel) => {
    if (!insight.id) {
      dispatch(addError(`Cannot delete an ${insight.type} without an ID.`));
      return;
    }

    wsDeleteInsight({ id: insight.id }, (data, status) => {
      if (status === 'success') {
        dispatch(deleteInsight({ id: insight.id }));
        if (active === insight.id) {
          setActive(undefined);
        }
        dispatch(addSuccess(`${insight.type} deleted successfully`));
        console.debug(`${insight.type} deleted successfully`);
      } else {
        console.error(`Failed to delete ${insight.type}:`, data);
        dispatch(addError(`Failed to delete ${insight.type}`));
      }
    });
  };

  const handleSave = (insight: InsightModel | InsightRequest, generateEmail: boolean) => {
    if (Object.prototype.hasOwnProperty.call(insight, 'id')) {
      const updatedInsight = insight as InsightModel;
      wsUpdateInsight({ id: updatedInsight.id, insight: updatedInsight, generateEmail }, (data, status) => {
        if (status === 'success' && data) {
          dispatch(updateInsight(data));
          dispatch(addSuccess(`${insight.type} updated successfully`));
          console.log(`${insight.type} updated successfully`);
        } else {
          console.error(`Failed to update ${insight.type}:`, data);
          dispatch(addError(`Failed to update ${insight.type}.`));
        }
      });
    } else {
      const newInsight = insight as InsightRequest;
      wsCreateInsight(newInsight, (data, status) => {
        if (status === 'success' && data) {
          dispatch(addInsight(data));
          dispatch(addSuccess(`${insight.type} created successfully`));
          setActive(data.id);
        } else {
          console.error(`Failed to create ${insight.type}:`, data);
          dispatch(addError(`Failed to create ${insight.type}.`));
        }
      });
    }
  };

  return (
    <Dialog
      open={props.open}
      onOpenChange={ret => {
        if (!ret) props.onClose();
      }}
    >
      <DialogContent className="w-5/6 h-5/6 rounded-none py-0 px-0">
        <div className="flex w-full h-full">
          <div className="w-1/4 border-r overflow-auto flex flex-col h-full">
            <Sidebar setAdding={setAdding} setActive={setActive} />
          </div>

          <div className="w-3/4 p-8 overflow-y-auto">
            {adding ? (
              <AddItem setAdding={setAdding} setActive={setActive} type={adding} />
            ) : !selectedInsight ? (
              <StartScreen setAdding={setAdding} />
            ) : (
              <FormInsight insight={selectedInsight} setActive={setActive} handleDelete={handleDelete} handleSave={handleSave} />
            )}
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
}
