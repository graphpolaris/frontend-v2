import React, { useState } from 'react';
import { useAppDispatch, useProjects, useSessionCache } from '..';
import { Databases } from './database';
import { ManagementViews } from './ManagementDialog';
import { SaveState } from 'ts-common';
import { Breadcrumb } from '../components/breadcrumb/Breadcrumb';
import { setPath, setCurrentProject } from '../data-access/store/projectSlice';

type Props = {
  onClose: () => void;
  onViewChange: (val: ManagementViews) => void;
  setSelectedSaveState: (val: SaveState) => void;
};

export function Workspace({ onClose, onViewChange, setSelectedSaveState }: Props) {
  const session = useSessionCache();
  const dispatch = useAppDispatch();
  const project = useProjects();

  return (
    <div className="flex flex-col gap-4">
      <Breadcrumb
        paths={project.currentPath.map(p => p.name)}
        onNavigate={index => {
          const newPath = project.currentPath.slice(0, index + 1);
          dispatch(setPath(newPath));
          dispatch(setCurrentProject(newPath[newPath.length - 1] || null));
        }}
      />

      <div>
        <Databases
          saveStates={session.saveStates}
          changeActive={val => onViewChange(val)}
          setSelectedSaveState={setSelectedSaveState}
          onClose={onClose}
        />
      </div>
    </div>
  );
}
