import React from 'react';

export function Settings() {
  return (
    <div className="flex items-start w-full h-full p-6">
      <div className="w-1/3 flex flex-col items-center">
        <img src="" />
        <span className="text-primary font-semibold">Change logo</span>
      </div>
      <div className="w-2/3 flex flex-col gap-y-4">
        <div className="flex flex-col">
          <span className="font-bold">Company name</span>
          <div className="flex gap-x-4">
            <span>Stichting VbV</span>
            <span className="text-primary font-semibold">Change name</span>
          </div>
        </div>
        <div className="flex flex-col">
          <span className="font-bold">Plan and Billing</span>
          <div className="flex gap-x-4">
            <span>Standard</span>
            <span className="text-primary font-semibold">Go to billing</span>
          </div>
        </div>
      </div>
    </div>
  );
}
