import React, { useState } from 'react';
import { ManagementViews } from './ManagementDialog';
import { Tabs, Tab } from '../components/tabs';
import { Workspace } from './Workspace';
import { SaveState } from 'ts-common';

type Props = {
  onClose: () => void;
  onViewChange: (val: ManagementViews) => void;
  setSelectedSaveState: (val: SaveState) => void;
};

export function Overview({ onClose, onViewChange, setSelectedSaveState }: Props) {
  const [activeTab, setActiveTab] = useState<'workspace' | 'company' | 'settings' | 'members' | 'admin'>('workspace');

  return (
    <div className="flex flex-col h-[calc(100vh-20rem)] max-w-[calc(100vw-20rem)] overflow-y-auto overflow-x-hidden p-8 gap-4">
      <h1 className="text-4xl font-semibold">{/* <img className="w-52" src="" /> */}Company name</h1>
      <Tabs tabType="simple">
        <Tab text="Workspace" activeTab={activeTab === 'workspace'} onClick={() => setActiveTab('workspace')}></Tab>
        <Tab text="Company" activeTab={activeTab === 'company'} onClick={() => setActiveTab('company')}></Tab>
        {/* TODO investigate how to best integrate <Tab text="Members" activeTab={activeTab === 'members'} onClick={() => setActiveTab('members')}></Tab> */}
        <Tab text="Admin Only" activeTab={activeTab === 'admin'} onClick={() => setActiveTab('admin')} variant="outline"></Tab>
      </Tabs>

      <div>
        {activeTab === 'workspace' && (
          <Workspace onClose={onClose} onViewChange={onViewChange} setSelectedSaveState={setSelectedSaveState} />
        )}
        {/* {activeTab === 'members' && <Members />} */}
      </div>
    </div>
  );
}
