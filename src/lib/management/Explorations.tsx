import React from 'react';
import { Button } from '../components';

type Props = {
  changeActive: (val: 'add' | 'update') => void;
};

export function Explorations({ changeActive }: Props) {
  return (
    <div>
      <div className="flex gap-x-4 mb-4">
        <Button
          label="Project"
          variantType="primary"
          iconComponent="icon-[ic--baseline-add]"
          iconPosition="leading"
          onClick={() => changeActive('add')}
        />
        <Button
          label="Learn how to use"
          variant="ghost"
          iconComponent="icon-[mdi--play]"
          iconPosition="leading"
          onClick={() => window.open('https://graphpolaris.com/', '_blank')}
        />
      </div>
      <table className="w-full">
        <thead>
          <tr>
            <th scope="col" className="text-left">
              Name
            </th>
            <th scope="col" className="text-left">
              Last modified
            </th>
            <th scope="col" className="text-left">
              Created
            </th>
            <th scope="col"></th>
          </tr>
        </thead>
      </table>
    </div>
  );
}
