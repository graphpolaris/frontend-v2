import { useMemo, useState, useEffect } from 'react';
import {
  Button,
  Icon,
  nilUUID,
  useActiveQuery,
  useAppDispatch,
  useAuthCache,
  useAuthentication,
  useGraphQueryResult,
  useProjects,
  useSessionCache,
  wsDeleteState,
} from '../..';
import { clearQB, deleteSaveState, selectSaveState } from '../../data-access/store/sessionSlice';
import { clearSchema } from '../../data-access/store/schemaSlice';
import { useHandleDatabase } from './useHandleDatabase';
import { CountQueryResultFromBackend, SaveState } from 'ts-common';

// --- Add the imports for project functionality ---
import { addProject, setCurrentProject, setProjects, deleteProject, addToPath } from '../../data-access/store/projectSlice';
import { wsCreateProject, wsDeleteProject, wsGetProjects } from '@/lib/data-access/broker/wsProject';
import { Dialog, DialogContent, Input } from '../..'; // For the new-project dialog
import { DatabaseLine } from './DatabaseLine';

type Props = {
  onClose: () => void;
  saveStates: { [id: string]: SaveState };
  changeActive: (val: 'add' | 'update') => void;
  setSelectedSaveState: (val: SaveState) => void;
};

type DatabaseTableHeaderTypes = 'name' | 'protocol' | 'url' | 'alarms' | 'statistics' | 'created at';

const DataTableHeader = ({
  name,
  orderBy,
  setOrderBy,
}: {
  name: DatabaseTableHeaderTypes;
  orderBy: [DatabaseTableHeaderTypes, 'asc' | 'desc'];
  setOrderBy: (val: [DatabaseTableHeaderTypes, 'asc' | 'desc']) => void;
}) => {
  return (
    <th
      scope="col"
      className="text-left hover:underline cursor-pointer"
      onClick={() => setOrderBy([name, orderBy[1] === 'desc' && orderBy[0] === name ? 'asc' : 'desc'])}
    >
      <span className="flex items-center gap-0">
        {name}
        {orderBy[0] !== name && <div className="min-w-6 min-h-4 " />}
        {orderBy[0] === name && orderBy[1] === 'asc' && <Icon component="icon-[ic--baseline-arrow-drop-up]" />}
        {orderBy[0] === name && orderBy[1] === 'desc' && <Icon component="icon-[ic--baseline-arrow-drop-down]" />}
      </span>
    </th>
  );
};

export function Databases({ onClose, saveStates, changeActive, setSelectedSaveState }: Props) {
  const dispatch = useAppDispatch();
  const auth = useAuthentication();
  const authCache = useAuthCache();
  const session = useSessionCache();
  const databaseHandler = useHandleDatabase();
  const [orderBy, setOrderBy] = useState<[DatabaseTableHeaderTypes, 'asc' | 'desc']>(['name', 'desc']);
  const [sharing, setSharing] = useState<boolean>(false);
  const activeQuery = useActiveQuery();
  const queryResult = useGraphQueryResult();

  const projectState = useProjects();
  const [showNewProjectDialog, setShowNewProjectDialog] = useState(false);
  const [newProjectName, setNewProjectName] = useState('');
  const [showDifferentiation, setShowDifferentiation] = useState(false);
  const [showQueries, setShowQueries] = useState(false);

  const configVisuals = {
    classNameVisuals: 'h-10',
    marginPercentage: { top: 0.1, right: 0.07, left: 0.07, bottom: 0.1 },
    maxValue: 1,
  };

  // load projects from DB on path change
  useEffect(() => {
    wsGetProjects(authCache.authentication?.userID || -1, projectState.currentProject?.id ?? null, projects => {
      dispatch(setProjects(projects));
    });
  }, [projectState.currentPath]);

  const orderedProjects = useMemo(
    () =>
      Object.values(projectState.projects)
        .filter(project => project.parentId === (projectState.currentProject?.id ?? null))
        .sort((a, b) => {
          if (orderBy[1] === 'asc') {
            return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
          }
          return b.name.toLowerCase().localeCompare(a.name.toLowerCase());
        }),
    [projectState.projects, projectState.currentPath, orderBy],
  );

  const orderedSaveStates = useMemo(
    () =>
      Object.keys(saveStates)
        .filter(key => {
          const saveState = saveStates[key];
          const currentProjectId = projectState.currentProject?.id ?? null;

          // only looking at actual db entries
          if (!saveState.dbConnections?.length) {
            return false;
          }

          // show dbs withh no projects
          if (!currentProjectId) {
            return !saveState.project;
          }

          // show dbs of the project
          return String(saveState.project?.id) === String(currentProjectId);
        })
        .sort((a, b) => {
          const dir = orderBy[1] === 'asc' ? 1 : -1;
          if (orderBy[0] === 'name') {
            if (saveStates[a].name.toLowerCase() <= saveStates[b].name.toLowerCase()) return dir;
            else return -dir;
          } else {
            if (
              (orderBy[0] === 'protocol' || orderBy[0] === 'url') &&
              saveStates[a].dbConnections?.[0]?.[orderBy[0]].toLowerCase() <= saveStates[b].dbConnections?.[0]?.[orderBy[0]].toLowerCase()
            )
              return dir;
            else return -dir;
          }
        }),
    [saveStates, orderBy, projectState.currentPath],
  );

  const handleCreateProject = () => {
    if (newProjectName) {
      wsCreateProject(
        {
          name: newProjectName,
          parent: projectState.currentProject?.id ?? null,
          userID: authCache.authentication?.userID || -1,
        },
        (data, status) => {
          if (status === 'success' && data) {
            dispatch(addProject(data));
            setShowNewProjectDialog(false);
            setNewProjectName('');

            wsGetProjects(authCache.authentication?.userID || -1, projectState.currentProject?.id ?? null, projects => {
              dispatch(setProjects(projects));
            });
          }
        },
      );
    }
  };

  function differentiationFor(saveStateId: string, queryIndex: number = 0) {
    const saveState = session.saveStates[saveStateId];
    const query = saveState?.queryStates.openQueryArray[queryIndex];

    // For the current query, use the most up to date results from queryresultslice
    let nodeCountsObj: CountQueryResultFromBackend | undefined;
    if (activeQuery != null && activeQuery.id == query.id) {
      nodeCountsObj = queryResult.nodeCounts ?? query.graph?.nodeCounts;
    } else {
      nodeCountsObj = query.graph?.nodeCounts;
    }

    if (nodeCountsObj == null || saveState?.schemas[0]?.stats == null) return 0;

    const nodeCounts = query.graph.nodes
      .filter(x => x.attributes.type == 'entity')
      .map(x => ({ name: x.attributes.name, count: nodeCountsObj[`${x.key}_count`] ?? 0 }));

    const totalCounts = query.graph.nodes
      .filter(x => x.attributes.type == 'entity')
      .map(x => ({ name: x.attributes.name, count: saveState?.schemas[0].stats?.nodes?.stats[x.attributes.name]?.count ?? 0 }));

    const diffs = nodeCounts.map((x, i) => x.count / totalCounts[i].count);
    const score = diffs.reduce((a, b) => a + b) / diffs.length;

    return Math.round(score * 100) / 100;
  }

  function timestampFor(saveStateId: string, queryIndex: number = 0) {
    const saveState = session.saveStates[saveStateId];
    const query = saveState?.queryStates.openQueryArray[queryIndex];

    if (query.graph?.nodeCounts == null || query.graph.nodeCounts.updatedAt == null) return 'unknown';

    return new Date(query.graph.nodeCounts.updatedAt).toLocaleString('nl-NL', {
      month: '2-digit',
      day: '2-digit',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
    });
  }

  return (
    <div className="flex flex-col gap-4">
      <div className="flex justify-between items-start">
        <div className="flex gap-x-4">
          <Button
            label="Database"
            variantType="primary"
            iconComponent="icon-[ic--baseline-add]"
            iconPosition="leading"
            onClick={() => changeActive('add')}
          />
          <Button
            label="Project"
            variantType="primary"
            iconComponent="icon-[mdi--folder-plus]"
            iconPosition="leading"
            onClick={() => setShowNewProjectDialog(true)}
          />
          <Button
            label="Learn how to use"
            variant="ghost"
            iconComponent="icon-[mdi--play]"
            iconPosition="leading"
            onClick={() => window.open('https://graphpolaris.com/', '_blank')}
          />
        </div>
        <div className="flex flex-col gap-y-2 align-end">
          <Input type="toggle" label="Show differentiation" value={showDifferentiation} onChange={setShowDifferentiation} />
          <Input type="toggle" label="Show queries" value={showQueries} onChange={setShowQueries} />
        </div>
      </div>

      <Dialog open={showNewProjectDialog} onOpenChange={open => !open && setShowNewProjectDialog(false)}>
        <DialogContent>
          <div className="p-4">
            <h2 className="text-lg font-semibold mb-4">Create New Project</h2>
            <Input type="text" label="Project Name" value={newProjectName} onChange={setNewProjectName} placeholder="Enter project name" />
            <div className="flex justify-end gap-2 mt-4">
              <Button variant="outline" label="Cancel" onClick={() => setShowNewProjectDialog(false)} />
              <Button variantType="primary" label="Create" onClick={handleCreateProject} disabled={!newProjectName} />
            </div>
          </div>
        </DialogContent>
      </Dialog>

      <table className="w-full">
        <thead>
          <tr>
            <th></th>
            <DataTableHeader name="name" orderBy={orderBy} setOrderBy={setOrderBy} />
            {showDifferentiation && <DataTableHeader name="statistics" orderBy={orderBy} setOrderBy={setOrderBy} />}
            <DataTableHeader name="created at" orderBy={orderBy} setOrderBy={setOrderBy} />
            <th></th>
          </tr>
        </thead>
        <tbody className="divide-y divide-white overflow-auto table-fixed">
          {orderedProjects.map(project => (
            <tr key={project.id}>
              <td className="w-8">
                <Icon component="icon-[mdi--folder-outline]" size={20} className="" />
              </td>
              <td>
                <Button
                  variant="ghost"
                  onClick={() => {
                    dispatch(addToPath(project));
                    dispatch(setCurrentProject(project));
                  }}
                >
                  {project.name}
                </Button>
              </td>
              {showDifferentiation && <td></td>}
              <td className="font-light">{project.createdAt}</td>
              <td className="text-right flex justify-end">
                <Button
                  iconComponent="icon-[mdi--trash-outline]"
                  variant="ghost"
                  onClick={() => {
                    wsDeleteProject(project.id);
                    dispatch(deleteProject(project.id));
                  }}
                />
                <Button iconComponent="icon-[mi--options-vertical]" variant="ghost" />
              </td>
            </tr>
          ))}

          {orderedSaveStates.map(key => (
            <>
              <DatabaseLine
                saveState={saveStates[key]}
                onSelect={() => {
                  if (key !== session.currentSaveState) {
                    dispatch(clearSchema());
                    dispatch(selectSaveState(key));
                    onClose();
                  }
                }}
                onUpdate={() => {
                  changeActive('update');
                  setSelectedSaveState(saveStates[key]);
                }}
                onClone={() => {
                  databaseHandler.submitDatabaseChange(
                    { ...saveStates[key], name: saveStates[key].name + ' (copy)', id: nilUUID },
                    'add',
                    true,
                    () => {},
                  );
                  setSelectedSaveState(saveStates[key]);
                }}
                onDelete={() => {
                  if (session.currentSaveState === key) {
                    dispatch(clearQB());
                    dispatch(clearSchema());
                  }
                  wsDeleteState({ saveStateID: key });
                  dispatch(deleteSaveState(key));
                }}
                onShare={async () => {
                  setSharing(true);
                  await auth.shareLink();
                  setSharing(false);
                }}
                showDifferentiation={showDifferentiation}
                showQueries={showQueries}
              />
            </>
          ))}
        </tbody>
      </table>
    </div>
  );
}
