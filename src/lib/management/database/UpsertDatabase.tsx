import React, { useEffect, useRef, useState } from 'react';
import { useAuthCache, nilUUID } from '../../data-access';
import { Button } from '../../components/buttons';
import { DatabaseForm, INITIAL_SAVE_STATE } from './DatabaseForm';
import { SampleDatabaseSelector } from './MockSaveStates';
import { Icon } from '../..';
import { useHandleDatabase } from './useHandleDatabase';
import { PartialSaveState, SaveState } from 'ts-common';

export const UpsertDatabase = (props: {
  onClose(): void;
  open: 'add' | 'update';
  saveState: SaveState | null;
  disableCancel?: boolean;
}) => {
  const databaseHandler = useHandleDatabase();
  const ref = useRef<HTMLDialogElement>(null);
  const authCache = useAuthCache();
  const [formData, setFormData] = useState<PartialSaveState>(
    props.saveState && props.open === 'update'
      ? props.saveState
      : { ...INITIAL_SAVE_STATE, userId: authCache.authentication?.userID || -1, id: nilUUID },
  );
  const [hasError, setHasError] = useState(false);
  const [sampleDataPanel, setSampleDataPanel] = useState<boolean | null>(false);
  const formTitle = props.open === 'add' ? 'Add' : 'Update';

  useEffect(() => {
    if (props.saveState && props.open === 'update') {
      setFormData(props.saveState);
      setSampleDataPanel(null);
    } else {
      setSampleDataPanel(false);
    }
  }, [props.saveState]);

  async function handleSubmit(saveStateData?: PartialSaveState, forceAdd: boolean = false): Promise<void> {
    if (!saveStateData) saveStateData = formData;
    databaseHandler.submitDatabaseChange(saveStateData, props.open, forceAdd, () => {
      closeDialog();
    });
  }

  function closeDialog(): void {
    setFormData({ ...INITIAL_SAVE_STATE, userId: authCache.authentication?.userID || -1 });
    ref.current?.close();
    props.onClose();
  }

  return (
    <div className="lg:min-w-[50rem] p-6">
      <>
        {sampleDataPanel === true ? (
          <SampleDatabaseSelector
            onClick={data => {
              setHasError(false);
              handleSubmit({ ...data, userId: authCache.authentication?.userID || -1 });
            }}
          />
        ) : (
          <DatabaseForm
            data={formData}
            onChange={(data: PartialSaveState, error: boolean) => {
              setFormData({ ...data, id: formData.id });
              setHasError(error);
            }}
          />
        )}

        {!(databaseHandler.connectionStatus.status === null) && (
          <div className={`flex flex-col justify-center items-center`}>
            <div className="flex justify-center items-center">
              {databaseHandler.connectionStatus.verified === false && (
                <Icon component="icon-[ic--baseline-error-outline]" className="text-secondary-400" />
              )}
              <p className="font-light text-sm	text-secondary-400	">{databaseHandler.connectionStatus.status}</p>
            </div>
            {databaseHandler.connectionStatus.verified === null && <progress className="progress w-56"></progress>}
          </div>
        )}

        <div
          className={`pt-4 flex flex-row gap-3 card-actions w-full justify-between items-center ${sampleDataPanel === true && 'hidden'}`}
        >
          <div className="flex justify-between items-center gap-2">
            <Button
              variantType="primary"
              className="flex-grow"
              label={
                databaseHandler.connectionStatus.updating
                  ? formTitle === 'Add'
                    ? formTitle + 'ing...'
                    : formTitle.slice(0, -1) + 'ing...'
                  : formTitle
              }
              onClick={event => {
                event.preventDefault();
                handleSubmit();
              }}
              disabled={databaseHandler.connectionStatus.updating || hasError}
            />
            {props.open === 'update' && (
              <Button
                variantType="secondary"
                className="flex-grow"
                label={'Clone'}
                onClick={event => {
                  handleSubmit({ ...formData, name: formData.name + ' (copy)', id: nilUUID }, true);
                }}
                disabled={databaseHandler.connectionStatus.updating || hasError}
              />
            )}
          </div>
          <div className="flex justify-end align-center mx-2">
            <div>
              {sampleDataPanel === true ? (
                <Button variant="outline" label="Go back" onClick={() => setSampleDataPanel(false)} />
              ) : sampleDataPanel === false ? (
                <>
                  <h1 className="font-light text-xs">No data?</h1>
                  <p className="font-light text-sm cursor-pointer underline" onClick={() => setSampleDataPanel(true)}>
                    Try sample data
                  </p>
                </>
              ) : (
                ''
              )}
            </div>
          </div>
        </div>
      </>
    </div>
  );
};
