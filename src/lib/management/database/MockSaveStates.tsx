import React from 'react';
import { nilUUID } from '../../data-access/broker';
import { SaveState } from 'ts-common';
import { DatabaseType } from 'ts-common/src/model/webSocket/dbConnection';

export type SaveStateSampleI = SaveState & {
  subtitle: string;
  description: string;
};

export const sampleSaveStates: Partial<SaveStateSampleI>[] = [
  {
    id: nilUUID,
    name: 'Recommendations',
    subtitle: 'Hosted by Neo4j',
    description: 'Network of movies, actors, directors and reviews by people',
    dbConnections: [
      {
        id: -1,
        username: 'recommendations',
        password: 'recommendations',
        url: 'demo.neo4jlabs.com',
        port: 7687,
        protocol: 'neo4j+s://',
        internalDatabaseName: 'recommendations',
        type: DatabaseType.Neo4j,
      },
    ],
    schemas: [],
    visualizations: { activeVisualizationIndex: -1, openVisualizationArray: [] },
    userId: -1,
    createdAt: '',
    updatedAt: '',
    project: null,
  },
  {
    id: nilUUID,
    name: 'Movies',
    subtitle: 'Hosted by Neo4j',
    description: 'Movies and people related to those movies as actors, directors and producers',
    dbConnections: [
      {
        id: -1,
        username: 'movies',
        password: 'movies',
        url: 'demo.neo4jlabs.com',
        port: 7687,
        protocol: 'neo4j+s://',
        internalDatabaseName: 'movies',
        type: DatabaseType.Neo4j,
      },
    ],
    schemas: [],
    visualizations: { activeVisualizationIndex: -1, openVisualizationArray: [] },
    userId: -1,
    createdAt: '',
    updatedAt: '',
    project: null,
  },
  {
    id: nilUUID,
    name: 'Northwind',
    subtitle: 'Hosted by Neo4j',
    description: 'Retail-system with products, orders, customers, suppliers and employees',
    dbConnections: [
      {
        id: -1,
        username: 'northwind',
        password: 'northwind',
        url: 'demo.neo4jlabs.com',
        port: 7687,
        protocol: 'neo4j+s://',
        internalDatabaseName: 'northwind',
        type: DatabaseType.Neo4j,
      },
    ],
    schemas: [],
    visualizations: { activeVisualizationIndex: -1, openVisualizationArray: [] },
    userId: -1,
    createdAt: '',
    updatedAt: '',
    project: null,
  },
  {
    id: nilUUID,
    name: 'Fincen',
    subtitle: 'Hosted by Neo4j',
    description: 'FinCEN files investigation for banks and countries',
    dbConnections: [
      {
        id: -1,
        username: 'fincen',
        password: 'fincen',
        url: 'demo.neo4jlabs.com',
        port: 7687,
        protocol: 'neo4j+s://',
        internalDatabaseName: 'fincen',
        type: DatabaseType.Neo4j,
      },
    ],
    schemas: [],
    visualizations: { activeVisualizationIndex: -1, openVisualizationArray: [] },
    userId: -1,
    createdAt: '',
    updatedAt: '',
    project: null,
  },
  {
    id: nilUUID,
    name: 'Game of Thrones',
    subtitle: 'Hosted by Neo4j',
    description: 'Character interactions and actors in the Game of Thrones movie',
    dbConnections: [
      {
        id: -1,
        username: 'gameofthrones',
        password: 'gameofthrones',
        url: 'demo.neo4jlabs.com',
        port: 7687,
        protocol: 'neo4j+s://',
        internalDatabaseName: 'gameofthrones',
        type: DatabaseType.Neo4j,
      },
    ],
    schemas: [],
    visualizations: { activeVisualizationIndex: -1, openVisualizationArray: [] },
    userId: -1,
    createdAt: '',
    updatedAt: '',
    project: null,
  },
];

export const SampleDatabaseSelector = (props: { onClick: (data: SaveState) => void }) => {
  return (
    <div className="flex flex-wrap gap-2">
      {sampleSaveStates.map(sample => (
        <div
          key={sample.name}
          className="card hover:bg-secondary-100 cursor-pointer mb-2 border w-[15rem] flex-grow "
          onClick={() => props.onClick(sample as SaveState)}
        >
          <div className="card-body">
            <h2 className="card-title">{sample.name}</h2>
            <p className="font-light text-secondary-400">{sample.subtitle}</p>
            <p className="font-light text-xs text-secondary-400">{sample.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
};
