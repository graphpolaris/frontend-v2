import { wsTestDatabaseConnection, wsCreateState, wsUpdateState, useAuthCache, useAppDispatch, useProjects } from '../../data-access';
import { useState } from 'react';
import { addNewQuery, addSaveState, selectSaveState, setActiveQueryID, testedSaveState } from '../../data-access/store/sessionSlice';
import { setSchemaLoading } from '../../data-access/store/schemaSlice';
import { PartialSaveState, SaveState } from 'ts-common';
import { addError } from '@/lib/data-access/store/configSlice';
import { Button } from '@/lib/components';
import { wsAddQuery, wsDeleteQuery, wsUpdateQuery } from '@/lib/data-access/broker';
import { queryAllByAltText } from '@testing-library/react';

export type ConnectionStatus = {
  updating: boolean;
  status: null | string;
  verified: boolean | null;
};

export const useHandleDatabase = () => {
  const dispatch = useAppDispatch();
  const [connectionStatus, setConnectionStatus] = useState<ConnectionStatus>({
    updating: false,
    status: null,
    verified: null,
  });
  const authCache = useAuthCache();
  const project = useProjects();

  async function submitDatabaseChange(
    saveStateData: PartialSaveState,
    type: 'add' | 'update',
    forceAdd: boolean = false,
    concludedCallback: () => void,
  ): Promise<void> {
    setConnectionStatus(() => ({
      updating: true,
      status: 'Testing database connection',
      verified: null,
    }));

    const currentProject = project.currentProject;
    if (currentProject) {
      saveStateData.project = currentProject;
    } else {
      saveStateData.project = null;
    }

    wsTestDatabaseConnection(saveStateData.dbConnections?.[0], data => {
      if (!saveStateData) {
        console.error('formData is null');
        return;
      }
      if (saveStateData.userId !== authCache.authentication?.userID && authCache.authentication?.userID) {
        console.error('user_id is not equal to auth.userID');
        saveStateData.userId = authCache.authentication.userID;
      }
      if (data && data.status === 'success') {
        setConnectionStatus(prevState => ({
          updating: false,
          status: 'Database connection verified',
          verified: true,
        }));
        dispatch(setSchemaLoading(true));
        dispatch(selectSaveState(undefined));
        if (type === 'add' || forceAdd) {
          // Reset query state, backend should handle creating this anew
          const data = JSON.parse(JSON.stringify(saveStateData)) as any; // undo readonly
          const queryStateToClone = JSON.parse(JSON.stringify(saveStateData.queryStates ?? {}));
          delete data.queryStates;

          wsCreateState(data, async newSaveState => {
            if (!newSaveState) {
              dispatch(addError('Failed to create new save state.'));
              return;
            }

            // Only if we are adding a save state that is cloned and contains a queryState
            if (saveStateData.queryStates != null) {
              // Clone query state
              for (const i in queryStateToClone.openQueryArray) {
                const query = queryStateToClone.openQueryArray[i];
                delete query.id;

                await new Promise<void>((resolve, reject) => {
                  if (Number(i) == 0) {
                    query.id = newSaveState.queryStates.openQueryArray[0].id;
                    queryStateToClone.activeQuery = query.id;
                    wsUpdateQuery({ saveStateID: newSaveState.id, query: query }, query => {
                      if (query == null) return reject();
                      return resolve();
                    });
                  } else {
                    wsAddQuery({ saveStateID: newSaveState.id }, newQuery => {
                      if (newQuery == null) {
                        return reject('Failed to create new save state (query state).');
                      }
                      query.id = newQuery.id;
                      queryStateToClone.activeQueryId = query.id;

                      wsUpdateQuery({ saveStateID: newSaveState.id, query: query }, query => {
                        if (query == null) reject();
                        return resolve();
                      });
                    });
                  }
                });
              }

              newSaveState.queryStates = queryStateToClone;
            }

            dispatch(addSaveState(newSaveState));
            dispatch(testedSaveState(newSaveState.id));
            setConnectionStatus({
              updating: false,
              status: null,
              verified: null,
            });
            concludedCallback();
          });
        } else {
          if (data.saveStateID) {
            dispatch(testedSaveState(data.saveStateID));
          }
          wsUpdateState(saveStateData as SaveState, updatedSaveState => {
            if (!updatedSaveState) {
              dispatch(addError('Failed to update save state.'));
              return;
            }
            dispatch(addSaveState(updatedSaveState));
            setConnectionStatus({
              updating: false,
              status: null,
              verified: null,
            });
            concludedCallback();
          });
        }
      } else {
        setConnectionStatus(prevState => ({
          updating: false,
          status: 'Database connection test failed',
          verified: false,
        }));
      }
    });
  }
  return { submitDatabaseChange, connectionStatus };
};
