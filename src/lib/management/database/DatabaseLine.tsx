import {
  Icon,
  Button,
  Popover,
  PopoverTrigger,
  PopoverContent,
  DropdownItem,
  TooltipProvider,
  Tooltip,
  TooltipTrigger,
  TooltipContent,
} from '@/lib/components';
import { Heatmap1D } from '@/lib/components/charts/heatmap1D';
import { IndicatorGraph } from '@/lib/components/charts/indicatorGraph';
import { wsDeleteState, nilUUID, useSessionCache, useAppDispatch, useActiveQuery, useGraphQueryResult } from '@/lib/data-access';
import { clearSchema } from '@/lib/data-access/store/schemaSlice';
import { selectSaveState, clearQB, deleteSaveState } from '@/lib/data-access/store/sessionSlice';
import { CountQueryResultFromBackend, GraphQueryResultMetaFromBackend, Query, SaveState } from 'ts-common';

// Update props to include missing functions and optional sharing flag.
interface DatabaseLineProps {
  saveState: SaveState;
  onSelect: () => void;
  onUpdate: () => void;
  onClone: () => void;
  onShare: () => void;
  onDelete: () => void;
  sharing?: boolean;
  showDifferentiation: boolean;
  showQueries: boolean;
}

const configVisuals = {
  classNameVisuals: 'h-10',
  marginPercentage: { top: 0.1, right: 0.07, left: 0.07, bottom: 0.1 },
  maxValue: 1,
};

function timestampFor(saveState: SaveState, queryIndex: number = 0) {
  const query = saveState.queryStates.openQueryArray[queryIndex];
  if (query.graph?.nodeCounts == null || query.graph.nodeCounts.updatedAt == null) return 'unknown';
  return new Date(query.graph.nodeCounts.updatedAt).toLocaleString('nl-NL', {
    month: '2-digit',
    day: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
  });
}

function differentiationFor(
  saveState: SaveState,
  queryIndex: number = 0,
  queryResult: GraphQueryResultMetaFromBackend,
  activeQuery?: Query,
): number {
  const query = saveState.queryStates.openQueryArray[queryIndex];
  // Use the most up-to-date counts based on active query.
  let nodeCountsObj: CountQueryResultFromBackend | undefined;
  if (activeQuery?.id === query.id) {
    nodeCountsObj = queryResult.nodeCounts ?? query.graph?.nodeCounts;
  } else {
    nodeCountsObj = query.graph?.nodeCounts;
  }
  if (nodeCountsObj == null || saveState?.schemas[0]?.stats == null || !nodeCountsObj.updatedAt || nodeCountsObj.updatedAt === 0) return 0;
  const nodeCounts = query.graph.nodes
    .filter(x => x.attributes.type === 'entity')
    .map(x => ({ name: x.attributes.name, count: nodeCountsObj[`${x.key}_count`] ?? 0 }));
  const totalCounts = query.graph.nodes
    .filter(x => x.attributes.type === 'entity')
    .map(x => ({ name: x.attributes.name, count: saveState?.schemas[0].stats?.nodes?.stats[x.attributes.name]?.count ?? 0 }));
  const diffs = nodeCounts.map((x, i) => x.count / totalCounts[i].count);
  const score = diffs.reduce((a, b) => a + b) / diffs.length;
  return Math.round(score * 100) / 100;
}

export const DatabaseLine = (props: DatabaseLineProps) => {
  const session = useSessionCache();
  const dispatch = useAppDispatch();
  const activeQuery = useActiveQuery();
  const queryResult = useGraphQueryResult();
  const sharing = props.sharing ?? false;

  return (
    <>
      <tr>
        <td className="w-8">
          <Icon component="icon-[mdi--database-outline]" size={20} />
        </td>
        <td>
          <Button
            variant={session.currentSaveState === props.saveState.id ? 'outline' : 'ghost'}
            onClick={() => {
              if (props.saveState.id !== session.currentSaveState) {
                dispatch(clearSchema());
                dispatch(selectSaveState(props.saveState.id));
                props.onSelect();
              }
            }}
          >
            {props.saveState.name}
          </Button>
        </td>
        {props.showDifferentiation && (
          <td className="text-left">
            <Heatmap1D
              data={props.saveState.queryStates.openQueryArray.map((_, qi) =>
                differentiationFor(props.saveState, qi, queryResult, activeQuery),
              )}
              numBins={40}
              maxValue={configVisuals.maxValue}
              marginPercentage={configVisuals.marginPercentage}
              ticks={true}
              className={configVisuals.classNameVisuals}
            />
          </td>
        )}
        <td className="text-left">
          <span className="font-light">{props.saveState.createdAt}</span>
        </td>
        <td className="text-right flex justify-end">
          <Button
            iconComponent="icon-[mdi--trash-outline]"
            variant="ghost"
            onClick={() => {
              if (session.currentSaveState === props.saveState.id) {
                dispatch(clearQB());
                dispatch(clearSchema());
              }
              wsDeleteState({ saveStateID: props.saveState.id });
              dispatch(deleteSaveState(props.saveState.id));
            }}
          />
          <Popover>
            <PopoverTrigger>
              <Button iconComponent="icon-[mi--options-vertical]" variant="ghost" />
            </PopoverTrigger>
            <PopoverContent className="p-1 min-w-24">
              <DropdownItem
                value="Open"
                onClick={() => {
                  props.onSelect();
                }}
              />
              <DropdownItem
                value="Update"
                onClick={() => {
                  props.onUpdate();
                }}
              />
              <DropdownItem
                value="Clone"
                onClick={() => {
                  props.onClone();
                }}
              />
              <DropdownItem
                value={sharing ? 'Creating Share Link' : 'Share'}
                disabled={sharing}
                onClick={async () => {
                  props.onShare();
                }}
              />
              <DropdownItem
                value="Delete"
                onClick={() => {
                  props.onDelete();
                }}
                className="text-danger"
              />
            </PopoverContent>
          </Popover>
        </td>
      </tr>
      {props.showQueries &&
        props.saveState.queryStates.openQueryArray.map((query, qi) => (
          <tr key={qi}>
            <td className="w-8">
              <Icon component="icon-[mdi--chevron-right]" size={20} />
            </td>
            <td>{query.name}</td>
            {props.showDifferentiation && (
              <td className="text-left">
                <TooltipProvider delay={300}>
                  <Tooltip placement="right">
                    <TooltipTrigger>
                      <IndicatorGraph
                        data={differentiationFor(props.saveState, qi, queryResult, activeQuery)}
                        maxValue={configVisuals.maxValue}
                        marginPercentage={configVisuals.marginPercentage}
                        ticks={true}
                        className={configVisuals.classNameVisuals}
                      />
                    </TooltipTrigger>
                    <TooltipContent>Last updated at: {timestampFor(props.saveState, qi)}</TooltipContent>
                  </Tooltip>
                </TooltipProvider>
              </td>
            )}
          </tr>
        ))}
    </>
  );
};
