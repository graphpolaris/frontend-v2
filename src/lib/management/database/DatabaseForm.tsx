import React, { useEffect, useState } from 'react';
import { databaseProtocolMapping, Input, nilUUID } from '../..';
import { PartialSaveState, SaveState } from 'ts-common';
import { DatabaseType, DatabaseTypeArray } from 'ts-common/src/model/webSocket/dbConnection';

export const INITIAL_SAVE_STATE: PartialSaveState = {
  name: 'Untitled',
  dbConnections: [
    {
      id: -1,
      username: 'neo4j',
      password: 'DevOnlyPass',
      url: 'localhost',
      port: 7687,
      protocol: 'neo4j+s://',
      internalDatabaseName: 'neo4j',
      type: DatabaseType.Neo4j,
    },
  ],
  schemas: [],
  visualizations: { activeVisualizationIndex: -1, openVisualizationArray: [] },
  createdAt: '',
  updatedAt: '',
  userId: -1,
  project: null,
};

export const DatabaseForm = (props: { data: PartialSaveState; onChange: (data: PartialSaveState, error: boolean) => void }) => {
  const [formData, setFormData] = useState(props.data);
  const [hasError, setHasError] = useState<Record<string, boolean>>({});

  function handlePortChanged(port: string): void {
    if (!isNaN(Number(port))) {
      const updatedFormData = { ...formData };
      updatedFormData.dbConnections[0].port = Number(port);
      setFormData(updatedFormData);
    }
  }

  useEffect(() => {
    props.onChange(formData, Object.values(hasError).includes(true));
  }, [formData]);

  return (
    <div className="flex flex-col gap-4 my-2">
      <Input
        type="text"
        label="Name of database connection"
        value={formData.name}
        onChange={(value: string) => setFormData({ ...formData, name: value })}
      />

      <Input
        type="text"
        label="Internal database name"
        value={formData.dbConnections[0].internalDatabaseName}
        placeholder="internalDatabaseName"
        required
        errorText="This field is required"
        validate={v => {
          setHasError({ ...hasError, internalDatabaseName: v.length === 0 });
          return v.length > 0;
        }}
        onChange={(value: string) =>
          setFormData({
            ...formData,
            dbConnections: [
              {
                ...formData.dbConnections[0],
                internalDatabaseName: value,
              },
            ],
          })
        }
      />

      <div className="flex w-full gap-2">
        <Input
          type="dropdown"
          className="w-full"
          label="Database Type"
          required
          value={formData.dbConnections[0].type}
          options={DatabaseTypeArray}
          onChange={value => {
            setFormData({
              ...formData,
              dbConnections: [
                {
                  ...formData.dbConnections[0],
                  type: value as any,
                  protocol: databaseProtocolMapping[value as DatabaseType][0],
                },
              ],
            });
          }}
        />

        <Input
          type="dropdown"
          label="Database Protocol"
          required
          value={formData.dbConnections[0].protocol}
          options={databaseProtocolMapping[formData.dbConnections[0].type]}
          info="Protocol via which the database connection will be established"
          onChange={value => {
            setFormData({
              ...formData,
              dbConnections: [
                {
                  ...formData.dbConnections[0],
                  protocol: value as any,
                },
              ],
            });
          }}
        />
      </div>

      <div className="flex w-full gap-2">
        <Input
          type="text"
          label="Hostname/IP"
          value={formData.dbConnections[0].url}
          placeholder="neo4j"
          required
          errorText="This field is required"
          info="Connection URL of your database"
          validate={v => {
            setHasError({ ...hasError, url: v.length === 0 });
            return v.length > 0;
          }}
          onChange={(value: string) => {
            setFormData({
              ...formData,
              dbConnections: [
                {
                  ...formData.dbConnections[0],
                  url: value,
                },
              ],
            });
          }}
        />

        <Input
          type="text"
          label="Port"
          value={formData.dbConnections[0].port.toString()}
          placeholder="neo4j"
          required
          errorText="Must be between 1 and 9999"
          info="The port is endpoint of service for communication purposes"
          validate={v => {
            setHasError({ ...hasError, port: !(v <= 9999 && v > 0) });
            return v <= 9999 && v > 0;
          }}
          onChange={(value: string) => {
            setFormData({
              ...formData,
              dbConnections: [
                {
                  ...formData.dbConnections[0],
                  port: Number(value),
                },
              ],
            });
          }}
        />
      </div>

      <div className="flex w-full gap-2">
        <Input
          type="text"
          label="Username"
          value={formData.dbConnections[0].username}
          placeholder="username"
          required
          errorText="This field is required"
          info="Username of your database instance"
          validate={v => {
            setHasError({ ...hasError, username: v.length === 0 });
            return v.length > 0;
          }}
          onChange={(value: string) => {
            setFormData({
              ...formData,
              dbConnections: [
                {
                  ...formData.dbConnections[0],
                  username: value,
                },
              ],
            });
          }}
        />

        <Input
          type="text"
          visible={false}
          label="Password"
          value={formData.dbConnections[0].password}
          placeholder="password"
          required
          errorText="This field is required"
          info="Password of your database instance"
          validate={v => {
            setHasError({ ...hasError, password: v.length === 0 });
            return v.length > 0;
          }}
          onChange={(value: string) => {
            setFormData({
              ...formData,
              dbConnections: [
                {
                  ...formData.dbConnections[0],
                  password: value,
                },
              ],
            });
          }}
        />
      </div>
    </div>
  );
};
