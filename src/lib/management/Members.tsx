import React, { useState } from 'react';
import { Button } from '../components/buttons';
import { useDialogContext } from '../components/layout/Dialog';
import { useUsersPolicy } from '../data-access/store';
import { useDispatch } from 'react-redux';
import { Icon } from '..';
import { setUsersPolicy } from '../data-access/store/authorizationUsersSlice';
import { TableUI } from '../components/tableUI/TableUI';
import { UserPolicy } from 'ts-common';

interface UserManagementContentProps {
  onConfirm?: (users: { name: string; email: string; type: string }[]) => void;
  onClickShareLink?: () => void;
  onClose?: () => void;
}

interface FieldConfig<T> {
  key: keyof T;
  label: string;
  type: 'text' | 'dropdown';
  required?: boolean;
}

export const Members: React.FC<UserManagementContentProps> = ({ onConfirm, onClickShareLink, onClose }) => {
  const { setOpen } = useDialogContext();
  const dispatch = useDispatch();

  // !FIXME: This should definited at high level
  const optionsTypeUser = ['', 'Creator', 'Viewer'];

  const usersPolicy = useUsersPolicy();
  // !FIXME: This should be populated from the store
  const userFields = [
    { key: 'name', label: 'Name', type: 'text', required: true },
    { key: 'email', label: 'Email', type: 'text', required: true },
    { key: 'type', label: 'Type', type: 'dropdown', required: true },
  ] as FieldConfig<UserPolicy>[];

  const options = {
    type: optionsTypeUser,
  };

  const handleUserChange = (newUsers: UserPolicy[]) => {
    dispatch(setUsersPolicy({ users: newUsers }));
  };

  return (
    <div className="flex flex-col p-4">
      <div className="bg-primary-50 flex items-center px-2 gap-x-2 my-2">
        <Icon component="icon-[ic--outline-info]" size={14} />
        <span className="font-light text-sm">These members have access to all projects within your company</span>
      </div>

      <div className="flex flex-col items-center flex-grow">
        <TableUI data={usersPolicy.users} fieldConfigs={userFields} dropdownOptions={options} onDataChange={handleUserChange} />
      </div>

      <div className="flex justify-center p-2 mt-auto">
        <div className="flex space-x-4">
          <Button variant="outline" size="md" label="Add row" onClick={() => {}} />
          <Button
            variantType="primary"
            size="md"
            label="Save"
            onClick={() => {
              if (onConfirm) onConfirm(usersPolicy.users);
              if (onClose) onClose();
            }}
          />
        </div>
      </div>
    </div>
  );
};
