import React, { useState, useEffect, useCallback } from 'react';
import { addError } from '../data-access/store/configSlice';
import { clearQB, selectSaveState } from '../data-access/store/sessionSlice';
import { clearSchema } from '../data-access/store/schemaSlice';
import { ManagementDialog, ManagementViews } from './ManagementDialog';
import { DatabaseStatus, Icon, LoadingSpinner, nilUUID, TooltipProvider, useAppDispatch, useSchemaGraph, useSessionCache } from '..';
import { Tooltip, TooltipContent, TooltipTrigger } from '@/lib/components/tooltip';

type Props = {
  managementOpen: boolean;
  setManagementOpen: (val: boolean) => void;
  current: ManagementViews;
  setCurrent: (val: ManagementViews) => void;
};

export function ManagementTrigger({ managementOpen, setManagementOpen, current, setCurrent }: Props) {
  const dispatch = useAppDispatch();
  const session = useSessionCache();
  const schemaGraph = useSchemaGraph();
  const [connecting, setConnecting] = useState<boolean>(false);

  useEffect(() => {
    setConnecting(false);
  }, [schemaGraph]);

  useEffect(() => {
    let timeoutId: ReturnType<typeof setTimeout>;
    if (connecting) {
      timeoutId = setTimeout(() => {
        dispatch(addError("Couldn't establish connection"));
        setConnecting(false);
        dispatch(selectSaveState(undefined));
        dispatch(clearQB());
        dispatch(clearSchema());
      }, 10000);
    }

    return () => {
      if (timeoutId) clearTimeout(timeoutId);
    };
  }, [connecting]);

  function classForDatabaseStatus(status?: DatabaseStatus) {
    switch (status) {
      case DatabaseStatus.online:
        return 'bg-success-500';
      case DatabaseStatus.offline:
        return 'bg-danger-500';
      case DatabaseStatus.testing:
        return 'bg-warning-500';
    }

    return 'hidden';
  }

  return (
    <div className="database-menu">
      <ManagementDialog
        open={managementOpen}
        onClose={() => setManagementOpen(!managementOpen)}
        current={current}
        setCurrent={setCurrent}
      />
      <div
        className="flex items-center cursor-pointer border rounded hover:bg-secondary-100 transition-colors duration-300 py-1 px-2"
        onClick={() => setManagementOpen(true)}
      >
        {connecting && session.currentSaveState && session.currentSaveState in session.saveStates ? (
          <>
            <LoadingSpinner />
            <p className="ml-2 truncate">Connecting to {session.saveStates[session.currentSaveState].name}</p>
          </>
        ) : session.currentSaveState && session.currentSaveState in session.saveStates && session.currentSaveState !== nilUUID ? (
          <div className="flex">
            <TooltipProvider delay={200}>
              <Tooltip>
                <TooltipTrigger>
                  <Icon
                    component="icon-[mdi--database-outline]"
                    size={20}
                    className="self-center -mb-1 text-secondary-700 hover:text-secondary-500"
                  />
                </TooltipTrigger>
                {session && session.currentSaveState && (
                  <TooltipContent>
                    <div className="flex flex-col">
                      <span className="text-sm font-semibold">Connection details</span>
                      <table className="border-separate border-spacing-1">
                        <tbody>
                          <tr>
                            <td className="font-semibold opacity-80">Name</td>
                            <td>{session.saveStates[session.currentSaveState].name}</td>
                          </tr>
                          <tr>
                            <td className="font-semibold opacity-80">Database</td>
                            <td>{session.saveStates[session.currentSaveState].dbConnections?.[0]?.internalDatabaseName}</td>
                          </tr>
                          <tr>
                            <td className="font-semibold opacity-80">Protocol</td>
                            <td>{session.saveStates[session.currentSaveState].dbConnections?.[0]?.protocol}</td>
                          </tr>
                          <tr>
                            <td className="font-semibold opacity-80">Hostname</td>
                            <td>{session.saveStates[session.currentSaveState].dbConnections?.[0]?.url}</td>
                          </tr>
                          <tr>
                            <td className="font-semibold opacity-80">Port</td>
                            <td className="">{session.saveStates[session.currentSaveState].dbConnections?.[0]?.port}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </TooltipContent>
                )}
              </Tooltip>
            </TooltipProvider>
            <span className="relative">
              <span
                className={`absolute bottom-0.5 right-3 h-2 w-2 border border-light  rounded-full class ${classForDatabaseStatus(session.testedSaveState[session.currentSaveState])}`}
              />
            </span>
            <p className="ml-1 truncate">{session.saveStates[session.currentSaveState].name}</p>
          </div>
        ) : session.saveStates === undefined ? (
          <>
            <LoadingSpinner />
            <p className="ml-2">Retrieving databases</p>
          </>
        ) : Object.keys(session.saveStates).length === 0 || session.currentSaveState === nilUUID ? (
          <>
            <p className="ml-2">Add your first Database</p>
          </>
        ) : (
          <>
            <div className="h-2 w-2 rounded-full bg-secondary-500" />
            <p className="ml-2">Select a database</p>
          </>
        )}
      </div>
    </div>
  );
}
