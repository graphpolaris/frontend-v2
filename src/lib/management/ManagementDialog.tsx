import React, { useState } from 'react';
import { Settings } from './Settings';
import { Overview } from './Overview';
import { Members } from './Members';
import { UpsertDatabase } from './database';
import { Button, Dialog, DialogContent, useAppDispatch, useAuthCache, useSessionCache } from '..';
import { addInfo } from '../data-access/store/configSlice';
import { SaveState } from 'ts-common';

type Props = {
  open: boolean;
  onClose: () => void;
  current: ManagementViews;
  setCurrent: (val: ManagementViews) => void;
};

export type ManagementViews = 'overview' | 'settings' | 'members' | 'add' | 'update';

export function ManagementDialog(props: Props) {
  const dispatch = useAppDispatch();
  const session = useSessionCache();
  const authCache = useAuthCache();
  const [selectedSaveState, setSelectedSaveState] = useState<SaveState | null>(null);

  const dialogMessage = (() => {
    switch (props.current) {
      case 'add':
        return 'Add Database';
      case 'update':
        return 'Updating Database Connection';
      case 'members':
        return 'Manage Members';
      default:
        return '';
    }
  })();

  const handleClose = () => {
    props.onClose();
    props.setCurrent('overview');
  };

  const handleConfirmUsers = (users: { name: string; email: string; type: string }[]) => {
    //TODO !FIXME: when the user clicks on confirm, users state is ready to be sent to backend
  };

  const handleClickShareLink = () => {
    //TODO !FIXME: add copy link to clipboard functionality
    dispatch(addInfo('Link copied to clipboard'));
  };

  return (
    <Dialog
      open={props.open}
      onOpenChange={ret => {
        if (!ret) handleClose();
      }}
    >
      <DialogContent className="border bg-light w-8/12 flex-grow mx-auto px-0 py-0">
        {props.current === 'overview' ? (
          <Overview onViewChange={props.setCurrent} setSelectedSaveState={setSelectedSaveState} onClose={() => handleClose()} />
        ) : (
          <div>
            <div className="flex justify-between items-center border-b p-4">
              <span className="text-secondary font-semibold">{dialogMessage}</span>
              <Button iconComponent="icon-[ic--outline-close]" variant="ghost" onClick={() => handleClose()} />
            </div>
            {props.current === 'settings' ? (
              <Settings />
            ) : props.current === 'members' ? (
              <Members
                onConfirm={handleConfirmUsers}
                onClickShareLink={handleClickShareLink}
                onClose={() => {
                  handleClose();
                }}
              />
            ) : (
              <UpsertDatabase
                open={props.current}
                saveState={props.current === 'update' ? selectedSaveState : null}
                disableCancel={
                  (session.saveStates && Object.keys(session.saveStates).length === 0) ||
                  session.currentSaveState === '00000000-0000-0000-0000-000000000000'
                }
                onClose={() => {
                  handleClose();
                }}
              />
            )}
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
}
