import React from 'react';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { App } from './App';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import * as Sentry from '@sentry/react';
import { store } from '@/lib/data-access/store';
import { ErrorBoundary } from '@/lib/components/errorBoundary';
import { getEnvVariable } from './config';
import './main.css';

if (getEnvVariable('SENTRY_ENABLED') === 'true') {
  Sentry.init({
    dsn: getEnvVariable('SENTRY_URL'),
    integrations: [Sentry.browserTracingIntegration(), Sentry.replayIntegration()],
    tracesSampleRate: 1.0,
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
  });
}

(window as any).global = window;
const domNode = document.getElementById('root');

if (domNode) {
  const root = createRoot(domNode);
  root.render(
    <StrictMode>
      <Provider store={store}>
        <Router>
          <ErrorBoundary fallback={<div>Oops! Something went wrong. Please try again.</div>}>
            <Routes>
              <Route path="/" element={<App load={undefined} />}></Route>
              <Route path="/fraud" element={<App load="5bdf3354-673f-4dec-b6a0-196e67cd211c" />}></Route>
            </Routes>
          </ErrorBoundary>
        </Router>
      </Provider>
    </StrictMode>,
  );
}
