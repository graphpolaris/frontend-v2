/// <reference types="vite/client" />

export const envVar = {
  GRAPHPOLARIS_VERSION: import.meta.env.GRAPHPOLARIS_VERSION,
  BACKEND_URL: import.meta.env.BACKEND_URL,
  BACKEND_WSS_URL: import.meta.env.BACKEND_WSS_URL,
  SKIP_LOGIN: import.meta.env.SKIP_LOGIN,
  BACKEND_USER: import.meta.env.BACKEND_USER,

  SENTRY_ENABLED: import.meta.env.SENTRY_ENABLED,
  SENTRY_URL: import.meta.env.SENTRY_URL,

  GP_AUTH_URL: import.meta.env.GP_AUTH_URL,

  LINK_PREDICTION: import.meta.env.LINK_PREDICTION,
  CENTRALITY: import.meta.env.CENTRALITY,
  COMMUNITY_DETECTION: import.meta.env.COMMUNITY_DETECTION,
  SHORTEST_PATH: import.meta.env.SHORTEST_PATH,

  TABLEVIS: import.meta.env.TABLEVIS,
  NODELINKVIS: import.meta.env.NODELINKVIS,
  RAWJSONVIS: import.meta.env.RAWJSONVIS,
  PAOHVIS: import.meta.env.PAOHVIS,
  MATRIXVIS: import.meta.env.MATRIXVIS,
  SEMANTICSUBSTRATESVIS: import.meta.env.SEMANTICSUBSTRATESVIS,
  MAPVIS: import.meta.env.MAPVIS,
  VIS0D: import.meta.env.VIS0D,
  VIS1D: import.meta.env.VIS1D,

  INSIGHT_SHARING: import.meta.env.INSIGHT_SHARING,
  VIEWER_PERMISSIONS: import.meta.env.VIEWER_PERMISSIONS,
  SHARABLE_EXPLORATION: import.meta.env.SHARABLE_EXPLORATION,
  EDGE_BUNDLING: import.meta.env.EDGE_BUNDLING,

  MAPBOX_TOKEN: import.meta.env.MAPBOX_TOKEN,
};

type EnvVarKey = keyof typeof envVar;

function getEnvVariable(key: EnvVarKey): string | undefined {
  const value = envVar[key];
  if (value === undefined) {
    console.error(`Environment variable ${key} is not defined`);
    return;
  }
  return value;
}

export { getEnvVariable };
export type { EnvVarKey };
