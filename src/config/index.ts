export * from './colors';
export * from './envVariables';

export const ATTRIBUTE_MAX_CHARACTERS = 500;
