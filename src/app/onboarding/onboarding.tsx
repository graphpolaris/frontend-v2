import React, { useState, useEffect } from 'react';
import Joyride, { ACTIONS, EVENTS, STATUS, Step } from 'react-joyride';
import { useLocation } from 'react-router-dom';
import { Button } from '@/lib/components/buttons';
import { useCases } from './use-cases';
import { OnboardingTooltip } from './tooltip';

interface OnboardingState {
  run?: boolean;
  stepIndex?: number;
}

export function Onboarding() {
  const location = useLocation();
  const [showWalkthrough, setShowWalkthrough] = useState<boolean>(false);
  const [onboarding, setOnboarding] = useState<OnboardingState>({
    run: false,
    stepIndex: 0,
  });

  useEffect(() => {
    // Check whether walkthrough cookie exists and if user is logged in
    const isWalkthroughCompleted = document.cookie.includes('walkthroughCompleted=true');
    if (!isWalkthroughCompleted) {
      setShowWalkthrough(true);
    }
  }, []);

  const handleJoyrideCallback = (data: { action: any; index: any; status: any; type: any }) => {
    const { action, index, status, type } = data;
    if ([EVENTS.STEP_AFTER, EVENTS.TARGET_NOT_FOUND].includes(type)) {
      setOnboarding({ stepIndex: index + (action === ACTIONS.PREV ? -1 : 1) });
    } else if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status)) {
      setOnboarding({ run: false });
      addWalkthroughCookie();
    }
  };

  const startWalkThrough = () => {
    setOnboarding({ ...onboarding, run: true });
    setShowWalkthrough(false);
  };

  const addWalkthroughCookie = () => {
    setShowWalkthrough(false);
    const date = new Date();
    date.setTime(date.getTime() + 24 * 60 * 60 * 1000 * 365);
    document.cookie = `walkthroughCompleted=true; expires=${date.toUTCString()};`;
  };

  const script: Array<Step> = useCases[location.pathname as keyof typeof useCases] || useCases.general;

  return (
    <div>
      {showWalkthrough && (
        <div className="bg-light alert absolute bottom-5 left-5 w-fit cursor-pointer z-50">
          <Button onClick={startWalkThrough} label={'Start a Tour'} variant="ghost" />
          <Button onClick={() => addWalkthroughCookie()} iconComponent="icon-[ic--baseline-close]" variant="ghost" rounded />
        </div>
      )}
      <Joyride
        run={onboarding.run}
        continuous={true}
        stepIndex={onboarding.stepIndex}
        steps={script}
        showProgress={true}
        showSkipButton={true}
        hideCloseButton={true}
        callback={handleJoyrideCallback}
        tooltipComponent={OnboardingTooltip}
        styles={{
          options: {
            primaryColor: '#FF7D00',
          },
          tooltip: {
            borderRadius: 0,
          },
          buttonNext: {
            borderRadius: 0,
          },
        }}
      />
    </div>
  );
}
