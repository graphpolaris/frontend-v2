import React from 'react';
import { TooltipRenderProps } from 'react-joyride';

const OnboardingTooltip = (props: TooltipRenderProps) => {
  const { backProps, closeProps, continuous, index, primaryProps, skipProps, step, tooltipProps } = props;

  return (
    <div className="bg-light p-4 rounded-lg shadow-lg" {...tooltipProps}>
      <button className="absolute top-2 right-2 text-secondary-500 hover:text-secondary-700" {...closeProps}>
        &times;
      </button>
      {step.title && <h4 className="text-lg font-semibold mb-2">{step.title}</h4>}
      <div className="mb-4">{step.content}</div>
      <div className="flex justify-between items-center">
        <button className="text-sm text-gray-500 hover:text-gray-700" {...skipProps}>
          {skipProps.title}
        </button>
        <div className="flex space-x-2">
          {index > 0 && (
            <button className="bg-light-200 text-light-700 px-3 py-1 rounded hover:bg-light-300" {...backProps}>
              {backProps.title}
            </button>
          )}
          {continuous && (
            <button className="bg-primary text-light px-3 py-1 rounded hover:bg-primary-dark" {...primaryProps}>
              {primaryProps.title}
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export { OnboardingTooltip };
