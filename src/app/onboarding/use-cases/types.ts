import { ReactNode } from 'react';
import { Step } from 'react-joyride';

export type GPStep = Step & {
  content: ReactNode;
  disableBeacon?: boolean;
  event?: string;
  floaterProps?: any;
  hideFooter?: boolean;
  isFixed?: boolean;
  offset?: number;
  placement?: Placement | 'auto' | 'center';
  placementBeacon?: Placement;
  target: string | HTMLElement;
  title?: ReactNode;
};

type Placement =
  | 'top'
  | 'top-start'
  | 'top-end'
  | 'bottom'
  | 'bottom-start'
  | 'bottom-end'
  | 'left'
  | 'left-start'
  | 'left-end'
  | 'right'
  | 'right-start'
  | 'right-end';
