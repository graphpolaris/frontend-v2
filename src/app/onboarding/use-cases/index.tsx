import { generalScript } from './general';
import { fraudScript } from './fraud';

export const useCases = {
  general: generalScript,
  fraud: fraudScript,
};
