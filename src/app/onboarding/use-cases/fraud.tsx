import { GPStep } from './types';

export const fraudScript: GPStep[] = [
  // TODO: make script for fraud detection

  {
    target: '.schema-panel',
    title: 'Schema Panel',
    content: 'The Schema Panel displays the structure of your database. You can drag pills from here to the query builder',
    placement: 'right',
    disableBeacon: true,
  },
];
