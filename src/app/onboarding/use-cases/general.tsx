import { GPStep } from './types';

export const generalScript: GPStep[] = [
  {
    target: '.schema-panel',
    title: 'Schema Panel',
    content:
      'The schema panel displays the structure of your database. The orange pills are nodes in your database, and the blue pills are edges. You can drag pills from here to the query builder to construct queries.',
    placement: 'right',
    disableBeacon: true,
  },
  {
    target: '.schema-settings',
    title: 'Schema Settings',
    content: 'Access schema settings to customize the style, structure and layout of the schema panel.',
    placement: 'right',
    disableBeacon: true,
  },
  {
    target: '.query-panel',
    title: 'Query Builder',
    content:
      'Use the query builder to create queries for your graph database. You can drag pills from the schema panel into here, and connect pills based on your needs.',
    placement: 'top',
    disableBeacon: true,
  },
  {
    target: '.query-settings',
    title: 'Query Builder Settings',
    content:
      'Adjust query builder settings to fine-tune your query-building experience. You can also incorporate machine-learning options into your query.',
    placement: 'left',
    disableBeacon: true,
  },
  {
    target: '.vis-panel',
    title: 'Visualization Panel',
    content: "The visualization panel is where you'll see your data visualizations, with the data from the executed query.",
    placement: 'left',
    disableBeacon: true,
  },
  {
    target: '.searchbar',
    title: 'Searching',
    content:
      'Utilize the search bar to search through GraphPolaris efficiently. You can use keywords like @schema, @data or @querybuilder to refine search scope.',
    placement: 'bottom',
    disableBeacon: true,
  },
  {
    target: '.database-menu',
    title: 'Menu',
    content: 'In the menu you can manage databases, switch between them, and perform other actions.',
    placement: 'bottom',
    disableBeacon: true,
  },
];
