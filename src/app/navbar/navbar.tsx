'use client';

/**
 * This program has been developed by students from the bachelor Computer Science at
 * Utrecht University within the Software Project course.
 * © Copyright Utrecht University (Department of Information and Computing Sciences)
 */

/* istanbul ignore file */
/* The comment above was added so the code coverage wouldn't count this file towards code coverage.
 * We do not test components/renderfunctions/styling files.
 * See testing plan for more details.*/
import React, { useState, useRef, useEffect } from 'react';
import GpLogo from './gp-logo';
import { getEnvVariable } from '@/config';
import { useAuthentication, useAuthCache, useActiveSaveStateAuthorization, Button, DropdownItem, useAppDispatch } from '@/lib';
import { FeatureEnabled } from '@/lib/components/featureFlags';
import { Popover, PopoverTrigger, PopoverContent } from '@/lib/components/popover';
import { ManagementViews, ManagementTrigger } from '@/lib/management';
import { addError } from '@/lib/data-access/store/configSlice';

export const Navbar = () => {
  const dispatch = useAppDispatch();
  const dropdownRef = useRef<HTMLDivElement>(null);
  const auth = useAuthentication();
  const authCache = useAuthCache();
  const authorization = useActiveSaveStateAuthorization();
  const [menuOpen, setMenuOpen] = useState(false);
  const buildInfo = getEnvVariable('GRAPHPOLARIS_VERSION');
  const [managementOpen, setManagementOpen] = useState<boolean>(false);
  const [current, setCurrent] = useState<ManagementViews>('overview');
  const [sharing, setSharing] = useState<boolean>(false);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
        setMenuOpen(false);
      }
    };
    if (menuOpen) document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [menuOpen]);

  return (
    <nav className="w-full h-11 flex flex-row items-center">
      <div className="w-11 flex items-center justify-center">
        <GpLogo className="h-7 w-7" includeText={false} />
      </div>
      <div className="flex grow items-center flex-row gap-2 pr-3">
        <ManagementTrigger
          managementOpen={managementOpen}
          setManagementOpen={setManagementOpen}
          current={current}
          setCurrent={setCurrent}
        />

        <Button
          label="Share"
          variantType="primary"
          size="md"
          iconComponent={sharing ? 'icon-[line-md--loading-loop]' : ''}
          disabled={sharing}
          iconPosition="trailing"
          onClick={async () => {
            setSharing(true);
            await auth.shareLink();
            setSharing(false);
          }}
        />
        <div className="ml-auto">
          <div className="w-fit" ref={dropdownRef}>
            <Popover>
              <PopoverTrigger>
                <div
                  className="relative inline-flex items-center justify-center w-8 h-8 overflow-hidden bg-secondary-500 rounded-full hover:bg-secondary-600 transition-colors duration-150 ease-in-out cursor-pointer"
                  onClick={() => setMenuOpen(!menuOpen)}
                >
                  <span className="font-medium text-light">{authCache.authentication?.username?.slice(0, 2).toUpperCase()}</span>
                </div>
              </PopoverTrigger>

              <PopoverContent className="w-56 z-30 bg-light divide-y divide-secondary-200">
                <div className="text-sm p-2">
                  <h2 className="font-bold">user: {authCache.authentication?.username}</h2>
                  <h3 className="text-xs break-words">session: {authCache?.authentication?.sessionID}</h3>
                  <h3 className="text-xs break-words">license: Creator</h3>
                </div>
                {authCache.authentication?.authenticated ? (
                  <div className="p-2">
                    <FeatureEnabled featureFlag="SHARABLE_EXPLORATION">
                      <DropdownItem
                        value={sharing ? 'Creating Share Link' : 'Share'}
                        disabled={sharing}
                        onClick={async () => {
                          setSharing(true);
                          try {
                            await auth.shareLink();
                            setSharing(false);
                          } catch (e) {
                            setSharing(false);
                            dispatch(addError('Failed to create share link'));
                          }
                        }}
                      />
                    </FeatureEnabled>
                    <FeatureEnabled featureFlag="VIEWER_PERMISSIONS">
                      {authCache.authorization?.savestate?.W && authorization.database?.W && (
                        <DropdownItem
                          value="Viewer Permissions"
                          onClick={() => {
                            setManagementOpen(true);
                            setCurrent('members');
                          }}
                        />
                      )}
                    </FeatureEnabled>
                    <DropdownItem value="Settings" onClick={() => {}} />
                    <DropdownItem
                      value="Log out"
                      onClick={() => {
                        const current = encodeURIComponent(window.location.href);
                        location.replace(`${getEnvVariable('GP_AUTH_URL')}if/flow/default-invalidation-flow/?next=${current}`);
                      }}
                    />
                    {authCache.authorization?.demoUser?.R && (
                      <DropdownItem
                        value="Impersonate Demo User"
                        onClick={() => {
                          const url = new URL(window.location.href);
                          url.searchParams.append('impersonateID', 'demoUser');
                          location.replace(url.toString());
                        }}
                      />
                    )}
                  </div>
                ) : (
                  <>
                    <DropdownItem value="Login" onClick={() => {}} />
                  </>
                )}

                {authCache.authentication?.roomID && (
                  <div className="p-2">
                    <h3 className="text-xs break-words">Share ID: {authCache.authentication?.roomID}</h3>
                  </div>
                )}
                <div className="p-2">
                  <h3 className="text-xs">Version: {buildInfo}</h3>
                </div>
              </PopoverContent>
            </Popover>
          </div>
        </div>
      </div>
    </nav>
  );
};
