import type { Config } from 'tailwindcss';
import * as defaultTheme from 'tailwindcss/defaultTheme';
import typography from '@tailwindcss/typography';
import daisyui from 'daisyui';
import daisyTheme from 'daisyui/src/theming/themes';
import { tailwindColors } from './src/config/colors';
import { addDynamicIconSelectors } from '@iconify/tailwind';

export default {
  content: ['./index.html', 'src/**/*.{js,ts,jsx,tsx,mdx}', '../../libs/*/lib/**/*.{js,ts,jsx,tsx,mdx}'],
  darkMode: ['class', '.dark-mode'],  // setting the dark mode class is important for the dark mode to work
  theme: {
    borderRadius: {
      none: 0,
      '2xs': '1px',
      xs: '0.125rem',
      sm: '0.1875rem',
      DEFAULT: '0.25rem',
      md: '0.375rem',
      lg: '0.5rem',
      xl: '0.75rem',
      '2xl': '1rem',
      '3xl': '1.5rem',
      full: '9999px',
    },
    fontSize: {
      '3xs': ['0.625rem', { lineHeight: '1.2' }],
      '2xs': ['0.6875rem', { lineHeight: '1.2727' }],
      xs: ['0.75rem', { lineHeight: '1.33333' }],
      sm: ['0.875rem', { lineHeight: '1.42857' }],
      base: ['1rem', { lineHeight: '1.5' }],
      lg: ['1.125rem', { lineHeight: '1.55556' }],
      xl: ['1.25rem', { lineHeight: '1.4' }],
      '2xl': ['1.5rem', { lineHeight: '1.33333' }],
      '3xl': ['1.875rem', { lineHeight: '1.2' }],
      '4xl': ['2.25rem', { lineHeight: '1.11111' }],
      '5xl': ['3rem', { lineHeight: '1' }],
      '6xl': ['3.75rem', { lineHeight: '1' }],
      '7xl': ['4.5rem', { lineHeight: '1' }],
      '8xl': ['6rem', { lineHeight: '1' }],
    },
    fontFamily: {
      sans: ['Inter', ...defaultTheme.fontFamily.sans],
      mono: ['Roboto Mono', ...defaultTheme.fontFamily.mono],
    },
    extend: {
      borderColor: {
        DEFAULT: 'hsl(var(--clr-sec--200) / <alpha-value>)',
      },
      colors: tailwindColors,
      animation: {
        openmenu: 'openmenu 0.3s ease-out',
        closemenu: 'closemenu 0.3s ease-out',
      },
      keyframes: {
        openmenu: {
          '0%': { opacity: '0' },
          '100%': { opacity: '1' },
        },
        closemenu: {
          '0%': { opacity: '1' },
          '100%': { opacity: '0' },
        },
      },
    },
  },
  plugins: [typography, daisyui, addDynamicIconSelectors({ scale: 0 })],
  // daisyUI config (optional - here are the default values)
  daisyui: {
    themes: [
      {
        graphpolarisWhite: {
          ...(daisyTheme as Record<string, Record<string, string>>)['[data-theme=corporate]'],
          primary: '#112646',
          secondary: '#B0B8C2',
          accent: '#FF7D00',
          neutral: '#1c1917',
          'base-100': '#F7F9FA',
          // 'base-200': '#F9FBFC',
          info: '#99622E',
          success: '#35a06a',
          warning: '#eab308',
          error: '#dc2626',

          // "--rounded-box": "0rem", // border radius rounded-box utility class, used in card and other large boxes
          // "--rounded-btn": "0.5rem", // border radius rounded-btn utility class, used in buttons and similar element
          // "--rounded-badge": "1.9rem", // border radius rounded-badge utility class, used in badges and similar
          // "--animation-btn": "0.25s", // duration of animation when you click on button
          // "--animation-input": "0.2s", // duration of animation for inputs like checkbox, toggle, radio, etc
          // "--btn-text-case": "uppercase", // set default text transform for buttons
          // "--btn-focus-scale": "0.95", // scale transform of button when you focus on it
          // "--border-btn": "1px", // border width of buttons
          // "--tab-border": "1px", // border width of tabs
          // "--tab-radius": "0.5rem", // border radius of tabs
        },
      },
      'dark',
      'corporate',
    ], // true: all themes | false: only light + dark | array: specific themes like this ["light", "dark", "cupcake"]
    darkTheme: false, // disabled for now
    // darkTheme: 'dark', // name of one of the included themes for dark mode
    base: false, // applies background color and foreground color for root element by default
    styled: true, // include daisyUI colors and design decisions for all components
    utils: true, // adds responsive and modifier utility classes
    rtl: false, // rotate style direction from left-to-right to right-to-left. You also need to add dir="rtl" to your html tag and install `tailwindcss-flip` plugin for Tailwind CSS.
    prefix: '', // prefix for daisyUI classnames (components, modifiers and responsive class names. Not colors)
    logs: false, // Shows info about daisyUI version and used config in the console when building your CSS
  },
};
