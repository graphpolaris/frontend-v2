import { configDefaults, defineConfig } from 'vitest/config';
import path from 'path';

export default defineConfig({
  test: {
    environment: 'jsdom',
    globals: true,
    threads: true,
    exclude: [...configDefaults.exclude],
    deps: {
      experimentalOptimizer: {
        web: { enabled: true },
        ssr: { enabled: true },
      },
    },
    benchmark: { reporters: ['default'] },
    reporters: ['default'],
    passWithNoTests: true,
    coverage: {
      provider: 'v8',
    },
    environmentOptions: {
      jsdom: {
        resources: 'usable',
      },
    },
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      '@graphpolaris/shared/lib': path.resolve(__dirname, './src/lib'),
      // "@graphpolaris/config": path.resolve(__dirname, "./src/config"),
    },
  },
});
