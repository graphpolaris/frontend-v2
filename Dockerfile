FROM oven/bun:alpine AS base
WORKDIR /app
ARG IMAGE_TAG="dev-no-image-tag"
ENV GRAPHPOLARIS_VERSION=$IMAGE_TAG


FROM base AS install
WORKDIR /app
COPY package.json bun.lockb ./
RUN apk add --no-cache git
RUN GIT_COMMIT=$(git ls-remote https://git.science.uu.nl/graphpolaris/ts-common.git HEAD | awk '{ print $1 }') \
    && echo "GIT_COMMIT=$GIT_COMMIT" \
    && sed -i "s|link:ts-common|git+https://git.science.uu.nl/graphpolaris/ts-common.git#$GIT_COMMIT|" ./package.json

# For some reason we need to first install to then be able to install for prod with frozen lockfile
RUN bun install
RUN bun install --frozen-lockfile --production


FROM base AS build
WORKDIR /app
COPY . ./
COPY --from=install /app /app
# Fixes: FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory
ENV NODE_OPTIONS="--max-old-space-size=8192"
ENV BUN_JSC_forceRAMSize="8192"
RUN bun run build


FROM base AS env-build
# Automatically set by build process: https://docs.docker.com/reference/dockerfile/#automatic-platform-args-in-the-global-scope
ARG TARGETARCH
WORKDIR /app
COPY --from=install /app/node_modules /app/node_modules
RUN if [ "$TARGETARCH" = "arm64" ]; then ARCHITECTURE=arm64; else ARCHITECTURE=x64; fi \
    && echo "System architecture: $ARCHITECTURE" \
    && bunx pkg ./node_modules/@import-meta-env/cli/bin/import-meta-env.js --target node18-alpine-${ARCHITECTURE} --output import-meta-env-alpine

FROM nginx:1.25-alpine
WORKDIR /app
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY entrypoint.sh /usr/bin/entrypoint.sh
COPY .env.example /.env.example
COPY --from=env-build /app/import-meta-env-alpine /import-meta-env-alpine
COPY --from=build /app/dist /usr/share/nginx/html
RUN chmod +x /usr/bin/entrypoint.sh
EXPOSE 4200
ENTRYPOINT ["/usr/bin/entrypoint.sh"]
# ENTRYPOINT ["ls", "-la"]

