
#  windows:
# 	bun build

#  macos:
# 	bun build

# linux:
# 	bun build

# @docker push harbor.graphpolaris.com/graphpolaris/frontend\:latest

setup:
	cp .env.development .env
	bun link ts-common
	bun install

build:
	@docker build -t harbor.graphpolaris.com/graphpolaris/frontend:latest .

run:
	@docker run --publish 4200:4200 harbor.graphpolaris.com/graphpolaris/frontend:latest

brun: build run

push:
	bun lint
	bun vitest

clean:
	rm -rf node_modules
	bun i

cleanrm:
	rm -rf pnpm-lock.yaml
	rm -rf bun.lockb
	rm -rf node_modules
	bun i
	bun build
